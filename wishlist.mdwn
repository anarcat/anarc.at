Voici des choses que vous pouvez m'acheter si vous êtes le Père Nowel (yeah right):

 * <del>une [ceinture fléchée](http://fr.wikipedia.org/wiki/Ceinture_fléchée)</del> - je n'ai plus le manteau pour aller avec et c'est trop cher...
 * la paix dans le monde (bon je sais que ça s'achète pas, m'enfin)
 * posters:
   * <http://laughingsquid.com/the-evolution-of-bicycles-by-pop-chart-lab/>
   * [Radio spectrum poster](http://www.unihedron.com/projects/spectrum/) ou [l'allocation canadienne](http://e-com.ic.gc.ca/eic/site/ic1.nsf/eng/03784.html)
   * [le poster "height"](http://store.xkcd.com/products/height-poster) de [xkcd.net](http://xkcd.net/)
   * [space exploration](http://i.imgur.com/UqQdj.jpg)
   * [network protocols](http://i.imgur.com/MZHN3.gif)
   * [Territory of the US](http://i.imgur.com/5fz82.jpg)
   * [US military bases](http://i.imgur.com/Y4ZWY.jpg)
   * [submarine cable map](https://blog.telegeography.com/topic/map), for example the [2022 one](https://submarine-cable-map-2022.telegeography.com/)
   * [internet maps](http://chrisharrison.net/projects/InternetMap/index.html)
   * une carte du monde [Dymaxion](http://en.wikipedia.org/wiki/Dymaxion_map), [Werner](http://en.wikipedia.org/wiki/Werner_map_projection) ou [Gall-Peters](http://en.wikipedia.org/wiki/Gall-Peters_projection)
 * <del>un laptop [novena](https://www.crowdsupply.com/kosagi/novena-open-laptop)</del>
   voir [[hardware/laptop]]
 * <del>un mini-PC comme le [fitlet](http://www.fit-pc.com/web/products/fitlet/) ([review](http://linuxgizmos.com/tiny-fanless-mini-pc-runs-linux-on-quad-core-amd-soc/))</del> j'ai
   achete un Intel NUC, voir [[hardware/laptop]]
 * un bon stylo (voir [cette liste](http://coolmaterial.com/feature/pens-of-kickstarter/), particulièrement le [Pen Type-A](https://www.kickstarter.com/projects/cwandt/pen-type-a-a-minimal-pen) a une règle, mais est très cher (150$), alors que le [PHX](http://www.bigidesign.com/welcome/phx-pen-2/) est aussi compatible avec les recharges Hi-Tec-C mais est seulement 30$)
 * d'autres trucs de [xkcd.net](http://shop.xkcd.net/), particulièrement [ce t-shirt](http://store.xkcd.com/xkcd/#TechSupport)
 * des livres:
   * romans:
     * Les armureries d'isher ou tout le cycle des marchands d'armes - en fait n'importe quoi de Van Vogt
     * anything from [Philip K. Dick](http://www.philipkdick.com/) (j'ai maintenant [un recueil de 5 nouvelles entre 1960 et 1970](http://www.amazon.com/gp/product/1598530259) et             '''     [The Man in the High Castle](http://www.amazon.ca/gp/product/0679740678/ref=ox_ya_os_product)''')
     * [Terry Pratchett](http://www.terrypratchettbooks.com/) (j'ai lu The Color of Magic, Equal Rites, The Light Fantastic, Thief of Time, Sourcery, ...)
     * John Irving
     * La série [The Atrocity Archives](http://en.wikipedia.org/wiki/The_Atrocity_Archives) de Charles Strauss
     * Du Isaac Asimov (j'en ai plusieurs, à vérifier)
     * [#aiww: the arrest of Ai Weiwei](http://search.perseusbooksgroup.com/book/paperback/aiww-the-arrest-of-ai-weiwei/9781848423312)
     * [#freetopiary](http://search.perseusbooksgroup.com/book/hardcover/freetopiary/9781628737110)
     * [d'autres livres de geek](http://blogs.guardian.co.uk/technology/archives/2005/11/09/top_20_geek_novels_the_results.html)
   * programmation:
     * la série [the art of computer programming](http://www-cs-faculty.stanford.edu/~knuth/taocp.html), les 4 volumes svp
     * [the art of electronics](http://www.artofelectronics.com/)
     * [programming pearls](http://www.cs.bell-labs.com/cm/cs/pearls/)
   * voile
     * [Atlas des océans](http://www.boutique.voilesetvoiliers.com/atlas-des-oceans,fr,4,92216.cfm) (ou les Pilot Charts, maintenant librement disponibles [en ligne](http://msi.nga.mil/NGAPortal/MSI.portal?_nfpb=true&_pageLabel=msi_portal_page_62&pubCode=0003) mais j'aimerais une version imprimée
     * [livre de bord fantaisiste](http://www.boutique.voilesetvoiliers.com/guide-des-antilles,fr,4,92255.cfm) - vérifier si j'en ai pas déjà un, ce qui est fort probable
     * [Connaître les cordages modernes et leurs usages à bord](http://www.boutique.voilesetvoiliers.com/bien-barrer-son-voilier,fr,4,92294_copie.cfm) un autre livre de noeuds!
     * [Le dictionnaire de la mer : savoir-faire, traditions, vocabulaire, techniques](http://www.worldcat.org/oclc/6327481) de Jean Merrien - Renaud Bray a une édition différente, voir [ISBN:9782258113275](https://en.wikipedia.org/wiki/Special:BookSources/9782258113275)
     * [Lexique nautique polyglotte](http://www.worldcat.org/oclc/21840200) - peut-être? du même auteur (Jean Merrien)
     * Les livres de Carl Mailhot et Yves Gélinas: La V'limeuse autour
       du monde, tome 1 et suivants
       ([ISBN:9782980447303](https://en.wikipedia.org/wiki/Special:BookSources/9782980447303), [chez abebooks.com](http://www.abebooks.com/servlet/BookDetailsPL?bi=14061305584&searchurl=x%3D0%26amp%3By%3D0%26amp%3Bbi%3D0%26amp%3Bds%3D30%26amp%3Bsts%3Dt%26amp%3Bbx%3Doff%26amp%3Bsortby%3D17%26amp%3Ban%3DCarl+Mailhot%26amp%3Brecentlyadded%3Dall)), De la
       V'limeuse a Dingo: L'Atlantique en solitaire sur un 6,50 Metres
       ([ISBN:9782980447327](https://en.wikipedia.org/wiki/Special:BookSources/9782980447327), [chez abebooks.com](http://www.abebooks.com/servlet/BookDetailsPL?bi=8882922329&searchurl=an%3DCarl+Mailhot%2C+Dominique+Manny)), Jean du Sud
       et l'Oizo-Magick
       ([ISBN: 9782857251842](https://en.wikipedia.org/wiki/Special:BookSources/9782857251842), [chez abebooks.com](http://www.abebooks.com/servlet/BookDetailsPL?bi=14250044964&searchurl=sts%3Dt%26amp%3By%3D0%26amp%3Bx%3D0%26amp%3Bkn%3D9782857251842), aussi
       en [DVD](http://www.capehorn.com/TrailerAng.htm))
   * autres
     * <https://xkcd.com/how-to/>
     * [La théorie du drone](http://www.worldcat.org/oclc/847564093)
     * [The ARRL Operating Manual](http://www.arrl.org/shop/The-ARRL-Operating-Manual/)
     * [Les idées noires](https://en.wikipedia.org/wiki/Id%C3%A9es_noires) de Franquin, [l'intégrale](http://www.worldcat.org/oclc/493932411)
     * [99% invisible city](https://99percentinvisible.org/book/)
 * <del>une liseuse 13" comme le [Sony DPT-S1](https://www.sony.com/electronics/digital-paper-notepads/dpts1#product_details_default) ou le [Onyx BOOX Max](https://onyxboox.com/boox_max),
   ou encore une tablette rootable qui roule le plus de logiciel libre
   possible</del> - j'en ai un maintenant, voir aussi [[hardware/tablet]]
 * des longues vacances au costa rica, dans le charlevoix ou à une autre place pas rapport
 * un "portable image scanner" comme le [SVP 4500](http://www.svp-tech.com/ps4400/ps4400.html) ou le Wolverine
   Data pass
 * un [[hardware/radio/FmTransmitter]]
 * un transceiver générique, e.g. le [hack RF](https://greatscottgadgets.com/hackrf/), esp. avec le [portapack](https://sharebrained.myshopify.com/products/portapack-for-hackrf-one)
 * <del>un appareil photo digital reflex de qualité...</del> - j'en ai
   un maintenant, voir [[hardware/camera]]

Voir aussi [[hardware]] pour le matériel que j'ai déjà...
