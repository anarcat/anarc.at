[[!toc levels=2]]

# Principales contributions au logiciel libre

## Debian

Je suis impliqué dans le projet [Debian][] depuis plusieurs années, et un développeur Debian depuis octobre 2011. Vous pouvez consulter ici [la liste de mes packages](http://qa.debian.org/developer.php?login=anarcat) ou plus largement mes [contributions à Debian](https://contributors.debian.org/contributor/anarcat%40debian).

[Debian]: https://debian.org/

J'ai écrit un manuel
[[d'entretien de packages debian|debian-development]] (en anglais)
parce qu'il semblait qu'il n'y avait rien de complet à ce niveau...

## feed2exec

J'ai écrit un lecteur de fils RSS nommé [feed2exec](https://gitlab.com/anarcat/feed2exec) afin de
connecter des fils RSS avec n'importe quoi: courriels, Twitter,
Mastodon, téléchargements, etc.

## pubpaste

[pubpaste](https://gitlab.com/anarcat/pubpaste) permet de facilement publier des captures d'écran, du
texte, des images, des galeries de photos, avec un simple serveur web
et un accès SSH.

## rsendmail and sshsendmail

[rsendmail](https://gitlab.com/anarcat/rsendmail) permet de gérer l'envoi de courriels par SSH, sans mot
de passe, à partir d'ordinateurs distants (par exemple un laptop).

## stressant

J'ai écrit un logiciel pour faire le [Burn-in][] de nouvelles
machines, nommé [stressant][]. Il est basé sur [Grml][], lui-même
dérivé de [Debian][].

[Grml]: https://grml.org/
[stressant]: https://gitlab.com/anarcat/stressant
[Burn-in]: https://en.wikipedia.org/wiki/Burn-in

## undertime

[undertime](https://gitlab.com/anarcat/undertime) permet de (pardonnez l'anglais) "quickly pick a meeting
time across multiple timezones for conference calls or other
coordinated events".

## wallabako

J'utilise [Wallabag](http://wallabag.org/) pour répertorier des
articles à lire plus tard. Pour éviter d'avoir à les lire sur un
ordinateur, j'ai écrit un programme embarqué nommé [Wallabako][] pour
les transférer sur ma liseuse électronique.

[Wallabako]: https://gitlab.com/anarcat/wallabako

# Autres contributions

Je milite pour les logiciels libres depuis la fin des années 1990 et
j'écris du logiciel libre depuis au moins 2000. J'essaie de maintenir
une [[liste presque exhaustive|contributions]] des projets libres
auxquels j'ai participé. Vous pouvez également constater mon activité
sur [Openhub](https://www.openhub.net/accounts/anarcat), [Gitlab](https://gitlab.com/users/anarcat/projects) ou [Github](https://github.com/anarcat?utf8=%E2%9C%93&tab=repositories&q=&type=source&language=).

# Documentation

(!) Voir la documentation de [[maintenance du réseau|services]].

Je fais *beaucoup* de documentation sur les logiciels que j'utilise,
principalement sur le [wiki de Koumbit](https://wiki.koumbit.net) mais
aussi localement, dans la page [[services]].

J'ai rapporté également une multitude de bugs sur des dizaines de
projets logiciels, principalement sur des packages Debian, voir
[ici](http://bugs.debian.org/from:anarcat@debian.org),
[ici](http://bugs.debian.org/cgi-bin/pkgreport.cgi?correspondent=anarcat%40debian.org)
ou encore
[ici](http://bugs.debian.org/cgi-bin/pkgreport.cgi?correspondent=anarcat%40koumbit.org). Voir
aussi [mon activité sur Github](https://github.com/anarcat?tab=activity).

# Mon histoire

La maintenance de mon [[desktop]] est quelquechose qui a toujours pris un certain temps, et qui témoigne mon enthousiasme pour les logiciels libres. J'ai pris un peu de temps pour documenter dans ma page [[software/history]] les différentes configurations que j'utilise, les tests que j'ai fait et les languages que j'ai appris.

# Projets désuets

## Monkeysphere

Le projet [Monkeysphere](http://monkeysphere.info/) visait à rendre la cryptographie OpenPGP plus accessible au commun des mortels. En particulier, j'ai écrit un logiciel nommé [Monkeysign](http://monkeysphere.info/monkeysign) qui permet de facilement signer des clefs PGP. J'ai écrit aussi un [guide simple pour l'utilisation de Monkeysphere](http://web.monkeysphere.info/getting-started-ssh/).

Le projet Monkeysign est [abandonné](https://0xacab.org/monkeysphere/monkeysign/-/issues/64) depuis 2018, en faveur
d'alternatives telles que [GNOME Keysign](https://wiki.gnome.org/Apps/Keysign), [pius](https://www.phildev.net/pius/) ou [caff](https://tracker.debian.org/pkg/signing-party).

## Freedom Box

J'ai travaillé, par l'entremise de Debian, à l'avancement du projet
[Freedom Box](http://freedomboxfoundation.org/). J'ai depuis longtemps un serveur "freedombox" chez
moi, il s'agit maintenant de partager comment on fabriquer un avec
tout le monde, et faire que c'est facile à gérer et utiliser. Voir
aussi [[services]].

> _We're building software for smart devices whose engineered purpose is to work together to facilitate free communication among people, safely and securely, beyond the ambition of the strongest power to penetrate. They can make freedom of thought and information a permanent, ineradicable feature of the net that holds our souls._ - [Eben Moglen](http://moglen.law.columbia.edu/)

# Autre documentation

Voici l'inventaire de la documentation non répertoriée explicitement ci-haut:

[[!orphans pages="page(services/*)"]]

# Toute la documentation

[[!map pages="page(services/*)"]]
