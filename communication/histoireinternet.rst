=============================
Histoire populaire d'Internet
=============================

:Auteur: Robin Millette, hacker expérimental
:Auteur: Antoine Beaupré, historien amateur
:Auteur: Mathieu Petit-Clair

L'histoire d'Internet est reliée aux développements
technologiques de la communication humaine et c'est pourquoi nous
amorçons notre histoire avec le développement des réseaux de
communications et de transport dans le monde. Comme le téléphone et le
télégraphe qui naissent à la même époque, le réseau de chemin de fer
devient aiguillé électriquement (à partir de 1832), manuellement au 
départ (à la manière des téléphonistes d'antan) puis automatiquement ensuite. On établissait
un chemin complet qu'on pouvait ensuite utiliser. Il s'agissait alors
de commutations de circuits.

Ce que les militaires nous ont laissé
-------------------------------------

Il faudra attendre 1960 pour que Paul Baran jette les
premières bases de la commutation de paquets, qui sépare les
communications en plusieurs morceaux prenant chacun leurs
routes pour être tous réassemblés dans le bon ordre à leur
destination. Cette idée deviendra un notion fondamentale de
l'architecture "IP" (Internet Protocol), qui fonctionne donc
un peu comme les wagons de trains dans une gare d'aiguillage:
les données sont divisés en "paquets" (les wagons) qui sont 
distribués sur différents chemins (les rails et aiguillages)
et reconstruits à la destination.

Comment utiliser ce réseau de télécommunications originalement conçu
pour la voix et en faire un réseau générique?  Paul Baran rapporte [#]_
une conversation qu'il a eu avec Joern Ostermann de AT&T après une
session exaspérante avec lui: « Premièrement, ça ne peut tout
simplement pas fonctionner, et si ça le pouvait, on ne va quand même
pas permettre la création de notre propre compétition. »

Et pourtant, du train qui inspira la commutation de circuits vocaux,
on allait dorénavant numériser
l'information en paquets et réinventer tout le concept de réseau. En
fin de compte, c'est avec le financement de la Défense nationale des
États-Unis d'Amérique que l'ARPANET a été développé, dans le but, dit la légende, de
résister à une attaque nucléaire. Ce réseau jeta les bases
pratiques du fonctionnement de l'Internet actuel. Le premier
ordinateur, l'ENIAC [#]_, a d'ailleurs lui aussi été conçu, en 1946, sous
des motifs militaires mais cette fois c'était dans le but de calculer
les trajectoires de tir d'obus d'artillerie.

.. on parle aussi de TBL et du WWW plus bas... couper?
.. RYM: Je crois que c'est correct de même

En 1959, Peter Samson, curieusement membre du Tech Model Railroad Club (TMRC), au
MIT, déclare « *All information should be free* » ( « Toute information devrait être libre » ou
« gratuite », selon ce qu'on entend). Le TMRC est réputé pour être le
berceau des premiers "hackers" informatiques. À l'époque, ceux-ci réutilisaient des
pièces de commutateurs téléphoniques pour automatiser les circuits de
leurs chemins de fer miniatures. Et la boucle est bouclée: le MIT est passé du hobby des trains miniatures au hobby des réseaux de paquets informatiques.

.. *Quel est le rapport entre des hackers et des chemins de fer miniatures? Peut-être me trompe-je, mais il me semble que le simple fait d'expliquer cette idée serait un peu long...*
.. *La boucle est bouclée? Quelle boucle?*
.. c'est mieux?

Il n'est pas clair qui peut prendre le crédit pour la création du
réseau des réseaux moderne, mais au moins trois individus ont été
nommés les « pères d'Internet » pour leurs contributions
respectives. Il s'agit de Vinton Cerf (1970), pour le design de
TCP/IP (avec Robert Kahn); Tim Berners-Lee (1990), pour avoir conçu
les protocoles clés du World Wide Web; et Paul Baran (1960), mentionné
plus haut.

Les années 1970 voient le réseau ARPANET se transformer en Internet en
s'ouvrant aux universités et aux des grandes entreprises. En 1990,
rares étaient les résidences qui avaient un accès direct via un
fournisseur d'accès Internet (FAI). Le terrain se préparait alors
pour le World Wide Web, qui allait bientôt frapper à nos portes.

Ce que le nucléaire nous a légué
--------------------------------

Pendant ce temps, au Conseil européen pour la recherche nucléaire
(CERN) à Genève, Tim Berners-Lee et Robert Cailliau conçoivent les
protocoles à la base du World Wide Web et programment les premiers
serveurs et clients. Ils déploient ainsi leur Toile (le "Web") comme une autre
couche sur le réseau Internet en place. Ainsi en 1993, le CERN lègue au domaine
public le premier fureteur et serveur web, logiciel et "code source" inclus.
Ceci permet alors à quiconque de modifier, d'adapter ou
de créer de nouveaux logiciels utilisant ces protocoles ouverts et
libres. Sans cette décision, le Web ne serait jamais sorti des laboratoires
selon Tim Berners-Lee [#]_. Parmi les accélérateurs de particules, la Toile
naissait. Petit train va loin.

Parmi les sites web notables à ses humbles débuts, mentionnons le
Internet Movie Database (IMDB), Sex.com, Whitehouse.gov, Yahoo!,
l'Archive sur les Simpsons et Pizza Hut.

.. pizza hut?
.. ci-bas: quel fureteur? Mosaic? ou amaya? (**Non, Erwise**)
.. aussi: la taxe microsoft, me semble que c'est windows, pas IE, qui
.. est bel et bien gratuit (**oui mais pour IE ça prend Windows**)
.. RYM: on peut considérer ces points ^ ^ comme résolus?

Comme le système d'exploitation libre GNU/Linux, le premier fureteur
en mode graphique, Erwise, a été conçu en Finlande [#]_. Et puis de l'Illinois nous
vint Mosaïc, qui permettait aux utilisateurs de PC Windows ou de
Macintosh de fureter graphiquement sur le Web. Cinq ans plus tard, en
1998, Netscape décide de libérer la version en développement de son
fureteur et ce projet devient Mozilla, qu'on connaît aujourd'hui pour
Firefox et Thunderbird. Netscape n'a jamais pu trouver un modèle
durable pour survivre face à Microsoft et Internet Explorer livré avec
tous les PC (mais pas gratuitement, c'est ce qu'on appelle la « taxe
Microsoft » et qui a fait la fortune de Bill Gates). En 1998, un
nouvel engin de recherche amical du nom de Google faisait une entrée
impitoyable dans un marché composé alors de plusieurs joueurs.

En 2000, 40% des Canadiens avaient accès à Internet. En 2008, c'est plus
du double, soit 84%. Le Web comptait un milliard de pages en 2000. En
2006, Google disait en indexer six milliard et cette croissance n'a
jamais cessé. Depuis quelques années, il est question de Web 2.0,
c'est à dire un Web qui se veut plus social, offrant une expérience
utilisateur et une interaction améliorée. La guerre des fureteurs est
derrière nous et les usages ont maturé. Parallèlement, on voit la
montée du Web 3.0, le Web porteur de sens, riche en informations et en
données assimilables, réutilisables et connectables. Nous en sommes donc au Web des
applications (Facebook, Google Mail, Myspace, etc) disponibles sur les téléphones celullaires et ordinateurs de plus en plus portables qui rappellent avec les immenses ordinateurs-autobus des débuts de l'informatique qu'on actionnait à partir d'un tout petit terminal de contrôle distant. L'accessibilité de ces ressources fait que ce ne sont plus là seulement l'apanage des scientifiques, chercheurs et militaires, mais des outils de masse disponibles à tous, et donc sociaux.

----

.. [#] http://www.privateline.com/Switching/gilder.html

.. [#] Daniel F. Burton, Jr., The Brave New Wired World, FOREIGN POLICY 22-37 (Spring 1997)

.. [#] http://tenyears-www.web.cern.ch/tenyears-www/Welcome.html

.. [#] http://www.w3.org/People.html#9
