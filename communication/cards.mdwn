# ideas for business cards

* [Debian template](https://people.debian.org/~hmh/debian-business-card/)
* [another source](https://www.debian.org/devel/misc/card.tex)

Another idea: use a simple circle A + cAt like the badge, with a nice
fonts.

Some font ideas:

 * https://fonts.google.com/specimen/Sanchez
 * https://fonts.google.com/specimen/Playfair+Display (in texlive)
 * https://fonts.google.com/specimen/Libre+Baskerville

There's also a GSF baskerville font in texlive which may be different
from the above but still interesting.

Then make an embosser with [this](http://www.thestampmaker.com/Products/Pocket-Embosser-with-Your-Text__EMBOSSER_WITH_TEXT_POCKET.aspx), maybe with "PROPERTY IS THEFT /
LA PROPRIÉTÉ C'EST DU VOL" around with the circle A in the middle.
