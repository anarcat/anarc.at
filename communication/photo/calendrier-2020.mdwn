[[!meta title="Projet Calendes 2020"]]

[[!toc levels=2]]

Dernière minute, miné par la vie, je m'essaie à nouveau sur ce projet
fou.

# Montage

Le montage du [[calendrier-2019#montage]] a été repris relativement intégralement.

# Dates choisies

Les dates de l'an passé ont été généralement réutilisées, mais on a
remis la fête des mères et des pères, qui avait été retirées à
l'impression.

## Fêtes importantes

On se limite à 4 jours identifiés par mois et/ou un par semaine.

 * 1er janvier: [Jour de l'an][]
 * 20 janvier: [MLK day][MLK], "Third Monday in January"
 * 14 février: [Saint-Valentin][]
 * 8 mars: [Fête des femmes][]
 * 11 mars: Début de l'[heure avancée](https://fr.wikipedia.org/wiki/Heure_d%27%C3%A9t%C3%A9) (on avance l'heure, deuxième
   dimanche de mars)
 * 14 mars: [Journée de Pi][]
 * 15 mars: [Journée contre la brutalité policière][JICBP]
 * 20 mars: [Équinoxe][] (peut varier d'une journée, selon les fuseaux horaires)
 * 1er avril: Naissance d'[Edmond Rostand][]
 * 20 avril: [Fête du pot][420] (au lieu du [Vendredi saint][])
 * 22 avril: [Jour de la terre][jour de la terre] (au lieu de [Pâques][])
 * 1er mai: [Fête des travailleurs][]
 * 20 mai: [Fête des mères][], deuxième dimanche de mai
 * 25 mai: [Jour de la serviette][Towel Day] (au lieu de la fête des patriotes le 20 mai)
 * 21 juin: [Fête des pères][], troisième dimanche de juin
 * 21 juin: [Solstice][] d'été, jour le plus long (au lieu de la St-Jean)
 * 1er juillet: [Jour du déménagement][] (au lieu de la fête nationale)
 * 13 août: [Jour des gauchers][]
 * 4 septembre: [Fête du travail][], premier lundi de septembre
 * 19 septembre: [Jour des pirates][]
 * 21 septembre: [Journée de la paix][]
 * 23 septembre: [Équinoxe][]
 * 12 octobre: [Jour des peuples autochtones][], au lieu de l'[action
   de grâce][Action de grâce] ou [Colombus day][Columbus day], deuxième lundi d'octobre
 * 31 octobre: [Halloween][]
 * 3 novembre: Début de l'heure normale (on recule l'heure, premier
   dimanche de novembre, 2:00)
 * 28 novembre: [Journée sans achat][], dernier vendredi de novembre
   (au lieu de [Thanksgiving][])
 * 14 décembre: [Fête des singes][]
 * 22 décembre: [Solstice][Solstice] d'hiver, jour le plus court
 * 25 décembre: [Naissance de Newton][Newtonmas] (au lieu de [Noël][])

[420]: https://en.wikipedia.org/wiki/420_(cannabis_culture)
[Abraham Maslow]: https://en.wikipedia.org/wiki/Abraham_Maslow
[Action de grâce]: https://fr.wikipedia.org/wiki/Action_de_gr%C3%A2ce_(Canada)
[Anarchisme]: https://en.wikipedia.org/wiki/Portal:Anarchism/Anniversaries
[Apple Inc.]: https://fr.wikipedia.org/wiki/Apple
[April 1st]: https://en.wikipedia.org/wiki/April_1
[April fool's day]: https://en.wikipedia.org/wiki/April_Fools%27_Day
[Autres évènements]: https://en.wikipedia.org/wiki/Lists_of_holidays
[Boxing day]: https://en.wikipedia.org/wiki/Boxing_Day
[Buy Nothing Day]: https://en.wikipedia.org/wiki/Buy_Nothing_Day
[Columbus day]: https://en.wikipedia.org/wiki/Columbus_Day
[Confédération]: https://fr.wikipedia.org/wiki/F%C3%AAte_du_Canada
[Darwin day]: https://en.wikipedia.org/wiki/Darwin_Day
[Day of the dead]: https://en.wikipedia.org/wiki/Day_of_the_Dead
[Diwali]: https://en.wikipedia.org/wiki/Diwali
[Edible book day]: https://en.wikipedia.org/wiki/Edible_Book_Festival
[Edmond Rostand]: https://en.wikipedia.org/wiki/Edmond_Rostand
[Eid al-Adha]: https://en.wikipedia.org/wiki/Eid_al-Adha
[Eid al-Fitr]: https://en.wikipedia.org/wiki/Eid_al-Fitr
[Festivus]: https://en.wikipedia.org/wiki/Festivus
[Fossil fools day]: https://en.wikipedia.org/wiki/Fossil_Fools_Day
[Friendship Day]: https://en.wikipedia.org/wiki/Friendship_Day
[Fête des femmes]: https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_des_femmes
[Fête des mères]: https://fr.wikipedia.org/wiki/F%C3%AAte_des_M%C3%A8res
[Fête des pères]: https://fr.wikipedia.org/wiki/F%C3%AAte_des_P%C3%A8res
[Fête des singes]: https://en.wikipedia.org/wiki/Monkey_Day
[Fête des travailleurs]: https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_des_travailleurs
[Fête du travail]: https://fr.wikipedia.org/wiki/F%C3%AAte_du_Travail
[Gmail]: https://fr.wikipedia.org/wiki/Gmail
[Google]: https://fr.wikipedia.org/wiki/Google
[Guy Fawkes Night]: https://en.wikipedia.org/wiki/Guy_Fawkes_Night
[Halloween]: https://fr.wikipedia.org/wiki/Halloween
[Human Rights Day]: https://en.wikipedia.org/wiki/Human_Rights_Day
[Independence day]: https://en.wikipedia.org/wiki/Independence_Day_(United_States)
[Indigenous Peoples' Day]: https://en.wikipedia.org/wiki/Indigenous_Peoples%27_Day
[Indépendance d'Haïti]: https://fr.wikipedia.org/wiki/Acte_de_l%27Ind%C3%A9pendance_de_la_R%C3%A9publique_d%27Ha%C3%AFti
[International Day of Peace]: https://en.wikipedia.org/wiki/International_Day_of_Peace
[International Lefthanders Day]: https://en.wikipedia.org/wiki/International_Lefthanders_Day
[International Talk Like a Pirate Day]: https://en.wikipedia.org/wiki/International_Talk_Like_a_Pirate_Day
[International day of the world's indigenous people]: https://en.wikipedia.org/wiki/International_Day_of_the_World%27s_Indigenous_Peoples
[JICBP]: https://en.wikipedia.org/wiki/International_Day_Against_Police_Brutality
[Jour de Darwin]: https://en.wikipedia.org/wiki/Darwin_Day
[Jour de l'an]: https://fr.wikipedia.org/wiki/Jour_de_l%27an
[Jour de l'environnement]: https://fr.wikipedia.org/wiki/Journ%C3%A9e_mondiale_de_l%27environnement
[Jour de la bastille]: https://fr.wikipedia.org/wiki/F%C3%AAte_nationale_fran%C3%A7aise
[Jour de la terre]: https://fr.wikipedia.org/wiki/Jour_de_la_Terre
[Jour des gauchers]: https://en.wikipedia.org/wiki/International_Lefthanders_Day
[Jour des peuples autochtones]: https://en.wikipedia.org/wiki/Indigenous_Peoples%27_Day
[Jour des pirates]: https://fr.wikipedia.org/wiki/International_Talk_Like_a_Pirate_Day
[Jour du déménagement]: https://fr.wikipedia.org/wiki/Jour_du_d%C3%A9m%C3%A9nagement
[Jour du souvenir]: https://fr.wikipedia.org/wiki/Jour_du_Souvenir
[Journée de Pi]: https://fr.wikipedia.org/wiki/Journ%C3%A9e_de_pi
[Journée de la paix]: https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_de_la_paix
[Journée nationale des Patriotes]: https://fr.wikipedia.org/wiki/Journ%C3%A9e_nationale_des_patriotes
[Journée sans achat]: https://fr.wikipedia.org/wiki/Journ%C3%A9e_sans_achat
[Kwanzaa]: https://en.wikipedia.org/wiki/Kwanzaa
[MLK]: https://en.wikipedia.org/wiki/Martin_Luther_King_Jr._Day
[Memorial day]: https://en.wikipedia.org/wiki/Memorial_Day
[Mexico]: https://en.wikipedia.org/wiki/Independence_Day_(Mexico)
[Monkey Day]: https://en.wikipedia.org/wiki/Monkey_Day
[Nanomonestotse]: https://en.wikipedia.org/wiki/Nanomonestotse
[National Gorilla Suit Day]: https://en.wikipedia.org/wiki/National_Gorilla_Suit_Day
[National Hugging Day]: https://en.wikipedia.org/wiki/National_Hugging_Day
[National day of reason]: https://en.wikipedia.org/wiki/National_Day_of_Reason
[Newtonmas]: https://en.wikipedia.org/wiki/Isaac_Newton_in_popular_culture#Newtonmas
[No Pants Day]: https://en.wikipedia.org/wiki/No_Pants_Day
[Noël]: https://fr.wikipedia.org/wiki/No%C3%ABl
[Nunavut]: https://fr.wikipedia.org/wiki/Nunavut
[Opposite Day]: https://en.wikipedia.org/wiki/Opposite_Day
[Pi Approximation Day]: https://en.wikipedia.org/wiki/Pi_Approximation_Day
[Pi Day]: https://en.wikipedia.org/wiki/Pi_day
[Pâques]: https://fr.wikipedia.org/wiki/P%C3%A2ques
[Ramadan]: https://fr.wikipedia.org/wiki/Ramadan
[Record store day]: https://en.wikipedia.org/wiki/Record_Store_Day
[Rosh Hashanah]: https://en.wikipedia.org/wiki/Rosh_Hashanah
[Royal Canadian Air Force]: https://fr.wikipedia.org/wiki/Royal_Canadian_Air_Force
[Saint-Valentin]: https://fr.wikipedia.org/wiki/Saint-Valentin
[Saint-patrick]: https://fr.wikipedia.org/wiki/F%C3%AAte_de_la_Saint-Patrick
[Sergei Rachmaninoff]: https://en.wikipedia.org/wiki/Sergei_Rachmaninoff
[Solstice]: https://fr.wikipedia.org/wiki/Solstice
[St-Jean-Baptiste]: https://fr.wikipedia.org/wiki/F%C3%AAte_nationale_du_Qu%C3%A9bec
[Thanksgiving]: https://en.wikipedia.org/wiki/Thanksgiving_(United_States)
[Towel Day]: https://en.wikipedia.org/wiki/Towel_Day
[Vendredi saint]: https://fr.wikipedia.org/wiki/Vendredi_saint
[Veterans day]: https://en.wikipedia.org/wiki/Veterans_Day
[Victoria day]: https://en.wikipedia.org/wiki/Victoria_Day
[Washington's birthday]: https://en.wikipedia.org/wiki/Washington%27s_Birthday
[Wold Humanist day]: https://en.wikipedia.org/wiki/World_Humanist_Day
[Yom Kippur]: https://en.wikipedia.org/wiki/Yom_Kippur
[Yule]: https://en.wikipedia.org/wiki/Yule
[Équinoxe]: https://fr.wikipedia.org/wiki/%C3%89quinoxe

## Évènements astronomiques

Une sélection des [évènements de Seasky.org](http://www.seasky.org/astronomy/astronomy-calendar-2020.html) a été faite. En plus
des équinoxes et solstices déjà inclus, on indique les oppositions,
conjonctions et élongations des planètes majeures généralement
visibles dans le ciel à l'oeil nu, soit Vénus, Mars, Jupiter et
Saturne. Voir également le [[calendrier-2019]] pour une meilleure
explication de ces évènements.

# Questions restantes

 * couverture: ajouter le titre (done)
 * couverture: considérer rails (DSCF0666) ou hibou (8546) (done, rails)
 * colophon: changer photo (fait un auto-portrait)
 * janvier: considérer le JPEG? ya des fans du projo (8178) et j'ai
   travaillé fort sur le vélo (8106) (done - le JPG est trop "smudgé"
   et j'aime beaucoup cette photo autrement, on va vivre avec le grain
   de DT)
 * février: OK, mais considérer buanderie (8344) vélo (8486) (done, on
   reste, le projet de buanderie reste à finir)
 * mars à juillet: OK
 * août: un peu banal? considérer la maison (1409)? (done, nope, happy)
 * septembre: beaucoup de choix! feuilles (1871) ou cloture (1881)?
   (done, happy)
 * colophon: ajouter du texte (done)
 * faire une première épreuve

# Impression

Pas de réponse de Mardigrafe. Quadriscan sont en face, on s'essaie.

Prévu: 30 copies. 400-420$, soit 13-14$ par copie.

Le projet est nommé "Projet Calendes 2.0", vu que c'est la deuxième
fois que je fais le projet. Des impressions futures du projet serait
numérotées 2.1, 2.2, ..., 2.10, etc. L'an prochain serait 3.0, s'il y
a lieu.

## 2.0: première impression

La première impression a été commandée pour 30 copies, mais 35 copies
ont été fournies par l'imprimeur, [Quadriscan](https://quadriscan.com/). La spécification
sur la facture:

 * Calendrier 13 feuilles relié *wire-o*
 * 10.4375 x 7,875 po
 * Indigo numérique couleur recto/verso avec marges perdues
 * Couché gloss couverture 12 Pts Blanc 222M
 * Épreuve PDF
 * Épreuve numérique couleur (Final)
 * coupé au format final, trouage 1 trou de 1/4", reliure wire-o
   couleur noir
 * En boîtes
 * 405$ + tx = 465.65$, soit 13.30$/copie tx inc. (à 35 copies) ou
   15.52$/copie tx inc. (à 30 copies)

# Errata

À noter la taille particulière: le représentant a remarqué que le
patron que j'avais amené (de l'an passé) n'était pas 11x8.5" comme
j'avais demandé, mais plus petit. Je ne sais pas trop quoi penser de
cette différence, mais ça m'importe peu. Dans tous les cas, peut-être
que ça devra être évalué avant une nouvelle impression.

Le colophon devrait indiquer quelle fonte est utilisée, car c'est un
des principaux détails inclus dans un colophon habituellement.

Le colophon indiquait la note suivante pour la couverture:

> Couverture f/2.2 1/3200s ISO 200 35mm Impasse piétonnière. Rue Beaubien, Montréal.

Cela devrait plutôt être:

> Couverture f/8 1/250 ISO 200 55mm Chemin de fer. Stockholm.
