Bell is named after [Bell Hooks](https://en.wikipedia.org/wiki/Bell_hooks). It's an old Panasonic Toughbook
CF-W5. The [Panasonic landing page](https://na.panasonic.com/us/computers-tablets-handhelds/computers/laptops/toughbook-cf-w5) is utterly useless other than
to say it's "discontinued". [According to small-laptops.com](https://www.small-laptops.com/panasonic-toughbook-w5/), it's
"being discontinued" since 2008, with an original tag price of 2269$
(!). It was reviewed positively by [Wired](https://www.wired.com/2007/01/review-panasoni/) and [ZDNet](https://www.zdnet.com/product/panasonic-toughbook-cf-w5/) in 2007 (!!).

The [Panasonic wiki has full specs](https://panasonic.fandom.com/wiki/CF-W5#Other_Infomation), which include:

 * CPU: Intel Core Duo 1.06GHz
 * RAM: 512MB, upgraded to 2GB
 * Storage: 150GB [Hitachi HTS54161 HDD](https://www.hdsentinel.com/storageinfo_details.php?lang=en&model=HITACHI%20HTS541616J9SA00) which, amazingly, still
   works
 * Display: 12.1" 1024x768 TFT LCD
 * PC card, SD card reader
 * weird 83-keys keyboards with japanese keys
 * D-Sub 15-pin video output
 * 56k Modem (!!) and 10/100 ethernet
 * 2.9lbs

This is one of my many spare laptops that I have lying around in a
drawer. It's running Debian "jessie" 8 with *some* "stretch"
`sources.list`... When I booted it in November 2022, the last login
was from 2017, running the Linux kernel 3.16.0. Incredibly, this OS is
*still* supported by [Debian LTS](https://wiki.debian.org/LTS), until 2025 for a [limited set of
packages](https://www.freexian.com/lts/extended/docs/debian-8-support/).

[[!tag node]]
