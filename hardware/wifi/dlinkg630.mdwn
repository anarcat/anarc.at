# The situation

Here we go, again. I got a [D-Link G630](http://www.dlink.com/products/?pid=317) v.B1 card right here. The [Madwifi](http://madwifi.org/) project support for it is ["unknown"](http://madwifi.org/wiki/Compatibility#DWLG630) (which I should probably change by now). The firmware version on the card is 1.2.0.30. PCI Id: 03:00.0 0280: 104c:9066. lspci -v:

[[!format text """
03:00.0 Network controller: Texas Instruments ACX 111 54Mbps Wireless Interface
        Subsystem: D-Link System Inc Unknown device 3b08
        Flags: medium devsel, IRQ 177
        Memory at 44020000 (32-bit, non-prefetchable) [size=8K]
        Memory at 44000000 (32-bit, non-prefetchable) [size=128K]
        Capabilities: [40] Power Management version 2
"""]]

In 2004, someone was already [saying](http://lists.shmoo.com/pipermail/hostap/2004-October/008278.html) that Texas Instrubments didn't want to share specs so the situation was described as "grim", for a similar card, the DWL-650+.

# The attempts

Before installing extra modules, here is what I get in the dmesg:

I get this in dmesg:

[[!format txt """
pccard: CardBus card inserted into slot 0
yenta EnE: chaning testregister 0xC9, 04 -> 04
"""]]

## Madwifi

I tried first the madwifi project:

[[!format txt """
m-a prepare
m-a get madwifi
m-a a-i madwifi
"""]]

Zlit. Nothing happens when I insert the card.

## ACX100

The [ACX100](http://acx100.sourceforge.net/) project is not even [mentionning](http://acx100.sourceforge.net/matrix.html) support for the G630, so that is even grimmer. I nevertheless tried it out:

[[!format txt """
m-a prepare
m-a get acx100
m-a a-i acx100
"""]]

[[!format txt """
pccard: CardBus card inserted into slot 0
yenta EnE: chaning testregister 0xC9, 04 -> 04
PCI: Enabling device 0000:03:00.0 (0000 -> 0002)
ACPI: PCI Interrupt 0000:03:00.0[A] -> GSI 16 (level, low) -> IRQ 177
PCI: Setting latency timer of device 0000:03:00.0 to 64
acx: found ACX111-based wireless network card at 0000:03:00.0, irq:177, phymem1:0x44020000, phymem2:0x44000000, mem1:0xdebd4000, mem1_size:8192, mem2:0xded80000, mem2_size:131072
initial debug setting is 0x000A
using IRQ 177
requesting firmware image 'tiacx111c16'
acx: firmware image 'tiacx111c16' was not provided. Check your hotplug scripts
requesting firmware image 'tiacx111'
acx: firmware image 'tiacx111' was not provided. Check your hotplug scripts
acx: reset_dev() FAILED
ACPI: PCI interrupt for device 0000:03:00.0 disabled
acx_pci: probe of 0000:03:00.0 failed with error -5
"""]]

Not bad! Only [firmware](http://acx100.sourceforge.net/wiki/Firmware) missing? The wiki indicates I need [this one](http://acx100.erley.org/acx_fw/acx111_xterasys_xn2522g/fw2/).

## The windows driver

The [[driver|ftp://ftp.dlink.com/Wireless/dwlg630_revB/Drivers/dwlg630_driver_200.zip]] could help me get the firmware. I found it through the [crappy Dlink support site](http://support.dlink.com/products/view.asp?productid=DWL%2DG630%5FrevB#).

I unzipped the driver, copied the Win2k/*.bin image files to /lib/firmware and tried again, same error.

I tried renaming/linking the filenames so that the driver would find them:

[[!format txt """
cd /lib/firmware
ln FW1130.bin tiacx111
ln FwRad16.bin tiacx111r16
"""]]

And the driver did find them:

[[!format txt """
Sep 12 09:59:12 orange kernel: requesting firmware image 'tiacx111r16'
Sep 12 09:59:12 orange kernel: acx_write_fw (radio): 0
Sep 12 09:59:12 orange kernel: acx: FATAL: firmware upload: data parts at offset 58376 don't match (0x2C0056A4 vs. 0x00000000)! I/O timing issues or defective memory, with DWL-xx0+? ACX_IO_WIDTH=16 may help. Please report
Sep 12 09:59:12 orange kernel: acx_validate_fw (radio): 1
Sep 12 09:59:12 orange kernel: acx: radio firmware upload attempt #1 FAILED, retrying...
Sep 12 09:59:13 orange kernel: acx_write_fw (radio): 0
... 5 attempts
Sep 12 09:59:17 orange kernel: acx: issue_cmd(): timed out polling for CMD_COMPLETE. irq bits:0x0000 irq_status:0x2000 timeout:49ms cmd_status:0 (Idle)
Sep 12 09:59:17 orange kernel: acx: issue_cmd(cmd:ACX1xx_CMD_WAKE) FAILED
Sep 12 09:59:17 orange kernel:  [<ded0ee5a>] acxpci_s_issue_cmd_timeo_debug+0x443/0x46f [acx]
Sep 12 09:59:17 orange kernel:  [<ded0effd>] acxpci_s_upload_radio+0x177/0x21b [acx]
Sep 12 09:59:17 orange kernel:  [<ded06fd9>] acx_s_init_mac+0x70/0x711 [acx]
Sep 12 09:59:17 orange kernel:  [<b0119ab3>] __do_softirq+0x34/0x7d
Sep 12 09:59:17 orange kernel:  [<b0104a20>] do_IRQ+0x22/0x2a
Sep 12 09:59:17 orange kernel:  [<b010335a>] common_interrupt+0x1a/0x20
Sep 12 09:59:17 orange kernel:  [<b011007b>] relocate_new_kernel+0x7b/0xa0
Sep 12 09:59:17 orange kernel:  [<ded0cd7a>] acxpci_read_eeprom_byte+0x6f/0xc1 [acx]
Sep 12 09:59:17 orange kernel:  [<ded0d7bf>] acxpci_s_reset_dev+0x33a/0x390 [acx]
Sep 12 09:59:17 orange kernel:  [<ded0f991>] acxpci_e_probe+0x3f8/0x60e [acx]
Sep 12 09:59:17 orange kernel:  [<b0112d1b>] __wake_up_common+0x29/0x49
Sep 12 09:59:17 orange kernel:  [<b01a1fa3>] vsnprintf+0x423/0x461
Sep 12 09:59:17 orange kernel:  [<b0159d60>] alloc_inode+0x27/0x17d
Sep 12 09:59:17 orange kernel:  [<b0159e2e>] alloc_inode+0xf5/0x17d
Sep 12 09:59:17 orange kernel:  [<b0159ecc>] new_inode+0x16/0x6f
Sep 12 09:59:17 orange kernel:  [<b01762d2>] sysfs_new_inode+0xaf/0xd5
Sep 12 09:59:17 orange kernel:  [<b0176c16>] sysfs_new_dirent+0x18/0x59
Sep 12 09:59:17 orange kernel:  [<b01a969c>] pci_match_device+0x14/0x8c
Sep 12 09:59:17 orange kernel:  [<b01a974c>] pci_device_probe+0x38/0x59
Sep 12 09:59:17 orange kernel:  [<b01f94f6>] driver_probe_device+0x42/0x8c
Sep 12 09:59:17 orange kernel:  [<b01f9540>] __device_attach+0x0/0x5
Sep 12 09:59:17 orange kernel:  [<b01f8d42>] bus_for_each_drv+0x45/0x71
Sep 12 09:59:17 orange kernel:  [<b01f9586>] device_attach+0x41/0x52
Sep 12 09:59:17 orange kernel:  [<b01f9540>] __device_attach+0x0/0x5
Sep 12 09:59:17 orange kernel:  [<b01f8d95>] bus_add_device+0x27/0xcb
Sep 12 09:59:17 orange kernel:  [<b01f82a6>] device_add+0xc6/0x123
Sep 12 09:59:17 orange kernel:  [<b01a6910>] pci_bus_add_device+0xe/0x36
Sep 12 09:59:17 orange kernel:  [<b01a694e>] pci_bus_add_devices+0x16/0x9c
Sep 12 09:59:17 orange kernel:  [<de8d69f7>] cb_alloc+0x96/0xa9 [pcmcia_core]
Sep 12 09:59:17 orange kernel:  [<de8d3a6a>] socket_insert+0x9e/0xc2 [pcmcia_core]
Sep 12 09:59:17 orange kernel:  [<de8d3c82>] pccardd+0x143/0x1d8 [pcmcia_core]
Sep 12 09:59:17 orange kernel:  [<b01135cd>] default_wake_function+0x0/0x15
Sep 12 09:59:17 orange kernel:  [<de8d3b3f>] pccardd+0x0/0x1d8 [pcmcia_core]
Sep 12 09:59:17 orange kernel:  [<b0101005>] kernel_thread_helper+0x5/0xb
Sep 12 09:59:17 orange kernel: initializing max packet templates
Sep 12 09:59:17 orange kernel: acx: issue_cmd(): timed out polling for CMD_COMPLETE. irq bits:0x0000 irq_status:0x2000 timeout:49ms cmd_status:0 (Idle)
Sep 12 09:59:17 orange kernel: acx: issue_cmd(cmd:cmd) FAILED
Sep 12 09:59:17 orange kernel:  [<ded0ee5a>] acxpci_s_issue_cmd_timeo_debug+0x443/0x46f [acx]
Sep 12 09:59:17 orange kernel:  [<ded06d2d>] acx_s_init_max_template_generic+0x4b/0x68 [acx]
Sep 12 09:59:17 orange kernel:  [<b0101005>] kernel_thread_helper+0x5/0xb
Sep 12 09:59:17 orange kernel:  [<b0115daf>] __call_console_drivers+0x33/0x41
Sep 12 09:59:17 orange kernel:  [<b0116815>] vprintk+0x21a/0x235
Sep 12 09:59:17 orange kernel:  [<ded0ee5a>] acxpci_s_issue_cmd_timeo_debug+0x443/0x46f [acx]
Sep 12 09:59:17 orange kernel:  [<b013e625>] __vunmap+0xab/0xbc
Sep 12 09:59:17 orange kernel:  [<b013e6cb>] vfree+0x23/0x26
Sep 12 09:59:17 orange kernel:  [<ded0f029>] acxpci_s_upload_radio+0x1a3/0x21b [acx]
Sep 12 09:59:17 orange kernel:  [<b0116842>] printk+0x12/0x16
Sep 12 09:59:17 orange kernel:  [<ded06e28>] acx_s_init_packet_templates+0x43/0x184 [acx]
Sep 12 09:59:17 orange kernel:  [<ded06ff9>] acx_s_init_mac+0x90/0x711 [acx]
Sep 12 09:59:17 orange kernel:  [<b0119ab3>] __do_softirq+0x34/0x7d
Sep 12 09:59:17 orange kernel:  [<b0104a20>] do_IRQ+0x22/0x2a
Sep 12 09:59:17 orange kernel:  [<b010335a>] common_interrupt+0x1a/0x20
Sep 12 09:59:17 orange kernel:  [<b011007b>] relocate_new_kernel+0x7b/0xa0
Sep 12 09:59:17 orange kernel:  [<ded0cd7a>] acxpci_read_eeprom_byte+0x6f/0xc1 [acx]
Sep 12 09:59:17 orange kernel:  [<ded0d7bf>] acxpci_s_reset_dev+0x33a/0x390 [acx]
Sep 12 09:59:17 orange kernel:  [<ded0f991>] acxpci_e_probe+0x3f8/0x60e [acx]
Sep 12 09:59:17 orange kernel:  [<b0112d1b>] __wake_up_common+0x29/0x49
Sep 12 09:59:17 orange kernel:  [<b01a1fa3>] vsnprintf+0x423/0x461
Sep 12 09:59:17 orange kernel:  [<b0159d60>] alloc_inode+0x27/0x17d
Sep 12 09:59:17 orange kernel:  [<b0159e2e>] alloc_inode+0xf5/0x17d
Sep 12 09:59:17 orange kernel:  [<b0159ecc>] new_inode+0x16/0x6f
Sep 12 09:59:17 orange kernel:  [<b01762d2>] sysfs_new_inode+0xaf/0xd5
Sep 12 09:59:17 orange kernel:  [<b0176c16>] sysfs_new_dirent+0x18/0x59
Sep 12 09:59:17 orange kernel:  [<b01a969c>] pci_match_device+0x14/0x8c
Sep 12 09:59:17 orange kernel:  [<b01a974c>] pci_device_probe+0x38/0x59
Sep 12 09:59:17 orange kernel:  [<b01f94f6>] driver_probe_device+0x42/0x8c
Sep 12 09:59:17 orange kernel:  [<b01f9540>] __device_attach+0x0/0x5
Sep 12 09:59:17 orange kernel:  [<b01f8d42>] bus_for_each_drv+0x45/0x71
Sep 12 09:59:17 orange kernel:  [<b01f9586>] device_attach+0x41/0x52
Sep 12 09:59:17 orange kernel:  [<b01f9540>] __device_attach+0x0/0x5
Sep 12 09:59:17 orange kernel:  [<b01f8d95>] bus_add_device+0x27/0xcb
Sep 12 09:59:17 orange kernel:  [<b01f82a6>] device_add+0xc6/0x123
Sep 12 09:59:17 orange kernel:  [<b01a6910>] pci_bus_add_device+0xe/0x36
Sep 12 09:59:17 orange kernel:  [<b01a694e>] pci_bus_add_devices+0x16/0x9c
Sep 12 09:59:17 orange kernel:  [<de8d69f7>] cb_alloc+0x96/0xa9 [pcmcia_core]
Sep 12 09:59:17 orange kernel:  [<de8d3a6a>] socket_insert+0x9e/0xc2 [pcmcia_core]
Sep 12 09:59:17 orange kernel:  [<de8d3c82>] pccardd+0x143/0x1d8 [pcmcia_core]
Sep 12 09:59:17 orange kernel:  [<b01135cd>] default_wake_function+0x0/0x15
Sep 12 09:59:17 orange kernel:  [<de8d3b3f>] pccardd+0x0/0x1d8 [pcmcia_core]
Sep 12 09:59:17 orange kernel:  [<b0101005>] kernel_thread_helper+0x5/0xb
Sep 12 09:59:17 orange kernel: wlan%%d: acx_s_init_packet_templates() FAILED
Sep 12 09:59:17 orange kernel: acx: init_mac() FAILED
Sep 12 09:59:17 orange kernel: ACPI: PCI interrupt for device 0000:03:00.0 disabled
Sep 12 09:59:17 orange kernel: acx_pci: probe of 0000:03:00.0 failed with error -5
"""]]

but crashed. That's because I chose the wrong firmware:

[[!format txt """
rm tiacx111r16
ln radio16.bin tiacx111r16.bin
"""]]

Now it works:

[[!format txt """
iwconfig wlan0 essid wificitoyen
ifconfig wlan0 up
dhclient wlan0
"""]]

gives me:

[[!format txt """
pccard: CardBus card inserted into slot 0
yenta EnE: chaning testregister 0xC9, 04 -> 04
PCI: Enabling device 0000:03:00.0 (0000 -> 0002)
ACPI: PCI Interrupt 0000:03:00.0[A] -> GSI 16 (level, low) -> IRQ 177
PCI: Setting latency timer of device 0000:03:00.0 to 64
acx: found ACX111-based wireless network card at 0000:03:00.0, irq:177, phymem1:0x44020000, phymem2:0x44000000, mem1:0xdebd4000, mem1_size:8192, mem2:0xded80000, mem2_size:131072
initial debug setting is 0x000A
using IRQ 177
requesting firmware image 'tiacx111c16'
acx: firmware image 'tiacx111c16' was not provided. Check your hotplug scripts
requesting firmware image 'tiacx111'
acx_write_fw (main/combined):0
acx_validate_fw (main/combined):0
requesting firmware image 'tiacx111r16'
acx_write_fw (radio): 0
acx_validate_fw (radio): 0
initializing max packet templates
dump queue head (from card):
len: 24
tx_memory_block_address: 190E0
rx_memory_block_address: 135E0
tx1_queue address: 12B14
rx1_queue address: 127D4
NVS_vendor_offs:01CD probe_delay:200 eof_memory:1114112
CCAModes:04 Diversity:01 ShortPreOpt:01 PBCC:01 ChanAgil:00 PHY:05 Temp:01
AntennaID:00 Len:02 Data:01 02
PowerLevelID:01 Len:02 Data:001E 000A
DataRatesID:02 Len:05 Data:02 04 11 22 44
DomainID:03 Len:06 Data:10 20 30 31 32 41
ProductID:04 Len:09 Data:TI ACX100
ManufacturerID:05 Len:07 Data:TI Test
get_mask 0x00004182, set_mask 0x00000000
don't know how to get sensitivity for radio type 0x16
got sensitivity value 0
got antenna value 0x4A
got regulatory domain 0x10
get_mask 0x00000000, set_mask 0x00000000 - after update
new ratevector: 82 84 0B 0C 12 16 18 24 2C 30 48 60 6C
setting RXconfig to 2010:0FDD
acx: form factor 0x01 ((mini-)PCI / CardBus), radio type 0x16 (Radia), EEPROM version 0x05, uploaded firmware 'Rev 1.2.0.30' (0x03010101)
creating /proc entry driver/acx_wlan0
creating /proc entry driver/acx_wlan0_diag
creating /proc entry driver/acx_wlan0_eeprom
creating /proc entry driver/acx_wlan0_phy
acx v0.3.35: net device wlan0, driver compiled against wireless extensions 19 and Linux 2.6.16-2-686
acx_set_status(1):SCANNING
updating initial settings on iface activation
get_mask 0x00000000, set_mask 0x0037EEFC
important setting has been changed. Need to update packet templates, too
updating packet templates
updating Tx fallback to 1 retries
updating transmit power: 15 dBm
updating antenna value: 0x4A
updating Energy Detect (ED) threshold: 0
acx111 doesn't support ED!
updating Channel Clear Assessment (CCA) value: 0x00
acx111 doesn't support CCA!
updating channel to: 1
updating: enable Tx
updating: enable Rx on channel: 1
updating short retry limit: 7, long retry limit: 4
updating tx MSDU lifetime: 4096
updating regulatory domain: 0x10
setting RXconfig to 2010:0FDD
updating WEP key settings
setting WEP key 0 as default
acx_set_status(1):SCANNING
starting radio scan
get_mask 0x00000000, set_mask 0x00000000 - after update
ADDRCONF(NETDEV_UP): wlan0: link is not ready
get_mask 0x00000000, set_mask 0x00000040
setting RXconfig to 2010:0FDD
get_mask 0x00000000, set_mask 0x00000000 - after update
acx_i_timer: adev->status=1 (SCANNING)
continuing scan (1 sec)
acx: unknown EID 42 in mgmt frame at offset 62. IE: 2A 01 04
acx: unknown EID 47 in mgmt frame at offset 65. IE: 2F 01 04
acx: unknown EID 221 in mgmt frame at offset 74. IE: DD 06 00 10 18 02 00 04
sta_list_add: sta=00:13:10:F1:56:F5
acx: unknown EID 42 in mgmt frame at offset 62. IE: 2A 01 04
acx: unknown EID 47 in mgmt frame at offset 65. IE: 2F 01 04
acx: unknown EID 221 in mgmt frame at offset 74. IE: DD 06 00 10 18 02 00 04
acx: unknown EID 42 in mgmt frame at offset 62. IE: 2A 01 04
acx: unknown EID 47 in mgmt frame at offset 65. IE: 2F 01 04
acx: unknown EID 221 in mgmt frame at offset 74. IE: DD 06 00 10 18 02 00 04
acx_i_timer: adev->status=1 (SCANNING)
continuing scan (2 sec)
sta_list_add: sta=00:11:95:78:A6:C6
scan table: SSID='wificitoyen' CH=6 SIR=25 SNR=0
peer_cap 0x0401, needed_cap 0x0001
found station with matching ESSID! ('wificitoyen' station, 'wificitoyen' config)
scan table: SSID='olivier' CH=11 SIR=8 SNR=0
peer_cap 0x0431, needed_cap 0x0001
ESSID doesn't match! ('olivier' station, 'wificitoyen' config)
matching station found: 00:13:10:F1:56:F5, joining
sending authentication1 request, awaiting response
acx_set_status(2):WAIT_AUTH
rates_basic:0003, rates_supported:1FFF
BSS_Type = 2
JoinBSSID MAC:00:13:10:F1:56:F5
acx_i_timer: adev->status=2 (WAIT_AUTH)
resend authen1 request (attempt 2)
sending authentication1 request, awaiting response
AUTHEN adev->addr=00:0F:3D:66:91:99 a1=00:0F:3D:66:91:99 a2=00:13:10:F1:56:F5 a3=00:13:10:F1:56:F5 adev->bssid=00:13:10:F1:56:F5
algorithm is ok
acx_process_authen auth seq step 2
acx_set_status(3):AUTHENTICATED
sending association request, awaiting response. NOT ASSOCIATED YET
association: requesting caps 0x0001, ESSID 'wificitoyen'
acx_set_status(4):ASSOCIATED
ASSOCIATED!
ADDRCONF(NETDEV_CHANGE): wlan0: link becomes ready
get_mask 0x00000000, set_mask 0x00000040
setting RXconfig to 2010:0FDD
wlan0: tx error 0x20, buf 00! (excessive Tx retries due to either distance too high or unable to Tx or Tx frame error - try changing 'iwconfig txpower XXX' or 'sens'itivity or 'retry')
get_mask 0x00000000, set_mask 0x00000000 - after update
wlan0: duplicate address detected!
acx_i_timer: adev->status=4 (ASSOCIATED)
sta_list_add: sta=00:0C:41:3E:41:1A
get_mask 0x00000000, set_mask 0x00000040
setting RXconfig to 2010:0FDD
get_mask 0x00000000, set_mask 0x00000000 - after update
get_mask 0x00000000, set_mask 0x00000040
setting RXconfig to 2010:0FDD
get_mask 0x00000000, set_mask 0x00000000 - after update
"""]]

I can ping the gateway, wheepee!!!
