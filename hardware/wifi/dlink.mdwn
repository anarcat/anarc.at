So i've got a [DLink DWL-650](http://www.dlink.com/products/wireless/dwl650/) wireless card. It's a 'b' card that seems to be working in linux. The biggest problem with it is that the model name doesn't tell anything about the actual chipset included in the device, as is often the case with wifi cards. Therefore, [Dlink](http://dlink.com/) has [Guide to identify the real model](http://www.dlink.com/products/support.asp?pid=9&sec=0#drivers). I have a DWL-650M1, because of the little "V.M1" tag at the end of the bar code on the back.

It seems this model uses some the !RealTek 8180 chipset, for which !RealTek provides a [[driver|ftp://210.51.181.211/cn/wlan/rtl8180l/rtl8180_linuxdrv_v15_rh90.zip]], with source, nothing less. However, this means I must recompile it on my side and therefore download [the source of my kernel](http://kernel.org/pub/linux/kernel/v2.4/linux-2.4.27.tar.bz2).

Seems like my [collection of wifi resources](http://del.icio.us/anarcat/wireless) will at last be useful!

Update: problems with the RealTek driver. doesn't compile. Other solutions:

 * [a sourceforge project](http://sourceforge.net/projects/rtl8180-sa2400/): this doesn't seem to support my card
 * [NDIS wrappers](http://ndiswrapper.sourceforge.net/): last chance!

Update: the [open source project](http://rtl8180-sa2400.sourceforge.net/) does seem to support my card after all! Indeed, in [Ubunutu Dapper](http://www.ubuntu.com/testing), the card is detected and configured automagically.  

Update: le driver marche!!! il me faut installer checkinstall pour l'installer, qui ne semble pas encore [entre dans testing](http://bjorn.haxx.se/debian/testing.pl?package=checkinstall)... -- 69.157.250.175 2006-04-09T16:37:21Z

Update: le driver a des bugs, surtout avec le wep, mais ça fait un bout que je les ai pas revu. J'ai eu de la misère à me brancher sur un WRT54G qui avait du WEP, mais c'était parce que la clé sélectionnée dans sa config était la 3 au lieu de la 1 (?!). -- TheAnarcat 2006-05-10T01:37:07-0400

References
==========

 * [howto en francah](http://eric.lamarque.free.fr/?linux/tew226pc-mdk92.html)
 * [free driver](http://rtl8180-sa2400.sourceforge.net/)
 * [wifi chipset/compatibility listing](http://www.linux-wlan.org/docs/wlan_adapters.html.gz) (huge table)
