[[!toc levels=2]]

evelyn is named [Evelyn Berezin][], inventor of the computer word
processor, who developed the first computerized banking system.

[Evelyn Berezin]: https://en.wikipedia.org/wiki/Evelyn_Berezin

It's my third cloud VM, replacing [[fumiko]], designed to handle
outages or overloads on [[hardware/server/marcos]]..

I picked Digital Ocean because it's cheap. I picked the NYC PoP
because it was cheaper than the Toronto one.

I hope to eventually shut down this shit.

I set it up with a plain Debian 11 bullseye install from DO, then went
through the [[services/upgrades/bullseye]] upgrade, after a very basic
bootstrap:

    apt install foot-terminfo tmux

Then went with the Puppet bootstrap:

    tmux
    apt install puppet
    puppet agent  --test --server puppet.anarc.at

then on the master:

    puppetserver ca list | grep $HASH_FROM_ABOVE
    puppetserver ca sign fumiko.anarc.at

then on the client again:

    puppet agent --test

[[!tag node]]
