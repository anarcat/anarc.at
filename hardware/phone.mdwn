This section documents my experiments with cellular phone
technology. I have more detailed guides and documentation on specific
phones as well:

[[!map pages="page(hardware/phone/*) or page(blog/2021-01-13-new-phone)"]]

**Table of contents**:

[[!toc levels=2]]

# Current phone

2024: I have given up and switched to Pixels, see
[[blog/2021-01-13-new-phone]] for details. I have used a Pixel 4a
which had a flaky screen after a year and switched to a 6a. I liked
the 4a but the 6a is too large. I used the "Google" case for the phone
at first but switched to [Spigen Liquid Air](https://www.spigen.com/collections/pixel-6-series-case-collection/products/pixel-6-series-case-liquid-air?variant=41718301524015) case that seems to
work well so far.

# Previous phones

## HTC One S

See [[htc-one-s]] for config details. [Specs](http://www.gsmarena.com/htc_one_s-4574.php):

 * Dual-core 1.5 GHz Krait
 * 16 GB, 1 GB RAM
 * 8 MP, autofocus, LED flash, check quality
 * FM radio, RDS
 * Wi-Fi 802.11 b/g/n, DLNA, hotspot
 * A-GPS
 * Non-removable battery, Up to 317 h, talk up to 10 h 30 min
 * 130.9 x 65 x 7.8 mm (5.15 x 2.56 x 0.31 in)
 * 119.5 g (4.20 oz)
 * 4.3" (~59.9% screen-to-body ratio)

## HTC Dream

The [[!wikipedia HTC Dream]] was the first commercial Android phone. It still works, although it is a little old and buggy here.

### Android / Cyanogenmod support

One of the issues with the device is that it doesn't (or can't!) run more recent Android releases, which basically means no software support. It runs Android 2.2 / CM 6.1!

 * [porting 2.3/CM 7 to it](http://forum.cyanogenmod.com/topic/13579-gingerbread-on-the-magicdream/)
 * [android 4.1 / CM 10 ported??](http://www.theverge.com/2012/8/9/3229163/android-4-1-ported-to-the-venerable-htc-g1)
 * [CM homepage for dream](http://wiki.cyanogenmod.org/w/Dream_sapphire_Info)

### podcasting

One of the thing that's missing is podcasting, various ideas:

 * [volksempfaenger](https://play.google.com/store/apps/details?id=net.x4a42.volksempfaenger) (android 4.0+?, [not on fdroid yet](http://f-droid.org/forums/topic/volksempfanger-podcast-app/)
 * Antennapod, android 2.3.3+ [fdroid](http://f-droid.org/repository/browse/?fdcategory=Multimedia&fdid=de.danoeh.antennapod&fdpage=1)
 * <http://www.doggcatcher.com/>

## Nokia n900

The [[!wikipedia Nokia_N900]] was a great machine, but those machines
are now so dead: no more software support from Nokia... and the
hardware is somewhat slow. There's [Neo900](http://neo900.org/), a
plan to rebuild a new phone based on the same case, but that's not yet
shipping.

I have two n900 machines, both have their SIM card socket broken now,
either desoldered or some other broken thing. [Wikipedia says this can
be fixed by resoldering][], and there are two references online:

* <http://talk.maemo.org/showthread.php?p=1154781#post1154781>
* <https://www.jabawok.net/?p=14>

 [Wikipedia says this can be fixed by resoldering]: https://en.wikipedia.org/wiki/Nokia_N900#Known_issues

 * 600MHz Cortex A8
 * 32GB, 256MB ram
 * 5MP
 * FM radio and transceiver(!)
 * Wi-Fi 802.11 b/g, DLNA
 * A-GPS
 * Removeable battery
 * 3.5" (800 x 480 pixels)
 * 110.9 x 59.8 x 18 mm, 181g

## Partial inventory

 * HTC Dream: works?
 * LG GB255g: old flip phone, good condition
 * Kyocera M2000: dead battery, slide keyboard, public mobile
 * 2x Nokia n900: broken sim card readers?
 * LG P999DW: old android, broken screen, still works!
 * HTC One S: broken wifi (drivers?), no more lineage OS support,
   rooted
 * LG G3 d852: not rooted, on "stolen or lost" list so unusable as a
   phone

# Features

Those features are nice to have. Unfortunately, they are now showing
their age and might not be relevant anymore.

## FM support

FM support in newer smartphones in spotty at best. According to [pdadb.net](http://pdadb.net/index.php?m=pdachooser), only 35 phones (out of 4111) have FM support. Amongst those, only 4 run android.

## External keyboard

Less rare in newer phones, real keyboards are still hard to find. Out of the 4111 android phones in the padb.net inventory, only 229 have actual keyboards, and often those are only regular phone keyboards, not actual QWERTY keyboards.

## Liberated baseband

The "[[!wikipedia Baseband processor]]" in a phone is a second processor in the phone that handles phone calls. Very often, and in fact in almost all cases, this is proprietary hardware and software that is hidden from the main processor, as a black box. So even if you manage to install free software (like cyanogenmod) on an Android device, you are still stuck with this [problematic backdoor](http://www.extremetech.com/computing/170874-the-secret-second-operating-system-that-could-make-every-mobile-phone-insecure).

Note that there is also software in the SIM card, which makes it three different operating systems running at once in your phone.

Some people are trying to fix this:

* [Osmocom](http://osmocom.org/) is a
  [collection of projects](http://openbsc.osmocom.org/trac/wiki/OsmocomOverview)
  that try to attack various communication projects, with
  [OpenBTS](http://cgit.osmocom.org/cgit/osmo-bts/) attacking GSM in
  particular
* [lima](http://limadriver.org/) and
  [freedreno](http://freedreno.github.io/) are attacking the graphics stack

.. but it's not in a phone yet. Ideally, a phone would just be another
general purpose computer, radio included, so that you'd have a simple
[SDR](https://en.wikipedia.org/wiki/Software-defined_radio) that you
would program GSM, FM, AM, CB, or whatever protocol acronym you would
fancy on top of that, all in software.

## Roaming and frequency support

What a nightmare... since [3G](https://en.wikipedia.org/wiki/3G) came
up, there's all sorts of very different frequencies for different
providers *and* for different
countries. [This map](http://www.worldtimezone.com/gsm.html) has a
good explanation of the world-wide coverage bands...

See also [the canada coverage map](http://maps.mobileworldlive.com/network.php?cid=170&cname=Canada)
to figure out exactly what protocols and what frequencies a provider
uses.

All numbers are in MHz unless otherwise noted.

### 2G

* Europe: 900, 1800
* Americas: 850, 1900 (except east of south-america)

### 3G

It gets complicated here. But in general:

* Europe: 900, 2100
* Americas: 850 ([Rogers][], [Bell][Bell 3G]), 1700 (Vidéotron), 1900
  ([Rogers][], [Bell][Bell 3G]). the three big networks seem to
  support HSDPA, HSPA+, UMTS or W-CDMA, and only Rogers not supporting
  EV-DO
  ([source](https://community.koodomobile.com/koodo/topics/the_big_three_canadian_network_frequencies))

See the
[source table](https://en.wikipedia.org/wiki/UMTS_frequency_bands#Deployments_by_region_.28UMTS-FDD.29)
for this.

 [Bell 3G]: http://support.bell.ca/Mobility/Smartphones_and_mobile_internet/Will_my_mobile_phone_or_smartphone_work_on_the_Bell_network

### 4G

Also known as `LTE`, `E-UTRA`, this is where it gets pretty messy.

* Asia: 800, 1800, 2600 (bands 1, 3, 5, 7, 8, 11, 13, 40)
* Europe: 700, 800, 900, 1800, 2600 (bands 3, 7, 20)
* Australia: 1800, 2300 (bands 3, 40)
* America: 700, 750, 800, 850, 1900, 1700/2100 (AWS/[Vidéotron][]), 2500, 2600
  ([Rogers][], [Bell][]) (bands 2, 4, 7, 12, 13, 17, 25,
  26, 41, 66)
* S. America: 2500

See also the [source][lte-frequency-bands] for the above and the [explicit deployment
chart](https://en.wikipedia.org/wiki/LTE_frequency_bands#Deployments_by_region). Basically, we need one (or many?) of those:

[lte-frequency-bands]: https://en.wikipedia.org/wiki/LTE_(telecommunication)#Frequency_bands

* base 4, 7 (1700/2100, 2600 MHz: [Bell][], [Rogers][]/[Fido][], others?)
* base 10 (700 MHz: [Vidéotron][])
* base 13 (700 MHz: [Bell][], [Vidéotron][], Telus)
* base 17 (700 MHz: [Bell][], [Rogers][]?/[Fido][])

See also [this post on koodoo](https://community.koodomobile.com/koodo/topics/the_big_three_canadian_network_frequencies) (dead link, no archive). [This
inventory of bands per provider in Canada/US](https://www.signalbooster.com/pages/what-are-the-cellular-frequencies-of-cell-phone-carriers-in-usa-canada) is useful as well.

 [Fido]: http://www.fido.ca/web/content/phonewarranty/configure_unlocked_device_guide&lang=fr
 [Rogers]: http://www.rogers.com/web/support/wireless/unlock/479?setLanguage=en
 [Vidéotron]: http://soutien.videotron.com/residentiel/mobile/appareils/limite-soutien-en-telephonie-mobile
 [Bell]: https://en.wikipedia.org/wiki/Bell_Mobility#LTE

# Places to buy

 * [Bestbuy](https://www.bestbuy.ca/en-ca/category/unlocked-android-phones/743360.aspx?)
 * [B&H](https://www.bhphotovideo.com/c/buy/smartphones/ci/24039/N/3955685938)
 * [Canada Computers](https://www.canadacomputers.com/index.php?cPath=571_30&sf=:)
 * [JP Mobiles](https://jpmobiles.ca/) (listed on Bestbuy, uses Amazon for "fulfillment")
 * [Newegg](https://www.newegg.ca/Cell-Phones/Category/ID-450?Tid=165898)
 * [Recy-cell](https://recy-cell.ca/) (used phones)
 * [Tiger Direct](http://www.tigerdirect.ca/applications/category/category_tlc.asp?CatId=5116) - not a great selection

# Potential phones

Must-have criteria:

 * removable SD card
 * good battery life
 * <= 5.5" screen (~LG G3)
 * supports LineageOS
 * 3.5mm jack
 * Wifi, GSM coverage in Québec
 * good GPS
 * no scratches
 * no contract with a GSM provider

Nice to have:

 * new phone, mint condition, easy returns
 * available somewhere else than Amazon
 * good [reparibility score](https://www.ifixit.com/smartphone-repairability?sort=score)
 * good camera
 * <= 5" screen
 * removable battery
 * "fair" sourced materials

## Picking a phone

Picking a phone is hard with all those restrictions. The `#lineageos`
folks are legendary for not helping you choose your phone but have
provided stellar advice like:

    19:41:04 <anarcat> haha great ... on https://itvends.com/irc Khaytsus's "random quote" is "This is LOS support. Not a place to find you a phone."
    19:41:23 <+Khaytsus> Yeah bitch.
    20:17:41 <@LuK1337> i wouldn't get a galaxy at all
    20:19:15 <[R]> yeah, the pocophone is great
    20:19:20 <[R]> super terrific chinese shitware great
    20:22:12 <+noahajac> anarcat: Just get a fucking Pixel
    20:37:09 <@LuK1337> xa2 is very hipster device

... and so on.

<https://stats.lineageos.org/> can be a source for the most popular
phones, but that doesn't say which phone is *still* supported. The
[download page](https://download.lineageos.org/) shows which devices are officially supported. The
latest release is 15.1, if it's not listed, it means support was
dropped in the latest release. The devices targeted for 16.0 right now
are [listed here](https://review.lineageos.org/c/LineageOS/hudson/+/214015/24/lineage-build-targets). Non-exhaustive list of the devices I could find
in the wiki:

 * [Moto X4](https://wiki.lineageos.org/devices/payton) ([review](https://forum.xda-developers.com/moto-x4/review))
 * [Moto Z2](https://wiki.lineageos.org/devices/nash) 
 * [Nexus 6](https://wiki.lineageos.org/devices/shamu)
 * [OnePlus 3 / 3T](https://wiki.lineageos.org/devices/oneplus3) ([review](https://forum.xda-developers.com/oneplus-3/review), [review](https://forum.xda-developers.com/oneplus-3t/review))
 * [OnePlus 5](https://wiki.lineageos.org/devices/cheeseburger) / [5T](https://wiki.lineageos.org/devices/dumpling) ([good iFixit score](https://www.ifixit.com/Teardown/OnePlus+5+Teardown/94173): 7/10)
 * [Pixel XL](https://wiki.lineageos.org/devices/marlin)
 * [Pixel sailfish](https://wiki.lineageos.org/devices/sailfish)
 * [Samsung Galaxy S9+](https://wiki.lineageos.org/devices/star2lteO)
 * [XA2](https://wiki.lineageos.org/devices/pioneer) ([review](https://forum.xda-developers.com/xperia-xa2/review)) / [XA2 ultra](https://wiki.lineageos.org/devices/discovery)
 * [Xiaomi Mi Note 3](https://wiki.lineageos.org/devices/jason)
 * [Zuk Z1](https://wiki.lineageos.org/devices/ham)

I ended up writing a small script to pull the currently supported
models from the [base de données](https://github.com/LineageOS/lineage_wiki/)

    #!/usr/bin/python3

    import sys

    from ruamel.yaml import YAML

    yaml = YAML()

    for path in sys.argv[1:]:
        with open(path) as stream:
            data = yaml.load(stream=stream)
            if data['current_branch'] == 15.1:
                print("{vendor:10.10s}| {name:20.20s}\t| {screen}\t | {release}".format(**data))

Si on regarde seulement les modèles sortis dans les derniers 3 ans, ça
nous donne ce joli tableau:

| Marque   | Modèle             | Taille             | Écran              |             Année |
| -------- | ------------------ | -----------------  | ------------------ | ----------------- |
| Asus     | Zenfone Max Pro M1 | 159 mm (6.26 in)   | 152.1 mm (5.99 in) |           2018-05 |
| BQ       | Aquaris X          | 146.5 mm (5.76 in) | 130 mm (5.2 in)    |           2017-06 |
| BQ       | Aquaris X Pro      | 146.5 mm (5.77 in) | 130 mm (5.2 in)    |           2017-06 |
| LeEco    | Le Pro3 / Élite    | 151.4 mm (5.96 in) | 5.5 in             | 2016-10 / 2017-03 |
| Motorola | Moto X4            | 148.4 mm (5.84 in) | 84.5 cm (5.2 in)   |           2017-10 |
| Motorola | Moto Z2 Force      | 155.8 mm (6.13 in) | 83.4 cm (5.5 in)   |           2017-07 |
| Motorola | Moto Z2 Play       | 156.2 mm (6.15 in) | 5.5 in             |           2017-06 |
| OnePlus  | 5                  | 154.2 mm (6.07 in) | 139.7 mm (5.5 in)  |           2017-06 |
| OnePlus  | 5T                 | 156.1 mm (6.15 in) | 152.7 mm (6.01 in) |           2017-11 |
| Samsung  | Galaxy S9          | 147.7 mm (5.81 in) | 5.8 inches         |        2018-03-11 |
| Samsung  | Galaxy S9+         | 158.1 mm (6.22 in) | 6.2 inches         |        2018-03-11 |
| Sony     | Xperia XA2         | 142 mm (5.59 in)   | 132 mm (5.2 in)    |           2018-02 |
| Sony     | Xperia XA2 Ultra   | 163 mm (6.42 in)   | 152.4 mm (6 in)    |           2018-02 |
| Xiaomi   | Mi 6               | 145.2 mm (5.72 in) | 130.8 mm (5.15 in) |           2017-04 |
| Xiaomi   | Mi A1              | 155.4 mm (6.12 in) | 139.7 mm (5.5 in)  |           2017-10 |
| Xiaomi   | Mi MIX 2           | 151.8 mm (5.98 in) | 152.1 mm (5.99 in) |           2017-09 |
| Xiaomi   | Mi Note 3          | 152.6 mm (6.01 in) | 139.7 mm (5.5 in)  |           2017-09 |
| Xiaomi   | Redmi 4(X)         | 139.2 mm (5.48 in) | 127 mm (5.0 in)    |           2017-05 |
| Xiaomi   | Redmi Note 4       | 151 mm (5.94 in)   | 139.7 mm (5.5 in)  |           2017-01 |
| Xiaomi   | Redmi Note 5 Pro   | 158.6 mm (6.24 in) | 152.1 mm (5.99 in) |           2018-02 |

De ceux là, je trouve le Moto X4 et Sony XA2 les plus intéressants,
principalement à cause de la taille. Le X4 est étanche mais a une
caméra moyenne, alors que le XA2 est difficile à réparer. La caméra du
X4 pourrait être un avantage, en fait, vu qu'elle est "seulement"
14Mpx, ça prend moins d'espace. Et les reviews de la caméra du XA2
sont mauvais sur XDA...

Ceci dit, j'ai découvert que le FP2 est possiblement en vente au
Canada (voir ci-bas) et j'ai fait une [demande](https://forum.fairphone.com/t/buying-a-fairphone-2-in-canada/48483) pour un usagé.

Update: j'ai acheté un Fairphone 2 chez Ecosto, pour ~500$CAD, voir
[[fairphone2]] pour les détails.

## Fairphone 2

Moved to [[fairphone2]].

## Fairphone 3

Moved to [[fairphone3]].

## Fairphone 4 and 5

Fairphone keeps pushing new phones out and I can't really keep track
anymore.

Note that the Fairphone 4 has come out and recently has teamed up with
[Murena](https://murena.com/) (AKA /e/) to [ship phones in the US](https://arstechnica.com/gadgets/2023/07/fairphone-is-coming-to-america/).

## Murena 2

Murena is doing their own crowdfunding for a [new phone](https://www.kickstarter.com/projects/murena/murena-2-switch-your-privacy-on/).

## Purism Librem 5

In development at the time of writing (2019-02-21), might ship in
"april 2019" according to their website but according to their [latest
report](https://puri.sm/posts/massive-progress-exact-cpu-selected-minor-shipping-adjustment/), "the previous Q2 estimate is now confirmed for Q3 product
shipping", so that means at least July 2019, if not later. Their demos
still don't have a finished device.

## SHIFTphone

[shiftphone 8](https://www.shift.eco/en/shiftphone-8-status-page-2/) will have [mainline support](https://www.phoronix.com/news/SHIFTphone-8-Linux-Patches),
incredibly. repairable, IPX rating. seems like a fat phone though.

## Google

### Pixels

Apart from, you know, being Google, the Pixel has a few more problems
that made me discard it, originally, there was no SD card and no
official CM build for the latest device (Pixel 3, since then fixed).

I've owned the Pixel 4a and 6a.

### Nexus S

[[!wikipedia Nexus_S]] - from 2010! Now at [[!wikipedia Nexus 6]], a [[!wikipedia Phablet]] now made by Google itself as part of the [[!wikipedia Google_Nexus]] family.

 * GSM/GPRS/EDGE Quad-band (850, 900, 1800, and 1900 MHz)
 * AWS WCDMA/HSPA Tri-band (900, 1700, and 2100 MHz) OR UMTS WCDMA/HSPA Tri-band (850, 1900, and 2100  MHz)
 * HSDPA 7.2 Mbit/s, HSUPA 5.76 Mbit/s

 * 512 MB RAM
 * 16 GB
 * 1,500 mAh replaceable

 * 4" display
 * 3-axis gyroscope, Accelerometer, Ambient light sensor, Capacitive
 * touch-sensitive buttons, Digital compass, Microphone, Multi-touch
 * capacitive touchscreen, Proximity sensor, Push buttons
 * 3.5 mm TRRS, A-GPS, Bluetooth 2.1 + EDR, Micro USB 2.0, NFC,
 * Wi-Fi 802.11b/g/n

No external keyboard, no FM transmitter?

## Motorola

Motorola is an interesting company. They made the first ever cell
phone and are the first company to [provide iFixit with OEM parts](https://ifixit.org/blog/11644/motorola-ifixit-partnership/),
so I should definitely give them a chance. LOS has good coverage of
their devices.

The [Moto Z](https://wiki.lineageos.org/devices/griffin) looks interesting but is not on sale in CC or BB. It
has a [good iFixit repair score](https://www.ifixit.com/Guide/Motorola+Moto+Z+Repairability+Assessment/79114) even if the battery is not
removable. The [Z2 force](https://wiki.lineageos.org/devices/nash) is well supported in LOS, but
unfortunately the [battery replacement](https://www.ifixit.com/Guide/Motorola+Moto+Z2+Force+Battery+Replacement/103378) is rated as "very
difficult" as it requires unplugging the mainboard, camera and
basically everything. Unfortunately, both of those are "big" (5.5",
like the LG G3).

The [Moto X4](https://wiki.lineageos.org/devices/payton) ([review](https://forum.xda-developers.com/moto-x4/review)) is very interesting, as it's
sealed. The only problem might be the lower battery life and the lower
resolution camera, when compared with the XA2. The body is about the
same size as the G3 and the screen is smaller, unfortunately.

## Samsung

Generally well supportedin LOS. The S7 has [good reviews](https://forum.xda-developers.com/galaxy-s7/review) but
hasn't been ported to the newer LOS 15.1. The [S9](https://wiki.lineageos.org/devices/starlte) is better and
also has [good reviews](https://forum.xda-developers.com/galaxy-s9/review) but is much more expensive. It also didn't
score well (4/10) in the [iFixit teardown](https://www.ifixit.com/Teardown/Samsung+Galaxy+S9+Teardown/104322) and is *huge* (5.8"/6.2").

Update: very tempted by the Samsung devices, but after being unable of
flashing a Samsung tablet, I'm wary of struggling against my hardware
manufacturer to have the freedom to install what I want on them. See
[this post for a hint](https://community.e.foundation/t/glaxay-s9-e-version-confusion/18076/27).

## Sony

The [XA2](https://wiki.lineageos.org/devices/pioneer) looks well maintained in LOS, and looks like generally a
nice phone. The [reviews](https://forum.xda-developers.com/xperia-xa2/review) are generally positive, except for the
camera. The XA2 is 5.2", the Ultra is 6.0" ([comparative](https://www.gsmarena.com/compare.php3?idPhone1=8985&idPhone2=8986)).

Another big downside is the repairability: you need a hot-air gun even
to just remove the back cover, according to [this video](https://www.ifixit.com/Teardown/Sony+Xperia+XA2+Teardown/110666).

## Xiaomi

Those make the fame [Pocophone F1](https://en.wikipedia.org/wiki/Xiaomi_Pocophone_F1) which I'm avoiding mostly
because of the notch but also [difficult battery access](https://www.youtube.com/watch?v=L5VWWba0coY&feature=youtu.be). It's also
gigantic (6.18").

Some Xiaomi devices like the Redmi Note 3 have [an excellent iFixit
score](https://www.ifixit.com/Device/Xiaomi_Redmi_Note_3) (8/10) but it's unclear if they are well supported in
LineageOS, as the phones listed are the [Mi Note 3](https://wiki.lineageos.org/devices/jason), the [Redmi
3S/3X](https://wiki.lineageos.org/devices/land) and the [Redmi 3/Prime](https://wiki.lineageos.org/devices/ido), the last of which is only
supported until 14. It's unclear how repairable those last three are.

Xiaomi devices are also hard to find at usual locations.

## Cosmo communicator

Huge phone running android, flip keyboard, 24MP camera, super powerful
but expensive.

https://www.indiegogo.com/projects/cosmo-communicator

 * Size: 17.14(W) x 7.93cm(D) x 1.6(H)cm

## Gemini & other PDAs

See [[laptop#gemini]].

# 2015 phones evaluation

This is getting incredibly out of date.

## Fairphone 1

The [[!wikipedia Fairphone]] is a really interesting project:

 * [homepage](http://www.fairphone.com/)
 * [specs](http://shop.fairphone.com/specs-page.html)

First, it's already shipping, although out of stocks now (feb
2015). Second, it really tries to avoid major human rights issues in
the production, something that's way too often overlooked.

 * Dual SIM
 * MicroUSB Port, Type B
 * GSM850/900/1800/1900MHZ
 * WCDMA 900/2100MHz
 * 1GB RAM
 * 16GB
 * 960x540
 * 8MP 1080P@30fps
 * 165 g
 * 2000mAh Replaceable
 * GPS, Wifi, FM (?), compass, proximity, gyro,
 * 8MP
 * 5" (143 mm x 73 mm x 11 mm)
 * 148 g (phone) + 20 g (external case)

Downside: it doesn't have an FM transmitter and the [baseband isn't
open](https://forum.fairphone.com/t/fairphone-baseband-os-firmware/1228), but that's pretty much the case for all phones out there
right now.

## Samsung Galaxy S3

[[!wikipedia Samsung_Galaxy_S_III]] - an interesting device:

 * 2G GSM/GPRS/EDGE - 850, 900, 1800, 1900 MHz
 * 3G UMTS/HSPA+/CDMA2000 - 850, 900, 1700, 1900, 2100 MHz
 * 4G LTE - 700, 800, 1700, 1800, 1900, 2600 MHz (NA, JP, AU, and KR versions)

 * 1 GB RAM
 * 16 or 32 GB
 * Up to 64 GB microSDXC
 * 2,100 mAh, replaceable
 * 4.8" 720×1280
 * 8MP f/2.6 1080p 30fps
 * Multi-touch capacitive touchscreen, aGPS, GLONASS, Barometer,
 * Gyroscope, Accelerometer, Digital compass
 * Wifi, NFC, Bluetooth 4.0 3.5 millimetres (0.14 in) TRRS

No FM transmitter, no external keyboard.

The S4 is similar, but one generation newer so better battery and faster LTE support (100mbps!), but at a slightly higher cost (140$ used vs 50-100$).

## Elephone

Very interesting phones: they are
[actively porting Cyanogenmod to their stack](http://www.elephone.cc/news/Elephone-port-CyanogenMod-121-to-Mediatek-phone/)
which is interesting, and they are dirt cheap (e.g. the
[G1](http://www.elephone.hk/G1.html) is
[60$USD](http://www.pandawill.com/elephone-g1-smartphone-android-44-mtk6582-quad-core-512mb-4gb-45-inch-black-p96895.html).

All the Elephone have, unless otherwise noted:

 * Dual SIM
 * MicroUSB port
 * MicroSD card
 * Cyanogenmod support

### G1

The [G1](http://www.elephone.hk/G1.html) is an interesting model, if
only because of the price (50-60$ USD). It has *no* 4G support, but
supports the 3G band we need for Rogers/Telus/Bell (850MHz) but not
Videotron (1700MHz).

It is also *not* supported by Cyanogenmod at this stage, and runs
Android 4.4.

* RAM: 512M
* ROM: 4GB
* 4.5" (`131.5*66*8.4 mm`, 125g)
* Wifi: :802.11b/g/n wireless
* WCDMA: band 1/5 (WCDMA 850/2100)
* GSM: band 2/3/5/8 (850/900/1800/1900)
* Camera: 5.0MP front:1.3MP
* Battery: 1800mAh
* Gravity sensor/ Proximity sensor / Light sensor, GPS/A-GPS

Price:

* [Pandawill](http://www.pandawill.com/elephone-g1-smartphone-android-44-mtk6582-quad-core-512mb-4gb-45-inch-black-p96895.html): 60$USD

The limited RAM could be a problem. This device is basically
comparable to the Nokia N900, without the FM transmitter, less builtin
storage and with Android.

### G2

The [G2](http://www.elephone.hk/G2.html) is also interesting, because
it supports Cyanogenmod and 4G, through base 7 (2600MHz: Bell, Rogers,
Vidéotron).

3G support is however problematic, because it only supports European
3G frequencies, which means we are stuck with 2G's GPRS and EDGE
connexions if there is no LTE support (which is basically spotty, at
best, in Canada right now).

* 4.5" (`134*66.6*8.9 mm`, 120g)
* RAM:	1GB
* Storage:	8GB
* Display:	4.5" , 854*480 pixels
* Battery:	2300 mAh
* OS:	Android 5.0/CM/MIUI/LEWA/Flyme
* Camera:	Rear-8.0MP,front-2.0MP
* SIM card:	Dual SIM card,dual standby
* Wi-Fi:	802.11n/a/c
* FDD-LTE:band 1/3/7/20 (800/1800/2100/2600)
* TDD-LTE:band 40 (2300)
* WCDMA:band 1/8 (WCDMA 900/2100)
* GSM:band 3/5/8 (850/900/1800)
* USB:	Micro USB
* Bluetooth:	4.0
* Positioning:	GPS,A-GPS
* Launch date:	April 1st, 2015
* Gravity sensor/ Proximity sensor / Light sensor
* FM support

Price:

* [Everbuying.net](http://www.everbuying.net/product815580.html): 103€

Other than the 3G support, looks like a great device.

### Trunk

The [Trunk](http://www.elephone.hk/Trunk.html) is really an awesome
phone. At
[120$](http://www.pandawill.com/elephone-trunk-smartphone-4g-64bit-snapdragon-410-android-51-50-inch-2gb-16gb-white-p104649.html),
it's very cheap for a 2GB LTE cell phone.

* 142.5*71.6*8.9mm
* Weight:	138g(with battery)
* CPU:	Snapdragon 410( Qualcomm MSM 8916)
* GPU:	Adreno 306
* RAM:	2GB
* Storage:	16GB
* Display:	5.0" HD
* Battery:	2100mAh （7.98Wh, Talk time: 360~400min Standby time:170~200h)
* OS:	Android 5.1
* Camera:	Rear-13 MP, front-2.0MP
* SIM card:	Dual SIM,dual standby (Micro SIM*2)
* Wi-Fi:	802.11b/g/n(2.4G)
* Bluetooth:	4.0
* Positioning:	GLONASS
* FDD:Band 1/3/7/20 (800 1800 2100 2600)
* WCDMA :Band 1/5/8 (850 900 2100)
* GSM:Band2/3/5/8 (850 900 1800 1900)
* Launch date:	August, 2015
* Gravity sensor / Proximity sensor / Light sensor, FM

It seems to support both 3G and 4G networks in Canada and in fact all
over the world. It
[unclear](http://bbs.elephone.hk/forum-288-1.html#.VdctWfMXBQI),
unfortunately, whether or not Cyanogenmod is supported on this
phone. The form factor is also problematic: this is a huge phone!!
Pretty much in the Phablet category...

This also seems to lack a critical component... a compass!
[To be confirmed](http://bbs.elephone.hk/thread-7211-1-1.html).

### P4000

The P series are also interesting, especially the
[P4000](http://www.elephone.hk/P4000.html) because of the extremely
long battery life:

* CPU:	MT6735
* GPU:	ARM Mali-T720
* RAM:	2GB
* Storage:	16GB
* Display:	5.0" HD (143.2*72.5*9.6mm)
* Battery:	4600mAh ( Typ.) 4420mAh (Rated) Li-POL
* OS:	Android 5.1
* Camera:	Samsung Rear-13.0 MP, front-2.0MP
* SIM card:	Dual SIM,dual standby (Micro SIM*2)
* Wi-Fi:	802.11b/g/n
* Bluetooth:	4.0
* Positioning:	GPS,A-GPS
* Networks:	FDD/TDD/WCDMA/GSM
* FDD-LTE:band 3/7/20 (800/1800/2600)
* TDD-LTE:band 40(2300）
* WCDMA:band 1/8 (900/2100)
* GSM:band 3/5/8 (850/900/1800)
* Launch date:	May, 2015

Unfortunately, it's also huge and has no local 3G support (!). But the
battery life is amazing.

Price:

* [Pandawill: 130$USD](http://www.pandawill.com/elephone-p4000-4g-smartphone-android-51-64bit-mtk6735-quad-core-4600mah-50-inch-white-p102197.html)

### P6000

The [P6000](http://www.elephone.hk/P6000-Pro-2gb.html) (5"),
[P7000](http://www.elephone.hk/P7000.html) (5.5"!) and
[P8000](http://www.elephone.hk/P8000.html) (5.5"!) have similar
issues, mostly because of their sheer size... half a feet long?? But
at least they all have 3G support. Here are the P6000 specs:

* CPU:	MTK6732
* GPU:	ARM MALI-T720
* RAM:	2GB
* Storage:	16GB
* Display:	5.0" , 1280*720pixels `144.5*71.6*8.9mm` (165g)
* Battery:	2700 mAh
* OS:	Android 5.1
* Camera:	rear-13MP,front-2.0MP
* SIM card:	Dual Micro SIM card,dual standby
* Wi-Fi:	802.11n wireless
* WCDMA:band 1/8 (900/2100)
* GSM:band 2/3/5/8 (850/900/1800/1900)
* FDD-LTE:band 1/3/7/20 (800/1800/2100/2600)
* USB:	2.0 Micro USB
* Bluetooth:	4.0
* Positioning:	GPS,A-GPS
* FOTA(wireless update):	support
* Launch date:	December 18, 2014

Prices:

* [Pandawill: 120$USD](http://www.pandawill.com/elephone-p6000-pro-smartphone-mtk6753-octa-core-50-inch-lg-screen-2gb-16gb-black-p103180.html) -
  warning: not the same band specs as above!!

## Nokia

Nokia has made very reliable, long-lasting phones forever, but they've
only recently started making Android phones. The [G22 phone](https://www.nokia.com/phones/en_int/nokia-g-22) has a
replaceable battery and runs Android 12. It only has support for 3
years however, but it looks like one of the first "easily repairable"
phones on the market by a big provider for years...

Also, it's cheap (<200$).

Downside: phablet, 6.5".

News:

 * [The Verge: HMD’s latest Nokia phone is designed to be repaired in
   minutes](https://www.theverge.com/2023/2/25/23611844/hmd-nokia-g22-repairable-smartphone-ifixit-sustainability)
 * [The Guardian: Nokia launches DIY repairable budget Android phone](https://www.theguardian.com/technology/2023/feb/25/nokia-launches-diy-repairable-budget-android-phone)
 * [Digital trends: Why Nokia made an Android phone it wants you to
   tear apart](https://www.digitaltrends.com/mobile/hmd-global-nokia-g22-quickfix-nokia-c32-nokia-c22-mwc-2023-news/)

## Other no-names

There are tons of other generic phones out there. A friend got
[this cubot phone](http://www.everbuying.net/product1055309.html)
which will be a good test for the 3G and 4G support.

# Vaporware

Those phones were nice ideas but never shipped.

## Phoneblocks

[[!wikipedia Phonebloks]] is the idea of a modular phone that could be easily fixable and field-upgradable. It was turned into a [discussion forum](https://phonebloks.com/) around 2013 by Motorola and Google in favor of their [[!wikipedia Project Ara]] scheduled for release in January 2015.

Here's a [pretty homepage](http://www.projectara.com/) (site dead, [archive](http://web.archive.org/web/20170329161342/https://atap.google.com/ara/) while we wait for something to actually happen.

Update: project was [killed by Google](http://venturebeat.com/2017/01/10/inside-project-ara-googles-revolutionary-modular-phone/), like [so many others](https://killedbygoogle.com/).

## Puzzlephone

[[!wikipedia Puzzlephone]] is a similar idea, with hopes of shipping somewhere in 2015.

Similarly, there's a [pretty homepage](http://www.puzzlephone.com/) while we wait for something to happen also.

## Teracube

<https://myteracube.com/>

4 years support, possibly greenwashing. 200USD.

# Provider packages

Canada has one of the worst markets for mobile phone service in the
world, which is unsurprising considering there's a relative oligopoly
with very little regulation to control it, the CRTC claiming
competition is sufficient to control the prices.

Furthermore, any list created here would be quickly out of date, so
it's somewhat pointless to even try. The [planhub.ca site][planhub]
does a good job at comparing prices, but I am not sure they are fully
independent. For example, they run ads, currently mainly for [Fizz][],
a branding operation from Vidéotron.

[planhub]: https://www.planhub.ca/
[Fizz]: https://fizz.ca

Here are the "big 4" in Québec:

 * [Bell](https://en.wikipedia.org/wiki/Bell_Mobility)
 * [Rogers](https://en.wikipedia.org/wiki/Rogers_Wireless)
 * [Telus](https://en.wikipedia.org/wiki/Telus)
 * [Vidéotron](https://en.wikipedia.org/wiki/Vid%C3%A9otron)

Then there are smaller ones that are resellers or branded versions:

 * [Chatr](https://en.wikipedia.org/wiki/Chatr) (Rogers, urban, entry-level)
 * [Fido](https://en.wikipedia.org/wiki/Fido_Solutions) (Rogers, originally funded by [T-Mobile](https://en.wikipedia.org/wiki/T-Mobile_US), mid-range)
 * [Fizz][] (Vidéotron, "DIY/BYOD" branding)
 * [Koodo](https://en.wikipedia.org/wiki/Koodo_Mobile) (Telus, no fixed term)
 * [Public Mobile](https://en.wikipedia.org/wiki/Public_Mobile) (Telus, originally autonomous, prepaid)
 * [Virgin Mobile](https://en.wikipedia.org/wiki/Virgin_Mobile#Virgin_Mobile_Canada) (Bell, "young")

I might go with Fizz: 50$/mth for unlimited text/voice Canada + USA,
10GB and voicemail. Nice.

## Data-only (2015)

See also this
[interesting article](http://misener.org/ditched-voice-plan-went-data/)
about the subject, and ensuing long discussion.

### Fido

[ipad plans](http://www.fido.ca/web/content/monthly/ipad_plans):

* Jusqu'à 150 Mo	10 $
* Jusqu'à 1 Go	25 $
* Jusqu'à 5 Go	35 $
* Données additionnelles : 10 $ pour 1 Go

### Rogers

[Confusing plans](http://www.rogers.com/consumer/wireless#plans)!

5$/mo:

* Up to 10MB	$5/mo
* Up to 100MB	$10/mo
* Up to 500MB	$20/mo
* Up to 5GB	$40/mo

10$/GB over.

$10/month:

* Up to 100MB	$10/mo±
* Up to 500MB	$30/mo±
* Up to 3GB	$45/mo±
* Up to 6GB	$65/mo
* Up to 10GB	$85/mo

10$/GB over.

$60/month:

* Up to 5GB	$60/mo±
* Up to 10GB	$75/mo
* Up to 20GB	$90/mo
* Up to 50GB	$110/mo
* Up to 100GB	$145/mo

$5/GB if usage is greater than 100GB

### Telus

[mobile internet](http://www.telus.com/fr/on/mobility/mobile-internet/)

Flex:

S'ajuste automatiquement à votre utilisation de données mensuelle.

* jusqu’à 100 Mo	10 $ / mo
* jusqu’à 500 Mo	30 $ / mp
* jusqu’à 2 Go	45 $ / mo
* jusqu’à 6 Go	70 $ / mo
* jusqu’à 10 Go	85 $ / mo
* plus de 10 Go	+ 5¢ / Mo (50$/Go)

Flex + Voix

* Appels nationaux illimités
* Messagerie vocale 25, appel conférence, afficheur et appel en attente
* 100 Mo de données flexibles

* jusqu’à 100 Mo	60 $ / mo3
* jusqu’à 500 Mo	80 $ / mo3
* jusqu’à 2 Go	95 $ / mo3
* jusqu’à 6 Go	120 $ / mo3
* jusqu’à 10 Go	135 $ / mo3

* jusqu’à 10 Go	+ 5¢ / Mo4

fixe:

* Données fixes au Canada
* 30 ¢ le texto envoyé ou reçu

* 2 Go	40 $ / mo
* 6 Go	65 $ / mo

Utilisation excédentaire	+ 5¢ / Mo (50$/Go)

# References

 * [Mapping Cyanogenmod to Android releases](https://en.wikipedia.org/wiki/CyanogenMod#Version_history)
 * [PDAdb.net](http://pdadb.net/) - searchable phone database
 * [Phonearena.com](http://www.phonearena.com/) - better searchable
   database, lacks prices (!)
 * <http://www.devicespecifications.com/> - good comparison tool
 * <http://www.gsmarena.com/> - more detailed and up to date tool!
 * <https://www.stockdroids.com/> - curated list
 * <http://www.planhub.ca/> - good plan comparison tool
 * <https://wiki.debconf.org/wiki/DebConf17/Sim-card-information> -
   quick research done for Debconf
 * <http://prepaid-data-sim-card.wikia.com/wiki/Canada> - prepaid sim
   card information wiki

[[!tag research]]
