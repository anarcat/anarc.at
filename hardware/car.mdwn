Car value
=========

The [Canadian black book](http://www.canadianblackbook.com/) is
somewhat authoritative for car values. Follow
[their convoluted form](http://www.canadianblackbook.com/black-book-values)
to find out the current value of any car, assuming the car is in good
shape, i gues.

For example, here's the
[value of a Toyota Echo 2003 (~3000$)](http://www.canadianblackbook.com/black-book-values/index/display/neworused/used/year/2003/make/Toyota/postalcode/H2L2C7/model/Echo/mode/marketanalysis/trim/NOTRIM/style/4D%20Sedan/).

CAA-Québec se
[base sur le black book et Guide Hebdo](https://www.caaquebec.com/fr/sur-la-route/services/estimation-de-la-valeur-dun-vehicule-doccasion/)
pour donner conseil...

Car comparisons
===============

* [axlegeeks.com](http://cars.axlegeeks.com/)
* [cars.com](http://www.cars.com/go/compare/modelCompare.jsp?myids=7644,4656,3883)
* [edmunds.com](https://www.edmunds.com/car-comparisons/)

Listings
========

* [Guide de l'auto](http://www.guideautoweb.com/occasions/)
* [Auto-hebdo.net](http://wwwa.autohebdo.net/autos/toyota/qc/montr%C3%A9al/?prx=100&prv=Qu%C3%A9bec&loc=h2s+2r8&sts=Neuf-Occasion&pRng=%2c5000&oRng=1000%2c&hprc=True&wcp=True&uag=C28484A8C31B6F670D1F7AFAE9610D92DFC8A897C244B774026A439C5DEAA458&rcs=0&rcp=100&srt=12)
* [Kijiji < 5000$ grand montréal](http://www.kijiji.ca/b-autos-camions/grand-montreal/autre+type+de+carrosserie__berline__bicorps__cabriolet__coupe__familiale/c174l80002a138?ad=offering&price=__5000)
* [Car gurus](https://www.cargurus.ca/)

Reference
=========

* [how to read a tire size](https://www.goodyearautoservice.com/en-US/tire-basics/tire-size)

[[!tag research]]
