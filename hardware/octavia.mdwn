[[!toc levels=2]]

Octavia est mon routeur maison. J'ai écrit mon tout premier article
pour LWN à son sujet, voir [[blog/2016-11-15-omnia]].

Il porte le nom de l'auteure de science-fiction [Octavia
E. Butler](https://en.wikipedia.org/wiki/Octavia_E._Butler), "*an African-American science-fiction writer*" known
for her "*critique of present day hierarchies, the remaking of the
human, the creation of alternative communities, and
afro-futurism*". The New York times described her novels as
"evocative" and "often troubling" explorations of "far-reaching issues
of race, sex, power".

> The destiny of Earthseed is to take root among the stars.
>
> -- Octavia Butler

Important note: this router is now a **wifi bridge** and most of the
documentation below is not applicable anymore. It was replaced with
[[margaret]].

# Bridge mode

To switch the router to bridge mode, I figured I would start from a
clean state so I tried a "factory reset", but this brings me to the
old Turris 3.x version, from which it is hard or impossible to upgrade
anymore. At least I couldn't get through the install wizard and I
would get weird errors when trying to update packages.

I also tried to flash the firmware with [this procedure](https://docs.turris.cz/hw/omnia/rescue-modes/#re-flash-router), but that
also led me to the old TurrisOS 3.x.

I eventually reverted back to an earlier snapshot and made the
following changes:

 1. removed a bunch of packages
 2. disabled the DHCP server on LAN
 3. turned LAN into a DHCP client
 4. assigned the MAC to an address on margaret
 5. connected it to the switch

The management interface is still available on the LAN IP, but it
needs to be known: it's currently hardcoded in DHCP but should be also
visible in the router's DHCP leases. Automated upgrades should still
be operational.

## SFP configuration

The SFP is not active by default. I found, [buried in some upgrade
notes](https://docs.turris.cz/geek/tos3-migration/#turris-omnia-specifics) that:

> Turris OS 5.0+ no longer supports switching between SFP and metallic in runtime. Switch is performed on boot for that boot-loader update is required. This is not yet available and so you have to switch between SFP and metallic manually. To do so you can run following command:
> 
>     ln -sf armada-385-turris-omnia-sfp.dtb /boot/dtb
> 
> To switch to metallic you just have to use phy instead of sfp in this command. Initial switch is performed as part of migration so you have to care about this only if you plan to switch between SFP and metallic WAN.

So at first I couldn't make the SFP port work at all: link wouldn't
even come up. But after running the above command and a reboot, the
link came back up and it even fetched an IP address from upstream on
the WAN port.

Unfortunately, that's not what I wanted: I had to bridge the port with
the `br-lan` bridge so that it keeps acting as a wireless bridge.

[This section explains a bit more how that works](https://docs.turris.cz/hw/omnia/omnia/#network-interfaces): the WAN and SFP
ports are not actually connected to the "switch-on-chip" at all and
instead to the SoC which means packets will necessarily go through the
CPU, which may impact performance. I might be interesting to compare
the Omnia performance between the SFP port or a LAN port behind a
media converter.

Configuration of the Omnia router
=================================

Basic configuration
-------------------

Features enabled in the basic config:

* auto-updates
* email notifications, setup from devnull@omnia.anarc.at, with SSMTP
  on port 465 straight to marcos, no password

Features disabled:

* wondershaper (wshaper package uninstalled) - it is apparently
  horrible for bufferbloat and we have plenty of bandwidth

UCI configuration:

* setup some internal static IPs
* setup DNS forwards for marcos
* firewall configuration (block everything but port forwards to marcos)
* change wifi frequency to Canada

Services retired:

* bitcoin (8333)
* retroshare (8359)
* TAHOE (3457)
* workrave (27273)
* xmpp (5222, 5269)
* irklab (8080)
* IPv6 firewall - disabled for now
* tinc and babel mesh
* munin node

Installed bash, screen and tcpdump:

    opkg update
    opkg install bash screen tcpdump

Interesting guide? http://www.jauu.net/2015/03/03/complete-openwrt-guide/

Had to hack the resolver to fix internal name resolution, see:

https://forum.turris.cz/t/knot-resolver-configuration-improvement-in-omnia/934/4

    -- forward requests to marcos for internal hostnames 
    policy:add(policy.suffix(policy.FORWARD('192.168.0.3'), {todname('anarc.at')}))
    policy:add(policy.suffix(policy.FORWARD('192.168.0.3'), {todname('anarcat.ath.cx')}))
    policy:add(policy.suffix(policy.FORWARD('192.168.0.3'), {todname('orangeseeds.org')}))
    policy:add(policy.suffix(policy.FORWARD('192.168.0.3'), {todname('168.192.in-addr.arpa')}))
    policy:add(policy.suffix(policy.FORWARD('192.168.0.3'), {todname('16.172.in-addr.arpa')}))
    policy:add(policy.suffix(policy.FORWARD('192.168.0.3'), {todname('9.0.0.0.1.0.0.0.8.2.9.1.1.0.0.2.ip6.arpa')}))
    policy:add(policy.suffix(policy.FORWARD('192.168.0.3'), {todname('a.0.d.4.b.e.1.9.0.c.2.f.7.0.6.2.ip6.arpa')}))
    policy:add(policy.suffix(policy.FORWARD('192.168.0.3'), {todname('0.0.f.8.f.0.0.f.0.c.2.f.7.0.6.2.ip6.arpa')}))

Traffic shaper
--------------

created  `/etc/updater/hook_postupdate/03_remove_wshaper.sh` with this:

    #!/bin/sh
    opkg remove "luci-i18n-wshaper-cs"
    opkg remove "luci-i18n-wshaper-en"
    opkg remove "luci-app-wshaper"
    opkg remove "wshaper"

so turris doesn't install it again, then installed sqm:

    opkg install luci-app-sqm

and followed: https://wiki.openwrt.org/doc/howto/sqm

before: http://www.dslreports.com/speedtest/6435557
after: http://www.dslreports.com/speedtest/6435680

lost 2mbps down, but fixed bufferbloat issue...

some tweaking may restore the 2mbps.

see also: https://forum.turris.cz/t/wondershaper-srsly-qos-aqm/1095

consider flent for network tests: https://forum.turris.cz/t/wondershaper-srsly-qos-aqm/1095

e.g. flent rrul netperf.bufferbloat.net

Deployment procedure
--------------------

1. change LAN IP
2. change WAN connection to `PPPoE`

Tests to run
------------

 1. Web, on another host:

        curl https://anarc.at/

    If no shell access, try [Webbloatscore][] since it produces a
    screenshot.

 2. DNS: local network should resolve locally, outside should show
    CNAMEs

        $ host shell.anarc.at
        shell.anarc.at is an alias for marcos.anarc.at.
        marcos.anarc.at has address 206.248.172.91

 3. SSH: test if we can reach the inside server from the outside of
    the network (and not the router)

        $ nc -v shell.anarc.at 22
        Connection to shell.anarc.at 22 port [tcp/ssh] succeeded!
        SSH-2.0-OpenSSH_6.7p1 Debian-5+deb8u3

Other ports are assumed to be correctly configured unless otherwise
noticed during later use.

[Webbloatscore]: https://www.webbloatscore.com

Changes from usual OpenWRT configurations
-----------------------------------------

There is a real syslog with persistent logfiles, in `/var/log`, so
forget about `logread`.

Same with SSH, it's `openssh`, so `/root/.ssh/authorized_keys` -
because `/etc/dropbear/authorized_keys` was weird anyways.

[[!tag node]]
