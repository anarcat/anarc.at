[GP-5 russian gas mask](https://en.wikipedia.org/wiki/GP-5_gas_mask)

https://ebay.us/BsNvOz 20 filters
https://www.amazon.ca/gp/offer-listing/B0861BTBWK/ 80$ for one!

3x[n95](https://en.wikipedia.org/wiki/N95_mask) respirators, two used


[DIY mask from Hong Kong](https://diymask.site/), similar to the n95 (~n70) if setup with
a nanoparticle filter

[home depot is all out of stocks](https://www.homedepot.ca/en/home/categories/tools/apparel-and-safety-gear/safety-and-protective-gear/respirators.html) except for [those](https://www.homedepot.ca/product/home-accents-halloween-animalistic-masks-price-per-item-/1001046333) which
offer... minimal protection.

[langevin has cartridges](https://www.langevinforest.com/fr/bloc-de-bois-de-rose-albergia-frutescens-9550)

there are [thread adapters](https://www.ebay.com/itm/GOST-Thread-Adapter/383482629513?_trkparms=aid%3D1110006%26algo%3DHOMESPLICE.SIM%26ao%3D1%26asc%3D225664%26meid%3D767fe51b51584db0a611f27dc6db06ff%26pid%3D100010%26rk%3D4%26rkt%3D12%26sd%3D202765700871%26itm%3D383482629513%26pmt%3D0%26noa%3D1%26pg%3D2047675%26algv%3DDefaultOrganicWithTitleNsfwFilter&_trksid=p2047675.c100010.m2109) between the old russian models and new
3m standard. that could be [this patent](https://patents.google.com/patent/US20170021201A1/en)


another mask design: https://melaniekham.com/homemade-face-mask/



why i'm interested in masks all of a sudden:
https://idlewords.com/2020/04/let_s_all_wear_a_mask.htm

another sewing pattern https://www.joann.com/on/demandware.static/-/Library-Sites-LibraryJoAnnShared/default/dw4148ae36/static/landingpage/assets/MaskInstructions_V2.pdf
