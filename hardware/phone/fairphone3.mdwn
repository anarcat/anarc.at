[[!toc levels=2]]

# Overview

The third generation of the fairphone was released in august 2019,
with pre-orders for October. Here are the hilights from the website:

 * Recycled and fair materials
 * Modular and repairable design
 * Full-day battery life
 * A commitment to fairness
 * Android 9, easy to use (update: now Android 10)
 * 12MP camera, great in low light (also updated in the FP3+)
 * Qualcomm Snapdragon 632
 * 64GB internal storage

See also the [[fairphone2]] page for a broader discussion of the
project.

# Specifications

 * OS: Android <del>9</del>10
 * Qualcomm Snapdragon 632 SoC
 * CPU: Cortex-A53, Qualcomm Kryo 250 (64bit, 8 core, 1.8GHz)
 * GPU: Qualcomm Adreno 506 650MHz
 * 4GB RAM
 * 64GB internal storage
 * MicroSD card ([precise specs unknonwn](https://forum.fairphone.com/t/fp3-microsd-controller-specs-a1-or-a2/52368), <=200GB)
 * 3060mAH battery (300 hours idle, 20h calls, 3.5h charge time)
 * 5.65" Full HD+ 18:9 LCD (IPS) touchscreen,  2160 x 1080 resolution,
   427ppi, Gorilla Glass 5, 16 million colors
 * Rear camera: 12MP f/1.8, 1/2.55" sensor, phase detection autofocus,
   Sony IMX363 sensor, digital image stabilization, dual LED flash, 8x
   digital zoom, 3840x2160 video resolution, 4k@30fps, 1080p@30fps,
   720p@60fps
 * Front camera: 8MP f/2.0, 1/4" sensor, Digital Image Stabilization,
   8x digital zoom
 * Wifi: 2.4 & 5 GHz, 802.11 a/b/g/n/ac, Wifi direct
 * Bluetooth 5 + LE
 * NFC
 * GNSS standards: GPS, Glonass, Galileo, A-GPS support
 * Dual Nano SIM (4FF, Max SAR head (W/kg @ 10g) = 0.388, Max SAR body
   (W/kg @ 10g) = 1.405)
 * Frequencies:

   |    | Bands                                                      | Technology                                                                                                           |
   |----|------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
   | 4G | Bands **1**, **2**, 3, **4**, **5**, 7, **13**, 20, **26** | (LTE)  Type - Cat. 13, MIMO - 4x2, 2CA Carrier Aggregation, VoLTE + VoWiFi, Max download 300Mbps, Max upload 150Mbps |
   | 3G | **800**, **850**, 900, **1700**, 1900, **2100** Mhz        | (HSPA+) HSDPA - Cat 24, HSUPA - Cat 6 Max download 42Mbps, Max upload 5.76Mbps                                       |
   | 2G | 850, 900, 1800, 1900 Mhz                                   | GMS, GPRS, EDGE) Type - Cat. 33                                                                                      |

   Bands in **bold** are new from the [[fairphone2]].
 * USB-C 2.0
 * Sensors: Fingerprint scanner, Ambient Light, Accelerometer,
   Gyroscope, Proximity, Barometer, Compass
 * Headphone jack
 * External speaker loudness: 95db @ 10cm
 * Audio: Miracast support, codecs: AAC/AAC+/eAAC+, MP3, WMA (v9,
   v10), WMALossless, WMAPro 10, AMR-NB, AMR-WB, FLAC, ALAC, Vorbis,
   AIFF, APE
 * Video: HEVC, H.264, MPEG-4, MPEG-2, H.263, VP8, VP9
 * Dark Translucent body and cover
   * Length 158 mm
   * Width 71.8 mm
   * Thickness 9.89 mm
   * Weight: 189g
   * IP54 certification
 * Again, [10/10 iFixit score](https://www.ifixit.com/Teardown/Fairphone+3+Teardown/125573)

# Issues

There are some problems, however, that I have found with the specs:

 * they [dropped support for Fairphone Open](https://support.fairphone.com/hc/en-us/articles/360032971751-Operating-systems-OS-for-the-Fairphone-3), their google-free
   version of Android. that's a real bummer, especially since
   LineageOS is [not yet supported](https://forum.fairphone.com/t/will-lineageos-be-supported-on-fairphone-3/52528) ([LineageOS also has an official port](https://forum.fairphone.com/t/official-lineageos-for-fairphone-3/66978) on the phone. so it's unclear
   you will be able to [use the phone without a google account](https://forum.fairphone.com/t/can-i-use-a-fp3-without-a-google-account/52569) and
   the associated surveillance at all. it's possible a post-launch
   update does provide support for those configurations, however. see
   also [this discussion](https://forum.fairphone.com/t/fp3-fairphone-open-os/52301). For now, the solution seems to be either
   [GSI](https://forum.fairphone.com/t/how-to-flash-a-custom-rom-on-fp3-with-gsi/57074) (Generic System Images, AKA plain AOSP), [LineageOS](https://forum.fairphone.com/t/lineageos-16-for-fp3-development/59849)
   (missing LTE, GPS, Updater and selinux) or [/e/](https://forum.fairphone.com/t/e-for-fp3-google-free-os/59328) ([missing
   sensors, camera, gps and fingerprint sensor](https://community.e.foundation/t/fairphone-3-fp3-support-on-e/6438/111)), TWRP has been
   [ported](https://forum.fairphone.com/t/twrp-for-fairphone-3/56995/26) but the port is [still unofficial](https://twrp.me/Devices/Fairphone/)
   Update: [/e/ *sells* FP3 phones now](https://esolutions.shop/shop/e-os-fairphone-3/)
 * the phone is 10% larger than the FP2 (FP3: 158 x 71.8 x 9.89 mm,
   FP2: 143 x 73 x 11 mm) but 2mm thinner and 1mm narrower

Things I'm still unsure about:

 * [no 4G support in the US](https://support.fairphone.com/hc/en-us/articles/360032577632-Connectivity-of-the-FP3-outside-of-Europe) - that's pretty dramatic, and is a
   blocker for a huge market, I don't understand how they made that
   decision. but you have to select *some* bands and it seems the
   range is [wider than it was with the FP2](https://forum.fairphone.com/t/fairphone-3-canada/52358/5?u=anarcat), so it might actually
   be *better* than the FP2, which *does* work (somewhat) in
   Canada. Apparently, it [works fine with Fizz/Vidéotron](https://forum.fairphone.com/t/canadian-fairphoners/17991/78?u=anarcat). As
   outlined by the chart above, there's a *large* number of new 3G
   and 4G bands available, but it's still a far cry from what you get
   with a (say) Samsung S9:
   
   |    | bands                                                                                                 |
   |----|-------------------------------------------------------------------------------------------------------|
   | 2G | GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2 (dual-SIM model only)                                     |
   || CDMA 800 / 1900 - USA                                                                                 |
   | 3G | HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100 - Global, USA                                               |
   || CDMA2000 1xEV-DO - USA                                                                                |
   | 4G | 1, 2, 3, 4, 5, 7, 8, 12, 13, 17, 18, 19, 20, 25, 26, 28, 32, 38, 39, 40, 41, 66 - Global              |
   || 1, 2, 3, 4, 5, 7, 8, 12, 13, 14, 17, 18, 19, 20, 25, 26, 28, 29, 30, 38, 39, 40, 41, 46, 66, 71 - USA |
   || HSPA 42.2/5.76 Mbps, LTE-A (6CA) Cat18 1200/200 Mbps                                                  |

   Still, it should be a significant improvement from the FP2.

 * "full day battery life" sounds like they haven't fixed the poor
   battery life of the FP2 I have witnessed. the advice on how to
   "[extend the battery life of your FP3](https://support.fairphone.com/hc/en-us/articles/360032857671-Extend-the-battery-life-of-your-Fairphone-3)" are stock "battery
   saver, etc" advice that worry me. but the specs say "300h standby
   time" so that could be much better than what I currently get, which
   is about 30-40h standby. see also [this discussion](https://forum.fairphone.com/t/fp3-vs-fp2-autonomy-and-durability/52407) and [this
   very positive report](https://forum.fairphone.com/t/fp3-battery-life-experiences/52936) (~10 days standby).
 * they say they have a "great" 12MP camera in "low light", but I'm
   not convinced that 12MP is that great for a camera phone at this
   point. it's the same resolution as the *older* fairphone 2 so I
   don't understand how they could give that an upgrade... apparently,
   the camera *is* better in that it can at least *compare* with the
   Pixel and Moto phone, according to [this review](https://tweakers.net/reviews/7290/4/fairphone-3-de-meest-repareerbare-smartphone-hardware-en-camera.html)

# Major advantages

 * headphone jack is still there
 * USB-C, although [without video output](https://forum.fairphone.com/t/fp3-and-hdmi-output-usb3-or-mhl/52511/5)
 * transluscent cover is back
 * [positive first impressions from iFixit](https://www.ifixit.com/News/the-fairphone-3-is-here-and-its-not-the-only-sustainable-phone-on-the-way) although they do
   mention problems with US networks and that there are now other
   porjects like the Fairphone (namely [Shift](https://www.shiftphones.com/en/) and
   [Teracube](https://myteracube.com/)). update: they did a 10/10 score in the [teardown](https://www.ifixit.com/Teardown/Fairphone+3+Teardown/125573)

# Why replace the FP2

I bought the [[fairphone2]] in march 2019, and now less than two years
later, I want to replace it, why? Isn't that device supposed to be
repairable and last forever? Turns out that's only partly true. Yes,
you can easily get spare parts for the Fairphone 2... if you live in
Europe! They do not ship globally, unfortunately. There's something to
be said about a somewhat generic iPhone or Samsung phone: anywhere in
the world there will be some geeky electronics corner store that will
be able to somewhat fix your phone, even if it means it's janky and
expensive. At least it exists, as much as Apple is fighting against
those shops of course...

The other big problem I have with the Fairphone 2 is that it's really
not well supported in Canada. Yes, it works, and sometimes I even get
4G (or at least HSPA). But coverage is really bad. Sometimes I don't
get signal at the grocery store or in parts of my house. It's really
hit and miss. I am hoping the FP3 will be better, but it might also
have trouble: it's the 4G band 12 and 66 which are used by Fido. Telus
uses the similar band 13 so maybe changing providers could help. Those
are the 700MHz bands which are critical to getting good long-range,
across-buildings signal... Still, it  already gives me 800 and 850MHz
on 3G, and 850MHz is where HSPA lives, so that might [improve things
significantly](https://forum.fairphone.com/t/repair-a-fairphone-2-or-buy-a-fairphone-3-in-canada/67434/3).

Finally, the FP2 was a nice experiment in a "screw-less" phone, but
that design has significant, real-life reliability issues. I often
need to "squeeze" the camera to get it to work, and sometimes it
doesn't work at all. The main screen sometimes "burns up" and the
touch becomes erratic until I squeeze it all over the place and then
sometimes it returns. I have tried the self-help troubleshooting
instructions, and they basically tell me to take it apart and back
together again, which is not really helpful. It's as if the connectors
between the modules loosen over time and just make the phone
unreliable.

Again, here, an attempt at making the phone easier to repair actually
made it harder. The FP3 has actual screws (and comes with a
screwdriver!) so this could possibly be better.

# Where to buy

Places that might ship the Fairphone 3 in Canada:

 * [Clove](https://www.clove.co.uk/collections/smartphones-fairphone) - 350GBP (~600CAD) + ~34CAD shipping + customs
 * [Vireo](https://www.vireo.de/fairphone/fairphone/8725/fairphone-3) - 450EUR (690CAD) + shipping + customs, ships to Kanada (note the
   K instead of C!)
 * [/e/](https://esolutions.shop/shop/e-os-fairphone-3/) - 480EUR (740CAD) + ship + customs, more expensive, but
   preinstalled with a liberated android! (backorder)

The Fairphone 3+ ([comparison](https://support.fairphone.com/hc/en-us/article_attachments/360075296031/FP3_vs_FP3_Plus_v1.3b.pdf)) is more expensive:

| Shop       | Price  | Converted |
|------------|--------|-----------|
| [Clove](https://www.clove.co.uk/collections/smartphones-fairphone/products/fairphone-4) | 338GBP | 584CAD    |
| [Vireo](https://www.vireo.de/fairphone/fairphone/8786/fairphone-3) | 469EUR | 728CAD    |
| [/e/](https://esolutions.shop/shop/e-os-fairphone-3-plus/)   | 500EUR | 776CAD    |

[Ecosto](https://www.ecosto.net/en/search/?q=fairphone+3), where I bought the [[fairphone2]], do not <del>yet</del> ship the
Fairphone 3 -- and has stopped shopping the FP2 as well, so they're
unlikely to ship the FP3.. See also the [Canadian fairphoners group](https://forum.fairphone.com/t/canadian-fairphoners/17991).

There's also this [third-party case for the FP3](https://annatreurniet.nl/products/fairphone-3-flip-case-book-type-bumper).

# Timeline

 * late 2020: Fairphone 2 started to show signs of hardware failure
 * 2020-11-30: ordered Fairphone 3+ from Clove, 628.86$CAD (shipping
   included, customs to be paid on delivery)
 * 2020-11-30: Clove asks me, by email, to send them a photo of my
   driver's license and credit card, or pay by PayPal. I refuse the
   former and accept the later, although it's unclear how and if they
   can now refund the credit card transaction...
 * 2020-12-15: (approximately) Clove did refund my credit card
 * 2021-01-13: I [[bought a Pixel 4a|blog/2021-01-13-new-phone]]
