Ursula is a home cinema, a [set-top box](https://en.wikipedia.org/wiki/Set-top_box) named after [Ursula K. Le
Guin](https://en.wikipedia.org/wiki/Ursula_K._Le_Guin), a "*novelist [who] worked mainly in the genres of fantasy
and science fiction, and authored children's books, short stories,
poetry, and essays. Her writing was first published in the 1960s and
often depicted futuristic or imaginary alternative worlds in politics,
the natural environment, gender, religion, sexuality, and
ethnography.*" - [Wikipedia](https://en.wikipedia.org/wiki/Ursula_K._Le_Guin#Selected_works)

> Hard times are coming when we will be wanting the voices of writers
> who can see alternatives to how we live now and can see through our
> fear-stricken society and its obsessive technologies to other ways
> of being, and even imagine some real grounds for hope. We will need
> writers who can remember freedom. Poets, visionaries—the realists of
> a larger reality.
> 
> -- Ursula Le Guin

# Configuration

The machine is a [Vero 4k+](https://osmc.tv/vero) for which I have analyzed the [hardware
specifications and performance](https://discourse.osmc.tv/t/full-specifications/75617). I also wrote a detailed [SSHFS](https://discourse.osmc.tv/t/sshfs-tutorial/77852)
tutorial. It so far works fairly well.

I migrated metadata from the previous Kodi install on
[[server/marcos]] using [this backup procedure](https://kodi.wiki/view/HOW-TO:Backup_the_video_library), which took an
extra 1189.32MB of disk space:

    git status --porcelain | sed 's/^...//;s/^"//;s/"$//' | while read path; do du -bs "$path" ; done | sort -n | awk ' {tot = tot+$1; print } END{ printf("%.2fMB\n",tot/(1024*1024)) }' 

I contributed that trick back to [Stack Overflow](https://stackoverflow.com/a/54433027/1174784). I'm not sure I
want to commit those into git, as they do take up quite a bit of space
(around 1GB). The artists images are duplicated a lot as well,
although git-annex would take care of deduplicating that.

# Future work

Possible improvements include having local storage, maybe with an
external HDD or a NAS, see [[server/marcos]] for a discussion on
those. I have a price watch on IronWolf drives, see [this
pcpartspicker item](https://ca.pcpartpicker.com/product/sD848d/seagate-ironwolf-8-tb-35-7200rpm-internal-hard-drive-st8000vn004), that could be combined with [this enclosure I
already have good experience with](https://www.canadacomputers.com/product_info.php?cPath=14_203&item_id=108319) or [that one](https://www.canadacomputers.com/product_info.php?cPath=14_203&item_id=072346) that looks a
bit sleeker.

I'm also looking at a way to trigger library rescans automatically:
Kodi supports doing that on restart, but that's somewhat
limited. There's a [plugin](https://kodi.wiki/view/Add-on:XBMC_Library_Auto_Update) to schedule those refresh based on a
timer, and another to watch the directory, see [this guide for
details](https://www.howtogeek.com/196025/ASK-HTG-HOW-DO-YOU-SET-YOUR-XBMC-LIBRARY-TO-AUTOMATICALLY-UPDATE/).

Ideally, rescans would be triggered from the outside, for example on
transmission or git-annex operations. This would involve the
[VideoLibrary.Scan](https://kodi.wiki/view/JSON-RPC_API/v9#VideoLibrary.Scan) API endpoint.

[[!tag node]]
