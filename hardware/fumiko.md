[[!toc levels=2]]

Update: this server was retired in favor of [[evelyn]].

fumiko was named after [Fumiko Kaneko](https://en.wikipedia.org/wiki/Fumiko_Kaneko), a Japanese feminist,
anti-colonialist, anarchist and nihilist, (possibly self-)convicted of
plotting to assassinate the Japanese emperor.

It's my first "cloud" VM (hence nihilism) designed to survive majour
outages or moves on [[hardware/server/marcos]].

I picked [linode](https://linode.com/) because it's cheap, it's not Hetzner (which we
use extensively at work), and they have locations close to me. At
first I thought of using their [Toronto PoP](https://speedtest.toronto1.linode.com/) but the [Newark
PoP](https://speedtest.newark.linode.com/) was 10 ms closer (40ms vs 30ms).

I set it up with a plain Debian 11 bullseye install from Linode, then
installed `git tmux puppet`.

At first I upgraded the box to bookworm, but destroyed the VM and
recreated it because puppet-agent 7 can't bootstrap against a
puppet-server 5.

So, bootstrap code is basically:

    apt install foot-terminfo tmux
    tmux
    apt upgrade
    hostname fumiko
    hostname > /etc/hostname
    echo 45.33.74.92 fumiko.anarc.at fumiko >> /etc/hosts
    apt install puppet
    puppet agent  --test --server puppet.anarc.at

then on the master:

    puppet ca list | grep $HASH_FROM_ABOVE
    puppet ca sign fumiko.anarc.at

then on fumiko again:

    puppet agent --test

[[!tag node]]
