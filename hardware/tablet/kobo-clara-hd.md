[[!meta title="Hacking my Kobo Clara HD"]]

I just got a new [Kobo](https://www.kobo.com/) ebook reader, a [Kobo Clara HD](https://ca.kobobooks.com/products/kobo-clara-hd?utm_source=Kobo&utm_medium=TopNav&utm_campaign=Clara). It's
pretty similar to the Glo HD I had but which has unfortunately [died
after 5 years](https://www.mobileread.com/forums/showthread.php?t=340012_), even after [trying to replace the battery](https://www.ifixit.com/Guide/Kobo+Glo+HD+eReader+Battery+Replacement/143903).

[[!toc levels=2]]

# Quick hardware review

This is a neat little device. It's *very* similar to the Glo HD, which
is a bit disappointing: you'd think they would have improved on the
design in the 5+ years since the Glo HD has come out.. It does have an
"amber" night light which is nice, but the bezel is still not level
with the display, and the device is still kind of on the thick side. A
USB-C (instead of micro-USB) port would have been nice too.

But otherwise, it's pretty slick, and just works. And because the
hardware design didn't change, I can still hack at it like a madman,
which is really why I bought this thing in the first place.

Hopefully it will last longer than 5 years. Ebook readers should
really last for decades, not years, but I guess that's too much to
expect from our consumerist, suicidal, [extinctionist society](https://rebellion.global/).

# Configuration hacks

Here are the hacks I done on the device. I had done many more hacks on
the Kobo Glo HD, but I decided to take a more streamlined, minimalist
and, hopefully, easier for new users than the pile of hacks I was
doing before (which I expand on at the end of the article).

[SSH support]: https://gitlab.com/anarcat/kobo-ssh
[wallabako]: https://gitlab.com/anarcat/wallabako

## SD card replacement

I [replaced the SD card](https://yingtongli.me/blog/2018/07/29/kobo-sd.html). The original card shipped with the Clara
HD was 8GB which meant all my books actually fitted on the original,
but just barely. The new card is 16GB.

Unfortunately, I did this procedure almost at the end of this guide
(right before writing the syncthing scripts, below). Next time, that
should be the first thing done so the original SD card acts as a
pristine copy of the upstream firmware. So even though this seems like
an invasive and difficult procedure, I actually do recommend you do it
first.

The process is basically to:

 1. crack open the Kobo case (don't worry, it sounds awful but I've
    done it often)
 2. take the SD card out
 3. copy it over to a new, larger card (say on your computer)
 4. put the larger card in

This [guide](https://yingtongli.me/blog/2018/07/29/kobo-sd.html) has all the details.

## Registration bypass hack

[This guide](https://yingtongli.me/blog/2018/07/30/kobo-rego.html) (from the same author!) has this awesome trick to
bypass the annoying registration step. Basically:

 1. pretend you do not have wifi
 2. mount the device
 3. `sqlite3 /media/.../KOBOeReader/.kobo/KoboReader.sqlite`
 4. `INSERT INTO user(UserID,UserKey) VALUES('1','');`
 5. unmount the device

More details in the above guide, again.

## Install koreader

My e-reader of choise is Koreader. It's just that great. I still don't
find the general user interface (ie. the "file browswer") as intuitive
as the builtin one, but the book reading just feels better. And
anyways it's the easier way to get a shell on the device.

Follow [those instructions](https://github.com/koreader/koreader/wiki/Installation-on-Kobo-devices), particularly the [NickelMenu](https://github.com/koreader/koreader/wiki/Installation-on-Kobo-devices#alternative-manual-installation-method-based-on-nickelmenu)
instructions (see also [the NickelMenu home page](https://pgaskin.net/NickelMenu/)). Yes, you need
to install some other thing to start koreader, which doesn't start on
its own. NickelMenu is the simplest and better integrated I have
found.

You might also want to [install some dictionnaries](https://github.com/koreader/koreader/wiki/Dictionary-support) and configure
SSH:

 1. mount USB
 2. drop your SSH public key in
    `.../KOBOeReader/.adds/koreader/settings/SSH/authorized_keys`
 3. unmount USB
 4. enable SSH in koreader (Gear -> Network -> SSH -> start SSH)

Note that ed25519 keys do *not* work: try an RSA key. This might be
because koreader ships with dropbear (or an older version), but I
haven't verified this.

## Install syncthing

I use [Syncthing](https://syncthing.net/) to copy all my books into the device now. I was
previously using Koreader's OPDS support with Calibre's web interface,
but that was clunky and annoying, and I'd constantly have to copy
books around. Now the entire collection is synchronized.

As a bonus, I can actually synchronise (and backup!) the koreader
metadata, since it's stored next to the files. So in theory, this
means I could use koreader from multiple devices and have my reading
progress sync'd, but I haven't tested that feature just yet.

I chose [Syncthing](https://syncthing.net/) because it's simple, lightweight, supported on
Linux and Android, and statically compiles by default which means it's
easy to deploy on the Kobo.

Here is how I installed and started Syncthing at first:

 1. [Download the latest version for ARM](https://syncthing.net/downloads/)
 2. extract the archive
 3. copy the `syncthing` binary into `.../KOBOeReader/.adds/`
 4. login over SSH (see above on how to enable) with `-p 2222 -l root`
 5. create the following directory: `~/.config/syncthing/` (which, i
    think, is actually directly `/.config/syncthing/`, yes, straight
    in the root directory, [YOLO](https://en.wikipedia.org/wiki/YOLO_(aphorism)))
 6. create the following configuration file, named `config.xml`:

        <configuration version="18">
            <gui enabled="true" tls="false" debugging="false">
                <address>0.0.0.0:8384</address>
            </gui>
        </configuration>
 7. copy a valid `ca-certificates.crt` file (say from your Linux
    desktop) into `/etc/ssl/certs/` on the Kobo (otherwise syncthing
    cannot bootstrap discovery servers)
 8. launch syncthing over SSH: `/mnt/onboard/.adds/syncthing`

You should now be able to connect to the syncthing GUI through your
web browser.

Immediately change the GUI admin user and password on the
`Settings: GUI` tab.

Then, figure out how to start it. Here are your options:

 1. on boot (`inittab` or whatever). downside: power usage.
 2. on wifi (`udev` hacks). downside: unreliable (see [wallabako][]).
 3. on demand (e.g. nickel menu, koreader terminal
    shortcuts). downside: kind of clunky in koreader, did not work in
    nickel menu.
 4. manually, through shell. downside: requires a shell, but then
    again we already have one through koreader?

What I have done is to write trivial shell scripts (in
`.../KOBOeReader/scripts`) to start syncthing. The first is
`syncthing-start.sh`:

    #!/bin/sh

    /mnt/onboard/.adds/syncthing serve &

Then `syncthing-stop.sh`:

    #!/bin/sh

    /usr/bin/pkill syncthing

This makes those scripts usable from the koreader file browser. Then
the folder can be added to the folder shortcuts and a long-hold on the
script will allow you to execute it.

Still have to figure out why the Nickel Menu script is not working,
but it could simply reuse the above to simplify debugging. This is the
script I ended up with, in `.../KOBOeReader/.adds/nm/syncthing`:

    menu_item :main    :Syncthing (toggle)    :cmd_spawn         :exec /mnt/onboard/scripts/syncthing-stop.sh
      chain_success:skip:4
        chain_success                      :cmd_spawn          :exec /mnt/onboard/scripts/syncthing-start.sh
        chain_success                      :dbg_toast          :Started Syncthing server
        chain_failure                      :dbg_toast          :Error starting Syncthing server
        chain_always:skip:-1
      chain_success                        :dbg_toast          :Stopped Syncthing server
    menu_item :main    :Syncthing (start)    :cmd_output         :exec /mnt/onboard/scripts/syncthing-start.sh
    menu_item :main    :Syncthing (stop)    :cmd_output         :exec /mnt/onboard/scripts/syncthing-stop.sh

It's unclear why this doesn't work: I only get "Error starting
Syncthing server" for the toggle, and no output for the `(start)`
action. In either case, syncthing doesn't actually start.

Update: according to a reader (thanks mmmaxwwwell!), this should work better:

    menu_item :main :Syncthing Start :cmd_spawn :quiet:/mnt/onboard/scripts/syncthing-start.sh
    chain_success :dbg_toast :Started Syncthing
    menu_item :main :Syncthing Stop :cmd_spawn :quiet:/mnt/onboard/scripts/syncthing-stop.sh
    chain_success :dbg_toast :Stopped Syncthing

... but I haven't tested this myself.

### Syncthing tweaks

As if the above wasn't complicated enough, you perhaps want to more
carefully configure some things in your shares to avoid wasting too
much time scanning and creating conflicts.

It seems like some filesystems (hello FAT!) have trouble keeping time,
and you are likely going to stumble upon this on Kobo. And
permissions, of course, cannot be synced. So in the folder se ttings
(`Edit` -> `Advanced` tab in the web GUI), you should make the
following changes:

- Ignore Permissions (checked)
- Sync Ownership (unchecked)
- Send Ownership (unchecked)
- Sync Extended Attributes (unchecked)
- Send Extended Attributes (unchecked)

There's also an "advanced folder setting" (which, oddly, is *not* in
the advanced section in the folder), called [modTimeWindowS](https://docs.syncthing.net/v1.27.10/users/config#config-option-folder.modtimewindows). To
change it through the GUI, in the main top-right menu, `Actions` ->
`Advanced` then find the right folder, scroll down to `Mod Time Window
(seconds)` and bump that to `2` (`0` is the default). (Note that this
is not [my first complaint about Syncthing usability](https://forum.syncthing.net/t/first-time-user-usability-review/6974)...)

In my case, my kobo kept generating conflicts on recently modified
files, and even attempting to remove those conflicts was failing: they
were just being resent to other syncthing clients from the kobo. Files
deleted from other clients would be recreated, and so on. It was
pretty irritating.

Scans were also extremely slow (10 minutes!), and it seems like
tweaking that setting fixed both issues.

Bizarrely, this is a [known issue in Syncthing](https://forum.syncthing.net/t/steps-to-speed-up-scanning/17923/2). In that
discussion, a user is being told to change the `mod time window`
setting (which doesn't really exist if you do a full, literal string
search, it's `modTimeWindowS`, thankfully the search engine forgives
you that whitespace error), without actually being told where to look
(and it's pretty deep).

What's even more bizarre is that there *is* code to automatically fix
that issue, by flipping that value to `2`, but only on
*Android*. There's [this forum thread about it](https://forum.syncthing.net/t/why-default-modtimewindows-to-2-for-fat-only-on-android/16256), where I asked
about this, we'll see.

## Avoided tasks

This list wouldn't be complete without listing more explicitly the
stuff I have done before on the Kobo Glo HD and which I have deliberately
decided *not* to do here because my time is precious:

 * [plato](https://www.mobileread.com/forums/showthread.php?t=292914) install: beautiful project, but koreader is good enough
 * [wallabako][] setup: too much work to maintain, Wallabag articles
   are too distracting and available on my phone anyways
 * using calibre to transfer books: not working half the time,
   different file layout than the source, one less [[Calibre
   dependency|software/desktop/calibre]]
 * using calibre to generate e-books based on RSS feeds (yes, I did
   that, and yes, it was pretty bad and almost useless)
 * [SSH support][]: builtin to koreader
 * offline Wikipedia support: incredibly, 512GiB micro-SD cards are a
   thing now, so all of Wikipedia could fit on a Kobo... Koreader has
   [Wikipedia support](https://github.com/koreader/koreader/wiki/Wikipedia-support) but cannot [work offline](https://github.com/koreader/koreader/issues/2333), although
   there's a [PR to add a plugin that works on a SQLite database
   converted from ZIM files](https://github.com/koreader/koreader/pull/9534). I've also found [hacks to make it
   work again on Nickel](https://phire.cc/Offline-Wikipedia-on-the-Kobo.html) ([another](https://a3nm.net/blog/kobo_glo_hacking.html)). But also, Kobo forcibly
   forces the `vfat` filesystem type for `/mnt/onboard`, so it's
   actually impossible to write the full Wikipedia archives, as FAT
   filesystems are limited to 4GB files. I tried patching that out of
   `/etc/init.d/rcS` but failed. The workaround typically used is to
   make another filesystem and add an extra init command to mount it
   at boot, and write files there.

Now maybe I'll have time to actually read a book...

[[!tag blog hardware python-planet debian-planet kobo syncthing]]
