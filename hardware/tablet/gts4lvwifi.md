[[!toc levels=3]]

[[!warning "I have NOT been able to flash my Samsung tablet with the
procedure here. The tablet is completely bricked and unusable. Do NOT
follow the instructions here, you have been warned."]]

# Picking an OS

During the brief moment where I thought I could recover this device, I
had to make a choice of which OS to install on there. There's
basically two options here:

 * LineageOS
 * /e/

## LineageOS

I've flashed LineageOS in the past (see [[hardware/phone/lg-g3-d852]]
and [[hardware/phone/htc-one-s]]) but the rather hostile IRC community
has always been undesirable to me. Still, it's kind of the baseline
that everyone looks at for everything, and has a crushing number of
supported devices, including this one.

The [install instructions](https://wiki.lineageos.org/devices/gts4lvwifi/install) are hellish and require Windows and
calling Odin. The [builds](https://download.lineageos.org/devices/gts4lvwifi/builds) are relatively up to date.

Unfortunately, [microg](https://microg.org/) is still not shipped with LOS and
sideloading it is challenging, so much so that microg now [builds its
own LOS fork](https://lineage.microg.org/).

Still, I should be able to sideload the [F-Droid privileged
extension](https://gitlab.com/fdroid/privileged-extension/#f-droid-privileged-extension) to get a minimum going in there.

## /e/

/e/ is an interesting LOS fork that is focused on privacy and "open
source". It also, more importantly, supports a large range of devices
including the tablet.

Oddly, it also ships one [proprietary maps app](https://doc.e.foundation/maps), [Magic
Earth](https://www.magicearth.com/). They make [their own privacy-branded phones](https://murena.com/) now. Also
ships with this weird [App Lounge](https://doc.e.foundation/app-lounge) that blends together apps from
the Google Store and F-Droid, although the latter is shipped through
this weird [CleanAPK thing](https://info.cleanapk.org/) (see also the [/e/ FAQ about
this](https://doc.e.foundation/app-lounge#how-can-i-trust-the-cleanapk-api-cleanapkorg)). See also [What's in /e/OS?](https://doc.e.foundation/what-s-e#whats-in-eos) and [default
applications](https://doc.e.foundation/what-s-e#eos-default-applications).

It should be noted that /e/ funds the development of microG since
2020.

The [install instructions](https://doc.e.foundation/devices/gts4lvwifi/install) are very similar to LOS and [here is the
direct link to their images](https://images.ecloud.global/dev/gts4lvwifi/).

The Q, R and S builds are /e/ code names for Android versions,
[according to ChatGPT](https://www.phind.com/search?cache=16f71844-cc16-410e-a725-583f3dcb6c86). It seems like after Android 9 "Pie" Google
stopped using candy names publicly and instead refer only to numbers
although internally they still use incrementing literal versions,
[according to Wikipedia](https://en.wikipedia.org/wiki/Android_version_history#Overview). So /e/ seems to be taking a cue from that
and has [this map of versions](https://doc.e.foundation/support-topics/about-e#from-which-version-of-android-is-eos-forked) so that Q is Androind 10, R is 11
and S is 12, which means that /e/ is one major version behind the
latest Android.

# Picked microG's LOS

The [microG LOS installation instructions](https://lineage.microg.org/#instructions) are basically the same
as LOS's except they have their own [fork of the update_verifier](https://github.com/lineageos4microg/update_verifier)
(which, interestingly, LOS doesn't actually mention in their
instructions). I have quickly audited commit
`7529ac8fdd82c758ab9520b20ea42b956811dcdb` of the microG fork and it
seems somewhat like a legit implementation of a ZIP signature check.

This is [their download link](https://download.lineage.microg.org/gts4lvwifi/).

# Flashing a Samsung Galaxy Tab S5e

Note: some parts below were written before the above.

## Failures

While this device *is* (still in 2023!) supported by LOS, it *still*
doesn't seem possible to flash it without Windows ([reddit](https://www.reddit.com/r/LineageOS/comments/hj3j80/comment/fwkc3wz/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button)). I
have tried with Heimdall and tried to follow the [postmarket
instructions](https://wiki.postmarketos.org/wiki/Manual_flashing) to use Heimdall with:

    heimdall download-pit --output file.pit

... because I don't have a `.pit` file and presumably I need that? But
this fails with:

    Initialising connection...
    Detecting device...
    Claiming interface...
    Setting up interface...

    Initialising protocol...
    ERROR: Failed to send handshake!ERROR: Protocol initialisation failed!

    Releasing device interface...

It seems this is a known problem with Heimdall and that [this
patch](https://github.com/Benjamin-Dobell/Heimdall/pull/478) fixes it! The patch was never merged though and it looks
like the Heimdall project is moribund. [This fork](https://git.sr.ht/~grimler/Heimdall) includes a bunch
of patches (including the above) and [/e/ OS](https://gitlab.e.foundation/e/tools/heimdall) and [LineageOS also
have forks](https://github.com/LineageOS/android_external_heimdall). Heimdall *does* detect the device:

    anarcat@angela:Downloads$ heimdall detect
    Device detected
    anarcat@angela:Downloads$

... when in "download mode" (power down and unplug device, press
power, volume up and down simultaneously and plug it back in).

The [postmarket OS](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_Tab_S5e_Wi-Fi_(gts4lvwifi)) page claims this device doesn't boot under
postmarket, so that's already a bad sign. However it *does* say the
pull request fixes the issue, but [in my experience](https://github.com/Benjamin-Dobell/Heimdall/pull/478#issuecomment-1548205467) it doesn't.

Heimdall is not very well documented. Part of the problem I'm having
(even before failing to communicate with the device above) is that
Heimdall expects `.pit` files or "Heimdall Firmware Package" files,
which I don't know how to create. The [Heimdall Linux README](https://raw.githubusercontent.com/Benjamin-Dobell/Heimdall/master/Linux/README) has
more information which could possibly fix this, but then the
communication issue keeps me from even downloading the original pit
file.

It does seem like [this person produces images](https://github.com/luk1337/gts4lv-fw/releases/tag/T720XXS3DWA1) for the Samsung S5e
that are usable. And [this reddit thread](https://www.reddit.com/r/LineageOS/comments/101c2iy/upgrade_firmware_of_samsung_tab_s5e_gts4lvwifi/) shows how to use
them. But neither of those because I can't communicate with the
device with any Heimdall version.

At some point I *was* able to dump the PIT (see the output in
[[pit.txt]]), but nothing much more. I think the next step is to flash
a recovery with:

    ~/dist/Heimdall/bin/heimdall flash --RECOVERY recovery.img --no-reboot --verbose

And then, from recovery, load the LOS `.zip` file. Update: it looks
like this kind of works, but is very flaky:

    anarcat@angela:lineage-20.0-20230515-nightly-gts4lvwifi-signed[SIGINT]$ ~/dist/Heimdall/bin/heimdall flash --RECOVERY recovery.img   --no-reboot --verbose
    Heimdall v2.0.2

    Copyright (c) 2010-2017 Benjamin Dobell, Glass Echidna
    https://www.glassechidna.com.au/

    This software is provided free of charge. Copying and redistribution is
    encouraged.

    If you appreciate this software and you would like to support future
    development please consider donating:
    https://www.glassechidna.com.au/donate/

    Initialising connection...
    Detecting device...
          Manufacturer: "Samsung"
               Product: "SDM710"

                length: 18
          device class: 2
                   S/N: 0
               VID:PID: 04E8:685D
             bcdDevice: 021B
       iMan:iProd:iSer: 1:2:0
              nb confs: 1

    interface[0].altsetting[0]: num endpoints = 1
       Class.SubClass.Protocol: 02.02.01
           endpoint[0].address: 82
               max packet size: 0010
              polling interval: 09

    interface[1].altsetting[0]: num endpoints = 2
       Class.SubClass.Protocol: 0A.00.00
           endpoint[0].address: 01
               max packet size: 0200
              polling interval: 00
           endpoint[1].address: 81
               max packet size: 0200
              polling interval: 00
    Claiming interface...
    Initialising protocol...
    Linux distro ID: debian
    Protocol initialisation successful.

    Beginning session...

    Some devices may take up to 2 minutes to respond.
    Please be patient!

    Session begun.

    Downloading device's PIT file...
    WARNING: Empty bulk transfer after receiving packet failed. Continuing anyway...
    PIT file download successful.

    Uploading RECOVERY
    0%ERROR: libusb error -7 whilst receiving bulk transfer. Retrying...
    ERROR: libusb error -7 whilst receiving bulk transfer. Retrying...
    ERROR: libusb error -7 whilst receiving bulk transfer. Retrying...
    ERROR: libusb error -7 whilst receiving bulk transfer. Retrying...
    ERROR: libusb error -7 whilst receiving bulk transfer. Retrying...
    ERROR: libusb error -7 whilst receiving bulk transfer.

    ERROR: Failed to receive file part response!

    ERROR: Retrying...
    1%

    3%

    4%

    6%

    7%

    9%

    10%

    12%

    14%

    15%

    17%

    18%

    20%

    21%

    23%

    25%

    26%

    28%

    29%

    31%

    32%

    34%

    35%

    37%

    39%

    40%

    42%

    43%

    45%

    46%
    ERROR: libusb error -7 whilst receiving bulk transfer. Retrying...

I retried this a few times and at some point it reached 100% but with
tons of errors. After this, the boot loader was kind of hosed and
would fail to boot the device with some checksum error on
`vbmeta.img`... Oops. Possibly that I should really have loaded all
those units at once instead of just the one recovery. I have since
then tried to load all files from the LOS `.zip` file without much
success:

    anarcat@angela:lineage-20.0-20230515-nightly-gts4lvwifi-signed[1]$ ~/dist/Heimdall/bin/heimdall flash --RECOVERY recovery.img --BOOT boot.img --VBMETA vbmeta.img --DTBO dtbo.img  --no-reboot --verbose

## Partial Success with TWRP

The [TWRP install instructions](https://twrp.me/samsung/samsunggalaxytabs5ewifi.html) require either
rooting the box with [Magisk](https://topjohnwu.github.io/Magisk/) (some obscure process I believe I failed
to follow last time I attempted this) or Odin. At first, this seemed
like a dead end, but eventually, it *worked*, see [[success-output.txt]].

The trick is to just try and try again. Encouraged by [a fellow
developer](https://github.com/Benjamin-Dobell/Heimdall/pull/478#issuecomment-817360071), I eventually managed to flash TWRP onto the device
with:

    heimdall flash --VBMETA vbmeta.img --RECOVERY recovery.img --no-reboot

Every time I retried this, I had to reboot by holding <kbd>volume
down</kbd> and <kbd>power</kbd> together then, after the screen
flashes off, release the keys and immediately press and hold
<kbd>volume down</kbd>, <kbd>volume up</kbd> and <kbd>power</kbd>
together until the download menu comes up. Then press <kbd>volume
up</kbd> *twice* to get to the actual download mode.

This moment of pure joy was short though, as when I tried to sideload
the LOS ZIP file from MicroG (see below), I was served with this
error:

    ERROR: This package requires firmware from an Android 11 based stock ROM build. Please upgrade firmware and retry!

So it seems I now need to reflash the device *again*, but this time
with the much larger stock firmware. Considering how flaky this whole
process is, this is depressingly unlikely to happen.

## Alternative procedure

As an aside, a colleague sent me the following procedure which *might*
have worked better:

 1. patch heimdall using github.com/Benjamin-Dobell/Heimdall/pull/478

 2. boot to download from adb

        adb reboot download

 3. now write the new VBMETA image

        ~/src/heimdall/Heimdall/build/bin/heimdall flash --VBMETA vbmeta.img --verbose

 4. reboot, reset up android with wifi, now reboot into download

        adb reboot download

 5. flash LOS recovery:
 
        ~/src/heimdall/Heimdall/build/bin/heimdall flash --RECOVERY lineage-18.1-20220228-recovery-gts4lvwifi.img --verbose --no-reboot

 6. then reboot straight into recovery - power and volume down to get
    out of download, then as soon as the screen goes black power and
    volume up

        adb sideload lineage-18.1-20220228-nightly-gts4lvwifi-signed.zip

 7. optional: google apps and Magisk

        adb sideload MindTheGapps-11.0.0-arm64-20220217_100228.zip
        adb sideload Magisk-Canary-v24.2.apk.zip

## Installing Android 11 stock firmware

I did try to go legit. I went on <https://samsung.com/> and asked what
was probably a chat bot a link to download the official stock rom
images. The [[Chat_Transcript.txt]] is pretty obnoxious.

So instead, I tried again with some stock firmware I found online, one
of the files found on [samfw.com section about the tablet](https://samfw.com/firmware/SM-T720/XAC/bit/all/av/R(Android%2011)).

First I extracted the ZIP file and then all the `tar.md5` files (which
are really just normal `.tar` files) and *then* the embedded `.lz4`
files. There were 32 files. I tried to generate a single commandline
to flash them all:
    
    for f in *.* ; do echo --$(grep -B 1 "$f" ~/wikis/anarc.at/hardware/tablet/gts4lvwifi/pit.txt | grep Partition | head -1 | sed 's/.*: //') $f \\ ; done

But eventually figured out what the heck, let's just try the GUI
frontend, and delicately loaded each image into its associated
partition.

I tried to bundle all of this in a Heimdall package and flash that,
but heimdall "crashed" according to heimdall-frontend.

Then I tried to sideload other LOS versions, still no lock.

## Completely bricking the device

[[!warning "DO NOT RUN THE COMMANDS BELOW IT WILL BRICK YOUR DEVICE"]]

Noticing TWRP was complaining about Android 11 right after doing a
comparison of `TZ`, I figured i might be able to flash just that
partition and fool TWRP in going ahead. So I foolishly uploaded some
binary I found on the internet, from the
`Samfw.com_SM-T720_XAC_T720XXS3DWA1_fac.zip` file.

    ~/dist/Heimdall/bin/heimdall flash --TZ tz.mbn

This ... kind of worked ("TZ upload successful" and no error from
Heimdall!). Full output:

    anarcat@angela:files[1]$ ~/dist/Heimdall/bin/heimdall  flash  --TZ tz.mbn --no-reboot
    Heimdall v2.0.2

    Copyright (c) 2010-2017 Benjamin Dobell, Glass Echidna
    https://www.glassechidna.com.au/

    This software is provided free of charge. Copying and redistribution is
    encouraged.

    If you appreciate this software and you would like to support future
    development please consider donating:
    https://www.glassechidna.com.au/donate/

    Initialising connection...
    Detecting device...
    Claiming interface...
    Initialising protocol...
    Protocol initialisation successful.

    Beginning session...

    Some devices may take up to 2 minutes to respond.
    Please be patient!

    Session begun.

    Downloading device's PIT file...
    PIT file download successful.

    Uploading TZ
    100%
    TZ upload successful

    Ending session...
    Releasing device interface...

    anarcat@angela:files$ ~/dist/Heimdall/bin/heimdall  flash  --TZ tz.mbn 
    Heimdall v2.0.2

    Copyright (c) 2010-2017 Benjamin Dobell, Glass Echidna
    https://www.glassechidna.com.au/

    This software is provided free of charge. Copying and redistribution is
    encouraged.

    If you appreciate this software and you would like to support future
    development please consider donating:
    https://www.glassechidna.com.au/donate/

    Initialising connection...
    Detecting device...
    Claiming interface...
    Initialising protocol...
    ERROR: Protocol initialisation failed!

    Releasing device interface...

... but then after rebooting the device from download mode (power and
volume down for 10 seconds), it never came back up!!!

Now the device is all blank. It's unclear if it's powered down at
all. Plugging in a USB-C cable into my laptop doesn't bring it to life
at all either, and Linux complains about the port being messed up:

    May 17 23:31:47 angela kernel: usb 1-4: new low-speed USB device number 108 using xhci_hcd 
    May 17 23:31:47 angela kernel: usb 1-4: device descriptor read/64, error -71 
    May 17 23:31:48 angela kernel: usb 1-4: device descriptor read/64, error -71 
    May 17 23:31:48 angela kernel: usb 1-4: new low-speed USB device number 109 using xhci_hcd 
    May 17 23:31:48 angela kernel: usb 1-4: device descriptor read/64, error -71 
    May 17 23:31:48 angela kernel: usb 1-4: device descriptor read/64, error -71 
    May 17 23:31:48 angela kernel: usb usb1-port4: attempt power cycle 
    May 17 23:31:49 angela kernel: usb 1-4: new low-speed USB device number 110 using xhci_hcd 
    May 17 23:31:49 angela kernel: usb 1-4: Device not responding to setup address. 
    May 17 23:31:49 angela kernel: usb 1-4: Device not responding to setup address. 
    May 17 23:31:49 angela kernel: usb 1-4: device not accepting address 110, error -71 
    May 17 23:31:49 angela kernel: usb 1-4: new low-speed USB device number 111 using xhci_hcd 
    May 17 23:31:49 angela kernel: usb 1-4: Device not responding to setup address. 
    May 17 23:31:50 angela kernel: usb 1-4: Device not responding to setup address. 
    May 17 23:31:50 angela kernel: usb 1-4: device not accepting address 111, error -71 
    May 17 23:31:50 angela kernel: usb usb1-port4: unable to enumerate USB device 
    May 17 23:31:50 angela kernel: usb 1-4: new low-speed USB device number 112 using xhci_hcd 
    May 17 23:31:50 angela kernel: usb 1-4: device descriptor read/64, error -71 
    May 17 23:31:50 angela kernel: usb 1-4: device descriptor read/64, error -71 
    May 17 23:31:51 angela kernel: usb 1-4: new low-speed USB device number 113 using xhci_hcd 
    May 17 23:31:51 angela kernel: usb 1-4: device descriptor read/64, error -71 
    May 17 23:31:51 angela kernel: usb 1-4: device descriptor read/64, error -71 
    May 17 23:31:51 angela kernel: usb usb1-port4: attempt power cycle 
    May 17 23:31:52 angela kernel: usb 1-4: new low-speed USB device number 114 using xhci_hcd 
    May 17 23:31:52 angela kernel: usb 1-4: Device not responding to setup address. 
    May 17 23:31:52 angela kernel: usb 1-4: Device not responding to setup address. 
    May 17 23:31:52 angela kernel: usb 1-4: device not accepting address 114, error -71 
    May 17 23:31:52 angela kernel: usb 1-4: new low-speed USB device number 115 using xhci_hcd 
    May 17 23:31:52 angela kernel: usb 1-4: Device not responding to setup address. 
    May 17 23:31:52 angela kernel: usb 1-4: Device not responding to setup address. 
    May 17 23:31:52 angela kernel: usb 1-4: device not accepting address 115, error -71 
    May 17 23:31:52 angela kernel: usb usb1-port4: unable to enumerate USB device

Normally, even if the device would be powered down and not powering
up, some embedded controller kicks in here and shows a charge logo
with a percentage. Even that's gone.

Interestingly, `strings tz.mbn | grep QC_IMAGE_VERSION_STRING` shows
`QC_IMAGE_VERSION_STRING=TZ.XF.5.0.2-00229` which is eerily
familiar. It looks like the version string TWRP was showing when
failing to flash. An older release (from another stock image,
`SM-T720_1_20210204194205_blrhsxo8je_fac`) shows a different version
(`QC_IMAGE_VERSION_STRING=TZ.XF.5.0.2-315709-9`) that is less
familiar. 

So maybe I flashed the wrong thing, the wrong way. In any case, the
thing is just totally dead now and I have lost all hope of recovery.

I was so close, yet so far. Typical Android flashing experience.

The machine is out of warranty at the retailer (30 days) but maybe the
"Samsung Experience Store" can do something with it, we'll see.
