[[!toc levels=3]]

See also [[USB-C docks and power supplies blog post|blog/2023-02-10-usb-c]].

# Power banks

## Specification

Minimum requirements:

 * USB-C input and output
 * phone-sized, not too bulky
 * airplane-ready (<=100Wh, [according to this](https://community.frame.work/t/this-looks-like-a-decent-portable-140w-power-bank-good-match-for-the-fw-laptop/23032/8?u=anarcat))
 * ~65W USB-C PD, enough to power a laptop for a while
 * fast *and* slow charging

Nice to have:

 * 100W+ USB-C PD
 * user-serviceable batteries
 * LED output with temperature, full charge, input/output power, etc,
   state of the art might be Sharge here...

One thing to keep in mind with user-swappable battery banks is that
even if the capacity itself isn't that big, it's possible to carry an
large number of (full) batteries and swap them along, or rotate them
in another charger (although another charger is then required).

## Ali Express stuff

There are a number of power banks with user-replaceable batteries on
Ali Express. See [this 18650/21700 PD45W power pack](https://www.aliexpress.com/item/1005005187644748.html) or [200W](https://www.aliexpress.com/item/1005005201173947.html)
(!!). Came from [this discussion](https://community.frame.work/t/we-want-a-battery-case/28802/60). The 200W might be a bit too
bulky and not airline-friendly. The 45W is pretty ugly and too bulky.

[This model](https://www.aliexpress.com/item/1005004338082463.html) boasts room for *16* 18650 but only 15W output. 15$
though. 90$ batteries included. [Recommended by this reddit user](https://old.reddit.com/r/SteamDeck/comments/ydb5uy/i_tried_the_aliexpress_ip5389_power_bank_with_my/),
found in [this thread](https://old.reddit.com/r/18650masterrace/comments/x9403q/help_with_purchasing_power_bank/iny6t6n/).

[This model](https://www.aliexpress.com/item/1005004244989066.html) is much more promising: 8x18650 batteries, 38.4Ah, PD
100W. 

Note: Ali Express also has [this USB tester](https://www.aliexpress.com/item/1005004417453269.html) which could be used to
compare battery packs and chargers.

## Anker

Anker is [widely](https://old.reddit.com/r/BuyItForLife/comments/987psf/request_looking_for_a_portable_power_bank/) [recommended](https://old.reddit.com/r/BuyItForLife/comments/i06nwq/request_bifl_power_bank/). The [Anker 737](https://www.anker.com/products/a1289?variant=41974285041814) is pretty
nice:

 * decent display
 * 140W in/out
 * simultaneous charge/discharge
 * 24Ah
 * 2xUSB-C 1xUSB-A
 * a little bulky: 155.7 × 54.6 × 49.5 mm
 * heavy: 636g
 * expensive: 170$

The [Anker prime](https://www.anker.com/products/a1336-20000mah-power-bank?variant=42691854925974) is slightly cheaper (130$) and shorter (124.6 x
53.3 x 48.26mm). 100W, 20Ah.

## Belkin

I have bought a number of Power banks from the local Best Buy. As
typical from Best Buy, all of the products I find in my order history
are gone from their website, but the current (late 2023) equivalent
would probably be this [10Ah power bank](https://www.bestbuy.ca/fr-ca/produit/chargeur-portable-a-deux-ports-usb-de-10000-mah-de-belkin-bleu/16937984).

Specs:

 * 15W output (total!)
 * 2xUSB-A 1xUSB-C
 * 68,33 x 131,83 x 15,75 mm, 222g

Poids : 222 g

The actual device I have on hand here is:

 * Output: 5V/2.1A
 * Capacity: 20Ah/74Wh
 * 2xUSB-A 1xUSB-C 1xMicroUSB (input)
 * Button with 4 LEDs indicating current charge
 * 144 x 67mm x 26mm, mass unknown, about a pound?

## Sharge

[Sharge](https://sharge.com/) also has interesting products, even if a little flashy and
expensive, [their Storm2 charger](https://sharge.com/products/storm2) looks like it has replaceable
batteries, [and this post seems to say it's possible to replace
them](https://old.reddit.com/r/mobilerepair/comments/zm2j9b/repairable_portable_power_banks_can_i_repair_this/j08nulc/), but [this teardown is disappointing](https://www.youtube.com/watch?v=jak02wPBHzg), and doesn't even
show how to replace the batteries. [This comment says they are "welded
in"](https://old.reddit.com/r/18650masterrace/comments/x9403q/help_with_purchasing_power_bank/io32bvt/), although that might be an exaggeration.

Specs:

 * 151 × 59 × 47mm, 591.3g
 * 25,600mAh / 93.5Wh
 * USB-C × 2 (100W / 70W + 30W), USB-A × 1 (18W), DC × 1 (75W, 5.5x2.5
   barrel connector)
 * PD 3.0
 * 199$
 * DC output port with voltage adjustable from 3.3V to 25.2V in 0.1V
   or 1V increments.

They have a [crowdfunding for a new model](https://www.indiegogo.com/projects/shargeek-170-unbeatable-power-unparalleled-style#/) as well, unclear how
user-serviceable that is.

They have a [slim version](https://sharge.com/products/storm2-slim) with less capacity (20Ah), less ports (1x
each USB-C USB-A) but can still deliver 100W. It also claims to be
built with Samsung 18650 batteries (but not if it's replaceable) but
cheaper (150$).

[This Framework user is very happy with his Storm2](https://community.frame.work/t/this-looks-like-a-decent-portable-140w-power-bank-good-match-for-the-fw-laptop/23032/12?u=anarcat).

Update: I ended up buying a Storm2 for now.

## TOFU

I bought the [MASA power bank](https://zentofu.com/tpb.php) which promise a 68.4Wh supply
so, in theory, could act as a second battery for my framework
laptop. I'll believe it when I see it though. It also acts as a
wireless charger which would be nice if I had any wireless charging
thing. It ships in a nice case and a USB-C wire with two adapters that
actually fit in the case if you roll them up just so. 

Specifications:

 * Full recharge in 3 hours with 33W input, 50% in one hour
 * Type-c Port1: Support USB-PD/Maximum 30W(5/9/12/15/20V)
 * Output Port1: Maximum 36W(20/15/12/9/5V)
 * Output Port2: Maximum 18W(9V2A,5V3A)
 * Wireless: QI standard Maximum 7.5W
 * Battery: Li-ion Polymer battery
 * 3 cells: 68.4Wh (18000mAh±5%)
 * Dimensions, closed: 80x80x28 mm
 * Dimensions, opened: 82x80x57 mm
 * Weight: 320g
 * [User manual](https://www.zentofu.com/book/en_TPB.jpg)
 * [Home page](https://zentofu.com/tpb.php)
 * [Shop](https://www.elvesfactory.com/worldshop/EN/TOFU/TPB) (88$USD)

A little bulky. Doesn't seem to actually charge anything, hugely
disappointing.

Update: in contact with tech support, it seems I am misinterpreting
the output of the LEDs. Also, when the battery is fully discharged, it
can't charge fast with USB-C.

Here are the LED meanings I could gather:

 * when clicked:
   * all four LEDs steady: battery full, 100% charged
   * 3 LEDs steady: 75% charged
   * 2 LEDs steady: 50% charged
   * 1 LED steady: 25% charged
   * all four LEDs blinking: low battery warning, plug in a USB-A slow
     charger for an hour
   * no LED: flat dead, plug in a slow charger
 * when charging:
   * rightmost LED blinking: 0-25% charged, need slow charging
   * one steady LED, second LED blinking: 25%+ charged, can charge
     fast

The LED button can be pushed for two seconds to reset the protection
circuits.

Update 2: even with that knowledge, the last time I tried to use this,
I failed. I was simply trying to charge a Pixel 6a phone, and it
wouldn't charge. Garbage.

## ZMI

Found out about the company in [this post on the Framework forum](https://community.frame.work/t/this-looks-like-a-decent-portable-140w-power-bank-good-match-for-the-fw-laptop/23032/8?u=anarcat)
referring to some battery pack of theirs they were happy with.

One charger I was puzzled by is this [combined charger /
battery](https://www.zmifi.com/en/product/1/156.html). It's a 45W charger with a small (6700mAh, so about
presumably 25Wh). It has a USB-A and USB-C port. Otherwise they have a
single [30W 10Ah battery](https://www.zmifi.com/en/product/1/173.html) which can presumably charge a Framework
laptop in an hour.

They're also doing this [crowdfunding campaign](https://www.indiegogo.com/projects/zmi-no-20-the-world-s-most-powerful-powerpack#/) for "ZMI No. 20:
The World's Most Powerful PowerPack 25000mAh Battery w/ 3 PD Ports |
Revolutionary 210W Max Output | 100W USB-C/USB-A | Fast Charge". That
product is actually "shipping" but is not on their main store page
yet, and it's not possible to buy it on the IndieGogo page either.

Interestingly, it seems to embed a 21700 battery, similar to the 18650
but more compact and apparently [used in Tesla cars](https://www.ecolithiumbattery.com/21700-vs-18650/), see also
[this comparison with the 18650](https://tritekbattery.com/21700-or-18650-battle-of-batteries-on-lev/). This gives at least some promise
that the batteries could be eventually changed, although there's no
promise on the repairability of this thing, which I would assume to be
poor unless proven otherwise.

## Xtar

[This thread](https://old.reddit.com/r/18650masterrace/comments/qpx456/power_bank_options/) pointed at the [PBS2 charger](https://www.xtar.cc/product/XTAR-PB2S-Charger-122.html) which features
swappable batteries, but is limited to ~25W output if I read it right.

30$.

## Folomov

An extreme case is the [Folomov A1](http://www.folomov.com/content/?126.html) which is just two terminals
that you connect to both end of a battery. Tiny, basically the size of
the battery, but then it's only one battery, so only 5W charging,
which is very slow. Upside: it can also charge the battery from an
external power source. [Review](https://lygte-info.dk/review/Review%20Charger%20Folomov%20A1%20UK.html)

# UPS

## Battery capacity estimates

### Upstairs office

| item     | DC V | DC A | DC W  | AC V | AC A  | VA   | rated VA   |
|----------|------|------|-------|------|-------|------|------------|
| ATA      | 5    | 2    | 10    | 120  | 0.3   | 36.  | 23-36      |
| phone    | 6    | 0.4  | 2.4   | 120  | 0.150 | 18.  |            |
| cordless | 6    | 0.3  | 1.8   | 120  | 0.025 | 3.   |            |
| Omnia    | 12   | 3.33 | 39.96 | 120  | 1.5   | 180. |            |
| Modem    | 12   | 2    | 24    | 120  | 0.7   | 84.  |            |
| Total    |      |      | 78.16 |      |       | 321. | 320?       |
<!-- this was an org-mode table that computed sums automatically: -->
<!-- #+TBLFM: $4=$2*$3::$7=$5*$6::@1$4=DC W::@1$7=VA::@7$4=vsum(@2$4..@6$4)::@7$7=vsum(@2$7..@6$7) -->

Setup on the APC BR1000MS (1000VA), which estimates ~3h standby time, with
about 18W in actual use.

This survived for about 2h30 minutes during a 3h power outage on 2021-06-09,
with indoor temperature of about 27C.

### Downstairs

server should take less than 500W AC, according to the specs. But according to
what's actually inside, it should be much less:

| item        | rating | count | rating count |
|-------------|--------|-------|--------------|
| Motherboard | 50 W   | 1     | 50 W         |
| CPU         | 65 W   | 1     | 65 W         |
| Memory      | 3 W    | 1     | 3 W          |
| HDDs        | 9 W    | 4     | 36 W         |
| SSDs        | 3 W    | 2     | 6 W          |
| Total       | -      | -     | 160 W        |
<!-- this was an org-mode table that computed sums automatically: -->
<!-- #+TBLFM: $4=$2*$3::@7$4=vsum(@2$4..@6$4)::<r2> -->

Actual power usage, according to the UPS downstairs (APC BX1500M 1500VA),
fluctuates between 60 and 80 watts, with about 50 minutes of standby time.

## Possible hardware

  * APC Back-UPS BR1500MS, 270$ 1500VA 4-15min load, USB 10 ports <https://www.mikescomputershop.com/product/8866164>
  * APC Back-UPS BR1500G 290$ 1500VA 3-13min load, no USB, 5 ports + 5 surge <https://www.mikescomputershop.com/product/76822>
  * APC Back-UPS 650 105$ 650VA 3-13min load max 400W load 4 ports + 4 surge <https://www.mikescomputershop.com/product/474878>
  * spare batteries: <https://www.upsbatterycenter.ca/>
  * how to pick a UPS (TL;DR: VA = 1.6*W): <https://www.howtogeek.com/161479/how-to-select-a-battery-backup-for-your-computer/>

See also [pc parts picker](https://ca.pcpartpicker.com/products/ups/) for this, cheapest rack-mount 1500KVA
UPS seems to be the [cyberpower CPS1500AVR](https://ca.pcpartpicker.com/product/wWX2FT/cyberpower-ups-cps1500avr) at 585$CAD at the time
of writing, but at that price you don't even get an LCD, for that you
need [640$](https://ca.pcpartpicker.com/product/JKZ2FT/cyberpower-ups-or1500lcdrt2u).

## Actual hardware

I ended up ordering this from Amazon (yes, I know):

  * APC BR1000MS: 1000VA, 196$
  * APC BX1500M: 1500VA, 212$

# See also

 * [[power.org]]
