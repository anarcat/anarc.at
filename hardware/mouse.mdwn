[Computer Mice](https://en.wikipedia.org/wiki/Computer_mouse). They've been with us since almost the very
beginning of computing, at least for machines with graphical
interfaces.

I used various mice over the years, from the [Mac plus](https://en.wikipedia.org/wiki/Macintosh_Plus) one-button
mouse to the almost-standard [modern optical mouse](https://en.wikipedia.org/wiki/Optical_mouse) that crowds
millions of offices world-wide. But now, I love [trackballs](https://en.wikipedia.org/wiki/Trackball). I
only use those now.

Kensington expert mouse
=======================

Specifically, a colleague/friend *gave* me a [Kensington Expert
mouse](https://www.kensington.com/p/products/control/trackballs/expert-mouse-wired-trackball/) during a serious RSI episode, for which I will ever be
grateful. It took a while to get used to it, but I will never go
back. It's really the most expensive mice I have ever owned (at
90$USD). Unfortunately, it's hard to find in real stores. The only
canadian store I could find it at is [CDW.ca](https://www.cdw.ca/product/kensington-expert-mouse/610537?pfm=srh), for 127$CAD. Staples
has it in the US as well, but the Canadian equivalent (Bureau En Gros)
only has the [wireless version](https://www.bureauengros.com/products/2125926-fr-kensington-boule-de-commande-sans-fil-expert-mouse-k72359ww) at 133$CAD.

Other stores:

 * [Insight](https://ca.insight.com/en_CA/shop/product/64325/KENSINGTON/64325/Kensington-Expert-Mouse--trackball--PS2-USB/) (130$CAD)
 * [SHI](https://www.shi.ca/products/productdetail.aspx?SHISystemID=SHICommodity&ProductIdentity=14220337&EventID=12692951-3336-42b3-8f27-75ec821bdaa6) (120$CAD)
 * [CDW.ca](https://www.cdw.ca/product/kensington-expert-mouse/610537?pfm=srh) (127$CAD)

The mouse is very comfortable, the ball is heavy and precise, and the
mouse wheel is the best I have ever used. It has so much inertia that
you can just send that thing *flying* and scroll quickly to long
documents, a bit like you can scroll quickly through things on phones
by swiping up repeatedly. I always find scroll wheels on other mouse
frustrating since I have found this one.

I also like the four buttons although I must admit I never use the
fourth one and always forget what it's for. The layout needed a bit of
time to get used to, but now it's wired into my fingers.

I just love this mouse.

Fun bit of trivia: according to Wikipedia, the original Kensington
expert mouse could use normal US pool balls to replace the trackball!
(I actually added a `{{citation needed}}`) And while it's [featured in
the Ocean's 8 movie](https://www.trackballmouse.org/kensington-expert-trackball-in-the-movie-oceans-8/), [actual tests](https://www.youtube.com/watch?v=e9QcsBrN5I4) show the 8-ball doesn't
track so well while the red 3-ball doesn't track at all, so I doubt
this really works in any real way.

But it's still freaking cool. Some even [built a custom mouse](https://maniacallabs.com/2019/01/22/billiard-ball-arcade-trackball-mouse/)
inspired by the movie. 

Kensington Orbit
================

Because the "expert" is so hard to find and expensive, I bought a
[orbit trackball](https://www.kensington.com/p/products/control/trackballs/orbit-trackball-with-scroll-ring/) for use at home. I never found it as satisfying,
as it has many issues:

 1. the scroll wheel doesn't have as much inertia as the "expert"
 2. there are only two buttons, so no middle button, which I use
    profusely

It's otherwise a decent mouse and is good for travel as the "ball"
doesn't fall off like it does with the "expert" model.

Clearly superior
================

I only mention this because I read about that hardware in some blog
that was swearing only by their trackballs. Their laser trackballs are
called [L-Trac](https://www.clearlysuperiortech.com/l-trac-product-selector) and they have an interesting design where the
scroll wheel is above the buttons. I'm not sure I like it, but it's
worth looking into I guess...
