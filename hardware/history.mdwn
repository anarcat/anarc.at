An attempt at an exhaustive list of hardware I bought and/or used as
personal computers. Does not count the hundreds of machines I have
setup and maintained as part of my work, at
[Koumbit.org](https://koumbit.org/) and elsewhere.

See also my software [[software/history]].

Key highlights:

 * oldest running machine: [[hardware/server/marcos]] (2011)
 * newest: [[ursula]] (2019)
 * first machine used: Vic-20, in the 1980s
 * number of workstations/servers built/managed at home: at least 5
 * number of routers/wifi hotspots: at least 5
 * number of laptops: 14
 * most common architecture: Intel (32 or 64 bits)

Here's the detailed history:

* 1980s: [Vic-20](https://en.wikipedia.org/wiki/Commodore_VIC-20)
* 1984+: [Mac Plus](https://en.wikipedia.org/wiki/Macintosh_Plus)
* 1998-2005: Pentium 1 clone: 32MB ram, 1GB disk ([[and a bunch more|blog/2005-08-24-ma-victoire-contre-la-machine]])
* 1998-2000: used [Indigo](https://en.wikipedia.org/wiki/SGI_Indigo%C2%B2_and_Challenge_M)
  workstations at university
* ?-2005: Thinkpad 380z (Pentium II 300Mhz + 96Mo de RAM et 3Go disk
* ~2005?: Toshiba Satellite 4090xDVD
* 2005 linux counter entry:
  * lenny: AMD Duron 1GHz 200GB disk, 512MB ram, workstation (Debian
    sid)
  * tangerine: Pentium III 700MHz 30GB disk, 256MB ram, laptop (Debian
    etch)
  * marvin: Pentium II 233MHz 34GB disk, 65MB ram, server, merged with
    lenny to yield marcos in 2009 (2011?)
    (Debian 3.1)
* 2006: [[Thinkpad T22|laptop/thinkpadt22]], cause of death: stolen?
* 2006: [[laptop/MobilePro780]] (PDA / NetBSD experiment)
* 2006-2007?: [[Toshiba Satellite A30|laptop/ToshibaSatelliteA30]],
  cause of death: slowness
* ?-2007: squatting hugo's machines: 20070906065732.GK4796@mumia.anarcat.ath.cx
* 2007-2011: [[Thinkpad X31|laptop/thinkpadx31]]  2008 - lenny in 2010?
* 2008: still on X31: 20080520194824.GB22856@mumia.anarcat.ath.cx
* 2008: laptop pété: 20080129214948.GA12870@anarcat.ath.cx
* 2009: mumia? 20080520194824.GB22856@mumia.anarcat.ath.cx
* 2009: feu laptop: 20091026192858.GH8286@anarcat.ath.cx
* 2008/2009?-2011: Asus Aspire One D250 (Atom N270 1.6GHz, 1GB ram, 160GB
  disque), cause of death: suspected compromise by RCMP ([install notes](http://wiki.debian.org/InstallingDebianOn/Acer/AspireOne-D250-1821))
* 2010 linux counter entry:
  * lenny: AMD Athlon 1.1GHz 200GB disk, 1GB ram, workstation (debian lenny)
  * mumia: Pentium M 1GHz 40GB disk, 1GB ram, laptop (Debian lenny)
* 2010: HP Mini 10 ([[many problems|blog/2010-03-18-hp-mini-10-netbook-doom]])
* 2011-2020: custom server ([[server/marcos]]), merge of marvin and
  lenny, backups of marvin archived in two disks (~120GB)
* 2012-2017: mesh experiments with [[server/plastik]], [[server/roadkiller]]
* 2011-2018: [[laptop/thinkpad-x120e]] (angela, 600$, 4GB RAM (2x2GB),
  AMD E-350, [[battery changed in
  2015|blog/2015-09-28-fun-with-batteries]], debian wheezy, jessie,
  then stretch. cause of death: screen cracked)
* 2016-...: [[octavia]] (Turris Omnia router, [[server/roadkiller]] replacement)
* 2017-2023: Intel NUC desktop (curie, 750$, 16GB, Intel i3-6100U
  2.3Ghz 4 threads, M.2 500GB disk,
  [installation report](https://wiki.debian.org/InstallingDebianOn/Intel/NUC6i3SYH#preview),
  debian stretch)
* 2017?-2018: [[server/mafalda]] (Raspbery Pi, print server moved to
  [[server/plastik]])
* (2012-2017) 2018-2020?: [[server/plastik]] (wifi router and print
  server in the office)
* 2018: Thinkpad x201 (temporary angela, 0$ from micah, i5 m520, 8GB
  RAM (2x4GB), physically worn out: keys falling off, disk slot broken, drive
  taken from old angela the x120e, running stretch, 128GB Crucial M4 SSD)
* 2018-2023: Thinkad x220 (new [[angela]], 150$ refurb from Encan Depot,
  8GB ram, running stretch, 512GB SSD)
* 2019-...: Vero 4k+ ([[ursula]], home cinema service replacing a part of
  marcos, which is moved to the basement)
* 2020-...: [[server/marcos]] hardware replacement, OS installed in 2010
* 2020-2022: TP-Link AC1750 router ([[emma]]), wireless bridge
* 2023-...: [[Framework 12|hardware/laptop/framework-12th-gen]] laptop
  (new [[angela]])
* 2023-...: Ubiquiti [Unifi AP 6 lite](https://store.ui.com/collections/unifi-network-access-points/products/unifi-ap-6-lite) wifi bridge ([[svetlana]])
* 2024-...: Protectli router ([[margaret]]), Omnia router
  ([[octavia]]) downgraded to wifi bridge
