[[!meta title="standby battery tests after 3.06 BIOS upgrade"]]

[[!toc levels=4]]

Result summary:

| Device      | Wattage | Amperage | Days | Note                              |
|-------------|---------|----------|------|-----------------------------------|
| baseline    | 0.25W   | 16mA     | 9    | no cards, same as before upgrade  |
| 1 USB-C     | 0.25W   | 16mA     | 9    | same as before                    |
| 2 USB-C     | 0.25W   | 16mA     | 9    | same                              |
| 1 USB-A     | 0.80W   | 62mA     | 3    | +550mW!! *worse* than before      |
| 2 USB-A     | 1.12W   | 73mA     | <2   | +320mW, on top of the above, bad! |
| Ethernet    | 0.62W   | 40mA     | 3-4  | new result, decent                |
| 1TB SSD     | 0.52W   | 34mA     | 4    | a bit worse than before (+2mA)    |
| MicroSD     | 0.51W   | 22mA     | 4    | same                              |
| DisplayPort | 0.52W   | 34mA     | 4+   | upgrade improved by 300mW         |
| 1 HDMI      | ?       | 38mA     | ?    | same                              |
| 2 HDMI      | ?       | 45mA     | ?    | a bit worse than before (+3mA)    |
| Normal      | 1.08W   | 70mA     | ~2   | Ethernet, 2 USB-C, USB-A          |

Dig below for the procedure and raw results.

# Procedure

Basically, the test procedure is:

    1. resume
    2. add/remove modules, change configuration
    3. suspend for 60 minutes
    4. collect results

The command, as root:

    journalctl -b | grep charge_now | tail -2 ; date ;\
    rtcwake -m no -s 3600 && systemctl suspend ; date ;\
    sleep 10 ; date ;\
    /opt/batterylog/batterylog.py ; \
    journalctl -b | grep charge_now | tail -2

Note that for the `mA` calculations, I just subtracted the
before/after stats as I have found out that the few extra seconds
typically do not change the result, it's generally a rounding error
and within the margin of error. This runs the risk of expanding the
margin of error by an extra `mA` of course. But a quick spot check on
a few calculations showed the result typically do not diverge even
after rounding.

# Research hypothesis and bias

The theory is that the 3.06 BIOS upgrade improved power usage. I am
biased towards feeling the Framework uses too much power and I am
doubtful of the claims. Let's see!

# Baseline: 0.25W, 16mA, ~9 days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;\
        rtcwake -m no -s 3600 && systemctl suspend ; date ;\
        sleep 10 ; date ;\
        /opt/batterylog/batterylog.py ; \
        journalctl -b | grep charge_now | tail -2
    jan 21 22:22:20 angela systemd-sleep[65127]: /sys/class/power_supply/BAT1/charge_now                     =   1692 [mAh]
    jan 22 14:20:05 angela systemd-sleep[65404]: /sys/class/power_supply/BAT1/charge_now                     =   2679 [mAh]
    dim 22 jan 2023 14:55:21 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Sun Jan 22 20:55:22 2023
    dim 22 jan 2023 14:55:21 EST
    dim 22 jan 2023 15:55:31 EST
    Slept for 1.00 hours
    Used 0.26 Wh, an average rate of 0.26 W
    For your 52.88 Wh battery this is 0.49%/hr or 11.88%/day
    jan 22 14:55:22 angela systemd-sleep[73362]: /sys/class/power_supply/BAT1/charge_now                     =   2352 [mAh]
    jan 22 15:55:23 angela systemd-sleep[73654]: /sys/class/power_supply/BAT1/charge_now                     =   2335 [mAh]

again:

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 22 16:02:38 angela systemd-sleep[75222]: /sys/class/power_supply/BAT1/charge_now                     =   2298 [mAh]
    jan 23 10:45:04 angela systemd-sleep[75517]: /sys/class/power_supply/BAT1/charge_now                     =   2822 [mAh]
    lun 23 jan 2023 11:38:30 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Mon Jan 23 17:38:31 2023
    lun 23 jan 2023 11:38:30 EST
    lun 23 jan 2023 12:38:40 EST
    Slept for 1.00 hours
    Used 0.25 Wh, an average rate of 0.25 W
    For your 52.88 Wh battery this is 0.47%/hr or 11.18%/day
    jan 23 11:38:31 angela systemd-sleep[83629]: /sys/class/power_supply/BAT1/charge_now                     =   2915 [mAh]
    jan 23 12:38:32 angela systemd-sleep[83909]: /sys/class/power_supply/BAT1/charge_now                     =   2899 [mAh]

This is a good baseline, fairly reliable as it's mostly reproducible
(within the 1mAh margin of error). Using 16mA or 0.25W power usage as
a baseline.

This does show that the BIOS upgrade did not improve power consumption
with no cards, that said. Worse, my battery is slightly more depleted
than the last time those tests were ran: it lost 1.65Wh or 3% of its
capacity in a little over 3 months. At that rate, it will be
completely depleted in 8 years or, more realistically, it will be at
50% capacity in 4 years.

# 1 USB-C: 0.25W, 16mA, ~9 days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 23 11:38:31 angela systemd-sleep[83629]: /sys/class/power_supply/BAT1/charge_now                     =   2915 [mAh]
    jan 23 12:38:32 angela systemd-sleep[83909]: /sys/class/power_supply/BAT1/charge_now                     =   2899 [mAh]
    lun 23 jan 2023 12:59:04 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Mon Jan 23 18:59:05 2023
    lun 23 jan 2023 12:59:04 EST
    lun 23 jan 2023 13:59:14 EST
    Slept for 1.00 hours
    Used 0.25 Wh, an average rate of 0.25 W
    For your 52.88 Wh battery this is 0.47%/hr or 11.18%/day
    jan 23 12:59:04 angela systemd-sleep[86439]: /sys/class/power_supply/BAT1/charge_now                     =   2829 [mAh]
    jan 23 13:59:06 angela systemd-sleep[86741]: /sys/class/power_supply/BAT1/charge_now                     =   2813 [mAh]
    root@angela:/home/anarcat#

Same as the baseline. Last time we did those tests, we had *better*
results than the baseline with the USB-C card, which was strange.

# 2 USB-C: 0.25W, 16mA, ~9 days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 23 12:59:04 angela systemd-sleep[86439]: /sys/class/power_supply/BAT1/charge_now                     =   2829 [mAh]
    jan 23 13:59:06 angela systemd-sleep[86741]: /sys/class/power_supply/BAT1/charge_now                     =   2813 [mAh]
    lun 23 jan 2023 14:01:20 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Mon Jan 23 20:01:20 2023
    lun 23 jan 2023 14:01:20 EST
    lun 23 jan 2023 15:01:29 EST
    Slept for 1.00 hours
    Used 0.25 Wh, an average rate of 0.25 W
    For your 52.88 Wh battery this is 0.47%/hr or 11.18%/day
    jan 23 14:01:21 angela systemd-sleep[88077]: /sys/class/power_supply/BAT1/charge_now                     =   2801 [mAh]
    jan 23 15:01:21 angela systemd-sleep[88313]: /sys/class/power_supply/BAT1/charge_now                     =   2785 [mAh]

Same as a single USB-C.

# 1 USB-A: 0.80W, 52mA, ~3 days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 23 14:01:21 angela systemd-sleep[88077]: /sys/class/power_supply/BAT1/charge_now                     =   2801 [mAh]
    jan 23 15:01:21 angela systemd-sleep[88313]: /sys/class/power_supply/BAT1/charge_now                     =   2785 [mAh]
    lun 23 jan 2023 15:08:48 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Mon Jan 23 21:08:48 2023
    lun 23 jan 2023 15:08:48 EST
    lun 23 jan 2023 16:08:59 EST
    Slept for 1.00 hours
    Used 0.80 Wh, an average rate of 0.80 W
    For your 52.88 Wh battery this is 1.51%/hr or 36.32%/day
    jan 23 15:08:49 angela systemd-sleep[89636]: /sys/class/power_supply/BAT1/charge_now                     =   2749 [mAh]
    jan 23 16:08:51 angela systemd-sleep[89930]: /sys/class/power_supply/BAT1/charge_now                     =   2697 [mAh]

Here we see that the USB-A card uses *more* power than the USB-C,
which is similar to the result we had previously, so no improvement
from the 3.06 BIOS upgrade.

# 2 USB-A: 1.12W, 73mA, <2 days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 23 17:54:40 angela systemd-sleep[94541]: /sys/class/power_supply/BAT1/charge_now                     =   2558 [mAh]
    jan 23 17:55:02 angela systemd-sleep[94770]: /sys/class/power_supply/BAT1/charge_now                     =   2558 [mAh]
    lun 23 jan 2023 17:55:08 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Mon Jan 23 23:55:09 2023
    lun 23 jan 2023 17:55:08 EST
    lun 23 jan 2023 18:55:19 EST
    Slept for 1.00 hours
    Used 1.12 Wh, an average rate of 1.12 W
    For your 52.88 Wh battery this is 2.12%/hr or 50.99%/day
    jan 23 17:55:09 angela systemd-sleep[95415]: /sys/class/power_supply/BAT1/charge_now                     =   2556 [mAh]
    jan 23 18:55:11 angela systemd-sleep[95648]: /sys/class/power_supply/BAT1/charge_now                     =   2483 [mAh]

Even worse, those cards *still* add up, just like last time. Now your
battery is down to two days.

# 3 USB-A: N/A

I didn't have a third USB-A card on hand to test at this very moment,
might come later if requested.

# Ethernet: 0.62W, 40mA, 3-4 days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 23 17:55:09 angela systemd-sleep[95415]: /sys/class/power_supply/BAT1/charge_now                     =   2556 [mAh]
    jan 23 18:55:11 angela systemd-sleep[95648]: /sys/class/power_supply/BAT1/charge_now                     =   2483 [mAh]
    lun 23 jan 2023 19:10:54 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Tue Jan 24 01:10:55 2023
    lun 23 jan 2023 19:10:54 EST
    lun 23 jan 2023 20:11:05 EST
    Slept for 1.00 hours
    Used 0.62 Wh, an average rate of 0.62 W
    For your 52.88 Wh battery this is 1.16%/hr or 27.94%/day
    jan 23 19:10:55 angela systemd-sleep[97482]: /sys/class/power_supply/BAT1/charge_now                     =   2419 [mAh]
    jan 23 20:10:57 angela systemd-sleep[97793]: /sys/class/power_supply/BAT1/charge_now                     =   2379 [mAh]

Interestingly, the Ethernet card uses *less* power than the dreaded
USB-A cards. This is just beyond comprehension for me, as the
electronics in the Ethernet card surely *must* be more complex than
the USB-A card.

# 1TB SSD: 0.52W, 34mA, ~4 days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 23 19:10:55 angela systemd-sleep[97482]: /sys/class/power_supply/BAT1/charge_now                     =   2419 [mAh]
    jan 23 20:10:57 angela systemd-sleep[97793]: /sys/class/power_supply/BAT1/charge_now                     =   2379 [mAh]
    lun 23 jan 2023 20:17:28 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Tue Jan 24 02:17:29 2023
    lun 23 jan 2023 20:17:28 EST
    lun 23 jan 2023 21:17:39 EST
    Slept for 1.00 hours
    Used 0.52 Wh, an average rate of 0.52 W
    For your 52.88 Wh battery this is 0.99%/hr or 23.74%/day
    jan 23 20:17:29 angela systemd-sleep[99542]: /sys/class/power_supply/BAT1/charge_now                     =   2335 [mAh]
    jan 23 21:17:31 angela systemd-sleep[99830]: /sys/class/power_supply/BAT1/charge_now                     =   2301 [mAh]

This is similar to last time results, but a little (+2mA) worse.

# MicroSD: 0.51W, 33mA, ~4 days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 23 22:23:32 angela systemd-sleep[106217]: /sys/class/power_supply/BAT1/charge_now                     =   1980 [mAh]
    jan 24 07:16:37 angela systemd-sleep[106523]: /sys/class/power_supply/BAT1/charge_now                     =   1703 [mAh]
    mar 24 jan 2023 07:17:20 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Tue Jan 24 13:17:21 2023
    mar 24 jan 2023 07:17:20 EST
    mar 24 jan 2023 08:17:31 EST
    Slept for 1.00 hours
    Used 0.51 Wh, an average rate of 0.51 W
    For your 52.88 Wh battery this is 0.96%/hr or 23.05%/day
    jan 24 07:17:21 angela systemd-sleep[108096]: /sys/class/power_supply/BAT1/charge_now                     =   1693 [mAh]
    jan 24 08:17:23 angela systemd-sleep[108339]: /sys/class/power_supply/BAT1/charge_now                     =   1660 [mAh]
    root@angela:/home/anarcat#

Within the 1mA margin of error from last test.

# normal setup (Ethernet, 2 USB-C, 1 USB-A): 1.08W, 70mA, ~2 days

Then I switched to my normal setup to go home, and ran the test by hand:

    root@angela:/home/anarcat#  /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    Slept for 3.11 hours
    Used 3.37 Wh, an average rate of 1.08 W
    For your 52.88 Wh battery this is 2.05%/hr or 49.17%/day
    jan 24 08:19:10 angela systemd-sleep[109724]: /sys/class/power_supply/BAT1/charge_now                     =   1646 [mAh]
    jan 24 11:25:57 angela systemd-sleep[109965]: /sys/class/power_supply/BAT1/charge_now                     =   1427 [mAh]

So basically, with my normal setup, I have a two-day standby
battery. Not great. It should also be noted that a lot of that power
(~34mA, almost half!) usage is due to the USB-A card. The Ethernet
card takes another good chunk (~25mA) but that's understandable and I
expected it to take more power. The USB-A card, though, is really
annoying.

# DisplayPort: 0.52W, 34mA, 4+ days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 24 11:26:41 angela systemd-sleep[111222]: /sys/class/power_supply/BAT1/charge_now                     =   1420 [mAh]
    jan 24 11:27:16 angela systemd-sleep[111456]: /sys/class/power_supply/BAT1/charge_now                     =   1419 [mAh]
    mar 24 jan 2023 11:27:24 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Tue Jan 24 17:27:26 2023
    mar 24 jan 2023 11:27:24 EST
    mar 24 jan 2023 12:27:36 EST
    Slept for 1.00 hours
    Used 0.52 Wh, an average rate of 0.52 W
    For your 52.88 Wh battery this is 0.99%/hr or 23.74%/day
    jan 24 11:27:25 angela systemd-sleep[112157]: /sys/class/power_supply/BAT1/charge_now                     =   1418 [mAh]
    jan 24 12:27:28 angela systemd-sleep[112388]: /sys/class/power_supply/BAT1/charge_now                     =   1384 [mAh]

Finally, a significant improvement! The previous results were pretty
bad at 0.85W (55mA), now we're down to a more reasonable number, but
it's still rather high for an inactive card during standby.

# 1 HDMI: W, 38mA, days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 24 11:27:25 angela systemd-sleep[112157]: /sys/class/power_supply/BAT1/charge_now                     =   1418 [mAh]
    jan 24 12:27:28 angela systemd-sleep[112388]: /sys/class/power_supply/BAT1/charge_now                     =   1384 [mAh]
    mar 24 jan 2023 12:28:31 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Tue Jan 24 18:28:32 2023
    mar 24 jan 2023 12:28:31 EST
    mar 24 jan 2023 13:28:34 EST
    Slept for -0.02 hours
    Used -0.12 Wh, an average rate of 6.93 W
    For your 52.88 Wh battery this is 13.10%/hr or 314.50%/day
    jan 24 12:28:32 angela systemd-sleep[113684]: /sys/class/power_supply/BAT1/charge_now                     =   1376 [mAh]
    jan 24 13:28:34 angela systemd-sleep[113928]: /sys/class/power_supply/BAT1/charge_now                     =   1338 [mAh]

Here, `batterylog` seems to have gone a little cookoo and seems to be
counting backwards (`Slept for -0.02 hours`). We can still calculate
the mA used during that hour (38mA), which is similar to past results.

# 2 HDMI: W, 45mA, days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 24 12:28:32 angela systemd-sleep[113684]: /sys/class/power_supply/BAT1/charge_now                     =   1376 [mAh]
    jan 24 13:28:34 angela systemd-sleep[113928]: /sys/class/power_supply/BAT1/charge_now                     =   1338 [mAh]
    mar 24 jan 2023 13:58:36 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Tue Jan 24 19:58:37 2023
    mar 24 jan 2023 13:58:36 EST
    mar 24 jan 2023 14:58:39 EST
    Slept for -0.50 hours
    Used -1.85 Wh, an average rate of 3.69 W
    For your 52.88 Wh battery this is 6.98%/hr or 167.55%/day
    jan 24 13:58:36 angela systemd-sleep[117036]: /sys/class/power_supply/BAT1/charge_now                     =   1218 [mAh]
    jan 24 14:58:39 angela systemd-sleep[117336]: /sys/class/power_supply/BAT1/charge_now                     =   1173 [mAh]

Again batterylog is unhappy and counting backwards, not sure
why. We're again close to our previous results, if a little worse off
(by 3mA), so that's not great.

# s2idle, 1 USB-A: 0.42W, 27mA, 4+ days

After someone [raised the possibility the poor USB-A performance might
be due to deep sleep](https://community.frame.work/t/test-results-for-standby-battery-use-of-expansion-cards/23711/38?u=anarcat), I reran the USB-A test with `s2idle` sleep:
    
    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 25 14:36:34 angela systemd-sleep[26685]: /sys/class/power_supply/BAT1/charge_now                     =   3460 [mAh]
    jan 25 15:36:34 angela systemd-sleep[26914]: /sys/class/power_supply/BAT1/charge_now                     =   3460 [mAh]
    mer 25 jan 2023 15:52:07 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Wed Jan 25 21:52:08 2023
    mer 25 jan 2023 15:52:07 EST
    mer 25 jan 2023 16:52:16 EST
    Slept for 1.00 hours
    Used 0.42 Wh, an average rate of 0.42 W
    For your 53.28 Wh battery this is 0.78%/hr or 18.73%/day
    jan 25 15:52:08 angela systemd-sleep[30145]: /sys/class/power_supply/BAT1/charge_now                     =   3175 [mAh]
    jan 25 16:52:08 angela systemd-sleep[30435]: /sys/class/power_supply/BAT1/charge_now                     =   3148 [mAh]

It seems like the power usage is lower in `s2idle` with a USB-A
module! In my earlier tests, I stuck with `deep` sleep, because
previous results seem to show it had better power usage. My baseline
tests also confirmed this, but I did not test *with* expansion cards
plugged in. So it could very well be that `s2idle` is actually more
efficient in the general usage case.

Doing the test again shows it's reproducible:

    root@angela:/home/anarcat# /opt/batterylog/batterylog.py ; journalctl -b | grep charge_now | tail -2
    Slept for 2.25 hours
    Used 0.75 Wh, an average rate of 0.34 W
    For your 53.28 Wh battery this is 0.63%/hr or 15.14%/day
    jan 25 16:53:26 angela systemd-sleep[31636]: /sys/class/power_supply/BAT1/charge_now                     =   3138 [mAh]
    jan 25 19:08:09 angela systemd-sleep[31868]: /sys/class/power_supply/BAT1/charge_now                     =   3089 [mAh]
    root@angela:/home/anarcat#

... in fact, this second test shows a *lower* wattage use, which is
strange. But that test was done in rather exotic conditions: I was
traveling home with the laptop, in a bag, in sub-zero temperatures,
instead of just leaving it open on my desk... So to take with a grain
of salt.

# normal setup in s2idle: 0.88W, 57mA, 2.5 days

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2 ; date ;    rtcwake -m no -s 3600 && systemctl suspend ; date ;    sleep 10 ; date ;    
    /opt/batterylog/batterylog.py ;     journalctl -b | grep charge_now | tail -2
    jan 25 16:53:26 angela systemd-sleep[31636]: /sys/class/power_supply/BAT1/charge_now                     =   3138 [mAh]
    jan 25 19:08:09 angela systemd-sleep[31868]: /sys/class/power_supply/BAT1/charge_now                     =   3089 [mAh]
    mer 25 jan 2023 19:45:41 EST
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup using /dev/rtc0 at Thu Jan 26 01:45:42 2023
    mer 25 jan 2023 19:45:41 EST
    mer 25 jan 2023 20:45:50 EST
    Slept for 1.00 hours
    Used 0.88 Wh, an average rate of 0.88 W
    For your 53.28 Wh battery this is 1.65%/hr or 39.54%/day
    jan 25 19:45:42 angela systemd-sleep[44135]: /sys/class/power_supply/BAT1/charge_now                     =   2770 [mAh]
    jan 25 20:45:42 angela systemd-sleep[44426]: /sys/class/power_supply/BAT1/charge_now                     =   2713 [mAh]
