[[!toc levels=3]]

# Test procedure

 1. raise DPMS timeout to avoid screen saver skewing results:
 
        xset 600 3

 2. prepare the test (disable/enable radios, plug in module, etc)

 3. set desired brightness, run test, reset brightness
 
        sudo true; xbacklight -set 0 ; sudo powerstat ; xbacklight -set 100

We run `sudo` before changing the backlight to make sure the `sudo`
token is still recorded and the second `sudo` won't prompt us while
the screen is blank.

Note that those tests were performed in full multi-user mode, in a
graphical Xorg session. Further tests [performed in single-user
mode](https://community.frame.work/t/intel-12th-gen-frame-work-laptop-consumer-reviews/22123/8?u=anarcat) showed that single user mode saves about 300-400mW of power
from the full session.

Other folks have different test procedures. Hrishi Olickel worked on a
[method][] based on [GNOME battery bench][] which is interesting for
real-life, actual work loads. The tests here are only *idle*
workloads, which have limited applications for real life situations,
but do show a good baseline for power consumption.

See also the [[powerstat-306]] page for results after the 3.06 BIOS
beta release and [[powerstat-wayland]] page for results done on
[[software/desktop/wayland]] (instead of Xorg, here).

[method]: https://olickel.com/benchmarking-the-battery-on-an-ubuntu-machine
[GNOME battery bench]: https://blog.fishsoup.net/2015/01/15/gnome-battery-bench/

# Research hypothesis and bias

My bias is to interpret results based on the "minimum": that's the
ground, base noise that a new module (for example) would
introduce. 

The [powerstat](https://github.com/ColinIanKing/powerstat) tool is used here. It does, by default, 30 samples
every 10 seconds (so for 5 minutes) with a 3 minute "settle down"
period before. That settle-down period ensures the machine spins back
down to idle so there's less be variation based on CPU usage or
frequency scaling.

If there's a result that's really out of whack, it will be rerun to
see if we can reproduce.

I originally forgot to turn off radios. I was assuming radios would
take power and introduce variability, so all tests were reran with
radios turned off, basically with the equivalent of `rfkill block
all`, but done with the handy <kbd>fn-F10</kbd> keybinding which puts
the laptop in "airplane mode".

The hypothesis is that the screen, WiFi, and *some* modules take
power. Specifically, I heard that USB-C and DisplayPort modules do
*not* take power while all the others take some power, with the SSD
and MicroSD cards taking up to 1 to 5W.

# Radio tests

Those tests were specifically done to see if turning on Bluetooth (not
connected to anything) and WiFi (connected to a WPA access point)
were taking power.

The "radios on" tests here were actually done as part of the normal
batch (below) and were retroactively added back here.

## 100% brightness, no modules

### no radios

This is the same test result as the "100% brightness, no radios, no
modules" test below.

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1102.5  288.9 85.9 21.7 86.4   4.70
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1098.1  287.8 83.5 19.2 83.9   4.70
      StdDev   0.0   0.0   0.0   0.1   0.0  0.2   99.9   26.8 25.5 15.2 26.6   0.06
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.3   0.0  1.0  919.7  250.9 73.0 16.0 73.0   4.61
     Maximum   0.2   0.2   0.2  99.8   0.1  2.0 1307.9  366.7 196.0 87.0 195.0   4.87
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   4.70 Watts on average with standard deviation 0.06

### radios on, +0.03W min, +-0W average, +0.01W stdev

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.1 1073.7  276.0 83.3 20.5 84.3   4.70
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1067.2  275.0 81.8 18.9 82.7   4.70
      StdDev   0.0   0.0   0.0   0.1   0.0  0.4  117.1   23.2 20.3 12.3 20.9   0.07
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.5   0.0  1.0  863.2  233.8 72.0 16.0 72.0   4.58
     Maximum   0.2   0.2   0.2  99.8   0.1  3.0 1261.5  314.4 186.0 80.0 185.0   4.88
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   4.70 Watts on average with standard deviation 0.07

## minimum brightness, no modules

### no radios

This is the same test result as the "minimum brightness, no radios, no
modules" test below.

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.1 1036.1  275.0 82.5 20.5 82.6   2.24
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1031.4  274.0 81.0 18.8 81.0   2.24
      StdDev   0.0   0.0   0.0   0.1   0.0  0.4  100.4   23.2 20.6 12.7 20.5   0.05
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.5   0.0  1.0  875.4  241.7 72.0 16.0 72.0   2.13
     Maximum   0.2   0.2   0.2  99.8   0.1  3.0 1317.4  342.2 186.0 80.0 186.0   2.35
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.24 Watts on average with standard deviation 0.05

### radios: +0.29W min, +0.25W average, +0.25W stdev

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.2   0.0   0.1  99.6   0.0  1.0 1540.7  361.6 82.8 18.6 83.1   2.49
     GeoMean   0.2   0.0   0.1  99.6   0.0  1.0 1527.3  351.4 82.0 17.8 82.3   2.48
      StdDev   0.0   0.1   0.0   0.1   0.0  0.2  220.0  103.1 14.4  7.4 13.9   0.30
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.2   0.0  1.0 1303.9  282.0 75.0 16.0 76.0   2.30
     Maximum   0.3   0.3   0.3  99.7   0.1  2.0 2219.0  742.5 158.0 56.0 156.0   3.71
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.49 Watts on average with standard deviation 0.30

## screen off, no modules

### no radios

This is the same as the "screen off, no radios, no modules" test below.

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.2   0.0   0.1  99.7   0.0  1.0 1116.0  331.7 82.9 19.7 82.7   2.24
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1100.8  305.4 81.8 18.5 81.7   2.22
      StdDev   0.1   0.0   0.0   0.1   0.0  0.0  208.8  225.2 15.4  9.3 15.1   0.33
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.1   0.0  1.0  849.8  231.8 72.0 16.0 72.0   2.06
     Maximum   0.6   0.2   0.2  99.8   0.1  1.0 2038.4 1535.4 151.0 56.0 151.0   3.81
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.24 Watts on average with standard deviation 0.33

### radios: +0.10W min, +0.25W average, +0.04W stdev

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.2   0.0   0.1  99.6   0.1  1.0 1577.7  377.9 83.5 19.6 83.9   2.47
     GeoMean   0.2   0.0   0.1  99.6   0.0  1.0 1561.8  367.3 82.5 18.5 82.9   2.44
      StdDev   0.0   0.1   0.0   0.1   0.0  0.2  235.6   99.5 15.6  8.9 15.5   0.37
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.2   0.0  1.0 1308.0  287.6 73.0 16.0 74.0   2.16
     Maximum   0.3   0.3   0.3  99.8   0.1  2.0 2201.1  681.6 157.0 56.0 156.0   3.56
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.47 Watts on average with standard deviation 0.37

## screen off, 1 USB-C

### no radios

This is the same test result as the "screen off, no radios, 1 USB-C"
test below.

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1044.4  271.7 81.2 19.6 81.6   2.09
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1037.3  270.8 80.2 18.4 80.6   2.09
      StdDev   0.0   0.0   0.0   0.1   0.0  0.0  126.1   22.7 15.5  9.2 15.4   0.07
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.4   0.0  1.0  813.5  239.0 70.0 16.0 71.0   1.96
     Maximum   0.2   0.2   0.2  99.8   0.1  1.0 1460.2  349.1 153.0 56.0 153.0   2.23
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.09 Watts on average with standard deviation 0.07

### radios: +0.18W min, +0.30W average, +0.34W stdev

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.2   0.0   0.1  99.6   0.0  1.0 1504.4  356.2 80.9 18.4 81.0   2.39
     GeoMean   0.2   0.0   0.1  99.6   0.0  1.0 1485.4  344.7 80.5 17.7 80.6   2.36
      StdDev   0.0   0.1   0.1   0.2   0.0  0.2  273.3  113.8  8.9  6.9  9.0   0.41
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  98.9   0.0  1.0 1237.9  276.5 75.0 16.0 75.0   2.14
     Maximum   0.2   0.4   0.4  99.8   0.2  2.0 2549.2  797.7 125.0 53.0 126.0   3.80
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.39 Watts on average with standard deviation 0.41

## screen off, 2 USB-C

### no radios

This is the same test result as the "screen off, no radios, 2 USB-C"
test below.

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.1 1100.6  282.5 86.9 21.7 86.8   2.09
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.1 1096.0  281.3 84.7 19.6 84.7   2.09
      StdDev   0.0   0.0   0.0   0.1   0.0  0.4  102.6   25.9 24.1 13.5 23.9   0.05
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.5   0.0  1.0  900.8  246.1 73.0 16.0 73.0   1.99
     Maximum   0.2   0.1   0.2  99.8   0.1  3.0 1335.8  365.8 188.0 80.0 187.0   2.19
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.09 Watts on average with standard deviation 0.05

### radios: +0.15W min, +0.44W average, +0.63W stdev
 
      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.2   0.0   0.1  99.5   0.0  1.1 1608.6  388.2 87.4 20.5 87.0   2.53
     GeoMean   0.2   0.0   0.1  99.5   0.0  1.0 1568.0  364.4 84.5 18.8 84.3   2.46
      StdDev   0.4   0.1   0.1   0.5   0.0  0.2  421.7  176.0 32.5 12.6 30.5   0.68
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  96.9   0.0  1.0 1127.3  265.6 72.0 16.0 72.0   2.14
     Maximum   2.2   0.4   0.6  99.8   0.1  2.0 3366.0 1150.2 258.0 83.0 247.0   4.56
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.53 Watts on average with standard deviation 0.68

## screen off, 1 USB-A

### no radios

This is the same test result as the "screen off, no radios, 1 USB-A"
test below.

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1052.7  279.1 81.6 19.6 81.6   2.11
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1049.1  278.3 80.5 18.4 80.6   2.11
      StdDev   0.0   0.0   0.0   0.1   0.0  0.0   88.1   22.5 16.0  9.2 15.6   0.05
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.5   0.0  1.0  881.0  250.4 72.0 16.0 72.0   2.00
     Maximum   0.2   0.2   0.2  99.8   0.1  1.0 1290.1  351.0 158.0 56.0 156.0   2.23
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.11 Watts on average with standard deviation 0.05

### radios: +0.13W min, +0.14W average, +0.05W stdev

## Conclusion: radios take 100mW+

Overall, radios seem to add a minimum of 0.10W (or 100mW) of power
usage to the tests. That's minimal, of course, but it does
significantly increase standard deviation, by about half a watt.

One theory is that an active network connection would trigger network
traffic which in itself means more CPU usage, but it also means
graphical updates as the status bar shows traffic measurements.

So it's good that we redid those tests.

# no radios tests

## 100% brightness, no modules

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1102.5  288.9 85.9 21.7 86.4   4.70
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1098.1  287.8 83.5 19.2 83.9   4.70
      StdDev   0.0   0.0   0.0   0.1   0.0  0.2   99.9   26.8 25.5 15.2 26.6   0.06
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.3   0.0  1.0  919.7  250.9 73.0 16.0 73.0   4.61
     Maximum   0.2   0.2   0.2  99.8   0.1  2.0 1307.9  366.7 196.0 87.0 195.0   4.87
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   4.70 Watts on average with standard deviation 0.06

## minimum brightness, no modules

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.1 1036.1  275.0 82.5 20.5 82.6   2.24
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1031.4  274.0 81.0 18.8 81.0   2.24
      StdDev   0.0   0.0   0.0   0.1   0.0  0.4  100.4   23.2 20.6 12.7 20.5   0.05
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.5   0.0  1.0  875.4  241.7 72.0 16.0 72.0   2.13
     Maximum   0.2   0.2   0.2  99.8   0.1  3.0 1317.4  342.2 186.0 80.0 186.0   2.35
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.24 Watts on average with standard deviation 0.05

## screen off

### no modules

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.2   0.0   0.1  99.7   0.0  1.0 1116.0  331.7 82.9 19.7 82.7   2.24
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1100.8  305.4 81.8 18.5 81.7   2.22
      StdDev   0.1   0.0   0.0   0.1   0.0  0.0  208.8  225.2 15.4  9.3 15.1   0.33
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.1   0.0  1.0  849.8  231.8 72.0 16.0 72.0   2.06
     Maximum   0.6   0.2   0.2  99.8   0.1  1.0 2038.4 1535.4 151.0 56.0 151.0   3.81
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.24 Watts on average with standard deviation 0.33

### conclusion: screen power usage between 70mW to 2.55W

The monitor seems surprisingly effectively, especially at the minimum
brightness, which is almost indistinguishable from the "screen off"
power usage.

We need to look at the "minimum" stats to see a different: there we see
that the minimum brightness might take 70mW (2.13W-2.06W), while the
full brightness takes a more solid 2.55W.

One question that remains is: why is the above result *lower* than the
USB-C result below? Normally, it should be the same or lower... Note,
however, that we have higher standard deviation above, so it could be
outliers messing with the numbers.

### 1 USB-C

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1044.4  271.7 81.2 19.6 81.6   2.09
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1037.3  270.8 80.2 18.4 80.6   2.09
      StdDev   0.0   0.0   0.0   0.1   0.0  0.0  126.1   22.7 15.5  9.2 15.4   0.07
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.4   0.0  1.0  813.5  239.0 70.0 16.0 71.0   1.96
     Maximum   0.2   0.2   0.2  99.8   0.1  1.0 1460.2  349.1 153.0 56.0 153.0   2.23
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.09 Watts on average with standard deviation 0.07

### 2 USB-C

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.1 1100.6  282.5 86.9 21.7 86.8   2.09
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.1 1096.0  281.3 84.7 19.6 84.7   2.09
      StdDev   0.0   0.0   0.0   0.1   0.0  0.4  102.6   25.9 24.1 13.5 23.9   0.05
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.5   0.0  1.0  900.8  246.1 73.0 16.0 73.0   1.99
     Maximum   0.2   0.1   0.2  99.8   0.1  3.0 1335.8  365.8 188.0 80.0 187.0   2.19
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.09 Watts on average with standard deviation 0.05

### conclusion: USB-C cards take little to no power

It looks like the theory that USB-C cards are passive is valid. From
the above, we *may* have a 30mW power drain between one and two USB-C
cards, but that is within the standard deviation (50mW).

### 1 USB-A

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1052.7  279.1 81.6 19.6 81.6   2.11
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1049.1  278.3 80.5 18.4 80.6   2.11
      StdDev   0.0   0.0   0.0   0.1   0.0  0.0   88.1   22.5 16.0  9.2 15.6   0.05
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.5   0.0  1.0  881.0  250.4 72.0 16.0 72.0   2.00
     Maximum   0.2   0.2   0.2  99.8   0.1  1.0 1290.1  351.0 158.0 56.0 156.0   2.23
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.11 Watts on average with standard deviation 0.05

### 2 USB-A

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1073.5  289.0 80.1 18.7 80.0   2.12
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1069.3  288.1 79.8 18.0 79.6   2.12
      StdDev   0.0   0.0   0.0   0.1   0.0  0.2   95.4   23.6  7.9  6.4  8.0   0.05
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.4   0.0  1.0  869.8  246.3 72.0 16.0 72.0   2.01
     Maximum   0.2   0.2   0.2  99.8   0.1  2.0 1270.1  356.5 116.0 49.0 117.0   2.25
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.12 Watts on average with standard deviation 0.05

### 3 USB-A

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1074.0  286.1 81.7 19.8 82.0   2.13
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1067.8  284.9 80.6 18.6 80.9   2.13
      StdDev   0.0   0.0   0.0   0.1   0.0  0.2  119.1   26.5 15.8  9.2 15.8   0.06
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.5   0.0  1.0  910.8  240.9 73.0 16.0 73.0   2.03
     Maximum   0.2   0.2   0.2  99.8   0.1  2.0 1338.3  347.9 156.0 56.0 156.0   2.27
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.13 Watts on average with standard deviation 0.06

### conclusion: USB-A cards may use 10mW or negligible

This looks like a fairly reliable pattern: adding a USB-A card adds
0.01W of power usage, both in minimum and average power
consumption. That is within the standard deviation however, which
doesn't really change by adding more modules, so it's likely the USB-A
ports use very little power as well.

### setting baseline at 2.10W

It's hard to establish a baseline power usage for those tests, because
they actually fluctuate a lot, even between similar test
environments. 

For example, the first few tests in this "screen off" section seem to
show USB-C cards use *less* power than no cards at all. And further
powerstat runs are having trouble reproducing the 1.96W minimum
encountered above.

Let's set a 2.10W baseline and call it that.

### 1TB SSD: 1.5-2W

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1127.2  283.4 87.5 21.7 87.9   3.89
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1123.0  282.6 85.4 19.6 85.8   3.89
      StdDev   0.0   0.0   0.0   0.1   0.0  0.2  100.6   22.0 24.0 13.5 23.9   0.18
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.5   0.0  1.0  944.1  255.1 75.0 16.0 75.0   3.75
     Maximum   0.2   0.1   0.2  99.8   0.1  2.0 1423.3  356.3 188.0 80.0 188.0   4.49
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   3.89 Watts on average with standard deviation 0.18

This is at least 1.65W above the baseline, with 1.79W on average and a
max 2W power usage.

This is just with the SSD plugged in, detected by the kernel, but not
mounted or active in any way.

### 1 MicroSD: 1.6W-6W!!

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   1.3   0.1  98.4   0.0  1.3 1169.5  375.8 85.3 19.5 86.0   5.00
     GeoMean   0.1   0.0   0.1  98.4   0.0  1.2 1165.5  351.3 84.0 18.1 84.6   4.69
      StdDev   0.0   2.5   0.0   2.5   0.0  0.5  102.5  155.2 19.4 11.6 19.5   1.99
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  93.5   0.0  1.0 1076.2  264.7 78.0 16.0 79.0   3.65
     Maximum   0.2   6.3   0.1  99.8   0.1  3.0 1570.5  719.4 188.0 80.0 188.0   8.71
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   5.00 Watts on average with standard deviation 1.99

There is also a 1.6W power drain minimum here, but look at the
maximum: that's over 6 watts used! On *average* we have a 3 watt power
drain here, with a very large standard deviation (1.99)!

This is the most power-hungry module that I have tested, by far.

### 1 HDMI: ~500mW

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.2   0.0   0.1  99.7   0.0  1.0 1151.7  287.7 86.2 20.5 86.4   2.86
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1148.1  286.2 84.6 18.6 84.8   2.86
      StdDev   0.0   0.0   0.0   0.1   0.0  0.0   93.0   29.5 21.6 13.8 21.3   0.10
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.4   0.0  1.0 1036.3  249.6 78.0 16.0 78.0   2.74
     Maximum   0.2   0.2   0.2  99.8   0.1  1.0 1376.2  365.8 195.0 87.0 194.0   3.18
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.86 Watts on average with standard deviation 0.10

This seems to confirm the hypothesis that the HDMI modules are *not*
passive. But let's try with two to be certain.

### 2 HDMI

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.2   0.0   0.1  99.6   0.0  1.0 1161.9  306.0 89.6 21.5 89.5   3.30
     GeoMean   0.2   0.0   0.1  99.6   0.0  1.0 1141.3  295.9 84.1 18.3 84.3   3.29
      StdDev   0.4   0.0   0.1   0.5   0.0  0.0  279.7  111.4 51.4 23.4 49.6   0.29
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  97.2   0.0  1.0  963.5  248.3 75.0 16.0 75.0   3.12
     Maximum   2.3   0.0   0.5  99.8   0.1  1.0 2602.5  892.2 366.0 147.0 356.0   4.60
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   3.30 Watts on average with standard deviation 0.29

Here we add another 440mW on average, 380mW on minimum, and an extra
190mW of standard deviation.

Clearly the HDMI cards do use some power.

### 1 DisplayPort: 300mW

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1120.7  282.4 84.0 19.6 84.7   2.49
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1117.2  281.6 83.1 18.4 83.7   2.49
      StdDev   0.0   0.0   0.0   0.1   0.0  0.0   91.0   22.8 15.0  9.2 15.3   0.06
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.4   0.0  1.0 1018.2  260.2 77.0 16.0 77.0   2.41
     Maximum   0.2   0.2   0.2  99.8   0.1  1.0 1354.3  355.8 154.0 56.0 156.0   2.70
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.49 Watts on average with standard deviation 0.06

Here, the result is a little more ambiguous. We're pretty close to the
baseline here, 300mW above the 2.1W baseline we declared above. Still,
it's non-negligible so I'd tend to call this card non-passive because
of that power usage.

### no module again

      Time    User  Nice   Sys  Idle    IO  Run Ctxt/s  IRQ/s Fork Exec Exit  Watts
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.1   0.0   0.1  99.7   0.0  1.0 1122.2  282.4 83.7 19.6 83.8   2.16
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0 1117.8  281.5 82.7 18.4 82.8   2.16
      StdDev   0.0   0.0   0.0   0.1   0.0  0.2  101.3   23.3 15.7  9.2 15.4   0.06
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.1   0.0   0.1  99.4   0.0  1.0  969.0  248.4 74.0 16.0 74.0   2.06
     Maximum   0.2   0.2   0.2  99.8   0.1  2.0 1308.8  342.4 158.0 56.0 156.0   2.32
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
    Summary:
    System:   2.16 Watts on average with standard deviation 0.06

This should normally be the same as the first test in this
section. But the average differs, by about 80mW. This is pretty close
to the standard deviation so it *could* be considered part of the
margin of error for those measurements.

What is confusing is this measurement and the related one above is
that they are *both* higher than the USB-C tests, which both have a
lower average (2.09W exactly) and minimums (1.96W and 1.99W).

So there are clearly some uncontrolled variables in my tests.
