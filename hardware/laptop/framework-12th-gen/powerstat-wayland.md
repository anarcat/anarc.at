# Baseline research

This is an attempt to reproduce the results from the [[powerstat]]
page under my current environment.

To do those tests, I ran my normal session in Sway, with Waybar,
network manager, and so on. Syncthing was stopped and Docker was not
installed (podman is, but doesn't keep a daemon around).

Those tests were all performed before the 3.06 BIOS update, which is
supposed to improve power usage.

The procedure here is slightly different than the original
[[powerstat]] procedure:

 1. disable `swayidle` to keep the screensaver from starting:

        systemctl --user stop swayidle

 2. prepare the test

 3. set desired brightness, run test, reset brightness

        sudo true; brightnessctl set 0 ; sudo powerstat ; brightnessctl set 50

See the [[powerstat]] page for an in-depth discussion of the test
procedure.

See the [[powerstat-306]] page for results after the BIOS 3.06 beta
release.

## screen off, no modules, radios

First test:

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.0   0.0   0.1  99.7   0.1  1.0  623.1  285.4  6.0  3.6  6.5   4.05 
     GeoMean   0.0   0.0   0.1  99.7   0.1  1.0  589.4  268.9  0.0  0.0  0.0   4.04 
      StdDev   0.0   0.1   0.1   0.2   0.0  0.0  249.5  120.2 15.3  9.1 16.2   0.37 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.2   0.0  1.0  457.2  214.5  0.0  0.0  0.0   3.82 
     Maximum   0.1   0.2   0.3  99.8   0.2  1.0 1343.2  645.4 72.0 37.0 77.0   5.26 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
    Summary:
    System:   4.05 Watts on average with standard deviation 0.37

This seems to be vastly different than the baseline (2.10W) from
[[powerstat]]...

## screen off, no modules, no radios

Strangely, we are saving 700mW here, which is unexpected:

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.0   0.0   0.1  99.9   0.0  1.0  417.0  228.4  3.2  2.3  3.5   3.33 
     GeoMean   0.0   0.0   0.1  99.9   0.0  1.0  413.9  227.7  0.0  0.0  0.0   3.33 
      StdDev   0.0   0.0   0.0   0.1   0.0  0.0   54.2   18.7  7.8  6.3  8.1   0.06 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.5   0.0  1.0  354.7  199.3  0.0  0.0  0.0   3.25 
     Maximum   0.1   0.2   0.2  99.9   0.1  1.0  633.0  296.6 41.0 33.0 43.0   3.48 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

## screen off, no modules, single user mode

To see if we could reach the baseline, we try again in single user
mode (`systemctl isolate rescue ; pkill -u anarcat`):

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Average   0.0   0.0   0.0 100.0   0.0  1.0   38.9   22.4  0.0  0.0  0.0   2.15
     GeoMean   0.0   0.0   0.0 100.0   0.0  1.0   38.8   22.3  0.0  0.0  0.0   2.15
      StdDev   0.0   0.0   0.0   0.0   0.0  0.0    2.1    2.0  0.0  0.0  0.2   0.01
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
     Minimum   0.0   0.0   0.0 100.0   0.0  1.0   35.3   19.7  0.0  0.0  0.0   2.13
     Maximum   0.0   0.0   0.0 100.0   0.0  1.0   42.5   27.0  0.0  0.0  1.0   2.16
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

So it seems we *can* reach the baseline described in the forum
post. This would mean that Sway/Wayland uses much more power than the
previous Xorg setup which is disconcerting.

Sway, in particular, seems to use 1-2% of a CPU core, all the time,
which is bound to trigger some nasty power usage. In this case, it
seems to be close to 2W.

## Baseline: screen off, no modules or radio

After dropping to `rescue.target`, we redid the test a few times to
try to converge over a baseline.

Those tests were performed in a full multi-user mode Wayland graphical
session, with this setup:

    systemctl stop swayidle syncthing
    sudo rfkill block

And then:

    brightnessctl set 0% ; sudo powerstat ; brightnessctl set 50%

4 tests were ran, a few seconds apart:

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.0   0.0   0.1  99.9   0.0  1.0  315.5  180.3  6.4  4.4  6.7   2.38 
     GeoMean   0.0   0.0   0.1  99.9   0.0  1.0  312.4  179.6  0.0  0.0  0.0   2.38 
      StdDev   0.0   0.0   0.0   0.1   0.0  0.0   51.1   16.5 20.3 12.7 20.1   0.04 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.5   0.0  1.0  288.8  161.0  0.0  0.0  0.0   2.33 
     Maximum   0.0   0.2   0.2  99.9   0.1  1.0  551.8  251.4 108.0 64.0 107.0   2.53 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.1   0.0   0.1  99.7   0.0  1.2  427.2  252.9 17.8 10.5 17.5   2.59 
     GeoMean   0.0   0.0   0.1  99.7   0.0  1.1  349.6  214.3  0.0  0.0  0.0   2.55 
      StdDev   0.5   0.0   0.2   0.8   0.0  0.5  430.5  221.1 86.6 50.3 85.2   0.54 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  95.6   0.0  1.0  284.3  174.4  0.0  0.0  0.0   2.28 
     Maximum   3.1   0.0   1.2  99.9   0.1  3.0 2253.9 1063.5 484.0 281.0 476.0   4.85 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.0   0.0   0.1  99.8   0.0  1.0  312.9  191.1  5.3  3.5  5.4   2.34 
     GeoMean   0.0   0.0   0.1  99.8   0.0  1.0  310.1  190.3  0.0  0.0  0.0   2.34 
      StdDev   0.0   0.0   0.0   0.1   0.0  0.2   47.7   19.0 14.4  8.7 14.4   0.07 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.5   0.0  1.0  285.0  171.8  0.0  0.0  0.0   2.25 
     Maximum   0.1   0.2   0.2  99.9   0.1  2.0  529.7  266.0 72.0 36.0 72.0   2.50 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.0   0.0   0.1  99.8   0.0  1.1  315.5  179.6  8.8  5.6  8.9   2.38 
     GeoMean   0.0   0.0   0.1  99.8   0.0  1.1  311.4  178.3  0.0  0.0  0.0   2.38 
      StdDev   0.0   0.0   0.0   0.1   0.0  0.3   57.6   23.0 25.6 15.5 25.5   0.06 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.5   0.0  1.0  283.9  158.7  0.0  0.0  0.0   2.31 
     Maximum   0.1   0.2   0.2  99.9   0.1  2.0  517.9  255.1 117.0 71.0 117.0   2.60 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

Of all of those, the "minimum minimum" was 2.25 averages fluctuated
but a good mark seems to be 2.38W. stdev is around 60mW. Last time we
picked 2.1W as a baseline, so let's pick, also arbitrarily, 2.3W as a
baseline with the understanding the actual minimum is somewhere around
2.25W...

This baseline is close enough to the original Xorg baseline to
disprove the theory that Wayland is using more power. It's also stable
enough (±60mW minimum) to be used as a baseline.

# Module tests

Those tests were performed with rfkill, no syncthing, no swayidle,
screen turned off.

## USB-C

Just another control:

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.0   0.0   0.1  99.9   0.0  1.1  307.7  185.0  7.6  4.5  7.8   2.36 
     GeoMean   0.0   0.0   0.1  99.9   0.0  1.1  306.0  184.7  0.0  0.0  0.0   2.36 
      StdDev   0.0   0.0   0.0   0.0   0.0  0.3   36.1   11.4 22.8 12.9 22.5   0.05 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.7   0.0  1.0  286.1  173.8  0.0  0.0  0.0   2.27 
     Maximum   0.1   0.0   0.1  99.9   0.1  2.0  450.5  225.4 108.0 64.0 107.0   2.45 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

Close enough to baseline to call the "USB-C uses no power" reproducible.

## USB-A

Control test again:

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.1   0.0   0.1  99.7   0.0  1.1  372.1  213.5 19.3 11.7 19.1   2.60 
     GeoMean   0.0   0.0   0.1  99.7   0.0  1.0  332.9  196.7  0.0  0.0  0.0   2.56 
      StdDev   0.5   0.0   0.2   0.7   0.0  0.2  298.9  145.3 86.4 50.4 83.3   0.55 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  95.7   0.0  1.0  286.4  170.0  0.0  0.0  0.0   2.34 
     Maximum   3.0   0.2   1.2  99.9   0.1  2.0 1946.5  990.3 483.0 281.0 466.0   4.74 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

Also confirms the previous research results of "USB-A uses more power".

## Ethernet

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.0   0.0   0.1  99.9   0.0  1.0  314.3  192.4  8.8  5.6  9.2   3.93 
     GeoMean   0.0   0.0   0.1  99.9   0.0  1.0  311.2  191.6  0.0  0.0  0.0   3.93 
      StdDev   0.0   0.0   0.0   0.1   0.0  0.0   50.2   18.9 23.4 13.9 23.1   0.04 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.5   0.0  1.0  287.2  177.6  0.0  0.0  0.0   3.89 
     Maximum   0.1   0.2   0.2  99.9   0.1  1.0  514.7  269.4 108.0 64.0 107.0   4.06 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

Seems to be about 1.6W minimum.

TODO: Test with link, with load.

## USB-C, keyboard backlight off

Oops. Noticed that all tests above were done with the keyboard
backlight turned on. Redid the test without:

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.0   0.0   0.1  99.9   0.0  1.9  301.4  183.7  2.8  2.3  2.8   1.94 
     GeoMean   0.0   0.0   0.1  99.9   0.0  1.8  299.9  183.3  0.0  0.0  0.0   1.94 
      StdDev   0.0   0.0   0.0   0.0   0.0  0.6   33.8   12.2  5.5  4.6  5.5   0.04 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.7   0.0  1.0  277.2  165.5  0.0  0.0  0.0   1.89 
     Maximum   0.1   0.1   0.1  99.9   0.1  3.0  469.8  225.4 20.0 18.0 21.0   2.07 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

Looks like the keyboard backlight takes 300-400mW!! Quite significant!

That said, that doesn't invalidate the Ethernet card results, which is
what I was looking for.

# Backlight tests

Let's see how much power the keyboard backlight drains...

## screen off, no cards, no radios, backlight off

Another control test, to re-establish the baseline:

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.1   0.0   0.1  99.7   0.0  1.0  459.6  262.4  7.1  4.8  7.5   2.50 
     GeoMean   0.0   0.0   0.1  99.7   0.0  1.0  426.7  249.5  0.0  0.0  0.0   2.48 
      StdDev   0.1   0.1   0.0   0.2   0.0  0.0  196.0   89.7 15.5 10.2 15.3   0.37 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.3   0.0  1.0  298.6  183.8  0.0  0.0  0.0   2.11 
     Maximum   0.3   0.3   0.3  99.9   0.1  1.0 1046.5  485.4 72.0 38.0 72.0   3.22 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

Decidedly, it seems we're around 2.1W as a baseline.

## backlight level 1: +290mW

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.1   0.0   0.1  99.7   0.0  1.0  520.8  249.4 17.5 13.3 17.2   2.64 
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0  408.7  228.4  0.0  0.0  0.0   2.62 
      StdDev   0.2   0.0   0.1   0.3   0.0  0.0  550.2  131.6 40.6 32.7 38.6   0.28 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  98.7   0.0  1.0  287.7  174.1  0.0  0.0  0.0   2.39 
     Maximum   0.7   0.2   0.4  99.9   0.1  1.0 2719.9  714.9 157.0 129.0 149.0   3.25 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

## level 2: +600mW (+890mW total)

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.2   0.0   0.1  99.7   0.0  1.1  433.4  234.4 24.3 15.0 24.2   3.36 
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.0  373.7  213.5  0.0  0.0  0.0   3.33 
      StdDev   0.5   0.0   0.2   0.8   0.0  0.2  367.4  157.4 76.3 45.9 75.5   0.46 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  95.7   0.0  1.0  279.0  164.7  0.0  0.0  0.0   2.99 
     Maximum   3.0   0.2   1.2  99.9   0.1  2.0 2137.3 1036.6 411.0 245.0 407.0   5.28 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

## level 3: +800mW (+1.69W total)

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.0   0.0   0.1  99.8   0.0  1.2  397.5  227.7  5.5  3.6  5.7   4.07 
     GeoMean   0.0   0.0   0.1  99.8   0.0  1.2  367.6  213.5  0.0  0.0  0.0   4.05 
      StdDev   0.0   0.1   0.0   0.2   0.0  0.4  188.8   96.3 14.3  8.4 14.7   0.39 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.4   0.0  1.0  289.5  169.9  0.0  0.0  0.0   3.79 
     Maximum   0.1   0.2   0.2  99.9   0.1  2.0  980.4  511.4 72.0 36.0 72.0   5.03 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------

So it seems the progression is not linear: the first level takes less
power than the difference between the first and second, for
example. It seems a somewhat geometric progression, which is a useful
thing to remember if you want to save power on that little device.

Another way to put this is: just having the backlight set to minimum
won't hurt your battery life too much, but maxed all the way up, it's
one of the biggest power drains apart from the monitor backlight at
full blast.

## level 0 again

Turning off the backlight brings us back to baseline, as expected:

    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Average   0.1   0.0   0.1  99.7   0.0  1.2  434.9  246.6  5.4  3.4  5.3   2.46 
     GeoMean   0.1   0.0   0.1  99.7   0.0  1.1  402.5  236.7  0.0  0.0  0.0   2.44 
      StdDev   0.0   0.1   0.1   0.2   0.0  0.6  197.4   77.0 14.2  8.2 14.4   0.31 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------ 
     Minimum   0.0   0.0   0.1  99.3   0.0  1.0  292.0  179.0  0.0  0.0  0.0   2.10 
     Maximum   0.1   0.3   0.6  99.9   0.1  4.0  992.3  459.7 72.0 36.0 72.0   3.28 
    -------- ----- ----- ----- ----- ----- ---- ------ ------ ---- ---- ---- ------
