[[!meta title="standby battery tests"]]

[[!toc levels=4]]

# test procedure

I have deployed [batterylog](https://github.com/lhl/batterylog) and [[my own
hack|blog/2022-09-28-suspend-battery-use]] to collect metrics. The
actual procedure is:

    1. resume
    2. add/remove modules, change configuration
    3. suspend for 60 minutes
    4. collect results

At first, the "suspend" was triggered by hitting the power key, which
was reconfigured in `logind.conf` to make systemd suspend the machine
instead of shutdown. The timing is done with a "wall clock" (an
Android phone timer, technically), and the actual delay will vary
between tests. the actual delay used will be the one provided by the
`tlp-stat -b` entry in the log.

We originally set a timer of 5 minutes to expedite the tests, but this
was seeing too little variation in the `tlp-stat -b` output: the first
test led to 2 mAh usage (23mA), but that means the margin of error
(±1mAh) is basically *half* of the measurement. `batterylog` reported
0.36W and it's unclear what its margin of error is.

At one hour, we'd expect 12 times the usage (so 24mAh), and therefore
a more reasonable 4% margin of error.

Eventually, we found out about the `rtcwake` binary (part of
[util-linux](https://tracker.debian.org/pkg/util-linux)) to automate that part of the procedure. At the end,
our test procedure was:

    date ; rtcwake -m no -s 3600 && systemctl suspend ; date ;\
    sleep 10 ; date ; /opt/batterylog/batterylog.py ; \
    journalctl -b | grep charge_now | tail -2

The `mAh` metrics were computed with the excellent [qalculate](https://qalculate.github.io/) with
a formula like this for the simple, ~1h tests:

    > (2205mAh−2150mAh)/3601s

      (((2205 * meter) * ampère * heure) - ((2150 * meter) * ampère * heure)) /
      (3601 * seconde) =
      approx. 54,984726 A*m

For longer tests, some creative time diffs were done because qalculate
doesn't support complicated calendar calculations. This run, for
example:

    oct 12 20:19:01 angela systemd-sleep[149720]: /sys/class/power_supply/BAT1/charge_now                     =   1836 [mAh]
    oct 13 08:52:54 angela systemd-sleep[150028]: /sys/class/power_supply/BAT1/charge_now                     =   1636 [mAh]

... would result in the formula:

    > (1836mAh-1636mAh)/((20h+52min+54s)-(8h+19min+1s))

    (((1836 * meter) * ampère * heure) - ((1636 * meter) * ampère * heure)) /
    (((20 * heure) + (52 * minute) + (54 * seconde)) - ((8 * heure) + (19 *
    minute) + (1 * seconde))) =
    approx. 15,917582 A*m

Tests were performed on a 12th gen Framework laptop with the [latest
BIOS](https://knowledgebase.frame.work/en_us/framework-laptop-bios-and-driver-releases-12th-gen-intel-core-Bkx2kosqq) (3.05, as of 2022-10-13), running Debian bookworm, with
`powerstat --autotune`, and `tlp.service` enabled. Other tweaks might
be relevant here, but see the [[full
review|hardware/laptop/framework-12th-gen]] on things that were done
to this laptop.

## a note on turbostat

I've seen elsewhere people using turbostat to profile the suspend
mode, but I couldn't make this work.

This, for example, doesn't give the results I'd expect:

    root@angela:/home/anarcat# turbostat --show GFX%rc6,Pkg%pc2,Pkg%pc3,Pkg%pc6,Pkg%pc7,Pkg%pc8,Pkg%pc9,Pk%pc10,SYS%LPI rtcwake -m freeze -s 3600 ; /opt/batterylog/batterylog.py ; journalctl -b | grep charge_now | tail -2 ;\
    > turbostat --show GFX%rc6,Pkg%pc2,Pkg%pc3,Pkg%pc6,Pkg%pc7,Pkg%pc8,Pkg%pc9,Pk%pc10,SYS%LPI systemctl suspend ; /opt/batterylog/batterylog.py ; journalctl -b | grep charge_now | tail -2
    turbostat version 2022.04.16 - Len Brown <lenb@kernel.org>
    CPUID(0): GenuineIntel 0x20 CPUID levels
    CPUID(1): family:model:stepping 0x6:9a:3 (6:154:3) microcode 0x421
    CPUID(0x80000000): max_extended_levels: 0x80000008
    CPUID(1): SSE3 MONITOR SMX EIST TM2 TSC MSR ACPI-TM HT TM
    CPUID(6): APERF, TURBO, DTS, PTM, HWP, HWPnotify, HWPwindow, HWPepp, HWPpkg, EPB
    cpu4: MSR_IA32_MISC_ENABLE: 0x00850089 (TCC EIST MWAIT PREFETCH TURBO)
    CPUID(7): No-SGX
    CPUID(0x15): eax_crystal: 2 ebx_tsc: 110 ecx_crystal_hz: 38400000
    TSC: 2112 MHz (38400000 Hz * 110 / 2 / 1000000)
    CPUID(0x16): base_mhz: 2100 max_mhz: 4400 bus_mhz: 100
    cpu4: MSR_MISC_PWR_MGMT: 0x000030c2 (ENable-EIST_Coordination ENable-EPB DISable-OOB)
    RAPL: 9362 sec. Joule Counter Range, at 28 Watts
    cpu4: MSR_PLATFORM_INFO: 0x804043df0811500
    4 * 100.0 = 400.0 MHz max efficiency frequency
    21 * 100.0 = 2100.0 MHz base frequency
    cpu4: MSR_IA32_POWER_CTL: 0x00e4005b (C1E auto-promotion: ENabled)
    cpu4: MSR_TURBO_RATIO_LIMIT: 0x2323232323252c2c
    35 * 100.0 = 3500.0 MHz max turbo 8 active cores
    35 * 100.0 = 3500.0 MHz max turbo 7 active cores
    35 * 100.0 = 3500.0 MHz max turbo 6 active cores
    35 * 100.0 = 3500.0 MHz max turbo 5 active cores
    35 * 100.0 = 3500.0 MHz max turbo 4 active cores
    37 * 100.0 = 3700.0 MHz max turbo 3 active cores
    44 * 100.0 = 4400.0 MHz max turbo 2 active cores
    44 * 100.0 = 4400.0 MHz max turbo 1 active cores
    cpu4: MSR_CONFIG_TDP_NOMINAL: 0x00000011 (base_ratio=17)
    cpu4: MSR_CONFIG_TDP_LEVEL_1: 0x000e00a0 (PKG_MIN_PWR_LVL1=0 PKG_MAX_PWR_LVL1=0 LVL1_RATIO=14 PKG_TDP_LVL1=160)
    cpu4: MSR_CONFIG_TDP_LEVEL_2: 0x00150118 (PKG_MIN_PWR_LVL2=0 PKG_MAX_PWR_LVL2=0 LVL2_RATIO=21 PKG_TDP_LVL2=280)
    cpu4: MSR_CONFIG_TDP_CONTROL: 0x00000000 ( lock=0)
    cpu4: MSR_TURBO_ACTIVATION_RATIO: 0x00000010 (MAX_NON_TURBO_RATIO=16 lock=0)
    cpu4: MSR_PKG_CST_CONFIG_CONTROL: 0x74008008 (UNdemote-C1, demote-C1, locked, pkg-cstate-limit=8 (unlimited))
    /dev/cpu_dma_latency: 2000000000 usec (default)
    current_driver: intel_idle
    current_governor: menu
    current_governor_ro: menu
    cpu4: POLL: CPUIDLE CORE POLL IDLE
    cpu4: C1E: MWAIT 0x01
    cpu4: C6: MWAIT 0x20
    cpu4: C8: MWAIT 0x40
    cpu4: C10: MWAIT 0x60
    cpu4: cpufreq driver: intel_pstate
    cpu4: cpufreq governor: powersave
    cpufreq intel_pstate no_turbo: 0
    cpu4: MSR_MISC_FEATURE_CONTROL: 0x00000000 (L2-Prefetch L2-Prefetch-pair L1-Prefetch L1-IP-Prefetch)
    cpu0: MSR_PM_ENABLE: 0x00000001 (HWP)
    cpu0: MSR_HWP_CAPABILITIES: 0x01111638 (high 56 guar 22 eff 17 low 1)
    cpu0: MSR_HWP_REQUEST: 0xc0003806 (min 6 max 56 des 0 epp 0xc0 window 0x0 pkg 0x0)
    cpu0: MSR_HWP_REQUEST_PKG: 0x8000ff01 (min 1 max 255 des 0 epp 0x80 window 0x0)
    cpu0: MSR_HWP_INTERRUPT: 0x00000001 (EN_Guaranteed_Perf_Change, Dis_Excursion_Min)
    cpu0: MSR_HWP_STATUS: 0x00000000 (No-Guaranteed_Perf_Change, No-Excursion_Min)
    cpu0: EPB: 7 (custom)
    cpu0: MSR_RAPL_POWER_UNIT: 0x000a0e03 (0.125000 Watts, 0.000061 Joules, 0.000977 sec.)
    cpu0: MSR_PKG_POWER_INFO: 0x000000e0 (28 W TDP, RAPL 0 - 0 W, 0.000000 sec.)
    cpu0: MSR_PKG_POWER_LIMIT: 0x42820000dd80f0 (UNlocked)
    cpu0: PKG Limit #1: ENabled (30.000 Watts, 28.000000 sec, clamp ENabled)
    cpu0: PKG Limit #2: ENabled (64.000 Watts, 0.002441* sec, clamp DISabled)
    cpu0: MSR_VR_CURRENT_CONFIG: 0x000002d0
    cpu0: PKG Limit #4: 90.000000 Watts (UNlocked)
    cpu0: MSR_DRAM_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: DRAM Limit: DISabled (0.000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_PP0_POLICY: 9
    cpu0: MSR_PP0_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: Cores Limit: DISabled (0.000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_PP1_POLICY: 13
    cpu0: MSR_PP1_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: GFX Limit: DISabled (0.000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_IA32_TEMPERATURE_TARGET: 0x80640000 (100 C)
    cpu0: MSR_IA32_PACKAGE_THERM_STATUS: 0x88420000 (34 C)
    cpu0: MSR_IA32_PACKAGE_THERM_INTERRUPT: 0x02000003 (100 C, 100 C)
    cpu4: MSR_PKGC3_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu4: MSR_PKGC6_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu4: MSR_PKGC7_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu4: MSR_PKGC8_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu4: MSR_PKGC9_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu4: MSR_PKGC10_IRTL: 0x00000000 (NOTvalid, 0 ns)
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup from "freeze" using /dev/rtc0 at Wed Oct 12 03:03:31 2022
    3601.483927 sec
    GFX%rc6 Pkg%pc2 Pkg%pc3 Pkg%pc6 Pkg%pc7 Pkg%pc8 Pkg%pc9 Pk%pc10 SYS%LPI
    15.63   0.03    0.01    0.00    0.00    0.00    0.00    100.50  99.80
    15.63   0.03    0.01    0.00    0.00    0.00    0.00    100.50  99.80
    Slept for 1.01 hours
    Used 0.75 Wh, an average rate of 0.75 W
    For your 54.53 Wh battery this is 1.38%/hr or 33.03%/day
    oct 11 20:19:44 angela systemd-sleep[12975]: /sys/class/power_supply/BAT1/charge_now                     =   3284 [mAh]
    oct 11 21:20:04 angela systemd-sleep[13222]: /sys/class/power_supply/BAT1/charge_now                     =   3235 [mAh]
    turbostat version 2022.04.16 - Len Brown <lenb@kernel.org>
    CPUID(0): GenuineIntel 0x20 CPUID levels
    CPUID(1): family:model:stepping 0x6:9a:3 (6:154:3) microcode 0x421
    CPUID(0x80000000): max_extended_levels: 0x80000008
    CPUID(1): SSE3 MONITOR SMX EIST TM2 TSC MSR ACPI-TM HT TM
    CPUID(6): APERF, TURBO, DTS, PTM, HWP, HWPnotify, HWPwindow, HWPepp, HWPpkg, EPB
    cpu1: MSR_IA32_MISC_ENABLE: 0x00850089 (TCC EIST MWAIT PREFETCH TURBO)
    CPUID(7): No-SGX
    CPUID(0x15): eax_crystal: 2 ebx_tsc: 110 ecx_crystal_hz: 38400000
    TSC: 2112 MHz (38400000 Hz * 110 / 2 / 1000000)
    CPUID(0x16): base_mhz: 2100 max_mhz: 4400 bus_mhz: 100
    cpu1: MSR_MISC_PWR_MGMT: 0x000030c2 (ENable-EIST_Coordination ENable-EPB DISable-OOB)
    RAPL: 9362 sec. Joule Counter Range, at 28 Watts
    cpu1: MSR_PLATFORM_INFO: 0x804043df0811500
    4 * 100.0 = 400.0 MHz max efficiency frequency
    21 * 100.0 = 2100.0 MHz base frequency
    cpu1: MSR_IA32_POWER_CTL: 0x00e4005b (C1E auto-promotion: ENabled)
    cpu1: MSR_TURBO_RATIO_LIMIT: 0x2323232323252c2c
    35 * 100.0 = 3500.0 MHz max turbo 8 active cores
    35 * 100.0 = 3500.0 MHz max turbo 7 active cores
    35 * 100.0 = 3500.0 MHz max turbo 6 active cores
    35 * 100.0 = 3500.0 MHz max turbo 5 active cores
    35 * 100.0 = 3500.0 MHz max turbo 4 active cores
    37 * 100.0 = 3700.0 MHz max turbo 3 active cores
    44 * 100.0 = 4400.0 MHz max turbo 2 active cores
    44 * 100.0 = 4400.0 MHz max turbo 1 active cores
    cpu1: MSR_CONFIG_TDP_NOMINAL: 0x00000011 (base_ratio=17)
    cpu1: MSR_CONFIG_TDP_LEVEL_1: 0x000e00a0 (PKG_MIN_PWR_LVL1=0 PKG_MAX_PWR_LVL1=0 LVL1_RATIO=14 PKG_TDP_LVL1=160)
    cpu1: MSR_CONFIG_TDP_LEVEL_2: 0x00150118 (PKG_MIN_PWR_LVL2=0 PKG_MAX_PWR_LVL2=0 LVL2_RATIO=21 PKG_TDP_LVL2=280)
    cpu1: MSR_CONFIG_TDP_CONTROL: 0x00000000 ( lock=0)
    cpu1: MSR_TURBO_ACTIVATION_RATIO: 0x00000010 (MAX_NON_TURBO_RATIO=16 lock=0)
    cpu1: MSR_PKG_CST_CONFIG_CONTROL: 0x74008008 (UNdemote-C1, demote-C1, locked, pkg-cstate-limit=8 (unlimited))
    /dev/cpu_dma_latency: 2000000000 usec (default)
    current_driver: intel_idle
    current_governor: menu
    current_governor_ro: menu
    cpu1: POLL: CPUIDLE CORE POLL IDLE
    cpu1: C1E: MWAIT 0x01
    cpu1: C6: MWAIT 0x20
    cpu1: C8: MWAIT 0x40
    cpu1: C10: MWAIT 0x60
    cpu1: cpufreq driver: intel_pstate
    cpu1: cpufreq governor: powersave
    cpufreq intel_pstate no_turbo: 0
    cpu1: MSR_MISC_FEATURE_CONTROL: 0x00000000 (L2-Prefetch L2-Prefetch-pair L1-Prefetch L1-IP-Prefetch)
    cpu0: MSR_PM_ENABLE: 0x00000001 (HWP)
    cpu0: MSR_HWP_CAPABILITIES: 0x011c1638 (high 56 guar 22 eff 28 low 1)
    cpu0: MSR_HWP_REQUEST: 0xc0003806 (min 6 max 56 des 0 epp 0xc0 window 0x0 pkg 0x0)
    cpu0: MSR_HWP_REQUEST_PKG: 0x8000ff01 (min 1 max 255 des 0 epp 0x80 window 0x0)
    cpu0: MSR_HWP_INTERRUPT: 0x00000001 (EN_Guaranteed_Perf_Change, Dis_Excursion_Min)
    cpu0: MSR_HWP_STATUS: 0x00000000 (No-Guaranteed_Perf_Change, No-Excursion_Min)
    cpu0: EPB: 7 (custom)
    cpu0: MSR_RAPL_POWER_UNIT: 0x000a0e03 (0.125000 Watts, 0.000061 Joules, 0.000977 sec.)
    cpu0: MSR_PKG_POWER_INFO: 0x000000e0 (28 W TDP, RAPL 0 - 0 W, 0.000000 sec.)
    cpu0: MSR_PKG_POWER_LIMIT: 0x42820000dd80f0 (UNlocked)
    cpu0: PKG Limit #1: ENabled (30.000 Watts, 28.000000 sec, clamp ENabled)
    cpu0: PKG Limit #2: ENabled (64.000 Watts, 0.002441* sec, clamp DISabled)
    cpu0: MSR_VR_CURRENT_CONFIG: 0x000002d0
    cpu0: PKG Limit #4: 90.000000 Watts (UNlocked)
    cpu0: MSR_DRAM_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: DRAM Limit: DISabled (0.000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_PP0_POLICY: 9
    cpu0: MSR_PP0_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: Cores Limit: DISabled (0.000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_PP1_POLICY: 13
    cpu0: MSR_PP1_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: GFX Limit: DISabled (0.000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_IA32_TEMPERATURE_TARGET: 0x80640000 (100 C)
    cpu0: MSR_IA32_PACKAGE_THERM_STATUS: 0x884f0000 (21 C)
    cpu0: MSR_IA32_PACKAGE_THERM_INTERRUPT: 0x02000003 (100 C, 100 C)
    cpu1: MSR_PKGC3_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu1: MSR_PKGC6_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu1: MSR_PKGC7_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu1: MSR_PKGC8_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu1: MSR_PKGC9_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu1: MSR_PKGC10_IRTL: 0x00000000 (NOTvalid, 0 ns)
    0.012176 sec
    GFX%rc6 Pkg%pc2 Pkg%pc3 Pkg%pc6 Pkg%pc7 Pkg%pc8 Pkg%pc9 Pk%pc10 SYS%LPI
    82.13   0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00
    90.10   0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00
    Slept for 1.01 hours
    Used 0.75 Wh, an average rate of 0.75 W
    For your 54.53 Wh battery this is 1.38%/hr or 33.03%/day
    oct 11 20:19:44 angela systemd-sleep[12975]: /sys/class/power_supply/BAT1/charge_now                     =   3284 [mAh]
    oct 11 21:20:04 angela systemd-sleep[13222]: /sys/class/power_supply/BAT1/charge_now                     =   3235 [mAh]

See how the second suspend didn't correctly run? It looks like
`rtcwake` doesn't correctly trigger the systemd hooks. And indeed, the
above it doesn't even go in the correct sleep more (`freeze` instead
of `mem`).

Another run, with `mem`:

    root@angela:/home/anarcat# turbostat --show GFX%rc6,Pkg%pc2,Pkg%pc3,Pkg%pc6,Pkg%pc7,Pkg%pc8,Pkg%pc9,Pk%pc10,SYS%LPI rtcwake -m mem -s 3600 ; /opt/batterylog/batterylog.py ; journalctl -b | grep charge_now | tail -2
    turbostat version 2022.04.16 - Len Brown <lenb@kernel.org>
    CPUID(0): GenuineIntel 0x20 CPUID levels
    CPUID(1): family:model:stepping 0x6:9a:3 (6:154:3) microcode 0x421
    CPUID(0x80000000): max_extended_levels: 0x80000008
    CPUID(1): SSE3 MONITOR SMX EIST TM2 TSC MSR ACPI-TM HT TM
    CPUID(6): APERF, TURBO, DTS, PTM, HWP, HWPnotify, HWPwindow, HWPepp, HWPpkg, EPB
    cpu6: MSR_IA32_MISC_ENABLE: 0x00850089 (TCC EIST MWAIT PREFETCH TURBO)
    CPUID(7): No-SGX
    CPUID(0x15): eax_crystal: 2 ebx_tsc: 110 ecx_crystal_hz: 38400000
    TSC: 2112 MHz (38400000 Hz * 110 / 2 / 1000000)
    CPUID(0x16): base_mhz: 2100 max_mhz: 4400 bus_mhz: 100
    cpu6: MSR_MISC_PWR_MGMT: 0x000030c2 (ENable-EIST_Coordination ENable-EPB DISable-OOB)
    RAPL: 9362 sec. Joule Counter Range, at 28 Watts
    cpu6: MSR_PLATFORM_INFO: 0x804043df0811500
    4 * 100.0 = 400.0 MHz max efficiency frequency
    21 * 100.0 = 2100.0 MHz base frequency
    cpu6: MSR_IA32_POWER_CTL: 0x00e4005b (C1E auto-promotion: ENabled)
    cpu6: MSR_TURBO_RATIO_LIMIT: 0x2323232323252c2c
    35 * 100.0 = 3500.0 MHz max turbo 8 active cores
    35 * 100.0 = 3500.0 MHz max turbo 7 active cores
    35 * 100.0 = 3500.0 MHz max turbo 6 active cores
    35 * 100.0 = 3500.0 MHz max turbo 5 active cores
    35 * 100.0 = 3500.0 MHz max turbo 4 active cores
    37 * 100.0 = 3700.0 MHz max turbo 3 active cores
    44 * 100.0 = 4400.0 MHz max turbo 2 active cores
    44 * 100.0 = 4400.0 MHz max turbo 1 active cores
    cpu6: MSR_CONFIG_TDP_NOMINAL: 0x00000011 (base_ratio=17)
    cpu6: MSR_CONFIG_TDP_LEVEL_1: 0x000e00a0 (PKG_MIN_PWR_LVL1=0 PKG_MAX_PWR_LVL1=0 LVL1_RATIO=14 PKG_TDP_LVL1=160)
    cpu6: MSR_CONFIG_TDP_LEVEL_2: 0x00150118 (PKG_MIN_PWR_LVL2=0 PKG_MAX_PWR_LVL2=0 LVL2_RATIO=21 PKG_TDP_LVL2=280)
    cpu6: MSR_CONFIG_TDP_CONTROL: 0x00000000 ( lock=0)
    cpu6: MSR_TURBO_ACTIVATION_RATIO: 0x00000010 (MAX_NON_TURBO_RATIO=16 lock=0)
    cpu6: MSR_PKG_CST_CONFIG_CONTROL: 0x74008008 (UNdemote-C1, demote-C1, locked, pkg-cstate-limit=8 (unlimited))
    /dev/cpu_dma_latency: 2000000000 usec (default)
    current_driver: intel_idle
    current_governor: menu
    current_governor_ro: menu
    cpu6: POLL: CPUIDLE CORE POLL IDLE
    cpu6: C1E: MWAIT 0x01
    cpu6: C6: MWAIT 0x20
    cpu6: C8: MWAIT 0x40
    cpu6: C10: MWAIT 0x60
    cpu6: cpufreq driver: intel_pstate
    cpu6: cpufreq governor: powersave
    cpufreq intel_pstate no_turbo: 0
    cpu6: MSR_MISC_FEATURE_CONTROL: 0x00000000 (L2-Prefetch L2-Prefetch-pair L1-Prefetch L1-IP-Prefetch)
    cpu0: MSR_PM_ENABLE: 0x00000001 (HWP)
    cpu0: MSR_HWP_CAPABILITIES: 0x01141638 (high 56 guar 22 eff 20 low 1)
    cpu0: MSR_HWP_REQUEST: 0xc0003806 (min 6 max 56 des 0 epp 0xc0 window 0x0 pkg 0x0)
    cpu0: MSR_HWP_REQUEST_PKG: 0x8000ff01 (min 1 max 255 des 0 epp 0x80 window 0x0)
    cpu0: MSR_HWP_INTERRUPT: 0x00000001 (EN_Guaranteed_Perf_Change, Dis_Excursion_Min)
    cpu0: MSR_HWP_STATUS: 0x00000000 (No-Guaranteed_Perf_Change, No-Excursion_Min)
    cpu0: EPB: 7 (custom)
    cpu0: MSR_RAPL_POWER_UNIT: 0x000a0e03 (0.125000 Watts, 0.000061 Joules, 0.000977 sec.)
    cpu0: MSR_PKG_POWER_INFO: 0x000000e0 (28 W TDP, RAPL 0 - 0 W, 0.000000 sec.)
    cpu0: MSR_PKG_POWER_LIMIT: 0x42820000dd80f0 (UNlocked)
    cpu0: PKG Limit #1: ENabled (30.000 Watts, 28.000000 sec, clamp ENabled)
    cpu0: PKG Limit #2: ENabled (64.000 Watts, 0.002441* sec, clamp DISabled)
    cpu0: MSR_VR_CURRENT_CONFIG: 0x000002d0
    cpu0: PKG Limit #4: 90.000000 Watts (UNlocked)
    cpu0: MSR_DRAM_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: DRAM Limit: DISabled (0.000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_PP0_POLICY: 9
    cpu0: MSR_PP0_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: Cores Limit: DISabled (0.000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_PP1_POLICY: 13
    cpu0: MSR_PP1_POWER_LIMIT: 0x00000000 (UNlocked)
    cpu0: GFX Limit: DISabled (0.000 Watts, 0.000977 sec, clamp DISabled)
    cpu0: MSR_IA32_TEMPERATURE_TARGET: 0x80640000 (100 C)
    cpu0: MSR_IA32_PACKAGE_THERM_STATUS: 0x884b0000 (25 C)
    cpu0: MSR_IA32_PACKAGE_THERM_INTERRUPT: 0x02000003 (100 C, 100 C)
    cpu6: MSR_PKGC3_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu6: MSR_PKGC6_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu6: MSR_PKGC7_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu6: MSR_PKGC8_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu6: MSR_PKGC9_IRTL: 0x00000000 (NOTvalid, 0 ns)
    cpu6: MSR_PKGC10_IRTL: 0x00000000 (NOTvalid, 0 ns)
    rtcwake: assuming RTC uses UTC ...
    rtcwake: wakeup from "mem" using /dev/rtc0 at Wed Oct 12 14:11:02 2022

    264.737865 sec
    GFX%rc6 Pkg%pc2 Pkg%pc3 Pkg%pc6 Pkg%pc7 Pkg%pc8 Pkg%pc9 Pk%pc10 SYS%LPI
    0.84    1609.15 1609.15 1609.15 0.00    1609.15 0.00    1609.15 6967928093745.08
    0.84    100.57  100.57  100.57  0.00    100.57  0.00    100.57  6968124499895.82
    Slept for 9.04 hours
    Used 2.09 Wh, an average rate of 0.23 W
    For your 54.53 Wh battery this is 0.42%/hr or 10.20%/day
    oct 11 23:03:32 angela systemd-sleep[42344]: /sys/class/power_supply/BAT1/charge_now                     =   2969 [mAh]
    oct 12 08:05:52 angela systemd-sleep[42611]: /sys/class/power_supply/BAT1/charge_now                     =   2833 [mAh]

That *seems* to have worked, but then the actual numbers from
`turbostat` are not really that meaningful. It seems we're using 100%
of the CPU there, and millions of percent of SYS. Anyways, I don't
understand how turbostat could be used that way anyways, since, well,
it's suspended...

# assumptions

 * we assume standby battery usage doesn't fluctuate depending on the
   level of the battery; as of this writing, the battery is at 95%
   with an estimated 10h remaining

 * we assume we will get similar results than runtime power usage,
   that is the SSD and MicroSD expansion cards use a *lot* of power,
   HDMI and DisplayPort a little, and USB-A the less, while USB-C
   should use none at all

 * we have heard theories that USB-A uses a lot more power on suspend
   than at runtime

 * we assume deep sleep provides better power saving

 * we expect to get close to 14mA drain on suspend

# previous work

The [previous results](https://community.frame.work/t/high-battery-drain-during-suspend/3736/10?u=anarcat) were:

 * System on, locked and screen off, with HDMI & USB expansion cards: around 3W
 * s2idle sleep with USB-A and HDMI expansion cards: around 1.9 W
 * s2idle sleep with only USB-C expansion cards: around 0.8 W
 * deep sleep with USB-A and HDMI expansion cards: around 0.9 W
 * deep sleep with only USB-C expansion cards: around 0.4 W

# no modules

## baseline: sleep=deep, nvme.noacpi=1, 0.25W, 16mA, 9 days

this is done by adding `mem_sleep_default=deep` to grup, rebuilding
grub, and rebooting, then sleeping 5 minutes.

    Slept for 0.09 hours
    Used 0.03 Wh, an average rate of 0.36 W
    For your 54.53 Wh battery this is 0.66%/hr or 15.74%/day

    oct 11 09:58:51 angela systemd-sleep[3372668]: /sys/class/power_supply/BAT1/charge_now                     =   3317 [mAh]
    oct 11 10:04:01 angela systemd-sleep[3372950]: /sys/class/power_supply/BAT1/charge_now                     =   3315 [mAh]

not conclusive, too close (2mAh) to the margin of error
(1mAh). redoing with 60 minutes delay:

    Slept for 1.00 hours
    Used 0.25 Wh, an average rate of 0.25 W
    For your 54.53 Wh battery this is 0.45%/hr or 10.83%/day

    root@angela:/home/anarcat/wikis/anarc.at# journalctl -b | grep charge_now | tail -2
    oct 11 10:10:42 angela systemd-sleep[3377619]: /sys/class/power_supply/BAT1/charge_now                     =   3270 [mAh]
    oct 11 11:10:46 angela systemd-sleep[3377936]: /sys/class/power_supply/BAT1/charge_now                     =   3254 [mAh]

So about 0.45% per hour, 0.25W, or 16mA, or about 9 days standby. This
is close to our expected 14mA. If the metrics from the two tools
match, it would mean a 15.625 volt potential from the battery, which
seems reasonable.

## sleep=s2idle, nvme.noacpi=1, 0.29W, 18.9mA, ~7 days

same as the above, but with `mem_sleep_default=s2idle`.

odd result. no power consumption detected in one hour of suspend:

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2
    oct 11 12:34:48 angela systemd-sleep[13030]: /sys/class/power_supply/BAT1/charge_now                     =   3541 [mAh]
    oct 11 13:34:51 angela systemd-sleep[13265]: /sys/class/power_supply/BAT1/charge_now                     =   3541 [mAh]

This even crashed battery log:

    root@angela:/home/anarcat# /opt/batterylog/batterylog.py
    Traceback (most recent call last):
      File "/opt/batterylog/batterylog.py", line 123, in <module>
        until_empty_h = Decimal(resume['energy_min']/1000000000000)/ power_use_w
    decimal.DivisionByZero: [<class 'decimal.DivisionByZero'>]

And no, the laptop was not plugged in. Probably because the battery
was recharged between the first test and second, it's possible it was
"over charged" and the battery use wasn't detected.

Really odd result, will retry.

    root@angela:/home/anarcat# /opt/batterylog/batterylog.py
    Slept for 1.00 hours
    Used 0.29 Wh, an average rate of 0.29 W
    For your 54.53 Wh battery this is 0.54%/hr or 12.86%/day

    root@angela:/home/anarcat# journalctl -b | grep charge_now | tail -2
    oct 11 14:00:41 angela systemd-sleep[29700]: /sys/class/power_supply/BAT1/charge_now                     =   3497 [mAh]
    oct 11 15:00:45 angela systemd-sleep[29983]: /sys/class/power_supply/BAT1/charge_now                     =   3478 [mAh]

That's 18.9mA, 0.29W, 0.54% per hour, or about a week standby. 

In here, we could say `s2idle` costs us 0.04W, or about 5mA, which is
pretty small, but still negligible: about two days standby time, out
of the 9 days from the baseline.

## sleep=s2idle, no nvme config, 0.31W, 20mA, ~7 days

Here, we kept the `mem_sleep_default=s2idle` kernel parameter but
removed the `nvme.noacpi=1` flag.
    
    Slept for 1.00 hours
    Used 0.31 Wh, an average rate of 0.31 W
    For your 54.53 Wh battery this is 0.56%/hr or 13.55%/day

    oct 11 15:06:11 angela systemd-sleep[3499]: /sys/class/power_supply/BAT1/charge_now                     =   3447 [mAh]
    oct 11 16:06:13 angela systemd-sleep[3793]: /sys/class/power_supply/BAT1/charge_now                     =   3427 [mAh]

That's 19.98mA (let's just call it 20mA), 0.31W, 0.56% per hour, or
about a week of standby.

It's only slightly more than with `nvme.noacpi=1`, but it's basically
within the margin of error (1mAh or 0.02W).

# 1 USB-C: 0.23W, 15mA, ~10 days

baseline configuration (`nvme.noacpi=1`, `mem_sleep_default=deep`)
with only one USB-C expansion card connected

    Slept for 1.00 hours
    Used 0.23 Wh, an average rate of 0.23 W
    For your 54.53 Wh battery this is 0.42%/hr or 10.15%/day

    oct 11 16:15:20 angela systemd-sleep[3637]: /sys/class/power_supply/BAT1/charge_now                     =   3378 [mAh]
    oct 11 17:15:25 angela systemd-sleep[3946]: /sys/class/power_supply/BAT1/charge_now                     =   3363 [mAh]

15mA, 0.23W, 0.42%/h, that's 10 days standby, *more* than without
modules??? weeeird... Could it be that USB-C cards do something to the
USB-C ports that make them take *less* power?

# 1 USB-C, longer suspend: 0.22W, 14.5mA, ~10+ days

Just a spot check.

    Slept for 1.93 hours
    Used 0.43 Wh, an average rate of 0.22 W
    For your 54.53 Wh battery this is 0.41%/hr or 9.82%/day

    oct 11 17:18:58 angela systemd-sleep[6788]: /sys/class/power_supply/BAT1/charge_now                     =   3348 [mAh]
    oct 11 19:14:58 angela systemd-sleep[7039]: /sys/class/power_supply/BAT1/charge_now                     =   3320 [mAh]

Again, around 14mA (14.5mA), 0.22W, 0.41%/hr. Excellent result.

# 2 USB-C: 0.23W, 14.9mA, ~10 days

    Slept for 1.01 hours
    Used 0.23 Wh, an average rate of 0.23 W
    For your 54.53 Wh battery this is 0.42%/hr or 10.10%/day

    oct 11 19:17:34 angela systemd-sleep[11024]: /sys/class/power_supply/BAT1/charge_now                     =   3307 [mAh]
    oct 11 20:17:57 angela systemd-sleep[11277]: /sys/class/power_supply/BAT1/charge_now                     =   3292 [mAh]

14.9mA, 0.23W, 0.42%/hr. Basically similar to one USB-C port, as the
above, lower than the baseline.

# 1 USB-A: 0.75W, 48.7mA, 3 days

    Slept for 1.01 hours
    Used 0.75 Wh, an average rate of 0.75 W
    For your 54.53 Wh battery this is 1.38%/hr or 33.03%/day

    oct 11 20:19:44 angela systemd-sleep[12975]: /sys/class/power_supply/BAT1/charge_now                     =   3284 [mAh]
    oct 11 21:20:04 angela systemd-sleep[13222]: /sys/class/power_supply/BAT1/charge_now                     =   3235 [mAh]

48.7mAm 0.75W, 1.38%/hre, basically 3 days standby max! That's a
three-fold performance decrease from the previous metric.

Okay, here's a problem. That's half a watt, for one USB-A card, doing
nothing, during suspend.

Something is definitely wrong with the USB-A connectors.

# spot check: redoing baseline, longer test

Redoing the baseline for a longer test, as the first test above gave
odd results, where the discharge rate without USB-C cards is *higher*
than with the card(s).

    Slept for 9.04 hours
    Used 2.09 Wh, an average rate of 0.23 W
    For your 54.53 Wh battery this is 0.42%/hr or 10.20%/day
    oct 11 23:03:32 angela systemd-sleep[42344]: /sys/class/power_supply/BAT1/charge_now                     =   2969 [mAh]
    oct 12 08:05:52 angela systemd-sleep[42611]: /sys/class/power_supply/BAT1/charge_now                     =   2833 [mAh]

That's 15mA and 0.23W, closer to the "only USB-C" metric. So this is
our new baseline.

# 2 USB-A: 1.11W, 72mA, 2 days

    Slept for 1.00 hours
    Used 1.11 Wh, an average rate of 1.11 W
    For your 54.53 Wh battery this is 2.03%/hr or 48.79%/day
    oct 12 09:16:35 angela systemd-sleep[47702]: /sys/class/power_supply/BAT1/charge_now                     =   2775 [mAh]
    oct 12 10:16:36 angela systemd-sleep[47962]: /sys/class/power_supply/BAT1/charge_now                     =   2703 [mAh]

72mA, 1.11W, another ~500mW (360mW, actually) for the extra USB-A
card. ouch. Barely two days standby with this configuration.

# 3 USB-A: 1.48W, 96mA, < 2 days

    Slept for 1.00 hours
    Used 1.48 Wh, an average rate of 1.48 W
    For your 54.53 Wh battery this is 2.71%/hr or 65.05%/day
    oct 12 11:47:43 angela systemd-sleep[100669]: /sys/class/power_supply/BAT1/charge_now                     =   2460 [mAh]
    oct 12 12:47:44 angela systemd-sleep[100986]: /sys/class/power_supply/BAT1/charge_now                     =   2364 [mAh]

96mA, 1.48W, another 370mW. Seems to confirm there's about 360mW per
USB-A card, with a base 150mW cost once any is plugged. That's a
total of 1.25W for three USB-A cards, or about 420mW per card on
average.

Less than two days standby.

# 1TB SSD: 0.49W, 32mA, <5 days

    Slept for 1.00 hours
    Used 0.49 Wh, an average rate of 0.49 W
    For your 54.53 Wh battery this is 0.90%/hr or 21.68%/day
    oct 12 13:01:15 angela systemd-sleep[110500]: /sys/class/power_supply/BAT1/charge_now                     =   2304 [mAh]
    oct 12 14:01:16 angela systemd-sleep[110779]: /sys/class/power_supply/BAT1/charge_now                     =   2272 [mAh]

# MicroSD: 0.52W, 34mA, ~4 days

    Slept for 1.00 hours
    Used 0.52 Wh, an average rate of 0.52 W
    For your 54.53 Wh battery this is 0.96%/hr or 23.04%/day
    oct 12 14:04:38 angela systemd-sleep[113632]: /sys/class/power_supply/BAT1/charge_now                     =   2254 [mAh]
    oct 12 15:04:38 angela systemd-sleep[113882]: /sys/class/power_supply/BAT1/charge_now                     =   2220 [mAh]

# 1 DisplayPort: 0.85W, 55mA, <3 days

    Slept for 1.00 hours
    Used 0.85 Wh, an average rate of 0.85 W
    For your 54.53 Wh battery this is 1.55%/hr or 37.27%/day
    oct 12 15:07:49 angela systemd-sleep[116530]: /sys/class/power_supply/BAT1/charge_now                     =   2205 [mAh]
    oct 12 16:07:50 angela systemd-sleep[116797]: /sys/class/power_supply/BAT1/charge_now                     =   2150 [mAh]

# 1 HDMI: 0.58W, 38mA, ~4 days

    Slept for 1.00 hours
    Used 0.59 Wh, an average rate of 0.58 W
    For your 54.53 Wh battery this is 1.07%/hr or 25.74%/day
    oct 12 16:14:23 angela systemd-sleep[122142]: /sys/class/power_supply/BAT1/charge_now                     =   2123 [mAh]
    oct 12 17:14:25 angela systemd-sleep[122476]: /sys/class/power_supply/BAT1/charge_now                     =   2085 [mAh]

# 2 HDMI: 0.65W, 42mA, <4 days

    Slept for 1.00 hours
    Used 0.65 Wh, an average rate of 0.65 W
    For your 54.53 Wh battery this is 1.19%/hr or 28.46%/day
    oct 12 17:53:19 angela systemd-sleep[125049]: /sys/class/power_supply/BAT1/charge_now                     =   2054 [mAh]
    oct 12 18:53:20 angela systemd-sleep[125304]: /sys/class/power_supply/BAT1/charge_now                     =   2012 [mAh]

# last spot check, no cards: 0.25W, 16mA, >9 days

    Slept for 12.56 hours
    Used 3.08 Wh, an average rate of 0.25 W
    For your 54.53 Wh battery this is 0.45%/hr or 10.79%/day
    oct 12 20:19:01 angela systemd-sleep[149720]: /sys/class/power_supply/BAT1/charge_now                     =   1836 [mAh]
    oct 13 08:52:54 angela systemd-sleep[150028]: /sys/class/power_supply/BAT1/charge_now                     =   1636 [mAh]

Interestingly, this is the same as the original baseline, which is
higher than the "USB-C" case. This posibly confirms that having USB-C
cards plugged in *helps* with power consumption, but further tests
would be required to confirm this hypothesis.

At the end of the run, the battery was at 45%, with 4h26m remaining
according to [py3status][]. Almost all the tests here were done on a single
charge, although there was *one* charge earlier in the test (in
`sleep=s2idle, nvme.noacpi=1`). It doesn't seem like charge level
influences battery consumption apart from the "full battery" case.

[py3status]: https://github.com/ultrabug/py3status
