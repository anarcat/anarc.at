So i have a new laptop, ''again''. Not that i'm unhappy about it, but there's always a few glitches to fix, thanks to the general interoperability of hardware these days.

The laptop is a [Toshiba](http://toshiba.ca/) Satellite PSA30C-00YS5. Some files are available for [download](http://209.167.114.38/support/download/files_downloads/index/Satellite/sa30en.htm) on the toshiba site, but it's all windows drivers.

# Previous experience

At least one person documented the A30 on linux, but the only page left is  [a backup](http://web.archive.org/web/20050205233442/tiptsoft.com/laptop/linux.html) in [archive.org](http://archive.org/).

# The touchpad

After a debian etch install, the touchpad seemed to only move around (no tap, no scroll) and very slowly. The various mouse settings controls of GNOME didn't fix anything, so I had to dig deeper. I found [this thread](http://www.ubuntuforums.org/showthread.php?p=673281#post673281) on [Ubuntuforums.org](http://ubuntuforums.org/) that showed me a dump of xorg.conf:

[[!format txt """
Section "InputDevice"
    Identifier    "Synaptics Touchpad"
    Driver        "synaptics"
    Option "AlwaysCore"
    Option        "SendCoreEvents"    "true"
    Option        "Device"        "/dev/input/event2"
    Option        "Protocol"        "event"
    Option "LeftEdge" "130"
    Option "RightEdge" "840"
    Option "TopEdge" "130"
    Option "BottomEdge" "640"
    Option "FingerLow" "7"
    Option "FingerHigh" "8"
    Option "MaxTapTime" "180"
    Option "MaxTapMove" "110"
    Option "ClickTime" "0"
    Option "EmulateMidButtonTime" "75"
    Option "VertScrollDelta" "20"
    Option "HorizScrollDelta" "20"
    Option "MinSpeed" "0.60"
    Option "MaxSpeed" "1.10"
    Option "AccelFactor" "0.030"
    Option "EdgeMotionMinSpeed" "200"
    Option "EdgeMotionMaxSpeed" "200"
    Option "UpDownScrolling" "1"
    Option "CircularScrolling" "1"
    Option "CircScrollDelta" "0.1"
    Option "CircScrollTrigger" "2"
    Option "SHMConfig" "on"
    Option "Emulate3Buttons" "on"
EndSection
""" ]]

With this, the scroll works, the mouse has an acceptable speed. and tapping works, although a bit retarded. 

# Hardware monitor

Unsurprisingly, mbmon doesn't cut it. The webarchive page tells me that the [omnibook driver](http://sourceforge.net/projects/omke/) is what i need, but it [doesn't compile in 2.6.17](http://sourceforge.net/tracker/index.php?func=detail&aid=1512041&group_id=48623&atid=453668).

# Frequency scaling

Ça marche bien, mais il serait bien d'avoir une meilleure granularité dans le scaling (ie. pas 1.6 et 3.2). Comment faire: Le module acpi-cpufreq semble trouver le processeur:
[[!format txt """
Sep 10 13:17:33 orange kernel: acpi-cpufreq: CPU0 - ACPI performance management activated.
""" ]]
Il faut aussi activer les modules `freq_table`, `cpufreq_ondemand`, `cpufreq_power`, `cpufreq_powersave` et activer les scripts de cpufrequtils dans `/etc/default/cpufrequtils` et relancer le truc avec `/etc/init.d/cpufrequtils restart`. Le petit applet de gnome me montre que mon processeur est maintenant à 1.6Ghz, yay.

# DRI/accélération graphique

   [[!format txt """
$ glxgears
2656 frames in 5.0 seconds = 531.062 FPS
2657 frames in 5.0 seconds = 531.267 FPS
2656 frames in 5.0 seconds = 531.072 FPS
$ glxinfo | grep direct
direct rendering: Yes
""" ]]
   après l'upgrade à Xorg 7.0, il faut [télécharger une librairie de experimental](http://packages.debian.org/cgi-bin/download.pl?arch=i386&file=pool%2Fmain%2Fm%2Fmesa%2Flibgl1-mesa-dri_6.5.0.cvs.20060524-1_i386.deb&md5sum=d5d3ff355c14144e80cd0bd803f8b1be&arch=i386&type=main) et installer le `/usr/lib/dri` qui en provient:
   [[!format txt """
# mkdir tmp
# dpkg -x libgl1-mesa-dri_6.5.0.cvs.20060524-1_i386.deb tmp
# mv /usr/lib/dri /usr/lib/dri.old
# mv tmp/usr/lib/dri/ /usr/lib/dri
# exit
$ glxinfo | grep direc
direct rendering: Yes
""" ]]
    * [truc d'un geekblog](http://geekblog.over-blog.com/article-3472661.html)
    * [bug](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=359328) et [discussion](http://lists.alioth.debian.org/pipermail/pkg-mesa-devel/2006-May/000210.html) reliés

# suspend/resume

Le "suspend to disk" ne marche pas. Ça essaie de suspendre puis ça gèle:

[[!format txt """
cpufreq: suspend failed to assert current frequency is what timing core thinks it is
swsusp: Need to copy 31320 pages
swsusp: critical section/: done (31320 pages copied)
...
CPU0: Intel P4/Xeon Extended MCE MSRs (12) available
CPU0: thermal monitoring enabled
swsusp: restoring highmem
cpufreq: suspend failed to assert current frequency is what timing core thinks it is
...
""" ]]

Le "suspend to ram" éteint la machine d'un coup puis en redémarrant, gèle sans montrer le BIOS.

Refs:

 * a [similar problem](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=262751)
 * [something to try?](http://pbbuttons.sourceforge.net/projects/pbbuttonsd/doc.html)

# Sommaire

 * boîtier: (./) bien joli, un peu lourd, mais tres bien en general
 * écran: luminosité excellente, fixe et solide, sans dead pixel. Ne descend pas a plat comme le thinkpad, mais ca va.  (./)
 * clavier: pas de scroll lock, disposition des touches pgup/down et du # bizarres, mais autrement, touche confortable et toutes touches fonctionnelles. Les touches Fn de volume, luminosité, désactivation du touchpad, numluck et padlock fonctionnent. La touche "energy saving", "lock", suspend et "wifi" (duh) ne fonctionnent pas. (./)
 * souris: fonctionne bien, les deux boutons et le touchpad avec le hack ci-haut  (./)
 * ports audio
  * line in <!> à tester
  * line out (./)
  * mic in <!> à tester
  * micro <!> à tester
  * speakers (./) sonnent cacane un peu
 * batterie (2h de batteries sans wifi) (./)
 * cdrom/dvd: <!> burner à tester, reconnu par cdrecord dev=/dev/hdc -scanbus, configuré dans /etc/default/cdrecord.
 * disque dur: pas d'erreur à date, 30G (./)
 * RAM: 700+M en deux barrettes <!> à tester
 * Celeron 2.6Ghz (./)
 * carte video: Intel(R) 852GME (./) DRI marche.
 * ports:
  * modem: <!> à tester
  * réseau: (./) whoohoo! onboard!
  * USB <!> à tester
  * s-video out <!> à tester
  * SVGA out: <!> à tester
  * PCMCIA: <!> à tester
  * infrarouge: <!> à tester
  * série: <!> à tester
  * PS/2: <!> à tester
 * ACPI: <!> à tester
  * suspend to disk
  * suspend to ram
  * poweroff
 * hardware monitor: /!\ marche pas avec mbmon
