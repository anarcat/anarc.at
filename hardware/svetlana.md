> Svetlana Yevgenyevna Savitskaya (Russian: Светла́на Евге́ньевна
> Сави́цкая; born 8 August 1948) is a Russian former aviator and Soviet
> cosmonaut who flew aboard Soyuz T-7 in 1982, becoming the second
> woman in space. On her 1984 Soyuz T-12 mission she became the first
> woman to fly to space twice, and the first woman to perform a
> spacewalk. [...]  While she and Valentina Tereshkova were both
> chosen for missions into space due to Soviet propaganda purposes,
> Savitskaya was much more trained and experienced in aeronautics
> whereas [Tereshkova](https://en.wikipedia.org/wiki/Valentina_Tereshkova) was chosen as a political stunt. --
> [Wikipedia](https://en.wikipedia.org/wiki/Svetlana_Savitskaya)

The name Valentina, for Tereshkova, was briefly considered for naming
this machine until it was found that there are sanctions against her
for being party to the invasion of Ukraine.

# Specification

Device is a [Unifi AP 6 lite](https://store.ui.com/collections/unifi-network-access-points/products/unifi-ap-6-lite):

 * Dimensions: Ø160 x 33 mm (Ø6.3 x 1.3" - it's tiny!)
 * Wired network: 1Gbit ethernet with PoE, passive PoE (48V) power
   supply
 * Operating temperature: -30 to 60° C (-22 to 140° F)
 * Support for 802.11abg/n/ac/ax (WiFi 4/5/6)

# Installation

Physical installation was hairy. I had setup two Cat 6A pairs into a
decora/keystone pair of outlets, but I tried to instead remove the
plate and install the router directly over the hole, to make it look
prettier. It was messy and didn't work so well, but the device is at
least installed and it's relatively solid.

One of the ports (#2) has a short and seems impossible to repair. I
have tried to crimp a RJ45 adapter to it, but that seems to not work
so well. Next step there is to try to re-wire the other end of the
connector, on the patch panel. It was rewired once, but hastily, so
maybe it's still broken.

Followed [the flashing instructions](https://openwrt.org/toh/ubiquiti/unifi_6_lite), terrified because device
switched to 192.168.1.1 and I thought it was bricked.

# Confguration

Did the following config:

 1. disable DHCP on LAN
 2. disable SLAAC and RA on IPv6
 3. assigned an IP on the central DHCP server
 4. disable DHCP server on router
 5. configured WPA password
 6. manually tuned radio channels and power

Update, 2024-07-01: Tweaked the wifi security down from WPA2/WPA3
mixed to WPA/WPA2 because a Nintendo Switch couldn't even *see* the
access point. An old iMac also had trouble connecting, but I couldn't
confirm if the issue was related as I haven't retried that.

According to <https://fast.com> and <https://speed.cloudflare.com/>, this
hotspot can saturate my uplink (130/30mbps) but with some bufferbloat
(14 vs 63ms loaded).

# References

 * [installation instructions](https://openwrt.org/toh/ubiquiti/unifi_6_lite)
 * [Ubiquiti LED patterns](https://help.ui.com/hc/en-us/articles/204910134)
