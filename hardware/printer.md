[[!toc levels=3]]

# Requirements

## Must have

 * Ethernet port
 * "driverless printing" AKA "airprint" AKA stellar Linux support
 * laser (specifically, cheap per-page prints)

## Nice to have

 * colors
 * double-sided printing, AKA "[duplex printing](https://en.wikipedia.org/wiki/Duplex_printing)"
 * fits one "ream" ("rame", 500 sheets)

## Must not have

 * scanner, or at least photocopier - I've given up on that completely
   and typically use my phone camera to scan documents
 * ink-jet printing, or specifically:
   * high cost
   * "drying out", I don't print often and this needs to keep working

# Advice received

 * avoid HP (twice), although one concern was with their "printing as
   a service" approach with inkjet printer, which may not apply to me
   (see [this HN thread warning about HP using DRM for their cartridges](https://news.ycombinator.com/item?id=35063208))
 * good words about Canon, which I have a personal chip on the
   shoulder with (see also [this HN thread warning about Canon
   printers, which also includes laser printers problems](https://news.ycombinator.com/item?id=29850987))
 * Brother printers have a lot of sympathy all around but *also*
   started blocking non-OEM cartridges (see [this HN thread](https://news.ycombinator.com/item?id=31860131), yes,
   again)
 * don't expect scanning to work from Linux, instead scan to a SMB
   share from the device (so look from SMB support)
 * [Eco Tanks](https://www.epson.com.au/v2/ecotank/) are a bit of a game changer as they provide
   laser-like price for ink jet printers... the Epson printers that do
   it do have a hefty upfront cost, and they still seem to suffer from
   "drying out" problems when not in use
 * [use Brother or Epson](https://hachyderm.io/@jbcrawford/112018421075831627)
 * [brother label printer](https://mas.to/@zekjur/112117296104194523)

# Options

Picking some common ratings websites (rtings.com and Wirecutters), I
come to the following conclusion:

 * HP has a more expensive model, but long-term per page cost is lower
 * according to rtings.com, it's the opposite, the "Canon imageCLASS
   MF743Cdw" has a better per-page cost
 * according to rtings.com, it prints B&W faster, has a better
   scanner, but worse photo printer, again than the MF743Cdw
 * that said, the "Canon imageCLASS MF743Cdw" is not available at Best
   Buy or Staples, the closest alternative at Staples is the MF741Cdw,
   which doesn't have duplex scanning, but is cheaper (see [bh
   comparison](https://www.bhphotovideo.com/c/compare/Canon_MF743Cdw_vs_Canon_MF644Cdw_vs_Canon_MF741Cdw/BHitems/1489652-REG_1489654-REG_1489653-REG)). it's out of stock at Staples and Bestbuy as well

## Brother

After being recommended Brother by a family member and the [internet](https://hachyderm.io/@jbcrawford/112018421075831627),
[twice](https://mas.to/@zekjur/112117296104194523), I've mostly given up and decided to just go with Brother
printers.

Rtings suggests the [Brother HL-L2325DW](https://www.rtings.com/printer/reviews/brother/hl-l2325dw#page-retailers), a plain black and white
laser printer, in their [2023 best laser printer review](https://www.rtings.com/printer/reviews/best/by-type/laser). [206$ at
Staples](https://www.staples.ca/products/2764645-en-brother-hl-l2370dw-wireless-monochrome-laser-printer) for the Brother HL-L2370DW variant, which features an
Ethernet port as well. Wirecutters [suggest the HL-L2350DW variant](https://www.nytimes.com/wirecutter/reviews/best-laser-printer/)
as well. They do not rate devices available locally (bestbuy or staples).

For color printers, Staples' cheapest is the [Brother HL-L3220CDW](https://www.staples.ca/products/3074146-en-brother-hl-l3220cdw-wireless-colour-laser-printer)
at 286$ (on sale from 405$!). It is also [well rated at rtings](https://www.rtings.com/printer/reviews/brother/hl-l3280cdw-hl-l3220cdw-hl-l3295cdw),
but the 3220 doesn't have Ethernet. For that you need the [Brother
HL-L3280CDW](https://www.staples.ca/products/3074145-en-brother-hl-l3280cdw-wireless-colour-laser-printer) (356$, on sale from 486$) which is the model reviewed
by rtings. 

Naturally, the color printer is larger (27.4cm x 39.9cm x 40.2cm) and
heavier (15.4Kg) than its monochrome counterpart (20cm x 36.4cm x
40.8cm). It also has a visual touch screen instead of a single-line
LCD display. The color printer will also stop printing when it thinks
the toner is empty, whereas the black-and-white one will keep pushing
out dimmer pages. The color printer prints the first page faster (12s)
than the black and white (24s).

They both hold 250 sheets.

Another alternative is the Brother HL-L3270CDW ([306$ in clearance at
staples](https://www.staples.ca/products/24342682-en-brother-hl-l3270cdw-wireless-colour-mobile-ready-laser-printer)) which has also a [good review at rtings](https://www.rtings.com/printer/reviews/brother/hl-l3270cdw-laser), but it feels
a tad expensive for an EOL device. In the 3280 review, they say:

> The Brother HL-L3280CDW is a newer version of the Brother
> HL-L3270CDW Laser. They have identical features and perform
> similarly in print quality. The HL-L3280CDW prints slightly faster
> but doesn't yield as many color prints as the older HL-L3270CDW. The
> biggest difference is that the HL-L3270CDW requires more maintenance
> because its drum unit wears out much faster.

It seems the cost per page is slightly lower on the older model, that
said. The older printer is bulkier, however.

Unclear if non-OEM cartridges work, but honestly I just get the OEM
typically...

## HP Color LaserJet Pro MFP M479FDW

<https://www.hp.com/us-en/shop/pdp/hp-color-laserjet-pro-mfp-m479fdw>

Was recommended by [Wirecutters](https://www.nytimes.com/wirecutter/reviews/best-laser-printer/) in 2021 and rtings.com

 * [Staples](https://www.staples.ca/products/2946505-en-hp-color-laserjet-pro-mfp-m479dw-printer-w1a77abgj): CAD$699.99, [replacement B&W](https://www.staples.ca/products/24398984-en-hp-414a-w2020a-black-original-laserjet-toner-cartridge): 110$, 0.046$/page

Carefully review this thread before buying anything HP: https://news.ycombinator.com/item?id=29850987

## References

 * [Wirecutters review](https://www.nytimes.com/wirecutter/reviews/best-laser-printer/), updated yearly, currently recommends the
   [HP Color LaserJet Pro M255dw](https://www.hp.com/us-en/shop/pdp/hp-color-laserjet-pro-m255dw). update: they now (2024) switched
   to the HP Color LaserJet Pro MFP M283fdw, and the Brother
   HL-L2350DW for "budget"
 * [RTINGS](https://www.rtings.com/printer/reviews/best/by-type/laser) also produce a yearly review, currently (2023) recommends the
   [Canon imageCLASS MF743Cdw](https://www.rtings.com/printer/reviews/canon/imageclass-mf743cdw) ([656$ at BestBuy](https://www.bestbuy.ca/fr-ca/produit/13796853), back-order,
   [656$ at Staples](https://www.staples.ca/products/2948189-en-canon-imageclass-mf743cdw-colour-laser-printer)), 300 sheets only, toner is expensive [200$
   for black at Staples](https://www.staples.ca/products/3029652-en-fuzion-canon-3020c001-055h-compatible-toner-high-yield-black) but has a high yield (7600 pages,
   0.026$/sheet)
 * [Tom's hardware laser vs inkjet](https://www.tomsguide.com/us/inkjet-vs-laser-printers,review-6199.html)
 * [Digital trends laser vs inkjet](https://www.digitaltrends.com/computing/laser-printer-vs-inkjet/)
 * [Consumer Reports review](https://www.consumerreports.org/electronics-computers/printers/) is paywalled
