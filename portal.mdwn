[[!meta title="Bienvenue sur le Réseau de l'Anarchat"]]

<span /><div class="nocount">
Si vous êtes arrivés ici, c'est probablement parce que vous avez
visité mon point d'accès sans fil (`wifi.anarcat.ath.cx`) et que vous
essayez d'accéder à internet. Cela devrait maintenant être possible!

Il suffit de recharger cette page pour accéder au web!
======================================================

Mais vous pouvez également consulter ce site qui comporte les
dernières nouvelles du
réseau: <http://anarcat.ath.cx/> -
toujours à votre disposition!

<div style="float: right">
<iframe height="60px" width="300px" src="http://192.168.2.1/" frameborder="0"></iframe>
</div>

Si votre adresse est bien détectée ci-contre, vous devriez maintenant
avoir accès au réseau.

La documentation de comment ce portail fonctionne est
dans [[software/portal]].

Si quelquechose ne fonctionne pas, venez me voir ou commentez dans la page en utilisant l'onglet "Discussion" en haut.
</div>
