#!/bin/sh

for YEAR in $(seq 2005 $(date +%Y) | tac); do

    cat <<EOF
## $YEAR

[[!inline pages="
(
  page(blog/*)
  or tagged(blog)
)
and creation_year($YEAR)
and !blog/*/*
and !link(foo)
and !tagged(draft)
and !tagged(redirection)"
archive=yes
quick=yes
]]

EOF

done
