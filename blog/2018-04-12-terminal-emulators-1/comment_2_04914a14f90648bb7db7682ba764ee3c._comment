[[!comment format=mdwn
 username="anarcat"
 avatar="https://seccdn.libravatar.org/avatar/741655483dd8a0b4df28fb3dedfa7e4c"
 subject="bugs"
 date="2018-04-14T01:41:54Z"
 content="""
>  Have you considered submitting these bugs to upstream? Especially the ones that are maintained regularly like Konsole, Xterm, etc? 

If you have followed other articles I wrote, you know I usually do this, pretty extensively. :) For example, I filed almost a dozen bug upstream while writing [[2018-03-19-sigal]] ([list here](https://lwn.net/Articles/748662/)).

But this series was different: I was not reviewing a single program, but multiple. And there are a bunch that I reviewed that are *not* mentioned here. Filing bugs and following up on everything would have been too prohibitive. I have spent already way too much time writing those articles and couldn't afford following up on all those issues.

Besides, many of those are controversial, as you can see in the comments on the LWN site. RTL, for example, is especially tricky to agree on. Others question the very idea that paste problems are even a security in the first place. I do not really want to get into that with all those upstreams...

But of course, the door is wide open for others to file those issues now, and I would very much welcome help in that regard. :)

Cheers!
"""]]
