[[!meta title="February 2018 report: LTS, ..."]]

[[!toc levels=2]]

Debian Long Term Support (LTS)
==============================

This is my monthly [Debian LTS][] report. This month was exclusively
dedicated to my frontdesk work. I actually forgot to do it the first
week and had to play catchup during the weekend, so I [brought up a
discussion](https://lists.debian.org/87vaexhtzf.fsf@curie.anarc.at) about how to avoid those problems in the future. I
proposed an automated reminder system, but it turns out people found
this was overkill. Instead, Chris Lamb [suggested](https://lists.debian.org/1518792850.3740462.1273156672.24C2F08A@webmail.messagingengine.com) we simply send a
ping to the next person in the list, which has proven useful the next
time I was up. In the two weeks I was frontdesk, I ended up triaging
the following notable packages:

 * [[!debcve isc-dhcp]] - remote code execution exploits - time to get
   rid of those root-level daemons?
 * [[!debcve simplesamlphp]] - under embargo, quite curious
 * [[!debcve golang]] - the return of remote code execution in `go
   get` ([[!debcve CVE-2018-6574]], similar to [[!debcve
   CVE-2017-15041]] and [[!debcve CVE-2018-7187]]) - ended up being
   marked as minor, unfortunately
 * [[!debcve systemd]] - [[!debcve CVE-2017-18078]] was marked as
   unimportant as this was neutralized by kernel hardening and systemd
   was not really in use back in wheezy. besides, [[!debcve
   CVE-2013-4392]] was about a similar functionality which was claimed
   to not be supported in wheezy. i did, however, proposed to forcibly
   enable the kernel hardening through default sysctl configurations
   ([[!debbug 889098]]) so that custom kernels would be covered by the
   protection in stable suites.

There were more minor triage work not mentioned here, those are just
the juicy ones...

Speaking of juicy, the other thing I did during the month was to help
with the documentation on the [Meltdown and Spectre attacks](https://meltdownattack.com/) on
Intel CPUs. Much has been written about this and I won't do yet
another summary. However, it seems that no one actually had written
even semi-official documentation on the state of fixes in Debian,
which lead to many questions to the (LTS) security team(s). Ola
Lundqvist did a first draft of a page detailing the current status,
and I expanded on the page to add formatting and more details. The
page is visible here:

<https://wiki.debian.org/DebianSecurity/SpectreMeltdown>

I'm still not fully happy with the results: we're missing some
userland like Qemu and a timeline of fixes. In comparison, the [Ubuntu
page](https://wiki.ubuntu.com/SecurityTeam/KnowledgeBase/SpectreAndMeltdown) still looks much better in my opinion. But it's leagues ahead
of what we had before, which was nothing... The next step for LTS is
to backport the retpoline fixes back into a compiler. Roberto
C. Sanchez is working on this, and the remaining question is whether
we [try to backport to GCC 4.7](https://lists.debian.org/20180301125645.wt3nkodmksce3a6v@connexer.com) or we backport GCC 4.9 itself into
wheezy. In any case, it's a significant challenge and I'm glad I'm not
the one dealing with such arcane code right now...

[Debian LTS]: https://www.freexian.com/services/debian-lts.html

Other free software work
========================

Not much to say this month, en vrac:

 * did the usual linkchecker maintenance
 * finally got my [Prometheus node exporter directory size sample](https://github.com/prometheus/node_exporter/pull/789)
   merged
 * added some docs [updating](https://github.com/datproject/docs/pull/111) the Dat project comparison with IPFS
   after investigating Dat. Turns out Dat's security garantees aren't
   as good as I hoped... 
 * reviewed some PRs in the Git-Mediawiki project
 * found what I consider to be a security issue in the Borg backup
   software, but was disregarded as such by upstream. This ended up in
   a [simple issue](https://github.com/borgbackup/borg/issues/3628) that I do not hope much from.
 * so I got more interested in the [Restic](https://restic.net/) community as well. I
   [proposed a code of conduct](https://forum.restic.net/t/should-restic-adopt-a-code-of-conduct/445/2) to test the waters, but the
   feedback so far has been mixed, unfortunately.
 * started working on a [streams page](https://github.com/saimn/sigal/issues/303) for the Sigal gallery.
   Expect an article about Sigal soon.
 * published [undertime](https://gitlab.com/anarcat/undertime) in Debian, which brought a slew of bug
   reports (and consequent fixes).
 * started looking at alternative GUIs because GTK2 is going a way and
   I need to port two projects. I have a [list of "hello world"](https://gitlab.com/anarcat/gui-tests) in
   various frameworks now, still not sure which one I'll use.
 * also worked on updating the Charybdis and Atheme-services packages
   with new co-maintainers (hi!)
 * worked with Darktable to try and [render an exotic image](https://discuss.pixls.us/t/out-of-the-box-raw-rendering-not-up-to-jpegs/6669 ) out of
   my new camera. Might turn into a LWN article eventually as well.
 * started getting more involved in the [local free software
   forum](https://forumsdulibre.quebec), a nice little community. In particular, i went to a
   "repair cafe" and wrote a [full report](https://forumsdulibre.quebec/t/petit-retour-sur-le-repair-cafe-de-la-poly/319) on the experience there.

I'm trying to write more for LWN these days so it's taking more
time. I'm also trying to turn those reports into articles to help
ramping up that rhythm, which means you'll need to [subscribe](https://lwn.net/subscribe/Info) to
LWN to get the latest goods before the 2 weeks exclusivity period.

[[!tag debian-planet debian debian-lts python-planet monthly-report]]
