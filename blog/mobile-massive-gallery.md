## nextcloud

docker-compose -f compose-nextcloud.yml up -d
docker-compose -f compose-nextcloud.yml down -v

docker-compose -f compose-nextcloud.yml exec --user www-data app php occ app:enable files_external

docker-compose -f compose-nextcloud.yml exec --user www-data app php occ files_external:create photos local null::null -c datadir=/srv/Photos


docker-compose -f compose-nextcloud.yml exec --user www-data app php occ files:scan --all
root@marcos:/etc/docker# docker-compose -f compose-nextcloud.yml exec --user www-data app php occ files:scan --all
Starting scan for user 1 out of 1 (admin)
^CInterrupted by user
+---------+--------+--------------+
| Folders | Files  | Elapsed time |
+---------+--------+--------------+
| 103298  | 112298 | 00:16:44     |
+---------+--------+--------------+

root@marcos:/etc/docker# docker-compose -f compose-nextcloud.yml down -v --rmi all
Stopping docker_app_1 ... done
Removing docker_app_1 ... done
Removing network docker_default
Removing volume docker_nextcloud
Removing image nextcloud

[Nextcloud Yaga](https://vauvenal5.github.io/yaga-docs/) might be an acceptable alternative?

https://apps.nextcloud.com/apps/previewgenerator
https://rayagainstthemachine.net/linux%20administration/nextcloud-photos/

https://gitlab.com/steviehs/digipics might help in fixing the "albums"?

possibly alternative by bohwaz https://github.com/kd2org/karadav/
https://mamot.fr/@bohwaz/109890893734823557

## photoprism

https://www.photoprism.app/

lack of mobile app
https://github.com/photoprism/photoprism/issues/1666

incompatible flutter app
https://github.com/thielepaul/photoprism-mobile

new app in f-droid works somewhat
okay. https://f-droid.org/en/packages/ua.com.radiokot.photoprism/
([source code](https://github.com/Radiokot/photoprism-android-client)) problems are mostly that images that don't load and
there are performance issues, and jumping around dates is
confusing. there's no offline mode either.

whole thing has an opencore vibe, some features are locked behind
paywall, including in the above mobile app.

requires mariadb, sqlite not recommended


https://docs.photoprism.app/getting-started/docker-compose/

https://docs.photoprism.app/user-guide/first-steps/

root@marcos:/etc/docker# docker-compose -f /home/anarcat/compose-photoprism.yml exec photoprism photoprism index --cleanup

root@marcos:/etc/docker# docker-compose -f /home/anarcat/compose-photoprism.yml up -d

root@marcos:/etc/docker# docker-compose -f /home/anarcat/compose-photoprism.yml exec photoprism photoprism passwd

INFO[2023-02-20T00:25:26Z] indexed 81,622 files in 3h59m47.915792756s


next run:

```
INFO[2023-02-20T16:15:19Z] cleanup: removed 50 index entries and 100 sidecar files [1.964059441s] 
INFO[2023-02-20T16:15:19Z] cleanup: searching for orphaned thumbnails   
INFO[2023-02-20T16:15:20Z] cleanup: removed 449 thumbnail files [766.178938ms] 
INFO[2023-02-20T16:15:20Z] cleanup: removed 549 files in total [2.930403997s] 
INFO[2023-02-20T16:15:20Z] indexed 82,628 files in 26m41.307215045s     
```

noop run:

```
INFO[2023-02-20T16:21:20Z] indexed 82,628 files in 3m46.58783669s       
```

calendar issues (missing parts of 2022): https://github.com/photoprism/photoprism/discussions/3214


no support for XMP tags: https://github.com/photoprism/photoprism/issues/1143
or 5-stars https://github.com/photoprism/photoprism/issues/713
does not write metadata back to disk https://github.com/photoprism/photoprism/issues/402


rescan --force 

```
INFO[2023-02-20T17:54:11Z] indexed 82,692 files in 1h10m28.674348629s   
```

somehow failed to find photos from my camera (but found my phone!) in
august 2021, possibly missing all high quality photos from that
camera. possibly an issue with https://docs.photoprism.app/getting-started/troubleshooting/#missing-pictures

bumped two settings, then:

INFO[2024-08-17T14:35:52Z] faces: updated 231 markers, recognized 151 faces, 81 unknown [16.252126634s]
INFO[2024-08-17T14:35:52Z] index: updated 5,047 files [30m47.140279115s]
INFO[2024-08-17T14:37:57Z] indexed 100,808 files in 32m51.738153382s


      PHOTOPRISM_ORIGINALS_LIMIT: -1               # file size limit for originals in MB (increase for high-res video)
      PHOTOPRISM_RESOLUTION_LIMIT: -1

added videos, amazingly, it was super fast:

```
INFO[2024-08-17T20:15:07Z] indexed 101,914 files in 20m37.481057559s
```

INFO[2024-08-17T22:14:28Z] purge: removed 82 files and 12 photos [2m26.029346119s] 
INFO[2024-08-17T22:14:28Z] indexed 102,615 files in 1h48m21.693727003s  


## others

https://arstechnica.com/gadgets/2021/06/the-big-alternatives-to-google-photos-showdown/

https://photonix.org/

interesting, has a [mobile app](https://play.google.com/store/apps/details?id=org.photonix.mobile) (not in f-droid, but [source
available](https://github.com/photonixapp/photonix-mobile), also [desktop app](https://github.com/photonixapp/photonix-desktop) (probably Electron). main app is
Python + JS.

https://github.com/LibrePhotos/librephotos [mobile apps](https://docs.librephotos.com/2/mobile/) immature.

[lychee](https://github.com/LycheeOrg/Lychee/) [no mobile app](https://github.com/LycheeOrg/Lychee/issues/1013)

[piwigo](https://github.com/Piwigo/) has a [mobile app](https://github.com/Piwigo/Piwigo-Android) but is struggling to keep it up to
date.

## photochiotte

[PhotoChiotte](https://gitlab.com/LaDaubePhotoChiotte/photochiotte) has an awful name but seems like a clever
solution. All the logic is inside the mobile app and the server is a
"dumb" web server with pregenerated thumbnails. It could be a great
solution except the user interface is absolute shit. And I don't mind
using such a strong word since it's literally the name of the software
in French ("chiotte") so I guess it's self-assuming. It's barely
usable, even with just local images. There are weird labels (the
folder names I think?) under each image that take up needless space,
dragging the images around lets you drag into nothingness, it's
really, really hard to use. I haven't actually attempted to generate
the thumbnails.

## ente

[ente](https://ente.io/) just (March 2024) just open-sourced their server and it's
interesting: a relatively generic API (Go + PostgreSQL) server backed
by MinIO object storage backends. They're mostly a business that wants
to host your data, but now it's also clearly an option.

Desktop app is Electron (appimage, [not on Flathub](https://flathub.org/en-GB/apps/search?q=ente)), but they have
a decent web app as well. Trick is we have a large collection that
would cost somewhere between 13$ (500GB) and 30$/mth (2TB) to host on
their servers, so that might be impractical for us, even cost-wise.

Best would be if they would have some sort of CLI sync that would
allow us to sync a local copy, but then maybe that's exactly the
mentality we need to switch away from: just don't keep everything
locally and sync as needed.

Unclear how the mobile app works, but it does have a very exciting
"free up device space" button that probably removes files that were
already uploaded remotely. This would *really* help with the
phone-to-archive workflow.

I haven't tried submitting the stack to the entirety of my collection
just yet, as I only tested the 1GB free trial. It looks pretty fast
though, and the interface feels snappier than Photoprism's,
*especially* since there's a native app for the phone.

Still, this would mean a *major* shift (away from git-annex,
specifically) for photo management, towards object storage
(basically), fully encrypted (which means keys are suddenly a huge
concern). Not sure.

I like the simple design (one golang app + postgresql) especially when
compared to Immich (2 microservices in Typescript/Dart, Redis,
PostgreSQL).

## immich

complicated, lots of microservices, unsure if i want to embark on
testing again.

## photoview

refreshing: single go binary

setup complicated for no good reason, separate .env file complicates
things, stripped it down to just a docker-compose with sqlite (which
should really just be default).

started scan at 2024/08/17 00:38:23 local ended around 2:54:11, but no
clear "scan finished" message that i could find - end of the log is
flooded with SQL queries.

had to fiddle with user permissions in /srv/Photos because that was
just accessilbe to my user, made a photoview user/group, photoprism
container now uses the same group which has +rx on the folder.

interface much more minimalist than photoprism.

no mobile app, on the roadmap at least
https://github.com/photoview/photoview/issues/701, prototype in
https://github.com/photoview/photoview-android

nice in-site progress bar after about 10-20 minutes... lots of errors
about videos.

each folder is treated as an album, which in my case is a bad match as
each folder is a *day*, typically, which makes it hard to share all
images from "a trip", say.

ultimately went back to photoprism after figuring out fix for missing
images. deal breaker is lack of arbitrary album sharing and mobile
app.

## current status

another solution I tried is to use the plain [Simple Gallery](https://github.com/SimpleMobileTools/Simple-Gallery/) to
browse a remote folder, but this is [explicitely not supported](https://github.com/SimpleMobileTools/Simple-Gallery/issues/2422) as
they don't want their app to access the network (and rightly so, I
guess). But there was [this comment](https://github.com/SimpleMobileTools/Simple-Gallery/issues/2422#issuecomment-1126840645) which pointed me at the [Amaze
file manager](https://f-droid.org/en/packages/com.amaze.filemanager/) which actually supports SFTP! Unfortunately, in my
tests it didn't work so well and of course didn't leverage existing
thumbnails...

for now, photoprism it is...  big blocker with missing images.

consider tls client certs.

update: https://apps.nextcloud.com/apps/memories seems to do what we need

[[!tag draft]]
