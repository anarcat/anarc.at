<!-- Drupal node 41 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="De la neige!"]]
[[!meta date="2005-10-30T18:10:50-0500"]]
[[!meta updated="2012-12-14T22:44:23-0500"]]
[[!meta guid="41 at http://anarcat.koumbit.org"]]

Incroyable journée de marche samedi dernier, encore une fois. Cette fois-ci, c'est le sommet de Sutton qui nous a ouvert ses merveilles. Avec 40 à 60cm de neige sur la montagne, la randonnée était radicalement différente des deux précédentes. Nous n'étions pas correctement préparés à cette aventure, mais par chance le sentier était bien dégagé et ouvert par les marcheurs précédents. Nous avons fait le "Round top" (un des sommets) pour continuer sur la crête en faisant le tour des pistes de ski.

J'ai eu l'honneur de guider les aventuriers dans les dédales de sentiers et de routes de la journée, mais le retour a encore une fois été difficile: après une journée de marche, on dirait que j'ai besoin d'une bonne sieste avant de m'asseoir devant un volant sans somnoler. Il faut que je trouve une solution à ça, en fait le plus souvent ça se résume à laisser le volant à un autre... Pas un gros problème. Aussi, une simple sieste de 20 minutes règle habituellement le problème, alors si le temps le permet, tout est possible!

Enfin, je suis bien heureux d'avoir pu traverser cette montagne, journée et party d'halloween de [Mathieu](http://mathieu.koumbit.org/) sans dommage permanent. Évidemment, je crois que la cuisine et l'accueil de ce qui devient une routine alimentaire agréable d'Alison, a été grandement bénéfique.

À la vôtre!

[[!tag "vraie vie" "plein air"]]