[[!meta title="L'Internet néo-colonial"]]

> Cet article est une traduction d'un [[article écrit originalement en
> anglais|blog/2021-10-23-neo-colonial-internet/]]. Merci à
> [Globenet](https://www.globenet.org/) ([leur copie](https://www.globenet.org/L-Internet-neo-colonial.html)). Voir aussi les [notes de la
> traduction](#annexe-notes-de-traduction) pour le contexte particulier de cet article. J'ai
> également fait une entrevue [entrevue avec une radio Slovène](https://radiostudent.si/druzba/tehno-klistir/koncne-meje-neokolonialnega-interneta) sur
> le sujet...

J'ai grandi avec Internet, et son éthique et sa politique ont toujours
été importantes dans ma vie. Mais je me suis également engagé à
d'autres niveaux [contre la brutalité policière][against police brutality], pour [de la
bouffe, pas des bombes][Food, Not Bombs], [l'autonomie des travailleur·euses][koumbit],
les logiciels libres, etc. Longtemps, tout cela m'a paru cohérent.

[software freedom]: https://www.debian.org/
[Food, Not Bombs]: https://foodnotbombs.net/
[against police brutality]: https://cobp.resist.ca/
[koumbit]: https://www.koumbit.org/
[declaration of independence of cyberspace]: https://www.eff.org/cyberspace-independence


Mais plus j'observe l'Internet moderne — et les mégacorporations qui
le contrôlent — et moins j'ai confiance en mon analyse originale du
potentiel libérateur de la technologie. J'en viens à croire que
l'essentiel de notre développement technologique est dommageable pour
la grande majorité de la population de la planète, et bien évidemment
pour reste de la biosphère. Et je ne pense plus que c'est un nouveau
problème.

C'est que l'Internet est un outil néo-colonial, et ce depuis ses
débuts. Je m'explique.

[[!toc]]

# Qu'est-ce que le néo-colonialisme ?

Le terme « [néo-colonialisme][neo-colonialism] » a été inventé par [Kwame
Nkrumah][], premier président du [Ghana][]. Dans *Le
néo-colonialisme : Dernier stade de l'impérialisme* (1965), il écrit :

[Ghana]: https://en.wikipedia.org/wiki/Ghana
[Kwame Nkrumah]: https://en.wikipedia.org/wiki/Kwame_Nkrumah
[neo-colonialism]: https://en.wikipedia.org/wiki/Neocolonialism

> À la place du colonialisme, principal instrument de l'impérialisme,
> nous avons aujourd'hui le néo-colonialisme […] [qui], comme le
> colonialisme, est une tentative d'exporter les conflits sociaux des
> pays capitalistes. […]
>
> Le résultat du néo-colonialisme est que le capital étranger est
> utilisé pour l'exploitation plutôt que pour le développement des
> régions moins développées du monde.
>
> L'investissement, sous le néo-colonialisme, accroît, au lieu de le
> réduire, le fossé entre les pays riches et les pays
> pauvres. (Traduction libre)

En résumé, si le [colonialisme][colonialism], ce sont des Européens apportant
génocide, guerre et religion au reste du monde, le néo-colonialisme,
ce sont des Américains ramenant le capitalisme au reste du monde.

[colonialism]: https://en.wikipedia.org/wiki/Colonialism

Avant de voir comment cela s'applique à l'Internet, nous devons par
conséquent faire un petit détour par l'histoire des États-Unis. C'est
important car il serait difficile pour quiconque de séparer le
néo-colonialisme de l'empire sous lequel il se développe, et ici nous
ne pouvons que nommer les États-Unis d'Amérique.

# Déclaration d'indépendance des États-Unis

Commençons avec la [Déclaration d'indépendance des États-Unis][United States declaration of independence]
(1776). Beaucoup d'Américain·es pourraient s'en étonner, parce que
cette déclaration ne fait pas réellement partie de la [constitution des
États-Unis][US constitution] et sa légitimité juridique est donc discutable. Pour autant, elle
a tout de même été une force philosophique influente sur la fondation
de la nation. Comme son auteur, Thomas Jefferson, l'affirmait :

[US constitution]: https://en.wikipedia.org/wiki/Constitution_of_the_United_States
[United States declaration of independence]: https://en.wikipedia.org/wiki/United_States_Declaration_of_Independence

> Cela devait être une expression de l'esprit américain, et donner à
> cette expression le ton et l'esprit requis par
> l'occasion. (traduction libre)

Dans ce document vieillissant, on trouve la perle suivante :

> Nous tenons pour évidentes pour elles-mêmes les vérités suivantes :
> tous les hommes sont créés égaux ; ils sont dotés par le Créateur de
> certains droits inaliénables, parmi lesquels figurent la Vie, la
> Liberté et la recherche du Bonheur.

NDLTr: nous avons choisi la [traduction de Wikipedia](https://fr.wikisource.org/wiki/D%C3%A9claration_unanime_des_treize_%C3%89tats_unis_d%E2%80%99Am%C3%A9rique), mais vous
pouvez aussi consulter la [traduction de Jefferson](https://www.state.gov/wp-content/uploads/2020/02/French-translation-U.S.-Declaration-of-Independence.pdf).

En tant que document fondateur, la Déclaration a toujours un impact
dans le sens que la citation précédente a été qualifiée de :

> « déclaration immortelle », et « peut-être [la] seule phrase » de la
> période de la Révolution américaine avec une telle « importance
> persistante ». ([Wikipédia](https://en.wikipedia.org/wiki/All_men_are_created_equal), traduction libre)

Relisons cette « déclaration immortelle » : « tous les hommes sont
créés égaux ». « Hommes », dans ce contexte, est limité à un certain
nombre de personnes, à savoir « [propriétaires ou contribuables mâles
blancs, soit environ 6% de la population][property-owning or tax-paying white males, or about 6% of the population] ». À l'époque de la
rédaction, les femmes n'avaient pas le droit de vote, et l'esclavage
était légal. Jefferson lui-même possédait des centaines d'esclaves.

[property-owning or tax-paying white males, or about 6% of the population]: https://en.wikipedia.org/wiki/Voting_rights_in_the_United_States#Milestones_of_national_franchise_changes

La déclaration était adressée au [Roi][King] et était une [liste de
doléances][list of grievances]. L'une des préoccupations des colons était que le Roi :

[list of grievances]: https://en.wikipedia.org/wiki/Grievances_of_the_United_States_Declaration_of_Independence
[King]: https://en.wikipedia.org/wiki/George_III_of_the_United_Kingdom

> a excité parmi nous des insurrections domestiques, et il a cherché à
> attirer sur les habitants de nos frontières les Indiens, ces
> sauvages sans pitié, dont la manière bien connue de faire la guerre
> est de tout massacrer, sans distinction d'âge, de sexe ni de
> condition.

Voilà un signe évident du [mythe de la Frontière][frontier myth], qui a ouvert la
voie à la colonisation du territoire — et à l'extermination de ses
peuples — que certain·es nomment maintenant les États-Unis
d'Amérique.

[frontier myth]: https://en.wikipedia.org/wiki/Frontier_myth

La Déclaration d'indépendance est évidemment un document colonial,
puisqu'elle a été écrite par des colons. Rien de tout cela n'est
vraiment surprenant, historiquement parlant, mais il est bon de
rappeler l'origine d'Internet, étant né aux États-Unis.

# Déclaration d'indépendance du cyberespace

Deux cent vingt ans plus tard, en 1996, [John Perry Barlow][] écrit une
[déclaration d'indépendance du cyberespace][declaration of independence of cyberspace]. À ce stade, (presque) tout
le monde a le droit de vote (y compris les femmes), l'esclavage est
aboli (bien que certain·es considèrent qu'il [existe toujours sous la
forme du système carcéral][some argue it still exists in the form of the prison system]) ; les États-Unis ont fait d'énormes
progrès. Sûrement le texte a bien mieux vieilli que la déclaration
précédente, dont il s'est clairement inspiré. Voyons comment il peut
être lu aujourd'hui et comment il correspond à la manière dont
Internet est réellement construit aujourd'hui.

[some argue it still exists in the form of the prison system]: https://en.wikipedia.org/wiki/Angela_Davis#Political_activism_and_speeches
[almost]: https://en.wikipedia.org/wiki/Voting_rights_in_the_United_States
[John Perry Barlow]: https://en.wikipedia.org/wiki/John_Perry_Barlow_

# Frontières de l'indépendance

L'une des idées clé que Barlow apporte est que « le cyberespace ne se
situe pas dans vos frontières ». En ce sens, le cyberespace est
[l'ultime frontière][final frontier] : n'ayant pas réussi à coloniser la lune, le
peuple Américain s'est replié sur lui-même, plus profondément dans la
technologie, mais toujours avec l'idéologie de la frontière. Et
d'ailleurs, Barlow est un des cofondateurs de l'Electronic Frontier
Foundation (la bien-aimée [EFF][]), fondée six ans auparavant.

[EFF]: https://en.wikipedia.org/wiki/Electronic_Frontier_Foundation
[final frontier]: https://en.wikipedia.org/wiki/Final_Frontier

Mais il y a d'autres problèmes avec cette idée. Comme le [cite Wikipédia][wikipedia quotes] :

[Wikipedia quotes]: https://en.wikipedia.org/wiki/A_Declaration_of_the_Independence_of_Cyberspace#Critical_response

> La déclaration a été critiquée pour ses incohérences internes
> [[9](https://web.archive.org/web/20161105174638/http://ieet.org/index.php/IEET/more/evans0220)]. L'affirmation de la déclaration que le « cyberespace » est
> un endroit retiré du monde physique a aussi été contestée par des
> personnes qui ont pointé le fait qu'Internet reste toujours lié à sa
> géographie sous-jacente [[10](https://doi.org/10.1111%2Fgeoj.12009)].

Et en effet, l'Internet est indéniablement un objet physique. Au
départ contrôlé et sévèrement limité par les entreprises de
télécommunications comme AT&T, il a été quelque peu « libéré » de ce
monopole en 1982 lorsqu'un [procès][anti-trust lawsuit] a [mis fin au monopole][broke up the monopoly], un
événement clé qui a vraisemblablement rendu l'Internet possible.

[broke up the monopoly]: https://en.wikipedia.org/wiki/Breakup_of_the_Bell_System
[anti-trust lawsuit]: https://en.wikipedia.org/wiki/United_States_v._AT%26T

(À partir de là, les fournisseurs de « dorsale » ont pu se lancer sur
le marché de la concurrence et croître, pour éventuellement 
fusionner en de nouveaux monopoles : Google a le monopole des moteurs
de recherche et de la publicité, Facebook de la communication pour
quelques générations, Amazon du stockage et de la capacité de
traitement, Microsoft du matériel, etc. Même AT&T est maintenant
presqu'aussi consolidé qu'auparavant.)

En clair : toutes ces compagnies possèdent de gigantesques centres de
données et des câbles intercontinentaux. Ceux-ci priorisent le monde
occidental, le cœur de cet empire. Pour donner un exemple, le [dernier
câble sous-marin de 7000 km de Google][google's latest 3,900 mile undersea cable] ne connecte pas l'Argentine à
l'Afrique du Sud ou à la Nouvelle-Zélande, il relie les États-Unis au
Royaume-Uni et à l'Espagne. Pas vraiment une fibre révolutionnaire.

[Google's latest 3,900 mile undersea cable]: https://www.cnet.com/tech/google-finishes-3900-mile-subsea-cable-connecting-us-to-uk-and-spain/

# Internet privé

Mais revenons à la Déclaration :

> Ne pensez pas que vous pouvez le construire, comme si c’était un
> projet de travaux publics. Vous ne le pouvez pas. C’est un phénomène
> naturel, et il croît par lui même au travers de nos actions collectives.

Dans la pensée de Barlow, le « public » est mauvais et le privé est
bon, naturel. Ou, en d'autres termes, un « projet de travaux publics »
n'est pas naturel. Et effectivement, la « nature » moderne du
développement est privée : la plus grande part de l'Internet est
maintenant possédée et opérée par le privé.

Je dois reconnaître qu'en tant qu'anarchiste, j'ai adoré cette phrase
quand je l'ai lue. J'étais pour « nous », les opprimés, les
révolutionnaires. Et, en un sens, c'est toujours le cas : je suis au
conseil d'administration de [Koumbit][] et travaille pour un
[organisme à but non lucratif][non-profit] qui s'est réorienté contre la
censure et la surveillance. Et maintenant, je ne peux m'empêcher de
penser que, collectivement, nous n'avons pas réussi à établir cette
indépendance et avons trop fait confiance aux entreprises
privées. Rétrospectivement, c'est évident, mais ça ne l'était pas
il y a trente ans.

[non-profit]: https://www.torproject.org/

Internet n'a désormais aucun comptes à rendre aux pouvoirs politiques
traditionnels supposés représenter le peuple, ou seulement même ses
utilisateur·ices. La situation est en fait pire que lorsque les
États-Unis ont été fondés (à savoir quand "6 % de la population avait
le droit de vote"), car les propriétaires des géants de la techno ne
sont qu'une poignée de personnes qui ont le pouvoir d'outrepasser
toute décision. Il n'y a qu'un seul patron chez Amazon, il s'appelle
[Jeff Bezos][] et il a un contrôle total.

[Jeff Bezos]: https://en.wikipedia.org/wiki/Jeff_Bezos

# Contrat social

Voici une autre affirmation de la Déclaration :

> Nous formons notre propre Contrat Social.

Je me souviens des premiers jours, à l'époque où « [nétiquette][netiquette] »
était un mot commun, on avait le sentiment d'avoir une sorte de
contrat. Évidemment pas écrit comme un "standard" — ou alors de
justesse (voir la [RFC1855][]) — mais comme un accord tacite. Comme
nous avions tort! Il suffit de regarder Facebook pour comprendre à
quel point cette idée est problématique à l'échelle d'un réseau
global.

[RFC1855]: https://datatracker.ietf.org/doc/html/rfc1855
[netiquette]: https://en.wikipedia.org/wiki/Etiquette_in_technology#Netiquette

Facebook est la quintessence de l'idéologie « hacker » mise en
pratique. Mark Zuckerberg a [explicitement refusé][explicitly refused] d'être l'« arbitre de
la vérité », ce qui implique qu'il laissera les
mensonges proliférer sur ses plateformes.

[explicitly refused]: https://www.theguardian.com/technology/2020/may/28/zuckerberg-facebook-police-online-speech-trump

Il voit également Facebook comme un endroit où tout le monde est égal,
ce qui fait écho à la Déclaration :

> Nous créons un monde où tous peuvent entrer sans privilège ou
> préjugés découlant de la race, du pouvoir économique, de la force
> militaire ou du lieu de naissance.

(Notons au passage l'omission du genre dans cette liste, répétant le
fameux tristement célèbre « Tous les hommes sont créés égaux » de la
déclaration des États-Unis.)

Comme les « [Facebook files][] » du Wall Street Journal (WSJ) l'ont
montré plus tard, ces "contrats sociaux" sont bien limités au sein de
Facebook. Il y a des [célébrités qui échappent systématiquement aux
contrôles][vips who systematically bypass moderation systems], y compris des [fascistes][fascists] et des [violeurs][rapists]. Les
cartels de drogue et les trafiquants de personnes [prospèrent sur la
plateforme][thrive on the platform]. Zuckerberg a lui-même essayé de dompter la plateforme
— pour la rendre plus [saine][healthier] ou pour promouvoir la
[vaccination][vaccinated] — et il a échoué : Facebook est devenu « encore plus
fâchée » et les conspirations « antivax » pullulent.

[healthier]: https://www.wsj.com/articles/facebook-algorithm-change-zuckerberg-11631654215?mod=article_inline
[vaccinated]: https://www.wsj.com/articles/facebook-mark-zuckerberg-vaccinated-11631880296?mod=article_inline
[thrive on the platform]: https://www.wsj.com/articles/facebook-drug-cartels-human-traffickers-response-is-weak-documents-11631812953?mod=article_inline
[rapists]: https://en.wikipedia.org/wiki/Neymar
[fascists]: https://en.wikipedia.org/wiki/Trumpism
[VIPs who systematically bypass moderation systems]: https://www.wsj.com/articles/facebook-files-xcheck-zuckerberg-elite-rules-11631541353
[Facebook files]: https://www.wsj.com/articles/the-facebook-files-11631713039

C'est que le « contrat social » derrière Facebook et ces grandes
compagnies est un mensonge : leur préoccupation est le profit et cela
passe par la publicité, l'« engagement » avec la plateforme, ce qui
[provoque une augmentation de l'anxiété et de la dépression chez les
adolescent·es][causes increased anxiety and depression in teens], par exemple.

[causes increased anxiety and depression in teens]: https://www.bbc.com/news/technology-58570353

La réponse de Facebook est qu'elle travaille vraiment fort sur la
modération. Mais la vérité est que même ce système est sévèrement
biaisé. Le WSJ a montré que Facebook n'a de traducteur·ices que pour
50 langues. Il est [étonnamment difficile de compter les langues][surprisingly hard to count human languages], mais
les estimations de leur nombre oscillent entre 3 000 et 7 000 langues
vivantes. Donc si, à première vue, 50 langues, ça semble important,
c'est en fait une infime fraction de la population utilisant
Facebook. Si on prend la [liste Wikipédia des 50 premières langues par
nombre de locuteur·ices][wikipedia list of languages by native speakers], on écarte le néerlandais (52<sup>e</sup>), le
grec (74<sup>e</sup>) et le hongrois (78<sup>e</sup>), et il ne s'agit
là que de quelques nations choisies au hasard en Europe.

[Wikipedia list of languages by native speakers]: https://en.wikipedia.org/wiki/List_of_languages_by_number_of_native_speakers
[surprisingly hard to count human languages]: https://www.linguisticsociety.org/content/how-many-languages-are-there-world

Facebook a par exemple des difficultés à modérer même une langue
majeure comme l'arabe. La plateforme a censuré du contenu en
provenance de médias arabes légitimes quand ils ont mentionné le mot
[al-Aqsa][], car Facebook l'associe avec les [Brigades des martyrs
d'Al-Aqsa][al-aqsa martyrs' brigades], alors que ces médias parlaient de la [mosquée
al-Aqsa][al-aqsa mosque]… Ce biais contre les Arabes montre comment Facebook
reproduit la politique coloniale américaine.

[Al-Aqsa Mosque]: https://en.wikipedia.org/wiki/Al-Aqsa_Mosque
[al-Aqsa Martyrs' Brigades]: https://en.wikipedia.org/wiki/Al-Aqsa_Martyrs%27_Brigades
[al-Aqsa]: https://en.wikipedia.org/wiki/Al-Aqsa

Le WSJ souligne également que Facebook consacre seulement 13 % de ses
ressources de modération en dehors des États-Unis, même si ça
représente 90 % de ses utilisateur·ices. Facebook passe trois fois
plus de temps à modérer sur la « sécurité des marques », ce qui montre
que sa priorité n'est pas la sécurité de ses utilisateur·ices, mais
celle des publicitaires.

# Internet militaire

[Sergey Brin][] et [Larry Page][] sont les "[Lewis and Clark][]" de
notre génération. Tout comme ces derniers ont été envoyés par
Jefferson (le même) pour déclarer la souveraineté sur toute la côte
ouest des États-Unis, Google a déclaré la souveraineté sur tout le
savoir humain, avec sa déclaration de mission en s'attribuant la
mission d'« organiser l'information du monde et [de] la rendre
universellement accessible et utile ». (Il convient de noter que Page
a quelque peu [remis en question][questioned that mission] cette mission, mais uniquement
parce qu'elle n'était pas assez ambitieuse, Google l'ayant « dépassée
».)

[questioned that mission]: https://www.theguardian.com/technology/2014/nov/03/larry-page-google-dont-be-evil-sergey-brin
[Lewis and Clark]: https://en.wikipedia.org/wiki/Lewis_and_Clark_Expedition
[Larry Page]: https://en.wikipedia.org/wiki/Larry_Page
[Sergey Brin]: https://en.wikipedia.org/wiki/Sergey_Brin

L'expédition Lewis et Clark, tout comme Google, avait un prétexte
scientifique, car c'est ainsi qu'on colonise un monde,
supposément. Pourtant, les deux hommes étaient des militaires et
ont dû recevoir une formation scientifique avant de partir. Le "[Corps
of Discovery][]" était composé de quelques dizaines de militaires et
d'une douzaine de civils, dont [York][], un esclave afro-américain
possédé par Clark et vendu après l'expédition, son destin ultime noyé
dans l'Histoire.

[Corps of Discovery]: https://en.wikipedia.org/wiki/Corps_of_Discovery
[York]: https://en.wikipedia.org/wiki/York_(explorer)

Et tout comme Lewis et Clark, Google a des liens serrés avec les
militaires. Par exemple, Google Earth n'a pas été construit à
l'origine par Google mais est le fruit de l'acquisition d'une société
appelée Keyhole qui avait des [liens avec la CIA][ties with the cia]. Ces liens ont
été [intégrés à Google lors de l'acquisition][brought inside google during the acquisition]. L'investissement
croissant de Google au sein du complexe militaro-industriel a
finalement conduit les travailleur·euses de Google à organiser une
révolte, si bien que je ne sache pas exactement à quel point Google est
impliqué dans l'appareil militaire en ce moment. (Mise à jour: [cet
article de Novembre 2021](https://cloud.google.com/blog/topics/inside-google-cloud/update-on-google-clouds-work-with-the-us-government) indique qu'ils vont "fièrement travailler
avec le département de la défense".) D'autres entreprises,
évidemment, n'ont pas une telle réserve : Microsoft, Amazon et
beaucoup d'autres, répondent à des offres de contrats militaires avec
un enthousiasme débordant.

[organizing a revolt]: https://en.wikipedia.org/wiki/Google_worker_organization
[brought inside Google during the acquisition]: https://pando.com/2014/03/07/the-google-military-surveillance-complex/
[ties with the CIA]: https://en.wikipedia.org/wiki/Google_Earth#History

# Propagation d'Internet

Je ne suis évidemment pas le premier à identifier des structures
coloniales dans l'Internet. Dans un article intitulé « [The Internet
as an Extension of Colonialism][] » (« L'Internet comme extension du
colonialisme »), Heather McDonald identifie à juste titre les
problèmes fondamentaux liés au « développement » de nouveaux « marchés
» de « consommateurices » de l'Internet, en faisant valoir
principalement qu'il crée une [fracture numérique][] qui engendre un «
manque d'autonomie et de liberté individuelle » :

[fracture numérique]: https://fr.wikipedia.org/wiki/Fracture_num%C3%A9rique_(g%C3%A9ographique)
[The Internet as an Extension of Colonialism]: https://thesecuritydistillery.org/all-articles/the-internet-as-an-extension-of-colonialism

> De nombreuses personnes africaines ont désormais accès à ces
> technologies, mais pas à la liberté de développer à leur manière des
> contenus tels que des pages web ou des plateformes de médias
> sociaux. Les natif·ves du numérique ont beaucoup plus de pouvoir et
> l'utilisent donc pour créer leur propre espace avec leurs propres
> normes, façonnant leur monde en ligne en fonction de leurs propres
> perspectives.

Mais la fracture numérique n'est certainement pas le pire problème
auquel nous devons faire face sur Internet aujourd'hui. Pour en
revenir à la Déclaration, nous pensions à l'origine que nous créions
un monde entièrement nouveau :

> Cette gouvernance émergera des conditions de notre monde, pas du
> vôtre. Notre monde est différent.

Comme [j'aurais aimé que ce soit vrai][]. Malheureusement, l'Internet
n'est pas si différent du monde hors ligne. Ou, pour être plus précis,
les valeurs que nous avons intégrées à l'Internet, notamment la
liberté d'expression absolue, le sexisme, le corporatisme et
l'exploitation, sont en train d'exploser en dehors d'Internet, dans le
monde « réel ».

[j'aurais aimé que ce soit vrai]: https://fr.wikipedia.org/wiki/Septembre_%C3%A9ternel

L'internet a été construit avec des logiciels libres qui,
fondamentalement, reposent sur le travail quasi bénévole d'une élite
d'hommes blancs ayant manifestement trop de temps libre (et aussi :
pas d'enfants). L'écriture mythique de GCC et d'Emacs par Richard
Stallman en est un bon exemple, mais l'intégralité d'Internet semble
aujourd'hui fonctionner sur des morceaux disparates construits par des
programmeur·euses à la sauvette travaillant sur leur généreux temps
libre. Dès que l'un de ces éléments [tombe en panne][fails], il peut
[compromettre][compromise] ou faire tomber des systèmes entiers. (M'enfin, j'ai
écrit cet article pendant mon jour de repos… [NDLT: et que dire des
traducteur·ices?!])

[compromise]: https://lwn.net/Articles/773121/
[fails]: https://www.davidhaney.io/npm-left-pad-have-we-forgotten-how-to-program/

Ce modèle — qui est fondamentalement du « *cheap labour* » —
se répand au-delà d'Internet. Les livreur·euses sont exploité·es
jusqu'à l'os par des applications comme Uber — même s'il convient de
noter que ces gens [s'organisent et se défendent][organise and fight back]. Les conditions des
centre de distribution d'Amazon dépassent l'imagination, avec
l'interdiction de prendre des pauses jusqu'au point qu'on doive
[pisser dans des bouteilles][pee in bottles], avec des [ambulances prêtes à évacuer
les corps][ambulances nearby to carry out the bodies]. Au plus fort de la pandémie, le personnel était
[dangereusement exposé au virus][dangerously exposed to the virus]. Tout cela alors qu'Amazon est
plus oui moins en train de prendre le contrôle de [l'ensemble de
l'économie][the entire economy].

[the entire economy]: https://www.vice.com/en/article/7xpgvx/amazons-is-trying-to-control-the-underlying-infrastructure-of-our-economy
[dangerously exposed to the virus]: https://www.politico.eu/article/coronavirus-amazon-employees-rage/
[ambulances nearby to carry out the bodies]: https://www.nytimes.com/2015/08/16/technology/inside-amazon-wrestling-big-ideas-in-a-bruising-workplace.html
[pee in bottles]: https://www.businessinsider.com/amazon-warehouse-workers-have-to-pee-into-bottles-2018-4
[organise and fight back]: https://www.curbed.com/article/nyc-delivery-workers.html

La Déclaration culmine avec cette prophétie :

> Nous nous propagerons sur la planète afin que personne ne puisse
> arrêter nos pensées.

Cette prédiction, qui semblait d'abord révolutionnaire, donne
maintenant froid dans le dos.

# Internet colonial

Internet est, sinon néo-colonial, tout bonnement colonial. Les
colonies avaient des champs de coton et des esclaves, nous avons des
[portables jetables][disposable cell phones] et [Foxconn][Foxconn workers]. Le Canada a son [génocide
culturel][cultural genocide], Facebook a ses propres génocides en [Éthiopie][Ethiopia], au
[Myanmar][Myanmar], et provoque des lapidations en [Inde][India]. Apple accepte
implicitement le [génocide ouïghour][Uyghur genocide]. Et tout comme les esclaves de
la colonie, ces atrocités sont ce qui fait fonctionner l'empire.

[Uyghur genocide]: https://en.wikipedia.org/wiki/Uyghur_genocide
[India]: https://www.buzzfeednews.com/article/pranavdixit/whatsapp-destroyed-village-lynchings-rainpada-india
[Myanmar]: https://www.salon.com/2020/08/20/facebook-is-a-global-threat-to-public-health-avaaz-report-says/
[Ethiopia]: https://www.vice.com/en_us/article/xg897a/hate-speech-on-facebook-is-pushing-ethiopia-dangerously-close-to-a-genocide
[cultural genocide]: https://www.thecanadianencyclopedia.ca/en/article/genocide-and-indigenous-peoples-in-canada
[Foxconn workers]: https://www.theguardian.com/technology/2017/jun/18/foxconn-life-death-forbidden-city-longhua-suicide-apple-iphone-brian-merchant-one-device-extract
[disposable cell phones]: https://www.jacobinmag.com/2015/03/smartphone-usage-technology-aschoff/

La Déclaration se termine en fait comme ceci, une citation que j'ai
dans mon [fichier de citations](https://anarc.at/fortunes.txt) :

> Nous créerons une civilisation de l'Esprit dans le
> Cyberespace. Puisse-t-elle être plus humaine et plus juste que le
> monde que vos gouvernements ont créé auparavant.

Ça demeure une source d'inspiration pour moi. Mais si nous voulons
rendre le « cyberespace » plus humain, nous devons le
décoloniser. Travailler à la cyberpaix plutôt qu'à la
cyberguerre. Établir un code de conduite clair, discuter de l'éthique
et remettre en question ses propres biais, privilèges et sa culture. Pour
moi, la première étape de la décolonisation de mon esprit est
l'écriture de cet article. Briser les monopoles technologiques est
peut-être une étape importante, mais cela ne suffira pas : nous devons
également opérer un changement de culture, et c'est le nœud le plus
difficile à défaire.

# Appendice : excuses à Barlow

J'ai quelque peu mauvaise conscience de cribler la déclaration
de Barlow comme ça, point par point. C'est un peu injuste, d'autant
plus que Barlow est décédé il y a quelques années et qu'il ne peut pas
élaborer une réponse (même en supposant humblement qu'il pourrait
lire ceci). Mais d'un autre côté, il a lui-même reconnu en 2009 qu'il
était un peu trop « optimiste », [disant][saying] que « tout le monde devient
plus mature et intelligent » :

[saying]: https://reason.com/2004/08/01/john-perry-barlow-20-2/

> Je suis un optimiste. Pour être "libertarien", il faut être
> optimiste. Il faut avoir une vision bienveillante de la nature
> humaine, pour croire que les êtres humains livrés à eux-mêmes sont
> fondamentalement bons. Mais je n'en suis pas si sûr pour les
> institutions humaines, et je pense que le vrai sujet de discussion
> ici est de savoir si oui ou non les grandes entreprises sont des
> institutions humaines ou une autre entité que nous devons penser à
> restreindre. La plupart des libertarien s'inquiètent du gouvernement
> mais pas des entreprises. Je pense que nous devons nous inquiéter
> des entreprises exactement de la même manière que nous nous
> inquiétons du gouvernement.

Et, dans un sens, c'était un peu naïf de s'attendre à ce que Barlow ne
soit pas un colon. Barlow est, entre autres, un éleveur qui a grandi
dans un ranch colonial du Wyoming. Le ranch a été fondé en 1907 par
son grand-oncle, 17 ans après l'entrée de l'État dans l'Union, et
seulement une ou deux générations après la [guerre de Powder River][Powder River War]
(1866-1868) et la [guerre des Black Hills][Black Hills War] (1876-1877), au cours
desquelles les États-Unis ont volé les terres occupées par les
Lakotas, les Cheyennes, les Arapahos et d'autres nations autochtones,
dans le cadre des dernières [guerres aux Premières Nations][First Nations Wars].

[First Nations Wars]: https://en.wikipedia.org/wiki/American_Indian_Wars
[Black Hills War]: https://en.wikipedia.org/wiki/Great_Sioux_War_of_1876
[Powder River War]: https://en.wikipedia.org/wiki/Red_Cloud%27s_War

# Annexe : articles connexes

Il existe un autre article qui porte presque le même titre que
celui-ci : « [Facebook and the New Colonialism][] » (« Facebook et le
nouveau colonialisme »). (Curieusement, la balise `<title>` de
l'article est en fait « Facebook l'empire colonial », ce que je trouve
également approprié).  L'article mérite d'être lu dans son
intégralité, mais j'ai tellement aimé cette citation que je n'ai pas
pu résister à l'envie de la reproduire ici :

[Facebook and the New Colonialism]: https://www.theatlantic.com/technology/archive/2016/02/facebook-and-the-new-colonialism/462393/

> Les représentations du colonialisme sont depuis longtemps présentes
> dans les paysages numériques. (« Même Super Mario Brothers », me
> disait le concepteur de jeux vidéo Steven Fox l'an dernier. « Vous
> courez dans le paysage, vous piétinez tout, et vous hissez votre
> drapeau à la fin. »). Mais le colonialisme sur Internet n'est pas
> une abstraction. Les forces qui façonnent un nouveau type
> d'impérialisme vont au-delà de Facebook.

Cela continue ainsi :

> Prenons, par exemple, les projets de numérisation qui se concentrent
> principalement sur la littérature anglaise. Si le web est
> censé être la nouvelle bibliothèque d'Alexandrie de l'humanité, un
> dépôt vivant de toutes les connaissances de l'humanité, c'est un
> problème. De même, la grande majorité des pages Wikipédia portent
> sur une portion relativement minuscule de la planète. Par exemple,
> 14 % de la population mondiale vit en Afrique, mais moins de 3 % des
> articles géolocalisés de Wikipédia en sont originaires, selon un
> rapport de l'Oxford Internet Institute datant de 2014.

Et elle présente une autre définition du néo-colonialisme, tout en
mettant en garde contre l'abus du mot, comme je le fais en quelque
sorte ici :

> « Je répugne à lancer des mots comme “colonialisme”, mais il est
> difficile d'en ignorer les airs de famille et l'ADN reconnaissable », a
> déclaré Deepika Bahri, professeure d'anglais à l'université Emory,
> qui se concentre sur les études postcoloniales. Dans un courriel,
> Mme Bahri a résumé ces similitudes sous forme de liste :
>
> 1. fait son entrée comme sauveur
> 2. brandit des mots comme égalité, démocratie, droits fondamentaux
> 3. dissimule le mobile du profit à long terme (voir le point 2 ci-dessus)
> 4. justifie la logique de la diffusion partielle comme étant mieux que rien
> 5. s'associe aux élites locales et aux intérêts particuliers
> 6. accuse les critiques d'ingratitude
>
> « En fin de compte, m'a-t-elle dit, si ce n'est pas un canard, ça ne
> devrait pas cancaner comme un canard. »

Une autre bonne lecture est le classique "[Code and other laws of
cyberspace][]" (Code et autres lois du cyberespace, 1999, [PDF
gratuit][free pdf]), qui est également critique de la déclaration de
Barlow. Dans Code is law, Lawrence Lessig soutient que

[free PDF]: https://lessig.org/images/resources/1999-Code.pdf
[Code and other laws of cyberspace]: https://lessig.org/product/code

> le code informatique (ou « code de la côte ouest », en référence à
> la Silicon Valley) régit la conduite de la même manière que le code
> juridique (ou « code de la côte est », en référence à Washington,
> D.C.)  ([Wikipedia](https://en.wikipedia.org/wiki/Code_and_Other_Laws_of_Cyberspace)).

Et à présent, on a l'impression que la côte ouest a gagné sur la côte
est, ou peut-être l'a-t-elle recolonisée. En tout cas, Internet
[couronne désormais les empereurs][christens emperors].

[christens emperors]: https://en.wikipedia.org/wiki/2016_United_States_presidential_election

# Annexe: notes de traduction

Alors. Des camarades bienveillant·es et extrêmement généreux·ses
(merci [Globenet](https://www.globenet.org/)!) ont
fignolé le gros de cette traduction. J'ai copié-collé sur mon blogue,
fait quelques modifications, ajouté des liens, et pouf, je parle
français à nouveau. C'est franchement bizarre d'écrire même cette note
ici parce que ça fait des lustres que j'ai pas écrit d'article en
français (la honte). Mais plus important encore, je ne sais pas si
j'aurais écrit cet article de cette façon du tout, si j'avais commencé
en français.

De un, les notions de colonialismes sont complètement différentes au
Québec: les francophones ici ont l'idée particulière d'être des
*victimes* de la colonisation (des anglais), omettant évidemment la
partie où ils ont participé au génocide, mais bon, c'est compliqué. 

De deux, que dire du passé colonial de la France! Il faudrait parler
de l'Afrique, de l'Asie, et bien sûr aussi de l'Amérique et des
Antilles... Sans parler que pour écrire cet article du point de vue de
la France demanderait aussi de parler du minitel, de FDN, et j'en
passe...

Bref, à prendre avec un certain grain de sel, et je regrette ne pas
écrire d'avantage en français. Mes excuses au lectorat de ma langue
natale...

[[!tag français histoire internet politique colonialisme analyse traduction]]
