<!-- Drupal node 4 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Tous unis contre Wal-Marde!"]]
[[!meta date="2005-06-30T11:35:35-0500"]]
[[!meta updated="2012-12-14T22:23:06-0500"]]
[[!meta guid="4 at http://anarcat.koumbit.org"]]

Contrairement à Jonquière qui s'est [fait fermer son Wal-Marde dans la face](http://www.radio-canada.ca/regions/saguenay-lac/nouvelles/200502/09/004-arbitrage_wal-mart.shtml), Vancouver prend les devants et [refuse carrément l'entrée de Wal-Mart sur son territoire](http://www.radio-canada.ca/nouvelles/Index/nouvelles/200506/30/003-WALMARTVANC.shtml), comme 300 autres villes nord-américaines. Un grand pas pour cette ville en explosion... Reste juste à syndiquer les quelques War-malt de Montréal.

[[!tag "nouvelles" "syndicalisme"]]