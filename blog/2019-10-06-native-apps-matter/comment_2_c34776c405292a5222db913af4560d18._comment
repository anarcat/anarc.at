[[!comment format=mdwn
 ip="213.219.166.29"
 claimedauthor="Frans"
 url="https://fransdejonge.com"
 subject="Higher quality YouTube is DASH-only"
 date="2019-10-07T08:09:01Z"
 content="""
Unless something changed recently, VLC is limited to 720p. That won't truly affect any conclusions, but for a completely fair comparison you'd have to take down YT to 720p, specifically in mp4 so any potential hardware acceleration can do its job on the browser too. On Chromia you might have to flip a flag to enable that at all.

Alternatively and for a better video viewing experience, you can download or pipe better quality video/audio with youtube-dl.

```
$ youtube-dl -F \"https://www.youtube.com/watch?v=tyOHcXKlyU8\"  
[youtube] tyOHcXKlyU8: Downloading webpage
[youtube] tyOHcXKlyU8: Downloading video info webpage
WARNING: Unable to extract video title
[info] Available formats for tyOHcXKlyU8:
format code  extension  resolution note
249          webm       audio only DASH audio   60k , opus @ 50k, 6.30MiB
250          webm       audio only DASH audio   86k , opus @ 70k, 8.18MiB
140          m4a        audio only DASH audio  131k , m4a_dash container, mp4a.40.2@128k, 15.53MiB
251          webm       audio only DASH audio  164k , opus @160k, 15.87MiB
160          mp4        256x144    144p  109k , avc1.4d400c, 24fps, video only, 6.70MiB
278          webm       256x144    144p  118k , webm container, vp9, 24fps, video only, 11.36MiB
242          webm       426x240    240p  229k , vp9, 24fps, video only, 13.57MiB
133          mp4        426x240    240p  294k , avc1.4d4015, 24fps, video only, 12.39MiB
243          webm       640x360    360p  401k , vp9, 24fps, video only, 22.38MiB
134          mp4        640x360    360p  579k , avc1.4d401e, 24fps, video only, 22.12MiB
244          webm       854x480    480p  703k , vp9, 24fps, video only, 32.00MiB
135          mp4        854x480    480p  809k , avc1.4d401e, 24fps, video only, 31.22MiB
136          mp4        1280x720   720p 1125k , avc1.4d401f, 24fps, video only, 46.40MiB
247          webm       1280x720   720p 1242k , vp9, 24fps, video only, 53.06MiB
248          webm       1920x1080  1080p 2642k , vp9, 24fps, video only, 152.24MiB
137          mp4        1920x1080  1080p 3770k , avc1.640028, 24fps, video only, 161.78MiB
43           webm       640x360    medium , vp8.0, vorbis@128k, 83.67MiB
18           mp4        640x360    medium  460k , avc1.42001E, mp4a.40.2@ 96k (44100Hz), 55.23MiB
22           mp4        1280x720   hd720 1428k , avc1.64001F, mp4a.40.2@192k (44100Hz) (best)
```

If I right click on a YouTube video and click \"stats for nerds,\"  I find that it typically prefers to send VP9 with Opus. The Opus stream (251) provides the best audio.

In my case, to match what YouTube sends to my browser I have to use something like this:

```
$ youtube-dl -f 248+251 \"https://www.youtube.com/watch?v=tyOHcXKlyU8\"
```

Play that back in VLC or mpv and you'll find it uses a lot more CPU. By contrast, force YouTube to use 720p with H.264 for hardware acceleration and it'll use less. The difference should still be pronounced, mind you, but not quite by an order of magnitude.
"""]]
