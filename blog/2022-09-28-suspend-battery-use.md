[[!meta title="Evaluating suspend battery use with systemd"]]

This is a quick hack that will allow you to do some (manual)
computations on power usage during suspend on your laptop, using
systemd hooks.

It might be possible to use a similar hack on non-systemd systems of
course, you just need something that fires a hook on suspend and
resume.

On systemd, this happens thanks to the
[systemd-suspend.service](https://www.freedesktop.org/software/systemd/man/systemd-suspend.service.html). That service is not designed to be
called directly, but it fires off a series of hooks and targets that
makes it possible to do things before and after suspend. There is a
`sleep.target` that you can hook other services too, but the really
much easier way is to just drop a shell script in
`/usr/lib/systemd/system-sleep/`. 

The simplest way I found to dump battery use is with [tlp program](https://linrunner.de/tlp/)
([Debian package](https://tracker.debian.org/pkg/tlp)):

    apt install tlp
    tlp-stat -b

This should show you something like this:

    root@angela:~# tlp-stat  -b
    --- TLP 1.3.1 --------------------------------------------

    +++ Battery Features: Charge Thresholds and Recalibrate
    natacpi    = inactive (laptop not supported)
    tpacpi-bat = inactive (laptop not supported)
    tp-smapi   = inactive (laptop not supported)

    +++ Battery Status: BAT
    /sys/class/power_supply/BAT/manufacturer                    = TPS
    /sys/class/power_supply/BAT/model_name                      = S10
    /sys/class/power_supply/BAT/cycle_count                     = (not supported)
    /sys/class/power_supply/BAT/charge_full_design              =   6040 [mAh]
    /sys/class/power_supply/BAT/charge_full                     =   6098 [mAh]
    /sys/class/power_supply/BAT/charge_now                      =   6098 [mAh]
    /sys/class/power_supply/BAT/current_now                     =    850 [mA]
    /sys/class/power_supply/BAT/status                          = Full

    Charge                                                      =  100.0 [%]
    Capacity                                                    =  101.0 [%]

Then you just need to hook that into a simple shell script, say in
`/lib/systemd/system-sleep/tlp-stat-battery`:

    #!/bin/sh

    # tlp - systemd suspend/resume hook
    #
    # Copyright (c) 2020 Thomas Koch <linrunner at gmx.net> and others.
    # This software is licensed under the GPL v2 or later.

    case $1 in
        pre)  tlp-stat -b ;;
        post) tlp-stat -b ;;
    esac

Then when your laptop suspends, the script will run before sleep and
dump the battery stats in the systemd journal (or syslog). When it
resumes, it will do the same, so you will be able to compare.

Then a simple way to compare suspend usage is to suspend the laptop
for (say) 10 minutes and see how much power was used. This is the
usage on my Purism Librem 13v4, for example.

Before:

    sep 28 11:19:45 angela systemd-sleep[209379]: --- TLP 1.3.1 --------------------------------------------
    sep 28 11:19:45 angela systemd-sleep[209379]: +++ Battery Features: Charge Thresholds and Recalibrate
    sep 28 11:19:45 angela systemd-sleep[209379]: natacpi    = inactive (laptop not supported)
    sep 28 11:19:45 angela systemd-sleep[209379]: tpacpi-bat = inactive (laptop not supported)
    sep 28 11:19:45 angela systemd-sleep[209379]: tp-smapi   = inactive (laptop not supported)
    sep 28 11:19:45 angela systemd-sleep[209379]: +++ Battery Status: BAT
    sep 28 11:19:45 angela systemd-sleep[209379]: /sys/class/power_supply/BAT/manufacturer                    = TPS
    sep 28 11:19:45 angela systemd-sleep[209379]: /sys/class/power_supply/BAT/model_name                      = S10
    sep 28 11:19:45 angela systemd-sleep[209379]: /sys/class/power_supply/BAT/cycle_count                     = (not supported)
    sep 28 11:19:45 angela systemd-sleep[209379]: /sys/class/power_supply/BAT/charge_full_design              =   6040 [mAh]
    sep 28 11:19:45 angela systemd-sleep[209379]: /sys/class/power_supply/BAT/charge_full                     =   6098 [mAh]
    sep 28 11:19:45 angela systemd-sleep[209379]: /sys/class/power_supply/BAT/charge_now                      =   6045 [mAh]
    sep 28 11:19:45 angela systemd-sleep[209379]: /sys/class/power_supply/BAT/current_now                     =   1024 [mA]
    sep 28 11:19:45 angela systemd-sleep[209379]: /sys/class/power_supply/BAT/status                          = Discharging
    sep 28 11:19:45 angela systemd-sleep[209655]: Charge                                                      =   99.1 [%]
    sep 28 11:19:45 angela systemd-sleep[209656]: Capacity                                                    =  101.0 [%]

After:

    sep 28 11:29:47 angela systemd-sleep[209725]: --- TLP 1.3.1 --------------------------------------------
    sep 28 11:29:47 angela systemd-sleep[209725]: +++ Battery Features: Charge Thresholds and Recalibrate
    sep 28 11:29:47 angela systemd-sleep[209725]: natacpi    = inactive (laptop not supported)
    sep 28 11:29:47 angela systemd-sleep[209725]: tpacpi-bat = inactive (laptop not supported)
    sep 28 11:29:47 angela systemd-sleep[209725]: tp-smapi   = inactive (laptop not supported)
    sep 28 11:29:47 angela systemd-sleep[209725]: +++ Battery Status: BAT
    sep 28 11:29:47 angela systemd-sleep[209725]: /sys/class/power_supply/BAT/manufacturer                    = TPS
    sep 28 11:29:47 angela systemd-sleep[209725]: /sys/class/power_supply/BAT/model_name                      = S10
    sep 28 11:29:47 angela systemd-sleep[209725]: /sys/class/power_supply/BAT/cycle_count                     = (not supported)
    sep 28 11:29:47 angela systemd-sleep[209725]: /sys/class/power_supply/BAT/charge_full_design              =   6040 [mAh]
    sep 28 11:29:47 angela systemd-sleep[209725]: /sys/class/power_supply/BAT/charge_full                     =   6098 [mAh]
    sep 28 11:29:47 angela systemd-sleep[209725]: /sys/class/power_supply/BAT/charge_now                      =   6037 [mAh]
    sep 28 11:29:47 angela systemd-sleep[209725]: /sys/class/power_supply/BAT/current_now                     =    850 [mA]
    sep 28 11:29:47 angela systemd-sleep[209961]: /dev/sda:
    sep 28 11:29:47 angela systemd-sleep[209961]:  setting standby to 36 (3 minutes)
    sep 28 11:29:47 angela systemd-sleep[209725]: /sys/class/power_supply/BAT/status                          = Discharging
    sep 28 11:29:47 angela systemd-sleep[210013]: Charge                                                      =   99.0 [%]
    sep 28 11:29:47 angela systemd-sleep[210018]: Capacity                                                    =  101.0 [%]
    sep 28 11:29:47 angela systemd[1]: systemd-suspend.service: Succeeded.

The important parts being of course:

    sep 28 11:19:45 angela systemd-sleep[209379]: /sys/class/power_supply/BAT/charge_now                      =   6045 [mAh]
    sep 28 11:29:47 angela systemd-sleep[209725]: /sys/class/power_supply/BAT/charge_now                      =   6037 [mAh]

In other words, 8mAh were used in the 10 minutes (and 2 seconds) test
I did. This works out to around 48mAh per hour, or, with this battery,
about 127 hours or roughly 5 days.

Obviously, an improvement could be to actually write this to a file,
do the math and log only that. But that's more work and I'm lazy right
now, exercise for the reader I guess.

Update: someone actually wrote a full [Python/SQLite implementation of
this](https://github.com/lhl/batterylog) already, which does all the computations above, although it's
hardcoded for the Framework laptop and will not necessarily work out
of the box with other systems.

[[!tag hardware hack debian-planet]]
