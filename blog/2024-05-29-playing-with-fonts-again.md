[[!meta title="Playing with fonts again"]]

I am getting increasingly frustrated by Fira Mono's [lack of italic
support](https://github.com/mozilla/Fira/issues/38) so I am looking at [[alternative fonts
again|2020-03-10-font-changes]].

# Commit Mono

This time I seem to be settling on either [Commit Mono](https://commitmono.com/) or [Space
Mono](https://www.colophon-foundry.org/custom-projects/space-mono). For now I'm using Commit Mono because it's a little more
compressed than Fira and does have a italic version. I don't like how
Space Mono's parenthesis (`()`) is "squarish", it feels visually
ambiguous with the square brackets (`[]`), a big no-no for my primary
use case (code).

So here I am using a new font, again. It required changing a bunch of
configuration files in my home directory (which is in a private
repository, sorry) and Emacs configuration (thankfully that's
public!). 

One gotcha is I realized I didn't actually have a global font
configuration in Emacs, as some [Faces](https://www.gnu.org/software/emacs/manual/html_node/emacs/Faces.html) define their own font
family, which overrides the frame defaults.

This is what it looks like, before:

<figure>
<img src="snap-20240529T171950-fira-mono.png" alt="A dark terminal
showing the test sheet in Fira Mono" />
<figcaption>Fira Mono</figcaption>
</figure>

After:

<figure>
<img src="snap-20240529T171846-commit-mono.png" alt="A dark terminal
showing the test sheet in Fira Mono" />
<figcaption>Commit Mono</figcaption>
</figure>

(Notice how those screenshots are not sharp? I'm surprised too. The
originals *look* sharp on my display, I suspect this is something to
do with the Wayland transition. I've tried with both [grim](https://sr.ht/~emersion/grim/) and
[flameshot](https://github.com/flameshot-org/flameshot), for what its worth.)

They are pretty similar! Commit Mono feels a *bit* more vertically
compressed maybe too much so, actually -- the line height feels too
low.  But it's heavily customizable so that's something that's
relatively easy to fix, if it's really a problem. Its weight is also a
little heavier and wider than Fira which I find a little distracting
right now, but maybe I'll get used to it.

All characters seem properly distinguishable, although, if I'd really
want to nitpick I'd say the © and ® are *too* different, with the
latter (`REGISTERED SIGN`) being way too small, basically unreadable
here. Since I see this sign approximately never, it probably doesn't
matter at all.

I like how the ampersand (`&`) is more traditional, although I'll miss
the exotic one Fira produced... I like how the back quotes (`` ` ``,
`GRAVE ACCENT`) drop down low, nicely aligned with the apostrophe. As
I mentioned before, I like how the bar on the "f" aligns with the
other top of letters, something in Fira mono that really annoys me now
that I've noticed it (it's not aligned!).

# A UTF-8 test file

Here's the test sheet I've made up to test various characters. I could
have sworn I had a good one like this lying around somewhere but
couldn't find it so here it is, I guess.

```
US keyboard coverage:

abcdefghijklmnopqrstuvwxyz`1234567890-=[]\;',./
ABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+{}|:"<>?

latin1 coverage: ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿
EURO SIGN, TRADE MARK SIGN: €™

ambiguity test:

e¢coC0ODQ iI71lL!|¦
b6G&0B83  [](){}/\.…·•
zs$S52Z%  ´`'"‘’“”«»

all characters in a sentence, uppercase:

the quick fox jumps over the lazy dog
THE QUICK FOX JUMPS OVER THE LAZY DOG

same, in french:

Portez ce vieux whisky au juge blond qui fume.

dès noël, où un zéphyr haï me vêt de glaçons würmiens, je dîne
d’exquis rôtis de bœuf au kir, à l’aÿ d’âge mûr, &cætera.

DÈS NOËL, OÙ UN ZÉPHYR HAÏ ME VÊT DE GLAÇONS WÜRMIENS, JE DÎNE
D’EXQUIS RÔTIS DE BŒUF AU KIR, À L’AŸ D’ÂGE MÛR, &CÆTERA.

Ligatures test:

-<< -< -<- <-- <--- <<- <- -> ->> --> ---> ->- >- >>-
=<< =< =<= <== <=== <<= <= => =>> ==> ===> =>= >= >>=
<-> <--> <---> <----> <=> <==> <===> <====> :: ::: __
<~~ </ </> /> ~~> == != /= ~= <> === !== !=== =/= =!=
<: := *= *+ <* <*> *> <| <|> |> <. <.> .> +* =* =: :>
(* *) /* */ [| |] {| |} ++ +++ \/ /\ |- -| <!-- <!---

Box drawing alignment tests:
                                                                   █
╔══╦══╗  ┌──┬──┐  ╭──┬──╮  ╭──┬──╮  ┏━━┳━━┓ ┎┒┏┑   ╷  ╻ ┏┯┓ ┌┰┐    ▉ ╱╲╱╲╳╳╳
║┌─╨─┐║  │╔═╧═╗│  │╒═╪═╕│  │╓─╁─╖│  ┃┌─╂─┐┃ ┗╃╄┙  ╶┼╴╺╋╸┠┼┨ ┝╋┥    ▊ ╲╱╲╱╳╳╳
║│╲ ╱│║  │║   ║│  ││ │ ││  │║ ┃ ║│  ┃│ ╿ │┃ ┍╅╆┓   ╵  ╹ ┗┷┛ └┸┘    ▋ ╱╲╱╲╳╳╳
╠╡ ╳ ╞╣  ├╢   ╟┤  ├┼─┼─┼┤  ├╫─╂─╫┤  ┣┿╾┼╼┿┫ ┕┛┖┚     ┌┄┄┐ ╎ ┏┅┅┓ ┋ ▌ ╲╱╲╱╳╳╳
║│╱ ╲│║  │║   ║│  ││ │ ││  │║ ┃ ║│  ┃│ ╽ │┃ ░░▒▒▓▓██ ┊  ┆ ╎ ╏  ┇ ┋ ▍
║└─╥─┘║  │╚═╤═╝│  │╘═╪═╛│  │╙─╀─╜│  ┃└─╂─┘┃ ░░▒▒▓▓██ ┊  ┆ ╎ ╏  ┇ ┋ ▎
╚══╩══╝  └──┴──┘  ╰──┴──╯  ╰──┴──╯  ┗━━┻━━┛          └╌╌┘ ╎ ┗╍╍┛ ┋ ▏▁▂▃▄▅▆▇█

Dashes alignment test:

HYPHEN-MINUS, MINUS SIGN, EN, EM DASH, HORIZONTAL BAR, LOW LINE
--------------------------------------------------
−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−
––––––––––––––––––––––––––––––––––––––––––––––––––
——————————————————————————————————————————————————
――――――――――――――――――――――――――――――――――――――――――――――――――
__________________________________________________
```

Update: [here is another such sample sheet](https://sheet.shiar.nl/sample), it's pretty good and
has support for more languages while being still relatively small.

So there you have it, got completely nerd swiped by typography
again. Now I can go back to writing a too-long proposal again.

Sources and inspiration for the above:

- the `unicode(1)` command, to lookup individual characters to
  disambiguate, for example, `-` (`U+002D HYPHEN-MINUS`, the minus
  sign next to zero on US keyboards) and − (`U+2212 MINUS SIGN`, a
  math symbol)

- [searchable list of characters and their names](https://web.archive.org/web/20080515015236/http://www.columbia.edu/kermit/utf8-t1.html) - roughly
  equivalent to the `unicode(1)` command, but in one page, amazingly
  the `/usr/share/unicode` database doesn't have any one file like
  this

- [bits/UTF-8-Unicode-Test-Documents](https://github.com/bits/UTF-8-Unicode-Test-Documents) - full list of UTF-8
  characters

- [UTF-8 encoded plain text file](https://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-demo.txt) - nice examples of edge cases,
  curly quotes example and box drawing alignment test which,
  incidentally, showed me I needed specific faces customisation in
  Emacs to get the Markdown code areas to display properly, also the
  idea of comparing various dashes

- [sample sentences in many languages](https://www.cl.cam.ac.uk/~mgk25/ucs/examples/quickbrown.txt) - unused, "Sentences that
  contain all letters commonly used in a language"

- [UTF-8 sampler](https://web.archive.org/web/20080515024332/http://www.columbia.edu/kermit/utf8.html) - unused, similar

# Other fonts

In [[my previous blog post about fonts|2020-03-10-font-changes]], I
had a list of alternative fonts, but it seems people are not digging
through this, so I figured I would redo the list here to preempt "but
have you tried Jetbrains mono" kind of comments.

My requirements are:

- *no* ligatures: yes, in the previous post, I *wanted* ligatures but
  I have changed my mind. after testing this, I find them distracting,
  confusing, and they often break the monospace nature of the display
  (note that some folks wrote [emacs code to selectively enable
  ligatures](https://github.com/mickeynp/ligature.el) which is an interesting compromise)z
- monospace: this is to display code
- italics: often used when writing Markdown, where I do make use of
  italics... Emacs falls back to underlining text when lacking italics
  which is hard to read
- free-ish, ultimately should be packaged in Debian

Here is the list of alternatives I have considered in the past and why
I'm not using them:

- [agave](https://b.agaric.net/page/agave): recommended by tarzeau, not sure I like the lowercase
  `a`, a bit too exotic, packaged as [fonts-agave](https://tracker.debian.org/pkg/fonts-agave)

- [Cascadia code](https://github.com/microsoft/cascadia-code): optional ligatures, multilingual, not liking the
  alignment, ambiguous parenthesis (look too much like square
  brackets), new default for [Windows Terminal](https://en.wikipedia.org/wiki/Windows_Terminal) and Visual Studio,
  packaged as [fonts-cascadia-code](https://tracker.debian.org/pkg/fonts-cascadia-code)

- Fira Code: ligatures, was using Fira Mono from which it is derived,
  lacking italics except for forks, interestingly, Fira Code succeeds
  the alignment test but Fira Mono fails to show the X signs properly!
  packaged as [fonts-firacode](https://tracker.debian.org/fonts-firacode)

- [Hack](https://sourcefoundry.org/hack/): no ligatures, very similar to Fira, italics, good
  alternative, fails the X test in box alignment, packaged as
  [fonts-hack](https://tracker.debian.org/fonts-hack)

- [Hermit](https://github.com/Swordfish90/cool-retro-term/tree/master/app/qml/fonts/modern-hermit): no ligatures, smaller, alignment issues in box drawing
  and dashes, packaged as [fonts-hermit](https://tracker.debian.org/fonts-hermit) somehow part of [cool-retro-term](https://github.com/Swordfish90/cool-retro-term/)

- [IBM Plex](https://www.ibm.com/plex/plexness/): irritating website, replaces Helvetica as the IBM
  corporate font, no ligatures by default, italics, proportional alternatives,
  serifs and sans, multiple languages, partial failure in box alignment test (X signs),
  fancy curly braces contrast perhaps too much with the rest of the
  font, packaged in Debian as [fonts-ibm-plex](https://tracker.debian.org/fonts-ibm-plex)

- [Inconsolata](https://levien.com/type/myfonts/inconsolata.html): no ligatures, maybe italics? more compressed than
  others, feels a little out of balance because of that, packaged in
  Debian as [fonts-inconsolata](https://tracker.debian.org/fonts-inconsolata)

- [Intel One Mono](https://github.com/intel/intel-one-mono/): nice legibility, no ligatures, alignment issues
  in box drawing, not packaged in Debian

- [Iosevka](https://typeof.net/Iosevka/): optional ligatures, italics, multilingual, good
  legibility, has a proportional option, serifs and sans, line height
  issue in box drawing, fails dash test, not in Debian

- [Jetbrains Mono](https://www.jetbrains.com/lp/mono/): (mandatory?) ligatures, good coverage,
  originally rumored to be not DFSG-free (Debian Free Software
  Guidelines) but ultimately [packaged in Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=950152) as
  [fonts-jetbrains-mono](https://tracker.debian.org/pkg/fonts-jetbrains-mono)

- [Monoid](https://larsenwork.com/monoid/): optional ligatures, feels much "thinner" than
  Jetbrains, not liking alignment or spacing on that one, ambiguous
  `2Z`, problems rendering box drawing, packaged as [fonts-monoid](https://tracker.debian.org/fonts-monoid)

- [Mononoki](https://madmalik.github.io/mononoki/): no ligatures, looks good, good alternative, suggested
  by the Debian fonts team as part of [fonts-recommended](https://tracker.debian.org/fonts-recommended), problems
  rendering box drawing, em dash bigger than en dash, packaged as
  [fonts-mononoki](https://tracker.debian.org/fonts-mononoki)

- [Server mono](https://servermono.com/): no ligatures, italics, old school

- [Source Code Pro](http://adobe-fonts.github.io/source-code-pro/): italics, looks good, but dash metrics look
  whacky, [not in Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=736681)

- [spleen](https://github.com/fcambus/spleen): bitmap font, old school, spacing issue in box drawing
  test, packaged as [fonts-spleen](https://tracker.debian.org/pkg/fonts-spleen)

- [sudo](https://www.kutilek.de/sudo-font/): personal project, no ligatures, zero originally not
  dotted, relied on metrics for legibility, spacing issue in box
  drawing, not in Debian

- [victor mono](https://rubjo.github.io/victor-mono/): italics are cursive by default (distracting),
  ligatures by default, looks good, more compressed than commit mono,
  good candidate otherwise, has a [nice and compact proof sheet](https://rubjo.github.io/victor-mono/img/styles-dark.42978ce3.png)

So, if I get tired of Commit Mono, I might probably try, in order:

1. Hack
1. Jetbrains Mono
1. IBM Plex Mono

Iosevka, Monoki and Intel One Mono are also good options, but have
alignment problems. Iosevka is particularly disappointing as the `EM
DASH` metrics are just completely wrong (much too wide).

This was tested using the [Programming fonts](https://www.programmingfonts.org/) site which has *all*
the above fonts, which cannot be said of [Font Squirrel](https://www.fontsquirrel.com/) or [Google
Fonts](fonts.google.com/), amazingly. Other such tools:

 * [Coding Font](https://www.codingfont.com/) (broken in Firefox as of 2024-05-30)
 * [dev fonts comparator](https://devfonts.gafi.dev/)
 * [Font Squirrel](https://www.fontsquirrel.com/)
 * [Google Fonts](fonts.google.com/)
 * [Programming fonts](https://www.programmingfonts.org/)

Also note that there is now a package in Debian called [fnt](https://github.com/alexmyczko/fnt) to
manage fonts like this locally, including in-line previews (that don't
work in bookworm but should be improved in trixie and later).

[[!tag debian-planet python-planet typography meta theming usability]]

<!-- posted to the federation on 2024-05-29T17:44:57.933852 -->
[[!mastodon "https://kolektiva.social/@Anarcat/112526563590503074"]]
