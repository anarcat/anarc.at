[[!meta title="CRTC Complaint about Fido and Canada's phone cartel"]]

> I have just [filed a complaint with the CRTC](https://applications.crtc.gc.ca/question/eng/public-inquiries-form) about my phone
> provider's outrageous fees. This is a copy of the complaint.

I am traveling to Europe, specifically to Ireland, for a 6 days for a
work meeting.

I thought I could use my phone there. So I looked at my phone provider's
services in Europe, and found the "Fido roaming" services:

https://www.fido.ca/mobility/roaming

The fees, at the time of writing, at fifteen (15!) dollars *PER DAY*
to get access to my *regular* phone service (not unlimited!!).

If I do *not* use that "roaming" service, the fees are:

 * 2$/min
 * 0.75$/text
 * 10$/20MB

That is absolutely outrageous. Any random phone plan in Europe will be
cheaper than this, by at least one order of magnitude. Just to take
any example:

https://www.tescomobile.ie/sim-only-plans.aspx

Those fine folks offer a one-time, prepaid plan for €15 for 28 days
which includes:

 * unlimited data
 * 1000 minutes
 * 500 text messages
 * 12GB data elsewhere in Europe

I think it's absolutely scandalous that telecommunications providers
in Canada can charge so much money, especially since the *most*
prohibitive fee (the "non-prepaid" plans) are *automatically* charged
if I happen to forget to remove my sim card or put my phone in
"airplane mode".

As advised, I have called customer service at Fido for advice on how
to handle this situation. They have confirmed those are the only plans
available for travelers and could not accommodate me otherwise. I have
notified them I was in the process of filing this complaint.

I believe that Canada has become the technological dunce of the world,
and I blame the CRTC for its lack of regulation in that matter. You
should not allow those companies to grow into such a cartel that they
can do such price-fixing as they wish.

I haven't investigated Fido's competitors, but I will bet at least one
of my hats that they do not offer better service.

I attach a [screenshot](Screenshot_2022-09-08_at_10-26-42_Travel_and_Roaming_Plans_with_Fido_Roam_Fido.png) of the Fido page showing those
outrageous fees.

> I have no illusions about this having any effect. I thought of
> filing such a complain after the [Rogers outage](https://anarc.at/blog/2022-08-26-nationalize-internet/) as well, but
> felt I had less of a standing there because I wasn't affected *that
> much* (e.g. I didn't have a life-threatening situation myself).
>
> This, however, was ridiculous and frustrating enough to trigger this
> outrage. We'll see how it goes...

> > "We will respond to you within 10 working days."

# Response from CRTC

They did respond within 10 days. Here is the full response:

> Dear Antoine Beaupré:
> 
> Thank you for contacting us about your mobile telephone international roaming service plan rates concern with Fido Solutions Inc. (Fido).
> 
> In Canada, mobile telephone service is offered on a competitive basis. Therefore, the Canadian Radio-television and Telecommunications Commission (CRTC) is not involved in Fido's terms of service (including international roaming service plan rates), billing and marketing practices, quality of service issues and customer relations.
> 
> If you haven't already done so, we encourage you to escalate your concern to a manager if you believe the answer you have received from Fido's customer service is not satisfactory.
> 
> Based on the information that you have provided, this may also appear to be a Competition Bureau matter. The Competition Bureau is responsible for administering and enforcing the Competition Act, and deals with issues such as false or misleading representations, deceptive marketing practices and collusion. You can reach the Competition Bureau by calling 1-800-348-5358 (toll-free), by TTY (for deaf and hard of hearing people) by calling 1-866-694-8389 (toll-free). For more contact information, please visit http://www.competitionbureau.gc.ca/eic/site/cb-bc.nsf/eng/00157.html
> 
> When consumers are not satisfied with the service they are offered, we encourage them to compare the products and services of other providers in their area and look for a company that can better match their needs. The following tool helps to show choices of providers in your area: https://crtc.gc.ca/eng/comm/fourprov.htm
> 
> Thank you for sharing your concern with us.

In other words, complain with Fido, or change providers. Don't
complain to us, we don't manage the telcos, they self-regulate.

Great job, CRTC. This is going great. This is exactly why we're one of
the [most expensive countries on the planet for cell phone service](https://ici.radio-canada.ca/nouvelle/1726369/etude-prix-donnees-data-mobile-cellulaire-telephonie-canada-comparaison-mobile-co-uk-pierre-larouche).

# Live chat with Fido

Interestingly, the *day* after I received that response from the CRTC,
I received this email from Fido, while traveling:

> Date: Tue, 13 Sep 2022 10:10:00 -0400
> From: Fido <DONOTREPLY@fido.ca>
> To: REDACTED
> Subject: Courriel d’avis d’itinérance | Fido
> 
> Roaming Welcome Confirmation
> 
> Fido     
> 
> Date : 13 septembre 2022     
> Numéro de compte : [redacted]
> 
> Bonjour     
> Antoine Beaupré!      
> 
> Nous vous écrivons pour vous indiquer qu’au moins un utilisateur inscrit à votre compte s’est récemment connecté à un réseau en itinérance.      
> Vous trouverez ci-dessous le message texte de bienvenue en itinérance envoyé à l’utilisateur (ou aux utilisateurs), qui contenait les tarifs d’itinérance     
> applicables.      
> 
> Message texte de bienvenue en itinérance     
> 
> Destinataire : REDACTED
> 
> Date et heure : 2022-09-13 / 10:10:00       
> Allo, ici Fido : Bienvenue à destination! Vous êtes inscrit à Fido Nomade alors utilisez vos données, parlez et textez comme vous le faites à la       
> maison. Depuis le 1 mars 2022 le tarif cette destination pour 15 $/jour (+ taxes) et valide tous les jours jusqu'à 23 h 59 HE, peu importe le fuseau       
> horaire dans lequel vous vous trouvez. Bon voyage! Des questions? Consultez fido.ca/m/itinerance ou composez +15149333436 (sans frais).        
> 
> Besoin d’aide?      
> 
> * PLANIFIEZ UN VOYAGE AVEC Fido NomadeMC      
>  Découvrez nos options d’itinérance et restez en contact à l’étranger sans vous soucier de votre      
>  facture.       
> 
>  Détails       
> 
> * Gérez votre compte       
>  Gérez vos produits et services Fido à la maison ou sur la route grâce à Mon      
>  Compte.       
> 
>  Détails       
> 
> Ce courriel est produit automatiquement; veuillez ne pas y répondre. Ce courriel (et toute pièce jointe) est confidentiel. Si vous n’êtes pas le destinataire,   
> veuillez supprimer le présent message et en détruire toute copie.   
> 
> FIDO SOLUTIONS  800, RUE DE LA GAUCHETIÈRE OUEST     
> BUREAU 4000    MONTRÉAL (QUÉBEC)    H5A 1K3     
> Fido

I found that message utterly confusing (and yes, I can read
french). Basically, it says that some user (presumably me!) connected
to the network with roaming. I did just disabled airplane mode on my
phone to debug a [Syncthing](https://syncthing.net/) bug but had *not* enabled roaming.

So this message seemed to say that I would be charged 15$ (per DAY!)
for roaming from now on. Confused, I tried their live chat to try to
clarify things, worried I would get charged even more for calling tech
support on `*611`. This is a transcript of the chat:

> F: Hi! What are we doing today?
>
> Type in your question or choose from the options below:
> * Track my Equipment Order
> * View Bill Online
> * Payment Options
> * iPhone 14 Pre-Order

> A: i received a message about roaming while abroad but i did not
> enable roaming on my phone, will i be charged anyways?

> F: I think I know what you’re asking for.
>
> Select the topic that best matches your request or try rephrasing your question.

> A: no

> F: Thank you, this will help us to improve! Would you like to chat with a specialist?
>
> Chat with a specialist
>
> I'll get a specialist to help you with this.
>
> It appears that you're not signed in. Your session may have timed out.
>
> To save time and identify your account details, please sign in to My Account.
>
> * Sign in
> * I'm not able to sign in
>
> Have any questions specific to your Fido account? To service you faster, please identify yourself by completing the form below.

> A: Personal info
> Form submitted

> F: Thank you! I'll connect you with the next available specialist.
>
> Your chat is being transferred to a Live Chat agent. Thanks for your patience.
>
> We are here to assist you and we kindly ask that our team members be treated with respect and dignity. Please note that abuse directed towards any Consumer Care Specialist will not be tolerated and will result in the termination of your conversation with us.
>
> All of our agents are with other customers at the moment. Your chat is in a priority sequence and someone will be with you as soon as possible. Thanks!
>
> Thanks for continuing to hold. An agent will be with you as soon as possible.
>
> Thank you for your continued patience. We’re getting more Live Chat requests than usual so it’s taking longer to answer. Your chat is still in a priority sequence and will be answered as soon as an agent becomes available.
>
> Thank you so much for your patience – we're sorry for the wait. Your chat is still in a priority sequence and will be answered as soon as possible.
>
> Hi, I'm [REDACTED] from Fido in [REDACTED]. May I have your name please? 

> A: hi i am antoine, nice to meet you
>
> sorry to use the live chat, but it's not clear to me i can safely
> use my phone to call support, because i am in ireland and i'm
> worried i'll get charged for the call

> F: Thank You Antoine , I see you waited to speak with me today, thank you for your patience.Apart from having to wait, how are you today? 

> A: i am good thank you

[... delay ...]

> A: should i restate my question?

> F: Yes please what is the concern you have?

> A: i have received an email from fido saying i someone used my phone for roaming
>
> it's in french (which is fine), but that's the gist of it
>
> i am traveling to ireland for a week
>
> i do *not* want to use fido's services here... i have set the phon eto airplane mode for most of my time here

> F: The SMS just says what will be the charges if you used any services.

> A: but today i have mistakenly turned that off and did *not* turn on roaming
>
> well it's not a SMS, it's an email

> F: Yes take out the sim and keep it safe.Turun off or On for roaming you cant do it as it is part of plan.

> A: wat

> F: if you used any service you will be charged if you not used any service you will not be charged.

> A: you are saying i need to physically take the SIM out of the phone?
>
> i guess i will have a fun conversation with your management once i return from this trip
>
> not that i can do that now, given that, you know, i nee dto take the sim out of this phone
>
> fun times

> F: Yes that is better as most of the customer end up using some kind of service and get charged for roaming.

> A: well that is completely outrageous
>
> roaming is off on the phone
>
> i shouldn't get charged for roaming, since roaming is off on the phone
>
> i also don't get why i cannot be clearly told whether i will be charged or not
>
> the message i have received says i will be charged if i use the service
>
> and you seem to say i could accidentally do that easily
>
> can you tell me if i have indeed used service sthat will incur an extra charge?
>
> are incoming text messages free?

> F: I understand but it is on you if you used some data SMS or voice mail you can get charged as you used some services.And we cant check anything for now you have to wait for next bill.
>
> and incoming SMS are free rest all service comes under roaming.
>
> That is the reason I suggested take out the sim from phone and keep it safe or always keep the phone or airplane mode.

> A: okay
>
> can you confirm whether or not i can call fido by voice for support?
>
> i mean for free

> F: So use your Fido sim and call on +1-514-925-4590 on this number it will be free from out side Canada from Fido sim.

> A: that is quite counter-intuitive, but i guess i will trust you on that
>
> thank you, i think that will be all

> F: Perfect, Again, my name is [REDACTED] and it’s been my pleasure to help you today. Thank you for being a part of the Fido family and have a great day! 

> A: you too

So, in other words:

 1. they can't tell me if I've actually been roaming 
 2. they can't tell me how much it's going to cost me
 3. I should remove the SIM card from my phone (!?) or turn on
    airplane mode, but the former is safer
 4. I can call Fido support, but *not* on the usual `*611`, and
    instead on that long-distance-looking phone number, and yes, that
    means turning off airplane mode and putting the SIM card in, which
    contradicts step 3

Also notice how the phone number from the live chat
(`+1-514-925-4590`) is *different* than the one provided in the email
(`15149333436`). So who knows what would have happened if I would have
called the latter. The former is [mentioned in their contact page](https://www.fido.ca/contact/mobile).

I guess the next step is to call Fido over the phone and talk to a
manager, which is what the CRTC told me to do in the first place...

I ended up talking with a manager (another 1h phone call) and they
confirmed there is no other package available at Fido for this. At
best they can provide me with a credit if I mistakenly use the roaming
by accident to refund me, but that's it. The manager also confirmed
that I cannot know if I have actually used any data before reading the
bill, which is issued on the 15th of every month, but only
available... three days later, at which point I'll be back home
anyways.

Fantastic.

Update: I *was* charged 12$ for traveling, but it turns out it wasn't
for traveling to Europe, but just because I strayed too close to the
US border, which is almost worse. I tend to go there quite a bit, and
it's quite likely I hit those towers. Every time that happens, I need
to call Fido to tell them to remove the charge, which is a ridiculous
waste of everyone's time...

[[!tag canada québec "network neutrality" phone debian-planet python-planet]]
