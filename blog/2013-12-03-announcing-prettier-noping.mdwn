<!-- Drupal node 200 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Announcing a prettier noping"]]
[[!meta date="2013-12-03T00:00:29-0500"]]
[[!meta updated="2014-12-04T09:46:33-0500"]]
[[!meta guid="200 at http://anarcat.koumbit.org"]]

I have implemented really pretty histograms in the venerable `ping` software, something I never thought could be improved, until I discovered `prettyping.sh`, something that was just begging for improvements.

Which are now done.
<!--break-->
But first some history...

First, in 1983 (!), there was [ping](https://en.wikipedia.org/wiki/Ping_(networking_utility)) and network operators rejoiced, as they could see if a host was down or not, and have all sorts of geeky statistics:

    PING koumbit.net (209.44.112.66): 48 data bytes
    56 bytes from 209.44.112.66: icmp_seq=0 ttl=52 time=25.076 ms
    56 bytes from 209.44.112.66: icmp_seq=1 ttl=52 time=24.006 ms
    56 bytes from 209.44.112.66: icmp_seq=2 ttl=52 time=24.106 ms
    ^C--- koumbit.net ping statistics ---
    3 packets transmitted, 3 packets received, 0% packet loss
    round-trip min/avg/max/stddev = 24.006/24.396/25.076/0.483 ms

Then, in 2006, there was [noping](http://verplant.org/liboping/), and things were, well, not much better, but we had colors and could ping multiple hosts at once, and there was some rejoicing.

<a href="/blog/files/images/20131202230046.png" title="noping before"><img src="/blog/files/imagecache/normal/images/20131202230046.png" alt="noping before"  class="imagecache-normal" /></a>

Then, in october 2013, there was [prettyping.sh](https://bitbucket.org/denilsonsa/small_scripts/src/64896f2937f8828ba984c07a5346251b899324a4/prettyping.sh?at=default)([announcement](https://web.archive.org/web/20131222220532/http://my.opera.com/CrazyTerabyte/blog/2013/10/18/prettyping-sh-a-better-ui-for-watching-ping-responses)), and things got really flashy and "oh wow, you can do that?" There was [much rejoicing](https://www.reddit.com/r/linux/comments/1op98a/prettypingsh_a_better_ui_for_watching_ping/).

<img src="https://web.archive.org/web/20140326051224im_/http://files.myopera.com/CrazyTerabyte/blog/prettyping-1x.gif" alt="prettyping animation" />

Then, tonight, I [learned ncurses](http://www.tldp.org/HOWTO/NCURSES-Programming-HOWTO/index.html) and there was [much headaches](http://newsgroups.derkeiler.com/Archive/Rec/rec.games.roguelike.development/2010-09/msg00050.html).

But then, I reimplemented prettyping in noping, and I am so happy that I wrote this blog post:

<a href="/blog/files/images/20131202234910.png" ><img src="/blog/files/imagecache/normal/images/20131202234910.png" alt="noping now"  class="imagecache-normal" /></a>

Unless you haven't figured out how cool this is, let me break it down for you:

 1. it supports IPv4 and IPv6
 2. it allows you to track multiple hosts at the same time, and compare them
 3. this allows you to easily track down failure points in a network, something for which you usually need [smokeping](http://oss.oetiker.ch/smokeping/) (needs a webserver) or [mtr](http://www.bitwizard.nl/mtr/) (doesn't have colors)
 3. it allows you to track a *lot* (the last minute at least) of history by default
 4. it is visually easy to track, even from a distance

You may know of that hack that can make "ping" ring a bell when it receives a packet? This is better: you can *see* the packets latency (or when they are just dropped!) from a distance, using an intuitive color code.

Thanks to the well architectured `noping`, the patches were not that complicated to implemented.

The code was [submitted as a pull request upstream](https://github.com/octo/liboping/pull/3) and [merged in the 1.7.0 release](https://github.com/octo/liboping/releases/tag/liboping-1.7.0). This is also [part of your favorite Debian distribution](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=731207) (Debian Jessie).

Rejoice!

[[!tag "debian-planet" "geek" "hack" "network" "news" "software"]]