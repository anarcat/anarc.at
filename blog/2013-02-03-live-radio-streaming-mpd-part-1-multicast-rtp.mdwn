<!-- Drupal node 190 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Live radio streaming with MPD, part 2: multicast RTP"]]
[[!meta date="2013-02-03T21:48:31-0500"]]
[[!meta updated="2013-02-04T19:45:46-0500"]]
[[!meta guid="190 at http://anarcat.koumbit.org"]]

The [[previous article|2013-02-03-live-radio-streaming-mpd-part-1]] was introducing basic streaming principles based on Icecast. The issue with this, of course, is lag and overhead of HTTP-based connexions. In this article we introduce [RTP](https://en.wikipedia.org/wiki/Real-time_Transport_Protocol)-based streaming system, ([[unfortunately|2013-02-04-why-i-dont-pulseaudio]] based on [Pulseaudio](http://pulseaudio.org/) and [multicast](https://en.wikipedia.org/wiki/Multicast).
<!--break-->
*(Update: there was a [[rant|2013-02-04-why-i-dont-pulseaudio]] here about Pulseaudio (PA) that I have moved out of this post because it's not what I was aiming to talk about. Those wanting to answer that troll are welcome to [[join the flamewar here|2013-02-04-why-i-dont-pulseaudio]].)*

A word on multicast
===================

I took the opportunity of trying out Multicast (it's actually [IP multicast](https://en.wikipedia.org/wiki/IP_multicast), to be more precise), since I wanted to have only minimal configuration to perform on the client side. Plus, since the RTP frames are raw 44khz 16 bit audio, the traffic is actually quite heavy, 1.4mbps, to be more precise (punch in `44kHz * 16bit/Hz * 2` in the excellent [Qalculate](http://qalculate.sourceforge.net/) to see why), so I didn't want bandwidth to explode as more listeners joined in. As a comparison, also note that the Icecast Vorbis stream is around 64 or 128kbps depending on quality.

Multicast allows packets to be sent only once for multiple listeners on the same network. It's actually quite cool, but doesn't work very well over the wider internet, for reasons that are still unclear to me. I presume that the fact there is virtual monopoly over traditional broadcasting systems (e.g. television and radio) means that multicast hasn't really kicked in yet... I hope this will make sense over the [Montreal mesh](http://reseaulibre.ca/), although the quality would probably need to be dialed down, or the audio streamed encoded instead of be sent as raw samples.

So why PA? VLC/pipe failures
============================

That aside, it seems that to stream through RTP, I have no choice. I have tried to use [VLC](https://www.videolan.org/vlc/) as a pipe to stream data out of `MPD` as RTP, but somehow this config snippet just failed:

    audio_output {
       type            "pipe"
       name            "RTP"
       command         "cvlc -q - --sout '#rtp{dst=239.72.72.144,port=5004}'"
       encoder         "vorbis"                # optional, vorbis or lame
       format          "44100:16:2"
    }

I don't know why - the multicast port wouldn't open properly and MPD would even hang when restarting. It seems the `pipe` plugin is somewhat broken. Note that the vlc command above actually works fine to stream audio content out through multicast, which is a good way to test if multicast works in the first place...

Shoving PA down MPD's throat
============================

Instead, I had to go to the clunky PA-way (also know as "my way or the highway"). The first step was to configure the `default.pa` to enable RTP streaming. I copied over the default `/etc/pulse/default.pa` file into `~mpd/.pulse/default.pa` and uncommented the following lines:

    # just to get the PA announced
    load-module module-zeroconf-publish
    # we need a null sink, no clue why, but without this glue, it fails
    load-module module-null-sink sink_name=rtp format=s16be channels=2 rate=44100 sink_properties="device.description='RTP Multicast Sink'"
    # port is specified otherwise it is random (?!)
    # loop=1 is to broadcast locally too
    # other useful setting: destination (defaults to 224.0.0.56)
    load-module module-rtp-send source=rtp.monitor port=5004 loop=1

See [the rtp-send module documentation](http://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Modules#module-rtp-send) for more information here.

Then we need MPD to connect to that sink:

    audio_output {
        type            "pulse"
        name            "Pulsaudio RTP"
        sink            "rtp"
    }

Also, now that we got into the PA kitchen sink, we are kind of forced to use it for the local sound card output too, so we'll add this as a sink to:

    audio_output {
        type            "pulse"
        name            "Pulseaudio sound card"
        sink            "alsa_output.pci-0000_00_1b.0.analog-stereo"
    }

(Take a moment to admire the nice sink name. Yours may be different, have fun finding it in the output of `pacmd list-sinks`, but that works only once PA is started!)

So now you need to restart MPD for those changes to take effect. Note that if the `mpd` user has already started PA, you're in luck: it won't restart it by itself, and you need to:

    service mpd stop
    killall pulseaudio
    service mpd start

... yep. Basically because pulseaudio just detaches completely from the parent process when spawned automatically. Lots of fun.

Testing and listening
=====================

Of course, you can listen to the stream using... Pulseaudio. I assume this would magically show up on the network thanks to avahi, if you run Pulseaudio on (say) your laptop. Since I still want to stay away from it, I would rather use a straight commandline tool to listen to the stream. According to [this page](http://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Network/RTP), there aren't that many clients that could listen to such a funky stream. I chose VLC, since its commandline options actually make sense.

Here's the magic mantra to repeat every night before you go to bed:

    cvlc rtp://@224.0.0.56:5004

Yes, that's all. Drop the `c` if you want a GUI.

With mplayer, it's [more complicated](http://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Network/RTP) - you need to specify a bitmask using a hexadecimal string (fun stuff):

    mplayer -demuxer rawaudio -rawaudio format=0x20776172 rtp://224.0.0.56:5004

Yes, good old `0x20776172`...

Results! We want results!
=========================

All this work reduced the lag from 15 seconds to around 1 second. It's still present, and confusing to listen to, but maybe more acceptable. It seems most of the lag is in the `vlc` client buffering, when I enable debugging, I see:

    [0x963c7bc] main input debug: Stream buffering done (1001 ms in 976 ms)

I was able to reduce that delay significantly by using the `--rtp-caching` option of vlc, as such:

    cvlc --rtp-caching=48  rtp://@224.0.0.56:5004

The value is in miliseconds. Anything below 48ms introduces audible artifacts (probably buffer *underruns*). With that setting, there's still a noticeable delay, especially with isolated sounds that are naturally more noticeable. With more harmonious music, it sounds more like an echo.

I have also tried to fiddle with similar settings with `mplayer` and simply failed to get any results. `--no-cache` simply drops samples like crazy. `--cache 64` is the lowest I could push it down to, and it still has noticeable delay, which I couldn't measure, but I would assume would be around 400ms at least.

Also note that contrarily to the previous setup I had, based on Icecast, VLC will automatically resume streaming from the RTP stream as it is stopped or started, which is also very nice - as it allows me to disable remote RTP streams easily and harmlessly.

Troubled areas
==============

Speaking of insane nonsense, here are a few pitfalls to avoid.

Take care to disable the ALSA output, if it's not yet commented out, because MPD will freak out with the following error message:

    Feb 03 20:25 : output: "Sound card" [alsa] failed to play: Resource temporarily unavailable

I have *not* been able to find a way to make PA let go of the audio sound card. I would have prefered MPD to open ALSA directly if PA wasn't needed (I need it only for RTP streaming), but it seems that PA assumes you'll want to play stuff locally.

I have tried commenting out the following in `default.pa`:

    load-module module-udev-detect

Seems like it's not enough. Also note that PA is designed with "dumb users" in mind (read: you need a GUI to operate it), so in the context of MPD, it's really annoying to debug things unless you can actually fire up a X11 session *as the MPD user* (which strikes me as a crazy idea) to configure PA properly with `paprefs` and all the graphical gizmos.

I just gave up and went with PA all the way, and just added the RTP sink through the commandlines. [Other guides](http://askubuntu.com/questions/28039/how-to-stream-music-over-the-network-to-multiple-computers) can show you screenshots to clikety your way through inane graphical interfaces to do the above, if you are into that kind of stuff.

With PA, you will want to get familiar with the `pacmd` and `pactl` commands. Why there are two different commands to basically do the same thing is completely beyond me.

Reference and more reading
==========================

I got a lot of information in the [Pulseaudio MPD wiki page](http://mpd.wikia.com/wiki/PulseAudio) and the [Pulseaudio user documentation](http://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User). The [VLC manual about RTP streaming (server)](http://www.videolan.org/doc/streaming-howto/en/ch04.html#id349956) [(client)](https://www.videolan.org/doc/play-howto/en/ch04.html) was also useful for debugging.

The [Pulseaudio Network documentation](http://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Network) has a set of recipes for using pulseaudio for everything. This [Ask Ubuntu question](http://askubuntu.com/questions/28039/how-to-stream-music-over-the-network-to-multiple-computers) explains how to clikety your way through `paprefs` to essentially do the above.

[[!tag "audio" "debian-planet" "free software" "freedombox" "geek" "mpd" "radio" "rtp"]]
