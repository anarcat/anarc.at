[[!meta title="Archiving web sites"]]

[[!meta date="2018-09-25T00:00:00+0000"]]

[[!meta updated="2018-10-04T13:46:25-0400"]]

[[!toc levels=2]]

I recently took a deep dive into web site archival for friends who were
worried about losing control over the hosting of their work online in
the face of poor system administration or hostile removal. This makes
web site archival an essential instrument in the toolbox of any system
administrator. As it turns out, some sites are much harder to archive
than others. This article goes through the process of archiving
traditional web sites and shows how it falls short when confronted with
the latest fashions in the single-page applications that are bloating
the modern web.

Converting simple sites
=======================

The days of handcrafted HTML web sites are long gone. Now web sites are
dynamic and built on the fly using the latest JavaScript, PHP, or Python
framework. As a result, the sites are more fragile: a database crash,
spurious upgrade, or unpatched vulnerability might lose data. In my
previous life as web developer, I had to come to terms with the idea
that customers expect web sites to basically work forever. This
expectation matches poorly with "move fast and break things" attitude
of web development. Working with the [Drupal][] content-management
system (CMS) was particularly challenging in that regard as major
upgrades deliberately break compatibility with third-party modules,
which implies a costly upgrade process that clients could seldom afford.
The solution was to archive those sites: take a living, dynamic web site
and turn it into plain HTML files that any web server can serve forever.
This process is useful for your own dynamic sites but also for
third-party sites that are outside of your control and you might want to
safeguard.

For simple or static sites, the venerable [Wget][] program works well.
The incantation to mirror a full web site, however, is byzantine:

        $ nice wget --mirror --execute robots=off --no-verbose --convert-links \
                    --backup-converted --page-requisites --adjust-extension \
                    --base=./ --directory-prefix=./ --span-hosts \
                    --domains=www.example.com,example.com http://www.example.com/

The above downloads the content of the web page, but also crawls
everything within the specified domains. Before you run this against
your favorite site, consider the impact such a crawl might have on the
site. The above command line deliberately ignores [`robots.txt`][]
rules, as is now [common practice for archivists][], and hammer the
website as fast as it can. Most crawlers have options to pause between
hits and limit bandwidth usage to avoid overwhelming the target site.

The above command will also fetch "page requisites" like style sheets
(CSS), images, and scripts. The downloaded page contents are modified so
that links point to the local copy as well. Any web server can host the
resulting file set, which results in a static copy of the original web
site.

That is, when things go well. Anyone who has ever worked with a computer
knows that things seldom go according to plan; all sorts of things can
make the procedure derail in interesting ways. For example, it was
trendy for a while to have calendar blocks in web sites. A CMS would
generate those on the fly and make crawlers go into an infinite loop
trying to retrieve all of the pages. Crafty archivers can resort to
regular expressions (e.g. Wget has a `--reject-regex` option) to ignore
problematic resources. Another option, if the administration interface
for the web site is accessible, is to disable calendars, login forms,
comment forms, and other dynamic areas. Once the site becomes static,
those will stop working anyway, so it makes sense to remove such clutter
from the original site as well.

  [Drupal]: https://drupal.org
  [Wget]: https://www.gnu.org/software/wget/
  [`robots.txt`]: https://en.wikipedia.org/wiki/Robots_exclusion_standard
  [common practice for archivists]: https://blog.archive.org/2017/04/17/robots-txt-meant-for-search-engines-dont-work-well-for-web-archives/

JavaScript doom
===============

Unfortunately, some web sites are built with much more than pure HTML.
In single-page sites, for example, the web browser builds the content
itself by executing a small JavaScript program. A simple user agent like
Wget will struggle to reconstruct a meaningful static copy of those
sites as it does not support JavaScript at all. In theory, web sites
should be using [progressive enhancement][] to have content and
functionality available without JavaScript but those directives are
rarely followed, as anyone using plugins like [NoScript][] or
[uMatrix][] will confirm.

Traditional archival methods sometimes fail in the dumbest way. When
trying to build an offsite backup of a local newspaper
([pamplemousse.ca][]), I found that WordPress adds query strings (e.g.
`?ver=1.12.4`) at the end of JavaScript includes. This confuses
content-type detection in the web servers that serve the archive, which
rely on the file extension to send the right `Content-Type` header. When
such an archive is loaded in a web browser, it fails to load scripts,
which breaks dynamic websites.

As the web moves toward using the browser as a virtual machine to run
arbitrary code, archival methods relying on pure HTML parsing need to
adapt. The solution for such problems is to record (and replay) the HTTP
headers delivered by the server during the crawl and indeed professional
archivists use just such an approach.

  [progressive enhancement]: https://en.wikipedia.org/wiki/Progressive_enhancement
  [NoScript]: https://noscript.net/
  [uMatrix]: https://github.com/gorhill/uMatrix
  [pamplemousse.ca]: https://pamplemousse.ca/

Creating and displaying WARC files
==================================

At the [Internet Archive][], Brewster Kahle and Mike Burner designed the
[ARC][] (for "ARChive") file format in 1996 to provide a way to
aggregate the millions of small files produced by their archival
efforts. The format was eventually standardized as the WARC ("Web
ARChive") [specification][] that was released as an ISO standard in
2009 and revised in 2017. The standardization effort was led by the
[International Internet Preservation Consortium][] (IIPC), which is an
"*international organization of libraries and other organizations
established to coordinate efforts to preserve internet content for the
future*", according to Wikipedia; it includes members such as the US
Library of Congress and the Internet Archive. The latter uses the WARC
format internally in its Java-based [Heritrix crawler][].

A WARC file aggregates multiple resources like HTTP headers, file
contents, and other metadata in a single compressed archive.
Conveniently, Wget actually supports the file format with the `--warc`
parameter. Unfortunately, web browsers cannot render WARC files
directly, so a viewer or some conversion is necessary to access the
archive. The simplest such viewer I have found is [pywb][], a Python
package that runs a simple webserver to offer a Wayback-Machine-like
interface to browse the contents of WARC files. The following set of
commands will render a WARC file on `http://localhost:8080/`:

        $ pip install pywb
        $ wb-manager init example
        $ wb-manager add example crawl.warc.gz
        $ wayback

This tool was, incidentally, built by the folks behind the
[Webrecorder][] service, which can use a web browser to save dynamic
page contents.

Unfortunately, pywb has trouble loading WARC files generated by Wget
because it [followed][] an [inconsistency in the 1.0 specification][],
which was [fixed in the 1.1 specification][]. Until Wget or pywb fix
those problems, WARC files produced by Wget are not reliable enough for
my uses, so I have looked at other alternatives. A crawler that got my
attention is simply called [crawl][]. Here is how it is invoked:

        $ crawl https://example.com/

(It *does* say "very simple" in the README.) The program does support
some command-line options, but most of its defaults are sane: it will
fetch page requirements from other domains (unless the
`-exclude-related` flag is used), but does not recurse out of the
domain. By default, it fires up ten parallel connections to the remote
site, a setting that can be changed with the `-c` flag. But, best of
all, the resulting WARC files load perfectly in pywb.

  [Internet Archive]: https://archive.org
  [ARC]: http://www.archive.org/web/researcher/ArcFileFormat.php
  [specification]: https://iipc.github.io/warc-specifications/
  [International Internet Preservation Consortium]: https://en.wikipedia.org/wiki/International_Internet_Preservation_Consortium
  [Heritrix crawler]: https://github.com/internetarchive/heritrix3/wiki
  [pywb]: https://github.com/webrecorder/pywb
  [Webrecorder]: https://webrecorder.io/
  [followed]: https://github.com/webrecorder/pywb/issues/294
  [inconsistency in the 1.0 specification]: https://github.com/iipc/warc-specifications/issues/23
  [fixed in the 1.1 specification]: https://github.com/iipc/warc-specifications/pull/24
  [crawl]: https://git.autistici.org/ale/crawl/

Future work and alternatives
============================

There are plenty more [resources][] for using WARC files. In particular,
there's a Wget drop-in replacement called [Wpull][] that is
specifically designed for archiving web sites. It has experimental
support for [PhantomJS][] and [youtube-dl][] integration that should
allow downloading more complex JavaScript sites and streaming
multimedia, respectively. The software is the basis for an elaborate
archival tool called [ArchiveBot][], which is used by the "*loose
collective of rogue archivists, programmers, writers and loudmouths*"
at [ArchiveTeam][] in its struggle to "*save the history before it's
lost forever*". It seems that PhantomJS integration does not work as
well as the team wants, so ArchiveTeam also uses a rag-tag bunch of
other tools to mirror more complex sites. For example, [snscrape][] will
crawl a social media profile to generate a list of pages to send into
ArchiveBot. Another tool the team employs is [crocoite][], which uses
the Chrome browser in headless mode to archive JavaScript-heavy sites.

This article would also not be complete without a nod to the [HTTrack][]
project, the "website copier". Working similarly to Wget, HTTrack
creates local copies of remote web sites but unfortunately does not
support WARC output. Its interactive aspects might be of more interest
to novice users unfamiliar with the command line.

In the same vein, during my research I found a full rewrite of Wget
called [Wget2][] that has support for multi-threaded operation, which
might make it faster than its predecessor. It is [missing some
features][] from Wget, however, most notably reject patterns, WARC
output, and FTP support but adds RSS, DNS caching, and improved TLS
support.

Finally, my personal dream for these kinds of tools would be to have
them integrated with my existing bookmark system. I currently keep
interesting links in [Wallabag][], a self-hosted "read it later"
service designed as a free-software alternative to [Pocket][] (now owned
by Mozilla). But Wallabag, by design, creates only a "readable"
version of the article instead of a full copy. In some cases, the
"readable version" is actually [unreadable][] and Wallabag sometimes
[fails to parse the article][]. Instead, other tools like
[bookmark-archiver][] or [reminiscence][] save a screenshot of the page
along with full HTML but, unfortunately, no WARC file that would allow
an even more faithful replay.

The sad truth of my experiences with mirrors and archival is that data
dies. Fortunately, amateur archivists have tools at their disposal to
keep interesting content alive online. For those who do not want to go
through that trouble, the Internet Archive seems to be here to stay and
Archive Team is obviously [working on a backup of the Internet Archive
itself][].

------------------------------------------------------------------------

  [resources]: https://archiveteam.org/index.php?title=The_WARC_Ecosystem
  [Wpull]: https://github.com/chfoo/wpull
  [PhantomJS]: http://phantomjs.org/
  [youtube-dl]: http://rg3.github.io/youtube-dl/
  [ArchiveBot]: https://www.archiveteam.org/index.php?title=ArchiveBot
  [ArchiveTeam]: https://archiveteam.org/
  [snscrape]: https://github.com/JustAnotherArchivist/snscrape
  [crocoite]: https://github.com/PromyLOPh/crocoite
  [HTTrack]: http://www.httrack.com/
  [Wget2]: https://gitlab.com/gnuwget/wget2
  [missing some features]: https://gitlab.com/gnuwget/wget2/wikis/home
  [Wallabag]: https://wallabag.org/
  [Pocket]: https://getpocket.com/
  [unreadable]: https://github.com/wallabag/wallabag/issues/2825
  [fails to parse the article]: https://github.com/wallabag/wallabag/issues/2914
  [bookmark-archiver]: https://pirate.github.io/bookmark-archiver/
  [reminiscence]: https://github.com/kanishka-linux/reminiscence
  [working on a backup of the Internet Archive itself]: http://iabak.archiveteam.org

> *This article [first appeared][] in the [Linux Weekly News][].*

[first appeared]: https://lwn.net/Articles/766374/
[Linux Weekly News]: http://lwn.net/

>  As usual, here's the list of issues and patches generated while researching this article:
>
> * [fix broken link to WARC specification](https://github.com/iipc/warc-specifications/pull/45)
> * [sample Apache configuration](https://github.com/webrecorder/pywb/pull/374) for pywb
> * [make job status less chatty](https://github.com/ArchiveTeam/ArchiveBot/pull/326) in ArchiveBot
> * [Debian packaging](https://github.com/jjjake/internetarchive/issues/270) of the `ia` commandline tool
> * [document the --large flag](https://github.com/ArchiveTeam/ArchiveBot/pull/330) in ArchiveBot
> * [mention collections](https://github.com/jjjake/internetarchive/pull/272) in the `ia` documentation
> * [fix warnings in docs builds](https://github.com/jjjake/internetarchive/pull/273) of `ia`
>
> I also want to personally thank the folks in the #archivebot channel
> for their assistance and letting me play with their toys.
>
> The Pamplemousse crawl is now [available on the Internet
> Archive](https://archive.org/details/pamplemousse-ca), it might end up in the wayback machine at some point if
> the Archive curators think it is worth it.
>
> Another example of a crawl is [this archive of two Bloomberg
> articles][] which the "save page now" feature of the Internet
> archive wasn't able to save correctly. But [webrecorder.io][] could!
> Those pages can be seen in the [web recorder player][] to get a
> better feel of how faithful a WARC file really is.
>
> Finally, this article was originally written as a set of notes and
> documentation in the [[services/archive]] page which may also be of
> interest to my readers. This blog post will not be updated in the
> future while the latter wiki page might.

  [this archive of two Bloomberg articles]: https://archive.org/details/anarcat-bloomberg
  [webrecorder.io]: https://webrecorder.io/
  [web recorder player]: https://webrecorder.io/anarcat/bloomberg/index


[[!tag debian-planet lwn archive web warc python-planet]]
