[[!meta title="Looking at Wayland terminal emulators"]]

Back in 2018, I made a [two part series](https://anarc.at/blog/2018-04-12-terminal-emulators-1/) about terminal emulators
that was actually pretty painful to write. So I'm not going to retry
this here, not at all. Especially since I'm not submitting this to the
excellent [LWN editors](https://lwn.net/) so I can get away with not being very good
at writing. Phew.

Still, it seems my future self will thank me for collecting my
thoughts on the terminal emulators I have found out about since I
wrote that article. Back then, Wayland was not quite at the level
[where it is now][], being the default in Fedora (2016), Debian (2019),
RedHat (2019), and Ubuntu (2021). Also, a bunch of folks thought they
would solve everything by using OpenGL for rendering. Let's see how
things stack up.

[where it is now]: https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)#Adoption

[[!toc]]

# Recap

In the previous article, I touched on those projects:

| Terminal           | Changes since review                                                                                                                  |
|--------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| [Alacritty][]      | releases! scrollback, better latency, URL launcher, clipboard support, still [not in Debian][], but close                             |
| [GNOME Terminal][] | not much? couldn't find a changelog                                                                                                   |
| [Konsole][]        | [outdated changelog][], [color][], [image previews][], clickable files, multi-input, [SSH plugin][], [sixel images][]                 |
| [mlterm][]         | [long changelog][] but: supports console mode (like GNU screen?!), Wayland support through libvte, sixel graphics, zmodem, mosh (!)   |
| [pterm][]          | [changes][]: Wayland support                                                                                                          |
| [st][]             | [unparseable changelog][], suggests [scroll(1)][] or [scrollback.patch][] for scrollback now                                          |
| [Terminator][]     | moved to GitHub, Python 3 support, not being dead                                                                                     |
| [urxvt][]          | no significant changes, a single release, still in CVS!                                                                               |
| [Xfce Terminal][]  | [hard to parse changelog][], presumably some improvements to paste safety?                                                            |
| [xterm][]          | [notoriously hard to parse changelog][], improvements to paste safety (`disallowedPasteControls`), fonts, [clipboard improvements][]? |

[xterm]: http://invisible-island.net/xterm/
[Xfce Terminal]: https://docs.xfce.org/apps/terminal/start
[urxvt]: http://software.schmorp.de/pkg/rxvt-unicode.html
[Terminator]: https://github.com/gnome-terminator/terminator/
[st]: https://st.suckless.org/
[PuTTY]: https://www.chiark.greenend.org.uk/%7Esgtatham/putty/
[pterm]: https://manpages.debian.org/pterm
[mlterm]: http://mlterm.sourceforge.net/
[Konsole]: https://konsole.kde.org/
[VTE]: https://github.com/GNOME/vte
[GNOME Terminal]: https://wiki.gnome.org/Apps/Terminal
[Alacritty]: https://github.com/jwilm/alacritty

After writing those articles, bizarrely, I was still using rxvt even
though it did not come up as shiny as I would have liked. The colors
problems were especially irritating.

I briefly played around with Konsole and xterm, and eventually
switched to XTerm as my default `x-terminal-emulator` "alternative" in
my Debian system, while writing this.

I quickly noticed why I had stopped using it: clickable links are a
huge limitation. I ended up adding keybindings to open URLs in a
command. There's another keybinding to dump the history into a
command. Neither are as satisfactory as just clicking a damn link.

[hard to parse changelog]: https://gitlab.xfce.org/apps/xfce4-terminal/-/blob/master/NEWS
[notoriously hard to parse changelog]: https://invisible-island.net/xterm/xterm.log.html
[clipboard improvements]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=901249
[unparseable changelog]: https://git.suckless.org/st/
[changes]: https://www.chiark.greenend.org.uk/~sgtatham/putty/changes.html
[long changelog]: https://raw.githubusercontent.com/arakiken/mlterm/3.9.2/doc/en/ReleaseNote
[outdated changelog]: https://konsole.kde.org/changelog.html
[not in Debian]: http://bugs.debian.org/851639
[color]: https://kde.org/announcements/releases/2020-12-apps-update/
[image previews]: https://kde.org/announcements/gear/21.08.0/
[sixel images]: https://kde.org/announcements/gear/22.04.0/
[SSH plugin]: https://kde.org/announcements/gear/21.12.0/
[scroll(1)]: https://tools.suckless.org/scroll/
[scrollback.patch]: https://st.suckless.org/patches/scrollback/

# Requirements

Figuring out my requirements is actually a pretty hard thing to do. In
my last reviews, I just tried a bunch of stuff and collected
*everything*, but a lot of things (like tab support) I don't actually
care about. So here's a set of things I actually do care about:

 * latency
 * resource usage
 * proper clipboard support, that is:
   * mouse selection and middle button uses PRIMARY
   * <kbd>control-shift-c</kbd> and <kbd>control-shift-v</kbd> for
     CLIPBOARD
 * true color support
 * no known security issues
 * active project
 * paste protection
 * clickable URLs
 * scrollback
 * font resize
 * non-destructive text-wrapping (ie. resizing a window doesn't drop
   scrollback history)
 * proper unicode support (at least latin-1, ideally "everything")
 * good emoji support (at least showing them, ideally "nicely"), which
   involves font fallback

Latency is particularly something I wonder about in Wayland. Kitty
seem to have been [pretty dilligent](https://github.com/kovidgoyal/kitty/issues/2701#issuecomment-636497270) at doing latency tests,
claiming 35ms with a [hardware-based latency tester](https://thume.ca/2020/05/20/making-a-latency-tester/) and 7ms [with
typometer](https://github.com/kovidgoyal/kitty/issues/2701#issuecomment-911089374), but it's unclear how those would come up in Wayland
because, as far as I know, [typometer does not support Wayland](https://github.com/pavelfatin/typometer/issues/8).

# Candidates

Those are the projects I am considering.

 * [darktile](https://github.com/liamg/darktile) - GPU rendering, Unicode support, themable, ligatures
   (optional), Sixel, window transparency, clickable URLs, true color
   support, not in Debian
 * [foot](https://codeberg.org/dnkl/foot) - Wayland only, daemon-mode, sixel images, scrollback
   search, true color, font resize, URLs not clickable, but
   [keyboard-driven selection](https://codeberg.org/dnkl/foot#user-content-urls), proper clipboard support, in Debian
 * [havoc](https://github.com/ii8/havoc) - minimal, scrollback, configurable keybindings, not in
   Debian
 * [sakura](https://www.pleyades.net/david/projects/sakura) - libvte, Wayland support, tabs, no menu bar, original
   libvte gangster, dynamic font size, probably supports Wayland, in Debian
 * [termonad](https://github.com/cdepillabout/termonad) - Haskell? in Debian
 * [wez](https://wezfurlong.org/wezterm/) - Rust, Wayland, multiplexer, ligatures, scrollback
   search, clipboard support, bracketed paste, panes, tabs, serial
   port support, Sixel, Kitty, iTerm graphics, built-in SSH client
   (!?), [not in Debian](http://bugs.debian.org/993625)
 * XTerm - status quo, no Wayland port obviously
 * [zutty](https://github.com/tomszilagyi/zutty): OpenGL rendering, true color, clipboard support, small
   codebase, no Wayland support, [crashes on bremner's](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1014080), in Debian

# Candidates not considered

## Alacritty

I would really, really like to use [Alacritty][], but it's still not
packaged in Debian, and they [haven't fully addressed the latency
issues](https://github.com/alacritty/alacritty/issues/673#issuecomment-658784144) although, to be fair, maybe it's just an impossible
task. Once it's [packaged in Debian](http://bugs.debian.org/851639), maybe I'll reconsider.

## Kitty

[Kitty](https://github.com/kovidgoyal/kitty) is a "fast, feature-rich, GPU based", with ligatures,
emojis, hyperlinks, pluggable, scriptable, tabs, layouts, history,
file transfer over SSH, its own graphics system, and probably much
more I'm forgetting. It's packaged in Debian.

So I immediately got two people commenting (on IRC) that they use
Kitty and are pretty happy with it. I've been hesitant in directly
talking about Kitty publicly, but since it's likely there will be a
pile-up of similar comments, I'll just say why it's not the first in
my list, even if it might, considering it's packaged in Debian and
otherwise checks all the boxes.

I don't trust the Kitty code. Kitty was written by the same author as
Calibre, which has a horrible security history and generally really
messy source code. I have tried to do LTS work on Calibre, and have
mostly given up on the idea of making that program secure in any
way. See [[software/desktop/calibre]] for the details on that.

Now it's possible Kitty is different: it's quite likely the author has
gotten some experience writing (and maintaining for so long!) Calibre
over the years. But I would be more optimistic if the author's
reaction to the security issues were more open and proactive.

I've also seen the same reaction play out on Kitty's side of
things. As anyone who worked on writing or playing with non-XTerm
terminal emulators, it's quite a struggle to make something
(bug-for-bug) compatible with everything out there. And Kitty is in
that uncomfortable place right now where it diverges from the canon
and needs its own entry in the ncurses database. I don't remember the
specifics, but the author also managed to get into fights with those
people as well, which I don't feel is reassuring for the project going
forward.

If security and compatibility wasn't such big of a deal for me, I
wouldn't mind so much, but I'll need a lot of convincing before I
consider Kitty more seriously at this point.

# Next steps

It seems like Arch Linux defaults to foot in Sway, and I keep seeing
it everywhere, so it is probably my next thing to try, if/when I
[[switch to Wayland|software/desktop/wayland]]. 

One major problem with foot is that it's yet another terminfo
entry. They did make it into ncurses (patch [2021-07-31](https://invisible-island.net/ncurses/NEWS.html#index-t20210731)) but only
after Debian bullseye stable was released. So expect some weird
compatibility issues when connecting to any other system that is older
or the same as stable (!). Thankfully, there is a `foot-terminfo`
package that is available starting from Debian bullseye.

One question mark with all Wayland terminals, and Foot in particular,
is how much latency they introduce in the rendering pipeline. The
[foot performance](https://codeberg.org/dnkl/foot/wiki/Performance) and [benchmarks](https://codeberg.org/dnkl/foot/src/branch/master/doc/benchmark.md) look excellent, but do not
include latency benchmarks.

# No conclusion

So I guess that's all I've got so far, I may try alacritty if it hits
Debian, or foot if I switch to Wayland, but for now I'm hacking in
xterm still. Happy to hear ideas in the comments.

[Stay tuned for more happy days](https://www.youtube.com/watch?v=kemivUKb4f4).

Update: a few months after this article was written, I did switch a
laptop to Wayland, and as part of that migration, adopted foot. It's
great, and latency is barely noticeable. I recommend people try it out
when they switch to Wayland, and, inversely, try out Wayland if only
to try out Foot, it's a great terminal emulator.

[[!tag debian-planet review terminals wayland]]
