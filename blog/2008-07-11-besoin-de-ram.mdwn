<!-- Drupal node 134 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Besoin de ram"]]
[[!meta date="2008-07-11T10:20:37-0500"]]
[[!meta updated="2011-12-22T01:23:20-0500"]]
[[!meta guid="134 at http://anarcat.koumbit.org"]]

Alors la machine que <a href="http://zhugo.org/">Hugo</a> m'a passé, en partie suite à ma <a href="/blog/2007-04-24-mon-serveur-est-mort-besoin-de-dons">dernière quête</a> a des gros problèmes, comme pourraient vous en parler longuement mes colocs. Mes derniers tests de <a href="http://www.memtest.org/">memtest86+</a> ont montré plusieurs erreurs de mémoire sur la seule barrette qui reste. J'ai donc besoin de nouvelles barrettes de mémoire. Voici ce que j'ai besoin:

<code>
DDR-333Mhz Clock 2.5ms
PC2700U
</code>

Alors si quelqu'un peut me filer une ou deux barrettes, j'aurais besoin au minimum de 512M de ram...<!--break-->

[[!tag "vraie vie" "geek"]]
