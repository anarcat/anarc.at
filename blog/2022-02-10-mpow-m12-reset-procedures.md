[[!meta title="Mpow M12 Bluetooth ear buds reset procedures"]]

TL;DR: if your M12 earbuds play only from one ear and show up as two
devices, go to the very end of this post and try to follow
[procedure 8](#anarcats-solution).

I have a pair of [Mpow M12 wireless earbuds](https://www.xmpow.com/products/mpow-463-wireless-earbuds) that I kind of like
but cannot recommend because they have this weird bug where they can
end up disconnected from one another. In that state, they appear as
two separate Bluetooth devices and you can associate only with
one. When you play audio, it comes out only in one ear.

So aggravating.

Today is the second time I find myself in this situation and not only could
I not remember how to fix it, I couldn't figure out how to do it even
knowing I had found the solution on Youtube *and* finding the actual
video I thought would fix it.

This is an inventory of the procedures I have found and, hopefully, a
working procedure that is currently just a draft because, well, they
are paired again now and I don't actually know how to break them apart
again to test the procedure.

On their [support site](https://www.xmpow.com/products/mpow-463-wireless-earbuds), they have at least *four* different
procedures they suggest for everyone who seems to have the same
problem as me, and they are all different. I named those procedure 1
through 4, from oldest to newest. The procedures after are Youtube
videos I managed to gather that did interesting things.

The last procedure somehow worked after a few tries. I think.

[[!toc]]

# 7 seconds hold

This was the first response on their Q&A page:

 1. Please press the earbuds for 7 seconds at the same time when they
    were charging in the charging box to reboot them, then please
    connect them again for a try.

Effect: after 7 seconds, the LEDs turn white-ish for half a second,
then turn off, then the LEDs return flashing red/blue. They still show
up as two Bluetooth devices.

# Clear paired devices

 1. Make sure the Bluetooth function in the device is turned off.
 
 2. Press and hold both earbuds for 5 seconds in the charging case to
    clear paired devices.

Effect: same as the above.

## User manual procedure

Note that this procedure is similar to the "reset" procedure described
[in the user manual](https://mpow.s3-us-west-1.amazonaws.com/Document/Audio/MPOW+M12++User+Manual.pdf), which is:

 1. Make sure Bluetooth is turned off in your device.
 
 2. When both earbuds are in the charging case, simultaneously press
    and hold both earbuds for 5 seconds to clear paired devices.
 
 3. The earbud's light will flash red and blue simultaneously which
    means successful resetting.

 4. The LED will go out. 1 second later, Mpow M12 will automatically
    enter back into pairing mode.

I think that "red and blue simultaneously" is something i see as a
"white" light somehow.

# "Clean and dry the charger"

This is the third response:

 1. Press and hold both earbuds for 5 seconds in the charging case to
    clear paired devices.
 
 2. Could you please try to clean the charger box and charge it again?
    The charger box is not waterproof,please use dry item to clear it.
 
 3. The distance between the phone and headphone is too far, or there
    is obstruction resulting in unstable signal, such as WIFI and so
    on. In this situation, please reboot the headphone, fully charge
    it and try it again anywhere without signal interference.

I just love this one. "Dry" what? the charger box? What does that have
to do with anything?

Obviously, this is exactly the same as procedure 2, except with
completely unrelated and irrelevant instructions ("fully charge the
phone"? come on.)

# "Long press the MFB"

I couldn't quite figure out this one:

 1. Please delete the Bluetooth record on your phone device. 
 
 2. Take one of the earbuds out of the charging case, the earbud will
    enter into pairing mode, the red light and blue light flashes
    alternately, then please long press the MFB of the earbud, the red
    light and blue light flashes at the same time.
 
 3. Please take another earbud out of the case, the earbud will enter
    into pairing mode as well. 
    
 4. Please make two earbuds close together, after 3-5 seconds, the
    light of the earbuds will stop, then the Blue light will on for
    about 3 seconds then stop. The light of the earbud also stops and
    another one will flash Blue and Red.
 
 5. After the above steps, please connect the earbuds to your phone
    device via Bluetooth, two earbuds will connect to your
    device. Hope this will help and have a nice day.

Now this one is interesting, not because it works, because at least a
part of it is familiar: "the blue light will [go] on for about 3
seconds then stop". 

I can't actually get to that step, as I can't figure out what the
"long press the MFB of the earbud" step: if I "long press" (e.g. 3
seconds) the earbuds turns off; if I longer press (e.g. 5-7 seconds),
it gets into that weird state from procedure 1 or 2, presumably where
it clears known devices or reboots.

It's familiar because I remember seeing a Youtube video that would
describe a *working* procedure to reset the earbuds. Which brings me
to the other procedures I found.

# Youtube: Skid marks AKA "um"

[This one](https://www.youtube.com/watch?v=XJhXR0G1ytg) is kind of hilarious, partly because how unclear and
"humming" it is but also because the person doesn't actually
demonstrate the procedure because "I'm scared the whole... they'll
break again". The procedure suggested seems to be:

 1. take both earbuds out of the case

 2. tap one "five times" ("you just tap it tap it tap it until it goes
    this color", which seems to be a similar color than the 5 seconds
    hold above!)

 3. then "leave them in the case" (didn't we just take them *out* of
    the case?)

 4. don't close the case, leave like that for a few hours (!?)

 5. "next time you turn them on, just leave them overnight or
    something"
    
"They should work fine."

What's interesting about *this* procedure is that it accesses a state
not documented in any of the procedures on the official website, in
which both earbuds' LEDs turn white and *stay* white.

I had to "tap tap" *seven* times. I think.

This is *not* the same as procedure 1 and 2 where you "hold" the
buttons until the LEDs turn white, because then they don't *stay*
white. It also only works while the devices are in "pairing" mode
(flashing red/blue).

If you put them in the case, nothing happens. They are also
unresponsive to taps: single tap, double tap, long taps, nothing turns
them off. If you close the case and reopen it, they go back to their
broken state. Also, unfortunately, in that weird state they still show
up as two devices (so at least the BT radios still work).

I haven't tried "leaving them overnight or something". In any case,
this procedure didn't work, but did give shiny lights that I have no
idea what they mean.

# Youtube: Rock5Steady video

This procedure is the one I thought made it work last I had this
problem. But when I followed it, it didn't actually work:

 1. unpair from your device

 2. switch off the bluetooth on that device

 3. take them out of the case

 4. double tap on each earbud separately

 5. and then you press and hold, each one separately for three seconds

 7. put them back in the case

 8. close it for a few seconds

 9. turn on the bluetooth on your device

 10. open the case and they will pair as normal

I failed at step 4: double-tap on each didn't do anything. A "press
and hold" for 3 seconds turns them off.
 
(One fun thing about this is the "thank you Lydia on Amazon". I
couldn't actually trace what that comment was about, but presumably
some support person on Amazon gave those tips? Kind of funny how the
Internet is turning in this weird oral tradition place where you can't
actually find a written trace of anything...)

# Heather Clausen's comment

Amazingly, there's another procedure that can enter a new state that
is different from all the above. That is from a comment in the above
video:

 1. Put them back in the case
 
 2. Then unpair from your device
 
 3. Open the lid and both of them, they should start flashing blue/red
 
 4. Without taking them out of the case tap at the same time 4 times
 
 5. It should reset and now only the right one should be flashing. You
    will know the buds are paired if only the right earbud is flashing
    blue/red

Step 3 didn't quite work here: the earbuds didn't immediately flash
blue/red when I open the lid. If I wait, they eventually do, but when
I tap 4 times, only the left one does something, and it *powers off*
and powers back on again.

But, amazingly, eventually that procedure worked, *thanks* [Heather
Clausen](https://www.youtube.com/channel/UCXcwdSfyKf0z8V0YY423xnA), whoever you are. I think I tried the procedure a few
times before it worked, and drafted the following procedure to try to
come up with a more consistent solution.

# Anarcat's solution

So I think this is the procedure, expanded from the above "Heather
Clausen" procedure:

 1. unpair the device(s) on your phone (both, if you made the mistake
    of trying to pair them separately)

 2. put the buds in the case, close the case, wait a few seconds (5?)

 3. open the lid

 4. tap both of them simultaneously, 4 times (or more?)

 5. close the lid?

 6. open the lid?

 7. now only the right one should be flashing red/blue

 8. pair with your phone

 9. enjoy stereo sound

 10. put the buds back in their case

 11. close the lid
 
 12. open the lid
 
 13. the devices should say "power on" and flash simultaneously

I haven't formally tested it yet because I just redid Heather's
procedure a few times, cycling the buds through the case and "tap
tapping" , until it worked. It's actually hard to tap two things
really simultaneously, and only four times, bizarrely. And I play
music. Go figure.

Next time this happens, I'll try this procedure and update this blog.

Now as to *why* this happens, I have no frigging idea. I wish it just
didn't, or at least I wish I knew, so that I wouldn't do it. I think
it might have to do with putting only one earbud back in the case and
closing it, but I am not sure. That also remains to be clarified.

In any case, don't buy those things, find a better model, and comment
below to tell me which one it is please.

PS: yeah, this title and whole article might be SEO/clickbait, but I
frankly really don't care and it's not why I do those things. This is
really, and genuinely just a problem-solving mechanism for me
(systematically document the procedures somewhere, to see clearly what
works and what doesn't) and a way to make sure I don't forget this
stuff. As some dude once said (paraphrasing here):

> upload it on the internet and make archive.org crawl it.

Not linking to the dude in question because either (a) you know who it
is or (b) I shouldn't encourage his ego even more.

[[!tag hardware review audio]]
