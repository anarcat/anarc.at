[[!meta title="New phone: Pixel 4a"]]

I'm sorry to announce that I gave up on the Fairphone series and
switched to a Google Phone (Pixel 4a) running [CalyxOS](https://calyxos.org/).

[[!toc]]

# Problems in fairy land

My [[hardware/phone/fairphone2]], even if it is less than two years
old, is having major problems:

 * from time to time, the screen flickers and loses "touch" until I
   squeeze it back together
 * the camera similarly disconnects regularly
 * even when it works, the camera is... pretty bad: low light is
   basically unusable, it's slow and grainy
 * the battery can barely keep up for one day
 * the cellular coverage is very poor, in Canada: I lose signal at the
   grocery store and in the middle of my house...

Some of those problems are known: the Fairphone 2 is old now. It was
probably old even when I got it. But I can't help but feel a little
sad to let it go: the entire point of that device was to make it easy
to fix. But alas, because it's sold only in Europe, local stores don't
carry replacement parts. To be fair, Fairphone did offer to fix the
device, but with a 2 weeks turnaround, I had to get another phone
anyways.

I did actually try to buy a [[hardware/phone/fairphone3]], from
Clove. But they did some crazy validation routine. By email, they
asked me to provide a photo copy of a driver's license and the credit
card, arguing they need to do this to combat fraud. I found that
totally unacceptable and asked them to cancel my order. And because
I'm not sure the FP3 will fix the coverage issues, I decided to just
give up on Fairphone until they officially ship to the Americas.

# Do no evil, do not pass go, do not collect 200$

So I got a Google phone, specifically a Pixel 4a. It's a nice device,
all small and shiny, but it's "plasticky" - I would have prefered
metal, but it seems you need to pay much, much more to get that (in
the Pixel 5).

In any case, it's certainly a better form factor than the Fairphone 2:
even though the screen is bigger, the device itself is actually
smaller and thinner, which feels great. The OLED screen is beautiful,
awesome contrast and everything, and preliminary tests show that the
camera is much better than the one on the Fairphone 2. (The be fair,
again, that is another thing the FP3 improved significantly. And that
is with the stock Camera app from CalyxOS/AOSP, so not as good as the
Google Camera app, which does AI stuff.)

# CalyxOS: success

The Pixel 4a not [not supported by LineageOS](https://wiki.lineageos.org/devices/): it seems every time
I pick a device in that list, I manage to miss the right device by one
(I bought a Samsung S9 before, which is also unsupported, even though
the S8 is). But thankfully, it is [supported by CalyxOS](https://calyxos.org/get/).

That install was a breeze: I was hesitant in playing again with
installing a custom Android firmware on a phone after fighting with
this quite a bit in the past (e.g. [[hardware/phone/htc-one-s]],
[[hardware/phone/lg-g3-d852/]]). But it turns out their install
instructions, mostly using a [AOSP alliance](https://github.com/AOSPAlliance) [device-flasher](https://github.com/AOSPAlliance/device-flasher/)
works absolutely great. It assumes you know about the commandline, and
it does require to basically `curl | sudo` (because you need to
download their binary and run it as root), but it Just. Works. It
reminded me of how great it was to get the Fairphone with TWRP
preinstalled... 

Oh, and kudos to the people in `#calyxos` on Freenode: awesome tech
support, super nice folks. An amazing improvement over the ambiance in
`#lineageos`! :)

# Migrating data

Unfortunately, migrating the data was the usual pain in the back. This
should improve the next time I do this: CalyxOS ships with
[seedvault](https://github.com/seedvault-app/seedvault/), a secure backup system for Android 10 (or 9?) and
later which backs up everything (including settings!) with
encryption. Apparently it works great, and CalyxOS is also working on
a migration system to switch phones.

But, obviously, I couldn't use that on the Fairphone 2 running Android
7... So I had to, again, improvised. The first step was to install
[Syncthing](https://syncthing.net/), to have an easy way to copy data around. That's easily
done through [F-Droid](https://f-droid.org/), already bundled with CalyxOS (including the
privileged extension!). Pair the devices and boom, a magic portal to
copy stuff over.

The other early step I took was to copy apps over using the F-Droid
"find nearby" functionality. It's a bit quirky, but really helps in
copying a bunch of APKs over.

Then I setup a temporary [keepassxc](https://keepassxc.org/) password vault on the
Syncthing share so that I could easily copy-paste passwords into
apps. I used to do this in a text file in Syncthing, but copy-pasting
in the text file is much harder than in [KeePassDX](https://f-droid.org/packages/com.kunzisoft.keepass.libre). (I just picked
one, maybe [KeePassDroid](https://f-droid.org/packages/com.android.keepass) is better? I don't know.) Do keep a copy
of the URL of the service to reduce typing as well.

Then the following apps required special tweaks:

 * **AntennaPod** has an import/export feature: export on one end,
   into the Syncthing share, then import on the other. then go to the
   queue and select all episodes and download
 * the **Signal** "chat backup" *does* copy the secret key around, so
   you don't get the "security number change" warning (even if it
   prompts you to re-register) - external devices need to be relinked
   though
 * AnkiDroid, DSub, Nextcloud, and Wallabag required copy-pasting
   passwords

I tried to sync contacts with [DAVx5](https://www.davx5.com/) but that didn't work so well:
the account was setup correctly, but contacts didn't show up. There's
probably just this one thing I need to do to fix this, but since I
don't really need sync'd contact, it was easier to export a VCF file
to Syncthing and import again.

# Known problems

One problem with CalyxOS I found is that the fragile little microg
tweaks didn't seem to work well enough for Signal. That was unexpected
so they encouraged me to [file that as a bug](https://gitlab.com/CalyxOS/calyxos/-/issues/292).

<del>The other "issue" is that the bootloader is locked, which makes it
impossible to have "root" on the device. That's rather unfortunate: I
often need root to debug things on Android. In particular, it made it
difficult to restore data from OSMand</del> (see below). But I guess that
most things just work out of the box now, so I don't really need it
and appreciate the extra security. Locking the bootloader means full
cryptographic verification of the phone, so that's a good feature to
have!

OSMand still [doesn't have a good import/export](https://www.reddit.com/r/OsmAnd/comments/jo3fhf/exportimport/) story. I ended up
sharing the `Android/data/net.osmand.plus/files` directory and
importing waypoints, favorites and tracks by hand. <del>Even though maps
are actually in there, it's not possible for Syncthing to write
directly to the same directory on the new phone, "thanks" to the new
permission system in Android which forbids this kind of inter-app
messing around.</del>

<del>Tracks are particularly a problem: my older OSMand setup had all those
folders neatly sorting those tracks by month. This makes it really
annoying to track every file manually and copy it over. I have mostly
given up on that for now, unfortunately. And I'll still need to
reconfigure profiles and maps and everything by hand. Sigh. I guess
that's a good clearinghouse for my old tracks I never use...</del>

Update: turns out setting storage to "shared" fixed the issue, see
comments below!

# Conclusion

Overall, CalyxOS seems like a good Android firmware. The install is
smooth and the resulting install seems solid. The above problems are
mostly annoyances and I'm very happy with the experience so far,
although I've only been using it for a few hours so this is very
preliminary.

[[!tag debian-planet hardware phone android python-planet]]
