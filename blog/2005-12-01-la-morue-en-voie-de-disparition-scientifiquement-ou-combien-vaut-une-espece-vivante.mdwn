<!-- Drupal node 56 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="La morue en voie de disparition... scientifiquement ou: combien vaut une espèce vivante?"]]
[[!meta date="2005-12-01T10:01:55-0500"]]
[[!meta updated="2012-12-14T22:44:59-0500"]]
[[!meta guid="56 at http://anarcat.koumbit.org"]]

À défaut d'être reconnue officiellement par le gouvernement fédéral comme étant en voie de disparition, la morue est du moins [reconnue scientifiquement comme étant en voie de disparition](http://www.radio-canada.ca/nouvelles/regional/modele.asp?page=/regions/atlantique/2005/11/29/007-TNL-morue.shtml) par le Comité sur la situation des espèces en péril au Canada ([COSEPAC](http://www.cosewic.gc.ca/fra/sct5/index_f.cfm)), depuis maintenant 2003. Pourquoi ça fait maintenant les manchettes? Eh bien c'est que le ministre des pêcheries, Geoff Regan, refuse de mettre l'espèce sous la protection de la loi sur les espècse en péril, parce que ça infligerait des pertes économiques, évaluées 82 millions de dollars par année.

Ce qui m'amène spontanément à la réflexion: combien vaut l'existence même de la morue? 15 millions? Combien de pertes économiques auraient été acceptables, anyways? Est-ce que c'est vraiment des pertes économiques dont on se préoccuppe ou c'est des pertes politiques associées à une telle décision?

Fidèle à leur invertébrisme habituel, les politiciens sont incapables de trancher de cette façon, même si la décision du ministre signifie à long terme la disparition de la morue et des pertes économiques d'autant plus grande car la perte de la resource est inévitable. Pour faire un parallèle, c'est comme si on aurait jamais arrêté la chasse au [béluga](http://www.speciesatrisk.gc.ca/search/speciesDetails_f.cfm?SpeciesID=146) parce qu'il y avait des intérêts économiques en jeu. Évidemment, c'est la morue, il y en a jusqu'à la fin des temps, n'est-ce pas? COSEPAC n'est pas d'accord: " Il y a moins de 3 % de ce poisson qui reste. Malgré le moratoire depuis 12 ans, il n'y a aucun rétablissement."

[[!tag "réflexion" "nouvelles" "environnement"]]