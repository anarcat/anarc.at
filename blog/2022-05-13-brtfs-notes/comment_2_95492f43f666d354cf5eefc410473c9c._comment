[[!comment format=mdwn
 username="anarcat"
 subject=""""""
 date="2022-05-16T14:45:50Z"
 content="""
> of course it is circumstantial, but facebook runs a few thousand servers with btrfs (AFAIR)

i wouldn't call this "circumstantial", that's certainly a strong data point. But Facebook has a whole other scale and, if they're anything like other large SV shops, they have their own Linux kernel fork that might have improvements we don't.

>  and personally I am running it since many years without a single failure.

that, however, I would call "anecdotal" and something I hear a lot.... often followed with things like:

> with some quirks, again, I admit - namely that healing is not automatic

... which, for me, is the entire problem here. "it kind of works for me, except sometimes not" is really not what I expect from a filesystem.

> Concerning subvolumes - you don't get separate disk usage simply because it is part of the the main volume (volid=5), and within there just a subdirectory. So getting disk usage from there would mean reading the whole tree (du-like -- btw, I recommend gdu which is many many times faster than du!).
>
> The root subvolume on fedora is not the same as volid=5 but just another subvolume. I also have root volumes for Debian/Arch/Fedora on my system (sharing /usr/local and /home volumes). That they called it root is indeed confusing.

[Hacker News](https://news.ycombinator.com/item?id=31383007) helpfully reminded me that:

> the author intentionally gave up early on understanding and simply
> rants about everything that does not look or work as usual

I think it's framed as criticism of my work, but I take it as a compliment. I reread the two paragraphs a few times, and they still don't make much sense to me. It just begs more questions:

 1. can we have more than one main volumes?
 2. why was it setup with subvolumes instead of volumes?
 3. why isn't everything volumes?

I know I sound like a newbie meeting a complex topic and giving up. But here's the thing: I've encountered (and worked in production) with at least half a dozen filesystems in my lifetime (ext2/ext3/ext4, XFS, UFS, FAT16/FAT32, NTFS, HFS, ExFAT, ZFS), and for most of those, I could use them without having to go very deep into the internals. 

But BTRFS gets obscure *quick*. Even going through official documentation (e.g. [BTRFS Design](https://btrfs.wiki.kernel.org/index.php/Btrfs_design)), you *start* with C structs. And somewhere down there there's this confusing diagram about the internal mechanics of the btree and how you build subvolumes and snapshots on top of that.

If you want to hack on BTRFS, that's great. You can get up to speed pretty quick. But I'm not looking at BTRFS from an enthusiast, kernel developer look. I'm looking at it from a "OMG what is this" look, with very little time to deal with it. Every other filesystem architecture I've used like this so far has been able to somewhat be operational in a day or two. After spending multiple days banging my head on this problem, I felt I had to write this down, because everything seems so obtuse that I can't wrap my head around it.

Anyways, thanks for the constructive feedback, it certainly clarifies things a little, but really doesn't make me want to adopt BTRFS in any significant way.
"""]]
