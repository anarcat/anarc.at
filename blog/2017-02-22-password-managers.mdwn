[[!meta title="A look at password managers"]]
[[!meta date="2017-02-15T12:00:00-0500"]]
[[!meta updated="2017-02-22T19:49:36-0500"]]

[[!toc]]

> This article is part of series of 4 articles on passwords:
>
> * [[Reliably generating good passwords|2017-02-18-passwords-entropy]]
> * [[A look at password managers|2017-02-22-password-managers]]
> * [[The case against password hashers|2017-03-02-password-hashers]]
> * [[A short history of password hashers|2017-03-02-hashers-history]]

As we noted in an
[[earlier article|blog/2017-02-18-passwords-entropy]], passwords are a
liability and we'd prefer to get rid of them, but the current reality
is that we do use a plethora of passwords in our daily lives. This
problem is especially acute for technology professionals, particularly
system administrators, who have to manage a lot of different
machines. But it also affects regular users who still use a large
number of passwords, from their online bank to their favorite
social-networking site. Despite the
[remarkable memory capacity of the human brain][], humans are actually
terrible at recalling even short sets of arbitrary characters with the
precision needed for passwords.

[remarkable memory capacity of the human brain]: https://www.scientificamerican.com/article/what-is-the-memory-capacity/

Therefore humans [reuse passwords][], make them trivial or guessable,
write them down on little paper notes and stick them on their screens,
or just reset them by email every time. Our memory is undeniably
failing us and we need help, which is where password managers come
in. Password managers allow users to store an arbitrary number of
passwords and just remember a single password to unlock them all.

[reuse passwords]: https://xkcd.com/792/

But there is a large variety of password managers out there, so which
one should we be using? At my previous job, an inventory was done of
about [40 different free-software password managers][] in different
stages of development and of varying quality. So, obviously, this
article will not be exhaustive, but instead focus on a smaller set of
some well-known options that may be interesting to readers.

[40 different free-software password managers]: https://wiki.koumbit.net/PasswordManagementService/SoftwareComparison

# KeePass: the popular alternative

The most commonly used password-manager design pattern is to store
passwords in a file that is encrypted and password-protected. The most
popular free-software password manager of this kind is probably
[KeePass][].

[KeePass]: http://keepass.info/

An important feature of KeePass is the ability to auto-type passwords in
forms, most notably in web browsers. This feature makes KeePass really
easy to use, especially considering it also supports global key bindings
to access passwords. KeePass databases are designed for simultaneous
access by multiple users, for example, using a shared network drive.

KeePass has a graphical interface written in C\#, so it uses the Mono
framework on Linux. A separate project, called [KeePassX][] is a
clean-room implementation written in C++ using the Qt framework. Both
support the [AES][] and [Twofish][] encryption algorithms, although
KeePass recently added support for the [ChaCha20][] cipher. AES
[key derivation][] is used to generate the actual encryption key for
the database, but the latest release of KeePass also added using
[Argon2][], which was the winner of the July 2015
[password-hashing competition][]. Both programs are more or less
equivalent, although the original KeePass seem to have more features
in general.

[KeePassX]: https://www.keepassx.org/
[AES]: https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
[Twofish]: https://en.wikipedia.org/wiki/Twofish
[ChaCha20]: https://en.wikipedia.org/wiki/ChaCha20
[key derivation]: http://keepass.info/help/base/security.html#secdictprotect
[Argon2]: https://en.wikipedia.org/wiki/Argon2
[password-hashing competition]: https://password-hashing.net/

The KeePassX project has recently been forked into another project now
called [KeePassXC][] that implements a set of new
features that are present in KeePass but missing from KeePassX like:

[KeePassXC]: https://keepassxc.org/

* auto-type on Linux, Mac OS, and Windows
* database merging — which allows multi-user support
* using the web site's favicon in the interface

So far, the maintainers of KeePassXC seem to be open to
[re-merging the project][] "*if the original maintainer of KeePassX in
the future will be more active and will accept our merge and
changes*". I can confirm that, at the time of writing, the original
[KeePassX project][] now has 79 pending pull requests and only one
pull request was merged since the last release, which was 2.0.3 in
September 2016.

[re-merging the project]: https://github.com/keepassxreboot/keepassxc/issues/43#issuecomment-254045934
[KeePassX project]: https://github.com/keepassx/keepassx

While KeePass and derivatives allow multiple users to access the same
database through the merging process, they do not support multi-party
access to a single database. This may be a limiting factor for larger
organizations, where you may need, for example, a different password set
for different technical support team levels. The solution in this case
is to use separate databases for each team, with each team using a
different shared secret.

# Pass: the standard password manager?

I am currently using [password-store][], or pass, as a password
manager. It aims to be "*the standard Unix password manager*". Pass is
a GnuPG-based password manager that features a surprising number of
features given its small size:

[password-store]: https://www.passwordstore.org/

 * copy-paste support
 * Git integration
 * multi-user/group support
 * pluggable extensions (in the upcoming 1.7 release)

The command-line interface is simple to use and intuitive. The
following, will, for example, create a pass repository, a 20 character
password for your LWN account and copy it to the clipboard:

        $ pass init
        $ pass generate -c lwn 20

The main issue with pass is that it doesn't encrypt the *name* of
those entries: if someone were to compromise my machine, they could
easily see which sites I have access to simply by listing the
passwords stored in `~/.password-store`. This is a deliberate design
decision by the upstream project, as [stated][] by a mailing list
participant, Allan Odgaard:

[stated]: https://lists.zx2c4.com/pipermail/password-store/2014-June/001001.html

> Using a single file per item has the advantage of shell completion,
> using version control, browse, move and rename the items in a file
> browser, edit them in a regular editor (that does GPG, or manually
> run GPG first), etc.

Odgaard goes on to point out that there are alternatives that do encrypt
the entire database (including the site names) if users really need that
feature.

Furthermore, there is a [tomb plugin][] for pass that encrypts the
password store in a [LUKS][] container (called a "[tomb][]"), although
it requires explicitly opening and closing the container, which makes
it only marginally better than using full disk encryption
system-wide. One could also argue that password file names do not hold
secret information, only the site name and username, perhaps, and that
doesn't require secrecy. I do believe those should be kept secret,
however, as they could be used to discover (or prove) which sites you
have access to and then used to perform other attacks. One could draw
a parallel with the SSH `known_hosts` file, which used to be plain
text but is now hashed so that hosts are more difficult to discover.

[tomb plugin]: https://github.com/roddhjav/pass-tomb
[LUKS]: https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup
[tomb]: https://github.com/dyne/Tomb

Also, sharing a database for multi-user support will require some sort
of file-sharing mechanism. Given the integrated Git support, this will
likely involve setting up a private Git repository for your team,
something which may not be accessible to the average Linux
user. Nothing keeps you, however, from sharing the `~/.password-store`
directory through another file sharing mechanism like (say)
[Syncthing][] or [Dropbox][].

[Syncthing]: https://syncthing.net/
[Dropbox]: https://en.wikipedia.org/wiki/Dropbox_(service)

You can use multiple distinct databases easily using the
`PASSWORD_STORE_DIR` environment variable. For example, you could have
a shell alias to use a different repository for your work passwords
with:

        alias work-pass="PASSWORD_STORE_DIR=~/work-passwords pass"

Group support comes from a clever use of the GnuPG multiple-recipient
encryption support. You simply have to specify multiple OpenPGP
identities when initializing the repository, which also works in
subdirectories:

        $ pass init -p Ateam me@example.com joelle@example.com
        mkdir: created directory '/home/me/.password-store/Ateam'
        Password store initialized for me@example.com, joelle@example.com
        [master 0e3dbe7] Set GPG id to me@example.com, joelle@example.com.
         1 file changed, 2 insertions(+)
         create mode 100644 Ateam/.gpg-id

The above will configure pass to encrypt the passwords in the `Ateam`
directory for me@example.com and joelle@example.com. Pass depends on
GnuPG to do the right thing when encrypting files and how those
identities are treated is entirely delegated to GnuPG's default
configuration. This could lead to [problems][] if
arbitrary keys can be injected into your key ring, which could confuse
GnuPG. I would therefore recommend using full key fingerprints instead
of user identifiers.

[problems]: https://lwn.net/Articles/697417/

Regarding the actual encryption algorithms used, in my tests, GnuPG
1.4.18 and 2.1.18 seemed to default to 256-bit AES for encryption, but
that has not always been the case. The chosen encryption algorithm
actually depends on the recipient's key preferences, which may vary
wildly: older keys and versions may use anything from 128-bit AES to
[CAST5][] or [Triple DES][]. To figure out which algorithm GnuPG
chose, you may want to try this pipeline:

        $ echo test | gpg -e -r you@example.com | gpg -d -v
        [...]
        gpg: encrypted with 2048-bit RSA key, ID XXXXXXX, created XXXXX
          "You Person You <you@example.com>"
        gpg: AES256 encrypted data
        gpg: original file name=''
        test

[CAST5]: https://en.wikipedia.org/wiki/CAST-128
[Triple DES]: https://en.wikipedia.org/wiki/Triple_DES

As you can see, pass is primarily a command-line application, which
may make it less accessible to regular users. The community has
produced different graphical interfaces that are either using pass
directly or operate on the storage with their own GnuPG integration. I
personally use pass in combination with [Rofi][] to get quick access
to my passwords, but less savvy users may want to try the [QtPass][]
interface, which should be more user-friendly. QtPass doesn't actually
depend on pass and can use GnuPG directly to interact with the pass
database; it is available for Linux, BSD, OS X, and Windows.

[Rofi]: https://davedavenport.github.io/rofi/
[QtPass]: https://qtpass.org/

# Browser password managers

Most users are probably already using a password manager through their
web browser's "remember password" functionality. For example, Chromium
will ask if you want it to remember passwords and encrypt them with
your operating system's facilities. For Windows, this encrypts the
passwords with your login password and, for GNOME, it will store the
passwords in the gnome-keyring storage. If you synchronize your
Chromium settings with your Google account, Chromium will store those
passwords on Google's servers, encrypted with a key that is
[stored in the Google Account itself][].  So your passwords are then
only as safe as your Google account. Note that this was
[covered here in 2010][], although back then Chromium didn't
synchronize with the Google cloud or encrypt with the system-level key
rings. That facility was only
[added][]
in 2013.

[stored in the Google Account itself]: https://productforums.google.com/forum/#!topic/chrome/p0vglq4ZG-8
[covered here in 2010]: https://lwn.net/Articles/388309/
[added]: https://arstechnica.com/information-technology/2013/11/google-finally-hides-passwords-from-snoopers-in-new-builds-of-chromium/

In Firefox, there's an optional, profile-specific master password that
unlocks all passwords. In this case, the issue is that browsers are
generally always open, so the vault is always unlocked. And this is for
users that actually *do* pick a master password; users are often
completely unaware that they should set one.

The unlocking mechanism is a typical convenience-security trade-off:
either users need to constantly input their master passwords to login
or they don't, and the passwords are available in the clear. In this
case, Chromium's approach of actually asking users to unlock their
vault seems preferable, even though the developers actually
[refused to implement the feature for years][].

[refused to implement the feature for years]: https://bugs.chromium.org/p/chromium/issues/detail?id=1397

Overall, I would recommend against using a browser-based password
manager. Even if it is not used for critical sites, you will end up with
hundreds of such passwords that are vulnerable while the browser is
running (in the case of Firefox) or at the whim of Google (in the case
of Chromium). Furthermore, the "auto-fill" feature that is often coupled
with browser-based password managers is often vulnerable to serious
attacks, which is mentioned below.

Finally, because browser-based managers generally lack a proper
password generator, users may fail to use properly generated
passwords, so they can then be easily broken. A password generator has
been requested for Firefox, according to this [feature request][]
opened in 2007, and there *is* a [password generator in Chrome][], but
it is disabled by default and hidden in the mysterious
[chrome://flags][] URL.

[feature request]: https://bugzilla.mozilla.org/show_bug.cgi?id=376674
[password generator in Chrome]: https://www.chromium.org/developers/design-documents/password-generation
[chrome://flags]: http://blogs.adobe.com/cantrell/archives/2012/07/all-about-chrome-flags.html

# Other notable password managers

Another alternative password manager, briefly mentioned in the
previous article, is the minimalistic [Assword][] password manager
that, despite its questionable name, is also interesting. Its main
advantage over pass is that it uses a single encrypted JSON file for
storage, and therefore doesn't leak the name of the entries by
default. In addition to copy/paste, Assword also supports
automatically entering passphrases in fields using the [xdo][]
library. Like pass, it uses GnuPG to encrypt passphrases. According to
Assword maintainer Daniel Kahn Gillmor in email, the main issue with
Assword is "interaction between generated passwords and insane
password policies". He gave the example of the Time-Warner Cable
registration form that requires, among other things, "letters and
numbers, between 8 and 16 characters and not repeat the same
characters 3 times in a row".

[Assword]: https://finestructure.net/assword/
[xdo]: https://www.semicomplete.com/projects/xdotool

Another well-known password manager is the commercial [LastPass][]
service which [released][] a free-software command-line client called
[lastpass-cli][] about three years ago. Unfortunately, the server
software of the lastpass.com service is still proprietary. And given
that LastPass has had at least [two serious security breaches][] since
that release, one could legitimately question whether this is a viable
solution for storing important secrets.

[LastPass]: https://lastpass.com/
[released]: https://blog.lastpass.com/2014/10/open-sourced-lastpass-command-line-application-now-available.html/
[lastpass-cli]: https://github.com/lastpass/lastpass-cli
[two serious security breaches]: https://en.wikipedia.org/wiki/LastPass#Security_issues

In general, web-based password managers expose a whole new attack
surface that is not present in regular password managers. A
[2014 study by University of California researchers][] showed that,
out of five password managers studied, every one of them was
vulnerable to at least one of the vulnerabilities studied. LastPass
was, in particular, vulnerable to a cross-site request forgery (CSRF)
attack that allowed an attacker to bypass account authentication and
access the encrypted database.

[2014 study by University of California researchers]: https://www.usenix.org/conference/usenixsecurity14/technical-sessions/presentation/li_zhiwei

# Problems with password managers

When you share a password database within a team, how do you remove
access to a member of the team? While you can, for example, re-encrypt a
pass database with new keys (thereby removing or adding certain
accesses) or change the password on a KeePass database, a hostile party
could have made a backup of the database before the revocation. Indeed,
in the case of pass, older entries are still in the Git history. So
access revocation is a problematic issue found with all shared password
managers, as it may actually mean going through every password and
changing them online.

This fundamental problem with shared secrets can be better addressed
with a tool like [Vault][] or [SFLvault][]. Those tools aim to provide
teams with easy ways to store dynamic tokens like API keys or service
passwords and share them not only with other humans, but also make
them accessible to machines. The general idea of those projects is to
store secrets in a central server and send them directly to relevant
services without human intervention. This way, passwords are not
actually shared anymore, which is similar in spirit to the approach
taken by centralized authentication systems like [Kerberos][]. If you
are looking at password management for teams, those projects may be
worth a look.

[Vault]: https://www.vaultproject.io/
[SFLvault]: http://sflvault.org/
[Kerberos]: https://en.wikipedia.org/wiki/Kerberos_(protocol)

Furthermore, some password managers that support auto-typing were
found to be vulnerable to HTML injection attacks: if some third-party
ad or content is able to successfully hijack the parent DOM content,
it masquerades as a form that could fool auto-typing software as
demonstrated by this [paper][] that was [submitted at USENIX 2014][].
Fortunately, KeePass was not vulnerable according to the security
researchers, but LastPass was, again, vulnerable.

[paper]: http://crypto.stanford.edu/~dabo/pubs/abstracts/pwdmgrBrowser.html
[submitted at USENIX 2014]: https://www.usenix.org/conference/usenixsecurity14/technical-sessions/presentation/silver

# Future of password managers?

All of the solutions discussed here assume you have a trusted computer
you regularly have access to, which is a usage pattern that seems to be
disappearing with a majority of the population. You could consider your
phone to be that trusted device, yet a phone can be lost or stolen more
easily than a traditional workstation or even a laptop. And while
KeePass has Android and iOS ports, those do not resolve the question of
how to share the password storage among those devices or how to back
them up.

Password managers are fundamentally file-based, and the "file" concept
seems to be quickly disappearing, faster than we technologists sometimes
like to admit. Looking at some relatives' use of computers, I notice it
is less about "files" than images, videos, recipes, and various abstract
objects that are stored in the "cloud". They do not use local storage so
much anymore. In that environment, password managers lose their primary
advantage, which is a local, somewhat offline file storage that is not
directly accessible to attackers. Therefore certain password managers
are specifically designed for the cloud, like LastPass or web browser
profile synchronization features, without necessarily addressing the
inherent issues with cloud storage and opening up huge privacy and
security issues that we absolutely need to address.

This is where the "password hasher" design comes in. Also known as
"stateless" or "deterministic" password managers, password hashers are
emerging as a convenient solution that could possibly replace
traditional password managers as users switch from generic computing
platforms to cloud-based infrastructure. We will cover password hashers
and the major security challenges they pose in a future article.

> *Note: this article [first appeared][] in the
> [Linux Weekly News][].*

[first appeared]: https://lwn.net/Articles/714473/
[Linux Weekly News]: http://lwn.net/

[[!tag debian-planet debian passwords lwn geek security crypto]]
