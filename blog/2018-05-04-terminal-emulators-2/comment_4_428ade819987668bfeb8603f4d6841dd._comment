[[!comment format=mdwn
 username="anarcat"
 subject="""another performance comparison and new emulator"""
 date="2021-01-18T15:59:33Z"
 content="""
If I would do this review again today, it seems I would definitely need to include a terminal emulator called [Zutty](https://tomscii.sig7.se/zutty). It checks all the boxes for me (almost):

 * fast
 * low latency
 * good unicode support
 * correct terminal emulation (which is a major annoyance in urxvt)
 * low resource usage
 * good font handling
 * written in plain C++

<del>Major blockers for adoption:</del>

 * scrollback support
 * [support for *BOTH* PRIMARY or CLIPBOARD](https://github.com/tomszilagyi/zutty/issues/9), which i use often in rxvt

<del>But author seems open to improvements, so who knows.</del> Update: both were implemented! Very nice! I guess my only concern at switching now would be whether it will survive the Wayland apocalypse (whether that will come or not... ;) In theory, since it relies so much on OpenGL, Wayland shouldn't be "that hard"...

Anyways, author did an excellent [latency review](https://tomscii.sig7.se/2021/01/Typing-latency-of-Zutty) and [general comparison](https://tomscii.sig7.se/2020/12/A-totally-biased-comparison-of-Zutty) that is definitely worth a read if you liked this article.
"""]]
