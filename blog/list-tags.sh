#! /bin/sh
pcregrep -h '(?<!\\)\[\[!tag' *.mdwn | sed 's/\[\[!tag//g;s/\]\]//g;s/"//g;s/ /\n/g' | sort | grep . | uniq -c | sort -n
