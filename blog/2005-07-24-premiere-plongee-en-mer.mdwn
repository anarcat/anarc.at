<!-- Drupal node 12 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Première plongée en mer"]]
[[!meta date="2005-07-24T18:27:05-0500"]]
[[!meta updated="2011-12-22T01:25:53-0500"]]
[[!meta guid="12 at http://anarcat.koumbit.org"]]

Après une courte nuit de sommeil troublée par le vent, nous avons paqueté la tente et sommes roulés vers le Centre Nautique de Percé.  Surexcités, nous sommes arrivés 30 minutes d'avance, avant les employés de la place qui semblaient encore endormis à l'ouverture, à 8h du matin. Après un petit cours sur le bases de la plongée en mer, j'ai dû essayer un "wet suit". Je le mentionne parce que ça a été une expérience assez difficile..  Pour ceux qui n'avaient pas remarqué, j'ai une grosse tête (même lorsqu'elle n'est pas enflée) et une chevelure pas moins volumineuse. J'ai tenté d'enfouir  mes dreads, ma barbe et ma grosse tête dans de multiple costumes en latex, ce qui semblait me donner une allure tout à fait burlesque à en juger par l'hilarité générale du club nautique (et de moi-même, d'ailleurs). J'ai fini par devoir me tailler la barbe pour pouvoir entrer dans la combinaison caoutchouteuse.

Rapide excursion en bateau suivie, lourde bouteille et multiples pièces d'équipement, mais l'attente en vaut la peine: la mer en apparence grise, méchante et obscure cache en vérité une multitude de couleurs dûes à une faune et une flore à la variété explosive (probablement dû au fait que nous plongions au sud de l'Île Bonaventure, dans une zone de préservation du parc provincial). Une expérience unique qui dépasse largement celle que j'avais eue en piscine au CEGEP Ahuntsic et qui m'a donné le goût de prendre ma certification de plongeur, formation coûteuse mais qui permet de plonger sans avoir un "divemaster" qui vous tient par la main et permet de descendre plus bas que nos 5 mètres.

Je recommande donc à tous l'initiation à la plongée à Percée, un des seuls clubs qui offrent ce genre d'expérience au Québec, à ce qu'on m'a dit...

Nous avons cependant quitté Percé. Notre voyage tire rapidement à sa fin (28 au soir: Rivière du Loup, 29: Montréal) alors nous avons continué notre chemin et nous voici à Pabos Mills, au "Bourg de Pabos", lieu historique que nous avons rapidement (~1h?) visité (5$/personne). On se repose un peu ici de toutes ces émotions au camping du parc du bourg de pabos (18$/nuit).

[[!tag "voyage"]]