<!-- Drupal node 71 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Je suis vivant et à Montréal"]]
[[!meta date="2006-02-15T00:58:08-0500"]]
[[!meta updated="2011-12-22T01:25:51-0500"]]
[[!meta guid="71 at http://anarcat.koumbit.org"]]

Juste une petite note pour annoncer que je suis bel et bien revenu de Portland. Oui, on a manqué notre vol, mais après tout on a atteri sans encombres à Montréal, après deux copieuses fouilles à la sécurité de Vancouver. Tout ça parce qu'on a fait l'erreur d'aller chercher des sandwiches (dégueulasses) au Subway qui, apparamment en dehors de la zone de "sécurité". On a bien fait attention de demander au petit préposé où était la bouffe. Il a juste omis de nous dire qu'il allait falloir passer les gardes à nouveau à notre retour.

Beurkl... marre des aéroports et de la sécurité illusoire... N'empêche.. ça fait du bien d'être de retour, même si j'ai pas vraiment eu le temps de rien faire depuis que je suis passé par Dorval. Débordé de travail, la vie roule trop vite..

M'enfin, juste pour finir mon chiâlage habituel: Dorval, c'est vraiment poche. Il faudrait *vraiment*, à défaut d'avoir un aéroport décent, avoir au moins du transport en commun qui en fait l'allez retour de façon décente. À 15$/personne/voyage pour la navette qui va et vient au centre-ville de l'aéroport, dès qu'on atteint la masse critique de 2 personnes, il revient moins cher d'utiliser le taxi (~30$). Ridicule! Portland, et la plupart des aéroports de ce monde, a un train qui part de l'aéroport et se rend directement au centre-ville. Le tout pour un maigre 2$US. À quand un train de Dorval?

Et ceci, sans parler du système de transport exemplaire de Portland. Il y a deux circuits de tramways électriques en surface et un "fareless square" fantastique: une zone   du centre-ville où tous les transports en commun sont gratuits, pour évacuer les voitures du centre-ville. Et ça marche! Les habitants de Portland, très sympatiques, utilisent massivement le transport en commun, qui devient du coup très efficace et attrayant pour les commanditaires. Curieux phénomène, d'ailleurs: les arrêts des trams sont commandités par des compagnies locales. À quand "cet arrêt est commandité par les Garages Daniels Inc."?

[[!tag "voyage" "nouvelles" "meta"]]