<!-- Drupal node 32 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="une petite grippe avec ça?"]]
[[!meta date="2005-09-30T00:24:50-0500"]]
[[!meta updated="2012-12-14T22:44:23-0500"]]
[[!meta guid="32 at http://anarcat.koumbit.org"]]

Je le voyais venir... Un petit mal de gorge hier, aujourd'hui, je suis "sur le cul", comme on dit. Ce qui est bien, par contre, c'est que je peux prendre congé. Anyways, c'est pas comme si j'aurais pu faire grand chose aujourd'hui... J'ai sorti les vidanges et je me suis enligné la fin de la première saison de Star Trek Enterprise, que je me gardais pour une journée malade comme aujourd'hui.

En parlant de journée malade, il semblerait que je n'ai pas été le seul à avoir été privé de courant aujourd'hui. Même que Québec avait peur que [le lac St-Charles déborde ](http://www.radio-canada.ca/nouvelles/Index/nouvelles/200509/29/003-vents-degats-qc-ont.shtml) (merci [Mathieu](http://mathieu.koumbit.org/node/17)). Pourquoi ça m'intéresse? C'est que j'ai de la famille là-bas. Je crois que je vais appeler demain pour voir s'ils ont les deux pieds dans la flotte. Et dire que le lac s'est presque vidé durant l'été à cause de la sécheresse.

Ceci, sans parler de la [fonte de la calotte glacière ](http://www.liberation.fr/page.php?Article=327445) que les [médias ](http://news.bbc.co.uk/1/hi/sci/tech/4290340.stm) [viennent](http://www.kanaalz.be/fr/Belga/BelgaNieuws.asp?ArticleID=41256&SectionID=10)  [tout](http://www.lexpress.fr/info/quotidien/actu.asp?id=385) [juste](http://info.france2.fr/monde/14316214-fr.php) [de](http://permanent.nouvelobs.com/sciences/20050929.OBS0631.html)
[remarquer](http://radio-canada.ca/)... Oups! Mauvais lien, pas de nouvelles sur [Radio-Cadenas](http://radio-canada.ca/). Serait-ce à cause de leurs commandites de char, qu'ils ne peuvent pas se permettre de parler de ça trop fort? Enfin, pour leur faire justice, la SRC présentait les nouvelles suivantes il y a longtemps:

* [Le Groenland perd sa calotte](http://www.radio-canada.ca/url.asp?/nouvelles/Santeeducation/nouvelles/200404/08/001-groenland.shtml)
* [Fonte accélérée de la calotte glaciaire](http://www.radio-canada.ca/nouvelles/Santeeducation/nouvelles/200509/04/001-groenland-greenpeace.shtml)
* [Des scientifiques canadiens affirment que la calotte glaciaire continue de fondre](http://www.radio-canada.ca/regions/manitoba/nouvelles/2002/archives/index.asp?val=32453)

"We are all going to die, I saw it on TV, it's true now."

[[!tag "vraie vie" "santé"]]