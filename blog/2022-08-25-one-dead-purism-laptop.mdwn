[[!meta title="One dead Purism laptop"]]

The "série noire" continues. I ordered my first Purism Librem 13v4
laptop in April 2019 and it arrived, unsurprisingly, more than three
weeks later. But more surprisingly, it did not work at all: a problem
eerily similar to [this post talking about a bricked Purism
laptop](https://forums.puri.sm/t/librem-13v3-bricked/5714). Thankfully, Purism was graceful enough to cross-ship a
replacement, and once I paid the extra (gulp) 190$ Fedex fee, I had my
new elite laptop read.

Less than a year later, the right USB-A port breaks: it would deliver
power, but no data signal (nothing in `dmesg` or `lsusb`). Two months
later, the laptop short-circuits and completely dies. And here goes
another RMA, this time without a shipping label or cross shipping, so
I had to pay shipping fees.

Now the third laptop in as many years is as good as dead. The left
hinge basically broke off. Earlier this year, I had noticed something
was off with the lid: it was wobbly. I figured that it was just the
way that laptop was, "they don't make it as sturdy as they did in the
good old days, do they". But it was probably a signal of some much
worse problem. Eventually, the bottom panel actually cracked open, and
I realized that some internal mechanism had basically exploded.

The hinges of the Librem are screwed into little golden sprockets that
are fitted in plastic shims of the laptop casing. The shims had
exploded: after opening the back lid, they litterally fell off
(alongside the tiny golden sprocket). Support confirmed that I needed
a case replacement, but unfortunately they were "out of stock" of
replacement cases for the Librem 13, and have been for a while. I am
13 on the waiting list, apparently.

So this laptop is basically dead for me right now: it's my travel
laptop. It's primary purpose is to sit at home until I go to a
conference or a meeting or a cafe or upstairs or wherever to do some
work. I take the laptop, pop the lid, tap-tap some work, close the
lid. Had I used that laptop as my primary device, I would probably have
closed and opened that lid thousands of times. But because it's a
travel laptop, that number is probably in the hundreds, which means
this laptop is not designed to withstand prolonged use.

I have now ordered a [framework laptop](https://frame.work/), 12th generation. I have
some questions about their compatibility with Debian (and Linux in
general), and concerns about power usage, but it certainly can't be
worse than the Purism, in any case. And it can only get better over
time: the main board is fully replaceable, and they have replacement
hinges on stock, although the laptop itself is currently in pre-order
(slated for September). I will probably post a full review when I
actually lay my hand on this device.

In the meantime, I strongly discourage anyone from buying Purism
products, [[as I previously
did|blog/2020-07-13-not-recommending-purism]]. You can the full
maintenance history of the laptop in the [[review
page|hardware/laptop/purism-librem13v4]] as well.

[[!tag debian-planet python-planet hardware review laptop]]
