[[!meta title="How to nationalize the internet in Canada"]]

[Rogers][] had a catastrophic failure in July
2022. It affected emergency services (as in: people couldn't call 911,
but also some 911 services themselves failed), hospitals (which
couldn't access prescriptions), banks and payment systems (as payment
terminals stopped working), and regular users as well. The outage
lasted almost a full day, and Rogers took days to give any technical
explanation on the outage, and even when they did, details were
sparse. So far the only detailed account is from outside actors like
[Cloudflare][] which seem to point at an internal BGP failure.

Its impact on the economy has yet to be measured, but it probably cost
millions of dollars in wasted time and possibly lead to
[life-threatening situations][]. Apart from holding Rogers (criminally?)
responsible for this, what should be done in the future to avoid such
problems?

It's not the first time something like this has happened: it happened to
[Bell Canada][] as well. The Rogers outage is also strangely similar
to the [Facebook outage][] last year, but, to its credit, Facebook
did post a [fairly detailed explanation only a day later][].

The internet is designed to be decentralised, and having
large companies like Rogers hold so much power is a crucial mistake
that should be reverted. The question is how. Some [critics][] were
quick to point out that we need more ISP diversity and competition,
but I think that's missing the point. Others have suggested that the
internet should be a [public good][] or even straight out
[nationalized][].

I believe the solution to the problem of large, private, centralised
telcos and ISPs is to replace them with smaller, public, decentralised
service providers. The only way to ensure that works is to make sure
that public money ends up creating infrastructure controlled by the
public, which means treating ISPs as a public utility. This has been
implemented elsewhere: it works, it's cheaper, and provides better
service.

> Note, this is an idea I have already [[proposed in
> 2012|blog/2012-06-20-pourquoi-un-monopole-sur-linternet-et-une-solution-reseau-quebec]]. See
> also the [[shorter, french
> version|blog/2022-09-25-pourquoi-nationaliser]].

[[!toc levels=2]]

# A modest proposal

Global wireless services (like phone services) and home internet
inevitably grow into monopolies. They are public utilities, just like
water, power, railways, and roads. The question of how they should be
managed is therefore inherently political, yet people don't seem to
question the idea that only the market (i.e. "competition") can solve
this problem. I disagree.

[10 years ago][] (in french), I suggested we, in Québec, should
nationalize large telcos and internet service providers. I no longer
believe is a realistic approach: most of those companies have crap
copper-based networks (at least for the last mile), yet are worth
billions of dollars. It would be prohibitive, and a waste, to buy them
out.

Back then, I called this idea "Réseau-Québec", a reference to the
already nationalized power company, [Hydro-Québec][]. (This idea,
incidentally, made it into the [plan of a political party][].)

Now, I think we should instead build our own, public internet. Start
setting up municipal internet services, fiber to the home in all
cities, progressively. Then interconnect cities with fiber, and build
peering agreements with other providers. This also includes a bid on
wireless spectrum to start competing with phone providers as well.

And while that sounds really ambitious, I think it's possible to take
this one step at a time.

## Municipal broadband

In many parts of the world, [municipal broadband][] is an elegant
solution to the problem, with solutions ranging from [Stockholm's
city-owned fiber network][] (dark fiber, [layer 1][]) to [Utah's
UTOPIA network][] ([fiber to the premises][], [layer 2][]) and
[municipal wireless networks][] like [Guifi.net][] which connects
about 40,000 nodes in Catalonia.

A good first step would be for cities to start providing broadband
services to its residents, directly. Cities normally own sewage and
water systems that interconnect most residences and therefore have
direct physical access everywhere. In [Montréal][], in particular,
there is an ongoing project to replace a *lot* of old [lead-based
plumbing][] which would give an
opportunity to lay down a wired fiber network across the city.

This is a wild guess, but I suspect this would be much less
expensive than one would think. [Some people agree with me and quote
this as low as 1000$ per household][]. There is about [800,000
households in the city of Montréal][], so we're talking about a 800
million dollars investment here, to *connect every household in
Montréal with fiber* and incidentally a quarter of the province's
population. And this is not an up-front cost: this can be built
progressively, with expenses amortized over many years.

(We should not, however, connect Montréal *first*: it's used as an
example here because it's a large number of households to connect.)

Such a network should be built with a redundant topology.
I leave it as an open question whether we
should adopt Stockholm's more minimalist approach or provide direct IP
connectivity. I would tend to favor the latter, because then you can
immediately start to offer the service to households and generate
revenues to compensate for the capital expenditures.

Given the ridiculous profit margins telcos currently have — 8 billion
$CAD net income for BCE ([2019][]), 2 billion $CAD for Rogers
([2020][]) — I also believe this would actually turn into a
profitable revenue stream for the city, the same way Hydro-Québec is
more and more considered as a revenue stream for the state. (I
personally believe that's actually wrong and we should treat those
resources as human rights and not money cows, but I digress. The
point is: this is not a cost point, it's a revenue.)

The other major challenge here is that the city will need competent
engineers to drive this project forward. But this is not different
from the way other public utilities run: we have electrical engineers
at Hydro, sewer and water engineers at the city, this is just another
profession. If anything, the computing science sector might be more at
fault than the city here in its failure to [provide competent and
accountable engineers to society][]...

Right now, most of the network in Canada is copper: we are hitting the
limits of that technology with [DSL][], and while cable has some life
left to it ([DOCSIS][] 4.0 does 4Gbps), that is nowhere near the
[capacity of fiber][]. Take the town of [Chattanooga, Tennessee][]: in
2010, the city-owned ISP [EPB][] finished deploying a fiber network to
the entire town and provided [gigabit internet][] to everyone. Now, 12
years later, they are using *this same network* to provide the
mind-boggling speed of [25 gigabit to the home][]. To give you an
idea, Chattanooga is roughly the size and density of [Sherbrooke][].

[capacity of fiber]: https://en.wikipedia.org/wiki/Fiber-optic_communication#Record_speeds

## Provincial public internet

As part of building a municipal network, the question of getting
access to "the internet" will immediately come up. Naturally, this
will first be solved by using already existing commercial providers to
hook up residents to the rest of the global network.

But eventually, networks should inter-connect: Montréal should connect
with [Laval][], and then [Trois-Rivières][], then [Québec
City][]. This will require long haul fiber runs, but those links are
not actually that expensive, and *many* of those already exist as a
public resource at [RISQ][] and [CANARIE][], which cross-connects
universities and colleges across the province and the
country. Those networks might not have the capacity to cover the
needs of the entire province right now, but that is a router upgrade
away, thanks to the amazing capacity of fiber.

There are two crucial mistakes to avoid at this point. First, the
network needs to remain decentralised. Long haul links should be IP
links with BGP sessions, and each city (or [MRC][]) should have its
own independent network, to avoid Rogers-class catastrophic failures.

Second, skill needs to remain in-house: RISQ has already made that
mistake, to a certain extent, by selling its neutral datacenter.
Tellingly, [MetroOptic][], probably the largest commercial
dark fiber provider in the province, now operates the [QIX][], the second
largest "public" internet exchange in Canada.

Still, we have a lot of infrastructure we can leverage here. If RISQ
or CANARIE cannot be up to the task, Hydro-Québec has power lines
running into every house in the province, with high voltage power lines
running hundreds of kilometers far north. The logistics of long
distance maintenance are already solved by that institution.

In fact, Hydro already [has fiber all over the province][], but it is
a private network, separate from the internet for security reasons
(and that should probably remain so). But this only shows they already
have the expertise to lay down fiber: they would just need to lay down
a parallel network to the existing one.

In that architecture, Hydro would be a "dark fiber" provider.

## International public internet

None of the above solves the problem for
the entire population of Québec, which is notoriously dispersed, with
an area three times the size of France, but with only an eight of its
population (8 million vs 67). More specifically, Canada was originally
a [french colony][], a land violently stolen from native people who
have lived here for thousands of years. Some of those people now live
in reservations, sometimes [far from urban centers][] (but definitely
[not always][]). So the idea of leveraging the Hydro-Québec
infrastructure doesn't always work to solve this, because while Hydro
will happily [flood a traditional hunting territory for an electric
dam][], they don't bother running power lines to the village they
forcibly moved, powering it instead with noisy and polluting diesel
generators. So before giving me fiber to the home, we should give
power (and [potable water][], for that matter), to those communities
*first*.

Update: some of those people are just [doing it themselves][].

So we need to discuss international connectivity. (How else could we
consider those communities than peer nations anyways?c) Québec has
virtually zero international links. Even in Montréal, which likes to
style itself a major player in gaming, AI, and technology, most
peering goes through either [Toronto][] or [New York][].

That's a problem that we must fix,
*regardless* of the other problems stated here. 
Looking at the [submarine cable map][], we
see very few international links actually landing in Canada. There is
the [Greenland connect][] which connects Newfoundland to Iceland
through Greenland. There's the [EXA][] which lands in Ireland, the UK
and the US, and Google has the [Topaz link][] on the west
coast. That's about it, and none of those land anywhere near any major
urban center in Québec.

We should have a cable running from [France][] up to
[Saint-Félicien][]. There should be a cable from [Vancouver][] to
[China][]. Heck, there should be a fiber cable running all the way
from the [end of the great lakes][] through Québec, then up around the
northern passage and back down to British Columbia. Those cables are
expensive, and the idea might sound ludicrous, but [Russia is actually
planning such a project for 2026][]. The US has cables running all the
way up (and around!) Alaska, neatly bypassing all of Canada in the
process. We just look ridiculous on that map.

(Addendum: I somehow forgot to talk about [Teleglobe][] here was
founded as publicly owned company in 1950, growing international phone
and (later) data links all over the world. It was privatized by the
conservatives in 1984, along with rails and other "crown
corporations". So that's one major risk to any effort to make public
utilities work properly: some government might be elected and promptly
sell it out to its friends for peanuts.)

[doing it themselves]: https://ici.radio-canada.ca/nouvelle/1933268/heiltsuk-changements-climatiques-energie-chauffage-diesel

## Wireless networks

I know most people will have rolled their eyes so far back their heads
have exploded. But I'm not done yet. I want wireless too. And by wireless, I
don't mean a bunch of geeks setting up OpenWRT routers on rooftops. I
[tried that][], and while it was fun and educational, it didn't scale.

A public networking utility wouldn't be complete without providing
cellular phone service. This involves [bidding for frequencies][] at
the federal level, and deploying a rather large amount of
infrastructure, but it could be a later phase, when the engineers and
politicians have proven their worth.

At least part of the Rogers fiasco would have been averted if such a
decentralized network backend existed. One might even want to argue that a separate
institution should be setup to provide phone services, independently
from the regular wired networking, if only for reliability.

Because remember here: the problem we're trying to solve is not just
technical, it's about political boundaries, centralisation, and
automation. If everything is ran by this one organisation again, we
will have failed.

However, I must admit that phone services is where my ideas fall a
little short. I can't help but think it's also an accessible goal —
maybe starting with a [virtual operator][] — but it seems slightly
*less* so than the others, especially considering how closed the phone
ecosystem is.

# Counter points

In debating these ideas while writing this article, the following
objections came up.

## I don't want the state to control my internet

One legitimate concern I have about the idea of the state running the
internet is the potential it would have to censor or control the
content running over the wires.

But I don't think there is necessarily a direct relationship between
resource ownership and control of content. Sure, China has strong
censorship in place, partly implemented through state-controlled
businesses. But Russia also has strong censorship in place, based on
regulatory tools: they force private service providers to install
back-doors in their networks to control content and surveil their
users.

Besides, the USA have been doing [warrantless wiretapping][] since
at least 2003 (and yes, that's 10 years before the [Snowden
revelations][]) so a commercial internet is no assurance that we have
a free internet. Quite the contrary in fact: if anything, the
commercial internet goes hand in hand with the [[neo-colonial
internet|2021-10-23-neo-colonial-internet]], just like [businesses][]
did in the "good old colonial days".

Large media companies are the primary
censors of content here. In Canada, the media cartel requested the
[first site-blocking order][] in 2018. The plaintiffs (including
Québecor, Rogers, and Bell Canada) are both content providers and
internet service providers, an obvious conflict of interest.

Nevertheless, there are some strong arguments against having a
centralised, state-owned monopoly on internet service providers. [FDN
makes a good point on this][]. But this is not what I am suggesting:
at the provincial level, the network would be purely physical, and
regional entities (which could include private companies) would peer
over that physical network, ensuring decentralization. Delegating the
management of that infrastructure to an independent non-profit or
cooperative (but owned by the state) would also ensure some level of
independence.

Update: [this article from the OECD forum][] talks about such
redundancy is also important in war time.

[this article from the OECD forum]: https://www.oecd-forum.org/posts/fragility-digitally-the-internet-during-world-war

## Isn't the government incompetent and corrupt?

Also known as "private enterprise is better skilled at handling this,
the state can't do anything right"

I don't think this is a "fait accomplit". If anything, I have found
publicly ran utilities to be spectacularly reliable here. I rarely
have trouble with sewage, water, or power, and keep in mind I live in
a city where we receive about [2 meters of snow a year][], which tend
to create lots of trouble with power lines. Unless there's a major
weather event, power just runs here.

I think the same can happen with an internet service provider. But it
would certainly need to have higher standards to what we're used to,
because frankly Internet is kind of janky.

## A single monopoly will be less reliable

I actually agree with that, but that is not what I am
proposing anyways. 
Current commercial or non-profit entities will be free to offer their services on
top of the public network. 

And besides, the current "ha! diversity is
great" approach is exactly what we have now, and it's not working. 
The pretense that we can have competition over a single network is
what led the US into the ridiculous situation where they also pretend
to have competition over the power utility market. This led to
[massive forest fires in California][] and [major power outages in
Texas][]. It doesn't work.

## Wouldn't this create an isolated network?

One theory is that this new network would be so hostile to incumbent
telcos and ISPs that they would simply refuse to network with the
public utility. And while it is true that the telcos currently *do*
also act as a kind of "[tier one][]" provider in some places, I
strongly feel this is *also* a problem that needs to be solved,
regardless of ownership of networking infrastructure.

Right now, telcos often hold both ends of the stick: they are the
gateway to users, the "last mile", but they *also* provide peering to
the larger internet in some locations. In at least one datacenter in
downtown Montréal, I've seen traffic go through Bell Canada that was
not directly targeted at Bell customers. So in effect, they are in a
position of charging *twice* for the same traffic, and that's not only
ridiculous, it should just be plain illegal.

And besides, this is not a big problem: there *are* other providers
out there. As bad as the market is in Québec, there is still *some*
diversity in Tier one providers that could allow for some exits to the
wider network (e.g. yes, [Cogent][] is here too).

## What about Google and Facebook?

Nationalization of other service
providers like Google and Facebook is out of scope of this discussion.

That said, I am not sure the state should get into the business of
organising the web or providing content services however, but I will
point out it already does do some of that through its own
[websites][]. It should probably keep itself to this, and also
consider providing normal services for people who don't or can't
access the internet.

(And I would also be ready to argue that Google and Facebook already
act as extensions of the state: certainly if Facebook didn't exist,
the CIA or the NSA would like to create it at this point. And Google
has lucrative business with the [US department of defense][].)

# What does not work

So we've seen one thing that could work. Maybe it's too
expensive. Maybe the political will isn't there. Maybe it will
fail. We don't know yet.

But we know what does *not* work, and it's what we've been doing ever
since the internet has gone commercial.

## Legal pressure and regulation

In 1984 (of all years), the US Department of Justice finally
[broke up AT&T][] in half a dozen corporations, after a 10 year legal
battle. Yet a decades later, we're back to only *three* large
providers doing essentially what AT&T was doing back then, and those
are regional monopolies: AT&T, Verizon, and Lumen (not counting
[T-Mobile][] that is from a different breed). So the legal approach
really didn't work that well, especially considering the political
landscape changed in the US, and the FTC seems [perfectly happy to let
those major mergers continue][].

In Canada, we never even pretended we would solve this problem at all:
[Bell Canada][bell-history] (the literal "[father][]" of AT&T) is in the same
situation now. We have either a regional monopoly (e.g. Videotron for
cable in Québec) or an oligopoly (Bell, Rogers, and Telus controlling
more than 90% of the market). Telus does have *one* competitor in the
west of Canada, [Shaw][], but Rogers has been [trying to buy it
out][]. The [competition bureau][] seems to have blocked the merger
for now, but it didn't stop other recent mergers like [Bell's
acquisition one of its main competitors in Québec, eBox][].

Regulation doesn't seem capable of ensuring those profitable
corporations provide us with decent pricing, which makes Canada [one
of the most expensive countries][] ([research][]) for mobile data on
the planet. The recent failure of the CRTC to properly protect smaller
providers has even lead to [price hikes][]. Meanwhile the oligopoly
is actually [agreeing on their own price hikes][] therefore becoming
a real [cartel][], complete with [price fixing][] and reductions in
output.

There are actually regulations in Canada supposed to keep the worst of
the Rogers outage from happening at all. [According to CBC][]:

> Under Canadian Radio-television and Telecommunications Commission
> (CRTC) rules in place since 2017, telecom networks are supposed to
> ensure that cellphones are able to contact 911 even if they do not
> have service.

I could personally confirm that my phone couldn't reach 911 services,
because *all* calls would fail: the problem was that towers were still
up, so your phone wouldn't fall back to alternative service providers
(which could have resolved the issue). I can only speculate as to why
Rogers didn't take cell phone towers out of the network to let phones
work properly for 911 service, but it seems like a dangerous game to
play.

Hilariously, the CRTC itself didn't have a reliable phone service due
to the service outage:

> Please note that our phone lines are affected by the Rogers network outage.
> Our website is still available: https://crtc.gc.ca/eng/contact/

<https://mobile.twitter.com/CRTCeng/status/1545421218534359041>

<!-- https://mobile.twitter.com/CRTCfra/status/1545421812166713346 -->

I wonder if they will file a complaint against Rogers themselves about
this. I probably should.

It seems the federal government is thinking more of the same medicine
will fix the problem and has told [companies should "help" each other
in an emergency][]. I doubt this will fix anything, and could
actually make things worse if the competitors actually interoperate
more, as it could cause multi-provider, cascading failures.

## Subsidies

The absurd price we pay for data does not actually mean everyone gets
high speed internet at home. Large swathes of the Québec countryside
don't get broadband at all, and it can be difficult or expensive, even
in large urban centers like Montréal, to get high speed internet.

That is despite having a series of subsidies that *all* avoided
investing in our own infrastructure. We had the "[fonds de l'autoroute
de l'information][]", "information highway fund" (site dead since
2003, [archive.org link][]) and "[branchez les familles][]",
"connecting families" (site dead since 2003, [archive.org
link][connecting-families])
which subsidized the development of a copper network. In 2014, more of
the same: the federal government poured hundreds of millions of
dollars into a program called [connecting Canadians][] to connect 280
000 households to "high speed internet". And now, the federal and
provincial governments are proudly announcing that "[everyone is now
connected to high speed internet][]", after pouring more than 1.1
*billion* dollars to connect, guess what, another 380 000 homes, right
in time for the provincial election.

Of course, technically, the deadline won't *actually* be met
until 2023. Québec is a big area to cover, and you can guess what
happens next: the telcos threw up their hand and said some areas just
can't be connected. (Or they [connect their CEO but not the poor folks
across the lake][].) The story then takes the predictable twist of
[giving more money out to billionaires, subsidizing now Musk's
Starlink system][] to connect those remote areas.

To give a concrete example: a friend who lives about 1000km away from
Montréal, 4km from a [small, 2500 habitant village][], has recently
got symmetric 100 mbps fiber at home from Telus, thanks to those subsidies. But I can't get that
service in Montréal at all, presumably because Telus and Bell colluded
to split that market. Bell doesn't provide me with such a service
either: they tell me they have "fiber to my neighborhood", and only
offer me a 25/10 mbps ADSL service. (There is Vidéotron offering
400mbps, but that's copper cable, again a dead technology, and
asymmetric.)

# Conclusion

Remember Chattanooga? Back in 2010, they funded the development of a
fiber network, and now they have deployed a network roughly a
*thousand* times faster than what we have just funded with a billion
dollars. In 2010, I was [[paying Bell
Canada|2011-12-22-bell-arretera-le-throttling-mais-restent-des-voleurs]]
60$/mth for 20mbps and a 125GB cap, and now, I'm *still* (indirectly)
paying Bell for roughly the same speed (25mbps). Back then, Bell was
throttling their competitors networks until 2009, when they were
[[forced by the CRTC to stop
throttling|2009-10-23-about-crtc-decision-network-neutrality]]. Both
Bell and Vidéotron still explicitly forbid you from running your own servers
at home, Vidéotron charges prohibitive prices which make it near
impossible for resellers to sell uncapped services. Those companies
are *not* spurring innovation: they are blocking it.

We have spent all this money for the private sector to build
us a private internet, over decades, without any assurance of quality,
equity or reliability. And while in some locations, ISPs *did* deploy
fiber to the home, they certainly didn't upgrade their entire network
to follow suit, and even less allowed resellers to compete on that
network.

In 10 years, when 100mbps will be laughable, I bet those service
providers will again punt the ball in the public courtyard and tell us
they don't have the money to upgrade everyone's equipment.

We got screwed. It's time to try something new.

# Updates

## Hacker news

There was a [discussion about this article on Hacker News](https://news.ycombinator.com/item?id=32646836) which
was surprisingly productive. Trigger warning: Hacker News is kind of
right-wing, in case you didn't know.

## Acquisitions

Since this article was written, at least two *more* major acquisitions
happened, just in Québec:

 * [Bell acquired Distributel](https://www.lapresse.ca/affaires/entreprises/2022-09-02/bell-met-la-main-sur-distributel.php)
 * [Québecor acquired vMedia](https://www.lapresse.ca/affaires/entreprises/2022-07-29/region-de-toronto/quebecor-achete-le-fournisseur-independant-vmedia.php)

In the latter case, vMedia was explicitly saying it couldn't grow
because of "lack of access to capital". So basically, we have given
those companies a billion dollars, and they are not using that very
money to buy out their competition. At *least* we could have given
that money to small players to even out the playing field. But this is
not how that works at all. Also, in a bizarre twist, an "analyst"
believes the acquisition is likely to help Rogers acquire Shaw.

## Washington Post article

Also, since this article was written, the Washington Post [published a
review](https://www.washingtonpost.com/outlook/2022/06/24/plan-fix-internet-put-government-charge/) of a book bringing similar ideas: [Internet for the People
The Fight for Our Digital Future](https://www.versobooks.com/books/3927-internet-for-the-people), by Ben Tarnoff, at Verso
books. It's short, but even more ambitious than what I am suggesting
in this article, arguing that all big tech companies should be broken
up and better regulated:

> He pulls from [Ethan Zuckerman](https://ethanzuckerman.com/)’s idea of a web that is “[plural in
> purpose](https://ethanzuckerman.com/2019/07/31/beyond-the-vast-wasteland-briefing-congresspeople-for-the-aspen-institute/)” — that just as pool halls, libraries and churches each have
> different norms, purposes and designs, so too should different
> places on the internet. To achieve this, Tarnoff wants governments
> to pass laws that would make the big platforms unprofitable and, in
> their place, fund small-scale, local experiments in social media
> design. Instead of having platforms ruled by engagement-maximizing
> algorithms, Tarnoff imagines public platforms run by local
> librarians that include content from public media.

(Links mine: the Washington Post obviously prefers to not link to the
real web, and instead doesn't link to Zuckerman's site all and
suggests Amazon for the book, in a cynical example.)

## AMBER failure

And in another example of how the private sector has failed us, there
was recently a fluke in the [AMBER alert system](https://en.wikipedia.org/wiki/Amber_alert) where the *entire*
province was warned about a loose shooter in [Saint-Elzéar](https://en.wikipedia.org/wiki/Saint-Elz%C3%A9ar,_Gasp%C3%A9sie%E2%80%93%C3%8Eles-de-la-Madeleine,_Quebec) [except
the people in the town](https://ici.radio-canada.ca/nouvelle/1910535/quebec-en-alerte-saint-elzear-bonaventure-reseau-cellulaire-region), because they have spotty cell phone
coverage. In other words, *millions* of people received a strongly
toned, "life-threatening", alert for a city sometimes hours away,
except the people most vulnerable to the alert. Not missing a beat,
the CAQ party is promising more of the same medicine again and [giving
more money to telcos](https://www.lapresse.ca/elections-quebec/2022-09-05/legault-promet-une-connexion-cellulaire-partout-d-ici-2026.php) to fix the problem, suggesting to spend
*three billion* dollars in private infrastructure.

## New CAQ fund and french article

A french redux of this article was posted
[[here|blog/2022-09-25-pourquoi-nationaliser]], and sent to [Le
Devoir](https://www.ledevoir.com/), which never published it.

Instead, they published what seems to be [trial balloon](https://en.wikipedia.org/wiki/Trial_balloon) [smack on
the frontpage](https://www.ledevoir.com/politique/quebec/769092/politique-quebecoise-plaidoyer-pour-un-hydro-quebec-de-la-fibre-optique). Basically, the CAQ now believes they need to invest
in public fiber networks and have reserved 3 billion dollars to do
... something. There's some vague talks about dark fiber with Hydro
and noises made about how we nationalized Hydro back in the glory
days. The article and the CAQ are very careful at attributing those
ideas to anyone else, either here or to [Québec Solidaire](https://en.wikipedia.org/wiki/Qu%C3%A9bec_solidaire), which
has had this in their program since 2018.

I suspect this is going to turn out to be yet another wealth transfer
from the public to the private, but who knows, things might turn out
... okay?

In the meantime, I still [don't have access to fiber](https://www.dslreports.com/forum/r33525932-) in the center
of the largest french-speaking city of the western hemisphere and the
second largest city in Canada. And that, even if Bell's old competitor
(and now subsidiary) [Ebox is now offering fiber to the
home](https://www.dslreports.com/forum/r33497645-Nouveaut-FTTH-chez-EBOX). Obviously, they haven't increased coverage significantly.

[[!tag "network neutrality" bell debian-planet python-planet internet
québec reseaulibre canada]]

[Rogers]: https://en.wikipedia.org/wiki/Rogers_Communications
[Cloudflare]: https://blog.cloudflare.com/cloudflares-view-of-the-rogers-communications-outage-in-canada/
[life-threatening situations]: https://www.cbc.ca/news/canada/hamilton/rogers-outage-911-call-1.6516958
[Bell Canada]: https://ici.radio-canada.ca/nouvelle/1854653/bell-panne-television-internet-telephonie-froid
[Facebook outage]: https://blog.cloudflare.com/during-the-facebook-outage/
[fairly detailed explanation only a day later]: https://engineering.fb.com/2021/10/05/networking-traffic/outage-details/
[critics]: https://www.ledevoir.com/opinion/libre-opinion/732636/libre-opinion-a-quand-une-loi-antitrust-pour-les-telecoms
[public good]: https://ici.radio-canada.ca/nouvelle/1896908/panne-rogers-reseaux-prives-telecom-economie-canadienne
[nationalized]: https://www.ledevoir.com/societe/732681/remise-en-question-dans-le-monde-des-telecoms
[10 years ago]: https://anarc.at/blog/2012-06-20-pourquoi-un-monopole-sur-linternet-et-une-solution-reseau-quebec/
[Hydro-Québec]: https://en.wikipedia.org/wiki/Hydro-Qu%C3%A9bec
[plan of a political party]: https://quebecsolidaire.net/nouvelle/internet-quebec-solidaire-veut-couper-les-prix-et-garantir-lacces-partout-sur-le-territoire
[municipal broadband]: https://en.wikipedia.org/wiki/Municipal_broadband
[Stockholm's city-owned fiber network]: https://stokab.se/en/stokab
[layer 1]: https://en.wikipedia.org/wiki/Physical_layer
[Utah's UTOPIA network]: https://en.wikipedia.org/wiki/Utah_Telecommunication_Open_Infrastructure_Agency
[fiber to the premises]: https://en.wikipedia.org/wiki/Fiber_to_the_premises
[layer 2]: https://en.wikipedia.org/wiki/Data_link_layer
[municipal wireless networks]: https://en.wikipedia.org/wiki/Municipal_wireless_network
[Guifi.net]: https://en.wikipedia.org/wiki/Guifi.net
[Montréal]: https://en.wikipedia.org/wiki/Montreal
[lead-based plumbing]: https://www.ledevoir.com/societe/sante/567857/comprendre-le-dossier-du-plomb-dans-l-eau-au-quebec
[Some people agree with me and quote this as low as 1000$ per household]: https://news.ycombinator.com/item?id=32036208
[800,000 households in the city of Montréal]: https://ville.montreal.qc.ca/pls/portal/docs/PAGE/MTL_STATS_FR/MEDIA/DOCUMENTS/PROFIL_MENAGES_LOGEMENTS_2016-VILLE_MONTR%C9AL.PDF
[2019]: https://en.wikipedia.org/wiki/BCE_Inc.
[2020]: https://investors.rogers.com/2020-annual-report/
[provide competent and accountable engineers to society]: https://cacm.acm.org/magazines/2022/6/261171-the-software-industry-is-still-the-problem/fulltext
[DSL]: https://en.wikipedia.org/wiki/Digital_subscriber_line#DSL_technologies
[DOCSIS]: https://en.wikipedia.org/wiki/DOCSIS#Versions
[Chattanooga, Tennessee]: https://en.wikipedia.org/wiki/Chattanooga,_Tennessee
[EPB]: https://epb.com
[gigabit internet]: https://www.muninetworks.org/content/chattanooga-announces-1-gbps-tier
[25 gigabit to the home]: https://epb.com/newsroom/press-releases/epb-launches-americas-first-community-wide-25-gig-internet-service/
[Gatineau]: https://en.wikipedia.org/wiki/Gatineau
[Sherbrooke]: https://en.wikipedia.org/wiki/Sherbrooke
[Laval]: https://en.wikipedia.org/wiki/Laval,_Quebec
[Trois-Rivières]: https://en.wikipedia.org/wiki/Trois-Rivi%C3%A8res<
[Québec City]: https://en.wikipedia.org/wiki/Quebec_City
[RISQ]: https://en.wikipedia.org/wiki/R%C3%A9seau_d%27informations_scientifiques_du_Qu%C3%A9bec
[CANARIE]: https://en.wikipedia.org/wiki/CANARIE
[MRC]: https://en.wikipedia.org/wiki/Regional_county_municipality
[QIX]: https://en.wikipedia.org/wiki/Montreal_Internet_Exchange
[MetroOptic]: https://metrooptic.com/en
[has fiber all over the province]: https://www.lapresse.ca/affaires/2020-01-30/internet-en-region-hydro-quebec-prete-a-ceder-de-la-fibre-optique
[french colony]: https://en.wikipedia.org/wiki/French_colonial_empire
[far from urban centers]: https://en.wikipedia.org/wiki/Obedjiwan
[not always]: https://en.wikipedia.org/wiki/Kanesatake
[flood a traditional hunting territory for an electric dam]: https://en.wikipedia.org/wiki/Gouin_Reservoir
[Toronto]: https://en.wikipedia.org/wiki/Toronto
[New York]: https://en.wikipedia.org/wiki/New_York_City
[submarine cable map]: https://www.submarinecablemap.com/
[Greenland connect]: https://www.submarinecablemap.com/submarine-cable/greenland-connect
[EXA]: https://www.submarinecablemap.com/submarine-cable/exa-north-and-south
[Topaz link]: https://www.submarinecablemap.com/submarine-cable/topaz
[France]: https://en.wikipedia.org/wiki/France
[Saint-Félicien]: https://en.wikipedia.org/wiki/Saint-F%C3%A9licien,_Quebec
[Vancouver]: https://en.wikipedia.org/wiki/Vancouver
[China]: https://en.wikipedia.org/wiki/China
[end of the great lakes]: https://en.wikipedia.org/wiki/Superior,_Wisconsin
[Russia is actually planning such a project for 2026]: https://www.submarinecablemap.com/submarine-cable/polar-express
[Teleglobe]: https://en.wikipedia.org/wiki/Teleglobe
[tried that]: https://wiki.reseaulibre.ca/
[bidding for frequencies]: https://en.wikipedia.org/wiki/Spectrum_auction
[virtual operator]: https://en.wikipedia.org/wiki/Mobile_virtual_network_operator
[tier one]: https://en.wikipedia.org/wiki/Tier_1_network
[Cogent]: https://en.wikipedia.org/wiki/Cogent_Communications
[2 meters of snow a year]: https://en.wikipedia.org/wiki/Montreal#Climate
[massive forest fires in California]: https://en.wikipedia.org/wiki/Camp_Fire_(2018)
[major power outages in Texas]: https://en.wikipedia.org/wiki/2021_Texas_power_crisis
[warrantless wiretapping]: https://en.wikipedia.org/wiki/Room_641A
[Snowden revelations]: https://en.wikipedia.org/wiki/Snowden_revelations
[businesses]: https://en.wikipedia.org/wiki/Hudson%27s_Bay_Company
[first site-blocking order]: https://torrentfreak.com/canadas-supreme-court-denies-teksavvys-site-blocking-appeal-220329/
[FDN makes a good point on this]: https://blog.fdn.fr/?post/2011/06/21/Il-ne-faut-pas-nationaliser-les-FAI-%21
[websites]: https://www.canada.ca/
[US department of defense]: https://cloud.google.com/blog/topics/inside-google-cloud/update-on-google-clouds-work-with-the-us-government
[New Deal]: https://en.wikipedia.org/wiki/New_Deal
[broke up AT&T]: https://en.wikipedia.org/wiki/Breakup_of_the_Bell_System
[T-Mobile]: https://en.wikipedia.org/wiki/T-Mobile
[perfectly happy to let those major mergers continue]: https://techcrunch.com/2019/11/05/fcc-approves-t-mobile-sprint-merger-despite-serious-concerns/
[bell-history]: https://en.wikipedia.org/wiki/Bell_Canada#History
[father]: https://en.wikipedia.org/wiki/Bell_Canada#Inception
[Shaw]: https://en.wikipedia.org/wiki/Shaw_Communications
[trying to buy it out]: https://en.wikipedia.org/wiki/Shaw_Communications#Acquisition_by_Rogers
[competition bureau]: https://en.wikipedia.org/wiki/Competition_Bureau
[Bell's acquisition one of its main competitors in Québec, eBox]: https://www.ledevoir.com/economie/685926/telecommunications-exode-des-clients-d-ebox-apres-son-acquisition-par-bell
[one of the most expensive countries]: https://ici.radio-canada.ca/nouvelle/1726369/etude-prix-donnees-data-mobile-cellulaire-telephonie-canada-comparaison-mobile-co-uk-pierre-larouche
[research]: https://www.cable.co.uk/mobiles/worldwide-data-pricing/
[price hikes]: https://www.lapresse.ca/affaires/entreprises/2022-01-13/internet/des-hausses-minimales-des-prix-en-attendant-ottawa.php
[agreeing on their own price hikes]: https://plus.lapresse.ca/screens/a83efcd5-196b-4868-96a8-1cdb73d5eb9c__7C___0.html?utm_content=ulink&utm_source=lpp&utm_medium=referral&utm_campaign=internal+share
[cartel]: https://en.wikipedia.org/wiki/Cartel
[price fixing]: https://en.wikipedia.org/wiki/Price_fixing
[According to CBC]: https://www.cbc.ca/news/business/rogers-outage-cell-mobile-wifi-1.6514373
[companies should "help" each other in an emergency]: https://ici.radio-canada.ca/nouvelle/1897295/telecoms-entraide-urgence-panne-rogers-champagne
[fonds de l'autoroute de l'information]: https://www.autoroute.gouv.qc.ca/
[archive.org link]: https://web.archive.org/web/20031204143044/http://www.autoroute.gouv.qc.ca/
[branchez les familles]: https://www.familles.mic.gouv.qc.ca
[connecting Canadians]: https://www.ic.gc.ca/eic/site/028.nsf/eng/50010.html
[everyone is now connected to high speed internet]: https://www.lapresse.ca/debats/editoriaux/2022-08-22/ca-y-est-tout-le-quebec-est-en-haute-vitesse.php
[connect their CEO but not the poor folks across the lake]: https://www.cbc.ca/news/canada/ottawa/bell-ceo-cottage-pemichangan-lake-1.5925882
[giving more money out to billionaires, subsidizing now Musk's Starlink system]: https://ici.radio-canada.ca/nouvelle/1881709/internet-haute-vitesse-branchement-regions-quebec-couts
[small, 2500 habitant village]: https://en.wikipedia.org/wiki/Maria,_Quebec
[potable water]: https://www.theguardian.com/world/2021/apr/30/canada-first-nations-justin-trudeau-drinking-water
[connecting-families]: https://web.archive.org/web/20030619020646/https://www.familles.mic.gouv.qc.ca/
