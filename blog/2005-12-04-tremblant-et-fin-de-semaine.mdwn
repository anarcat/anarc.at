<!-- Drupal node 57 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Tremblant et fin de semaine"]]
[[!meta date="2005-12-04T23:52:27-0500"]]
[[!meta updated="2011-12-22T01:25:51-0500"]]
[[!meta guid="57 at http://anarcat.koumbit.org"]]

Ouf! Grosse semaine, grosse fin de semaine... Cette semaine, j'ai déménagé mon ancienne coloc et de chambre. Beaucoup de travail pour tout démonter et remonter quelques mètres cubes à côté, mais ça en a valu la peine. Je suis beaucoup mieux installé. Enfin, c'est certainement pas le dernier déménagement que je me tape, mais j'espère que ça va se calmer, parce que j'en ai toujours aussi marre de déménager.

Pour couronner le tout, nous avons fait une bonne marche dans les superbes neiges des laurentides, dans le parc national du Mont Tremblant... Quelques 10km, ce n'était pas ce que nous avions prévu (le Mont Marcy) mais quand même superbe. C'est certain, avec le recul, que les Adirondacks, c'était loin et un peu fou, mais j'aurais quand même aimé tenter l'expérience. Ça sera pour une autre fois...

Dire qu'une nouvelle semaine commence demain, j'arrive à peine à y croire... au sommeil bien mérité!

[[!tag "vraie vie" "plein air"]]