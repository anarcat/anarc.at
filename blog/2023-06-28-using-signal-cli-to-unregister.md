[[!meta title="Using signal-cli to cancel your Signal account"]]

For [[obscure reasons|hardware/tablet/gts4lvwifi]], I have found
myself with a phone number registered with Signal but without any
device associated with it.

This is the [I lost my phone](https://support.signal.org/hc/en-us/articles/360007062452-What-do-I-do-if-my-phone-is-lost-or-stolen-) section in Signal support, which
rather unhelpfully tell you that, literally:

> Until you have access to your phone number, there is nothing that
> can be done with Signal.

To be fair, I guess that sort of makes sense: Signal relies heavily on
phone numbers for identity. It's how you register to the service and
how you recover after losing your phone number. If you have your PIN
ready, you don't even change safety numbers!

But my case is different: this phone number was a test number,
associated with my tablet, because you [can't link multiple Android
device to the same phone number](https://community.signalusers.org/t/allow-android-ios-devices-e-g-tablets-but-also-phones-to-be-linked-to-the-primary-device-i-e-used-as-secondary-device-like-the-desktop-app/2884). And now that I brilliantly
[[bricked that tablet|hardware/tablet/gts4lvwifi]], I just need to
tell people to stop trying to contact me over that thing (which wasn't
really working in the first place anyway because I wasn't using the
tablet that much, but I digress).

So. What do you do? You *could* follow the above "lost my phone" guide
and get a new Android or iOS phone to register on Signal again, but
that's pretty dumb: I don't want another phone, I already have one.

Lo and behold, [signal-cli](https://github.com/AsamK/signal-cli) to the rescue!

# Disclaimer: no warranty or liability

Before following this guide, make sure you remember the
[[license|meta/license]] of this website, which specifically has a
**Section 5 – Disclaimer of Warranties and Limitation of Liability.**

If you follow this guide literally, you might actually get into
trouble.

You have been warned. All Cats Are Beautiful.

# Installing in Docker

Because [signal-cli is not packaged in Debian](https://bugs.debian.org/926807) (but really should
be), I need to bend over backwards to install it. The installation
instructions suggest [building from source](https://github.com/AsamK/signal-cli#building) (what is this,
GentooBSD?) or [installing binary files](https://github.com/AsamK/signal-cli/releases/latest) (what is this,
Debiandows?), that's all so last millennium. I want something fresh
and fancy, so I went with the extremely legit [Docker registry](https://gitlab.com/packaging/signal-cli/container_registry) ran
by the not-shady-at-all [gitlab.com/packaging group](https://gitlab.com/packaging) which is
suspiciously [not owned by any GitLab.com person I know of](https://gitlab.com/packaging/signal-cli/-/project_members). 

This is surely perfectly safe.

> (Insert long digression on supply chain security here and how Podman
> is so much superior to Docker. Feel free to dive deep into how
> [RedHat sold out](https://lwn.net/Articles/936405/) [to the nazis](https://en.wikipedia.org/wiki/IBM_and_World_War_II) or how this is just me
> ranting about something I don't understand, again. I'm not going to
> do all the work for you.)

Anyway.

The magic command is:

    mkdir .config/signal-cli
    podman pull registry.gitlab.com/packaging/signal-cli/signal-cli-jre:latest
    # lightly hit computer with magic supply chain verification wand
    alias signal-cli="podman run --rm --publish 7583:7583 --volume .config/signal-cli:/var/lib/signal-cli --tmpfs /tmp:exec   registry.gitlab.com/packaging/signal-cli/signal-cli-jre:latest --config /var/lib/signal-cli"
    
At this point, you have a `signal-cli` alias that should more or less
behave as per [upstream documentation](https://github.com/AsamK/signal-cli/wiki/Quickstart). Note that it sets up a
network service on port `7583` which is unnecessary because you likely
won't be using `signal-cli`'s "daemon mode" here, this is a one-shot
thing. But I'll probably be reusing those instructions later on, so I
figured it might be a safe addition. Besides, it's what [the
instructions told me to do](https://packaging.gitlab.io/signal-cli/installation/docker/) so I'm blindly slamming my head in the
bash pipe, as trained.

Also, you're going to have the `signal-cli` configuration persist in
`~/.config/signal-cli` there. Again, totally unnecessary.

# Re-registering the number

Back to our original plan of canceling our Signal account. The next
step is, of course, to register with Signal.

> Yes, this is a little counter-intuitive and you'd think there would
> be a "I want off this boat" button on <https://signal.org> 
> that would do this for you, but hey, I guess that's only reserved for
> elite hackers who want to <del>screw people over</del>, I mean close
> their accounts. Mere mortals don't get access to such beauties.
>
> Update: a friend reminded me there *used* to be such a page at
> <https://signal.org/signal/unregister/> but it's mysteriously gone
> from the web, but [still available on the wayback machine](https://web.archive.org/web/20210217025218/https://signal.org/signal/unregister/)
> although surely that doesn't work anymore. Untested.

To register an account with `signal-cli`, you first need to [pass a
CAPTCHA](https://github.com/AsamK/signal-cli/wiki/Registration-with-captcha). Those are the funky images generated by deep neural
networks that try to fool humans into thinking other neural networks
can't break them, and generally annoy the hell out of people. This
will generate a URL that looks like:

    signalcaptcha://signal-hcaptcha.$UUID.registration.$THIRTYTWOKILOBYTESOFGARBAGE

Yes, it's a very long URL. Yes, you need the entire thing.

The URL is hidden behind the `Open Signal` link, you can right-click
on the link to copy it or, if you want to feel like it's [1988
again][], use `view-source:` or [butterflies](https://www.explainxkcd.com/wiki/index.php/378:_Real_Programmers) or something.

[1988 again]: https://en.wikipedia.org/wiki/Hackers_(film)

You will also need the phone number you want to unregister here,
obviously. We're going to take a not quite random phone number as an
example, [+18002677468](https://www.youtube.com/watch?v=IWsUZT1YHno). 

> Don't do this at home kids! Use the actual number and don't
> copy-paste examples from random websites!

So the *actual* command you need to run now is:

    signal-cli -a +18002677468 register --captcha signalcaptcha://signal-hcaptcha.$UUID.registration.$THIRTYTWOKILOBYTESOFGARBAGE

To confirm the registration, Signal will send a text message (SMS) to
that phone number with a verification code. (Fun fact: it's actually
[Twilio](https://en.wikipedia.org/wiki/Twilio) relaying that message for Signal and that is... [not
great](https://support.signal.org/hc/en-us/articles/4850133017242-Twilio-Incident-What-Signal-Users-Need-to-Know-).)

If you don't have access to SMS on that number, you can try again with
the `--voice` option, which will do the same thing with a actual phone
call. I wish it would say "Ok boomer" when it calls, but it doesn't.

If you don't have access to either, you're screwed. You may be able to
[port your phone number](https://en.wikipedia.org/wiki/Local_number_portability) to another provider to gain control of the
phone number again that said, but at that point it's a whole different
ball game.

With any luck now you've received the verification code. You use it with:

    signal-cli -a +18002677468 verify 131213

If you want to make sure this worked, you can try writing to another
[not random number at all](https://web.archive.org/web/20170625102110/http://www1.nyc.gov/site/nypd/about/about-nypd/contact-us.page), it should Just Work:

    signal-cli -a +18002677468 send -mtest +18005778477

> This is almost without any warning on the other end too, which says
> something amazing about Signal's usability and something horrible
> about its security.

# Unregistering the number

Now we get to the final conclusion, the climax. Can you feel it? I'll
try to refrain from further rants, I promise.

It's pretty simple and fast, just call:

    signal-cli -a +18002677468 unregister

That's it! Your peers will now see an "Invite to Signal" button
instead of a text field to send a text message.

# Cleanup

Optionally, cleanup the mess you left on this computer:

    rm -r ~/.config/signal-cli
    podman image rm registry.gitlab.com/packaging/signal-cli/signal-cli-jre

[[!tag debian-planet signal security phone hacking]]

<!-- posted to the federation on 2023-06-29T00:41:33.507097 -->
[[!mastodon "https://kolektiva.social/@Anarcat/110625665400013564"]]
