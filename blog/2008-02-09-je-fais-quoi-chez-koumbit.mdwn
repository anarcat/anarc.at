<!-- Drupal node 127 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Je fais quoi chez Koumbit?"]]
[[!meta date="2008-02-09T20:12:17-0500"]]
[[!meta updated="2012-12-14T22:45:09-0500"]]
[[!meta guid="127 at http://anarcat.koumbit.org"]]

Alors, je fais quoi chez Koumbit ces temps-ci? Toujours difficile de sortir un aperçu, une idée de ce qui est produit, concrètement, quand on a continuellement la tête dans la "job". J'ai donc produit un petit outil pour m'aider à examiner un peu plus ce que je fais au sein de l'organisation.<!--break--> C'est principalement une modification au [Timetracker](http://phptimetracker.sf.net/)  que j'ai écrit il y a déjà longtemps pour faire le suivi des feuilles de temps de l'organisation. Les rapports qu'on peut maintenant sortir examinent les champs "commentaires" complétés par l'usager lorsqu'il rempli sa feuille de temps. Dans mon cas, pour l'année 2007, j'ai la liste de tags suivants:

* meeting	85h
* fix	31h
* homere	31h
* upgrade	30h
* rtr1	24h
* setup	23h
* fixing	22h
* rt	21h
* nagios	21h
* conciliation	20h

Gardez en tête que ceci sont seulement des descriptifs ajoutés aux "punchs" de façon plus ou moins laxe.. En intégrant le nom des projets comme tags, on obtient une liste un peu plus intéressante et probablement plus représentative:

* sysadmin	275h
* comptabilité	108h
* meeting	85h
* maintenance	69h
* secrétariat	50h
* autres	41h
* férié	40h
* congé	40h
* stabilisation	40h
* développement	40h

Donc en bref, je fais quoi moi chez Koumbit?

* Sysadmin
* Comptabilité
* Coordination
* Beaucoup, beaucoup (trop?) de réunions
* Du secrétariat
* De la maintenance (fix, fixing, maintenance)
* Du développement/recherche (développement, rtr1, ...)
* Bien de la "poutine" ("autres")
* Beaucoup d'urgences ("urgence", "plantage", "crash" font chacun ~20h chacun)

De ces rapports, je trouve aussi des réalisations que j'avais plus ou moins pris pour acquis ou même oublié:

* Développement continu d'[AlternC](http://alternc.org/) (27h)
* Setup d'un nouveau routeur pour permettre un meilleur contrôle sur les statistiques réseau et améliorer la fiabilité du réseau ([rtr1](http://wiki.koumbit.net/rtr1-canix2.koumbit.net), 24h)
* Déménagement des serveurs dans notre propre cabinet (25h)
* Développement du timetracker (24h)
* Formation / training (19h)
* Projet Filmforge (17h)
* Développement de la [Wikifarm](http://wikifarm.koumbit.net/) pour maintenir le wiki actuel et créer des nouveaux wikis (16h)
* Début du travail sur [Puppet](http://wiki.koumbit.net/Puppet) pour gérer les serveurs (15h)

D'un côté, ce système est fort intéressant car il permet de voir sur quoi les gens passent véritablement leurs temps, et le coût réel de certaines tâches, qu'on ne suspecte pas (par exemple, la wikifarm prend beaucoup plus de temps que je l'imaginais, comparée, par exemple, à AlternC).

D'un autre côté, ces rapports passent sous silence les petites "gemmes", les tout petits trucs qui rendent la vie plus facile et qui fonctionnent bien et rapidement. Sans trop réfléchir, voici une liste rapide:

* Implantation de la copie et de l'extraction dans AlternC (3h)
* Installation et configuration correcte du PDU dans le cabinet (3h)
* Configuration de http://offline.koumbit.net/ (1h)
* Configuration de http://log.koumbit.net/ (8h)

J'ai pu aussi remarquer les factoïdes suivants fort intéressants:

* [chronos](http://wiki.koumbit.net/chronos.koumbit.net) un nouveau serveur fort similaire à [marius](http://wiki.koumbit.net/marius.koumbit.net), a pris la moitié moins de temps à configurer (5h vs 10h), même s'il a fallu développer du code Puppet pour le gérer. C'est grâce à la documentation du serveur (principalement la page [XenSetup](http://wiki.koumbit.net/XenSetup)) que ce genre de miracles se produisent
* les spammers ont coûté 11h de travail à Koumbit l'année dernière, ce qui est quand même considérable, mais moins que je m'attendais
* j'ai trouvé le tag "dream", mais c'était dans la phrase "try to deal with my tickets (dream on)"

Il y a probablement d'autres trucs à glaner de cet outil et je serais curieux de voir ce que les autres travailleurs en déduisent...

[[!tag "nouvelles" "koumbit" "geek"]]