[[!comment format=mdwn
 username="anarcat"
 subject="""key location"""
 date="2017-04-02T13:28:32Z"
 content="""
Maybe this should be made a little more explicit in the spec... We do touch on this later when we mention:

> When using this configuration, the system administrator SHOULD
> ensure that all other repositories on the system have an explicit
> Signed-By option, so that the derivative's key is not capable of
> impersonating other archives.

As I mentioned in the blog post here, posting new keys to `/etc` is a
security issue: it allows that key to impersonate the regular Debian
archives, something which we definitely want to avoid! Only Debian.org-managed keys should end up there.

You are correct in pointing out those keys should be managed by a package. As the spec mentions:

> Keys updates SHOULD be distributed by a Debian package called
> deriv-archive-keyring.

So downloading them in that location is merely to bootstrap the process of downloading that package, and we explicitly recommend instructions include downloading that package as part of the setup process.
"""]]
