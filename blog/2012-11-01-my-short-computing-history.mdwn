<!-- Drupal node 185 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="My short computing history"]]
[[!meta date="2012-11-01T02:16:40-0500"]]
[[!meta updated="2012-12-14T21:44:56-0500"]]
[[!meta guid="185 at http://anarcat.koumbit.org"]]

[[!toc]]

This is a long story. I wanted to document which tools I used on my desktop, but then realized it was difficult to do this without an historical context. This therefore, became a history of my computer usage and programming habits. I also struggle to document my current setup in [[software/desktop]].

Before the desktop: vic 20 and apple
====================================

My first contacts with computers were when my uncle showed me his Apple computers at his home. He was working at UQAM and had access to all sorts of crazy computers. My memory is vague, but I believe he showed me a turtle moving around the screen, which I suspect was Logo, and some rabbit game which I was quite fond of.

Later, I hooked up with a [Vic-20](https://en.wikipedia.org/wiki/Commodore_VIC-20) (similar to a Commodore 64) and typed in my first programs. Back then, you would painstakingly transcribe the program from a book into the screen, type `RUN` at the end and really hope that you didn't make a typo. You could also read and write data off audio tape with an external tape drive, which was really amazing.

To put things into perspective, the Vic-20 would plug into your television for a display, and the computer was "in the keyboard". Old school shit. The language was `BASIC`, which was awful.

Later, we got luxurious and had the privilege of a Macintosh Plus computer. 3000$ plus the printer, that was quite something. Still, that modern system could run Word, had games, and a 9" black and white screen. But the best part, and where I really started to learn to program, is with [Hypercard](https://en.wikipedia.org/wiki/HyperCard), the crazy software written by Bill Atkinson that became the inspiration for wikis, the Myst computer game and, to a certain extent, popularized the concept of hyperlinking, which is the basic principle of the World Wide Web.

The early years: afterstep and FreeBSD
======================================

But my real computer life all started with FreeBSD 2.2.x (probably [2.2.5](http://www.freebsd.org/releases/2.2.5R/announce.html)), around 1997, when I installed my first FLOSS operating system. This was just amazing: source code for everything, down to the compilers themselves, I was amazed, since I was coming from a Mac (still had that Mac Plus lying around) and Windows (95!) world.

Back then, X11 ([XFree86](https://en.wikipedia.org/wiki/XFree86), it was called) was a fantasy. Something I would compile with the FreeBSD ports system and sometimes would be able to run. Window managers were some weird things I wasn't sure how to use, but I figured out twm (yuck!) but was still amazed.

At the university, we had SGI workstations (remember the [Indigo](https://en.wikipedia.org/wiki/SGI_Indigo%C2%B2_and_Challenge_M) machines?) on which I started setting up the [Afterstep](http://www.afterstep.org/) window manager, which I used for a long time.

The web browser was [Netscape Communicator 4](https://en.wikipedia.org/wiki/Netscape_Communicator), and lo and behold, it also did email, although I mostly used [Pine](https://en.wikipedia.org/wiki/Pine_(e-mail_client)) over a modem terminal to the university.

Internet access at home was spotty and was dependent on students compiling discreetly copies of [slirp](http://slirp.sf.net/) which were allowing us, in a really clunky way, to run a PPP session over those terminal lines. That actually got my account suspended because I messed up my [sendmail](http://sendmail.org/) configuration and was sending emails that appeared to be coming as `<root@umontreal.ca>`, which freaked out the sysadmins that be over there.

The hardware was a Pentium 166Mhz with 32MB of ram and 20GB of disk space, which was a huge improvement over the 1MB of ram and 20MB of (external!) hard disk that the Mac Plus was proud of. By then, I started programming in more languages: I learned the basics of shell scripting, C programming and I had learned Pascal in a college class.

The real freedom: Debian and... enlightenment?
==============================================

I then switched to [Debian](http://debian.org/), thanks to my friend scyrma, which showed me the power of [dselect](https://en.wikipedia.org/wiki/Dselect) which, for a FreeBSD user, was really impressive. Imagine that: you could just install software and all dependencies without compiling anything! ;) I don't exactly remember when that was, but it was probably [Woody](http://www.debian.org/releases/woody/). I probably still used afterstep, but at that time, changing window managers was like changing t-shirts and we would waste countless hours fiddling around with borders and background images and other silly things. I remember running [Enlightenment](http://enlightenment.org) and writing [[software]] for it, for example, but not much else.

Back then, email was still over pine, but a new web browser was rearing its ugly head, [Mozilla](https://en.wikipedia.org/wiki/Mozilla_Application_Suite), and was quickly trimmed down to [Phoenix](https://www.mozilla.org/en-US/firefox/releases/0.1.html), shortly lived as Mozilla Firebird, only to be finally renamed [Mozilla Firefox](https://en.wikipedia.org/wiki/Firefox), and was unleashed as a [stable release](https://www.mozilla.org/en-US/firefox/releases/1.0.html) to an unsuspecting Internet Explorer and Microsoft-dominated world. Throughout this chaos, I also used various Mozilla derivatives like [Galeon](https://en.wikipedia.org/wiki/Galeon) since all of this was pretty unstable and heavy for my poor computers.

This was probably around 2002 to 2005. By then, I had formal classes on programming languages, was forced to learn Java, discovered C++, and learned more C as the university assumed that you knew that language in the data structures and algorithm class, which some students really didn't find funny at all. I also learned some Prolog and Haskell back then. Through my experiences with free software, I discovered various eclectic programming languages like TCL, and kept an eye out for other languages like lisp (thanks to Emacs), Forth (thanks to the FreeBSD boot loader), HTML/CSS/Javascript (no thanks to the web). There are probably others I am forgetting here, voluntarily or not.

The years of the desktops: rox, sawfish and gnome
=================================================

Another stable increment on my desktop was the use of the [sawfish window manager](http://sawfish.tuxfamily.org/) and the [rox desktop](https://en.wikipedia.org/wiki/ROX_Desktop), mostly because of the awesome Rox file manager that had no equal in the rest of the free software world. Gnome was an atrocity and KDE even more so: bloated and unstable.

Eventually, however, KDE and Gnome not only matured, but simply took over the whole of the graphical environment psyche of the free world. People nowadays don't even know what a window manager is and assume that you now have a "desktop environment", which, for me, is a real desk with a bunch of papers, a radio transceiver and maybe a computer with a screen lying around.

So I did use KDE, and then Gnome. I switched from KDE to Gnome because I felt that Gnome left me more space to run my own stuff without starting crazy daemons to preload everything. At this point, Firefox was the new thing and basically stable enough for daily use. This lasted until around 2009, I guess.

On the programming side, I had picked up Perl because I had to learn it to work at Ericsson (don't ask) and learned PHP to pay my rent, which I consider one of the biggest professional mistakes I have made so far in that story. It is around that time that [Koumbit](http://koumbit.org/) was founded by me and other friends.

The tiles fall on the gnomes: awesome desktop and sysadmin programming
======================================================================

At some point, friends (mostly comar and millette) introduced me to [wmii](http://wmii.suckless.org/) as the best thing since sliced bread. I banged my head against it with great pains, but I really enjoyed the hands off approach to window management. Tiling window managers really got a hold on me there, and in fact we can say on the whole desktop, with recent Gnome 3 taking up some tiling concepts itself. I have since then switched to the [Awesome Window Manager](http://awesome.naquadah.org/) which is probably the worst name you can use for software (try googling for "Awesome crash" to get an idea).

I have switched from Firefox to Chromium, after having repeated crashes from Firefox on a site that Chrome would load fine. I was expecting Chrome to crash only one tab, because of its design, and it turned out to not crash at all, and I have since given up on Firefox totally. I have also given up on desktop environments. While I have given them a chance in quick setups and still use them when forced to, I find them too bloated and single-minded for my needs. I want control over my desktop, and those tools work very hard to remove control and hide it behind *user interfaces* that treat me like a brain-dead dog.

Finally, I have kept on programming in PHP for work, because that's what we do over there (unfortunately). Mostly because of [Drupal](http://drupal.org/) and [Aegir](http://aegirproject.org/), in which I was and still am deeply involved. But I am quite fond of Python for my personnal project and still use shell scripting to get myself out of trouble or automate data processing from time to time. I have a rule though: once a program hits 100 lines of shell scripting, it needs to be rewritten, usually in Perl because the syntax is closer.

During the years 2009 to 2012, I have started to work more and more on system administration however. In 2010, I [became a Debian Developer](http://lists.debian.org/debian-news/2010/msg00011.html) and a lot of my work at Koumbit revolves around system administration, optimisation and maintenance. I work oncall and sometimes get called during the night to nurse servers (which is why I am up writing this right now).

The future of programming
=========================

I have become seriously disgruntled with programming. I have seen the emergence of the web, where programmers felt it was relevant to rewrite 30 years of programs into the web framework (webmail? wtf?), and now we are seeing the same thing happen with mobile devices, the tracking devices they call phones and "pads", we should rather say. And again they are trying to make people pay for trivial software. I am tired of seeing our community reinvent the wheel every 10 years because of infatuated egos and marketing sprints. We are wasting great minds on this, a lot of energy and precious time. I would rather maintain 5 year old imperfect software than coordinate a 5 year project to rewrite that software into another imperfect software. That is what is happening right now, and it's going nowhere.

So here's the current state of affairs, some perls, some tiles, but mostly system administration and less programming. Through all this time, the common pattern is that I have learned to solve problems and document my work, which are key concepts of the job of a system administrator, which is what I really am now. Somehow, that popped into my life like a natural fit. Maybe, one day, I will retire and become a shepherd in far away mountains, but I will always have in my mind the strong desire to fix shit up and make things right with the world. Maybe get involved in a project like [Open Source Ecology](https://en.wikipedia.org/wiki/Open_Source_Ecology_(project)) and especially their [Global Village Construction Set](http://opensourceecology.org/wiki/Global_Village_Construction_Set) to make sure humankind survives all that stupidity.

[[!tag "debian" "debian-planet" "freebsd" "geek" "gloating" "histoire" "software" "vraie vie"]]
