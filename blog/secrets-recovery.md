I've been using PGP for a long time, before it was even called
OpenPGP. The first PGP key I can find dates from 2000, and I have had
multiple keys since then.

[paperkey](https://www.jabberwocky.com/software/paperkey/) extracts only the secret bits of an OpenPGP secret key
and therefore allows you to make a smaller backup of secret key
material.

Since QR codes can [only store about 3KB of data](https://stackoverflow.com/questions/11065415/how-much-data-information-can-we-save-store-in-a-qr-code), we need
something more than "just tweak the `qrencode` settings". Something
like [qr-backup](https://github.com/za3k/qr-backup).

include text: https://github.com/za3k/qr-backup/issues/48
remove imagemagick dep: https://github.com/za3k/qr-backup/issues/47

qr-backup just fails to print, because of missing margins:
https://github.com/za3k/qr-backup/issues/49

https://github.com/za3k/qr-backup/issues?q=involves%3Aanarcat+sort%3Aupdated-desc
monospace: https://github.com/za3k/qr-backup/pull/51

https://github.com/jmlemetayer/gpg2qr only does pgp, less interesting.

https://github.com/intra2net/paperbackup is interesting as well, as it
has a text copy of the backup, something which qr-backup doesn't do.

paperbackup crashes because python-qrencode is desperately out of
date:

https://github.com/Arachnid/pyqrencode/issues/13

but also fails to verify because of same bug as qrbackup:
https://github.com/intra2net/paperbackup/issues/17

it also doesn't include good instructions in the restore:

https://github.com/intra2net/paperbackup/issues/16

nmu'd python-qrencode

https://github.com/intra2net/paperbackup/pull/18

printed first batch with paperbackup, mostly because of #49

... but could not actually recover, so going back to qr-backup.

https://sites.google.com/view/chewkeanho/guides/gnupg/delete-primary-key-master-key#h.p_N3yQQH0lmXxd

[[!tag draft]]

17:53:28 <diederik> TIL you can specify a successor in GH account settings for when you die :-O
qr-backup ITP https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1021089


https://github.com/cyphar/paperback


to review: https://news.ycombinator.com/item?id=37534615
