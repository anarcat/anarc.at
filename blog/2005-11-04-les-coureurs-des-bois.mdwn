<!-- Drupal node 43 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Les coureurs des bois"]]
[[!meta date="2005-11-04T21:01:00-0500"]]
[[!meta updated="2011-12-22T01:25:53-0500"]]
[[!meta guid="43 at http://anarcat.koumbit.org"]]

Encore! Oui! Encore! C'est tellement amusant et libérant! Partie la ville sainte et le travail malsain! Nous sommes cette fois-ci allés courir dans les bois au Mont St-Hilaire. Non sans notre saine dose habituelle de chaos et de problèmes: crevaison sur le vélo (encore!), crevaison sur le communauto (???), pris le mauvais pont (St-Hilaire, c'est sur la 20, épais!), mais on a fini par se rendre. Il reste encore des feuilles, un tout petit peu... On a commencé à monter à 15h20, il fallait redescendre pur 16h30. On a fait Burned Hill et le Pain de Sucre en un temps record, juste à temps pour voir le soleil commencer à se coucher au sommet du Pain de Sucre. Quelle palette de couleur féérique! Les rayons du soleil arrivaient à peine à percer la solide couche de nuage pour aller flatter le sol et le Richelieu.

Je ne suis pas resté très longtemps au sommet. Il était 16h15 et nous devions sortir l'auto pour 16h30. Défi intéressant, j'accepte et fait les 2-3km à la course en un bon temps (18 minutes) pour arriver à l'auto à 16h34. Honnêtement, je crois qu'il m'aurait laissé sortir à 16h45, mais je voulais pas essayer. Le gardien a quand même fait sa petite crise quand mes compagnons m'on rejoint une demie-heure plus tard. M'enfin... C'est son problème.

C'est quand même bête de faire payer les gens pour monter des montagnes, surtout que ça "appartient" à McGill, apparament, cette montagne. Révoltant même. On trouvera un autre chemin, c'est tout.

[[!tag "vraie vie" "plein air"]]