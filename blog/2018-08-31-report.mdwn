[[!meta title="August 2018 report: LTS, Debian, Upgrades"]]

[[!toc levels=2]]

Debian Long Term Support (LTS)
==============================

This is my monthly [Debian LTS][] report. 

## twitter-bootstrap

I [researched](https://lists.debian.org/87lg8rbx1j.fsf@curie.anarc.at) some of the security issue of the [Twitter Bootstrap
framework](https://getbootstrap.com/) which is clearly showing its age in Debian. Of the three
vulnerabilities, I couldn't reproduce two ([[!debcve CVE-2018-14041]]
and [[!debcve CVE-2018-14042]]) so I marked them as "not affecting"
jessie. I also found that [[!debcve CVE-2018-14040]] was relevant only
for Bootstrap 3 (because yes, we still have Bootstrap 2, in *all*
suites, which will [hopefully be fixed in buster](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=907724))

The patch for the latter was a little tricky to figure out, but ended
up being simple. I [tested the patch](https://jsbin.com/zaqojakira/1/edit?html,output) with a private copy of the
code which works here and published the result as [DLA-1479-1](https://lists.debian.org/20180827195611.GA3051@curie.anarc.at).

What's concerning with this set of vulnerabilities is they show a
broader problem than the one identified in those specific
instances. May found at least one similar other issue although I
wasn't able to exploit it in a quick attempt. Besides, I'm not sure we
want to audit the entire Bootstrap codebase: upstream fixed this issue
more widely in the v4 series, and Debian should follow suite, at least
in future releases, and remove older releases from the archive.

## tiff

A classic. I tried and failed to reproduce [[!debcve CVE-2018-15209]]
in the [[!debpkg tiff]] package. I'm a bit worried by Brian May's
results that the proof of concept eats up all memory in his
tests. Since I could not reproduce, I [marked the package as N/A in
jessie](https://lists.debian.org/87y3crc049.fsf@curie.anarc.at) and moved on.

## Ruby 2.1

Another classic source of vulnerabilities... The patches were easy to
backport, tests passed, so I just uploaded and published
[DLA-1480-1](https://lists.debian.org/20180827202550.GA20085@curie.anarc.at).

## GDM 3

I [reviewed](https://lists.debian.org/87efej5p5x.fsf@curie.anarc.at) Markus Koschany's work on [[!debcve CVE-2018-14424]]. His
patches seemed to work in my tests as I couldn't see any segfault in
jessie, either in the kernel messages or through a debugger.

True, the screen still "flashes" so one might *think* there is still a
crash, but this is actually expected behavior. Indeed, this is the
first D-Bus command being ran:

    dbus-send --system --dest=org.gnome.DisplayManager --type=method_call --print-reply=literal /org/gnome/DisplayManager/LocalDisplayFactory org.gnome.DisplayManager.LocalDisplayFactory.CreateTransientDisplay

Or, in short, `CreateTransientDisplay`, which is also known as [fast
user switching](https://en.wikipedia.org/wiki/Fast_user_switching), brings you back to the login screen. If you enter
the same username and password, you get your session back. So no
crash. After talking with Koschany, we'll wait a little longer for
feedback from the reporter but otherwise I expect to publish the fixed
package shortly.

## git-annex

This is a bigger one I took from Koschany. The patch was large, and in
a rather uncommon language (Haskell). 

The [first patch](http://source.git-annex.branchable.com/?p=source.git;a=commitdiff;h=28720c795ff57a55b48e56d15f9b6bcb977f48d9) was tricky as function names had changed and some
functionality (the P2P layer, the `setkey` command and content
verification) were completely missing. On advice from upstream, 
the [content verification](http://source.git-annex.branchable.com/?p=source.git;a=commitdiff;h=2fb3722ce993f01adb3ea9d5f8855c3e6a99c249) functionality was backported as it was
critical for the [second tricky patch](http://source.git-annex.branchable.com/?p=source.git;a=commitdiff;h=b657242f5d946efae4cc77e8aef95dd2a306cd6b) which required more Haskell
gymnastics.

This time again, Haskell was nice to work with: by changing type
configurations and APIs, the compiler makes sure that everything works
out and there are no inconsistencies. This logic is somewhat backwards
to what we are used to: normally, in security updates, we avoid
breaking APIs at all costs. But in Haskell, it's a fundamental way to
make sure the system is still coherent.

More details, including embarrassing fixes to the version numbering
scheme, are best explained in the [email thread](https://lists.debian.org/87wosaw0uc.fsf@curie.anarc.at). An update for
this will come out shortly, after giving more time for upstream to
review the final patchset.

## Fighting phishing

After mistyping the address of the [security tracker](https://security-tracker.debian.org/tracker/), I ended up
on this weird page:

<figure><img
src="https://paste.anarc.at/snaps/snap-2018.08.31-10.39.17.png" alt="Screenshot of a web page loaded from iiisurvey.com announcing I won the chance to answer to a survey for Teksavvy"/>
<figcaption>Some phishing site masquerading as a Teksavvy customer survey.</figcaption>
</figure>

Confused and alarmed, I thought I was being intercepted by my ISP, but
after looking on their forums, I found out they actually get phished
like this all the time. As it turns out, the domain name
[debain.org](https://whois.icann.org/en/lookup?name=debain.org) (notice the typo) is actually registered to some
scammers. So I implemented a series of [browser quick searches](https://salsa.debian.org/debian/debian-bookmarks-shortcuts) as
a security measure and shared those with the community. Only after
feedback from a friend did I realize that [surfraw](https://tracker.debian.org/surfraw) (SR) has been
doing this all along. The problem with SR is that it's mostly
implemented with messy shell scripts and those cannot easily be
translated back into browser shortcuts, which are still useful on
their own. That and the SR plugins (called "elvi" or "elvis" in
plural) are horribly outdated.

Ideally, trivial "elvis" would simply be "bookmarks" (which are really
just one link per line) that can then easily be translated back into
browser bookmarks. But that would require converting a bunch of those
plugins, something I don't currently have the energy (or time)
for. All this reminds me a lot of the [interwiki links](https://en.wikipedia.org/wiki/Interwiki_links) from the
wiki world and looks like an awful duplication of information. Even in
*this* wiki I have similar shortcuts, which are yet another database
of such redirections. Surely there is a better way than maintaining
all of this separately?

## Who claimed all the packages?

After struggling again to find some (easy, I admit) work, I worked on
[a patch](https://salsa.debian.org/security-tracker-team/security-tracker/commit/74a32e5a2b) to show per-user package claims. Now, if `--verbose` is
specified, the `review-update-needed` script will also show a list of
users who claimed packages and how many are claimed. This can help us
figure out who's overloaded and might need some help.

## Post-build notifications in sbuild 

I sent a [patch](https://salsa.debian.org/debian/sbuild/merge_requests/2) to sbuild to make sure we can hook into failed
builds on completion as well as successful builds. Upstream argued
this is best accomplished with a wrapper, but I believe it's
unsufficient as a wrapper will not have knowledge of the sbuild
internals and won't be able to effectively send notifications. It is,
after all, while there is a `post-build` hook right now, which runs
only on succesful builds.

## GnuTLS and other reviews

I reviewed [questions from Ola Lundqvist](id:CABY6=0nu1qG9Beb5qc-mbZfubmQGxp9dbgnicFuPPpiwz+oJnw@mail.gmail.com) regarding the pending
GnuTLS security vulnerabilities designated [[!debcve CVE-2018-10844]],
[[!debcve CVE-2018-10845]] and [[!debcve CVE-2018-10846]]. Those came
from a [paper](https://eprint.iacr.org/2018/747.pdf) called [Pseudo Constant Time Implementations of TLS
Are Only Pseudo Secure](https://eprint.iacr.org/2018/747). I am still unsure of the results: after
reviewing the paper in detail, I am worried the upstream fixes are
complete. Hopefully Lundqvist will figure it out but in any case I am
available to review this work again next week.

I also provided advice on a [squirrelmail bugfix backport
suggestion](https://lists.debian.org/87lg8my1zd.fsf@curie.anarc.at).

[Debian LTS]: https://www.freexian.com/services/debian-lts.html

Other free software work
========================

I was actually on vacation this month so this is a surprising amount
of activity for what was basically a week of work.

Buster upgrade
--------------

I upgraded my main workstation to buster, in order to install various
[Node.JS](https://tracker.debian.org/pkg/nodejs) programs through [npm](https://tracker.debian.org/pkg/npm) for that [Dat article](https://lwn.net/Articles/763492/)
(which will be public here shortly). It's part of my routine: when
enough backports pile up or I need too much stuff from unstable, it's
time to make the switch. This makes development on Debian easier and
helps testing the next version of stable before it is released. I do
this only on my most busy machine where I can fix things quickly and
they break: my laptop and server remain on stable so I don't have to
worry about them too much.

It was a bumpy ride: font rendering changed because of the new
rendering engine in [FreeType](https://www.freetype.org/). Someone ended up finding a
workaround in [[!debbug 866685]] which allowed me to keep the older
rendering engine but I am worried it might be removed in the
future. Hopefully that bug will trickle upstream and Debian users
won't see a regression when they upgrade to buster.

A major issue was a [tiny bug](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=907370) in the [[!debpkg python-sh]] library
which caused my entire [LWN workflow](https://gitlab.com/anarcat/lwn) to collapse. Thankfully, it
turned out upstream had already released a fix and all I had to do was
to update the package and NMU the result. As it turns out, I was
already part of the Python team, and that should have been marked as a
team upload, but I didn't know. Strange how memory works sometimes.

Other problems were similar: [[!debpkg dictd]], for example, failed to
upgrade ([[!debbug 906420]], fixed). There are about 15 different
packages that are missing from stretch: many FTBFS problems, other
with real critical bugs. Others are just merely missing from the
archive: I particularly pushed on [[!debpkg wireguard]] ([[!debbug
849308]]), [[!debpkg taffybar]] ([[!debbug 895264]]), and [hub](https://hub.github.com/)
([[!debbug 807866]]).

I won't duplicate the whole upgrade documentation here, the details
are in [[services/upgrades/buster]].

Debian 25th anniversary updates
-------------------------------

The [Debian project](https://debian.org/) turned 25 this year so it was a good occasion
to look back at history and present. I [became](https://nm.debian.org/legacy/process/anarcat) a Debian Developer
in 2010, a Debian maintainer in 2009, and my [first contributions](https://contributors.debian.org/contributor/anarcat@debian/)
to the project go all the way back to 2003, when I started filing
bugs. So this is anywhere between my 8th and 15th birthday in the
project.

I didn't celebrate this in any special way, although I did make sure
to keep my packages up to date when I returned from vacation. That
meant a few uploads:

 * [[!debpkg main]]: added myself as a maintainer and uploaded new
   upstream as [5.5.2-1](https://tracker.debian.org/news/983112/accepted-maim-552-1-source-into-unstable/)
 * [[!debpkg slop]]: uploaded new upstream as [7.4-1](https://tracker.debian.org/news/983111/accepted-slop-74-1-source-into-unstable/)
 * [[!debpkg percol]]: uploaded  [0.2.1-2](https://tracker.debian.org/news/981649/accepted-percol-021-2-source-into-unstable/)
 * [[!debpkg f3]]: uploaded new upstream as [7.1-1](https://tracker.debian.org/news/981654/accepted-f3-71-1-source-into-unstable/)
 * [[!debpkg atheme]]: uploaded [7.2.9-3](https://tracker.debian.org/news/975916/accepted-atheme-services-729-3-source-amd64-into-unstable/)
 * [[!debpkg etckeeper]]: uploaded new upstream as [1.18.8-1](https://tracker.debian.org/news/981618/accepted-etckeeper-1188-1-source-into-unstable/)
 * [[!debpkg smopeping]]: uploaded new upstream to unstable as [2.7.2-2](https://tracker.debian.org/news/982834/accepted-smokeping-272-2-source-into-unstable/)
 * [[!debpkg charybdis]]: security upload of new upstream as [4.1.1-1](https://tracker.debian.org/news/981657/accepted-charybdis-411-1-source-into-unstable/)
 * [[!debpkg rapid-photo-downloader]]: pushed new updates to the git
   repository, but held off on a new NMU to give a chance (another 30
   days!) for the maintainer to respond to the first one

Work on smokeping and charybdis happened as part of our now regular
[Debian & Stuff](https://veronneau.org/montreals-debian-stuff-august-2018.html) along with [LeLutin](https://lelutin.ca/) which is helping and
learning a few packaging tricks along the way.

Other software upgrades
-----------------------

During the above buster upgrade, [Prometheus](https://prometheus.io/) broke because the
[node exporter](https://github.com/prometheus/node_exporter) metrics labels changed. More accurately, what
happened is that [Grafana](https://grafana.com/) would fail to display some of the
nodes. As it turns out, all that was needed was to update a few
Grafana dashboard (as those don't update automatically of course). But
it brought to my attention that a bunch of packages I had installed
were *not* being upgraded as automatically as the rest of my Debian
infrastructure. There were a few reasons for that:

 1. packages were installed from a third-party repository
 
 2. packages were installed from unstable

 3. there *were* no packages: software was installed in a container
 
 4. there *were* no packages: software was installed by hand (!)

I'm not sure what is the worst between 3 and 4. As it turns out,
containers were harder to deal with because they also involved
upgrading [[!debpkg docker.io]] which was more difficult.

For each forgotten program, I tried to make sure they wouldn't stay
stale any longer in the case of 1 or 2, a proper apt preference (or
"pin") was added to automate upgrades. For 3 and 4, I added the
release feeds of the program to [feed2exec](https://feed2exec.readthedocs.io/en/stable/) so I get an email when
upstream makes a new release.

Those are the programs I had to deal with:

   * [rainloop](https://www.rainloop.net/): the [upgrade guide](https://www.rainloop.net/docs/upgrade/) is trivial. make a backup:
   
        (umask 0077; tar cfz rainloop.tgz /var/lib/rainloop)
     
     Then decompress archive on top. It keeps old data in
     `rainloop/v/1.11.0.203/` which should probably be removed on next
     upgrade. Upgrade presumably runs when visiting the [site](https://mail.anarc.at)
     which worked flawlessly after upgrade.

   * [grafana](https://grafana.com/): the [upgrade guide](http://docs.grafana.org/installation/upgrading/) says to backup the database in
     `/var/lib/grafana/grafana.db` but i backed up the whole thing:
     
        (umask 0077; tar cfz grafana /var/lib/grafana
    
      The upgrade from 4.x to 5.2.x was trivial and automated.  There
      is, unfortunately, still [no official package](https://bugs.debian.org/835210). A visit to
      the [Grafana instance](https://grafana.anarc.at/) shows some style changes and
      improvements and that things generally just work.

   * the [[!debpkg toot]] Mastodon client has entered Debian so I was
     able to remove yet another [third party repository](https://toot.readthedocs.io/en/latest/install.html#apt-package-repository). this
     involved adding a pin to follow the buster sources for this
     package:
     
        Package: toot
        Pin: release n=buster
        Pin-Priority: 500

   * Docker is in this [miserable state](https://tracker.debian.org/docker.io) in stretch. There is an
     really old binary in jessie-backports (1.6) and for some reason I
     had a random version from unstable running [1.13.1~ds1-2](https://tracker.debian.org/news/847029/accepted-dockerio-1131ds1-2-source-all-amd64-into-unstable/). I
     upgraded to the sid version, which installs fine in stretch
     because golang is statically compiled. But, the containers did
     not restart automatically. starting them by hand gave this error:
          
        root@marcos:/etc/apt/sources.list.d# ~anarcat/bin/subsonic-start
        e4216f435be477dacd129ed8c2b23b2b317e9ef9a61906f3ba0e33265c97608e
        docker: Error response from daemon: OCI runtime create failed: json: cannot unmarshal object into Go value of type []string: unknown.

     Strangely, the container *was* started, but is not reachable over
     the network. The problem is that `runc` needs to be upgraded as
     well, so that was promptly fixed.
     
     The magic pin to follow buster is like this:
     
        Package: docker.io runc
        Pin: release n=buster
        Pin-Priority: 500

   * [airsonic](https://airsonic.github.io/) upgrades are a little trickier because I run this
     inside a docker [[container|software/containers]]. First step is
     to fix the [Dockerfile](https://github.com/anarcat/subsonic-docker-image/tree/airsonic) and rebuild the container image:
   
        sed -i s/10.1.1/10.1.2/ Dockerfile
        sudo docker build -t anarcat/airsonic .
    
     then the image is ready to go. the previous container needs to be
     stopped and the new one started:
     
        docker ps
        docker stop 78385cb29cd5
        ~anarcat/bin/subsonic-start
    
     The latter is a script I wrote because I couldn't remember the
     magic startup sequence, which is silly: you'd think the
     Dockerfile would know stuff like that. A visit to the [radio
     site](https://radio.anarc.at/) showed that everything seemed to be in order but no
     deeper test was performed.

All of this assumes that updates to unstable will not be too
disruptive or that, if they do, the `NEWS.Debian` file will warn me so
I can take action. That is probably a little naive of me, but it beats
having outdated infrastructure running exposed on the network.

Other work
----------

Then there's the usual:

 * Yet another bugfix on [[!debpkg battery-stats]]: [handle null
   values in logfiles](https://github.com/petterreinholdtsen/battery-stats/pull/28)

 * [pywb](https://github.com/webrecorder/pywb/): [add sample Apache configuration](https://github.com/webrecorder/pywb/pull/374) - this might be the
   topic of an upcoming LWN article, but for now, see the
   documentation in [[services/archive]]

 * Usual PR review and triage in linkchecker

 * Got tired of [notmuch](https://notmuchmail.org/) hanging Emacs when refreshing OpenPGP
   keys so I [asked for notmuch to fetch keys asynchronously](https://notmuchmail.org/pipermail/notmuch/2018/026897.html) and
   amazingly, a [nice patchset](https://notmuchmail.org/pipermail/notmuch/2018/026927.html) came out of that discussion and I
   am now happier.
 
 * I added back breadcrumbs to this site's theme. It turns out it was
   essential in certain sections of the sites. It's two rather
   [small](https://gitlab.com/anarcat/ikiwiki-bootstrap-anarcat/commit/ba548926da4093e1df81d0053797d3ae0a7d6a2a) [commits](https://gitlab.com/anarcat/ikiwiki-bootstrap-anarcat/commit/3a67fe0dd3603561e3264df97a6c26c81e9dc185) and I'm not sure I like the result, but it
   beats having no navigation at all to go back up one level.

 * Add a [Python script to sort passwords by domain names in
   pass](https://gitlab.com/anarcat/scripts/blob/6dcbf71d8a4d51c314408582eaa8014c1fd2b42b/pass-domains). I did try to make that a "pass extension" but those
   stupidly require to be written in bash. There was no way I would
   write this in shell and I could figure out an easy way to inject
   the Python program in a shell script. I wish there was a better way
   to do this, maybe if I will redo this in another language if I
   switch to a pass clone of some sort.

 * I accepted an [esperanto translation](https://gitlab.com/anarcat/gameclock/merge_requests/1) in [gameclock](https://gitlab.com/anarcat/gameclock). No idea
   if it makes sense at all and it was strange to see a contribution
   to this mostly idle project.

 * Worked on fixing the pending feed2exec test failures by [adding
   support for 3.7](https://gitlab.com/anarcat/feed2exec/commit/d9d10c9b059c9eedc7badbfd4b19d1fe6c900079) and reviewing a [merge request to fix tests
   with the latest html2text](https://gitlab.com/anarcat/feed2exec/merge_requests/3)

 * Start using the [ptpb.pw pastebin](https://ptpb.pw/) and its (trivial) shell
   client [pb_cli](https://github.com/ptpb/pb_cli). I reviewed a bunch of issues and sent a [pull
   request](https://github.com/ptpb/pb_cli/pull/9) to "port to /bin/sh" and another to [add basic usage
   instructions](https://github.com/ptpb/pb_cli/pull/8) which was unfortunately refused. I like this
   little program - it could replace my screenshot tools and [[!debpkg
   pastebinit]] all at once. Too bad it does not do [client-side
   encryption](https://github.com/ptpb/pb/issues/230), otherwise it's a pretty neat project.

 * Started a redesign of [[!debpkg parcimonie]] in [[!debbug
   836266]]. Can no one can tell me what `gpg --import` and if it's
   safe on arbitrary datasets already?

[[!tag debian debian-lts monthly-report debian-planet python-planet]]
