[[!meta title="20 most significant programming languages in history"]]

This is a preposterous table I just made up after reading [Wikipedia's
History of Programming Languages](https://en.wikipedia.org/wiki/History_of_programming_languages). I was trying to figure out which
programming language or environment [this article](https://www.lapresse.ca/actualites/politique/202002/01/01-5259180-les-systemes-informatiques-federaux-menacent-de-seffondrer.php) might be
refering to. The article talks about some of the Canadian federal
government's computer systems "falling apart" because they are "60
years" old. Everyone [cried "COBOL"][] but I figured there might be
other culprits.

Update: see also the [CBC (english) version](https://www.cbc.ca/news/politics/federal-it-systems-critical-failure-1.5448871) for the article, and a
[Hackernews thread](https://news.ycombinator.com/item?id=22227252) which seems to [confirm the COBOL
hypothesis](https://news.ycombinator.com/item?id=22253697).

[cried "COBOL"]: https://en.wikipedia.org/wiki/Cassandra_(metaphor)

| Year | Language        | Founder                      | Claim to fame                                                                                    |
| ---- | --------------- | ---------------------------- | ------------------------------------------------------------------------------------------------ |
| 1954 | [Fortran][]     | [IBM][]                      | first high-level language with functional implementation                                         |
| 1958 | [LISP][]        | [MIT][]                      | first homoiconic language                                                                        |
| 1959 | [COBOL][]       | [US DoD][]                   | still in use                                                                                     |
| 1964 | [BASIC][]       | [Darmouth College][]         | saw explosive growth with PCs in 1970s                                                           |
| 1972 | [C][]           | [AT&T][]                     | early systems language, foundation of UNIX                                                       |
| 1972 | [Prolog][]      | [Alain Colmerauer][]         | first (and only?) "logic programming" language                                                   |
| 1978 | [SQL][]         | [Oracle][]?                  | first commercial language to use the [relational model][], still in use in most database systems |
| 1980 | [C++][]         | [AT&T Bell Labs][]           | major systems programming language                                                               |
| 1986 | [Objective C][] | [Apple Inc.][]               | main Apple language until the introduction of [Swift][]                                          |
| 1986 | [Erlang][]      | [Ericsson][]                 | originally written in Prolog, highly-available, hot-swapping, distributed language               |
| 1987 | [Perl][]        | [Larry Wall][]               | every sysadmin can write-only it                                                                 |
| 1990 | [Haskell][]     | [University of Glasgow][]    | first [type classes][] implementation                                                            |
| 1991 | [Python][]      | [Guido van Rossum][]         | ease of use and readability, built [Dropbox][]                                                   |
| 1995 | [Ruby][]        | [Yukihiro Matsumoto][]       | built [GitHub][]                                                                                 |
| 1995 | [Javascript][]  | [Netscape][]                 | you're running it right now, [most popular language on stackoverflow][]                          |
| 1995 | [Java][]        | [Sun][]                      | "write once, run everywhere", consistently the [most popular language on the TIOBE index][]      |
| 1995 | [PHP][]         | [Rasmus Lerdorf][]           | personal project, built [Facebook][], which eventually replaced it with [Hack][] (2014)          |
| 2001 | [C#][]          | [Microsoft][]                | multi-paradigm                                                                                   |
| 2009 | [Go][]          | [Google][]                   | readable, concurrent, high-performance                                                           |
| 2010 | [Rust][]        | [Mozilla][]                  | memory-safe, concurrent, high-performance                                                        |

[Hack]: https://en.wikipedia.org/wiki/Hack_(programming_language)
Some interesting observations:

 * all of those languages are still in use
 * in particular, COBOL is indeed 60 years old and still in use by
   governmental agencies, according to a US congress report
 * I am also aware that Fortran is still in use in institutions,
   particularly research, and particularly [Environnement Canada][]
 * a significant number of programming languages came from research
   (Lisp, Prolog, Haskell, Python, Ruby), but that has basically
   disappeared in the last two decades
 * the list is skewed towards languages I learned as I reached adult
   life
 * yet I can't help but think some years were especially fertile (like
   1995) and that things seem to be slowing down - after all, all the
   languages before the new ones still exist as [legacy code][] that
   needs to be rewritten
 * in this list, PHP is the only language that was not designed by an
   author working under a large corporation or university - it was,
   after all, designed for Personal Home Pages...

[legacy code]: https://en.wikipedia.org/wiki/Legacy_code

But for me, the most significant thing I find in this list is that
every corporate ruler eventually creates its own programming
language. IBM made Fortran. The US government made COBOL. AT&T made
C. Ericsson made Erlang. Google made Golang. Facebook made Hack. And
it's interesting to note that some languages came up shortly before
the business crashed (e.g. Ericsson, Netscape, Sun) or a dark period
(Apple post-Jobs, Google post don't-be-evil, Microsoft anti-trust
era). Maybe this means Mozilla is about to crash?

[Environnement Canada]: https://en.wikipedia.org/wiki/Environment_and_Climate_Change_Canada

Notable omissions and regrets
=============================

I originally jotted this down as a quick list of 18 languages I found
while reviewing the Wikipedia page. Then I couldn't help myself and
added Prolog, rounding up to 20 languages.

Then I realized I had forgotten Java, one of the most popular
programming languages and the foundation of many corporations. So I
was stuck and *had* to remove some things. Besides, there's only so
much stuff that can fit in here. So here's the list of langugages that
did not make it.

| Year  | Language      | Founder                               | Claim to fame                                           | Excluded                         |
| ----  | --------      | -------                               | -------------                                           | --------                         |
| 1940? | [Assembly][]  | [Alan Turing][]                       | first concept of a [stored program][]                   | not high level                   |
| 1970  | [Pascal][]    | [Niklaus Wirth][]                     | first major decent language with complex datatypes      | mostly dead                      |
| 1971  | [Shell][]     | [Ken Thompson][] / [AT&T Bell Labs][] | interactive programming                                 | not a real programming language  |
| 1983  | [Ada][]       | [US DoD][]                            | design-by-contract, used in safety systems              | own ignorance                    |
| 1987  | [Hypertalk][] | Dan Winkler / [Bill Atkinson][]       | english-like                                            | mostly disappeared               |
| 1996  | [OCaml][]     | [INRIA][]                             | the other significant functional language aside Haskell | too similar to Haskell in spirit |
| 2002  | [Scratch][]   | [MIT Media Lab][]                     | block-based visual language, used for teaching kids     | not very well known              |
| 2014  | [Swift][]     | [Apple Inc.][]                        | safer version of Objective C                            | too Apple-specific               |
| 2014  | [Hack][]      | [Facebook][]                          | [gradual typing][] for PHP                              | too Facebook-specific            |

[Ada]: https://en.wikipedia.org/wiki/Ada_(programming_language)
[gradual typing]: https://en.wikipedia.org/wiki/Gradual_typing

I also excluded things like <del>Ada,</del> Algol, APL, and other relics that are
historically significant but largely irrelevant now as they are not in
use anymore. I was surprised to see that Pascal was the most popular
programming language for a few years (1980-1984) until it was
surpassed by C, according to [this vizualisation](https://www.youtube.com/watch?v=Og847HVwRSI). (That video also
gives Ada the top row for 1985-1986, which I found surprising...) 

Scala, Groovy, Typescript, and other minorities are excluded because I
am not familiar with them at all.

See also [[my short computing history|blog/2012-11-01-my-short-computing-history]].

Update: I added Ada to the table above after being told it's
[still widely used][ada-widely-used] in [aerospace, avionics, traffic control and all
sorts of safety-critical systems][ada-uses]. It's also standardized and still
developed, with the latest stable release from 2012. Ada is also one
of the languages still [supported by GCC][], along with C, C++,
Objective-C, Objective-C++, Fortran, and Golang, at the time of
writing.

[ada-widely-used]: https://medium.com/@jannis.kirschner/the-most-secure-programming-language-youve-probably-never-even-heard-of-4ed9f7d73ade
[ada-uses]: https://www2.seas.gwu.edu/~mfeldman/ada-project-summary.html
[supported by GCC]: https://en.wikipedia.org/wiki/GNU_Compiler_Collection#Languages

Second update: I kind of cheated here. As the "preposterous" adjective
intended to indicate, this list is totally biased and reflects how I
see those languages and programming history, more than factual, hard
data. For example, [Python][] was founded by Guido while working at
the [CWI](https://en.wikipedia.org/wiki/Centrum_Wiskunde_%26_Informatica) in the Netherlands. Is CWI the founder, or is Guido? In
my mind Guido is the founding figure. Another, more important example:
[Oracle][] didn't create [SQL][]. Obviously, like a lot of computer
things at the time, [IBM][] initially developed SQL in 1974. But the
"the first commercially available implementation of SQL" is [Oracle
v2][] in 1979, back when Oracle was called "Relational Software,
Inc". And yes, IBM built [DB2][], which I didn't know about, but that
actually came later, in 1983.

But sure, we could assign SQL foundation status to IBM here, but do
you really think of IBM when you think of SQL? Or do you think of
Oracle, [MySQL][] (now also Oracle!) or [PostgreSQL][]? I sure know
where I stand...

Third update: Ars Technica did a [piece on how Oracle is full of it
for suing Google in the Java case, because it copied IBM over SQL](https://arstechnica.com/tech-policy/2020/03/before-it-sued-google-for-copying-from-java-oracle-got-rich-copying-ibms-sql/).

[PostgreSQL]: https://en.wikipedia.org/wiki/PostgreSQL
[MySQL]: https://en.wikipedia.org/wiki/MySQL

[DB2]: https://en.wikipedia.org/wiki/IBM_Db2_Family
[Oracle v2]: https://en.wikipedia.org/wiki/Oracle_Database

[AT&T Bell Labs]: https://en.wikipedia.org/wiki/Bell_Labs
[AT&T]: https://en.wikipedia.org/wiki/AT&T
[Alain Colmerauer]: https://en.wikipedia.org/wiki/Alain_Colmerauer
[Alan Turing]: https://en.wikipedia.org/wiki/Alan_Turing
[Apple Inc.]: https://en.wikipedia.org/wiki/Apple_Inc.
[Assembly]: https://en.wikipedia.org/wiki/Assembly_language
[BASIC]: https://en.wikipedia.org/wiki/BASIC
[Bill Atkinson]: https://en.wikipedia.org/wiki/Bill_Atkinson
[C#]: https://en.wikipedia.org/wiki/C_Sharp_(programming_language)
[C++]: https://en.wikipedia.org/wiki/C++
[COBOL]: https://en.wikipedia.org/wiki/COBOL
[C]: https://en.wikipedia.org/wiki/C_(programming_language)
[Darmouth College]: https://en.wikipedia.org/wiki/Dartmouth_College
[Dropbox]: https://en.wikipedia.org/wiki/Dropbox_(service)
[Ericsson]: https://en.wikipedia.org/wiki/Ericsson
[Erlang]: https://en.wikipedia.org/wiki/Erlang_(programming_language)
[Facebook]: https://en.wikipedia.org/wiki/Facebook
[Fortran]: https://en.wikipedia.org/wiki/Fortran
[GitHub]: https://en.wikipedia.org/wiki/GitHub
[Go]: https://en.wikipedia.org/wiki/Go_(programming_language)
[Google]: https://en.wikipedia.org/wiki/Google
[Guido van Rossum]: https://en.wikipedia.org/wiki/Guido_van_Rossum
[Haskell]: https://en.wikipedia.org/wiki/Haskell_(programming_language)
[Hypertalk]: https://en.wikipedia.org/wiki/HyperTalk
[IBM]: https://en.wikipedia.org/wiki/IBM
[INRIA]: https://en.wikipedia.org/wiki/French_Institute_for_Research_in_Computer_Science_and_Automation
[Java]: https://en.wikipedia.org/wiki/Java_(programming_language)
[Javascript]: https://en.wikipedia.org/wiki/JavaScript
[Ken Thompson]: https://en.wikipedia.org/wiki/Ken_Thompson_(computer_programmer)
[LISP]: https://en.wikipedia.org/wiki/Lisp_(programming_language)
[Larry Wall]: https://en.wikipedia.org/wiki/Larry_Wall
[MIT Media Lab]: https://en.wikipedia.org/wiki/MIT_Media_Lab
[MIT]: https://en.wikipedia.org/wiki/Massachusetts_Institute_of_Technology
[Microsoft]: https://en.wikipedia.org/wiki/Microsoft
[Mozilla]: https://en.wikipedia.org/wiki/Mozilla
[Netscape]: https://en.wikipedia.org/wiki/Netscape_(web_browser)
[Niklaus Wirth]: https://en.wikipedia.org/wiki/Niklaus_Wirth
[OCaml]: https://en.wikipedia.org/wiki/OCaml
[Objective C]: https://en.wikipedia.org/wiki/Objective-C
[Oracle]: https://en.wikipedia.org/wiki/Oracle_Corporation
[PHP]: https://en.wikipedia.org/wiki/PHP
[Pascal]: https://en.wikipedia.org/wiki/Pascal_(programming_language)
[Perl]: https://en.wikipedia.org/wiki/Perl
[Prolog]: https://en.wikipedia.org/wiki/Prolog
[Python]: https://en.wikipedia.org/wiki/Python_(programming_language)
[Rasmus Lerdorf]: https://en.wikipedia.org/wiki/Rasmus_Lerdorf
[Ruby]: https://en.wikipedia.org/wiki/Ruby_(programming_language)
[Rust]: https://en.wikipedia.org/wiki/Rust_(programming_language)
[SQL]: https://en.wikipedia.org/wiki/SQL
[Scratch]: https://en.wikipedia.org/wiki/Scratch_(programming_language)
[Shell]: https://en.wikipedia.org/wiki/Shell_script
[Sun]: https://en.wikipedia.org/wiki/Sun_Microsystems
[Swift]: https://en.wikipedia.org/wiki/Swift_(programming_language)
[US DoD]: https://en.wikipedia.org/wiki/US_Department_of_Defense
[University of Glasgow]: https://en.wikipedia.org/wiki/University_of_Glasgow
[Yukihiro Matsumoto]: https://en.wikipedia.org/wiki/Yukihiro_Matsumoto
[most popular language on stackoverflow]: https://insights.stackoverflow.com/survey/2019#technology
[most popular language on the TIOBE index]: https://www.tiobe.com/tiobe-index/
[relational model]: https://en.wikipedia.org/wiki/Relational_model
[stored program]: https://en.wikipedia.org/wiki/Stored-program_computer
[type classes]: https://en.wikipedia.org/wiki/Type_classes

[[!tag programming history debian-planet python-planet]]
