[[!meta title="Tip of the day: batch PDF conversion with LibreOffice"]]

Someone asked me today why they couldn't write on the [DOCX][]
document they received from a student using the pen in their [Onyx
Note Pro][] reader. The answer, of course, is that while the Onyx can
read those files, it can't annotate them: that only works with PDFs.

[Onyx Note Pro]: https://onyxboox.com/boox_notepro
[DOCX]: https://en.wikipedia.org/wiki/Office_Open_XML

Next question then, is of course: do I really need to open each file
separately and save them as PDF? That's going to take forever, I have
30 students per class!

> *Fear not, shell scripting and headless mode flies in to the rescue!*

As it turns out, one of the [Libreoffice parameters][] allow you to
run batch operations on files. By calling:

    libreoffice --headless --convert-to pdf *.docx

[Libreoffice parameters]: https://help.libreoffice.org/Common/Starting_the_Software_With_Parameters

LibreOffice will happily convert all the `*.docx` files in the current
directory to PDF. But because navigating the commandline can be hard,
I figured I could push this a tiny little bit further and wrote the
following script:

    #!/bin/sh
    
    exec libreoffice --headless --convert-to pdf "$@"

Drop this in `~/.local/share/nautilus/scripts/libreoffice-pdf`, mark
it executable, and voilà! You can batch-convert basically any text
file (or anything supported by LibreOffice, really) into PDF.

Now I wonder if this would be a useful addition to the Debian package,
anyone?

[[!tag debian-planet hack pdf python-planet debian]]
