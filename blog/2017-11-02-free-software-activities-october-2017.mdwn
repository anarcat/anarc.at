[[!meta title="October 2017 report: LTS, feed2exec beta, pandoc filters, git mediawiki"]]

[[!toc levels=2]]

Debian Long Term Support (LTS)
==============================

This is my monthly [Debian LTS][] report. This time I worked on the
famous KRACK attack, git-annex, golang and the continuous stream of
GraphicsMagick security issues.

[Debian LTS]: https://www.freexian.com/services/debian-lts.html

WPA & KRACK update
------------------

I spent most of my time this month on the [Linux WPA code](http://w1.fi/wpa_supplicant/), to
backport it to the old (~2012) `wpa_supplicant` release. I
first [published](https://lists.debian.org/87k1zlbfbe.fsf@curie.anarc.at) a patchset based on the patches shipped after the
embargo for the oldstable/jessie release. After feedback from the
list, I also [built packages for i386 and ARM](https://lists.debian.org/87bmktlo9f.fsf@angela.anarc.at).

I have also reviewed the [WPA protocol](https://en.wikipedia.org/wiki/Wi-Fi_Protected_Access) to make sure I understood
the implications of the changes required to backport the patches. For
example, I removed the patches touching the WNM sleep mode code as
that was introduced only in the 2.0 release. Chunks of code regarding
state tracking were also not backported as they are part of the state
tracking code introduced later, in [3ff3323](http://w1.fi/cgit/hostap/commit/?id=bb598c3bdd0616f0c15e1a42e99591d8f3ff3323). Finally, I still have
concerns about the nonce setup in patch #5. In the last chunk, you'll
notice `peer->tk` is reset, to_set to negotiate a new `TK`. The other
approach I considered was to backport [1380fcbd9f](http://w1.fi/cgit/hostap/commit/?id=1380fcbd9f) ("*TDLS: Do not
modify RNonce for an TPK M1 frame with same INonce*") but I figured I
would play it safe and not introduce further variations.

I should note that I share [Matthew Green's observations](https://blog.cryptographyengineering.com/2017/10/16/falling-through-the-kracks/) regarding
the opacity of the protocol. Normally, network protocols are freely
available and security researchers like me can easily review them. In
this case, I would have needed to read the
opaque [802.11i-2004 pdf](http://standards.ieee.org/getieee802/download/802.11i-2004.pdf) which is behind a [TOS](https://en.wikipedia.org/wiki/Terms_of_service) wall at
the [IEEE](https://en.wikipedia.org/wiki/Institute_of_Electrical_and_Electronics_Engineers). I ended up reading up on the [IEEE_802.11i-2004](https://en.wikipedia.org/wiki/IEEE_802.11i-2004)
Wikipedia article which gives a simpler view of the protocol. But it's
a real problem to see such critical protocols developed behind closed
doors like this.

At [Guido's suggestion](https://lists.debian.org/20171024061447.yeaxb6njunrtpvyg@bogon.m.sigxcpu.org), I [sent the final patch upstream](https://lists.debian.org/878tfxlgqq.fsf@angela.anarc.at)
explaining the concerns I had with the patch. I have not, at the time
of writing, received any response from upstream about this,
unfortunately. I uploaded the fixed packages as [DLA 1150-1](https://lists.debian.org/20171031144826.enjjqbqvsu2cohy2@curie.anarc.at) on
October 31st.

Git-annex
---------

The next big chunk on my list was completing the work on git-annex
([[!debcve CVE-2017-12976]]) that I started in August. It turns out
doing the backport was simpler than I expected, even with my rusty
experience with Haskell. Type-checking really helps in doing the right
thing, especially considering how Joey Hess implemented the fix: by
introducing a new type.

So I [backported the patch from upstream](https://lists.debian.org/87she9bn4c.fsf@curie.anarc.at) and notified the security
team that the jessie and stretch updates would be similarly easy. I
shipped the backport to LTS as [DLA-1144-1](https://lists.debian.org/debian-lts-announce/2017/10/msg00026.html). I also [shared](https://lists.debian.org/87wp3ikmf0.fsf@angela.anarc.at) the
updated packages for jessie (which required a similar backport) and
stretch (which didn't) and those Sebastien Delafond published those
as [DSA 4010-1](https://lists.debian.org/E1e94i7-0002xZ-9O@seger.debian.org).

Graphicsmagick
--------------

Up next was yet another security vulnerability in the Graphicsmagick
stack. This involved the usual deep dive into intricate and sometimes
just unreasonable C code to try and fit a round tree in a square
sinkhole. I'm always unsure about those patches, but the test suite
passes, smoke tests show the vulnerability as fixed, and that's pretty
much as good as it gets.

The announcement ([DLA 1154-1](https://lists.debian.org/20171031174800.q262mmqkt3ccxxc7@curie.anarc.at)) turned out to be a little special
because I had previously [noticed](https://lists.debian.org/87she4k3gt.fsf@angela.anarc.at) that the penultimate
announcement (DLA 1130-1) was never sent out. So I made a merged
announcement to cover both instead of re-sending the original 3 weeks
late, which may have been confusing for our users.

Triage & misc
-------------

We always do a bit of triage even when not on frontdesk duty, so I:

 * sent [another ping to the ca-certificates maintainer](https://lists.debian.org/87efpuc95w.fsf@curie.anarc.at)

 * triaged Puppet's [[!debcve CVE-2016-5714]] out of wheezy and other
   suites, after a thorough analysis of the what has become
   the [intricate numbering scheme for Puppet suites](https://puppet.com/docs/puppet/4.10/about_agent.html)

 * triaged ImageMagick as not affecting in wheezy and jessie, but it
   turned out the latter was a little too enthusiastic as the team
   wanted to wait for upstream confirmation before skipping jessie

 * did some research on tiff's [[!debcve CVE-2017-11613]] (skipped by
   RHEL) and [[!debcve CVE-2017-9935]] (no fix upstream)

I also did smaller bits of work on:

 * worked on a patch to add a `dch --lts` flag in [[!debbug 762715]]
   which is currently pending review

 * [golang](https://tracker.debian.org/pkg/golang)'s [[!debcve CVE-2017-15041]] which I
   originally [triaged out](https://lists.debian.org/87zi8g8h65.fsf@curie.anarc.at) but then [changed my mind](https://lists.debian.org/87vaj0k4fq.fsf@angela.anarc.at) as the
   patch was small and the impact was large. This turned
   into [DLA-1148-1](https://lists.debian.org/20171027154341.2ws6u6p7jzx4ibub@angela.anarc.at).

The latter reminded me of the concerns I have about the long-term
maintainability of the golang ecosystem: because everything is
statically linked, an update to a core library (say the SMTP library
as in [[!debcve CVE-2017-15042]], thankfully not affecting LTS)
requires a full rebuild of all packages including the library in all
distributions. So what would be a simple update in a shared library
system could mean an explosion of work on statically linked
infrastructures. This is a lot of work which can definitely be
error-prone: as I've seen in other updates, some packages (for example
the Ruby interpreter) just bit-rot on their own and eventually fail to
build from source. We would also have to investigate all packages to
see which one include the library, something which we are not well
equipped for at this point.

Wheezy was the first release shipping golang packages but at least
it's shipping only *one*... Stretch has shipped with *two* golang
versions (1.7 and 1.8) which will make maintenance ever harder in the
long term.

> We build our computers the way we build our cities--over time,
> without a plan, on top of ruins.
>                         - [Ellen Ullman](https://www.salon.com/1998/05/12/feature_321/)
   
Other free software work
========================

This month again, I was busy doing some serious [yak shaving](https://en.wiktionary.org/wiki/yak_shaving)
operations all over the internet, on top of publishing two of my
largest LWN articles to date
([[2017-10-16-strategies-offline-pgp-key-storage]] and
[[2017-10-26-comparison-cryptographic-keycards]]).

feed2exec beta
--------------

Since I
[[announced|2017-10-02-free-software-activities-september-2017#new-project-feed2exec]]
this new project last month I have released it as a [beta](https://gitlab.com/anarcat/feed2exec/tags/0.6.0) and
it [entered Debian](https://tracker.debian.org/pkg/feed2exec). I have also wrote useful plugins like the
`wayback` plugin that saves pages on the [Wayback machine](http://web.archive.org/) for
eternal archival. The `archive` plugin can also similarly save pages
to the local filesystem. I also added bash completion, expanded unit
tests and documentation, fixed default file paths and a bunch of bugs,
and refactored the code. Finally, I also started using two external
Python libraries instead of rolling my own code: the [pyxdg](https://pypi.python.org/pypi/pyxdg)
and [requests-file](https://pypi.python.org/pypi/requests-file) libraries, the latter which
I [packaged in Debian](https://packages.debian.org/python-requests-file) (and [fixed a bug in their test suite](https://github.com/dashea/requests-file/pull/9)).

The program is working pretty well for me. The only thing I feel is
really missing now is a retry/fail mechanism. Right now, it's a little
brittle: any network hiccup will yield an error email, which are
readable to me but could be confusing to a new user. Strangely enough,
I am particularly having trouble with (local!) DNS resolution that I
need to look into, but that is probably unrelated with the software
itself. Thankfully, the user can disable those with `--loglevel=ERROR`
to silence `WARNING`s.

Furthermore, some plugins still have some rough edges. For example,
The [Transmission](https://transmissionbt.com/) integration would probably work better as a
distinct plugin instead of a simple `exec` call, because when it adds
new torrents, the output is totally cryptic. That plugin could also
leverage more feed parameters to save different files in different
locations depending on the feed titles, something would be hard to do
safely with the `exec` plugin now.

I am keeping a steady flow of releases. I wish there was a way to see
how effective I am at reaching out with this project, but
unfortunately [GitLab doesn't provide usage statistics](https://gitlab.com/gitlab-org/gitlab-ce/issues/21743)... And I
have received only a few comments on IRC about the project, so maybe I
need to [reach out more](https://twitter.com/theanarcat/status/926106023022219265) like it says in
the [fine manual](https://opensource.guide/finding-users/). Always feels strange to have to promote your
project like it's some new bubbly soap...

Next steps for the project is a final review of the API and release
production-ready 1.0.0. I am also thinking of making a small
screencast to show the basic capabilities of the software, maybe
with [asciinema's upcoming audio support](https://github.com/asciinema/asciinema-server/issues/63)?

Pandoc filters
--------------

As I mentioned earlier, I dove again in Haskell programming when
working on the git-annex security update. But I also have a small
Haskell program of my own - a [Pandoc filter](https://gitlab.com/anarcat/scripts/blob/main/lwn-clean.hs) that I use to convert
the HTML articles I publish on LWN.net into a Ikiwiki-compatible
markdown version. It turns out the script was still missing a bunch of
stuff: image sizes, proper table formatting, etc. I also worked hard
on automating more bits of the publishing workflow by extracting the
time from the article which allowed me to simply extract the full
article into an almost final copy just by specifying the article
ID. The only thing left is to add tags, and the article is complete.

In the process, I learned about new weird Haskell constructs. Take
this code, for example:

[[!format haskell """
-- remove needless blockquote wrapper around some tables
--
-- haskell newbie tips:
--
-- @ is the "at-pattern", allows us to define both a name for the
-- construct and inspect the contents as once
--
-- {} is the "empty record pattern": it basically means "match the
-- arguments but ignore the args"
cleanBlock (BlockQuote t@[Table {}]) = t
"""]]

Here the idea is to remove `<blockquote>` elements needlessly wrapping
a `<table>`. I can't specify the `Table` type on its own, because then
I couldn't address the table as a whole, only its parts. I could
reconstruct the whole table bits by bits, but it wasn't as clean.

The other pattern was how to, at last, address multiple string
elements, which was difficult because Pandoc treats spaces specially:

[[!format haskell """
cleanBlock (Plain (Strong (Str "Notifications":Space:Str "for":Space:Str "all":Space:Str "responses":_):_)) = []
"""]]

The last bit that drove me crazy was the date parsing:

[[!format haskell """
-- the "GAByline" div has a date, use it to generate the ikiwiki dates
--
-- this is distinct from cleanBlock because we do not want to have to
-- deal with time there: it is only here we need it, and we need to
-- pass it in here because we do not want to mess with IO (time is I/O
-- in haskell) all across the function hierarchy
cleanDates :: ZonedTime -> Block -> [Block]
-- this mouthful is just the way the data comes in from
-- LWN/Pandoc. there could be a cleaner way to represent this,
-- possibly with a record, but this is complicated and obscure enough.
cleanDates time (Div (_, [cls], _)
                 [Para [Str month, Space, Str day, Space, Str year], Para _])
  | cls == "GAByline" = ikiwikiRawInline (ikiwikiMetaField "date"
                                           (iso8601Format (parseTimeOrError True defaultTimeLocale "%Y-%B-%e,"
                                                           (year ++ "-" ++ month ++ "-" ++ day) :: ZonedTime)))
                        ++ ikiwikiRawInline (ikiwikiMetaField "updated"
                                             (iso8601Format time))
                        ++ [Para []]
-- other elements just pass through
cleanDates time x = [x]
"""]]

Now that seems just dirty, but it was even worse before. One thing I
find difficult in adapting to coding in Haskell is that you need to
take the habit of writing smaller functions. The language is really
not well adapted to long discourse: it's more about getting small
things connected together. Other languages (e.g. Python) discourage
this because there's some overhead in calling functions (10
nanoseconds in my tests, but still), whereas functions are a
fundamental and important construction in Haskell that are much more
heavily optimized. So I constantly need to remind myself to split
things up early, otherwise I can't do anything in Haskell.

Other languages are more lenient, which does mean my code can be more
dirty, but I feel get things done faster then. The oddity of Haskell
makes frustrating to work with. It's like doing construction work but
you're not allowed to get the floor dirty. When I build stuff, I don't
mind things being dirty: I can cleanup afterwards. This is especially
critical when you don't actually *know* how to make things clean in
the first place, as Haskell
will [simply not *let* you do that at all](https://www.youtube.com/watch?v=ARJ8cAGm6JE).

And obviously, I fought with Monads, or, more specifically, "I/O" or
`IO` in this case. Turns out that getting the current time is `IO` in
Haskell: indeed, it's not a "pure" function that will always return
the same thing. But this means that I would have had to change the
signature of *all* the functions that touched time to include `IO`. I
eventually moved the time initialization up into `main` so that I had
only one `IO` function and moved that timestamp downwards as simple
argument. That way I could keep the rest of the code clean, which
seems to be an acceptable pattern.

I would of course be happy to get feedback from my Haskell readers (if
any) to see how to improve that code. I am always eager to learn.

Git remote MediaWiki
--------------------

Few people know that there is a [MediaWiki remote for Git](https://github.com/Git-Mediawiki/Git-Mediawiki/) which
allow you to mirror a [MediaWiki](https://www.mediawiki.org/) site as a Git repository. As a
disaster recovery mechanism, I have been keeping such a historical
backup of the [Amateur radio wiki](http://www.amateur-radio-wiki.net/) for a while now. This originally
started as a [homegrown Python script](https://gitlab.com/anarcat/mediawikigitdump) to also convert the contents
in Markdown. My theory then was to see if we could switch from
Mediawiki to Ikiwiki, but it took so long to implement that I never
completed the work.

When someone had the weird idea of renaming a page to
some [impossible long name](http://www.amateur-radio-wiki.net/index.php?title=Talk:Oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh_no_oh) on the wiki, my script broke. I tried
to look at fixing it and then remember I *also* had a mirror running
using the Git remote. It turns out it *also* broke on the same issue
and that got me looking in the remote again. I
got [lost in a zillion issues](https://github.com/Git-Mediawiki/Git-Mediawiki/issues?utf8=%E2%9C%93&q=involves%3Aanarcat%20updated%3A%3E%3D2017-10-01), including fixing
that [specific issue](https://github.com/Git-Mediawiki/Git-Mediawiki/issues/45), but I especially looked at the possibility
of [fetching all namespaces](https://github.com/Git-Mediawiki/Git-Mediawiki/issues/10) because I realized that the remote
fetches only a part of the wiki by default. And *that* drove me to
submit namespace support as a [patch](https://public-inbox.org/git/20171029160857.29460-1-anarcat@debian.org/) to
the [git mailing list](https://git.wiki.kernel.org/index.php/GitCommunity). Finally, the discussion came back
to [how to actually maintain that contrib](https://public-inbox.org/git/87vaix731f.fsf@curie.anarc.at/): in git core or outside?
Finally, it looks like I'll be doing some maintenance that project
outside of git, as I was [granted access](https://github.com/Git-Mediawiki/Git-Mediawiki/issues/33#issuecomment-340399575) to the GitHub
organisation...

Galore Yak Shaving
------------------

Then there's the usual hodgepodge of fixes and random things I did
over the month.

 * Beta testing for the new [Signal](https://signal.org/) [desktop app](https://signal.org/blog/standalone-signal-desktop/) which is,
   unfortunately, an Electron app. This means the binary is huge, at
   95MB, and the app takes a whopping 300MB of ram right after
   startup. Compare this with my IRC client ([irssi](https://irssi.org/)) which takes
   15MB of ram or pidgin, which takes 80MB of ram. Then ponder the
   costs of developer convenience versus impact on the environment and
   users...

 * Routine linkchecker maintenance: [3 PRs merged](https://github.com/linkcheck/linkchecker/commits?author=anarcat&since=2017-10-01T04:00:00Z&until=2017-11-01T04:00:00Z) including
   a [bugfix](https://github.com/linkcheck/linkchecker/pull/86) of my own, one of which inspired me to add the pyxdg
   dependency in feed2exec.

 * When adding "[badges](http://shields.io/)" to the feed2exec documentation, I also
   fixed an issue in [badges.debian.net](http://badges.debian.net/), where
   [Debian was referring to wheezy instead of the symbolic name](https://github.com/FedericoCeratto/distrobadges/pull/3)

 * Tested the [Wireguard](http://wireguard.io/) VPN with moderate success. It can only
   cross NAT one way, so it was useless for my use case. I ended up
   using end to end IPv6! still,
   I [updated the Turris OS Wireguard version](https://github.com/CZ-NIC/turris-os/pull/64) so that I could use
   this as a way to get end to end IPv4 connectivity to machines
   behind my NAT.

 * Tested a static image gallery generator replacement
   called [Sigal](http://sigal.saimon.org/en/latest/). I suggested a way to
   add [progress information for video processing](https://github.com/saimn/sigal/issues/268) and did a
   general issue review. I also looked at how to package it in Debian,
   in [RFP #879239](https://bugs.debian.org/879239). I looked into Sigal after finally getting a
   confirmation from the [Photofloat](https://git.zx2c4.com/PhotoFloat/about/) maintainer (Jason
   A. Donenfeld, who's also the Wireguard author)
   that [our patches](https://lists.zx2c4.com/pipermail/photofloat/2014-September/000054.html) will never get merged, after 3 years of radio
   silence. In comparison, the response I got from the communications
   with Sigal were much more positive. I have therefore [requested
   removal of photofloat from Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=880592)

 * More on ham radio stuff: I got a [Signalink](http://www.tigertronics.com/) working, which
   allowed me to receive and transmit APRS signals using a handheld
   transmitter

 * I worked more on [SafeEyes](http://slgobinath.github.io/SafeEyes/) to try and get it
   to [credit me idle time](https://github.com/slgobinath/SafeEyes/issues/192), which is still not working yet,
   and [suggested improvements to the preferences panel](https://github.com/slgobinath/SafeEyes/issues/190)

 * I found out about the [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) web page auditor, which
   doesn't seem to work at all in Chromium, but has a lot of
   potential. there's a standalone version as well, which could be
   packaged for Debian, but found that [#775925](https://bugs.debian.org/775925) is for a
   different [lighthouse](https://github.com/vinumeris/lighthouse/) - a dead Bitcoin-based crowdfunding
   platform

 * I helped [Koumbit.org](https://www.koumbit.org/) [configure load balancing](https://wiki.koumbit.net/action/diff/Relayd?action=diff&rev1=11&rev2=12) for their
   HTTPS services. For this, I needed to change my IP address, so I
   test again the [Bitmask](https://bitmask.net/) client and found problems
   with [polkit](https://0xacab.org/leap/bitmask-dev/issues/9050) and the [password failure prompt](https://0xacab.org/leap/bitmask-dev/issues/9127). I also
   suggested improvements the [kerying package](https://0xacab.org/leap/leap_se/issues/6).

 * After realizing there was [no future](https://github.com/docwhat/itsalltext/issues/94#issuecomment-336473024) in the [It's all text!](https://github.com/docwhat/itsalltext)
   (IAT) extension, I have switched to [Ghosttext](https://github.com/GhostText/GhostText) which works both
   in Chromium and Firefox and uses a model similar to
   the [Edit in Emacs](https://www.emacswiki.org/emacs/Edit_with_Emacs) extension for Chrome, but it also supports
   Firefox and other editors. I
   have [filed an issue about silent failures with uMatrix](https://github.com/GhostText/GhostText/issues/105) and
   generally tried to help the IAT community find an exit
   strategy. (Short story: it's impossible to port, and [XUL](https://en.wikipedia.org/wiki/XUL) will die a
   horrible death.)
   
> There is no [web extension] only XUL! - [Inside joke](https://en.wikipedia.org/wiki/XUL#Etymology_and_Ghostbusters_references)

[[!tag debian feed2exec git haskell mediawiki debian-lts software geek free debian-planet python-planet monthly-report]]
