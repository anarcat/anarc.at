[[!meta title="What is going on with web servers"]]

I stumbled upon [this graph recently](https://w3techs.com/technologies/history_overview/web_server/ms/y), which is [w3techs.com](https://w3techs.com/)
graph of "Historical yearly trends in the usage statistics of web
servers". It seems I hadn't looked at it in a long while because I was
surprised at *many* levels:

 1. Apache is now second, behind Nginx, since ~2022 (so that's really
    new at least)

 2. Cloudflare "server" is *third* ahead of the traditional third
    (Microsoft IIS) - I somewhat knew that Cloudflare was hosting a
    lot of stuff, but I somehow didn't expect to see it there at all
    for some reason

 3. I had to lookup what [LiteSpeed](https://en.wikipedia.org/wiki/LiteSpeed_Web_Server) was (and it's not a [bike
    company](https://en.wikipedia.org/wiki/Litespeed)): it's a drop-in replacement (!?) of the Apache web
    server (not a fork, a bizarre idea but which seems to be gaining a
    lot of speed recently, possibly because of its support for QUIC
    and HTTP/3

So there. I'm surprised. I guess the stats should be taken with a
grain of salt because they only partially correlate with [Netcraft's
numbers](https://news.netcraft.com/archives/2022/02/28/february-2022-web-server-survey.html) which barely mention LiteSpeed at all. (But they do point
at a rising share as well.) 

Netcraft also puts Nginx's first place earlier in time, around April
2019, which is about when Microsoft's IIS took a massive plunge
down. That is another thing that doesn't map with w3techs' graphs at
all either.

So it's all [lies and statistics](https://en.wikipedia.org/wiki/Lies%2C_damned_lies%2C_and_statistics), basically. Moving on.

Oh and of course, the two first most popular web servers, regardless
of the source, are package in Debian. So while we're working on
statistics and just making stuff up, I'm going to go ahead and claim
all of this stuff runs on Linux and that [BSD is dead](https://en.wikipedia.org/wiki/Netcraft_confirms_it). Or
something like that.

[[!tag web history stats debian-planet python-planet]]
