[[!meta redir=blog delay=30]]

stub file for the Drupal backwards compatible [[RSS feed|node/feed]]. see [[blog]] for the actual blog.

copy of [[../node]]

[[!inline pages="blog/* and !blog/*/* and !link(foo) and 
!tagged(draft) and !tagged(redirection)"
feedonly=yes
title="Anarcat. Blog."
description="Pourquoi faire simple quand on peut faire compliqué."
feedshow="10"
feedfile="feed"
]]

[[!tag redirection]]
