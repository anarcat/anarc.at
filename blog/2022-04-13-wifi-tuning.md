[[!meta title="Tuning my wifi radios"]]

After listening to an episode of the [2.5 admins podcast](https://2.5admins.com/), I
realized there was some sort of low-hanging fruit I could pick to
better tune my WiFi at home. You see, I'm kind of a fraud in WiFi: I
only [started a WiFi mesh in Montreal](https://wiki.reseaulibre.ca/) (now defunct), I don't
*really* know how any of that stuff works. So I was surprised to hear
one of the podcast host say "it's all about airtime" and "you want to
reduce the power on your access points" (APs). It seemed like sound
advice: better bandwidth means less time on air, means less
collisions, less latency, and less power also means less
collisions. Worth a try, right?

# Frequency

So the first thing I looked at was [WifiAnalyzer](https://vremsoftwaredevelopment.github.io/WiFiAnalyzer/) to see if I had
any optimisation I could do there. Normally, I try to avoid having
nearby APs on the same frequency to avoid collisions, but who knows,
maybe I had messed that up. And turns out I did! Both APs were on
"auto" for 5GHz, which typically means "do nothing or worse".

5GHz is really interesting, because, in theory, there are [LOTS of
channels][] to pick from, it goes up to 196!! And both my APs were on
36, what gives?

[LOTS of channels]: https://en.wikipedia.org/wiki/List_of_WLAN_channels#5_GHz_(802.11a/h/j/n/ac/ax)

So the first thing I did was to set it to channel 100, as there was
that long gap in WifiAnalyzer where *no* other AP was. But that just
broke 5GHz on the AP. The OpenWRT GUI (luci) would just say "wireless
not associated" and the ESSID wouldn't show up in a scan anymore.

At first, I thought this was a problem with OpenWRT or my hardware,
but I could reproduce the problem with both my APs: a [TP-Link Archer
A7 v5](https://openwrt.org/toh/tp-link/archer_a7_v5) and a [Turris Omnia](https://openwrt.org/toh/turris/turris_omnia) (see also [my review](https://anarc.at/blog/2016-11-15-omnia/)).

As it turns out, that's because that range of the WiFi band interferes
with trivial things like satellites and radar, which make the actually
very useful radar maps look like useless christmas trees. So those
channels require [DFS](https://en.wikipedia.org/wiki/Dynamic_frequency_selection) to operate. DFS works by first listening on
the frequency for a certain amount of time (1-2 minute, but could be
as high as 10) to see if there's something else transmitting at all.

So typically, that means they just don't operate at all in those
bands, especially if you're near any major city which generally means
you *are* near a weather radar that *will* transmit on that band.

In the system logs, if you have such a problem, you might see this:

    Apr  9 22:17:39 octavia hostapd: wlan0: DFS-CAC-START freq=5500 chan=100 sec_chan=1, width=0, seg0=102, seg1=0, cac_time=60s
    Apr  9 22:17:39 octavia hostapd: DFS start_dfs_cac() failed, -1

... and/or this:

    Sat Apr  9 18:05:03 2022 daemon.notice hostapd: Channel 100 (primary) not allowed for AP mode, flags: 0x10095b NO-IR RADAR
    Sat Apr  9 18:05:03 2022 daemon.warn hostapd: wlan0: IEEE 802.11 Configured channel (100) not found from the channel list of current mode (2) IEEE 802.11a
    Sat Apr  9 18:05:03 2022 daemon.warn hostapd: wlan0: IEEE 802.11 Hardware does not support configured channel

Here, it clearly says `RADAR` (in all caps too, which means it's
really important). `NO-IR` is also important, I'm not sure what it
means but it could be that you're not allowed to transmit in that band
because of other local regulations. 

There might be a way to workaround those by changing the "region" in
the Luci GUI, but I didn't mess with that, because I figured that
other devices will have *that* already configured. So using a
forbidden channel might make it more difficult for clients to connect
(although it's possible this is enforced only on the AP side).

In any case, 5GHz is promising, but in reality, you only get from
channel 36 (5.170GHz) to 48 (5.250GHz), inclusively. Fast counters
will notice that is *exactly* 80MHz, which means that if an AP is
configured for that hungry, all-powerful 80MHz, it will effectively
take up *all* 5GHz channels at once.

This, in other words, is as bad as 2.4GHz, where you also have only
two 40MHz channels. (Really, what did you expect: this is an
unregulated frequency controlled by commercial interests...)

So the first thing I did was to switch to 40MHz. This gives me two
distinct channels in 5GHz at no noticeable bandwidth cost. (In fact, I
couldn't find hard data on what the bandwidth ends up being on those
frequencies, but I could still get 400Mbps which is fine for my use
case.)

# Power

The next thing I did was to fiddle with power. By default, both radios
were configured to transmit as much power as they needed to reach
clients, which means that if a client gets farther away, it would
boost its transmit power which, in turns, would mean the client would
still connect to instead of *failing* and properly roaming to the
other AP. 

The higher power also means more interference with neighbors and other
APs, although that matters less if they are on different channels.

On 5GHz, power was about 20dBm (100 mW) -- and more on the Turris! --
when I first looked, so I tried to lower it drastically to 5dBm (3mW)
just for kicks. That didn't work so well, so I bumped it back up to 14
dBm (25 mW) and that seems to work well: clients hit about -80dBm when
they get far enough from the AP, which gets close to the noise floor
(and where the neighbor APs are), which is exactly what I want.

On 2.4GHz, I lowered it down even further, to 10 dBm (10mW) since it's
better at going through wells, I figured it would need less power. And
anyways, I rather people use the 5GHz APs, so maybe that will act as
an encouragement to switch. I was still able to connect correctly to
the APs at that power as well.

# Other tweaks

I disabled the "Allow legacy 802.11b rates" setting in the 5GHz
configuration. According to [this discussion](https://forum.openwrt.org/t/clarification-on-allow-legacy-802-11b-rates-in-advance-setting/65429):

> Checking the "Allow b rates" affects what the AP will transmit. In
> particular it will send most overhead packets including beacons,
> probe responses, and authentication / authorization as the slow,
> noisy, 1 Mb DSSS signal. That is bad for you and your neighbors. Do
> not check that box. The default really should be unchecked.

This, in particular, "*will make the AP unusable to distant clients,
which again is a good thing for public wifi in general*". So I just
unchecked that box and I feel happier now. I didn't make tests to see
the effect separately however, so this is mostly just a guess.

[[!tag wifi radio debian-planet python-planet openwrt]]
