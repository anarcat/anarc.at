<!-- Drupal node 75 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Victoire sur Loto-Québec! Pas de casino à Pointe St-Charles!"]]
[[!meta date="2006-03-11T14:13:04-0500"]]
[[!meta updated="2012-12-14T22:45:07-0500"]]
[[!meta guid="75 at http://anarcat.koumbit.org"]]

[Victoire](http://www.radio-canada.ca/nouvelles/Economie-Affaires/2006/03/10/004-casino-abandon.shtml) pour les [organismes de lutte](http://noncasino.net/) contre le projet de déménager le casino de Montréal à Pointe St-Charles! C'est pour moi et [mes camarades de lutte](http://laruebrique.org/) un grand soulagement de voir cet épisode tirer à sa fin. Pour résumer: après la publication tant réclamée du rapport Coulombe, le Cirque du Soleil s'est retiré du projet, ce qui, selon Loto-Québec, a forcé l'arrêt du projet.

Au point, on pouvait voir, Ô superbe neutralité, une entrevue avec deux promoteurs du projet: un du Cirque et l'autre de la Loto. Pas un mot évidemment sur la grande lutte citoyenne menée depuis le début contre cet infâme projet. On a même laissé dire à un sbire de LQ que Montréal avait __besoin__ d'un casino sans quoi notre armée de pauvre joueurs compulsifs seraient ''forcés'' d'aller dans d'autres casinos, en donnant comme exemple les 20-quelques casinos en Ontario ou dans les provinces Atlantiques.

C'est vraiment prendre les gens pour de cruches de penser qu'on va croire que ces joueurs vont se déplacer 800 à 1000km pour aller jouer dans un casino ailleurs. LQ prétend que le casino n'a plus d'espace? Pourquoi ne pas le fermer? Pourquoi ne pas, en pire cas, cesser son expansion?

La question plus générale __doit__ se poser: à quoi ça sert ça les casinos? Autre que le fric que ça rapporte à la société d'état pour financer ses réformes débiles et ses foutues baisses de taxes sur le dos des pauvres joueurs malades, ''à quoi ça sert?'' Pourquoi Montréal ne se présenterait-il pas mondialement comme un précurseur en s'échappant de cette insanité?

Et que les casinos clandestins foisonnent, en passant, je n'ai pas vraiment de problème avec ça. C'était pas un problème avant, et ça ne sera certainement pas plus un problème que cette atrocité commandité par l'état.

Ça, c'était ma première montée de lait, j'ai pas fini.

[[!tag "politique" "nouvelles"]]