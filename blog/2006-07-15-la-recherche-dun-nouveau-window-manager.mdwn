<!-- Drupal node 94 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="À la recherche d'un nouveau window manager"]]
[[!meta date="2006-07-15T06:44:45-0500"]]
[[!meta updated="2012-12-14T22:45:08-0500"]]
[[!meta guid="94 at http://anarcat.koumbit.org"]]

Bon, maintenant que Gnome commence à planter et que je commence à utiliser ce portable pour un peu plus que "mail, web, irc, terminal" (ie. Emacs), la mémoire commence à manquer et j'ai commencé à regarder pour des alternatives aux colosses que sont les "desktop environments".

Le premier truc que j'ai regardé est [wmii](http://wmii.de/), car des collègues l'utilisent déjà. Très intéressant, mais le problème est que, bien qu'ils appellent ça du "dynamic window management", c'est quand même très peu flexible, et ça laisse très peu de place à mon environnement traditionnel de "desktop". Je veux dire par là que les fenêtres flottent un peu partout, j'ai un desktop à l'arrière avec un joli fond d'écran et des dossiers qui lancent un file manager, etc. wmii n'est pas du tout dans ce mode là. Donc, viré, mais a des concepts très intéressants, j'apprécie en particulier les systèmes de tagging/tabbing, très puissants pour des terminaux, mais qui demandent trop de configurations pour les trucs plus flottants comme les dialogues/popups générés par un browser ou par des trucs comme GIMP.

J'ai ensuite ré-essayé fluxbox, mais il ne supporte pas le edge-flipping, alors aussi bien avoir wmii.

Finalement, je retombe tranquillement vers sawfish, mais le problème est qu'il est un peu balourd. Je n'ai pas besoin de 40 thèmes pour mes fenêtres. C'est un truc qui est très bien avec wmii: juste un thème, 1px autour des fenêtres, il m'embêtre très peu. En contraste, sawfish bouffe au moins 4 pixels pour les bordures et le thème par défaut doit prendre un bon 15-20 pixels pour les barres de titre. N'empêche, sawfish, avec sawfish-pager, supporte les "workspaces" de façon assez élégante et fonctionnelle, y compris le edge-flipping et est très configurable. C'est aussi ce que j'utilise à la maison, alors la conf est déjà faite. Le problème est que le projet est pratiquement mort et comporte encore plusieurs bugs qui le font planter à l'occasion. Embêtant.

Donc je cherche encore. L'idéal doit avoir:

* bordures minimales. pas besoin de plein de themes. pas de froufrous graphiques. (!= sawfish)
* un pager. (!= wmii)
* edge-flipping. (!= fluxbox, wmii)
* des tabs pour mes terminaux, j'ai adopté. (!= sawfish)
* footprint mémoire minimal et stable. (!= sawfish)
* me permettre de voir mon fond d'écran (!= wmii)
* gestion des fenêtres icônifiées (ROX peut faire ça)

J'en manque sûrement ici... Des idées? Et ne me dites pas e17, j'ai pas envie de compiler tous les 2 jours, et ça fait trop de truc. Je veux un WINDOW MANAGER, pas une usine à gaz qui me dit quand mon thé est prêt.

[[!tag "troll" "geek"]]