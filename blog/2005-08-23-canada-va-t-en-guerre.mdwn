<!-- Drupal node 24 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Canada-va-t-en guerre"]]
[[!meta date="2005-08-23T10:36:56-0500"]]
[[!meta updated="2012-12-14T22:40:12-0500"]]
[[!meta guid="24 at http://anarcat.koumbit.org"]]

Eh misère... les stéréotypes ont la vie dure... [Selon la BBC](http://news.bbc.co.uk/1/hi/world/europe/4175446.stm#text), le Canada envoie des navires de guerres pour "assumer la territorialité" d'une foutue roche dans l'artique. Ce qui s'est passé, c'est que notre cher ministre de la guerre (de la défense, pardon) est aller se balader sur cette île (Hans Island) qui semble être clamée par le Danemark. Ça a foutu les danois en rogne et notre gouvernement, en grand diplomate, envoie la cavalerie au lieu de négocier.

Ceci est évidemment relié au fait que la calotte artique est en train de fondre, ce qui va ouvrir un détroit historique de passage pour contourner l'Amérique au nord. Les US prétendent que ceci sont des eaux internationales (!!), et la Russie, le Danemark et le Canada vont, semble-t-il, se battre comme des chiens pour ce bout de mer.

Ce changement est le symptôme d'une catastrophe écologique de grande envergure que nous tentons encore de nier. Est-ce que nous allons continuer à garder la tête dans le sable en se battant entre nous, tels les grands chefs d'état planifiant la fin de l'humanité au fond des mines dans [Doctor Strangelove](http://www.imdb.com/title/tt0057012/)? Dans la haine, jusqu'à la fin?

L'artique est l'un des rares endroits restant sur la terre qui comporte une écosystème encore intacte et très peu exploré par l'humanité. Nous allons le détruire et y faire la guerre?

Vous vous croyez pacifistes? Ecolo et internationalistes? Ceux que vous avez élu s'en foutent éperdumment et curieusement, je ne suis pas surpris...

[[!tag "nouvelles" "environnement"]]