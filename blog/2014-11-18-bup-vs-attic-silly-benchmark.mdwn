<!-- Drupal node 204 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="bup vs attic silly benchmark"]]
[[!meta date="2014-11-18T00:39:30-0500"]]
[[!meta updated="2014-11-18T14:35:16-0500"]]
[[!meta guid="204 at http://anarcat.koumbit.org"]]

after see [attic](https://attic-backup.org/) [introduced](https://news.ycombinator.com/item?id=8621372) in a [discussion about bup](https://news.ycombinator.com/item?id=8620236), i figured out i could give it a try. it was answering two of my biggest concerns with bup:

* backup removal
* encryption

and seemed to magically out of nowhere and basically do everything i need, with an inline manual on top of it.

# disclaimer

Note: this is not a real benchmark! i would probably need to port bup and attic to [liw's seivot software](http://liw.fi/seivot/) to report on this properly (and that would amazing and really interesting, but it's late now). even worse, this was done on a production server with other stuff going on so take results with a grain of salt.

# procedure and results

Here's what I did. I setup backups of my ridiculously huge `~/src` directory on the external hard drive where I usually make my backups. I ran a clean backup with attic, than redid it, then I ran a similar backup with bup, then redid it. Here are the results:

    anarcat@marcos:~$ sudo apt-get install attic # this installed 0.13 on debian jessie amd64
    [...]
    anarcat@marcos:~$ attic init /mnt/attic-test:
    Initializing repository at "/media/anarcat/calyx/attic-test"
    Encryption NOT enabled.
    Use the "--encryption=passphrase|keyfile" to enable encryption.
    anarcat@marcos:~$ time attic create --stats /mnt/attic-test::src ~/src/
    Initializing cache...
    ------------------------------------------------------------------------------
    Archive name: src
    Archive fingerprint: 7bdcea8a101dc233d7c122e3f69e67e5b03dbb62596d0b70f5b0759d446d9ed0
    Start time: Tue Nov 18 00:42:52 2014
    End time: Tue Nov 18 00:54:00 2014
    Duration: 11 minutes 8.26 seconds
    Number of files: 283910

                           Original size      Compressed size    Deduplicated size
    This archive:                6.74 GB              4.27 GB              2.99 GB
    All archives:                6.74 GB              4.27 GB              2.99 GB
    ------------------------------------------------------------------------------
    311.60user 68.28system 11:08.49elapsed 56%CPU (0avgtext+0avgdata 122824maxresident)k
    15279400inputs+6788816outputs (0major+3258848minor)pagefaults 0swaps
    anarcat@marcos:~$ time attic create --stats /mnt/attic-test::src-2014-11-18 ~/src/
    ------------------------------------------------------------------------------
    Archive name: src-2014-11-18
    Archive fingerprint: be840f1a49b1deb76aea1cb667d812511943cfb7fee67f0dddc57368bd61c4bf
    Start time: Tue Nov 18 00:05:57 2014
    End time: Tue Nov 18 00:06:35 2014
    Duration: 38.15 seconds
    Number of files: 283910

                           Original size      Compressed size    Deduplicated size
    This archive:                6.74 GB              4.27 GB            116.63 kB
    All archives:               13.47 GB              8.54 GB              3.00 GB
    ------------------------------------------------------------------------------
    30.60user 4.66system 0:38.38elapsed 91%CPU (0avgtext+0avgdata 104688maxresident)k
    18264inputs+258696outputs (0major+36892minor)pagefaults 0swaps
    anarcat@marcos:~$ sudo apt-get install bup # this installed bup 0.25
    anarcat@marcos:~$ free && sync && echo 3 | sudo tee /proc/sys/vm/drop_caches && free # flush caches
    anarcat@marcos:~$ export BUP_DIR=/mnt/bup-test
    anarcat@marcos:~$ bup init
    Dépôt Git vide initialisé dans /mnt/bup-test/
    anarcat@marcos:~$ time bup index ~/src
    Indexing: 345249, done.
    56.57user 14.37system 1:45.29elapsed 67%CPU (0avgtext+0avgdata 85236maxresident)k
    699920inputs+104624outputs (4major+25970minor)pagefaults 0swaps
    anarcat@marcos:~$ time bup save -n src ~/src
    Reading index: 345249, done.
    bloom: creating from 1 file (200000 objects).
    bloom: adding 1 file (200000 objects).
    bloom: creating from 3 files (600000 objects).
    Saving: 100.00% (6749592/6749592k, 345249/345249 files), done.
    bloom: adding 1 file (126005 objects).
    383.08user 61.37system 10:52.68elapsed 68%CPU (0avgtext+0avgdata 194256maxresident)k
    14638104inputs+5944384outputs (50major+299868minor)pagefaults 0swaps
    anarcat@marcos:attic$ time bup index ~/src
    Indexing: 345249, done.
    56.13user 13.08system 1:38.65elapsed 70%CPU (0avgtext+0avgdata 133848maxresident)k
    806144inputs+104824outputs (137major+38463minor)pagefaults 0swaps
    anarcat@marcos:attic$ time bup save -n src2 ~/src
    Reading index: 1, done.
    Saving: 100.00% (0/0k, 1/1 files), done.
    bloom: adding 1 file (1 object).
    0.22user 0.05system 0:00.66elapsed 42%CPU (0avgtext+0avgdata 17088maxresident)k
    10088inputs+88outputs (39major+15194minor)pagefaults 0swaps

Disk usage is comparable:

    anarcat@marcos:attic$ du -sc /mnt/*attic*
    2943532K        /mnt/attic-test
    2969544K        /mnt/bup-test

People are encouraged to try and reproduce those results, which should be fairly trivial.

# Observations

Here are interesting things I noted while working with both tools:

* attic is Python3: i could compile it, with dependencies, by doing `apt-get build-dep attic` and running `setup.py` - i could also install it with `pip` if i needed to (but i didn't)
* bup is Python 2, and has a scary makefile
* both have an init command that basically does almost nothing and takes little enough time that i'm ignoring it in the benchmarks
* attic backups are a single command, bup requires me to know that i first want to `index` and then `save`, which is a little confusing
* bup has nice progress information, especially during save (because when it loaded the index, it knew how much was remaining) - just because of that, bup "feels" faster
* bup, however, lets me know about its deep internals (like now i know it uses a bloom filter) which is probably barely understandable by most people
* on the contrary, attic gives me useful information about the size of my backups, including the size of the current increment
* it is not possible to get that information from bup, even after the fact - you need to `du` before and after the backup
* attic modifies the files access times when backing up, while bup is more careful (there's a [pull request](https://github.com/jborg/attic/pull/113) to fix this in attic, which is how i found out about this)
* both backup systems seem to produce roughly the same data size from the same input

# Summary

attic and bup are about equally fast. bup took 30 seconds less than attic to save the files, but that's not counting the 1m45s it took indexing them, so on the total run time, bup was actually slower. attic is also (almost) two times faster on the second run as well. but this could be within the margin of error of this very quick experiment, so my provisional verdict for now would be that they are about as fast.

bup may be more robust (for example it doesn't modify the atimes), but this has not been extensively tested and is more based with my familiarity with the "conservatism" of the bup team rather than actual tests.

considering all the features promised by attic, it makes for a really serious contender to the already amazing bup.

# Next steps

The properly do this, we would need to:

* include other software (thinking of [Zbackup](http://zbackup.org/), [Burp](http://burp.grke.org/), [ddar](http://www.synctus.com/ddar/), [obnam](http://obnam.org/), [rdiff-backup](http://www.nongnu.org/rdiff-backup/) and [duplicity](http://duplicity.nongnu.org/))
* bench attic with the noatime patch
* bench dev attic vs dev bup
* bench data removal
* bench encryption
* test data recovery
* run multiple backup runs, on different datasets, on a cleaner environment
* ideally, extend [seivot](http://liw.fi/seivot/) to do all of that

Note that the Burp author already did an impressive comparative benchmark of a bunch of those tools for the [burp2 design paper](http://burp.grke.org/burp2/00contents.html), but it unfortunately doesn't include attic or clear ways to reproduce the results.

[[!tag "backup" "benchmark" "debian" "debian-planet" "review" "software" "tunisie"]]
