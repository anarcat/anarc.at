[[!meta title="Archived a part of my CD collection"]]

After about three days of work, I've finished archiving a part of my
old CD collection. There were about 200 CDs in a cardboard box that
were gathering dust. After reading [Jonathan Dowland's post](https://jmtd.net/log/imaging_discs/) about
CD archival, I got (rightly) worried it would be damaged beyond rescue
so I sat down and did some [[research|services/archive/rescue]] on the
rescue mechanisms. My notes are in [[services/archive/rescue]] and I
hope to turn this into a more agreeable LWN article eventually.

I post this here so I can put a note in the box with a permanent URL
for future reference as well.

Remaining work
==============

All the archives created were dumped in the `~/archive` or `~/mp3`
directories on [[hardware/curie]]. Data needs to be deduplicated,
replicated, and archived *somewhere* more logical.

Inventory
=========

I have a bunch of piles:

 * a spindle of disks that consists mostly of TV episodes, movies,
   distro and Windows images/ghosts. not imported.
 * a pile of tapes and Zip drives. not imported.
 * about fourty backup disks. not imported.
 * about five "books" disks of various sorts. ISOs generated. partly
   integrated in my collection, others failed to import or were in
   formats that were considered non-recoverable
 * a bunch of orange seeds piles
   * Burn Your TV masters and copies
   * apparently live and unique samples - mostly imported in `mp3`
   * really old stuff with tons of dupes - partly sorted through, in
     `jams4`, reste still in the pile
 * a pile of unidentified disks

All disks were eventually identified as **trash**, **blanks**,
**perfect**, **finished**, **defective**, or **not processed**. A
special **needs attention** stack was the "to do" pile, and would get
sorted through the other piles. each pile was labeled with a sticky
note and taped together summarily.

A post-it pointing to the [[blog post|blog/2018-10-11-cd-archive]] was
included in the box, along with a printed version of the blog post
summarizing a snapshot of this inventory.

Here is a summary of what's in the box.

| Type | Count | Note |
| ---- | ----- | ---- |
| **trash** | 13 | non-recoverable. not detected by the Linux kernel at all and no further attempt has been made to recover them. |
| **blanks** | 3 | never written to, still usable |
| **perfect** | 28 | successfully archived, without errors |
| **finished** | 4 | almost perfect: but mixed-mode or multi-session |
| **defective** | 21 | found to have errors but not considered important enough to re-process |
| **total** | 69 | |
| **not processed** | ~100 | visual estimate |

[[!tag debian-planet archive short debian data rescue hardware geek documentation meta]]
