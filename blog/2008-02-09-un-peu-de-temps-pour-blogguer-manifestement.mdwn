<!-- Drupal node 126 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Un peu de temps pour blogguer... manifestement"]]
[[!meta date="2008-02-09T13:26:09-0500"]]
[[!meta updated="2012-12-14T22:45:09-0500"]]
[[!meta guid="126 at http://anarcat.koumbit.org"]]

J'ai enfin eu un peu de temps (un samedi!) pour mettre à jour ce blog.. Trop occupé par la "vie normale" (si je peux appeler ça comme tel), je n'ai que peu de temps à passer à mettre à jour ce blog, pour deux raisons: (1) il y a seulement 24h dans une journée et une partie significative de ce temps doit être passée à dormir, manger et (dans mon cas) travailler comme un malade sur mon [projet encore favori](http://koumbit.org/) et (2) après 40h devant un écran d'ordinateur, je développe des migraines.

Pour vous donner une idée, ces deux dernières semaines, j'ai "punché" 41h de travail par semaine. J'ai dit "punché", parce que c'est ce que les rapports disent: j'ai probablement passé plus de temps devant l'ordinateur à "jouer" (comme les 3 heures que j'ai passés pour écrire le dernier billet sur les coupures réseau). Et j'ai tellement de choses que je voudrais vous parler ici, ça me prendrait un dictaphone... ou un de ces logiciels magiques qui comprennent quand on parle... ce qui n'existe malheureusement pas vraiment en logiciel libre, avec tout le respect que je dois au projet [CMU Sphinx](http://cmusphinx.sourceforge.net/html/cmusphinx.php), je n'ai jamais réussi à le faire fonctionner...

[[!tag "vraie vie" "meta"]]