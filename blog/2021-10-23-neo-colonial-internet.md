[[!meta title="The Neo-Colonial Internet"]]

> This article was [[translated into
> french|2021-12-22-internet-néocolonial]]. An interview about this
> article was also [published in Slovene](https://radiostudent.si/druzba/tehno-klistir/koncne-meje-neokolonialnega-interneta).

I grew up with the Internet and its ethics and politics have always
been important in my life. But I have also been involved at other
levels, [against police brutality][], for [Food, Not Bombs][],
[worker autonomy][koumbit], [software freedom][], etc. For a long time,
that all seemed coherent.

[software freedom]: https://www.debian.org/
[Food, Not Bombs]: https://foodnotbombs.net/
[against police brutality]: https://cobp.resist.ca/
[koumbit]: https://www.koumbit.org/
[declaration of independence of cyberspace]: https://www.eff.org/cyberspace-independence

But the more I look at the modern Internet -- and the
mega-corporations that control it -- and the less confidence I have in
my original political analysis of the liberating potential of
technology. I have come to believe that most of our technological
development is harmful to the large majority of the population of the
planet, and of course the rest of the biosphere. And now I feel this
is not a new problem.

This is because the Internet is a neo-colonial device, and has been
from the start. Let me explain.

[[!toc]]

# What is Neo-Colonialism?

The term "[neo-colonialism][]" was coined by [Kwame Nkrumah][],
first president of [Ghana][]. In *Neo-Colonialism, the Last Stage of
Imperialism* (1965), he wrote:

[Ghana]: https://en.wikipedia.org/wiki/Ghana
[Kwame Nkrumah]: https://en.wikipedia.org/wiki/Kwame_Nkrumah
[neo-colonialism]: https://en.wikipedia.org/wiki/Neocolonialism

> In place of colonialism, as the main instrument of imperialism, we
> have today neo-colonialism ... [which] like colonialism, is an
> attempt to export the social conflicts of the capitalist
> countries. ...
>
> The result of neo-colonialism is that foreign capital is used for
> the exploitation rather than for the development of the less
> developed parts of the world. Investment, under neo-colonialism,
> increases, rather than decreases, the gap between the rich and the
> poor countries of the world.

So basically, if [colonialism][] is Europeans bringing genocide, war,
and its religion to the Africa, Asia, and the Americas,
neo-colonialism is the Americans (note the "n") bringing capitalism to
the world.

[colonialism]: https://en.wikipedia.org/wiki/Colonialism

Before we see how this applies to the Internet, we must therefore make
a detour into US history. This matters, because anyone would be
hard-pressed to decouple neo-colonialism from the empire under which
it evolves, and here we can only name the United States of America.

# US Declaration of Independence

Let's start with the [United States declaration of independence][]
(1776). Many Americans may roll their eyes at this, possibly because
that declaration is not actually part of the [US constitution][] and
therefore may have questionable legal standing. Still, it was
obviously a driving philosophical force in the founding of the
nation. As its author, Thomas Jefferson, stated:

[US constitution]: https://en.wikipedia.org/wiki/Constitution_of_the_United_States
[United States declaration of independence]: https://en.wikipedia.org/wiki/United_States_Declaration_of_Independence

> it was intended to be an expression of the American mind, and to
> give to that expression the proper tone and spirit called for by the
> occasion

In that aging document, we find the following pearl:

> We hold these truths to be self-evident, that all men are created
> equal, that they are endowed by their Creator with certain
> unalienable Rights, that among these are Life, Liberty and the
> pursuit of Happiness.

As a founding document, the Declaration still has an impact in the
sense that the above quote has been called an:

> "immortal declaration", and "perhaps [the] single phrase" of the
> American Revolutionary period with the greatest "continuing
> importance." ([Wikipedia][])

[Wikipedia]: https://en.wikipedia.org/wiki/All_men_are_created_equal

Let's read that "immortal declaration" again: "all *men* are created
equal". "Men", in that context, is limited to a certain number of
people, namely "[property-owning or tax-paying white males, or about
6% of the population][]". Back when this was written, women didn't
have the right to vote, and slavery was legal. Jefferson himself owned
hundreds of slaves.

[property-owning or tax-paying white males, or about 6% of the population]: https://en.wikipedia.org/wiki/Voting_rights_in_the_United_States#Milestones_of_national_franchise_changes

The declaration was aimed at the [King][] and was a [list of
grievances][]. A concern of the colonists was that the King:

[list of grievances]: https://en.wikipedia.org/wiki/Grievances_of_the_United_States_Declaration_of_Independence
[King]: https://en.wikipedia.org/wiki/George_III_of_the_United_Kingdom

> has excited domestic insurrections amongst us, and has endeavoured
> to bring on the inhabitants of our frontiers, the merciless Indian
> Savages whose known rule of warfare, is an undistinguished
> destruction of all ages, sexes and conditions.

This is a clear mark of the [frontier myth][] which paved the way for
the US to exterminate and colonize the territory some now call the
United States of America.

[frontier myth]: https://en.wikipedia.org/wiki/Frontier_myth

The declaration of independence is obviously a colonial document,
having being written by colonists. None of this is particularly
surprising, historically, but I figured it serves as a good reminder
of where the Internet is coming from, since it was born in the US.

# A Declaration of the Independence of Cyberspace

Two hundred and twenty years later, in 1996, [John Perry Barlow][]
wrote a [declaration of independence of cyberspace][declaration of independence of cyberspace].  At this
point, ([almost][]) everyone has a right to vote (including women),
slavery was abolished (although [some argue it still exists in the
form of the prison system][]); the US has made tremendous
progress. Surely this text will have aged better than the previous
declaration it is obviously derived from. Let's see how it reads today
and how it maps to how the Internet is actually built now.

[some argue it still exists in the form of the prison system]: https://en.wikipedia.org/wiki/Angela_Davis#Political_activism_and_speeches
[almost]: https://en.wikipedia.org/wiki/Voting_rights_in_the_United_States
[John Perry Barlow]: https://en.wikipedia.org/wiki/John_Perry_Barlow_

# Borders of Independence

One of the key ideas that Barlow brings up is that "*cyberspace does
not lie within your borders*". In that sense, cyberspace is the [final
frontier][]: having failed to colonize the moon, Americans turn
inwards, deeper into technology, but still in the frontier
ideology. And indeed, Barlow is one of the co-founder of the
Electronic *Frontier* Foundation (the beloved [EFF][]), founded six
years prior.

[EFF]: https://en.wikipedia.org/wiki/Electronic_Frontier_Foundation
[final frontier]: https://en.wikipedia.org/wiki/Final_Frontier

But there are other problems with this idea. As [Wikipedia quotes][]:

[Wikipedia quotes]: https://en.wikipedia.org/wiki/A_Declaration_of_the_Independence_of_Cyberspace#Critical_response

> The declaration has been criticized for internal
> inconsistencies.[[9][]] The declaration's assertion that
> 'cyberspace' is a place removed from the physical world has also
> been challenged by people who point to the fact that the Internet is
> always linked to its underlying geography.[[10][]]

[10]: https://doi.org/10.1111%2Fgeoj.12009
[9]: https://web.archive.org/web/20161105174638/http://ieet.org/index.php/IEET/more/evans0220

And indeed, the Internet is definitely a physical object. First
controlled and severely restricted by "telcos" like AT&T, it was
somewhat "liberated" from that monopoly in 1982 when an [anti-trust
lawsuit][] [broke up the monopoly][], a key historical event that,
one could argue, made the Internet possible.

[broke up the monopoly]: https://en.wikipedia.org/wiki/Breakup_of_the_Bell_System
[anti-trust lawsuit]: https://en.wikipedia.org/wiki/United_States_v._AT%26T

(From there on, "backbone" providers could start competing and emerge,
and eventually coalesce into new monopolies: Google has a monopoly on
search and advertisement, Facebook on communications for a few
generations, Amazon on storage and computing, Microsoft on hardware,
etc. Even AT&T is now pretty much as consolidated as it was
before.)

The point is: all those companies have gigantic data centers and
intercontinental cables. And those are definitely prioritizing the
western world, the heart of the empire. Take for example [Google's
latest 3,900 mile undersea cable][]: it does not connect Argentina to
South Africa or New Zealand, it connects the US to UK and
Spain. Hardly a revolutionary prospect.

[Google's latest 3,900 mile undersea cable]: https://www.cnet.com/tech/google-finishes-3900-mile-subsea-cable-connecting-us-to-uk-and-spain/

# Private Internet

But back to the Declaration:

> Do not think that you can build it, as though it were a public
> construction project. You cannot. It is an act of nature and it
> grows itself through our collective actions.

In Barlow's mind, the "public" is bad, and private is good,
natural. Or, in other words, a "public construction project" is
unnatural. And indeed, the modern "nature" of development is private:
most of the Internet is now privately owned and operated.

I must admit that, as an anarchist, I loved that sentence when I read
it. I was rooting for "us", the underdogs, the revolutionaries. And,
in a way, I still do: I am on the board of [Koumbit][] and work for a
[non-profit][] that has pivoted towards censorship and surveillance
evasion. Yet I cannot help but think that, as a whole, we have failed
to establish that independence and put too much trust in private
companies. It is obvious in retrospect, but it was not, 30 years
ago.

[non-profit]: https://www.torproject.org/

Now, the infrastructure of the Internet has zero accountability to
traditional political entities supposedly representing the people, or
even its users. The situation is actually *worse* than when the US was
founded (e.g. "6% of the population can vote"), because the owners of the
tech giants are only a handful of people who can override any
decision. There's only one Amazon CEO, he's called [Jeff Bezos][], and he
has total control. (Update: Bezos actually ceded the CEO role to [Andy
Jassy][], AWS and Amazon music founder, while remaining [executive
chairman][]. I would argue that, as the founder and the richest man
on earth, he still has strong control over Amazon.)

[executive chairman]: https://en.wikipedia.org/wiki/Chairperson#Executive_chairperson
[Andy Jassy]: https://en.wikipedia.org/wiki/Andy_Jassy
[Jeff Bezos]: https://en.wikipedia.org/wiki/Jeff_Bezos

# Social Contract

Here's another claim of the Declaration:

> We are forming our own Social Contract.

I remember the early days, back when "[netiquette][]" was a word, it
did feel we had some sort of a contract. Not written in standards of
course -- or barely (see [RFC1855][]) -- but as a tacit
agreement. How wrong we were. One just needs to look at Facebook to
see how problematic that idea is on a global network.

[RFC1855]: https://datatracker.ietf.org/doc/html/rfc1855
[netiquette]: https://en.wikipedia.org/wiki/Etiquette_in_technology#Netiquette

Facebook is the quintessential "hacker" ideology put in practice. Mark
Zuckerberg [explicitly refused][] to be "arbiter of truth" which
implicitly means he will let lies take over its platforms. 

[explicitly refused]: https://www.theguardian.com/technology/2020/may/28/zuckerberg-facebook-police-online-speech-trump

He also sees Facebook as place where everyone is equal, something
that echoes the Declaration:

> We are creating a world that all may enter without privilege or
> prejudice accorded by race, economic power, military force, or
> station of birth.

(We note, in passing, the omission of gender in that list, also
mirroring the infamous "All men are created equal" claim of the US
declaration.)

As the Wall Street Journal's (WSJ) [Facebook files][] later shown,
both of those "contracts" have serious limitations inside Facebook. There are
[VIPs who systematically bypass moderation systems][] including
[fascists][] and [rapists][]. Drug cartels and human traffickers
[thrive on the platform][]. Even when Zuckerberg himself tried to
tame the platform -- to get people [vaccinated][] or to make it
[healthier][] -- he failed: "vaxxer" conspiracies multiplied and
Facebook got *angrier*.

[healthier]: https://www.wsj.com/articles/facebook-algorithm-change-zuckerberg-11631654215?mod=article_inline
[vaccinated]: https://www.wsj.com/articles/facebook-mark-zuckerberg-vaccinated-11631880296?mod=article_inline
[thrive on the platform]: https://www.wsj.com/articles/facebook-drug-cartels-human-traffickers-response-is-weak-documents-11631812953?mod=article_inline
[rapists]: https://en.wikipedia.org/wiki/Neymar
[fascists]: https://en.wikipedia.org/wiki/Trumpism
[VIPs who systematically bypass moderation systems]: https://www.wsj.com/articles/facebook-files-xcheck-zuckerberg-elite-rules-11631541353
[Facebook files]: https://www.wsj.com/articles/the-facebook-files-11631713039

This is because the "social contract" behind Facebook and those large
companies is a lie: their concern is profit and that means
advertising, "engagement" with the platform, which [causes increased
anxiety and depression in teens][], for example.

[causes increased anxiety and depression in teens]: https://www.bbc.com/news/technology-58570353

Facebook's response to this is that they are working really hard on
moderation. But the truth is that even that system is severely
skewed. The WSJ showed that Facebook has translators for only 50
languages. It's a [surprisingly hard to count human languages][] but
estimates range the number of distinct languages between 2500
and 7000. So while 50 languages seems big at first, it's actually a
tiny fraction of the human population using Facebook. Taking the first
50 of the [Wikipedia list of languages by native speakers][] we omit
languages like Dutch (52), Greek (74), and Hungarian (78), and that's
just a few random nations picks from Europe.

[Wikipedia list of languages by native speakers]: https://en.wikipedia.org/wiki/List_of_languages_by_number_of_native_speakers
[surprisingly hard to count human languages]: https://www.linguisticsociety.org/content/how-many-languages-are-there-world

As an example, Facebook has trouble moderating even a major language
like Arabic. It censored content from legitimate Arab news sources
when they mentioned the word [al-Aqsa][] because Facebook associates
it with the [al-Aqsa Martyrs' Brigades][] when they were talking
about the [Al-Aqsa Mosque][]... This bias against Arabs also shows
how Facebook reproduces the American colonizer politics.

[Al-Aqsa Mosque]: https://en.wikipedia.org/wiki/Al-Aqsa_Mosque
[al-Aqsa Martyrs' Brigades]: https://en.wikipedia.org/wiki/Al-Aqsa_Martyrs%27_Brigades
[al-Aqsa]: https://en.wikipedia.org/wiki/Al-Aqsa

The WSJ also pointed out that Facebook spends only 13% of its
moderation efforts outside of the US, even if that represents 90% of
its users. Facebook spends three more times moderating on "brand
safety", which shows its priority is not the safety of its users, but
of the advertisers.

# Military Internet

[Sergey Brin][] and [Larry Page][] are the [Lewis and Clark][] of
our generation. Just like the latter were sent by Jefferson (the same)
to declare sovereignty over the entire US west coast, Google declared
sovereignty over all human knowledge, with its mission statement "to
organize the world's information and make it universally accessible
and useful". (It should be noted that Page somewhat [questioned that
mission][] but only because it was not ambitious enough, Google
having "outgrown" it.)

[questioned that mission]: https://www.theguardian.com/technology/2014/nov/03/larry-page-google-dont-be-evil-sergey-brin
[Lewis and Clark]: https://en.wikipedia.org/wiki/Lewis_and_Clark_Expedition
[Larry Page]: https://en.wikipedia.org/wiki/Larry_Page
[Sergey Brin]: https://en.wikipedia.org/wiki/Sergey_Brin

The Lewis and Clark expedition, just like Google, had a scientific
pretext, because that is what you do to colonize a world,
presumably. Yet both men were military and had to receive scientific
training before they left. The [Corps of Discovery][] was made up of
a few dozen enlisted men and a dozen civilians, including [York][] an
African American slave *owned* by Clark and *sold* after the
expedition, with his final fate lost in history.

[Corps of Discovery]: https://en.wikipedia.org/wiki/Corps_of_Discovery

And just like Lewis and Clark, Google has a strong military
component. For example, Google Earth was not originally built at
Google but is the acquisition of a company called Keyhole which had
[ties with the CIA][]. Those ties were [brought inside Google during
the acquisition][]. Google's increasing investment inside the
military-industrial complex eventually led Google to workers
[organizing a revolt][] although it is currently unclear to me how
much Google is involved in the military apparatus. (Update: [this
November 2021 post][] says they "will proudly work with the DoD".)
Other companies,
obviously, do not have such reserve, with Microsoft, Amazon, and
plenty of others happily bidding on military contracts all the time.

[organizing a revolt]: https://en.wikipedia.org/wiki/Google_worker_organization
[brought inside Google during the acquisition]: https://pando.com/2014/03/07/the-google-military-surveillance-complex/
[ties with the CIA]: https://en.wikipedia.org/wiki/Google_Earth#History
[York]: https://en.wikipedia.org/wiki/York_(explorer)
[this November 2021 post]: https://cloud.google.com/blog/topics/inside-google-cloud/update-on-google-clouds-work-with-the-us-government

# Spreading the Internet

I am obviously not the first to identify colonial structures in the
Internet. In an article titled [The Internet as an Extension of
Colonialism][], Heather McDonald correctly identifies fundamental
problems with the "development" of new "markets" of Internet
"consumers", primarily arguing that it creates a [digital divide][]
which creates a "lack of agency and individual freedom":

[digital divide]: https://en.wikipedia.org/wiki/Digital_divide
[The Internet as an Extension of Colonialism]: https://thesecuritydistillery.org/all-articles/the-internet-as-an-extension-of-colonialism

> Many African people have gained access to these technologies but not
> the freedom to develop content such as web pages or social media
> platforms in their own way. Digital natives have much more power and
> therefore use this to create their own space with their own norms,
> shaping their online world according to their own outlook.

But the digital divide is certainly not the worst problem we have to
deal with on the Internet today. Going back to the Declaration, we
originally believed we were creating an entirely new world:

> This governance will arise according to the conditions of our
> world, not yours. Our world is different.

How I [dearly wished that was true][]. Unfortunately, the Internet is
not that different from the offline world. Or, to be more accurate,
the values we have embedded in the Internet, particularly of free
speech absolutism, sexism, corporatism, and exploitation, are now
exploding outside of the Internet, into the "real" world.

[dearly wished that was true]: https://en.wikipedia.org/wiki/Eternal_September

The Internet was built with free software which, fundamentally, was
based on quasi-volunteer labour of an elite force of white men with
obviously too much time on their hands (and also: no children). The
mythical writing of GCC and Emacs by Richard Stallman is a good
example of this, but the entirety of the Internet now seems to be
running on random bits and pieces built by hit-and-run programmers
working on their copious free time. Whenever any of those [fails][],
it can [compromise][] or bring down entire systems. (Heck, I wrote
*this article* on my day off...)

[compromise]: https://lwn.net/Articles/773121/
[fails]: https://www.davidhaney.io/npm-left-pad-have-we-forgotten-how-to-program/

This model of what is fundamentally "cheap labour" is spreading out
from the Internet. Delivery workers are being exploited to the bone by
apps like Uber -- although it should be noted that workers [organise
and fight back][]. Amazon workers are similarly exploited beyond
belief, forbidden to take breaks until they [pee in bottles][], with
[ambulances nearby to carry out the bodies][]. During peak of the
pandemic, workers were being [dangerously exposed to the virus][] in
warehouses. All this while Amazon is basically taking over [the entire
economy][].

[the entire economy]: https://www.vice.com/en/article/7xpgvx/amazons-is-trying-to-control-the-underlying-infrastructure-of-our-economy
[dangerously exposed to the virus]: https://www.politico.eu/article/coronavirus-amazon-employees-rage/
[ambulances nearby to carry out the bodies]: https://www.nytimes.com/2015/08/16/technology/inside-amazon-wrestling-big-ideas-in-a-bruising-workplace.html
[pee in bottles]: https://www.businessinsider.com/amazon-warehouse-workers-have-to-pee-into-bottles-2018-4
[organise and fight back]: https://www.curbed.com/article/nyc-delivery-workers.html

The Declaration culminates with this prophecy:

> We will spread ourselves across the Planet so that no one can arrest
> our thoughts.

This prediction, which first felt revolutionary, is now chilling.

# Colonial Internet

The Internet is, if not neo-colonial, plain colonial. The US colonies
had cotton fields and slaves, we have [disposable cell phones][] and
[Foxconn workers][]. Canada has its [cultural genocide][], Facebook
has his own genocides in [Ethiopia][], [Myanmar][], and mob violence
in [India][]. Apple is at least implicitly accepting the [Uyghur
genocide][]. And just like the slaves of the colony, those atrocities
are what makes the empire run.

[Uyghur genocide]: https://en.wikipedia.org/wiki/Uyghur_genocide
[India]: https://www.buzzfeednews.com/article/pranavdixit/whatsapp-destroyed-village-lynchings-rainpada-india
[Myanmar]: https://www.salon.com/2020/08/20/facebook-is-a-global-threat-to-public-health-avaaz-report-says/
[Ethiopia]: https://www.vice.com/en_us/article/xg897a/hate-speech-on-facebook-is-pushing-ethiopia-dangerously-close-to-a-genocide
[cultural genocide]: https://www.thecanadianencyclopedia.ca/en/article/genocide-and-indigenous-peoples-in-canada
[Foxconn workers]: https://www.theguardian.com/technology/2017/jun/18/foxconn-life-death-forbidden-city-longhua-suicide-apple-iphone-brian-merchant-one-device-extract
[disposable cell phones]: https://www.jacobinmag.com/2015/03/smartphone-usage-technology-aschoff/

The Declaration actually ends like this, a quote which I have in my
[fortune cookies file][]:

[fortune cookies file]: https://anarc.at/fortunes.txt

> We will create a civilization of the Mind in Cyberspace. May it be
> more humane and fair than the world your governments have made
> before.

That is still inspiring to me. But if we want to make "cyberspace"
more humane, we need to decolonize it. Work on cyberpeace instead of
cyberwar. Establish clear code of conduct, discuss ethics, and
question your own privileges, biases, and culture. For me the first
step in decolonizing my own mind is writing this article. [Breaking
up][] [tech monopolies][] might be an important step, but it won't be
enough: we have to do a culture shift as well, and that's the hard
part.

[tech monopolies]: https://www.theguardian.com/business/2018/mar/25/is-it-time-to-break-up-the-tech-giants-such-as-facebook
[Breaking up]: https://lwn.net/Articles/844102/

# Appendix: an apology to Barlow

I kind of feel bad going through Barlow's declaration like this, point
by point. It is somewhat unfair, especially since Barlow passed away a
few years ago and cannot mount a response (even humbly assuming that
he might read this). But then again, he himself recognized he was
a bit too "optimistic" in 2009, [saying][]: "we all get older and
smarter":

[saying]: https://reason.com/2004/08/01/john-perry-barlow-20-2/

> I'm an optimist. In order to be libertarian, you have to be an
> optimist. You have to have a benign view of human nature, to believe
> that human beings left to their own devices are basically good. But
> I'm not so sure about human institutions, and I think the real point
> of argument here is whether or not large corporations are human
> institutions or some other entity we need to be thinking about
> curtailing. Most libertarians are worried about government but not
> worried about business. I think we need to be worrying about
> business in exactly the same way we are worrying about government.

And, in a sense, it was a little naive to expect Barlow to *not* be a
colonist. Barlow is, among many things, a cattle rancher who grew up
on a colonial ranch in Wyoming. The ranch was founded in 1907 by his
great uncle, 17 years after the state joined the Union, and only a
generation or two after the [Powder River War][] (1866-1868) and
[Black Hills War][] (1876-1877) during which the US took over lands
occupied by Lakota, Cheyenne, Arapaho, and other native American
nations, in some of the last major [First Nations Wars][].

[First Nations Wars]: https://en.wikipedia.org/wiki/American_Indian_Wars
[Black Hills War]: https://en.wikipedia.org/wiki/Great_Sioux_War_of_1876
[Powder River War]: https://en.wikipedia.org/wiki/Red_Cloud%27s_War

# Appendix: further reading

There is another article that almost has the same title as this one:
[Facebook and the New Colonialism][]. (Interestingly, the `<title>`
tag on the article is actually "Facebook the Colonial Empire" which I
also find appropriate.) The article is worth reading in full, but I
loved this quote so much that I couldn't resist reproducing it here:

[Facebook and the New Colonialism]: https://www.theatlantic.com/technology/archive/2016/02/facebook-and-the-new-colonialism/462393/

> Representations of colonialism have long been present in digital
> landscapes. (“Even Super Mario Brothers,” the video game designer
> Steven Fox told me last year. “You run through the landscape, stomp
> on everything, and raise your flag at the end.”) But web-based
> colonialism is not an abstraction. The online forces that shape a
> new kind of imperialism go beyond Facebook.

It goes on:

> Consider, for example, digitization projects that focus primarily on
> English-language literature. If the web is meant to be humanity’s new
> Library of Alexandria, a living repository for all of humanity’s
> knowledge, this is a problem. So is the fact that the vast majority of
> Wikipedia pages are about a relatively tiny square of the planet. For
> instance, 14 percent of the world’s population lives in Africa, but
> less than 3 percent of the world’s geotagged Wikipedia articles
> originate there, according to a 2014 Oxford Internet Institute
> report.

And they introduce another definition of Neo-colonialism, while
warning about abusing the word like I am sort of doing here:

> “I’m loath to toss around words like colonialism but it’s hard to
> ignore the family resemblances and recognizable DNA, to wit,” said
> Deepika Bahri, an English professor at Emory University who focuses
> on postcolonial studies. In an email, Bahri summed up those
> similarities in list form:
> 
> 1. ride in like the savior
> 2. bandy about words like equality, democracy, basic rights
> 3. mask the long-term profit motive (see 2 above)
> 4. justify the logic of partial dissemination as better than nothing
> 5. partner with local elites and vested interests
> 6. accuse the critics of ingratitude 
> 
> “In the end,” she told me, “if it isn’t a duck, it shouldn’t quack
> like a duck.”

Another good read is the classic [Code and other laws of
cyberspace][] (1999, [free PDF][]) which is also critical of
Barlow's Declaration. In "Code is law", Lawrence Lessig argues that:

[free PDF]: https://lessig.org/images/resources/1999-Code.pdf
[Code and other laws of cyberspace]: https://lessig.org/product/code

> computer code (or "West Coast Code", referring to Silicon Valley)
> regulates conduct in much the same way that legal code (or "East
> Coast Code", referring to Washington, D.C.) does ([Wikipedia](https://en.wikipedia.org/wiki/Code_and_Other_Laws_of_Cyberspace))

And now it feels like the west coast has won over the east coast, or
maybe it recolonized it. In any case, Internet now [christens
emperors][].

[christens emperors]: https://en.wikipedia.org/wiki/2016_United_States_presidential_election

Update: a colleague pointed me at [this reading list][] which refers
to a lot of interesting people much more qualified than me to speak
about this topic. I defer to their wisdom, in doubt.

[this reading list]: https://beatricemartini.it/blog/decolonizing-technology-reading-list/

[[!tag debian-planet reflexion politics tech diversity ethics internet history colonialism]]

