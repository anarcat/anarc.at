[[!meta title="Last year on this blog"]]

So this blog is now celebrating its 21st birthday (or 20 if you count
from zero, or 18 if you want to be pedantic), and I figured I would do
this yearly thing of reviewing how that went.

# Number of posts

2022 was the [[official 20th
anniversary|blog/2023-01-07-bring-back-blogging]] in any case, and
that was one of my best years on record, with 46 posts, surpassed only
by the noisy 2005 (62) and matching 2006 (46). 2023, in comparison,
was underwhelming: a feeble 11 posts! What happened!

Well, I was busy with other things, mostly away from keyboard, that I
will not bore you with here...

The other thing that happened is that the one-liner I used to collect
stats was broken (it counted folders and other unrelated files) and
wildly overestimated 2022! Turns out I didn't write *that* much then:

    anarc.at$ ls blog | grep '^[0-9][0-9][0-9][0-9].*.md' | sed s/-.*// | sort | uniq -c  | sort -n -k2
         57 2005
         43 2006
         20 2007
         20 2008
          7 2009
         13 2010
         16 2011
         11 2012
         13 2013
          5 2014
         13 2015
         18 2016
         29 2017
         27 2018
         17 2019
         18 2020
         14 2021
         28 2022
         10 2023
          1 2024

But even that is inaccurate because, in ikiwiki, I can tag any page as
being featured on the blog. So we actually need to process the HTML
itself because we don't have much better on hand without going through
ikiwiki's internals:

    anarcat@angela:anarc.at$ curl -sSL https://anarc.at/blog/ | grep 'href="\./' | grep -o 20[0-9][0-9] | sort | uniq -c 
         56 2005
         42 2006
         19 2007
         18 2008
          6 2009
         12 2010
         15 2011
         10 2012
         11 2013
          3 2014
         15 2015
         32 2016
         50 2017
         37 2018
         19 2019
         19 2020
         15 2021
         28 2022
         13 2023

Which puts the top 10 years at:

    $ curl -sSL https://anarc.at/blog/ | grep 'href="\./' | grep -o 20[0-9][0-9] | sort | uniq -c  | sort -nr | head -10
         56 2005
         50 2017
         42 2006
         37 2018
         32 2016
         28 2022
         19 2020
         19 2019
         19 2007
         18 2008

Anyway. 2023 is certainly not a glorious year in that regard, in any case.

# Visitors

In terms of visits, however, we had quite a few hits. According to
[Goatcounter](https://github.com/arp242/goatcounter), I had 122 300 visits in 2023! 2022, in comparison,
had 89 363, so that's quite a rise.

## What you read

I seem to have hit the Hacker News front page at least twice. I say
"seem" because it's actually pretty hard to tell *what* the HN
frontpage actually is on any given day. I had 22k visits on
2023-03-13, in any case, and you [can't see me on the front that
day](https://news.ycombinator.com/front?day=2023-03-13). We do see a post of mine [on 2023-09-02](https://news.ycombinator.com/front?day=2023-09-02), all the way down
there, which seem to have generated another 10k visits.

In any case, here were the most popular stories for you fine visitors:

 * [[Framework 12th gen laptop
   review|hardware/laptop/framework-12th-gen]]: 24k visits, which is
   surprising for a 13k words article "without images", as some
   critics have complained. 15k referred by Hacker News. Good
   reference and time-consuming benchmarks, slowly bit-rotting.

   That is, by far, my most popular article ever. A popular article in
   2021 or 2022 was around 6k to 9k, so that's a big one. I suspect it
   will keep getting traffic for a long while.

 * [[Calibre replacement considerations|software/desktop/calibre]]:
   15k visits, most of which without a referrer. Was actually an old
   article, but I suspect HN brought it back to light. I keep updating
   that wiki page regularly when I find new things, but I'm still
   using Calibre to import ebooks.

 * [[Hacking my Kobo Clara HD|hardware/tablet/kobo-clara-hd]]: is not
   new but always gathering more and more hits, it had 1800 hits in
   the first year, 4600 hits last year and now brought 6400 visitors
   to the blog! Not directly related, but [this iFixit
   battery replacement guide I wrote](https://www.ifixit.com/Guide/Kobo+Glo+HD+eReader+Battery+Replacement/143903) also seem to be quite popular

Everything else was published before 2023. [Replacing Smokeping with
Prometheus](https://anarc.at/blog/2020-06-04-replacing-smokeping-prometheus) is still around and [[Looking at Wayland terminal
emulators|software/desktop/wayland]] makes an entry in the top five.

## Where you've been

People send less and less private information when they browse the
web. The number of visitors without referrers was 41% in 2021, it rose
to 44% in 2023. Most of the remaining traffic comes from Google, but
Hacker News is now a significant chunk, almost as big as Google.

In 2021, Google represented 23% of my traffic, in 2022, it was down to
15% so 18% is actually a rise from last year, even if it seems much
smaller than what I usually think of.

| Ratio | Referrer              | Visits |
|-------|-----------------------|--------|
| 18%   | Google                | 22 098 |
| 13%   | Hacker News           | 16 003 |
| 2%    | duckduckgo.com        | 2 640  |
| 1%    | community.frame.work  | 1 090  |
| 1%    | missing.csail.mit.edu | 918    |

Note that Facebook and Twitter do not appear at all in my referrers.

## Where you are

Unsurprisingly, most visits still come from the US:

| Ratio | Country        | Visits |
|-------|----------------|--------|
| 26%   | United States  | 32 010 |
| 14%   | France         | 17 046 |
| 10%   | Germany        | 11 650 |
| 6%    | Canada         | 7 425  |
| 5%    | United Kingdom | 6 473  |
| 3%    | Netherlands    | 3 436  |

Those ratios are nearly identical to last year, but quite different
from 2021, where Germany and France were more or less reversed.

Back in 2021, I mentioned there was a long tail of countries with at
least one visit, with 160 countries listed. I expanded that and
there's now 182 countries in that list, almost all of the 193 member
states in the UN.

## What you were

Chrome's dominance continues to expand, even on readers of this blog,
gaining two percentage points from Firefox compared to 2021.

| Ratio | Browser | Visits |
|-------|---------|--------|
| 49%   | Firefox | 60 126 |
| 36%   | Chrome  | 44 052 |
| 14%   | Safari  | 17 463 |
| 1%    | Others  | N/A    |

It seems like, unfortunately, my Lynx and Haiku users have not visited
in the past year. It seems like trying to read those metrics is like
figuring out tea leaves...

In terms of operating systems:

| Ratio | OS      | Visits |
|-------|---------|--------|
| 28%   | Linux   | 34 010 |
| 23%   | macOS   | 28 728 |
| 21%   | Windows | 26 303 |
| 17%   | Android | 20 614 |
| 10%   | iOS     | 11 741 |

Again, Linux and Mac are over-represented, and Android and iOS are
under-represented.

# What is next

I hope to write more next year. I've been thinking about a few posts I
could write for work, about how things work behind the scenes at Tor,
that could be informative for many people. We run a rather old setup,
but things hold up pretty well for what we throw at it, and it's worth
sharing that with the world...

So anyway, thanks for coming, faithful reader, and see you in the
coming 2024 year...

[[!tag debian-planet python-planet gloating meta stats]]


<!-- posted to the federation on 2024-01-09T15:38:17.848871 -->
[[!mastodon "https://kolektiva.social/@Anarcat/111727915674900043"]]
