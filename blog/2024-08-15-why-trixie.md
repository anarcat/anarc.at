[[!meta title="Why I should be running Debian unstable right now"]]

So a common theme on the Internet about Debian is [so old](https://workaround.org/debian-packages-are-so-old/). And
right, I am getting close to the stage that I feel a little laggy: I
am using a bunch of backports for packages I need, and I'm missing a
bunch of *other* packages that just landed in unstable and didn't make
it to backports for various reasons.

I disagree that "old" is a bad thing: we definitely run Debian stable
on a fleet of about 100 servers and can barely keep up, I would make
it *older*. And "old" is a good thing: (port) wine and (any) beer
needs time to age properly, and so do humans, although some humans
never seem to grow old enough to find wisdom.

But at this point, on my laptop, I am feeling like I'm [missing
out](https://en.wikipedia.org/wiki/Fear_of_missing_out). This page, therefore, is an evolving document that is a twist
on the classic [NewIn](https://wiki.debian.org/NewIn) game. Last time I played seems to be
[[#newinwheezy|2013-05-01-new-debian-release-my-contributions-newinwheezy-game]]
(2013!), so really, I'm due for an update. (To be fair to myself, I do
keep tabs on upgrades quite well at [[home|services/upgrades]] and
[work](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades), which do have their share of "new in", just after the fact.)

[[!toc]]

# New packages to explore

Those tools are shiny new things available in unstable or perhaps
Trixie (testing) already that I am not using yet, but I find
interesting enough to list here.

- [backdown](https://github.com/Canop/backdown/): clever file deduplicator
- [broot](https://dystroy.org/broot/): a TUI file manager with `ncdu` and `magit`-like features
- [codesearch](https://code.google.com/p/codesearch/): search all of Debian's source code (tens of
  thousands of packages) from the commandline! (see also [dcs-cli](https://github.com/jwilk/dcs-cli),
  not in Debian)
- [dasel](https://github.com/tomwright/dasel): JSON/YML/XML/CSV parser, similar to jq, but different
  syntax, not sure I'd grow into it, but often need to parse YML like
  JSON and failing
- [gomuks](https://github.com/tulir/gomuks/) and [ement.el](https://github.com/alphapapa/ement.el): new Matrix clients
- [fyi](https://codeberg.org/dnkl/fyi): notify-send replacement
- [git-subrepo](https://github.com/ingydotnet/git-subrepo): git-submodule replacement I am considering
- [gpg-from-sq](https://packages.debian.org/unstable/gpg-from-sq): Sequoia (Rust!) wrapper for GnuPG, might be able
  to replace everything but `gpg-agent`! currently at least missing
  [send-keys](https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg/-/issues/91), [card-status](https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg/-/issues/100), [performance improvements on
  key listings](https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg/-/issues/83), and [quick-gen-key](https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg/-/issues/82), but those can all be
  accessed through the `gpg-from-gpg`, and all work in progress
- [gtklock](https://github.com/jovanlanik/gtklock): swaylock replacement with bells and whistles,
  particularly interested in showing time, battery and so on
- [hyprland](https://hyprland.org): possible Sway replacement, but there are [rumors of a
  toxic community](https://drewdevault.com/2023/09/17/Hyprland-toxicity.html) ([rebuttal](https://blog.vaxry.net/articles/2023-hyprlandsCommunity), I haven't reviewed either in
  detail), so approach carefully)
- [kooha](https://github.com/SeaDve/Kooha): simple screen recorder with audio support, currently
  using `wf-recorder` which is a more.. minimalist option
- [linescroll](https://www.usenix.org.uk/content/linescroll.html): rate graphs on live logs, mostly useful on servers
  though
- [memray](https://bloomberg.github.io/memray/): Python memory profiler
- [ruff](https://github.com/charliermarsh/ruff/): faster Python formatter and linter, flake8/black/isort
  replacement, alas not mypy/LSP unfortunately, designed to be ran
  *alongside* such a tool, which is [not possible in Emacs eglot right
  now](https://github.com/joaotavora/eglot/discussions/1429#discussioncomment-10149990), but [is possible in lsp-mode](https://github.com/emacs-lsp/lsp-mode/pull/469)
- [sfwbar](https://github.com/LBCrion/sfwbar): pretty status bar, may replace waybar, which i am
  somewhat unhappy with (my UTC clock disappears randomly)
- [shoutidjc](https://idjc.sourceforge.io/index.html): streaming workstation, currently using [butt](https://tracker.debian.org/pkg/butt) but
  it doesn't support HTTPS correctly (update: not so exciting, no GUI,
  no great error reporting - failed to fail with incorrect password)
- [spytrap-adb](https://github.com/spytrap-org/spytrap-adb): cool spy gear
- [syslog-summary](https://github.com/shtrom/syslog-summary): log summarizer, one of many of the kind, but
  made by Lars, so it must be good
- [trippy](https://github.com/fujiapple852/trippy): trippy network analysis tool, kind of an improved MTR
- [yubikey-touch-detector](https://github.com/maximbaz/yubikey-touch-detector): notifications for when I need to touch
  my YubiKey

# New packages I won't use

Those are packages that I *have* tested because I found them
interesting, but ended up not using, but I think people could find
interesting anyways.

- [kew](https://github.com/ravachol/kew): surprisingly fast music player, parsed my entire library
  (which is huge) instantaneously and just started playing (I still
  use [Supersonic](https://github.com/dweymouth/supersonic/), for which I maintain a [flatpak](https://github.com/flathub/io.github.dweymouth.supersonic) on my
  [Navidrome](https://www.navidrome.org/) server)
- [mdformat](https://tracker.debian.org/mdformat): good markdown formatter, think `black` or `gofmt` but
  for markdown), but it [didn't actually do what I needed](https://github.com/executablebooks/mdformat/issues/420), and
  it's not quite as opinionated as it should (or could) be)

# Backports already in use

Those are packages I already use regularly, which have backports or
that can just be installed from unstable:

- [asn](https://github.com/nitefood/asn/): IP address forensics
- [diffr](https://github.com/mookid/diffr): improved git diffs - i typically have this in magit, but
  this is useful when calling `git diff` directly in a shell, which i
  still do sometimes - [riff](https://github.com/walles/riff) is similar but not in Debian.
  there's also [git-delta](https://github.com/dandavison/delta) that's only in trixie, but it also
  [supports ripgrep](https://dandavison.github.io/delta/grep.html) which is pretty cool
- [markdownlint](https://github.com/markdownlint/markdownlint): markdown linter, I use that *a lot*
- [poweralertd](https://sr.ht/~kennylevinsen/poweralertd): pops up "your battery is almost empty" messages
- [sway-notification-center](https://github.com/ErikReider/SwayNotificationCenter): used as part of my status bar, yet another status bar
  basically, a little noisy, stuck in a libc dep update
- [tailspin](https://github.com/bensadeh/tailspin): used to color logs

# Out of date packages

Those are packages that *are* in Debian stable (Bookworm) already, but
that are somewhat lacking and could benefit from an upgrade.

- [firmware-iwlwifi](https://git.kernel.org/cgit/linux/kernel/git/firmware/linux-firmware.git): out of date, can install from unstable
- [fuzzel][]: [log level noises][A], paste support
  and my [scripts](https://gitlab.com/anarcat/scripts/) in `~/bin` should be patched to use `--cache`
- [foot][]: [log level noises][B], [quotes selection][], [keyboard
  selection mode](https://codeberg.org/dnkl/foot/issues/419)
- nomacs: non-trivial backport, out of date version in sid
- [pandoc](https://pandoc.org/): [3.0](https://github.com/jgm/pandoc/releases/tag/3.0) is ridiculously huge, but particularly [remove
  spaces after list marker](https://github.com/jgm/pandoc/issues/7172)
- [podman](https://github.com/containers/podman): [better systemd integration](https://www.redhat.com/sysadmin/quadlet-podman)
- [pubpaste](https://gitlab.com/anarcat/pubpaste/): out of date, can install from unstable
- [tremotesf](https://github.com/equeim/tremotesf2): out of date backport
- [undertime](https://gitlab.com/anarcat/undertime): out of date, can install from unstable
- [yt-dlp](https://github.com/yt-dlp/yt-dlp): out of date, can install from unstable

[fuzzel]: https://codeberg.org/dnkl/fuzzel
[foot]: https://codeberg.org/dnkl/foot
[A]: https://codeberg.org/dnkl/fuzzel/pulls/266
[B]: https://codeberg.org/dnkl/foot/pulls/1215
[quotes selection]: https://codeberg.org/dnkl/foot/issues/1364

# Last words

If you know of cool things I'm missing out of, then by all means let
me know!

That said, overall, this is a pretty short list! I have most of what I
need in stable right now, and if I wasn't a Debian developer, I don't
think I'd be doing the jump now. But considering how easier it is to
develop Debian (and how important it is to test the next release!),
I'll probably upgrade soon.

Previously, I was running Debian testing (which why the slug on that
article is `why-trixie`), but now I'm actually considering just
running unstable on my laptop directly anyways. It's been a long time
since we had any significant instability there, and I can typically
deal with whatever happens, except maybe when I'm traveling, and then
it's easy to prepare for that (just pin testing).

[[!tag debian-planet python-planet debian packaging upgrade]]


<!-- posted to the federation on 2024-08-15T23:41:37.435834 -->
[[!mastodon "https://kolektiva.social/@Anarcat/112969626239564922"]]
