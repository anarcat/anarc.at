[[!meta title="Mastodon comments in ikiwiki"]]

Today I noticed bounces in my mail box. They were from ikiwiki trying
to send registration confirmation email to users who probably never
asked for it. 

I'm getting truly fed up with spam in my wiki. At this point, all
comments are manually approved and I still get trouble: now it's
scammers spamming the registration form with dummy accounts, which
bounce back to me when I make new posts, or just generate backscatter
spam for the confirmation email. It's really bad. I have hundreds of
users registered on my blog, and I don't know which are spammy, which
aren't. So. I'm considering ditching [ikiwiki comments](https://ikiwiki.info/plugins/comments/) altogether.

I am testing Mastodon as a commenting platforms. Others
(e.g. [JAK](https://blog.jak-linux.org/2018/10/13/new-blog/)) have implemented this [as a server](https://github.com/julian-klode/mastodon-comments) but a simpler
approach is toload them dynamically from Mastodon, which is what [Carl
Shwan has done](https://carlschwan.eu/2020/12/29/adding-comments-to-your-static-blog-with-mastodon/). They are using Hugo, however, so they can easily
embed page metadata in the template to load the right server with the
right comment ID.

I wasn't sure how to do this in ikiwiki: it's typically hard to access
page-specific metadata in templates. Even the [page name is not
there](https://ikiwiki.info/todo/include_page_variable_in_base_templates/), for example.

I have tried using [templates](https://ikiwiki.info/templates/), and that (obviously?) fails because
the `<script>` stuff gets sanitized away. It seems I would need to
split the JavaScript out of the template into a base template and then
make the page template refer to a function in there. It's kind of
horrible and messy.

I wish there was a way to just access page metadata from the page
template itself... I found out the [meta plugin](https://ikiwiki.info/plugins/meta/) passes along its
metadata, but that's not (easily) extensible. So i'd need to either
patch that module, and my history of [merged patches](https://ikiwiki.info/users/anarcat/) is not great
so far.

So: another plugin.

I have something that kind of works that's a combination of a
page.tmpl patch and a plugin. The plugin adds a `mastodon`
directive that feeds the `page.tmpl` with the right stuff. On clicking
a button, it injects comments from the Mastodon API, with a JavaScript
callback. It's not pretty (it's not themed at all!), but it works. 

If you want to do this at home, you need [this page.tmpl](https://gitlab.com/anarcat/ikiwiki-bootstrap-anarcat/-/blob/01524241a1346211bad2d681f0ed634af29d5c38/templates/page.tmpl) (or at
least [this patch](https://gitlab.com/anarcat/ikiwiki-bootstrap-anarcat/-/commit/491dd6bfba3bee3ce702545e7f445e227dc66b30) and [that one](https://gitlab.com/anarcat/ikiwiki-bootstrap-anarcat/-/commit/c0abd79e53b8fee60cf4582971df35e6feb957fd)) and the [mastodon.pm
plugin](https://gitlab.com/anarcat/ikiwiki/-/blob/2400baee8f5bc06f810b5ed96395ee84b05943ed/IkiWiki/Plugin/mastodon.pm) from my [mastodon-plugin](https://gitlab.com/anarcat/ikiwiki/-/tree/mastodon-plugin) branch.

I'm not sure this is a good idea. The first test I did was a "[test
comment](https://kolektiva.social/@Anarcat/109717302329531358)" which led to half a dozen "test reply". I then realized I
couldn't redact individual posts from there. I don't even know if,
when I mute a user, it actually gets hidden from everyone else too...

So I'll test this for a while, I guess.

I have also turned off all CGI on this site. It will keep users from
registering while I cleanup this mess and think about next steps. I
have [other options](https://anarc.at/services/wiki/ikiwiki-hugo-conversion/#comments) as well if push comes to shove, but I'm
unlikely to go back to ikiwiki comments. 

Mastodon comments are nice because they don't require *me* to run any
extra software: either I have my own federated service I reuse, or I
use someone else's, but I don't need to run something *extra*. And, of
course, comments are published in a standard way that's interoperable
with everything...

On the other hand, now I won't have comments enabled until the blog is
posted on Mastodon... Right now this happens only when [feed2exec][]
runs *and* the HTTP cache expires, which can take up to a day. I
should probably do this some other way, like flush the cache when a
new post arrives, or run post-commit hooks, but for now, this will
have to do.

Update: I figured out a way to make this work in a timely manner:

 1. there's a `post-merge` hook in my ikiwiki git repository which
    calls [feed2exec][] in `/home/w-anarcat/source/.git/hooks/` — took
    me a while to find it! I tried `post-update` and `post-receive`
    first, but ikiwiki actually pulls from the bare directory in the
    source directory, so only `post-merge` fires (even though it's not
    a merge)
 2. feed2exec then finds new blog posts (if any!) and fires up the new
    [ikiwikitoot plugin](https://gitlab.com/anarcat/feed2exec/-/blob/871a867a648500439f4e0cd9f54daf7697c88075/feed2exec/plugins/ikiwikitoot.py) which then...
 3. posts the toot using the [toot](https://github.com/ihabunek/toot) command (it just works, why
    reinvent the wheel), keeping the toot URL
 4. finds the Markdown source file associated with the post, and adds
    the magic `mastodon` directive
 5. commits and pushes the result

This will make the interaction with Mastodon much smoother: as soon as
a blog post is out of "draft" (i.e. when it hits the RSS feeds), this
will immediately trigger and post the blog entry to Mastodon, enabling
comments. It's kind of a tangled mess of stuff, but it works!

I have briefly considered *not* using feed2exec for this, but it turns
out it does an important job of parsing the result of ikiwiki's
rendering. Otherwise I would have to guess which post is really a blog
post, is this just an update or is it new, is it a draft, and so
on... all sorts of questions where the business logic already resides
in ikiwiki, and that I would need to reimplement myself.

Plus it goes alongside moving more stuff (like my feed reader) to
dedicated UNIX accounts (in this case, the blog sandbox) for security
reasons. Whee!

[[!tag ikiwiki comments meta debian-planet]]
[[!mastodon "https://kolektiva.social/@Anarcat/109718569330648379"]]

[feed2exec]: https://feed2exec.readthedocs.io/
