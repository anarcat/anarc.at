<!-- Drupal node 51 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Owl's Head en neige"]]
[[!meta date="2005-11-21T23:17:41-0500"]]
[[!meta updated="2012-12-14T22:44:58-0500"]]
[[!meta guid="51 at http://anarcat.koumbit.org"]]

Ce samedi, nous avons grimpé [Owl's Head](http://www.owlshead.com/), recouvert d'une fine couche de neige, du moins à sa base. C'est qu'ils ont commencé la fabrication de neige artificielle en partant du sommet, là où il fait plus froid. Je croyais originalement que la "neige artificielle" était polluante mais en fait, ce n'est même pas de la neige artificielle: c'est de la neige fabriquée, si on peut dire. On voit, quand on marche les montagnes de ski alpin, deux grosses canalisations en métal qui suivent les pistes. Ce sont en fait des tuyaux pour l'air comprimé et l'eau nécessaires à la création de la neige. En bref, finalement, ça ne pollue pas, du moins, à ce qu'en dit [wikipedia](http://fr.wikipedia.org/wiki/Canon_%C3%A0_neige) et [une page expliquant commen la neige est fabriquée](http://www.firsttracksonline.com/snowmaking.htm).

Enfin, Owl's Head est une jolie montagne, surtout pour la vue dont on profite tout au long de la montée, grâce aux larges pistes de ski. C'est un bon côté aux centres de ski: ils donnent un accès facile aux montagnes et nous donnent une vue imprenable sur les environs. Par contre, le contact avec la nature est réduit à néant: les arbres sont de la décoration sur les côtés des pistes et on se demande ce qui empêche les entrepreneurs de réduire ce qui reste à néant.

Owl's head, au moins, nous laisse un petit sommet non-exploité pour nous amuser un peu. Un petit sentier nous redonne le contact avec la nature pour un court moment pour nous donner accès au sommet avec une vue presque complète sur le paysage: Jay Peak qui nous défie,  le petit Pinacle qui pointe son nez familier par dessus l'épaule de Sutton qui dort au loin. Mais c'est évidemment le majustueux Memphrémagog, ce lac immense si beau et mystérieux qui est la composante principale du paysage que nous offre la montée et le sommet.

À la redescente, nous avons pu nous amuser dans la neige dure installée par les canons à neige, pour faire la couche de fond. Très proche de la glace, on a pu y glisser comme si nous avions des luges. Nous avons tenté de continuer l'expérience dans la couche de neige plus légère et naturelle du bas de la montagne et je me suis solidement abîmé le derrière sur un cailloux en chemin, ce qui a un peu refroidi ma folie.

Toujours aussi plaisantes ces sorties, surtout qu'il y a toujours du nouveau monde qui s'y intéresse...

[[!tag "plein air"]]