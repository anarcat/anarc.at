[[!meta title="How big is Debian?"]]

Now [this](https://gwolf.org/2023/09/debian-30-found-the-shirt-i-was-looking-for-last-month.html) was quite a tease! For those who haven't seen it, I
encourage you to check it out, it has a nice photo of a Debian t-shirt
I did not know about, to quote the Fine Article:

> Today, when going through a box of old T-shirts, I found the shirt I
> was looking for to bring to the occasion: [...]
>
> For the benefit of people who read this using a non-image-displaying
> browser or RSS client, they are respectively:
> 
>        10 years
>       100 countries
>      1000 maintainers
>     10000 packages
> 
> and
> 
>             1 project
>            10 architectures
>           100 countries
>          1000 maintainers
>         10000 packages
>        100000 bugs fixed
>       1000000 installations
>      10000000 users
>     100000000 lines of code
> 
> 20 years ago we celebrated eating grilled meat at J0rd1’s
> house. This year, we had vegan tostadas in the menu. And maybe we
> are no longer that young, but we are still very proud and happy of
> our project!
> 
> Now… How would numbers line up today for Debian, 20 years later?
> Have we managed to get the “bugs fixed” line increase by a factor of
> 10? Quite probably, the lines of code we also have, and I can only
> guess the number of users and installations, which was already just
> a wild guess back then, might have multiplied by over 10, at least
> if we count indirect users and installs as well…

Now I don't know about you, but I *really* expected someone to come up
with an answer to this, directly on [Debian Planet](https://planet.debian.org/)! I have
patiently waited for such an answer but enough is enough, I'm a Debian
member, surely I can cull all of this together. So, low and behold,
here are the actual numbers from 2023!

 * 1 project: unchanged, although we could count [129 derivatives in
   the current census](https://wiki.debian.org/Derivatives/Census)
 * ~10 architectures: number almost unchanged, but the actual
   architectures are of course different ([woody released](https://www.debian.org/releases/woody/) with
   `i386`, `m68k`, Alpha, SPARC, PowerPC, ARM, IA-64, `hppa`, `mips`,
   `s390`; while [bookworm released](https://www.debian.org/releases/bookworm/) with actually 9 supported
   architectures instead of 10: `i386`, `amd64`, `aarch64`, `armel`,
   `armhf`, `mipsel`, `mips64el`, `ppc64el`, `s390x`)

 * ~100 countries: actually 63 now, but I suspect we were generously
   rounding up last time as well (extracted with `ldapsearch -b
   ou=users,dc=debian,dc=org -D uid=anarcat,ou=users,dc=debian,dc=org
   -ZZ -vLxW '(c=*)' c | grep ^c: | sort | uniq -c | sort -n | wc -l`
   on `coccia`)

 * ~1000 maintainers: amazingly, almost unchanged (according to the
   last DPL vote, there were 831 DDs in 2003 and 996 in the last vote)

 * 35000 packages: that number obviously increased quite a bit, but
   according to [sources.debian.org](https://sources.debian.org/stats/), woody released with 5580
   source packages and bookworm with 34782 source packages and
   according to [UDD](https://wiki.debian.org/UltimateDebianDatabase), there are actually 200k+ *binary* packages (
   `SELECT COUNT(DISTINCT package) FROM all_packages;` => 211151)

 * 1 000 000+ (OVER ONE MILLION!) bugs fixed! now *that* number grew
   by a whole order of magnitude, incredibly (934809 done, 16 fixed,
   7595 forwarded, 82492 pending, 938 pending-fixed, according to UDD
   again, `SELECT COUNT(id),status FROM all_bugs GROUP BY status;`)

 * ~1 000 000 installations (?): that one is hard to call. [popcon](https://popcon.debian.org/)
   has 225419 recorded installs, but it is likely an underestimate -
   hard to count

 * how many users? even harder, we were claiming ten million users
   then, how many now? how can we even begin to tell, with Debian
   running on the space station?

 * 1 000 000 000+ (OVER ONE BILLION!) lines of code: that,
   interestingly, has *also* grown by an order of magnitude, from 100M
   to 1B lines of code, again according to [sources.debian.org](https://sources.debian.org/stats/),
   woody shipped with 143M lines of codes and bookworm with 1.3
   *billion* lines of code

So it doesn't line up as nicely, but it looks something like this:

             1 project
            10 architectures
            30 years
           100 countries (actually 63, but we'd like to have yours!)
          1000 maintainers (yep, still there!)
         35000 packages
        211000 *binary* packages
       1000000 bugs fixed
    1000000000 lines of code
     uncounted installations and users, we don't track you

So maybe the the more accurate, rounding to the nearest logarithm,
would look something like:

             1 project
            10 architectures
           100 countries (actually 63, but we'd like to have yours!)
          1000 maintainers (yep, still there!)
        100000 packages
       1000000 bugs fixed
    1000000000 lines of code
     uncounted installations and users, we don't track you

I really like how the "packages" and "bugs fixed" still have an order
of magnitude between them there, but that the "bugs fixed" vs "lines
of code" have an extra order of magnitude, that is we have fixed ten
times less bugs per line of code since we last did this count, 20
years ago.

Also, I am tempted to put `100 years` in there, but that would be
rounding up too much. Let's give it another 30 years first.

Hopefully, some real scientist is going to balk at this crude
methodology and come up with some more interesting numbers for the
next t-shirt. Otherwise I'm available for bar mitzvahs and children
parties.

[[!tag debian debian-planet python-planet]]


<!-- posted to the federation on 2023-09-26T22:24:49.829276 -->
[[!mastodon "https://kolektiva.social/@Anarcat/111134735705737208"]]