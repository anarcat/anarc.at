[[!meta title="Nmap traces from the Cuban internet"]]

[[!toc levels=2]]

Portal
======

    [1028]anarcat@angela:~130$ sudo nmap -v -O -A www.portal-wifi-temas.nauta.cu
    
    Starting Nmap 6.47 ( http://nmap.org ) at 2016-01-21 21:50 EST
    NSE: Loaded 118 scripts for scanning.
    NSE: Script Pre-scanning.
    Initiating Ping Scan at 21:50
    Scanning www.portal-wifi-temas.nauta.cu (190.6.81.230) [4 ports]
    Completed Ping Scan at 21:50, 0.22s elapsed (1 total hosts)
    Initiating Parallel DNS resolution of 1 host. at 21:50
    Completed Parallel DNS resolution of 1 host. at 21:50, 0.01s elapsed
    Initiating SYN Stealth Scan at 21:50
    Scanning www.portal-wifi-temas.nauta.cu (190.6.81.230) [1000 ports]
    Discovered open port 443/tcp on 190.6.81.230
    Discovered open port 80/tcp on 190.6.81.230
    Completed SYN Stealth Scan at 21:51, 19.40s elapsed (1000 total ports)
    Initiating Service scan at 21:51
    Scanning 2 services on www.portal-wifi-temas.nauta.cu (190.6.81.230)
    Completed Service scan at 21:51, 1.01s elapsed (2 services on 1 host)
    Initiating OS detection (try #1) against www.portal-wifi-temas.nauta.cu (190.6.81.230)
    Initiating Traceroute at 21:51
    Completed Traceroute at 21:51, 1.14s elapsed
    Initiating Parallel DNS resolution of 1 host. at 21:51
    Completed Parallel DNS resolution of 1 host. at 21:51, 0.01s elapsed
    NSE: Script scanning 190.6.81.230.
    Initiating NSE at 21:51
    Completed NSE at 21:52, 63.56s elapsed
    Nmap scan report for www.portal-wifi-temas.nauta.cu (190.6.81.230)
    Host is up (0.0086s latency).
    rDNS record for 190.6.81.230: mail2.etecsa.net
    Not shown: 998 filtered ports
    PORT    STATE SERVICE VERSION
    80/tcp  open  http?
    443/tcp open  https?
    Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
    Device type: specialized|WAP|phone
    Running: iPXE 1.X, Linksys Linux 2.4.X, Linux 2.6.X, Sony Ericsson embedded
    OS CPE: cpe:/o:ipxe:ipxe:1.0.0%2b cpe:/o:linksys:linux_kernel:2.4 cpe:/o:linux:linux_kernel:2.6 cpe:/h:sonyericsson:u8i_vivaz
    OS details: iPXE 1.0.0+, Tomato 1.28 (Linux 2.4.20), Tomato firmware (Linux 2.6.22), Sony Ericsson U8i Vivaz mobile phone
    
    TRACEROUTE (using port 443/tcp)
    HOP RTT       ADDRESS
    1   25.95 ms  10.156.41.1
    2   25.93 ms  10.156.41.1
    3   23.11 ms  10.156.41.1
    4   21.24 ms  10.156.41.1
    5   18.62 ms  10.156.41.1
    6   11.71 ms  10.156.41.1
    7   10.15 ms  10.156.41.1
    8   17.54 ms  10.156.41.1
    9   7.82 ms   10.156.41.1
    10  14.85 ms  10.156.41.1
    11  103.64 ms 10.156.41.1
    12  7.92 ms   10.156.41.1
    13  10.65 ms  10.156.41.1
    14  14.24 ms  10.156.41.1
    15  84.67 ms  10.156.41.1
    16  76.99 ms  10.156.41.1
    17  8.41 ms   10.156.41.1
    18  9.38 ms   10.156.41.1
    19  11.68 ms  10.156.41.1
    20  14.58 ms  10.156.41.1
    21  10.06 ms  10.156.41.1
    22  7.83 ms   10.156.41.1
    23  8.41 ms   10.156.41.1
    24  9.54 ms   10.156.41.1
    25  49.67 ms  10.156.41.1
    26  62.54 ms  10.156.41.1
    27  37.53 ms  10.156.41.1
    28  46.82 ms  10.156.41.1
    29  10.31 ms  10.156.41.1
    30  9.03 ms   10.156.41.1
    
    NSE: Script Post-scanning.
    Read data files from: /usr/bin/../share/nmap
    OS and Service detection performed. Please report any incorrect results at http://nmap.org/submit/ .
    Nmap done: 1 IP address (1 host up) scanned in 90.33 seconds
               Raw packets sent: 3103 (139.114KB) | Rcvd: 62 (4.474KB)

redirection host
================

    [1039]anarcat@angela:cuba130$ sudo nmap -v -O -A 192.168.134.138
    
    Starting Nmap 6.47 ( http://nmap.org ) at 2016-01-17 10:43 EST
    NSE: Loaded 118 scripts for scanning.
    NSE: Script Pre-scanning.
    Initiating Ping Scan at 10:43
    Scanning 192.168.134.138 [4 ports]
    Completed Ping Scan at 10:43, 0.22s elapsed (1 total hosts)
    Initiating Parallel DNS resolution of 1 host. at 10:43
    Completed Parallel DNS resolution of 1 host. at 10:43, 0.01s elapsed
    Initiating SYN Stealth Scan at 10:43
    Scanning 192.168.134.138 [1000 ports]
    Discovered open port 80/tcp on 192.168.134.138
    Discovered open port 22/tcp on 192.168.134.138
    Discovered open port 443/tcp on 192.168.134.138
    Discovered open port 53/tcp on 192.168.134.138
    Increasing send delay for 192.168.134.138 from 0 to 5 due to 120 out of 398 dropped probes since last increase.
    Increasing send delay for 192.168.134.138 from 5 to 10 due to 21 out of 68 dropped probes since last increase.
    Increasing send delay for 192.168.134.138 from 10 to 20 due to 18 out of 58 dropped probes since last increase.
    Increasing send delay for 192.168.134.138 from 20 to 40 due to 22 out of 73 dropped probes since last increase.
    Increasing send delay for 192.168.134.138 from 40 to 80 due to max_successful_tryno increase to 4
    Completed SYN Stealth Scan at 10:44, 63.12s elapsed (1000 total ports)
    Initiating Service scan at 10:44
    Scanning 4 services on 192.168.134.138
    Service scan Timing: About 75.00% done; ETC: 10:46 (0:00:33 remaining)
    Completed Service scan at 10:46, 111.48s elapsed (4 services on 1 host)
    Initiating OS detection (try #1) against 192.168.134.138
    Initiating Traceroute at 10:46
    Completed Traceroute at 10:46, 0.02s elapsed
    Initiating Parallel DNS resolution of 1 host. at 10:46
    Completed Parallel DNS resolution of 1 host. at 10:46, 0.06s elapsed
    NSE: Script scanning 192.168.134.138.
    Initiating NSE at 10:46
    Completed NSE at 10:47, 30.17s elapsed
    Nmap scan report for 192.168.134.138
    Host is up (0.0083s latency).
    Not shown: 996 closed ports
    PORT    STATE SERVICE   VERSION
    22/tcp  open  ssh       OpenSSH 3.0.2p1 (protocol 1.99)
    |_ssh-hostkey: ERROR: Script execution failed (use -d to debug)
    |_sshv1: Server supports SSHv1
    53/tcp  open  domain
    80/tcp  open  http      G4200.GSI.2.22.0155
    |_http-generator: ERROR: Script execution failed (use -d to debug)
    | http-server-header: Software version grabbed from Server header.
    | Consider submitting a service fingerprint.
    |_Run with --script-args http-server-header.skip
    |_http-title: Did not follow redirect to https://192.168.134.138/logout.user
    443/tcp open  ssl/https G4200.GSI.2.22.0155
    |_http-favicon: Unknown favicon MD5: 403F2997A2BEBD515932426213334F0E
    | http-server-header: Software version grabbed from Server header.
    | Consider submitting a service fingerprint.
    |_Run with --script-args http-server-header.skip
    | http-title: Logout
    |_Requested resource was https://192.168.134.138/logout.user
    | ssl-cert: Subject: organizationName=Gemtek Systems
    | Issuer: organizationName=Gemtek Systems
    | Public Key type: rsa
    | Public Key bits: 1024
    | Not valid before: 2002-10-07T06:46:53+00:00
    | Not valid after:  2019-03-12T06:46:53+00:00
    | MD5:   eea3 78e6 bd78 80aa 562c e808 3e6f b8e4
    |_SHA-1: 7696 a373 c1cf bf3f 11c4 b5f8 0aff 76c2 adcb 167e
    |_ssl-date: 1970-01-01T18:58:36+00:00; -46y15d20h47m56s from local time.
    | sslv2:
    |   SSLv2 supported
    |   ciphers:
    |     SSL2_DES_192_EDE3_CBC_WITH_MD5
    |     SSL2_RC2_CBC_128_CBC_WITH_MD5
    |     SSL2_RC4_128_WITH_MD5
    |     SSL2_RC4_64_WITH_MD5
    |     SSL2_DES_64_CBC_WITH_MD5
    |     SSL2_RC2_CBC_128_CBC_WITH_MD5
    |_    SSL2_RC4_128_EXPORT40_WITH_MD5
    Device type: general purpose
    Running: Linux 2.4.X
    OS CPE: cpe:/o:linux:linux_kernel:2.4
    OS details: Linux 2.4.21 - 2.4.31 (likely embedded)
    Uptime guess: 0.791 days (since Sat Jan 16 15:47:56 2016)
    Network Distance: 1 hop
    TCP Sequence Prediction: Difficulty=203 (Good luck!)
    IP ID Sequence Generation: All zeros
    
    TRACEROUTE (using port 8888/tcp)
    HOP RTT      ADDRESS
    1   14.47 ms 192.168.134.138
    
    NSE: Script Post-scanning.
    Initiating NSE at 10:47
    Completed NSE at 10:47, 0.00s elapsed
    Read data files from: /usr/bin/../share/nmap
    OS and Service detection performed. Please report any incorrect results at http://nmap.org/submit/ .
    Nmap done: 1 IP address (1 host up) scanned in 209.48 seconds
               Raw packets sent: 1688 (76.744KB) | Rcvd: 1356 (58.209KB)

gateway
=======

    [1049]anarcat@angela:cuba$ sudo nmap -v -O -A 10.156.41.1
    
    Starting Nmap 6.47 ( http://nmap.org ) at 2016-01-21 21:52 EST
    NSE: Loaded 118 scripts for scanning.
    NSE: Script Pre-scanning.
    Initiating ARP Ping Scan at 21:52
    Scanning 10.156.41.1 [1 port]
    Completed ARP Ping Scan at 21:52, 0.21s elapsed (1 total hosts)
    Initiating Parallel DNS resolution of 1 host. at 21:52
    Completed Parallel DNS resolution of 1 host. at 21:52, 0.01s elapsed
    Initiating SYN Stealth Scan at 21:52
    Scanning 10.156.41.1 [1000 ports]
    Discovered open port 80/tcp on 10.156.41.1
    Discovered open port 53/tcp on 10.156.41.1
    Discovered open port 23/tcp on 10.156.41.1
    Discovered open port 22/tcp on 10.156.41.1
    Discovered open port 443/tcp on 10.156.41.1
    Increasing send delay for 10.156.41.1 from 0 to 5 due to 140 out of 466 dropped probes since last increase.
    Increasing send delay for 10.156.41.1 from 5 to 10 due to 71 out of 236 dropped probes since last increase.
    Increasing send delay for 10.156.41.1 from 10 to 20 due to 11 out of 35 dropped probes since last increase.
    Increasing send delay for 10.156.41.1 from 20 to 40 due to 11 out of 28 dropped probes since last increase.
    Increasing send delay for 10.156.41.1 from 40 to 80 due to max_successful_tryno increase to 4
    Completed SYN Stealth Scan at 21:53, 47.86s elapsed (1000 total ports)
    Initiating Service scan at 21:53
    Scanning 5 services on 10.156.41.1
    Completed Service scan at 21:55, 113.07s elapsed (5 services on 1 host)
    Initiating OS detection (try #1) against 10.156.41.1
    NSE: Script scanning 10.156.41.1.
    Initiating NSE at 21:55
    Completed NSE at 21:55, 30.21s elapsed
    Nmap scan report for 10.156.41.1
    Host is up (0.0064s latency).
    Not shown: 995 closed ports
    PORT    STATE SERVICE   VERSION
    22/tcp  open  ssh       OpenSSH 3.0.2p1 (protocol 1.99)
    |_ssh-hostkey: ERROR: Script execution failed (use -d to debug)
    |_sshv1: Server supports SSHv1
    23/tcp  open  telnet    Linux telnetd
    53/tcp  open  domain
    80/tcp  open  http      G4200.GSI.2.22.0155
    |_http-generator: ERROR: Script execution failed (use -d to debug)
    | http-server-header: Software version grabbed from Server header.
    | Consider submitting a service fingerprint.
    |_Run with --script-args http-server-header.skip
    |_http-title: Did not follow redirect to https://192.168.134.138/logout.user
    443/tcp open  ssl/https G4200.GSI.2.22.0155
    |_http-generator: ERROR: Script execution failed (use -d to debug)
    | http-server-header: Software version grabbed from Server header.
    | Consider submitting a service fingerprint.
    |_Run with --script-args http-server-header.skip
    |_http-title: Did not follow redirect to https://192.168.134.138/logout.user
    | ssl-cert: Subject: organizationName=Gemtek Systems
    | Issuer: organizationName=Gemtek Systems
    | Public Key type: rsa
    | Public Key bits: 1024
    | Not valid before: 2002-10-07T06:46:53+00:00
    | Not valid after:  2019-03-12T06:46:53+00:00
    | MD5:   eea3 78e6 bd78 80aa 562c e808 3e6f b8e4
    |_SHA-1: 7696 a373 c1cf bf3f 11c4 b5f8 0aff 76c2 adcb 167e
    |_ssl-date: 1970-01-06T06:07:01+00:00; -46y15d20h48m03s from local time.
    | sslv2:
    |   SSLv2 supported
    |   ciphers:
    |     SSL2_DES_192_EDE3_CBC_WITH_MD5
    |     SSL2_RC2_CBC_128_CBC_WITH_MD5
    |     SSL2_RC4_128_WITH_MD5
    |     SSL2_RC4_64_WITH_MD5
    |     SSL2_DES_64_CBC_WITH_MD5
    |     SSL2_RC2_CBC_128_CBC_WITH_MD5
    |_    SSL2_RC4_128_EXPORT40_WITH_MD5
    MAC Address: 00:14:A5:32:BF:39 (Gemtek Technology Co.)
    Device type: general purpose
    Running: Linux 2.4.X
    OS CPE: cpe:/o:linux:linux_kernel:2.4
    OS details: Linux 2.4.21 - 2.4.31 (likely embedded)
    Uptime guess: 5.255 days (since Sat Jan 16 15:48:02 2016)
    Network Distance: 1 hop
    TCP Sequence Prediction: Difficulty=201 (Good luck!)
    IP ID Sequence Generation: All zeros
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    
    TRACEROUTE
    HOP RTT     ADDRESS
    1   6.39 ms 10.156.41.1
    
    NSE: Script Post-scanning.
    Initiating NSE at 21:55
    Completed NSE at 21:55, 0.00s elapsed
    Read data files from: /usr/bin/../share/nmap
    OS and Service detection performed. Please report any incorrect results at http://nmap.org/submit/ .
    Nmap done: 1 IP address (1 host up) scanned in 195.36 seconds
               Raw packets sent: 1657 (75.388KB) | Rcvd: 1309 (54.932KB)


