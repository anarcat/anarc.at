[[!meta title="Relaying mail through debian.org"]]

Back in 2020, I wrote [[this article about using DKIM to sign outgoing
debian.org mail|blog/2020-04-14-opendkim-debian/]]. This worked well
for me for a while: outgoing mail was signed with DKIM and somehow was
delivered. Maybe. Who knows.

But now [we have a relay server](https://lists.debian.org/debian-devel-announce/2022/07/msg00003.html) which makes this kind of moot. So
I have changed my configuration to use that relay instead of sending
email on my own. It seems more reliable that mail seems to be coming
from a real `debian.org` machine, so I'm hoping this will have better
reputation than my current setup.

In general, you should follow the [DSA documentation which includes a
Postfix configuration](https://dsa.debian.org/user/mail-submit/). In my case, it was basically this patch:

    diff --git a/postfix/main.cf b/postfix/main.cf
    index 7fe6dd9e..eabe714a 100644
    --- a/postfix/main.cf
    +++ b/postfix/main.cf
    @@ -55,3 +55,4 @@ smtp_sasl_security_options =
     smtp_sender_dependent_authentication = yes
     sender_dependent_relayhost_maps = hash:/etc/postfix/sender_relay
     sender_dependent_default_transport_maps = hash:/etc/postfix/sender_transport
    +smtp_tls_policy_maps = hash:/etc/postfix/tls_policy
    diff --git a/postfix/sender_relay b/postfix/sender_relay
    index b486d687..997cce19 100644
    --- /dev/null
    +++ b/postfix/sender_relay
    @@ -0,0 +1,2 @@
    +# Per-sender provider; see also /etc/postfix/sasl_passwd.
    +@debian.org    [mail-submit.debian.org]:submission
    diff --git a/postfix/sender_transport b/postfix/sender_transport
    index ca69bc7a..c506c1fc 100644
    --- /dev/null
    +++ b/postfix/sender_transport
    @@ -0,0 +1,1 @@
    +anarcat@debian.org     smtp:
    diff --git a/postfix/tls_policy b/postfix/tls_policy
    new file mode 100644
    index 00000000..9347921a
    --- /dev/null
    +++ b/postfix/tls_policy
    @@ -0,0 +1,1 @@
    +submission.torproject.org:submission   verify ciphers=high

This configuration differs from the one provided by DSA because I
already had the following configured:

    sender_dependent_relayhost_maps = hash:/etc/postfix/sender_relay
    smtp_sender_dependent_authentication = yes
    smtp_sasl_auth_enable = yes
    smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
    smtp_sasl_tls_security_options = noanonymous

I also don't show the patch on `/etc/postfix/sasl_passwd` for obvious
security reasons.

I also had to setup a `tls_policy` map, because I couldn't use `dane`
for all my remotes. You'll notice I also had to setup a
`sender_transport` because I use a non-default `default_transport` as
well.

It also seems like you can keep the previous DKIM configuration in
parallel with this one, as long as you don't double-sign outgoing
mail. Since this configuration here is done on my mail *client*
(i.e. not on the *server* where I am running OpenDKIM), I'm not
double-signing so I left the DKIM configuration alone. But if I wanted
to remove it, the magic command is:

    echo "del dkimPubKey" | gpg --clearsign | mail changes@db.debian.org

[[!tag tutorial debian-planet sysadmin debian email]]
