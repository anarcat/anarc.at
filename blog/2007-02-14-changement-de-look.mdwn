<!-- Drupal node 109 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Changement de look"]]
[[!meta date="2007-02-14T22:10:57-0500"]]
[[!meta updated="2012-12-14T22:45:08-0500"]]
[[!meta guid="109 at http://anarcat.koumbit.org"]]

Juste une note très rapide pour annoncer que j'ai changé le look de mon blogue... [chameleon](http://drupal.org/project/chameleon) a fait son temps, et il refusait de m'afficher la date de publication des articles alors j'ai essayé [zen](http://drupal.org/project/zen), qui, semblerait-il, est très facile à modifier pour ses besoins... Quand j'aurai du temps, je ferai comme [Mathieu](http://mathieu.koumbit.org/) et j'aurai un beau "theme custom" et tout...

[[!tag "meta" "geek"]]