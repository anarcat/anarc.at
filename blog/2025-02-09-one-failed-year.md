[[!meta title="A slow blogging year"]]

Well, 2024 will be remembered, won't it? I guess 2025 already wants to
make its mark too, but let's not worry about that right now, and
instead let's talk about me.

A [[little over a year ago|2024-01-08-one-more-year]], I was gloating
over how I had such a great blogging year in 2022, and was considering
2023 to be average, then went on to gather more stats and traffic
analysis... Then I said, and I quote:

> I hope to write more next year. I've been thinking about a few posts I
> could write for work, about how things work behind the scenes at Tor,
> that could be informative for many people. We run a rather old setup,
> but things hold up pretty well for what we throw at it, and it's worth
> sharing that with the world...

What a load of bollocks.

[[!toc]]

# A bad year for this blog

2024 was the second worst year ever in my blogging history, tied with
2009 at a measly 6 posts for the year:

```
anarcat@angela:anarc.at$ curl -sSL https://anarc.at/blog/ | grep 'href="\./' | grep -o 20[0-9][0-9] | sort | uniq -c | sort -nr | grep -v 2025 | tail -3
      6 2024
      6 2009
      3 2014
```

I did write about my work though, detailing the [migration from
Gitolite to GitLab we completed that year](https://anarc.at/blog/2024-05-01-gitolite-gitlab-migration/). But after August, total
radio silence until now.

# Loads of drafts

It's not that I have nothing to say: I have no less than *five* drafts
in my working tree here, not counting three *actual* drafts recorded
in the Git repository here:

```
anarcat@angela:anarc.at$ git s blog
## main...origin/main
?? blog/bell-bot.md
?? blog/fish.md
?? blog/kensington.md
?? blog/nixos.md
?? blog/tmux.md
anarcat@angela:anarc.at$ git grep -l '\!tag draft'
blog/mobile-massive-gallery.md
blog/on-dying.mdwn
blog/secrets-recovery.md
```

I just don't have time to wrap those things up. I think part of me is
disgusted by seeing my work [stolen by large corporations](https://arstechnica.com/tech-policy/2025/02/meta-torrented-over-81-7tb-of-pirated-books-to-train-ai-authors-say/) to build
proprietary large language models while my [idols](https://en.wikipedia.org/wiki/Aaron_Swartz) have been pushed
to suicide for trying to share science with the world.

Another part of me wants to make those things *just right*. The
"tagged drafts" above are nothing more than a huge pile of chaotic
links, far from being useful for anyone else than me, and even
then.

The `on-dying` article, in particular, is becoming my nemesis. I've
been wanting to write that article for over 6 years now, I think. It's
just too hard.

# Writing elsewhere

There's also the fact that I write for work already. A lot. Here are
the top-10 contributors to our team's wiki:

```
anarcat@angela:help.torproject.org$ git shortlog --numbered --summary --group="format:%al" | head -10
  4272	anarcat
   423	jerome
   117	zen
   116	lelutin
   104	peter
    58	kez
    45	irl
    43	hiro
    18	gaba
    17	groente
```

... but that's a bit unfair, since I've been there half a
decade. Here's the last year:

```
anarcat@angela:help.torproject.org$ git shortlog --since=2024-01-01 --numbered --summary --group="format:%al" | head -10
   827	anarcat
   117	zen
   116	lelutin
    91	jerome
    17	groente
    10	gaba
     8	micah
     7	kez
     5	jnewsome
     4	stephen.swift
```

So I still write the most commits! But to truly get a sense of the
amount I wrote in there, we should count actual changes. Here it is by
number of lines (from [commandlinefu.com](https://www.commandlinefu.com/commands/view/3889/prints-per-line-contribution-per-author-for-a-git-repository)):

```
anarcat@angela:help.torproject.org$ git ls-files | xargs -n1 git blame --line-porcelain | sed -n 's/^author //p' | sort -f | uniq -ic | sort -nr | head -10
  99046 Antoine Beaupré
   6900 Zen Fu
   4784 Jérôme Charaoui
   1446 Gabriel Filion
   1146 Jerome Charaoui
    837 groente
    705 kez
    569 Gaba
    381 Matt Traudt
    237 Stephen Swift
```

That, of course, is the entire history of the git repo, again. We
should take only the last year into account, and probably ignore the
`tails` directory, as sneaky Zen Fu imported the entire docs from
another wiki there...

```
anarcat@angela:help.torproject.org$ find [d-s]* -type f -mtime -365 | xargs -n1 git blame --line-porcelain 2>/dev/null | sed -n 's/^author //p' | sort -f | uniq -ic | sort -nr | head -10
  75037 Antoine Beaupré
   2932 Jérôme Charaoui
   1442 Gabriel Filion
   1400 Zen Fu
    929 Jerome Charaoui
    837 groente
    702 kez
    569 Gaba
    381 Matt Traudt
    237 Stephen Swift
```

Pretty good! 75k lines. But those are the files that were modified in
the last year. If we go a [little more nuts](https://gitlab.com/anarcat/scripts/-/blob/1a754448170387db4d70dad22d30a99b00ac58fc/git-count-words-range.py), we find that:

```
anarcat@angela:help.torproject.org$ $ git-count-words-range.py  | sort -k6 -nr | head -10
parsing commits for words changes from command: git log '--since=1 year ago' '--format=%H %al'
anarcat 126116 - 36932 = 89184
zen 31774 - 5749 = 26025
groente 9732 - 607 = 9125
lelutin 10768 - 2578 = 8190
jerome 6236 - 2586 = 3650
gaba 3164 - 491 = 2673
stephen.swift 2443 - 673 = 1770
kez 1034 - 74 = 960
micah 772 - 250 = 522
weasel 410 - 0 = 410
```

I wrote 126,116 words in that wiki, only in the last year. I also
*deleted* 37k words, so the final total is more like 89k words, but
still: that's about *forty* (40!) articles of the average size (~2k) I
wrote in 2022.

(And yes, I did go nuts and write a new log parser, essentially from
scratch, to figure out those word diffs. I did get the courage only
after asking GPT-4o for an example first, I must admit.)

Let's celebrate that again: I wrote 90 thousand words in that wiki
in 2024. [According to Wikipedia](https://en.wikipedia.org/wiki/Book#Fiction), a "novella" is 17,500 to 40,000
words, which would mean I wrote about a novella and a novel, in the
past year.

But interestingly, if I look at the [repository analytics](https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/graphs/master?ref_type=heads). I
certainly didn't write *that* much more in the past year. So that
alone cannot explain the lull in my production here.

# Arguments

Another part of me is just tired of the bickering and arguing on the
internet. I have at least two articles in there that I suspect is
going to get me a lot of push-back (NixOS and Fish). I know how to
deal with this: you need to write well, consider the controversy,
spell it out, and defuse things before they happen. But that's hard
work and, frankly, I don't really care that much about what people
think anymore.

I'm not writing here to convince people. I have stop evangelizing a
long time ago. Now, I'm more into documenting, and teaching. And,
while teaching, there's a two-way interaction: when you give out a
speech or workshop, people can ask questions, or respond, and you all
learn something. When you document, you quickly get told "where is
this? I couldn't find it" or "I don't understand this" or "I tried
that and it didn't work" or "wait, really? shouldn't we do X instead",
and you learn.

Here, it's static. It's my little soapbox where I scream in the
void. The only thing people can do is scream back.

# Collaboration

So.

Let's see if we can work together here.

If you don't like something I say, disagree, or find something wrong
or to be improved, instead of screaming on social media or ignoring
me, try contributing back. This site here is backed by a [git
repository](https://gitlab.com/anarcat/anarc.at) and I promise to read everything you send there,
whether it is an [issue](https://gitlab.com/anarcat/anarc.at/-/issues) or a [merge request](https://gitlab.com/anarcat/anarc.at/-/merge_requests).

I will, of course, still read comments sent by email or IRC or social
media, but please, be kind.

You can also, of course, follow the latest changes on the [TPA
wiki](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/). If you want to catch up with the last year, some of the
"novellas" I wrote include:

- [TPA-RFC-33: Monitoring](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-33-monitoring): Nagios to Prometheus conversion, see
  also the extensive [Prometheus documentation](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/prometheus) we wrote
- [TPA-RFC-45: email architecture](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-45-mail-architecture): draft of long-term email
  services at `torproject.org`
- [TPA-RFC-62: TPA password manager](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-62-tpa-password-manager): switch to password-store
- [TPA-RFC-63: storage server budget](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-63-storage-server-budget): buy a new backup storage
  server (5k$ + 100$/mth)
- [TPA-RFC-65: PostgreSQL backups](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-65-postgresql-backups): switching from legacy to
  pgBackRest for database backups
- [TPA-RFC-68: Idle canary servers](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-68-idle-canary-servers): provision test servers that
  sit idle to monitor infrastructure and stage deployments
- [TPA-RFC-71: Emergency email deployments, phase B](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-71-emergency-email-deployments-round-2): deploy a new
  sender-rewriting mail forwarder, migrate mailing lists off the
  legacy server to a new machine, migrate the remaining Schleuder list
  to the Tails server, upgrade eugeni.
- [TPA-RFC-76: Puppet merge request workflow](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-76-puppet-merge-request-workflow): how to mirror our
  private Puppet repo to GitLab safely
- [TPA-RFC-79: General merge request workflows](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-79-general-merge-request-workflows): how to use merge
  requests, assignees, reviewers, draft and threads on GitLab projects

(Well, no, you can't actually follow changes on a GitLab wiki. But we
have a [wiki-replica git repository](https://gitlab.torproject.org/tpo/tpa/wiki-replica/) where you can see the [latest
commits](https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/commits/master), and subscribe to the [RSS feed](https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/commits/master?format=atom).)

See you there!

[[!tag debian-planet python-planet gloating meta stats]]
