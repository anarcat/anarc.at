[[!meta title="Diversity, education, privilege and ethics in technology"]]

> This article is part of a series on KubeCon Europe 2018.
>
> * [[Diversity, education, privilege and ethics in technology|2018-05-26-kubecon-rant]] (this article)
> * [[Autoscaling for Kubernetes workloads|2018-05-29-autoscaling-kubernetes]]
> * [[Updates in container isolation|2018-05-31-secure-pods]]
> * [[Securing the container image supply chain|2018-05-31-securing-container-supply]]
> * [[Easier container security with entitlements|2018-06-13-easier-container-security-entitlements]]

This is a rant I wrote while attending [KubeCon Europe 2018](https://kccnceu18.sched.com/). I do
not know how else to frame this deep discomfort I have with the way
one of the most cutting edge projects in my community is moving. I see
it as a symptom of so many things wrong in society at large and
figured it was as good a way as any to open the discussion regarding
how free software communities seem to naturally evolved into corporate
money-making machines with questionable ethics.

<figure>
<a href="https://photos.anarc.at/events/kubecon-eu-2018/#/0">
<img src="https://photos.anarc.at/events/kubecon-eu-2018/DSCF3129.JPG" alt="A white male looking at his phone while a hair-dresser prepares him for a video shoot, with plants and audio-video equipment in the background" /></a>
<figcaption>A white man groomed by a white woman</figcaption>
</figure>

Diversity and education
=======================

There is often a great point made of diversity at KubeCon, and that is
something I truly appreciate. It's one of the places where I have seen
the largest efforts towards that goal; I was impressed by the efforts
done in Austin, and mentioned it in [[my overview of that
conference|blog/2017-12-13-kubecon-overview]] back then. Yet it is
still one of the less diverse places I've ever participated in: in
comparison, [Pycon](https://en.wikipedia.org/wiki/Python_Conference) "feels" more diverse, for example. And then, of
course, there's real life out there, where women constitute basically
half the population, of course. This says something about the actual
effectiveness diversity efforts in our communities.

<figure>
<a href="https://photos.anarc.at/events/kubecon-eu-2018/#/24">
<img src="https://photos.anarc.at/events/kubecon-eu-2018/DSCF3307.JPG" alt="a large conference room full of people that mostly look like white male, with a speaker on a large stage illuminated in white" />
</a>
<figcaption>4000 white men</figcaption>
</figure>

The truth is that contrary to programmer communities, "operations"
knowledge ([sysadmin](https://en.wikipedia.org/wiki/System_administrator), [SRE](https://en.wikipedia.org/wiki/Site_Reliability_Engineering), [DevOps](https://en.wikipedia.org/wiki/DevOps), whatever it's called
these days) comes not from institutional education, but from
self-learning. Even though I have years of university training, the
day to day knowledge I need in my work as a sysadmin comes not from
the university, but from late night experiments on my [[personal
computer network|2012-11-01-my-short-computing-history/]]. This was
first on the Macintosh, then on the FreeBSD source code of passed down
as a magic word from an uncle and finally through Debian consecrated
as the leftist's true computing way. Sure, my programming skills were
useful there, but I acquired those *before* going to university: even
there teachers expected students to learn programming languages (such
as C!) in-between sessions.

<figure>
<a href="https://photos.anarc.at/events/kubecon-eu-2018/#/42">
<img src="https://photos.anarc.at/events/kubecon-eu-2018/DSCF3502.JPG" alt="A bunch of white geeks hanging out with their phones next to a sign that says 'Thanks to our Diversity Scholarship Sponsors' with a bunch of corporate logos" />
</a>
<figcaption>Diversity program</figcaption>
</figure>

The real solutions to the lack of diversity in our communities not
only comes from a change in culture, but also real investments in
society at large. The mega-corporations subsidizing events like
KubeCon make sure they get a lot of good press from those diversity
programs. However, the money they spend on those is nothing compared
to tax evasion in their home states. As an example, [Amazon recently
put 7000 jobs on hold because of a tax the city of Seattle wanted to
impose on corporations to help the homeless population](https://www.jwz.org/blog/2018/05/amazon-puts-7000-jobs-on-hold-because-of-a-tax-that-would-help-seattles-homeless-population/). Google,
Facebook, Microsoft, and Apple all evade taxes like gangsters. This is
important because society changes partly through education, and that
costs money. Education is how more traditional [STEM](https://en.wikipedia.org/wiki/Science,_technology,_engineering,_and_mathematics) sectors like
engineering and medicine have changed: women, minorities, and poorer
populations were finally allowed into schools after the epic social
struggles of the 1970s finally yielded more accessible education. The
same way that culture changes are seeing a backlash, the tide is
turning there as well and the trend is reversing towards more costly,
less accessible education of course. But not everywhere. The impacts
of education changes are long-lasting. By evading taxes, those
companies are keeping the state from revenues that could level the
playing field through affordable education.

Hell, *any* education in the field would help. There is basically no
sysadmin education curriculum right now. Sure you can follow a Cisco
[CCNA](https://en.wikipedia.org/wiki/CCNA) or [MSCE](https://en.wikipedia.org/wiki/Microsoft_Certified_Professional) private trainings. But anyone who's been
seriously involved in running any computing infrastructure knows those
are a scam: that will tie you down in a proprietary universe (Cisco
and Microsoft, respectively) and probably just to "remote hands
monkey" positions and rarely to executive positions.

Velocity
========

Besides, providing an education curriculum would require the field to
slow down so that knowledge would settle down and trickle into a
curriculum. Configuration management is [pretty old](https://en.wikipedia.org/wiki/Configuration_management#History), but because
the changes in tooling are fast, any curriculum built in the last
decade (or even less) quickly becomes irrelevant. [Puppet](https://en.wikipedia.org/wiki/Puppet_%28software%29)
publishes a new release [every 6 month](https://puppet.com/misc/puppet-enterprise-lifecycle), Kubernetes is barely 4
years old now, and is changing rapidly with a [~3 month release
schedule](https://gravitational.com/blog/kubernetes-release-cycle/).

Here at KubeCon, Mark Zuckerberg's mantra of "move fast and break
things" is everywhere. We call it "[velocity](https://www.cncf.io/blog/2017/06/05/30-highest-velocity-open-source-projects/)": where you are going
does not matter as much as how fast you're going there. At one of the
many [keynotes](https://kccnceu18.sched.com/event/E5wI/keynote-shaping-the-cloud-native-future-abby-kearns-executive-director-cloud-foundry-foundation-slides-attached), Abby Kearns from the [Cloud Foundry Foundation](https://www.cloudfoundry.org/)
boasted at how Home Depot, in trying to sell more hammers than Amazon,
is now deploying code to production multiple times a day. I am still
unclear as whether this made Home Depot *actually* sell more hammers,
or if it's something that we should even care about in the first
place. Shouldn't we converge over selling *less* hammers? Making them
more solid, reliable, so that they are passed down from generations
instead of breaking and having to be replaced all the time?

<figure>
<img src="home-depot-speed.png" alt="Slide from Kearn's keynote that shows a women with perfect nail polish considering a selection of paint colors with the Home Depot logo and stats about 'speed' in their deployment " />
<figcaption>Home Depot ecstasy</figcaption>
</figure>

We're solving a problem that wasn't there in some new absurd faith
that code deployments will naturally make people happier, by making
sure Home Depot sells more hammers. And that's after telling us that
Cloud Foundry helped the USAF save 600M$ by moving their databases to
the cloud. No one seems bothered by the idea that the most powerful
military in existence would move state secrets into a private cloud,
out of the control of any government. It's the name of the game, at
KubeCon.

<figure>
<img src="usaf-cost-savings.png" alt="Picture of a jet fighter flying over clouds, the logo of the USAF and stats about the cost savings due their move to the cloud" />
<figcaption>USAF saves (money)</figcaption>
</figure>

In his [keynote](https://kccnceu18.sched.com/event/Duok/keynote-cncf-20-20-vision-alexis-richardson-founder-ceo-weaveworks), Alexis Richardson, CEO of [Weaveworks](https://www.weave.works/),
presented [the toaster project](http://www.thetoasterproject.org/) as an example of what *not* to
do. "He did not use any sourced components, everything was built from
scratch, by hand", obviously missing the fact that toasters are
*deliberately* *not* built from reusable parts, as part of the
[planned obsolescence](https://en.wikipedia.org/wiki/Planned_obsolescence) design. The goal of the toaster experiment
is also to show how fragile our civilization has become precisely
*because* we depend on layers upon layers of parts. In this
totalitarian view of the world, people are also "reusable" or, in that
case "disposable components". Not just the white dudes in California,
but also workers outsourced out of the USA decades ago; it depends on
precious metals and the miners of Africa, the specialized labour of
the factories and intricate knowledge of the factory workers in Asia,
and the flooded forests of the first nations powering this terrifying
surveillance machine.

Privilege
=========

<figure>
<img src="toaster-project.jpg" alt="Photo of the Toaster Project book which shows a molten toster that looks like it came out of a H.P. Lovecraft novel" />
<figcaption>
"Left to his own devices he couldn’t build a toaster. He could just
about make a sandwich and that was it." -- Mostly Harmless, Douglas
Adams, 1992
</figcaption>
</figure>


Staying in an hotel room for a week, all expenses paid, certainly puts
things in perspectives. Rarely have I felt more privileged in my
entire life: someone else makes my food, makes my bed, and cleans up
the toilet magically when I'm gone. For me, this is extraordinary, but
for many people at KubeCon, it's routine: traveling is part of the
rock star agenda of this community. People get used to being served,
both directly in their day to day lives, but also through the complex
supply chain of the modern technology that is destroying the planet.

<figure>
<a href="https://photos.anarc.at/events/kubecon-eu-2018/#/37">
<img src="https://photos.anarc.at/events/kubecon-eu-2018/DSCF3484.JPG" alt="An empty shipping container probably made of cardboard hanging over the IBM booth"/> 
</a>
<figcaption>Nothing is like corporate nothing.</figcaption>
</figure>

The nice little boxes and containers we call the cloud all abstract
this away from us and those dependencies are actively encouraged in
the community. We like containers here and their image is
ubiquitous. We acknowledge that a single person cannot run a Kube shop
because the knowledge is too broad to be possibly handled by a single
person. While there are interesting collaborative and social ideas in
that approach, I am deeply skeptical of its impact on civilization in
the long run. We already created systems so complex that we don't
truly know who hacked the Trump election or how. Many feel it was, but
it's really just a hunch: there were bots, maybe they were Russian, or
maybe from [Cambridge](https://en.wikipedia.org/wiki/Cambridge_Analytica)? The [DNC emails](https://en.wikipedia.org/wiki/2016_Democratic_National_Committee_email_leak), was that really
Wikileaks?  Who knows! Never mind failing close or open: the system
has become so complex that we don't even know *how* we fail when we
do. Even those in the highest positions of power seem [unable to
protect themselves](https://www.bloomberg.com/features/2018-palantir-peter-thiel/); politics seem to have become a game of Russian
roulette: we cock the bot, roll the secret algorithm, and see what
dictator will shoot out.

Ethics
======

All this is to build a new [Skynet][]; not [this one][] or [that
one][], those already exist. I was able to pleasantly joke about the
[AI takeover](https://en.wikipedia.org/wiki/AI_takeover) during breakfast with a random stranger without
raising as much as an eyebrow: we know it will happen, oh well. I've
skipped that track in my attendance, but multiple talks at KubeCon are
about AI, [TensorFlow](https://en.wikipedia.org/wiki/TensorFlow) (it's opensource!), self-driving cars, and
removing humans from the equation as much as possible, as a [general
principle](https://en.wikipedia.org/wiki/All_Watched_Over_by_Machines_of_Loving_Grace). Kubernetes is often shortened to "Kube", which I always
think of as a reference to the [Star Trek Borg](https://en.wikipedia.org/wiki/Borg_(Star_Trek)) all mighty ship,
the "cube". This might actually make sense given that Kubernetes is an
open source version of Google's internal software incidentally
called... [Borg](https://kubernetes.io/blog/2015/04/borg-predecessor-to-kubernetes/). To make such fleeting, tongue-in-cheek references
to a totalitarian civilization is not harmless: it makes more
acceptable the notion that AI domination is inescapable and that
resistance truly is futile, the ultimate neo-colonial scheme.

 [Skynet]: https://en.wikipedia.org/wiki/Skynet_(Terminator)
 [this one]: https://en.wikipedia.org/wiki/SKYNET_(surveillance_program)
 [that one]: https://en.wikipedia.org/wiki/Skynet_(satellite)

<figure class="align-right">
<a href="https://en.wikipedia.org/wiki/File:Picard_as_Locutus.jpg">
<img
src="https://upload.wikimedia.org/wikipedia/en/a/a1/Picard_as_Locutus.jpg"
alt="Captain Jean-Luc Picard, played by Patrick Stewart, assimilated by the Borg as 'Locutus'" />
</a>
<figcaption> "We are the Borg. Your biological and technological
distinctiveness will be added to our own. Resistance is
futile."</figcaption>
</figure>

The "hackers" of our age are building this machine with conscious
knowledge of the social and ethical implications of their work. At
best, people admit to [not knowing what they really are](https://insights.stackoverflow.com/survey/2018/#technology-and-society). In the
worse case scenario, the AI apocalypse will bring massive unemployment
and a collapse of the industrial civilization, to which Silicon Valley
executives are responding by [buying bunkers](https://www.newyorker.com/magazine/2017/01/30/doomsday-prep-for-the-super-rich) to survive the
eventual roaming gangs of revolted (and now armed) teachers and young
students coming for revenge.

Only the most privileged people in society could imagine such a
scenario and actually [opt out](https://www.youtube.com/watch?v=K1zVAfE0YdA) of society as a whole. Even the
[robber barons](https://en.wikipedia.org/wiki/Robber_baron_(industrialist)) of the 20th century knew they couldn't survive the
coming revolution: [Andrew Carnegie built libraries](https://en.wikipedia.org/wiki/Andrew_Carnegie#1901%E2%80%931919:_Philanthropist) after creating
the steel empire that drove much of US industrialization near the end
of the century and [John D. Rockefeller](https://en.wikipedia.org/wiki/John_D._Rockefeller#Philanthropy) subsidized education,
research and science. This is not because they were humanists: you do
not become an oil tycoon by tending to the poor. Rockefeller said that
"the growth of a large business is merely a survival of the fittest",
a [social darwinist](https://en.wikipedia.org/wiki/Social_Darwinism) approach he gladly applied to society as a
whole.

 [robber barons]: https://en.wikipedia.org/wiki/Robber_baron_(industrialist)

But the 70's rebel beat offspring, the children of the [cult of
Job](https://www.bbc.com/news/magazine-15194365), do not seem to have the depth of analysis to understand
what's coming for them. They want to "hack the system" not for
everyone, but for themselves. Early on, we have learned to be selfish
and self-driven: repressed as nerds and rejected in the schools, we
swore vengeance on the bullies of the world, and boy are we getting
our revenge. The bullied have become the bullies, and it's not small
boys in schools we're bullying, it is entire states, with which
companies are now [negotiating as equals](http://cphpost.dk/news/business/denmark-gets-worlds-first-digital-ambassador.html).

The fraud
=========

<figure>
<a href="https://photos.anarc.at/events/kubecon-eu-2018/#/40">
<img src="https://photos.anarc.at/events/kubecon-eu-2018/DSCF3496.jpg"
alt="A t-shirt from the Cloudfoundry booth that reads 'Freedom to create'"/> 
</a>
<figcaption>...but what are you creating exactly?</figcaption>
</figure>

And that is the ultimate fraud: to make the world believe we are
harmless little boys, so repressed that we can't communicate
properly. We're so sorry we're awkward, it's because we're all
somewhat on the autism spectrum. Isn't that, after all, a convenient
affliction for people that would not dare to confront the oppression
they are creating? It's too easy to hide behind such a real and
serious condition that does affect people in our community, but also
truly [autistic](https://en.wikipedia.org/wiki/Autism) people that simply cannot make it in the
fast-moving world the magical [rain man](https://en.wikipedia.org/wiki/Rain_Man) is creating. But the real
con is hacking power and political control away from traditional
institutions, seen as too slow-moving to really accomplish the
"change" that is "needed". We are creating an inextricable technocracy
that no one will understand, not even us "experts". Instead of serving
the people, the machine is at the mercy of markets and powerful
oligarchs.

A recurring pattern at Kubernetes conferences is the [KubeCon
chant](https://twitter.com/alstard/status/991615812397092865 ) where [Kelsey Hightower](https://github.com/kelseyhightower/) reluctantly engages the crowd in
a pep chant:

> When I say 'Kube!', you say 'Con!'
>
> 'Kube!' 'Con!'
> 'Kube!' 'Con!'
> 'Kube!' 'Con!'

Cube [Con](https://en.wikipedia.org/wiki/Confidence_trick) indeed...

I wish I had some wise parting thoughts of where to go from here or
how to change this. The tide seems so strong that all I can do is
observe and tell stories. My hope is that the people that need to hear
this will take it the right way, but I somehow doubt it. With chance,
it might just become irrelevant and everything will fix itself, but
somehow I fear things will get worse before they get better.

[[!tag debian-planet kubernetes containers reflexion politics tech diversity education ethics]]
