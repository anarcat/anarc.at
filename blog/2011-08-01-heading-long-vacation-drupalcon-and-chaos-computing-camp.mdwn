<!-- Drupal node 171 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Heading for a long vacation, Drupalcon and the Chaos Computing Camp"]]
[[!meta date="2011-08-01T23:14:28-0500"]]
[[!meta updated="2012-12-14T22:45:10-0500"]]
[[!meta guid="171 at http://anarcat.koumbit.org"]]

At the very last I am taking my second long leave from my beloved [job](http://koumbit.org/) in the 7 years I have dedicated to this project. The first time was 5 years ago and was refreshing, so it is now clearly overdue. This is both to make sure Koumbit can survive without me but also making sure that I can survive without Koumbit - I am basically testing the backup procedures here.

Another reason for this leave is to make sure that, within our self-managed organisation, power is evenly distributed. It is a common phenomenon in any group for some people to gather specific powers and capabilities over others, and this can lead to problematic situations if those situations are not acknowledged and resolved. I have identified a few of those pain points and hopefully my absence will result in more people taking the power back, so to speak, and make sure Koumbit remains true to its ideals and principles.

For my own health, I will also do my best to stay away from the computer, and will not check "work" email. This unfortunately include a lot of open source projects I am currently involved with, most prominently the [Aegir project](http://aegirproject.org/). 

I am therefore also taking a leave from Aegir, except for my presence at [Drupalcon London](http://london2011.drupal.org/), during which I will [provide training](http://london2011.drupal.org/page/aegir-hosting-system-deep-dive) and [give a presentation](http://london2011.drupal.org/conference/sessions/aegir-one-drupal-rule-them-all) about Aegir. We will also probably organize BOFs and dinners and whatnot to renew our contacts with the community. It is of course very hard to detach myself from those incarnations of my self, so I will certainly do advocacy work for free software and those projects I am involved in, but it is not the primary objective of this mighty trip.

I will be touring Europe for two months: first Germany and the [Chaos Computing Camp](https://events.ccc.de/camp/2011/), then the UK and Drupalcon, and finally .. well, the rest is none of your business - I'll just be offline traveling around. :)

De retour au Québec en automne pour les couleurs!

[[!tag "koumbit" "geek" "fun" "drupal" "debian-planet" "koumbit" "geek" "fun" "drupal" "debian-planet"]]