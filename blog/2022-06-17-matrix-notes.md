[[!meta title="Matrix notes"]]

I have some concerns about Matrix (the protocol, not the movie that
came out recently, although I do have concerns about that as
well). I've been watching the project for a long time, and it seems
more a promising alternative to many protocols like IRC, XMPP, and
Signal.

This review may sound a bit negative, because it focuses on those
concerns. I am the operator of an IRC network and people keep asking
me to bridge it with Matrix. I have myself considered just giving up
on IRC and converting to Matrix. This space is a living document
exploring my research of that problem space. The TL;DR: is that no,
I'm not setting up a bridge just yet, and I'm still on IRC.

This article was written over the course of the last three months, but
I have been watching the Matrix project for years (my logs seem to say
2016 at least). The article is rather long. It will likely take you
half an hour to read, so [copy this over to your ebook reader](https://gitlab.com/anarcat/wallabako),
your tablet, or dead trees, and lean back and relax as I show you
around the Matrix. Or, alternatively, just jump to a section that
interest you, most likely the [conclusion](#conclusion).

[[!toc levels=2]]

# Introduction to Matrix

Matrix is an "open standard for interoperable, decentralised,
real-time communication over IP. It can be used to power Instant
Messaging, VoIP/WebRTC signalling, Internet of Things communication -
or anywhere you need a standard HTTP API for publishing and
subscribing to data whilst tracking the conversation history".

It's also (when [compared with XMPP](https://matrix.org/faq/#what-is-the-difference-between-matrix-and-xmpp%3F)) "an eventually consistent
global JSON database with an HTTP API and pubsub semantics - whilst
XMPP can be thought of as a message passing protocol."

[According to their FAQ](https://matrix.org/faq/#what-is-the-current-project-status), the project started in 2014, has about
20,000 servers, and millions of users. Matrix works over HTTPS but
over a [special port](https://matrix.org/faq/#what-ports-do-i-have-to-open-up-to-join-the-global-matrix-federation%3F): 8448.

# Security and privacy

I have some concerns about the security promises of Matrix. It's
advertised as a "secure" with "E2E [end-to-end] encryption", but how
does it actually work?

## Data retention defaults

One of my main concerns with Matrix is data retention, which is a key
part of security in a threat model where (for example) an hostile
state actor wants to surveil your communications and can seize your
devices.

On IRC, servers don't actually keep messages all that long: they pass
them along to other servers and clients as fast as they can, only keep
them in memory, and move on to the next message. There are no concerns
about data retention on messages (and their metadata) other than the
network layer. (I'm ignoring the issues with user registration, which
is a separate, if valid, concern.)  Obviously, an hostile server
*could* log everything passing through it, but IRC federations are
normally tightly controlled. So, if you trust your IRC operators, you
should be fairly safe. Obviously, clients *can* (and often do, even if
[OTR](https://en.wikipedia.org/wiki/Off-the-Record_Messaging) is configured!) log all messages, but this is generally not
the default. [Irssi](https://irssi.org/), for example, does [not log by
default](https://github.com/irssi/irssi/blob/7d673653a13ed1123665e36270d1a578baefd9e5/docs/startup-HOWTO.txt#L399-L412). IRC bouncers are more likely to log to disk, of course,
to be able to do what they do.

Compare this to Matrix: when you send a message to a Matrix
homeserver, that server first stores it in its internal SQL
database. Then it will transmit that message to all clients connected
to that server and room, and to all other servers that have clients
connected to that room. Those remote servers, in turn, will keep a
copy of that message and all its metadata in their own database, by
default forever. On encrypted rooms those messages are encrypted, but
not their metadata.

There is a mechanism to expire entries in Synapse, but it is [not
enabled by default](https://github.com/matrix-org/synapse/blob/28989cb301fecf5a669a634c09bc2b73f97fec5d/docs/sample_config.yaml#L559). So one should generally assume that a message
sent on Matrix is never expired.

Update: one key thing with Matrix is that it's not a linear storage of
discussions like IRC or email. It's actually an directed acyclic graph
that servers try to continuously merged. And as such it's actually
*really hard* to delete past history. Even with expiration enabled,
that's advisory for other servers *and* some room parameters (like
join/part/bans) *must* be kept forever. See [this lengthy discussion
for details][why-not-matrix].

[why-not-matrix]: https://telegra.ph/why-not-matrix-08-07

## GDPR in the federation

But even if that setting was enabled by default, how do you control
it? This is a fundamental problem of the federation: if any user is
allowed to join a room (which is the default), those user's servers
will log all content and metadata from that room. That includes
private, one-on-one conversations, since those are essentially rooms
as well.

In the context of the GDPR, this is really tricky: who is the
responsible party (known as the "data controller") here? It's
basically any yahoo who fires up a home server and joins a room.

In a federated network, one has to wonder whether GDPR enforcement is
even possible at all. But in Matrix in particular, if you want to
enforce your right to be forgotten in a given room, you would have to:

 1. enumerate all the users that ever joined the room while you were
    there
 2. discover all their home servers
 3. start a GDPR procedure against all those servers

I recognize this is a hard problem to solve while still keeping an
open ecosystem. But I believe that Matrix should have much stricter
defaults towards data retention than right now. Message expiry should
be enforced *by default*, for example. (Note that there are also
redaction policies that could be used to implement part of the GDPR
automatically, see the privacy policy discussion below on that.)

Also keep in mind that, in the brave new [peer-to-peer](https://github.com/matrix-org/pinecone) world that
Matrix is heading towards, the boundary between server and client is
likely to be fuzzier, which would make applying the GDPR even more difficult.

Update: [this comment](https://lobste.rs/s/ixa4vr/matrix_notes#c_nzqaqb) links to [this post (in german)](https://www.cr-online.de/blog/2022/06/02/ein-fehler-in-der-matrix/) which
apparently studied the question and concluded that Matrix is not
GDPR-compliant.

In fact, maybe Synapse should be designed so that there's no
configurable flag to turn off data retention. A bit like how most
system loggers in UNIX (e.g. syslog) come with a log retention system
that typically rotate logs after a few weeks or month. Historically,
this was designed to keep hard drives from filling up, but it also has
the added benefit of limiting the amount of personal information kept
on disk in this modern day. (Arguably, syslog doesn't rotate logs on
its own, but, say, Debian GNU/Linux, as an installed system, does have
log retention policies well defined for installed packages, and those
[can be discussed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=759382). And "no expiry" is definitely a bug.

## Matrix.org privacy policy

When I first looked at Matrix, five years ago, Element.io was called
[Riot.im](https://riot.im/) and had a [rather dubious privacy policy](https://web.archive.org/web/20170317115535/https://riot.im/privacy):

> We currently use cookies to support our use of Google Analytics on
> the Website and Service. Google Analytics collects information about
> how you use the Website and Service.
>
> [...]
>
> This helps us to provide you with a good experience when you
> browse our Website and use our Service and also allows us to improve
> our Website and our Service. 

When I asked Matrix people about why they were using Google Analytics,
they explained this was for development purposes and they were aiming
for velocity at the time, not privacy (paraphrasing here).

They also included a "free to snitch" clause:

> If we are or believe that we are under a duty to disclose or share
> your personal data, we will do so in order to comply with any legal
> obligation, the instructions or requests of a governmental authority
> or regulator, including those outside of the UK.

Those are really *broad* terms, above and beyond what is typically
expected legally.

Like the current retention policies, such user tracking and
... "liberal" collaboration practices with the state set a bad
precedent for other home servers.

Thankfully, since the above policy was published (2017), the GDPR was
"implemented" ([2018](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)) and it seems like both the [Element.io
privacy policy](https://element.io/privacy) and the [Matrix.org privacy policy](https://matrix.org/legal/privacy-notice) have been
somewhat improved since.

Notable points of the new privacy policies:

 * [2.3.1.1](https://matrix.org/legal/privacy-notice#2311-federation): the "federation" section actually outlines that
   "*Federated homeservers and Matrix clients which respect the Matrix
   protocol are expected to honour these controls and
   redaction/erasure requests, but other federated homeservers are
   outside of the span of control of Element, and we cannot guarantee
   how this data will be processed*"
 * [2.6](https://matrix.org/legal/privacy-notice#26-our-commitment-to-childrens-privacy): users under the age of 16 should *not* use the
   `matrix.org` service
 * [2.10](https://matrix.org/legal/privacy-notice#210-who-else-has-access-to-my-data): Upcloud, Mythic Beast, Amazon, and CloudFlare possibly
   have access to your data (it's nice to at least mention this in the
   privacy policy: many providers don't even bother admitting to this
   kind of delegation)
 * [Element 2.2.1](https://element.io/privacy): mentions many more third parties (Twilio,
   Stripe, [Quaderno](https://www.quaderno.io/), LinkedIn, Twitter, Google, [Outplay](https://www.outplayhq.com/),
   [PipeDrive](https://www.pipedrive.com/), [HubSpot](https://www.hubspot.com/), [Posthog](https://posthog.com/), Sentry, and [Matomo](https://matomo.org/)
   (phew!) used when you are paying Matrix.org for hosting

I'm not super happy with all the trackers they have on the Element
platform, but then again you don't have to use that service. Your
favorite homeserver (assuming you are not on Matrix.org) probably has
their own Element deployment, hopefully without all that garbage.

Overall, this is all a huge improvement over the previous privacy
policy, so hats off to the Matrix people for figuring out a reasonable
policy in such a tricky context. I particularly like this bit:

> We will forget your copy of your data upon your request. We will
> also forward your request to be forgotten onto federated
> homeservers. However - these homeservers are outside our span of
> control, so we cannot guarantee they will forget your data.

It's great they implemented those mechanisms and, after all, if
there's an hostile party in there, nothing can prevent them from using
*screenshots* to just exfiltrate your data away from the client side
anyways, even with services typically seen as more secure, like
Signal.

As an aside, I also appreciate that Matrix.org has a fairly decent
[code of conduct](https://matrix.org/legal/code-of-conduct), based on the [TODO CoC](https://web.archive.org/web/20160214165232/http://todogroup.org/opencodeofconduct/) which checks all the
[boxes in the geekfeminism wiki](https://geekfeminism.fandom.com/wiki/Code_of_conduct_evaluations).

## Metadata handling

Overall, privacy protections in Matrix mostly concern message
contents, not metadata. In other words, who's talking with who, when
and from where is not well protected. Compared to a tool like Signal,
which goes through great lengths to anonymize that data with features
like [private contact discovery][], [disappearing messages](https://signal.org/blog/disappearing-messages/),
[sealed senders](https://signal.org/blog/sealed-sender/), and [private groups](https://signal.org/blog/signal-private-group-system/), Matrix is definitely
behind. (Note: there is an [issue open about message lifetimes in
Element](https://github.com/vector-im/element-meta/issues/364) since 2020, but it's not at even at the MSC stage yet.)

[private contact discovery]: https://signal.org/blog/private-contact-discovery/

This is a [known issue](https://github.com/matrix-org/synapse/issues/4565) (opened in 2019) in Synapse, but this is
not just an implementation issue, it's a flaw in the protocol
itself. Home servers keep join/leave of all rooms, which gives clear
text information about who is talking to. Synapse logs may also
contain privately identifiable information that home server admins
might not be aware of in the first place. Those log rotation policies
are separate from the server-level retention policy, which may be
confusing for a novice sysadmin.

Combine this with the federation: even if you trust your home server
to do the right thing, the second you join a public room with
third-party home servers, those ideas kind of get thrown out because
those servers can do whatever they want with that information. Again,
a problem that is hard to solve in any federation.

To be fair, IRC doesn't have a great story here either: any *client*
knows not only who's talking to who in a room, but also typically
their client IP address. Servers *can* (and often do) *obfuscate*
this, but often that obfuscation is trivial to reverse. Some servers
do provide "cloaks" (sometimes automatically), but that's kind of a
"slap-on" solution that actually moves the problem elsewhere: now the
server knows a little more about the user.

Overall, I would worry much more about a Matrix home server seizure
than a IRC or Signal server seizure. [Signal does get subpoenas](https://signal.org/blog/looking-back-as-the-world-moves-forward/),
and they can only give out a tiny bit of information about their
users: their phone number, and their registration, and last connection
date. Matrix carries a *lot* more information in its database, and
a lot of that information is carried permanently, think
"blockchain-level" permanence.

## Amplification attacks on URL previews

I (still!) run an [Icecast](https://en.wikipedia.org/wiki/Icecast) server and sometimes share links to it
on IRC which, obviously, also ends up on (more than one!) Matrix home
servers because some people connect to IRC using Matrix. This, in
turn, means that Matrix will connect to that URL to generate a link
preview.

I feel this outlines a security issue, especially because those
sockets would be kept open seemingly *forever*. I tried to warn the
Matrix security team but somehow, I don't think this issue was taken
very seriously. Here's the disclosure timeline:

 * January 18: contacted Matrix security
 * January 19: response: already [reported as a bug](https://github.com/matrix-org/synapse/issues/8302)
 * January 20: response: can't reproduce
 * January 31: [timeout added][PR 11784], considered solved
 * January 31: I respond that I believe the security issue is
   underestimated, ask for clearance to disclose
 * February 1: response: asking for two weeks delay after the next
   release (1.53.0) including [another patch][PR 11936], presumably in two
   weeks' time
 * February 22: Matrix 1.53.0 released
 * April 14: I notice the release, ask for clearance again
 * April 14: response: referred to the [public disclosure](https://github.com/matrix-org/synapse/security/advisories/GHSA-4822-jvwx-w47h)

There are a couple of problems here:

 1. the bug was publicly disclosed in September 2020, and not
    considered a security issue until I notified them, and even then,
    I had to insist

 2. no clear disclosure policy timeline was proposed or seems
    established in the project (there is a [security disclosure
    policy](https://matrix.org/security-disclosure-policy/) but it doesn't include any predefined timeline)

 3. I wasn't informed of the disclosure

 4. the actual solution is a size limit (10MB, already implemented), a
    time limit (30 seconds, implemented in [PR 11784][]), and a
    content type allow list (HTML, "media" or JSON, implemented in [PR
    11936][]), and I'm not sure it's adequate

 5. (pure vanity:) I did not make it to their [Hall of fame](https://matrix.org/security-disclosure-policy/)

[PR 11784]: https://github.com/matrix-org/synapse/pull/11784
[PR 11936]: https://github.com/matrix-org/synapse/pull/11936

I'm not sure those solutions are adequate because they all seem to
assume a single home server will pull that one URL for a little while
then stop. But in a federated network, *many* (possibly thousands)
home servers may be connected in a single room at once. If an attacker
drops a link into such a room, *all* those servers would connect to
that link *all at once*. This is an amplification attack: a small
amount of traffic will generate a lot more traffic to a single
target. It doesn't matter there are size or time limits: the
amplification is what matters here.

It should also be noted that *clients* that generate link previews
have more amplification because they are more numerous than
servers. And of course, the default Matrix client (Element) *does*
generate link previews as well.

That said, this is possibly not a problem specific to Matrix: any
federated service that generates link previews may suffer from this.

I'm honestly not sure what the solution is here. Maybe moderation?
Maybe link previews are just evil? All I know is there was this weird
bug in my Icecast server and I tried to ring the bell about it, and it
feels it was swept under the rug. Somehow I feel this is bound to blow
up again in the future, even with the current mitigation.

Update: thinking about this again recently made me realize the problem
is not whether or not clients or server generate the link
previews. That's missing the core of the question: the real question
is whether the *sender* or the *receiver* generates link previews. In
Signal, for example, the *sender* generates the link preview, which
removes a whole class of attacks, including the above
denial-of-service attack, but also de-anonymization attacks (which I
haven't mentioned above, but that is still an issue in Matrix of
course).

In other words, right now, still, you can find out the IP address of
any Matrix user (or their home server, or both, depending on their
configuration) just by sharing a link with them. Neat no? (Of course,
a homeserver's IP address is normally discoverable, but there might be
a space in the future where it isn't or should be hidden behind a
proxy, for example. That still feels like a security issue, and that
still isn't something Matrix seem to care about.)

[Mastodon has the same problem](https://github.com/mastodon/mastodon/issues/23662), and [people are starting to notice](https://news.itsfoss.com/mastodon-link-problem/).

# Moderation

In Matrix like elsewhere, Moderation is a hard problem. There is a
[detailed moderation guide](https://matrix.org/docs/guides/moderation) and much of this problem space is
actively worked on in Matrix right now. A fundamental problem with
moderating a federated space is that a user banned from a room can
rejoin the room from another server. This is why spam is such a
problem in Email, and why IRC networks have stopped federating ages
ago (see [the IRC history](https://en.wikipedia.org/wiki/Internet_Relay_Chat#History) for that fascinating story).

Update: it's much worse than I imagined. Abuse is *really* easy on
Matrix, with simple attacks like flood-joining permanently crippling
rooms. According to [Why Not Matrix][why-not-matrix] this is a problem that's been
known for years, a fundamental design flaw that's barely acknowledged,
but usually swept under the rug by Matrix people.

## The mjolnir bot

The [mjolnir moderation bot](https://github.com/matrix-org/mjolnir) is designed to help with some of those
things. It can kick and ban users, redact all of a user's message (as
opposed to one by one), all of this across multiple rooms. It can also
subscribe to a federated block list published by `matrix.org` to block
known abusers (users or servers). Bans are [pretty flexible](https://github.com/matrix-org/mjolnir/blob/main/docs/moderators.md#bans) and
can operate at the user, room, or server level.

Matrix people suggest making the bot admin of your channels, because
you can't take back admin from a user once given.

## The command-line tool

There's also a [new command line tool](https://git.fout.re/pi/matrixadminhelpers) designed to do things like:

> * System notify users (all users/users from a list, specific user)
> * delete sessions/devices not seen for X days
> * purge the remote media cache
> * select rooms with various criteria (external/local/empty/created by/encrypted/cleartext)
> * purge history of theses rooms
> * shutdown rooms

This tool and Mjolnir are based on the [admin API](https://matrix-org.github.io/synapse/latest/usage/administration/admin_api/index.html) built into
Synapse.

## Rate limiting

Synapse has pretty good [built-in rate-limiting](https://github.com/matrix-org/synapse/blob/28989cb301fecf5a669a634c09bc2b73f97fec5d/docs/sample_config.yaml#L879-L996) which blocks
repeated login, registration, joining, or messaging attempts. It may
also end up throttling servers on the federation based on those
settings.

## Fundamental federation problems

Because users joining a room may come from another server, room
moderators are at the mercy of the registration and moderation
policies of those servers. Matrix is like IRC's `+R` mode ("only
registered users can join") by default, except that anyone can
register their own homeserver, which makes this limited.

Server admins can block IP addresses and home servers, but those tools
are not easily available to room admins. There is an API
(`m.room.server_acl` in `/devtools`) but it is [not reliable](https://github.com/matrix-org/matrix-spec/issues/928)
(thanks Austin Huang for the clarification).

Matrix has the concept of guest accounts, but it is not used very
much, and virtually no client or homeserver supports it. This contrasts with the way
IRC works: by default, anyone can join an IRC network even without
authentication. Some channels require registration, but in general you
are free to join and look around (until you get blocked, of course).

I have [seen anecdotal evidence](https://twitter.com/matrixdotorg/status/1542065092048654337) (CW: Twitter, [nitter link](https://nitter.it/matrixdotorg/status/1542065092048654337)) that "moderating bridges is hell", and
I can imagine why. Moderation is already hard enough on one
federation, when you bridge a room with another network, you inherit
all the problems from *that* network but without the entire abuse
control tools from the original network's API...

## Room admins

Matrix, in particular, has the problem that room administrators (which
have the power to redact messages, ban users, and promote other users)
are bound to their Matrix ID which is, in turn, bound to their home
servers. This implies that a home server administrators could (1)
impersonate a given user and (2) use that to hijack the room. So in
practice, the home server is the trust anchor for rooms, not the user
themselves.

That said, if server B administrator hijack user `joe` on server B,
they will hijack that room *on that specific server*. This will not
(necessarily) affect users on the other servers, as servers could
refuse parts of the updates or ban the compromised account (or
server).

It does seem like a major flaw that room credentials are bound to
Matrix identifiers, as opposed to the E2E encryption credentials.  In
an encrypted room even with fully verified members, a compromised or
hostile home server can still take over the room by impersonating an
admin. That admin (or even a newly minted user) can then send events
or listen on the conversations.

This is even more frustrating when you consider that Matrix events are
actually [signed](https://spec.matrix.org/latest/#architecture) and therefore have *some* authentication attached
to them, acting like some sort of Merkle tree (as it contains a link
to previous events). That signature, however, is made from the
homeserver PKI keys, *not* the client's E2E keys, which makes E2E feel
like it has been "bolted on" later.

Update: note that this flaw has actually been called a "[serious
vulnerability](https://arstechnica.com/information-technology/2022/09/matrix-patches-vulnerabilities-that-completely-subvert-e2ee-guarantees/)" by Ars Technica, based on a [paper from
researchers](https://nebuchadnezzar-megolm.github.io/) that confirmed the flaw I hinted at above. The
[response from Matrix.org](https://matrix.org/blog/2022/09/28/upgrade-now-to-address-encryption-vulns-in-matrix-sdks-and-clients) has been rather underwhelming, with many
issues still unaddressed.

# Availability

While Matrix has a strong advantage over Signal in that it's
decentralized (so anyone can run their own homeserver,), I couldn't
find an easy way to run a "multi-primary" setup, or even a "redundant"
setup (even if with a single primary backend), short of going full-on
"replicate PostgreSQL and Redis data", which is not typically for the
faint of heart.

## How this works in IRC

On IRC, it's quite easy to setup redundant nodes. All you need is:

 1. a new machine (with it's own public address with an open port)
 
 2. a shared secret (or certificate) between that machine and an
    existing one on the network

 3. a `connect {}` block on both servers

That's it: the node will join the network and people can connect to it
as usual and share the same user/namespace as the rest of the
network. The servers take care of synchronizing state: you do not need
to worry about replicating a database server.

(Now, experienced IRC people will know there's a catch here: IRC
doesn't have authentication built in, and relies on "services" which
are basically bots that authenticate users (I'm simplifying, don't
nitpick). If *that* service goes down, the network still works, but
then people can't authenticate, and they can start doing nasty things
like steal people's identity if they get knocked offline. But still:
basic functionality still works: you can talk in rooms and with users
that are on the reachable network.)

## User identities

Matrix is more complicated. Each "home server" has its own identity
namespace: a specific user (say `@anarcat:matrix.org`) is bound to
that specific home server. If that server goes down, that user is
completely disconnected. They *could* register a new account elsewhere
and reconnect, but then they basically lose all their configuration:
contacts, joined channels are all lost.

(Also notice how the Matrix IDs don't look like a typical user address
like an email in XMPP. They at least did their homework and got [the
allocation for the scheme](https://www.iana.org/assignments/uri-schemes/prov/matrix).)

## Rooms

Users talk to each other in "rooms", even in one-to-one
communications. (Rooms are also used for other things like "spaces",
they're basically used for everything, think "everything is a file"
kind of tool.) For rooms, home servers act more like IRC nodes in that
they keep a local state of the chat room and synchronize it with other
servers. Users can keep talking inside a room if the server that
originally hosts the room goes down. Rooms can have a local,
server-specific "[alias](https://spec.matrix.org/latest/#room-aliases)" so that, say, `#room:matrix.org` is also
visible as `#room:example.com` on the `example.com` home server. Both
addresses refer to the same room underlying room.

(Finding this in the Element settings is not obvious though, because
that "alias" are actually called a "local address" there. So to create
such an alias (in Element), you need to go in the room settings'
"General" section, "Show more" in "Local address", then add the alias
name (e.g. `foo`), and then that room will be available on your
`example.com` homeserver as `#foo:example.com`.)

So a room doesn't belong to a server, it belongs to the federation,
and anyone can join the room from any server (if the room is public, or
if invited otherwise). You can create a room on server A and when a
user from server B joins, the room will be replicated on server B as
well. If server A fails, server B will keep relaying traffic to
connected users and servers.

A room is therefore not fundamentally addressed with the above alias.
Instead, it has a internal Matrix ID, which basically a random
string. It has a server name attached to it, but that was made just to
avoid collisions. That can get a little confusing. For example, the
`#fractal:gnome.org` room is an alias on the `gnome.org` server, but
the room ID is `!hwiGbsdSTZIwSRfybq:matrix.org`. That's because the
room was created on `matrix.org`, but the preferred branding is
`gnome.org` now.

As an aside, rooms, by default, live forever, even after the last user
quits. There's an [admin API to delete rooms](https://matrix-org.github.io/synapse/latest/admin_api/rooms.html#version-2-new-version) and a [tombstone
event](https://spec.matrix.org/v1.2/client-server-api/#events-17) to redirect to another one, but neither have a GUI yet. The
latter is part of [MSC1501](https://github.com/matrix-org/matrix-spec-proposals/blob/main/proposals/1501-room-version-upgrades.md) ("Room version upgrades") which allows
a room admin to close a room, with a message and a pointer to another
room.

## Spaces

Discovering rooms can be tricky: there *is* a per-server room
directory, but Matrix.org people are trying to deprecate it in favor
of "Spaces". Room directories were ripe for abuse: anyone can create a
room, so anyone can show up in there. It's possible to restrict who
can add aliases, but anyways directories were seen as too limited.

In contrast, a "Space" is basically a room that's an index of other
rooms (including other spaces), so existing moderation and
administration mechanism that work in rooms can (somewhat) work in
spaces as well. This enables a room directory that works across
federation, regardless on which server they were originally created.

New users can be added to a space or room [automatically](https://github.com/matrix-org/synapse/blob/12d1f82db213603972d60be3f46f6a36c3c2330f/docs/sample_config.yaml#L1378-L1388) in
Synapse. (Existing users can be told about the space with a server
notice.) This gives admins a way to pre-populate a list of rooms on a
server, which is useful to build clusters of related home servers,
providing *some* sort of redundancy, at the room -- not user -- level.

## Home servers

So while you can workaround a home server going down at the room
level, there's no such thing at the home server level, for user
identities. So if you want those identities to be stable in the long
term, you need to think about high availability. One limitation is
that the domain name (e.g. `matrix.example.com`) must never change in
the future, as [renaming home servers is not supported](https://matrix.org/faq/#why-can't-i-rename-my-homeserver%3F).

The documentation used to say you could "run a hot spare" but that has
been [removed](https://github.com/matrix-org/synapse/issues/7076). Last I heard, it was [not possible to run a
high-availability setup](https://www.reddit.com/r/matrixdotorg/comments/fh63p8/how_to_set_up_matrix_synapse_in_a_high/) where multiple, separate locations could
replace each other automatically. You can have high *performance*
setups where the load gets [distributed among workers](https://github.com/matrix-org/synapse/blob/master/docs/workers.md), but those
are based on a shared database (Redis and PostgreSQL) backend.

So my guess is it would be possible to create a "warm" spare server of
a matrix home server with [regular PostgreSQL replication](https://www.postgresql.org/docs/13/warm-standby.html#STREAMING-REPLICATION), but
that is not documented in the [Synapse manual](https://matrix-org.github.io/synapse/latest/?search=backup). This sort of setup
would also not be useful to deal with networking issues or denial of
service attacks, as you will not be able to spread the load over
multiple network locations easily. Redis and PostgreSQL heroes are
welcome to provide their multi-primary solution in the comments. In
the meantime, I'll just point out this is a solution that's handled
somewhat more gracefully in IRC, by having the possibility of
delegating the authentication layer.

Update: this was previously undocumented, but not only can you scale
the frontend workers to multiple hosts, you can also *shard* the
backend so that tables are distributed across multiple database
hots. This has been [documented only on 2022-07-11](https://github.com/matrix-org/synapse/pull/13212/), weeks after
this article was written, so you will forgive me for that omission,
hopefully. Obviously, this doesn't resolve the "high availability"
scenario since you still have a central server for that data, but it
might help resolving performance problems for very large instances.

## Delegations

If you do not want to run a Matrix server yourself, it's possible to
delegate the entire thing to another server. There's a [server
discovery API](https://spec.matrix.org/v1.2/server-server-api/#server-discovery) which uses the `.well-known` pattern (or SRV
records, but that's "not recommended" and [a bit confusing](https://github.com/matrix-org/synapse/issues/8982)) to
delegate that service to another server. Be warned that the server
still needs to be explicitly configured for your domain. You can't
just put:

    { "m.server": "matrix.org:443" }

... on `https://example.com/.well-known/matrix/server` and start using
`@you:example.com` as a Matrix ID. That's because Matrix doesn't
support "virtual hosting" and you'd still be connecting to rooms and
people with your `matrix.org` identity, not `example.com` as you would
normally expect. This is also why you cannot [rename your home
server](https://matrix.org/faq/#why-can't-i-rename-my-homeserver%3F).

The server discovery API is what allows servers to find each
other. Clients, on the other hand, use the [client-server discovery
API](https://spec.matrix.org/v1.2/client-server-api/#server-discovery): this is what allows a given client to find your home server
when you type your Matrix ID on login.

# Performance

The high availability discussion brushed over the performance of
Matrix itself, but let's now dig into that.

## Horizontal scalability

There were serious scalability issues of the main Matrix server,
[Synapse](https://github.com/matrix-org/synapse/), in the past. So the Matrix team has been working hard to
improve its design. Since Synapse 1.22 the home server can
horizontally scale to multiple workers (see [this blog post for details](https://matrix.org/blog/2020/11/03/how-we-fixed-synapses-scalability))
which can make it easier to scale large servers.

## Other implementations

There are other promising home servers implementations from a
performance standpoint ([dendrite](https://github.com/matrix-org/dendrite), Golang, [entered beta in late
2020](https://matrix.org/blog/2020/10/08/dendrite-is-entering-beta); [conduit](https://gitlab.com/famedly/conduit), Rust, beta; [others](https://matrix.org/faq/#can-i-write-a-matrix-homeserver%3F)), but none of those
are feature-complete so there's a trade-off to be made there.  Synapse
is also adding a lot of feature fast, so it's an open question whether
the others will ever catch up. (I have heard that Dendrite might
actually [surpass Synapse in features within a few years](https://arewep2pyet.com/), which would
put Synapse in a more "LTS" situation.)

## Latency

Matrix can feel slow sometimes. For example, joining the "Matrix HQ"
room in Element (from `matrix.debian.social`) takes a few *minutes*
and then *fails*. That is because the home server has to sync the
entire room state when you join the room. There was promising work on
this announced in the [lengthy 2021 retrospective](https://matrix.org/blog/2021/12/22/the-mega-matrix-holiday-special-2021#sync-v3), and some of
that work landed ([partial sync](https://github.com/matrix-org/matrix-doc/pull/3706)) in the [1.53 release](https://matrix.org/blog/2022/02/22/synapse-1-53-released) already.
Other improvements coming include [sliding sync](https://github.com/matrix-org/matrix-spec-proposals/blob/kegan/sync-v3/proposals/3575-sync.md), [lazy loading
over federation](https://github.com/matrix-org/matrix-spec-proposals/pull/2775), and [fast room joins](https://github.com/matrix-org/synapse/milestone/6). So that's actually
something that could be fixed in the fairly short term.

But in general, communication in Matrix doesn't feel as "snappy" as on
IRC or even Signal. It's hard to quantify this without instrumenting a
full latency test bed (for example the tools I used in the [[terminal
emulators latency tests|blog/2018-05-04-terminal-emulators-2/]]), but
even just typing in a web browser feels slower than typing in a xterm
or Emacs for me.

Even in conversations, I "feel" people don't immediately respond as
fast. In fact, this could be an interesting double-blind experiment to
make: have people guess whether they are talking to a person on
Matrix, XMPP, or IRC, for example. My theory would be that people
could notice that Matrix users are slower, if only because of the TCP
round-trip time each message has to take.

## Transport

Some [courageous person](https://blog.lewman.com/) actually made some [tests of various
messaging platforms on a congested network](https://blog.lewman.com/internet-messaging-versus-congested-network.html). His evaluation was
basically:

 * [Briar](https://briarproject.org/): uses Tor, so unusable except locally
 * Matrix: "struggled to send and receive messages", joining a room
   takes forever as it has to sync all history, "took 20-30 seconds
   for my messages to be sent and another 20 seconds for further
   responses"
 * [XMPP](https://xmpp.org/): "worked in real-time, full encryption, with nearly zero
   lag"

So that was interesting. I suspect IRC would have also fared better,
but that's just a feeling.

Other improvements to the transport layer include [support for
websocket](https://github.com/matrix-org/matrix-doc/issues/1148) and the [CoAP proxy](https://matrix.org/docs/projects/iot/coap-proxy) work [from 2019](https://matrix.org/blog/2019/03/12/breaking-the-100-bps-barrier-with-matrix-meshsim-coap-proxy) (targeting
100bps links), but both seem stalled at the time of writing. The
Matrix people have also [announced](https://matrix.org/blog/2021/05/06/introducing-the-pinecone-overlay-network) the [pinecone p2p overlay
network](https://github.com/matrix-org/pinecone) which aims at solving large, internet-scale routing
problems. See also [this talk at FOSDEM 2022](https://www.youtube.com/watch?v=diwzQtGgxU8&list=PLl5dnxRMP1hW7HxlJiHSox02MK9_KluLH&index=19).

Update: there is a formal proposal ([MSC3079](https://github.com/matrix-org/matrix-spec-proposals/pull/3079)) about a
low-bandwidth, UDP-based transport, but it's been stuck at that stage
for a few years now, because of security issues inherent to the UDP
protocol, at first glance. There's also a [golang implementation](https://github.com/matrix-org/lb).

# Usability

## Onboarding and workflow

The workflow for joining a room, when you use Element web, is not
great:

 1. click on a link in a web browser
 2. land on (say) <https://matrix.to/#/#matrix-dev:matrix.org>
 3. offers "Element", yeah that's sounds great, let's click "Continue"
 4. land on
    <https://app.element.io/#/room%2F%23matrix-dev%3Amatrix.org> and
    then you need to register, aaargh

As you might have guessed by now, there is a [specification](https://github.com/matrix-org/matrix-spec-proposals/blob/f295e828dc3107260a7979c40175442bf3a7fdc4/proposals/2312-matrix-uri.md) to
solve this, but web browsers need to adopt it as well, so that's far
from actually being solved. At least browsers generally know about the
`matrix:` scheme, it's just not exactly clear what they should do with
it, especially when the handler is just another web page (e.g. Element
web).

In general, when compared with tools like Signal or WhatsApp, Matrix
doesn't fare so well in terms of user discovery. I probably have some
of my normal contacts that have a Matrix account as well, but there's
really *no way* to know. It's kind of creepy when Signal tells you
"this person is on Signal!" but it's also pretty cool that it works,
and they actually implemented it [pretty well][private contact discovery].

Registration is also less obvious: in Signal, the app confirms your
phone number automatically. It's friction-less and quick. In Matrix,
you need to learn about home servers, pick one, register (with a
password! aargh!), and then setup encryption keys (not default),
etc. It's a lot more friction.

And look, I understand: giving away your phone number is a *huge*
trade-off. I don't like it either. But it solves a real problem and
makes encryption accessible to a *ton* more people. Matrix *does* have
"identity servers" that can serve that purpose, but I don't feel
confident sharing my phone number there. It doesn't help that the
identity servers don't have private contact discovery: giving them
your phone number is a more serious security compromise than with
Signal.

There's a catch-22 here too: because no one feels like giving away
their phone numbers, no one does, and everyone assumes that stuff
doesn't work anyways. Like it or not, Signal *forcing* people to
divulge their phone number actually gives them critical mass that
means actually a lot of my relatives *are* on Signal and I don't have
to install crap like WhatsApp to talk with them.

## 5 minute clients evaluation

Throughout all my tests I evaluated a handful of Matrix clients,
mostly from [Flathub](https://flathub.org/) because almost none of them are packaged in
Debian.

Right now I'm using Element, the flagship client from Matrix.org, in a
web browser window, with the [PopUp Window extension](https://github.com/ettoolong/PopupWindow). This makes
it look almost like a native app, and opens links in my main browser
window (instead of a new tab in that separate window), which is
nice. But I'm tired of buying memory to feed my web browser, so this
indirection has to stop. Furthermore, I'm often getting completely
logged off from Element, which means re-logging in, recovering my
security keys, and reconfiguring my settings. That is extremely
annoying.

Coming from Irssi, Element is really "GUI-y" (pronounced
"gooey"). Lots of clickety happening. To mark conversations as read,
in particular, I need to click-click-click on *all* the tabs that have
some activity. There's no "jump to latest message" or "mark all as
read" functionality as far as I could tell. In Irssi the former is
built-in (<kbd>alt-a</kbd>) and I made a custom `/READ` command for
the latter:

    /ALIAS READ script exec \$_->activity(0) for Irssi::windows

And yes, that's a Perl script in my IRC client. I am not aware of any
Matrix client that does stuff like that, except maybe Weechat, if we
can call it a Matrix client, or Irssi itself, now that it [has a
Matrix plugin](https://codeberg.org/ticho/irssi-matrix) (!).

As for other clients, I have looked through the [Matrix Client
Matrix](https://matrix.org/clients-matrix/) (confusing right?) to try to figure out which one to try,
and, even after selecting `Linux` as a filter, the chart is just too
wide to figure out anything. So I tried those, kind of randomly:

 * [FluffyChat](https://fluffychat.im/) (Dart, Flutter not in Debian, [Flatpak](https://flathub.org/apps/im.fluffychat.Fluffychat), solid
   on Android, also on iOS, works well on desktop too, [segfaults on
   calling](https://github.com/flathub/im.fluffychat.Fluffychat/issues/107), calls don't work on F-Droid)
 * [Cinny](https://cinny.in/) (Javascript/Typescript, electron-based, not in Debian,
   interesting, "Slack-like" interface, failed to login to Salsa
   because of no 2FA)
 * [Fractal](https://gitlab.gnome.org/GNOME/fractal) (Rust, [not in Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=900928), no support for spaces)
 * [Mirage](https://github.com/mirukana/mirage) (not in Debian, inactive upstream since 2021, forked
   into [Moment](https://mx-moment.xyz/))
 * [Nheko](https://github.com/Nheko-Reborn/nheko) (C++, Python, Qt) [![Debian Unstable
   package](https://repology.org/badge/version-for-repo/debian_unstable/nheko.svg)](https://repology.org/project/nheko/versions),
   not as good as Fluffy
 * [Quaternion](https://github.com/quotient-im/Quaternion) (C++, Qt) [![Debian Unstable package](https://repology.org/badge/version-for-repo/debian_unstable/quaternion.svg)](https://repology.org/project/quaternion/versions)

Unfortunately, I lost my notes on those, I don't actually remember
which one did what. I still have a session open with Mirage, so I
guess that means it's the one I preferred, but I remember they were
also all very GUI-y.

Maybe I need to look at `weechat-matrix` or `gomuks`. At least Weechat
is scriptable so I could continue playing the power-user. Right now my
strategy with messaging (and that includes microblogging like Twitter
or Mastodon) is that everything goes through my IRC client, so Weechat
could actually fit well in there. Going with `gomuks`, on the other
hand, would mean running it in parallel with Irssi or ... ditching
IRC, which is a leap I'm not quite ready to take just yet.

Oh, and basically none of those clients (except Nheko and Element)
support VoIP, which is still kind of a second-class citizen in
Matrix. It does not support large multimedia rooms, for example:
[Jitsi was used for FOSDEM](https://matrix.org/blog/2022/02/07/hosting-fosdem-2022-on-matrix/) instead of the native videoconferencing
system.

Update: I'm now (2024) using FluffyChat on desktop (through Flatpak)
and Android (through F-Droid).

Other interesting clients:

 - [ement.el](https://github.com/alphapapa/ement.el): Emacs clietn
 - [gomuks][]: TUI, golang, E2E
 - [iamb](https://iamb.chat/): TUI, threads, spaces, E2E, image previews, bell
   notification, message redaction, multiple profiles, vim keybindings

[gomuks]: https://github.com/tulir/gomuks/

## Bots

This falls a little aside the "usability" section, but I didn't know
where to put this... There's a few Matrix bots out there, and you are
likely going to be able to replace your existing bots with Matrix
bots. It's true that IRC has a long and impressive history with lots
of various bots doing various things, but given how young Matrix is,
there's still a good variety:

 * [maubot](https://github.com/maubot/maubot): generic bot with tons of usual plugins like sed, dice,
   karma, xkcd, echo, rss, reminder, translate, react, exec,
   gitlab/github webhook receivers, weather, etc, see [this huge
   list](https://plugins.mau.bot/), related bots:
   - [mautrix-telegram](https://github.com/mautrix/telegram): [recommended by sergiodj](https://blog.sergiodj.net/posts/chatting-21st-century/)
   - [gitlab](https://github.com/maubot/gitlab): same
 * [matrix-nio](https://github.com/poljar/matrix-nio): another framework, used to build [lots more
   bots](https://matrix-nio.readthedocs.io/en/latest/examples.html) like:
   * [hemppa](https://github.com/vranki/hemppa): generic bot with various functionality like weather,
     RSS feeds, calendars, cron jobs, OpenStreetmaps lookups, URL
     title snarfing, wolfram alpha, astronomy pic of the day, Mastodon
     bridge, room bridging, oh dear
   * [devops](https://github.com/rdagnelie/devops-bot): ping, curl, etc
   * [podbot](https://github.com/interfect/podbot): play podcast episodes from AntennaPod
   * [cody](https://gitlab.com/carlbordum/matrix-cody): Python, Ruby, Javascript REPL
   * [eno](https://github.com/8go/matrix-eno-bot): generic bot, "personal assistant"
   * [ChatGPT](https://github.com/h1ddenpr0cess20/infinigpt-matrix) and [Ollama](https://github.com/h1ddenpr0cess20/ollamarama-matrix) AI chat bots
   * [reminder bot](https://github.com/anoadragon453/matrix-reminder-bot)
   * [opsdroid](https://github.com/opsdroid/opsdroid): framework to implement "chat ops" in Matrix,
     connects with Matrix, GitHub, GitLab, Shell commands, Slack, etc
   * [commander](https://github.com/8go/matrix-commander): CLI interface
   * [full list](https://matrix-nio.readthedocs.io/en/latest/examples.html#projects-built-with-nio)
 * [mjolnir](https://github.com/matrix-org/mjolnir): moderation bot
 * [hookshot](https://github.com/Half-Shot/matrix-hookshot): bridge with GitLab/GitHub
 * [matrix-monitor-bot](https://github.com/turt2live/matrix-monitor-bot): latency monitor
 * [matrix-debate-bot](https://gitlab.com/imbev/matrix-debate-bot): simple timer
 * [nimb](https://github.com/susam/nimb): NIMB IRC Matrix Bridge, sock puppet
 * [gh-bot](https://git.sr.ht/~jae/gh-bot): GitHub, GitLab, Gitea webhook processor
 * [buscarron](https://gitlab.com/etke.cc/buscarron): web form (HTTP POST) to Matrix
 * [honoroit](https://gitlab.com/etke.cc/honoroit): help desk bot
 * [matrix-registration-bot](https://github.com/moan0s/matrix-registration-bot): invite token registration process
 * [matrix-releasetracker](https://github.com/ananace/matrix-releasetracker): track releases on Git, GitLab, GitHub repositories
 * [remarkable-matrix](https://gitlab.com/ptman/remarkable-matrix): [Remarkable](https://remarkable.com/) sync with Matrix
 * [matrix-menu-bot](https://gitlab.com/ptman/matrix-menu-bot): a menu-based bot, derived from polls
 * [postmoogle](https://gitlab.com/etke.cc/postmoogle): email gateway
 * [cdj-matrix](https://lab.trax.im/matrix/cdj-to-matrix): bridge with [Class Dojo](https://classdojo.com/)

One thing I haven't found an equivalent for is Debian's
[MeetBot](https://wiki.debian.org/MeetBot). There's an [archive bot](https://github.com/russelldavies/matrix-archive) but it doesn't have topics
or a meeting chair, or HTML logs.

Update: it's not a bot but [progval/matrix2051](https://github.com/progval/matrix2051) is quite
interesting for me, as a long-time IRC user: it's a homeserver gateway
that presents itself as an IRC server. So you can treat Matrix as one
big weird IRC server. Main limitation is DMs are basically broken, but
lack of TLS also keeps it from being useful as a drop-in replacement
for migrating an existing IRC network.

## Working on Matrix

As a developer, I find Matrix kind of intimidating. The specification
is *huge*. The [official specification itself](https://spec.matrix.org/latest/) looks somewhat
digestable: it's only 6 APIs so that looks, at first, kind of
reasonable. But whenever you start asking complicated questions about
Matrix, you quickly fall into the [Matrix Spec Change](https://spec.matrix.org/v1.2/proposals/)
specification (which, yes, is a separate specification). And there are
literally *hundreds* of MSCs flying around. It's hard to tell
what's been adopted and what hasn't, and even harder to figure out if
*your* specific client has implemented it.

(One trendy answer to this problem is to "rewrite it in rust": Matrix
are working on implementing a lot of those specifications in a
[matrix-rust-sdk](https://github.com/matrix-org/matrix-rust-sdk) that's designed to take the implementation
details away from users.)

Just taking the [latest weekly Matrix report](https://matrix.org/blog/2022/05/27/this-week-in-matrix-2022-05-27#dept-of-spec-), you find that
*three* new MSCs proposed, just last week! There's even a graph that
shows the number of MSCs is progressing steadily, at 600+ proposals
total, with the majority (300+) "new". I would guess the "merged" ones
are at about 150. 

That's a lot of text which includes stuff like [3D worlds](https://github.com/matrix-org/matrix-spec-proposals/pull/3815) which,
frankly, I don't think you should be working on when you have such
important security and usability problems. (The internet as a whole,
arguably, doesn't fare much better. [RFC600](https://datatracker.ietf.org/doc/html/rfc600) is a really obscure
discussion about "INTERFACING AN ILLINOIS PLASMA TERMINAL TO THE
ARPANET". Maybe that's how many MSCs will end up as well, left
forgotten in the pits of history.)

And that's the thing: maybe the Matrix people have a different
objective than I have. They want to connect everything to everything,
and make Matrix a generic transport for all sorts of applications,
including virtual reality, collaborative editors, and so on.

I just want secure, simple messaging. Possibly with good file
transfers, and video calls. That it works with existing stuff is good,
and it should be federated to remove the "Signal point of
failure". So I'm a bit worried with the direction all those MSCs are
taking, *especially* when you consider that clients other than Element
are still struggling to keep up with basic features like end-to-end
encryption or room discovery, never mind voice or spaces...

# Conclusion

Overall, Matrix is somehow in the space XMPP was a few years ago. It
has a *ton* of features, pretty good clients, and a large
community. It seems to have gained some of the momentum that XMPP has
lost. It could be seen as having the most potential to replace Signal
if something bad would happen to it (like, I don't know, [getting
banned](https://signal.org/blog/earn-it/) or [going nuts with cryptocurrency](https://signal.org/blog/update-on-beta-testing-payments/))...

But it's really not there yet, and I don't see Matrix trying to get
there either, which is a bit worrisome.

## Looking back at history

I'm also worried that we are repeating the errors of the past. The
history of federated services is really fascinating:. IRC, FTP, HTTP,
and SMTP were all created in the early days of the internet, and are
all still around (except, arguably, FTP, which was removed from major
browsers recently). All of them had to face serious challenges in
growing their federation.

IRC had *numerous* conflicts and forks, both at the technical level
but also at the *political* level. [The history of IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat#History) is really
something that *anyone* working on a federated system should study in
detail, because they are *bound* to make the same mistakes if they are
not familiar with it. The "short" version is:

 * 1988: Finnish researcher publishes first IRC source code
 * 1989: 40 servers worldwide, mostly universities
 * 1990: EFnet ("eris-free network") fork which blocks the "open
   relay", named [Eris][] - followers of Eris form the A-net, which
   promptly dissolves itself, with only EFnet remaining
 * 1992: Undernet fork, which offered authentication ("services"),
   routing improvements and timestamp-based channel synchronisation
 * 1994: DALnet fork, from Undernet, again on a technical disagreement
 * 1995: Freenode founded
 * 1996: IRCnet forks from EFnet, following a flame war of historical
   proportion, splitting the network between Europe and the Americas
 * 1997: Quakenet founded
 * 1999: (XMPP founded)
 * 2001: 6 million users, OFTC founded
 * 2002: DALnet peaks at 136,000 users
 * 2003: IRC as a whole peaks at 10 million users, EFnet peaks at
   141,000 users
 * 2004: (Facebook founded), Undernet peaks at 159,000 users
 * 2005: Quakenet peaks at 242,000 users, IRCnet peaks at 136,000
   (Youtube founded)
 * 2006: (Twitter founded)
 * 2009: (WhatsApp, Pinterest founded)
 * 2010: (TextSecure AKA Signal, Instagram founded)
 * 2011: (Snapchat founded)
 * ~2013: Freenode peaks at ~100,000 users
 * 2016: IRCv3 standardisation effort started (TikTok founded)
 * 2021: Freenode self-destructs, Libera chat founded
 * 2022: Libera peaks at 50,000 users, OFTC peaks at 30,000 users

(The numbers were taken from the Wikipedia page and
[Netsplit.de](https://netsplit.de/). Note that I also include other networks launch in
parenthesis for context.)

Pretty dramatic, don't you think? Eventually, somehow, IRC became
irrelevant for most people: few people are even aware of it now. With
less than a million users active, it's smaller than Mastodon, XMPP, or
Matrix at this point.[^1] If I were to venture a guess, I'd say that
infighting, lack of a standardization body, and a somewhat annoying
protocol meant the network could not grow. It's also possible that the
decentralised yet centralised structure of IRC networks limited their
reliability and growth.

But large social media companies have also taken over the space:
observe how IRC numbers peak around the time the wave of large social
media companies emerge, especially Facebook (2.9B users!!) and Twitter
(400M users).

[Eris]: https://en.wikipedia.org/wiki/Eris_(mythology)

## Where the federated services are in history

Right now, Matrix, and Mastodon (and email!) are at the
"pre-[EFnet](https://en.wikipedia.org/wiki/EFnet)" stage: anyone can join the federation. Mastodon has
started working on a global block list of fascist servers which is
interesting, but it's still an open federation. Right now, Matrix is
totally open, but `matrix.org` publishes a (federated) block list
of hostile servers (`#matrix-org-coc-bl:matrix.org`, yes, of course
it's a room).

Interestingly, Email is *also* in that stage, where there are block
lists of spammers, and it's a race between those blockers and
spammers. Large email providers, obviously, are getting closer to the
EFnet stage: you could consider they only accept email from themselves
or between themselves. It's getting increasingly hard to deliver mail
to Outlook and Gmail for example, partly because of bias against small
providers, but also because they are including more and more
machine-learning tools to sort through email and those systems are,
fundamentally, unknowable. It's not *quite* the same as splitting the
federation the way EFnet did, but the effect is similar.

HTTP has somehow managed to live in a parallel universe, as it's
technically still completely federated: anyone can start a web server
if they have a public IP address and anyone can connect to it. The
catch, of course, is how you find the darn thing. Which is how Google
became one of the most powerful corporations on earth, and how they
became the gatekeepers of human knowledge online.

I have only briefly mentioned XMPP here, and my XMPP fans will
undoubtedly comment on that, but I think it's somewhere in the middle
of all of this. It was [co-opted](https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish) by Facebook *and* Google, and
both corporations have abandoned it to its fate. I remember fondly the
days where I could do instant messaging with my contacts who had a
Gmail account. Those days are gone, and I don't talk to anyone over
Jabber anymore, unfortunately. And this is a threat that Matrix still
has to face.

It's also the threat Email is currently facing. On the one hand
corporations like Facebook want to completely destroy it and have
mostly succeeded: many people just have an email account to
register on things and talk to their friends over Instagram or
(lately) TikTok (which, I know, is not Facebook, but they started that
fire). 

On the other hand, you have corporations like Microsoft and Google who
are *still* using and providing email services — because, frankly, you
still *do* need email for stuff, just like [fax](https://en.wikipedia.org/wiki/Fax) is still around —
but they are more and more isolated in their own silo. At this point,
it's only a matter of time they reach critical mass and just decide
that the risk of allowing external mail coming in is not worth the
cost. They'll simply flip the switch and work on an allow-list
principle. Then we'll have closed the loop and email will be
dead, just like IRC is "dead" now.

I wonder which path Matrix will take. Could it liberate us from these
vicious cycles?

# Updates

## Other discussions

This generated some discussions on [lobste.rs](https://lobste.rs/s/ixa4vr/matrix_notes) and [Hacker
News](https://news.ycombinator.com/item?id=31782717).

## External research on Matrix privacy, exhibit A

I have also found a [research on Matrix's privacy](https://gitlab.com/libremonde-org/papers/research/privacy-matrix.org) which touches on
some of the issues outlined here. Unfortunately, it seems that
research is rather heavily biased; it was written by the
[libremonde.org](https://www.libremonde.org/) people who work on the [Grid protocol](https://gitlab.com/thegridprotocol/home#the-grid-protocol), a fork
of Matrix that seems to be [pretty much dead](https://gitlab.com/thegridprotocol/home/-/issues/4) anyways.

I have found that research through [this post on hackea.org](https://hackea.org/notas/matrix.html) that
is disturbingly badly sourced, with multiple sections containing
precious statements like:

> We have not seriously investigated those disturbing pieces of
> information, so let’s consider them as FUD.

> We have not investigated further, so let’s consider there is no
> freedom issue at all

> We have not read it. We do not think it is worth wasting more time.

So basically, they read a report critical of matrix, rumours about
Amdocs, panicked and ran away screaming without reading anything else
about the problem. I hesitated in even linking to the article here
precisely because it was exactly the kind of FUD I do not think it's
worth mentioning, but I also thought it was important to criticise the
lack of verification in the article, and the source of the
aforementioned research, possibly to defuse further comments linking
to it as well.

Not that people read articles thoroughly before commenting anyways, of
course.

## External research on Matrix, exhibit B

This one is more damning and makes me reconsider using Matrix at
all. I encourage anyone who is planning to do anything serious with
Matrix to take a long, deep look at [why not matrix][].

It certainly gave me food for thought. As a basic example: I have sent
myself an attachment over FluffyChat from my phone, in a (failed)
attempt at dumping a file from my phone to "the internet" at a print
shop. (It didn't work: I couldn't find the link to share.) But the
file *did* get posted on the homeserver, so there *was* a
link. Problem though: when I deleted the message from FluffyChat, the
attachment was still there, and will presumably be present forever.

(Update: that specific issue is being addressed in Matrix 1.11,
deployed slowly across the federation starting in August 2024,
presumably, see [this announcement](https://matrix.org/blog/2024/06/26/sunsetting-unauthenticated-media/).)

Ouch. Watch out what you upload to Matrix.

## External research on Matrix, exhibit C

[This furry](https://soatok.blog/2024/08/14/security-issues-in-matrixs-olm-library/) published a pretty damning review of the fundamental
E2E library used in most Matrix clients, libolm. It's vulnerable to
*significant* security issues like cache-timing attacks, malleable
signatures and, critically, private key material leakage.

Matrix's response to this was appalling: they simply (and rather
silently) [deprecated the library](https://gitlab.matrix.org/matrix-org/olm/-/commit/6d4b5b07887821a95b144091c8497d09d377f985), and [dug their heels in](https://news.ycombinator.com/item?id=41249371)
when confronted about those security issues.

At this point, one cannot really trust the cryptography in
Matrix. Even though the newer E2E library was audited, it's only used
by Element (and therefore not the client *I* am currently using). And
besides, with such attitude, I doubt we can trust Matrix security at
all going forward.

[[!tag matrix irc history debian-planet python-planet review
internet]]

[^1]: [According to Wikipedia](https://en.wikipedia.org/wiki/Internet_Relay_Chat#Modern_IRC), there are currently about 500
      distinct IRC *networks* operating, on about 1,000 servers,
      serving over 250,000 users. In contrast, Mastodon seems to be
      [around 5 million users](https://bitcoinhackers.org/@mastodonusercount), Matrix.org [claimed at FOSDEM
      2021](https://www.youtube.com/watch?v=TzUfS08lMek&t=265s) to have about 28 *million* globally visible accounts,
      and Signal lays claim to over [40 million souls](https://www.bbc.com/news/technology-59937614). XMPP claims
      to have "millions" of users on the [xmpp.org homepage](https://xmpp.org/) but
      the [FAQ says they don't actually know](https://xmpp.org/about/faq/). On the proprietary
      silo side of the fence, [this page says](https://wallaroomedia.com/blog/social-media/tiktok-statistics/)
      
       * Facebook: 2.9 billion users
       * WhatsApp: 2B
       * Instagram: 1.4B
       * TikTok: 1B
       * Snapchat: 500M
       * Pinterest: 480M
       * Twitter: 397M

      Notable omission from that list: Youtube, with its mind-boggling
      2.6 billion users...

      Those are not the kind of numbers you just "need to convince a
      brother or sister" to grow the network...
