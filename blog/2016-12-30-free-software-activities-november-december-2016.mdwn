[[!meta title="My free software activities, November and December 2016"]]

[[!toc levels=2]]

Debian Long Term Support (LTS)
==============================

Those were 8th and 9th months working on [Debian LTS][] started by
[Raphael Hertzog at Freexian][]. I had trouble resuming work in November
as I had taken a long break during the month and started looking at
issues only during the last week of November.

[Debian LTS]: https://www.freexian.com/services/debian-lts.html
[Raphael Hertzog at Freexian]: http://www.freexian.com

Imagemagick, again
------------------

I have, again, spent a significant amount of time fighting the
[ImageMagick][] (IM) codebase. About 15 more vulnerabilities were
found since the last upload, which resulted in [DLA-756-1][]. In the
advisory, I unfortunately forgot to mention [CVE-2016-8677][] and
[CVE-2016-9559][], something that was [noticed][] by my colleague
Roberto after the upload... More details about the upload are
available in [the announcement][].

[ImageMagick]: http://www.imagemagick.org/
[the announcement]: https://lists.debian.org/debian-lts/2016/12/msg00101.html
[noticed]: https://lists.debian.org/debian-lts/2016/12/msg00173.html
[CVE-2016-9559]: https://security-tracker.debian.org/CVE-2016-9559
[CVE-2016-8677]: https://security-tracker.debian.org/CVE-2016-9559
[DLA-756-1]: https://lists.debian.org/debian-lts-announce/2016/12/msg00032.html

When you consider that I worked on IM back in october, which lead to
an upload near the end of November covering around 80 more
vulnerabilities, it doesn't look good for the project at all. Of the
15 vulnerabilities I worked on, only 6 had CVEs assigned and I had to
[request CVEs][] for the other 9 vulnerabilities plus 11 more that
were still unassigned. This lead to the [assignment][] of 25 distinct
CVE identifiers as a lot of issues were found to be distinct enough to
warrant their own CVEs.

[assignment]: http://www.openwall.com/lists/oss-security/2016/12/26/9
[request CVEs]: http://www.openwall.com/lists/oss-security/2016/12/20/3

One could also question how many of those issues affect the fork,
[Graphicsmagick][]. A lot of the vulnerabilities were found through
fuzzing searches that may not have been tested on Graphicsmagick.
It seems clear to me that a public corpus of test data should be
available to test regressions and cross-project vulnerabilities.
It's already hard enough to track issues withing IM
itself, I can't imagine what it would be for the fork to keep track of
those issues, especially since upstream doesn't systematically request
CVEs for issues that they find, a questionable practice considering
the number of issues we all need to keep track of.

[Graphicsmagick]: http://www.graphicsmagick.org/

Nagios
------

I have also worked on the Nagios package and produced [DLA 751-1][]
which fixed two fairly major issues ([[!debcve CVE-2016-9565]] and
[[!debcve CVE-2016-9566]]) that could allow remote root access under
certain conditions. Fortunately, the restricted permissions setup by
default in the Debian package made both exploits limited to
information disclosure and privilege escalation if the debug log is
enabled.

 [DLA 751-1]: https://lists.debian.org/debian-lts-announce/2016/12/msg00026.html

This says a lot about how proper Debian packaging can help in limiting the
attack surface of certain vulnerabilities. It was also "interesting"
to have to re-learn dpatch to add patches to the package: I regret not
converting it to quilt, as the operation is simple and quilt is so
much easier to use.

People new to Debian packaging may be curious to learn about the
staggering number of
[[!debwiki debian/patches desc="patching systems"]] historically used in
Debian. On that topic, I started a [conversation][] about how much we
want to reuse existing frameworks when we work on those odd packages,
and the feedback was interesting. Basically, the answer is "it depends"...

[conversation]: https://lists.debian.org/debian-lts/2016/12/msg00100.html

NSS
---

I had already worked on the package in November and continued the work
in December. Most of the work was done by Raphael, which fixed a lot of issues with the
test suite. I tried to [wrap this up][] by fixing
[[!debcve CVE-2016-9074]], the build on armel
and the test suite. Unfortunately, I had to stop again because I ran
out of hours and the fips test suite was still failing, but
fortunately Raphael was able to complete the work with [DLA-759-1][].

As things stand now, the package is in better shape than in other
suites as tests ([[!debbug 806639]]) and autopkgtest
([[!debbug 806207]]) are still not shipped in the sid or stable
releases.

[DLA-759-1]: https://lists.debian.org/debian-lts-announce/2016/12/msg00034.html
[wrap this up]: https://lists.debian.org/debian-lts/2016/12/msg00125.html

Other work
----------

For the second time, I forgot to formally assign myself a package
before working on it, which meant that I wasted part of my hours
working on the [monit][] package. Those hours, of course, were not
counted in my regular hours. I still spent some time reviewing mejo's patch
to ensure it was done properly and it turned out we both made similar
patches working independently, always a good sign.

[monit]: https://security-tracker.debian.org/tracker/source-package/monit

As I reported in my [preliminary November report][], I have also
triaged issues in libxml2, ntp, openssl and tiff.

[preliminary November report]: https://lists.debian.org/debian-lts/2016/12/msg00021.html

Finally, I should mention my
[short review of the phpMyAdmin upload][], among the many posts i sent
to the LTS mailing list.

[short review of the phpMyAdmin upload]: https://lists.debian.org/87zijvk6k2.fsf@angela.anarc.at

Other free software work
========================

One reason why I had so much trouble getting paid work done in
November is that I was busy with unpaid work...

manpages.debian.org
-------------------

A major time hole for me was trying to tackle the
[manpages.debian.org service][], which had been offline since August.
After a thorough [evaluation][] of the available codebases, I figured
the problem space wasn't so hard and it was worth trying to do an
implementation from scratch. The result is a tool called [debmans][].

It took, obviously, way longer than I expected, as I experimented with
Python libraries I had been keeping an eye on for a while. For the
commanline interface, I used the [click][] library, which is really a
breeze to use, but a bit heavy for smaller scripts. For a web search
service prototype, I looked at [flask][], which was also very
interesting, as it is light and simple enough to use that I could get
started quickly. It also, surprisingly, fares pretty well in the
global [TechEmpower benchmarking tests][]. Those interested in those
tools may want to look at [the source code][], in particular the
[main command][] (using an interesting pattern itself, `__main__.py`)
and the [search prototype][].

[search prototype]: https://gitlab.com/anarcat/debmans/blob/master/debmans/search.py
[main command]: https://gitlab.com/anarcat/debmans/blob/master/debmans/__main__.py
[the source code]: https://gitlab.com/anarcat/debmans/tree/master/debmans

Debmans is the first project for which I have tried the
[CII Best Practices Badge program][], an interesting questionnaire to
review best practices in software engineering. It is an excellent
checklist I recommend every project manager and programmer to get familiar with.

I still need to complete my work on Debmans: as I write this, I
couldn't get access to the new server the [DSA team][] setup for this
purpose. It was a bit of a frustrating experience to wait for all the
bits to get into place while I had a product ready to test. In the
end, the existing `manpages.d.o` maintainer decided to deploy the existing
codebase on the new server while the necessary dependencies are
installed and accesses are granted. There's obviously still a
[bunch of work][] to be done for this to be running in production so I
have postponed all this work to January.

My hope is that this tool can be reused by other distributions, but
after talking with Ubuntu folks, I am not holding my breath: it seems
everyone has something that is "good enough" and that they don't want
to break it...

[bunch of work]: https://debmans.readthedocs.io/en/latest/todo.html
[DSA team]: https://wiki.debian.org/Teams/DSA
[click]: http://click.pocoo.org/5/
[flask]: http://flask.pocoo.org/
[TechEmpower benchmarking tests]: https://www.techempower.com/benchmarks/
[evaluation]: https://debmans.readthedocs.io/en/latest/design.html#software-evaluation
[debmans]: https://debmans.readthedocs.io/en/latest/
[manpages.debian.org service]: https://wiki.debian.org/manpages.debian.org
[CII Best Practices Badge program]: https://bestpractices.coreinfrastructure.org/

Monkeysign
----------

I spent a good chunk of time giving a kick in the Monkeysign project,
with the [2.2.2 release][], which features contributions from two
other developers, which may be a record for a single release.

[2.2.2 release]: https://lists.riseup.net/www/arc/monkeysphere/2016-12/msg00037.html

I am especially happy to have adopted a new [code of conduct][] - it
has been an interesting process to adapt the code of conduct for such
a relatively small project. Monkeysign is becoming a bit of a template
on how to do things properly for my Python projects: documentation on
readthedocs.org including a code of conduct, support and contribution
information, and so on. Even though the code now looks a bit old to me
and I am embarrassed to read certain parts, I still think it is a
solid project that is useful for a lot of people. I would love to have
more time to spend on it.

[code of conduct]: http://monkeysign.readthedocs.io/en/2.x/contributing.html#code-of-conduct

LWN publishing
--------------

As you may have noticed if you follow this blog, I have started
publishing articles for the [LWN magazine][], filed here under the
[[tag/lwn]] tag. It is a way for me to actually get paid for some of
my blogging work that used to be done for free. Reports like this one, for
example, take up a significant amount of my time and are done without
being paid. Converting parts of this work into paid work is part of my
recent effort to [[reduce|2016-06-01-work-volume]] the amount of time
I spend on the computer.

[LWN magazine]: https://lwn.net/

An funny note: I always found the layout of the site to be a bit odd,
until I looked at my articles posted there in a different web browser,
which didn't have my normal [ad blocker][] configuration. It turns out LWN
uses ads, and Google ones too, which surprised me. I definitely
didn't want to publish my work under banner ads, and will never do so
on this blog. But it seems fair that, since I get paid for this work,
there is some sort of revenue stream associated with it. If you prefer
to see my work without ads, you can wait for it to be published here
or [become a subscriber][] which allows you to get rid of the ads on
the site.

My experience with LWN is great: they're great folks, and very
supportive. It's my first experience with a real editor and it really
pushed me in improving my writing to make better articles that I
normally would here. Thanks to the LWN folks for their support! Expect
more of those quality articles in 2017.

[ad blocker]: https://www.ublock.org/
[become a subscriber]: https://lwn.net/subscribe/

Debian packaging
----------------

I have added a few packages to the Debian archive:

* [magic-wormhole](https://tracker.debian.org/magic-wormhole): easy
  file-transfer tool, co-maintained with Jamie Rollins
* [slop](https://tracker.debian.org/slop): screenshot tool
* [xininfo](https://tracker.debian.org/xininfo): utility used by teiler
* [teiler](https://tracker.debian.org/teiler) (currently in [NEW][]): GUI for screenshot and
  screencast tools

[NEW]: https://ftp-master.debian.org/new/teiler_3.1-1.html

I have also updated [sopel](https://tracker.debian.org/sopel) and
[atheme-services](https://tracker.debian.org/atheme-services).

Other work
----------

Against my better judgment, I worked again on the borg project. This
time I tried to improve the documentation, after a friend asked me for
help on "how to make a quick backup". I realized I didn't have any
good primer to send regular, non-sysadmin users to and figured that,
instead of writing a new one, I could improve the upstream
documentation instead.

I generated a surprising
[18 commits][] of documentation during that time, mainly to fix
display issues and streamline the documentation. My final attempt at
refactoring the docs eventually [failed][], unfortunately, again
reminding me of the difficulty I have in collaborating on that
project. I am not sure I succeeded in making the project more
attractive to non-technical users, but maybe that's okay too: borg is
a fairly advanced project and not currently aimed at such a
public. This is yet another project I am thinking of creating: a
metabackup program like backupninja that would implement the vision
created by liw in his [A vision for backups in Debian][] post, which
was [discarded by the Borg project][].

[discarded by the Borg project]: https://github.com/borgbackup/borg/issues/326
[A vision for backups in Debian]: http://blog.liw.fi/posts/debian-backups-by-defaut/
[failed]: https://github.com/borgbackup/borg/issues/1802
[18 commits]: https://github.com/borgbackup/borg/commits?author=anarcat&since=2016-11-01T04:00:00Z&until=2016-12-01T05:00:00Z

Github also tells me that I have opened 19 issues in 14 different
repositories in November. I would like to particularly bring your attention to the
[linkchecker][] project which seems to be dead upstream and for which
I am [looking for collaborators][] in order to create a healthy fork.

[linkchecker]: http://wummel.github.io/linkchecker/
[looking for collaborators]: https://github.com/wummel/linkchecker/issues/686

Finally, I started on reviving the [stressant][] project and
changing all my passwords, stay tuned for more!

[stressant]: https://gitlab.com/anarcat/stressant

[[!tag monthly-report debian-planet debian debian-lts python-planet software geek free monkeysign borg debmans]]
