[[!meta title="Deleted GitLab forks from my account"]]

I have just deleted two forks I had of the GitLab project in my
gitlab.com account. I did this after receiving a warning that quotas
would now start to be enforced. It didn't say that I was going over
quota, so I actually had to go look in the [usage quotas](https://gitlab.com/-/profile/usage_quotas#storage-quota-tab) page,
which stated I was using 5.6GB of storage. So far so good, I'm not
going to get billed because I'm below the 10GB threshold.

But still, I found that number puzzling. That's a *lot* of data! Maybe
[wallabako](https://gitlab.com/anarcat/wallabako/)? I build images there in CI... Or the ISOs in
[stressant](https://gitlab.com/anarcat/stressant)?

Nope. The biggest disk users were... my *forks* of [gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce/) and
[gitlab-ee](https://gitlab.com/gitlab-org/gitlab-ee/) (now respectively called `gitlab-foss` and `gitlab-ee`,
but whatever). CE was taking up roughly 1GB and EE was taking up the
rest.

So I deleted both repos, which means that the next time I want to
contribute a fix to their documentation — which is as far as I managed
to contribute to GitLab — I will need to re-fork those humongous
repositories.

Maybe I'm reading this wrong. Maybe there's a bug in the quotas
system. Or, if I'm reading this right, GitLab is actually
double-billing people: once for the source repository, and once for
the fork. Because *surely* repos are not duplicating all those blobs
on disk... right? RIGHT?

Either case, that's rather a bad move on their part, I feel like. With
GitHub [charging](https://github.com/pricing) 4$/user/month, it feels like GitLab is going to
have to trouble to compete by [charging](https://about.gitlab.com/pricing/) 20$/user/month as a
challenger...

(Update: as noted in the comments by Jim Sorenson, this is actually an
issue with older versions of GitLab. Deleting and re-forking the repos
will actually fix the issue so, in a way, I did exactly what I
should. Another workaround is to run the [housekeeping jobs](https://docs.gitlab.com/ee/administration/housekeeping.html#manual-housekeeping) on the
repo, although I cannot confirm this works myself.)

But maybe it's just me: I'm not an economist, surely there's some
brilliant plan here I'm missing...

In the meantime, free-ish alternatives include [Quay.io](https://quay.io/) (currently
free for public repos) and [sr.ht](https://sr.ht/) ([2$/mth](https://sourcehut.org/pricing/), but at least not
open-core, unfortunately [no plan for a container registry](https://lists.sr.ht/~sircmpwn/sr.ht-discuss/%3CFA9122A8-8BD1-41C4-88AF-661AF8241CA2%40nderjung.net%3E)). And
of course, you can painfully self-host GitLab, sr.ht, [gitea](https://gitea.io/en-us/),
[pagure](https://anarc.at/blog/2017-06-26-alioth-moving-pagure/), or whatever it is the [current fancy gitweb](https://git.zx2c4.com/cgit/).

[[!tag debian-planet python-planet gitlab]]
