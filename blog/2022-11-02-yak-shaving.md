[[!meta title="A typical yak shaving session"]]

Someone recently asked what [yak shaving](https://en.wiktionary.org/wiki/yak_shaving) means and, because I am a
professional at this pastime, I figured I would share my most recent
excursion in the field.

As a reminder, "yak shaving" describes a (anti?) pattern by which you
engage in more and more (possibly useless) tasks that lead you further
and further away from your original objective.

The path I took through the yak heard is this:

 1. i wondered if i can use my home network to experiment with another
    [VPN](https://en.wikipedia.org/wiki/Virtual_private_network) software (e.g. [Wireguard](https://en.wikipedia.org/wiki/WireGuard) instead of [IPsec](https://en.wikipedia.org/wiki/IPsec))

 2. then i tried [Tailscale](https://tailscale.com/) because I heard good things about it,
    and they have an [interesting approach to opensource](https://tailscale.com/blog/opensource/)

 3. I wasn't happy with that, so i tried an [IPv6 tunnel](https://tunnelbroker.net/)

 4. that broke after a few minutes, so i went on to try deploying
    Wireguard with [Puppet](https://en.wikipedia.org/wiki/Puppet_(software)), which involved reviewing about [4
    different Puppet modules](https://forge.puppet.com/modules?q=wireguard)

 5. while I was there, I might as well share those findings with the
    community, so I publish [that as a blog post](https://anarc.at/blog/2022-10-28-vpn-considerations/)

 6. someone else mentions that [Nebula](https://github.com/slackhq/nebula) (from Slack) is a thing,
    but after investigation, it's [not well packaged in Debian](https://tracker.debian.org/pkg/nebula), so
    didn't test it, but add it to the blog post

 7. now that I found the [right Puppet module](https://forge.puppet.com/modules/puppet/wireguard), I tried to deploy
    it with Puppet's [g10k](https://github.com/xorpaul/g10k/), which requires me to input a checksum

 8. I got lazy and figured if i would put the checksum wrong, it would
    tell me what the right checksum was, but it didn't: instead it
    silently succeeded instead of failing, which seemed really bad

 9. then I looked upstream for such a bug report and saw that the
    [Debian package](http://tracker.debian.org/g10k) was many versions behind and, because I'm on
    the [Golang packaging team](https://wiki.debian.org/Teams/DebianGoTeam), I figured I would just do the
    upgrade myself

 10. then there were problems with the [Debian-specific patch](https://sources.debian.org/src/g10k/0.5.7-1/debian/patches/patch_disable-tests-with-internet.diff/)
     trying to disable network tests, so i [rewrote the patch](https://salsa.debian.org/go-team/packages/g10k/-/commit/6744c9c1dbe375105fd4f8a9c9551d8ae621f374)

 11. ... but ended up realizing basically *all* tests require the
     network, so I just [disabled the build-time tests](https://salsa.debian.org/go-team/packages/g10k/-/commit/41212dbea44600a28c0069c9c84172ddc5dbb3e5)

 12. ... but then tried to [readd it](https://salsa.debian.org/go-team/packages/g10k/-/commit/054f29f1fb25fb2d723a7e108f528753ba06ebfd) to [Debian CI](https://ci.debian.net/) instead,
     which didn't work

At that point, I had given up, after shaving a 12th yak. Thankfully, a
[kind soul](https://salsa.debian.org/nilesh) provided a working test suite and I was able to roll
back all those parenthesis and:

 1. test the `g10k` package and confirm it works (and checks the
    checksums)

 2. [upload the package to the Debian archive](https://tracker.debian.org/news/1379920/accepted-g10k-093-1-source-into-unstable/)

 3. [deploy the package](https://gitlab.com/anarcat/puppet/-/commit/fcd8224fb66a5f3e817e459c2dbd8ae128950c13) in my [Puppet manifests](https://gitlab.com/anarcat/puppet/)

 4. deploy a [first](https://gitlab.com/anarcat/puppet/-/blob/0680011d3e2b3974b05c8934b8a7d98e42aaa6ea/manifests/curie.pp#L29-44) [tunnel](https://gitlab.com/anarcat/puppet/-/blob/0680011d3e2b3974b05c8934b8a7d98e42aaa6ea/manifests/marcos.pp#L21-35)

You'll also notice the work is not complete at all. I still need to:

 * make a full mesh between all nodes, probably with exported
   resources

 * have IP addresses in DNS so I don't need to remember them

 * hook up Prometheus into Puppet to monitor all nodes

 * deploy this at work ([torproject.org](https://torproject.org)), replacing the [IPsec
   module](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40373) I was originally trying to publish

Also notice the 8th yak, above, which *might* be a security issue. I
wasn't able to confirm it, because `g10k` does some pretty aggressive
caching, and I could "reproduce" it in the sense that the checksum
wasn't checked *if it exists in the cache*. So it might have just been
that I had actually already deployed the module before adding the
checksum... but I still had that distressing sentiment:

    <anarcat> there's a huge yak breathing down my neck with "CVE" written in large red letters on the side
    <anarcat> i'm trying to ignore it, it stinks like hell

Hopefully it's nothing to worry about. Right? Riiight.

Oh. And obviously, writing this blog post is the sugar on top, the one
last yak that is self-documented here.

[[!tag puppet debian-planet debian sysadmin]]
