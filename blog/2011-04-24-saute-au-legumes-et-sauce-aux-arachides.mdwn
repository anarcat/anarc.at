<!-- Drupal node 165 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Sauté au légumes et sauce aux arachides"]]
[[!meta date="2011-04-24T22:45:15-0500"]]
[[!meta updated="2011-04-24T22:45:15-0500"]]
[[!meta guid="165 at http://anarcat.koumbit.org"]]

Voici une petite recette que j'utilises souvent comme repas par défaut. Simple, rapide, peu dispendieux, et permet énormément de variations.
<!--break-->
* Préparation: 5 minutes
 * Cuisson: 20 minutes
 * Portion: 2-4 personnes
 * Coût: ~8$ total

Ingrédients
===========

 * 2 tasses de riz
 * 1 ctbl de huile de sésame
 * 1 lbs (454g) de tofu ferme taillé en cubes
 * 1 onion haché
 * 1 piment vert ou rouge frais taillé en cubes
 * 1 casseau de champignons de paris coupé en quarts
 * 1 carotte râpée
 * 1/4 tasse de gingembre râpé
 * 1 brocoli taillé grossièrement

D'autres légumes à ajouter au choix: céleri, courgette, etc, selon les
surplus disponibles et la saison.

Sauce
-----

 * 1 ctbl de cumin entier
 * 1 ctbl de graines de coriandre
 * 1 tasse d'eau
 * 2 ctbl de beurre d'arachides
 * 2 ctbl de vinaigre de riz
 * 2 ctbl de sauce hoisin
 * 1 cthé de sauche au piment rouge siparachu
 * 1 ctbl de sauce soya
 * 1/4 tasse de gingembre râpé
 * 1 ctbl de pâte au curry garam masala midas (optionnel)
 * 1 ctbl de miel (optionnel)

Proportions à vérifier, doser au goût durant la cuisson.

Préparation
===========

La préparation devrait prendre environ 20 minutes si executée en
parallèle. Elle est composée de trois parties: le riz, les légumes et la
sauce. Le riz peut être également remplacer par des nouilles au
vermicelle de riz ou aux oeufs, au goût, ce qui réduit d'autant le temps
de cuisson.

Riz
---

Faire bouillir 4 tasses d'eau pour le riz. Une fois l'eau en ébullition,
mettre le riz, couvrir et laisser mijoter à feu doux durant 20 minutes.

Sauté de légumes
----------------

Faire revenir le tofu dans une poêle avec l'huile de sésame, à feu
elevé. Lorsqu'il commence à dorer, ajouter l'onion. Une fois que le tofu
est bien rôti et que l'onion a ramolli, ajouter le piment puis les
champignons, réduire le feu.

Une fois que les champignons ont réduit et que le piment est cuit (au
goût), ajouter la carotte râpée et le gingembre.

Réduire le feu à moyen-doux, ajouter le brocolo et couvrir. Remuer
périodiquement pour éviter que la mixture colle, ajouter de l'eau au
besoin. Le brocoli doit cuire à la vapeur des autres aliments.

Sauce aux arachides
-------------------

Dans un petit chaudron, faire rôtir à feu élevé le cumin et la coriandre
jusqu'à ce qu'ils brunissent. Retirer du feu et broyer dans un pilon et
mortier.  Réserver.

Dans le chaudron, mélanger l'eau, le beurre d'arachide ainsi que le
vinaigre de riz et faire chauffer à feu moyen. (Le vinaigre sert à bien
défaire le beurre d'arachide.) Ajouter progressivement les autres
ingrédients (incluant les épices réservées) et réduire la sauce jusqu'à
la consistance désirée.

Service
-------

On peut préparer les 3 éléments en parallèle: lorsque le riz
commence à cuire, commencer à frire le tofu et faire dorer les épices.
Ajouter la sauce au sauté de légume avec le brocoli, afin d'aider à la
cuisson. Allonger la sauce au préalable si trop épaisse. Elle doit être
assez liquide pour fournir de la vapeur au brocoli.

Lorsque le riz est prêt, tout devrait être prêt.

Service la sauce sur un lit de riz ou mélanger le riz aux légumes
directement, au goût. Servir avec un thé au jasmin ou sencha.

[[!tag "recette"]]