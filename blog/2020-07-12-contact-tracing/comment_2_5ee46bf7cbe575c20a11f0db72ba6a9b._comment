[[!comment format=mdwn
 username="anarcat"
 avatar="https://seccdn.libravatar.org/avatar/741655483dd8a0b4df28fb3dedfa7e4c"
 subject="Re: no benefit to public health?"
 date="2020-07-13T19:26:45Z"
 content="""
> I doubt the \"no benefit to public health\" part. A contact tracing app speeds up contact tracing at least in some cases and so has some benefit.

I didn't make that up. I based that comment on [this article from the MIT Technology Preview](https://www.technologyreview.com/2020/05/11/1001541/iceland-rakning-c19-covid-contact-tracing/) which is titled: **Nearly 40% of Icelanders are using a covid app—and it hasn’t helped much**. That 40% was, at the time of writing, \"the largest penetration rate of all contact trackers in the world\".

I feel those comments are particularly alarming:

> one senior figure in the country’s covid-19 response says the real impact of Rakning C-19 has been small, compared with manual tracing techniques like phone calls

and:

> “The technology is more or less … I wouldn’t say useless,” says Gestur Pálmason, a detective inspector with the Icelandic Police Service who is overseeing contact tracing efforts. “But it’s the integration of the two that gives you results. I would say it [Rakning] has proven useful in a few cases, but it wasn’t a game changer for us.”

My fundamental concern with contact tracing app is that they try to solve a social problem (namely: reverse engineering human contacts, a fundamentally social activity) through technology (namely: by using radio transmissions as a trace of said contacts). This is bound to fail in all sorts of mysterious ways and triggers the very real risk of *replacing* proper contact tracing with a cheaper technological solution.

In other words, if we cheapen our contact tracing mechanisms by relying on technology too much, we can create a false sense of security and trigger new outbreaks, which will lead to more deaths.

And even if I was wrong and that you could make a contact tracing app that would be more useful than manual tracing techniques, I still doubt the privacy tradeoffs would be worth it. *Maybe* with a technique like the one proposed by bunnie. Maybe. But that is not what the government is proposing, in Québec, and that is that blind, I would even say ignorant, posturing that I object to.

TL:DR; maybe they are benefits to public health in contact tracing apps. I argue they are marginal enough to be non-existent, and definitely not worth the privacy trade-offs.
"""]]
