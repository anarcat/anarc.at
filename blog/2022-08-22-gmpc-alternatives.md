[[!meta title="Alternative MPD clients to GMPC"]]

GMPC (GNOME Music Player Client) is a audio player based on [MPD](https://musicpd.org)
(Music Player Daemon) that I've been using as my main audio player for
years now.

Unfortunately, it's marked as "unmaintained" in the [official list of
MPD clients](https://www.musicpd.org/clients/), along with basically *every* client available in
Debian. In fact, if you look closely, all but one of the 5
unmaintained clients *are* in Debian (ario, cantata, gmpc, and
sonata), which is kind of sad. And none of the active ones are
packaged.

# GMPC status and features

GMPC, in particular, is basically dead. The upstream website domain
has been lost and there has been no release in ages. It's built with
GTK2 so it's bound to be destroyed in a fire at some point anyways.

Still: it's really an awesome client. It has:

 * cover support
 * lyrics and tabs lookups (although those typically fail now)
 * last.fm lookups
 * high performance: loading thousands of artists or tracks is almost
   instant
 * repeat/single/consume/shuffle settings (single is particularly
   nice)
 * (global) keyboard shortcuts
 * file, artist, genre, tag browser
 * playlist editor
 * plugins
 * multi-profile support
 * avahi support
 * shoutcast support

Regarding performance, the only thing that I could find to slow down
gmpc is to make it load *all* of my 40k+ artists in a playlist. That's
slow, but it's probably understandable.

It's basically impossible to find a client that satisfies all of
those.

But here are the clients that I found, alphabetically. I restrict
myself to Linux-based clients.

# CoverGrid

[CoverGrid](https://www.suruatoel.xyz/codes/mcg) looks real nice, but is sharply focused on browsing
covers. It's explicitly "not to be a replacement for your favorite MPD
client but an addition to get a better album-experience", so probably
not good enough for a daily driver. I [asked for a FlatHub package](https://gitlab.com/coderkun/mcg/-/issues/83)
so it could be tested.

# mpdevil

[mpdevil](https://github.com/SoongNoonien/mpdevil) is a nice little client. It supports:

 * repeat, shuffle, single, consume mode
 * playlist support (although it fails to load any of my playlist with
   a `UnicodeDecodeError`)
 * nice genre / artist / album cover based browser
 * fails to load "all artists" (or takes too long to (pre-?)load
   covers?)
 * keyboard shortcuts
 * no file browser

Overall pretty good, but performance issues with large collections,
and needs a cleanly tagged collection (which is not my case).

# QUIMUP

[QUIMUP](https://sourceforge.net/projects/quimup/) looks like a simple client, C++, Qt, and mouse-based. No
Flatpak, not tested.

# SkyMPC

[SkyMPC](https://github.com/soramimi/SkyMPC) is similar. Ruby, Qt, documentation in Japanese. No
Flatpak, not tested.

# Xfmpc

[Xfmpc](https://goodies.xfce.org/projects/applications/xfmpc) is the XFCE client. Minimalist, doesn't seem to have all
the features I need. No Flatpak, not tested.

# Ymuse

[Ymuse](https://yktoo.com/en/software/ymuse/) is another promising client. It has trouble loading all my
artists or albums (and that's without album covers), but it eventually
does. It does have a Files browser which saves it... It's noticeably
slower than gmpc but does the job.

Cover support is spotty: it sometimes shows up in notifications but
not the player, which is odd. I'm missing a "this track information"
thing. It seems to support playlists okay.

I'm missing an album cover browser as well. Overall seems like the
most promising. 

Written in Golang. It [crashed on a library update](https://github.com/yktoo/ymuse/issues/60). There is an
[ITP in Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1019442), update: it just (2022-10-17) [entered Debian](https://tracker.debian.org/news/1374881/accepted-ymuse-021-1-source-amd64-into-unstable/),
see the [package tracker](https://tracker.debian.org/pkg/ymuse) for the current status.

# Conclusion

For now, I guess that ymuse is the most promising client, even though
it's still lacking some features and performance is suffering compared
to gmpc. I'll keep updating this page as I find more information about
the projects. I do not intend to package anything yet, and will wait a
while to see if a clear winner emerges.

[[!tag debian-planet debian radio mpd freedombox audio python-planet]]
