[[!meta title="Bridging Ikiwiki and Twitter with Python and feed2tweet"]]

Typical:

 1. find new interesting software
    ([feed2tweet](http://carlchenet.com/2015/12/28/parse-your-rss-feed-and-post-to-twitter-with-feed2tweet/))
 2. try it out, file two issues:
    * [no startup file](https://github.com/chaica/feed2tweet/issues/1)
    * [fails to install through pip](https://github.com/chaica/feed2tweet/issues/2)
 3. itch enough that i need to scratch it and file 3 pull requests to
    ask for forgiveness:
    * [respect filesystem standard locations](https://github.com/chaica/feed2tweet/pull/5)
    * [show an easy way to disable the hashtag list](https://github.com/chaica/feed2tweet/pull/4)
    * [add -n flag to test the software](https://github.com/chaica/feed2tweet/pull/3)

All fairly trivial, but it allowed me to make a simple cronjob to post
my Ikiwiki blog posts straight out to my Twitter account. It's
basically abusing my RSS feed to bridge with Twitter: the most boring
and annoying part is setting up a new app, pasting the credentials in
the config file and then running the thing in a
[cron job](https://github.com/chaica/feed2tweet/issues/1).

But in the end, I really ended up spending one more hour at a time
when I should be really sleeping, scratching an itch that I didn't
have before I started working on this thing in the first place.

Coincidentally, I [requested to be
added](https://github.com/python/planet/issues/95) to
[Python Planet](http://planetpython.org/), looks like a fun place...

[[!tag debian-planet geek software python python-planet twitter]]
