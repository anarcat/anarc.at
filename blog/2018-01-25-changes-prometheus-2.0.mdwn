[[!meta title="Changes in Prometheus 2.0"]]
[[!meta date="2018-01-25T00:00:00+0000"]]
[[!meta updated="2018-02-06T10:12:53-0500"]]

> This is one part of my coverage of KubeCon Austin 2017. Other
> articles include:
>
>  * [[An overview of KubeCon + CloudNativeCon|2017-12-13-kubecon-overview]]
>  * [[Docker without Docker at Red Hat|2017-12-20-docker-without-docker]]
>  * [[Demystifying Container Runtimes|2017-12-20-demystifying-container-runtimes]]
>  * [[Monitoring with Prometheus 2.0|2018-01-17-monitoring-prometheus]]
>  * [[Changes in Prometheus 2.0|2018-01-25-changes-prometheus-2.0]] (this article)
>  * [[The cost of hosting in the cloud|2018-03-12-cost-of-hosting]]

[[!toc levels=2]]

2017 was a big year for the [Prometheus](https://prometheus.io/)
project, as it [published its 2.0 release in
November](https://prometheus.io/blog/2017/11/08/announcing-prometheus-2-0/).
The new release ships numerous bug fixes, new features and, notably, a
new storage engine that brings major performance improvements. This
comes at the cost of incompatible changes to the storage and
configuration-file formats. An overview of Prometheus and its new
release was presented to the [Kubernetes](https://kubernetes.io/)
community in a
[talk](https://kccncna17.sched.com/event/Cs4d/prometheus-salon-hosted-by-frederic-branczyk-coreos-bob-cotton-freshtracksio-goutham-veeramanchaneni-tom-wilkie-kausal)
held during [KubeCon +
CloudNativeCon](http://events.linuxfoundation.org/events/kubecon-and-cloudnativecon-north-america).
This article covers what changed in this new release and what is brewing
next in the Prometheus community; it is a companion to [[this
article|2018-01-17-monitoring-prometheus]], which provided a general
introduction to monitoring with Prometheus.

What changed
============

Orchestration systems like Kubernetes regularly replace entire fleets of
containers for deployments, which means rapid changes in parameters (or
"labels" in Prometheus-talk) like hostnames or IP addresses. This was
creating significant performance problems in Prometheus 1.0, which
wasn't designed for such changes. To correct this, Prometheus ships a
new [storage engine](https://github.com/prometheus/tsdb) that was
[specifically designed](https://fabxc.org/tsdb/) to handle continuously
changing labels. This was tested by monitoring a Kubernetes cluster
where 50% of the pods would be swapped every 10 minutes; the new design
was proven to be much more effective. The new engine
[boasts](https://coreos.com/blog/prometheus-2.0-storage-layer-optimization)
a hundred-fold I/O performance improvement, a three-fold improvement in
CPU, five-fold in memory usage, and increased space efficiency. This
impacts container deployments, but it also means improvements for any
configuration as well. Anecdotally, there was no noticeable extra load
on the servers where I deployed Prometheus, at least nothing that the
previous monitoring tool (Munin) could detect.

Prometheus 2.0 also brings new features like snapshot backups. The
project has a longstanding design wart regarding data volatility:
backups are deemed to be unnecessary in Prometheus because metrics data
is considered disposable. According to Goutham Veeramanchaneni, one of
the presenters at KubeCon, "this approach apparently doesn't work for
the enterprise". Backups *were* possible in 1.x, but they involved using
filesystem snapshots and stopping the server to get a consistent view of
the on-disk storage. This implied downtime, which was unacceptable for
certain production deployments. Thanks again to the new storage engine,
Prometheus can now perform fast and consistent backups, triggered
through the web API.

Another improvement is a fix to the longstanding [staleness handling
bug](https://github.com/prometheus/prometheus/issues/398) where it would
take up to five minutes for Prometheus to notice when a target
disappeared. In that case, when polling for new values (or "scraping" as
it's called in Prometheus jargon) a failure would make Prometheus reuse
the older, stale value, which meant that downtime would go undetected
for too long and fail to trigger alerts properly. This would also cause
problems with double-counting of some metrics when labels vary in the
same measurement.

Another limitation related to staleness is that Prometheus wouldn't work
well with scrape intervals above two minutes (instead of the default
15 seconds). Unfortunately, that is still not fixed in Prometheus 2.0 as
the problem is more complicated than originally thought, which means
there's still a hard limit to how slowly you can fetch metrics from
targets. This, in turn, means that Prometheus is not well suited for
devices that cannot support sub-minute refresh rates, which, to be fair,
is rather uncommon. For slower devices or statistics, a solution might
be the node exporter "textfile support", which we mentioned in the
previous article, and the
[`pushgateway`](https://github.com/prometheus/pushgateway) daemon, which
allows pushing results from the targets instead of having the collector
pull samples from targets.

The migration path
==================

One downside of this new release is that the upgrade path from the
previous version is bumpy: since the storage format changed,
Prometheus 2.0 cannot use the previous 1.x data files directly. In his
presentation, Veeramanchaneni justified this change by saying this was
consistent with the project's [API stability
promises](https://prometheus.io/blog/2016/07/18/prometheus-1-0-released/#fine-print):
the major release was the time to "break everything we wanted to break".
For those who can't afford to discard historical data, a [possible
workaround](https://www.robustperception.io/accessing-data-from-prometheus-1-x-in-prometheus-2-0/)
is to replicate the older 1.8 server to a new 2.0 replica, as the
[network
protocols](https://prometheus.io/docs/prometheus/latest/federation/) are
still compatible. The older server can then be decommissioned when the
retention window (which defaults to fifteen days) closes. While there is
some work in progress to provide a way to convert 1.8 data storage
to 2.0, new deployments should probably use the 2.0 release directly to
avoid this peculiar migration pain.

Another key point in the [migration
guide](https://prometheus.io/docs/prometheus/2.0/migration/) is a change
in the rules-file format. While 1.x used a custom file format, 2.0 uses
YAML, matching the other Prometheus configuration files. Thankfully the
[`promtool`](https://github.com/prometheus/prometheus/tree/master/cmd/promtool)
command handles this migration automatically. The [new
format](https://prometheus.io/blog/2017/06/21/prometheus-20-alpha3-new-rule-format/)
also introduces [rule
groups](https://github.com/prometheus/prometheus/issues/1095), which
improve control over the rules execution order. In 1.x, alerting rules
were run sequentially but, in 2.0, the *groups* are executed
sequentially and each group can have its own interval. This fixes the
longstanding race conditions between dependent rules that create
inconsistent results when rules would reuse the same queries. The
problem should be fixed between groups, but rule authors still need to
be careful of that limitation *within* a rule group.

Remaining limitations and future
================================

As we saw in the introductory article, Prometheus may not be suitable
for all workflows because of its limited default dashboards and alerts,
but also because of the lack of data-retention policies. There are,
however, discussions about [variable per-series
retention](https://github.com/prometheus/prometheus/issues/1381) in
Prometheus and [native down-sampling support in the storage
engine](https://github.com/prometheus/tsdb/issues/56), although this is
a feature some developers are not really comfortable with. When asked on
IRC, Brian Brazil, one of the lead Prometheus developers,
[stated](https://riot.im/app/#/room/#prometheus:matrix.org/$15158612532461742oHkAM:matrix.org)
that "*downsampling is a very hard problem, I don't believe it should be
handled in Prometheus*".

Besides, it is already possible to selectively [delete an old
series](https://github.com/prometheus/prometheus/blob/master/docs/querying/api.md#delete-series)
using the new 2.0 API. But Veeramanchaneni
[warned](https://twitter.com/putadent/status/952420417276276736) that
this approach "*puts extra pressure on Prometheus and unless you know
what you are doing, its likely that you'll end up shooting yourself in
the foot*". A more common approach to native archival facilities is to
use [recording rules](https://prometheus.io/docs/practices/rules/) to
aggregate samples and collect the results in a second server with a
slower sampling rate and different retention policy. And of course, the
new release features [external storage
engines](https://prometheus.io/docs/operating/integrations/#remote-endpoints-and-storage)
that can better support archival features. Those solutions are obviously
not suitable for smaller deployments, which therefore need to make hard
choices about discarding older samples or getting more disk space.

As part of the staleness improvements, Brazil also started working on
"isolation" (the "I" in the [ACID
acronym](https://en.wikipedia.org/wiki/ACID)) so that queries wouldn't
see "partial scrapes". This hasn't made the cut for the 2.0 release, and
is still [work in
progress](https://github.com/prometheus/tsdb/pull/105), with some
performance impacts (about 5% CPU and 10% RAM). This work would also be
useful when heavy contention occurs in certain scenarios where
Prometheus gets stuck on locking. Some of the performance impact could
therefore be offset under heavy load.

Another performance improvement mentioned during the talk is an eventual
query-engine rewrite. The current query engine can sometimes cause
excessive loads for certain expensive queries, according the Prometheus
[security
guide](https://prometheus.io/docs/operating/security/#denial-of-service).
The goal would be to optimize the current engine so that those expensive
queries wouldn't harm performance.

Finally, another issue I discovered is that 32-bit support is limited in
Prometheus 2.0. The Debian package maintainers found that the [test
suite fails on
i386](https://github.com/prometheus/prometheus/issues/3665), which lead
Debian to [remove the package from the i386
architecture](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=886702).
It is currently unclear if this is a bug in Prometheus: indeed, it is
strange that Debian tests actually pass in other 32-bit architectures
like armel. Brazil, in the bug report, argued that "*Prometheus isn't
going to be very useful on a 32bit machine*". The position of the
project is currently that "*'if it runs, it runs' but no guarantees or
effort beyond that from our side*".

I had the privilege to meet the [Prometheus
team](https://prometheus.io/community/) at the conference in Austin and
was happy to see different consultants and organizations working
together on the project. It reminded me of my [golden days in the Drupal
community](https://www.drupal.org/user/1274/): different companies
cooperating on the same project in a harmonious environment. If
Prometheus can keep that spirit together, it will be a welcome change
from the
[drama](http://freesoftwaremagazine.com/articles/nagios_and_icinga/)
that affected certain monitoring software. This new Prometheus release
could light a bright path for the future of monitoring in the free
software world.

------------------------------------------------------------------------

> *This article [first appeared][] in the [Linux Weekly News][].*

[first appeared]: https://lwn.net/Articles/744721/
[Linux Weekly News]: http://lwn.net/

[[!tag debian-planet lwn conference]]
