[[!meta title="Pourquoi nationaliser Internet au Québec"]]

J’ai écrit un article un peu trop long sur [[comment nationaliser
Internet|2022-08-26-nationalize-internet]] (en anglais) mais j’ai
réalisé que je n’ai pas tant expliqué *pourquoi*.

> Note: ceci est une vieille idée que j'ai déjà [[proposé en
> 2012|blog/2012-06-20-pourquoi-un-monopole-sur-linternet-et-une-solution-reseau-quebec]].

Québec doit nationaliser Internet parce que le moment est venu
d’investir dans un réseau public moins cher, plus juste et
décentralisé. Cet ambitieux projet pourrait révolutionner
l’architecture de nos réseaux et permettre une plus grande innovation,
et peut-être même aider à réduire nos dépenses énergétiques.

[[!toc]]

# Bon moment

La première raison de reconsidérer l’architecture du réseau au Québec,
est que c’est le bon moment. Partout au Québec, les municipalités
doivent progressivement remplacer tout le système d’égouts désuets et d’eau
potable contaminés au [plomb][].

C’est une occasion unique d’installer un réseau de fibre optique. Ce
n’est pas tous les jours qu’une ville ouvre tous ses soubassements.
Il faut en profiter, maintenant.

Plus largement, beaucoup de nos réseaux sont encore basés sur du
cuivre — les vieux systèmes téléphonique et de télévision câblée — et
nous achevons une transition vers de la fibre optique. La question
est : à qui appartiendra cette ressource critique ?

# Investir dans le futur

Brancher tout le monde sur la fibre est un investissement, pas une
dépense. Construire une infrastructure de fibre publique est un projet
similaire à l’électrification du 20ᵉ siècle. Tout comme à cette époque
nous pouvons choisir d’investir dans le privé ou dans le public, dans
une technologie désuète (le cuivre) ou celle qui nous amènera dans le
prochain millénaire (la fibre).

Présentement, nous choisissons d’investir dans le privé au profit de
compagnies antiques basées sur réseaux de cuivre, surtout dans le
“dernier mille”. Au lieu de ce banditisme, nous pourrions créer un
réseau public qui serait ultimement générateur de revenus, tout comme
Hydro-Québec l’est désormais.

# Par justice

L’accès Internet n’est plus seulement un privilège, c’est un droit. Il
faut par exemple avoir accès à Internet pour [franchir la frontière
canadienne][], et de plus en plus de services migrent en ligne.

Présentement, l’accès internet est une forme de taxe privée,
régressive. Cette situation défavorise davantage les pauvres, qui ne
peuvent souvent pas se permettre de débourser 50-100$/mois pour
accéder au réseau.

Établir un réseau public n’éliminerait pas complètement ces
disparités, mais permettrait d’établir des critères plus justes pour
permettre l’accès Internet, par exemple en offrant des tarifs
dégressifs selon le revenu.

# Innovant

Quand les gouvernements ont donné un milliard de dollars dans ces
compagnies privées, ils ont établi un barème pour évaluer si les
compagnies auraient fait leur travail. La barre de la “[haute
vitesse][]” a été mise assez basse : 50 mégabits par seconde. Or, ce
niveau de service est disponible [depuis plus d’une décennie][], une
éternité dans le domaine des technologies.

Nous avons donc payé une fortune pour un réseau désuet.

Si, à l’inverse, nous investissons dans un réseau de fibre optique,
évolutif, il sera facile d’offrir des services plus performants à
l’avenir. La municipalité de [Chattanooga][] au Tennessee vient
d’implanter un service 25 *gigabit* — c’est-à-dire 25 000 mégabits par
seconde — soit 500 fois plus performant que le standard établi au
Québec. Pour ce faire, la ville n’a pas eu à rouvrir des canalisations
ou passer des nouveaux fils ; la fibre installée en 2010 était
parfaitement suffisante.

De telles vitesses donnent le vertige, et on est en droit de se
demander à quoi elle pourrait bien servir. Mais la venue du travail à
distance a changé la donne : la vidéo-conférence, autrefois artefact
de science-fiction, est maintenant dans la vie de tous les jours. Et
bien que, par miracle, nos réseaux de cuivre arrivent à fournir à la
demande, ce n’est pas sans compromis au niveau de la qualité du
signal. Ultimement, on défavorise encore la participation des moins
nantis, qui n’ont pas accès aux meilleurs signaux plus coûteuses.

Mais on pourrait aussi aller plus loin. Avec des vitesses passées le
gigabit, il est possible d’héberger des services de tout genre à la
maison. Normalement, ces services sont viables seulement dans un
centre de données climatisés. J’en sais quelque chose, ayant co-fondé
le [Réseau Koumbit][] il y a près de 20 ans maintenant. Les coûts
d’une telle opération sont prohibitifs et font que Koumbit est, à ce
jour, un des rares hébergeurs indépendants encore en opération au
Québec.

Tout le réseau est centralisé : la capacité est concentrée dans le
centre-ville. En créant un réseau de fibre universel, tout point du
réseau peut devenir un serveur. Ceci permettrait une plus saine
compétition sur Internet, présentement dominé par les gros joueurs
tels que Google, Amazon, Facebook, Apple et Microsoft.

[haute vitesse]: https://www.ledevoir.com/opinion/chroniques/756822/chronique-promesse-de-politicien

# Décentralisé

Concrètement, un réseau public devrait être fédéré autour des
municipalités et des [MRC][]s, chacune ayant l’autonomie de gérer son
propre réseau. Ceci éliminerait toute une classe de problèmes
similaire à la [coupure de service catastrophique de Rogers de juillet
2022][], ayant affecté des clients d’un océan à l’autre.

Cette idée a priori peut être bizarre, mais c’est en fait la façon
dont beaucoup de services publics opèrent. Les transports en commun,
la collecte des ordures, la voirie, ces compétences sont généralement
d’ordre municipal. Il devrait en être de même des télécommunications,
dans la mesure bien sûr où le provincial puisse fournir des dorsales
ou au moins les ressources financières pour interconnecter certaines
régions éloignées…

À tout le moins, le réseau devrait être conçu avec une dorsale
partagée, neutre. Quitte à ce qu’elle soit gérée par une compagnie
privée. Présentement, les dorsales sont toutes gérées par les mêmes
compagnies. Bell Canada, par exemple, offre des services Internet,
mais aussi téléphonique, télévision, journaux, magazines, radios, tout
en fournissant une dorsale pour d’autres fournisseurs Internet.

# Sauver le climat

Un réseau centralisé comme celui d’Amazon et Google peut permettre des
économies énergétiques, car les compagnies ont intérêt à optimiser les
ressources pour réduire leurs coûts. Mais leur modèle d’affaire — 
souvent basé sur la gratuité – cache parfois les coûts réels de
ces produits.

Ultimement, les gros centres de données mènent au gaspillage. La
dissipation de chaleur est un énorme problème dans ce modèle, par
exemple : la vague de chaleur de cet été a mené à des [coupures de
services nuagiques en Grande-Bretagne][]. Pire encore, la
sur-utilisation de l’électricité par ces compagnies privées surcharge
les réseaux électriques, à un tel point que [certains quartiers de
Londres ne peuvent plus construire][london] pour adresser la crise du
logement.

Il serait peut-être possible de concevoir un autre Internet,
réellement décentralisé, où votre petit “routeur” pourrait aussi faire
office de serveur pour vos contacts, photos, dossiers personnels, et,
pourquoi pas, vous permettre de publier vos propres contenus en ligne.

Peut-être que, de cette façon, nous sauverions aussi des coûts
écologiques, en économisant des frais de climatisation et en
réutilisant des machines qui, la plupart du temps, ne servent pas à
grand-chose à la maison.

[plomb]: https://www.ledevoir.com/societe/sante/567857/comprendre-le-dossier-du-plomb-dans-l-eau-au-quebec
[franchir la frontière canadienne]: https://www.ledevoir.com/opinion/idees/748461/idees-le-dangereux-precedent-d-arrivecan
[Réseau Koumbit]: https://koumbit.org
[Chattanooga]: https://en.wikipedia.org/wiki/Chattanooga,_Tennessee
[depuis plus d’une décennie]: https://web.archive.org/web/20090621065516/http://www.videotron.com/service/services-internet/acces-internet
[MRC]: https://fr.wikipedia.org/wiki/Municipalit%C3%A9_r%C3%A9gionale_de_comt%C3%A9
[coupure de service catastrophique de Rogers de juillet 2022]: https://ici.radio-canada.ca/nouvelle/1896581/panne-connexion-service-majeur-interruption-fournisseur-web
[coupures de services nuagiques en Grande-Bretagne]: https://gizmodo.com/google-oracle-cloud-heatwave-europe-1849199124
[london]: https://gizmodo.com/data-centers-london-tech-electric-grid-1849342150

# Mises à jour

> Cet article a été [[refusé au Devoir|tag/ledevoir-rejet]].

## Nationalisé par la CAQ??

La CAQ, après l'élection de 2022, a commencé à faire du bruit pour
financer le développement d'un réseau similaire à ce qui est proposé
ici, sans bien sûr porter attribution de cette idée à moi ou Québec
Solidaire, qui a un projet du genre dans son programme
depuis 2018. [Cet article du Devoir](https://www.ledevoir.com/politique/quebec/769092/politique-quebecoise-plaidoyer-pour-un-hydro-quebec-de-la-fibre-optique), en première page du journal
du 2 novembre 2022, ne fait également pas mention de QS ou de mes
idées précédentes, ce que je trouve fascinant.

À mon avis, ceci va dégénérer encore en une fuite de capitaux vers le
privée, mais on ne sait jamais, je pourrais être agréablement
surpris. En attendant, même si [Ebox offre maintenant la fibre de
Bell](https://www.dslreports.com/forum/r33497645-Nouveaut-FTTH-chez-EBOX), je n'y ai [toujours pas droit à Montréal](https://www.dslreports.com/forum/r33525932-).

Mise à jour, 2023-10-10: ils ont [flotté un autre ballon](https://www.latribune.ca/actualites/2023/10/10/la-caq-envisage-la-creation-dune-nouvelle-societe-detat-M4VXPRT3YJHILAOJLYOGWRH7QY/) pour voir
si on veut nationaliser ou pas. Maintenant, c'est 2 des 4 options, la
nationalisation. Et ils parlent de "Connectivité Québec" au lieu du
beaucoup plus joli "Réseau Québec", que j'ai [[proposé il y a plus de
10 ans
maintenant|blog/2012-06-20-pourquoi-un-monopole-sur-linternet-et-une-solution-reseau-quebec]]... Tout
ça pendant qu'ils financent le privé à [hauteur de 7 milliards](https://www.lapresse.ca/affaires/2023-09-28/northvolt-s-installe-au-quebec/l-aide-pourrait-depasser-7-milliards.php).

## Pas de service

Finalement, l'idée de passer par Musk et Starlink? ça marche pas si
bien, en fin de compte... Plusieurs communautés se plaignent de
[retards et ratés](https://www.lapresse.ca/actualites/regional/2022-11-10/internet-haute-vitesse-en-region/des-retards-et-des-rates.php) selon la Presse. Dans cet article on apprend
aussi que la subvention provinciale pour Starlink dure seulement deux
ans, date après laquelle les clients sont censés payer le prix complet
par eux-mêmes... Un bon moyen, finalement, de financer le
développement de la clientèle de ce milliardaire!

## En Ontario, Hydro fait de la fibre

Je viens de découvrir [cette nouvelle de 2021](https://dgtlinfra.com/hydro-one-fiber-telecom-acronym-solutions/) qui montre comment
[Hydro One](https://www.hydroone.com/), une [compagnie privée](https://en.wikipedia.org/wiki/Hydro_One) a une filiale qui se nomme
maintenant [Acronym solutions](https://acronymsolutions.com/) qui vend l'accès à son réseau de
fibre qui connecte, entre autres, Montréal à Toronto.

## Argenteuil aussi

Je viens d'apprendre que la [MRC d'Argenteuil](https://fr.wikipedia.org/wiki/Argenteuil_(municipalit%C3%A9_r%C3%A9gionale_de_comt%C3%A9)) (au Québec!) a 180km
de fibre municipale, voir <https://fibreargenteuil.ca/>. Population:
36 017 habitants (!).

Leur site a également un lien vers cette [carte interactive de
Québec](https://www.quebec.ca/gouvernement/politiques-orientations/carte-internet-haute-vitesse) qui montre quelles régions sont couvertes par un service de
"haute vitesse". On peut aussi chercher par adresse pour voir quels
fournisseurs sont disponibles à un point donné.

[[!tag analyse bell histoire "network neutrality" internet politique canada québec ledevoir-rejet]]
