[[!meta title="A terrible Pixel Tablet"]]

In a strange twist of history, Google finally woke and thought "I know
what we need to do! We need to make a TABLET!".

So some time soon in 2023, Google will release "The tablet that only
Google could make", the [Pixel Tablet](https://store.google.com/product/pixel_tablet).

Having owned a [[Samsung Galaxy Tab S5e|hardware/tablet/gts4lvwifi]]
for a few years, I was very curious to see how this would pan out and
especially whether it would be easier to flash than the Samsung. As an
aside, I figured I would give *that* a shot, and within a few days
managed to completely brick the device. Awesome. See
[[hardware/tablet/gts4lvwifi]] for the painful details of that.

In any case, Google made a tablet. I own a Pixel phone and I'm
moderately happy with it. It's easy to flash with [CalyxOS](https://calyxos.org/), maybe
this is the promise land of tablets?

[[!toc]]

# Compared with the Samsung

But it turns out that the [Pixel Tablet pales in comparison with the
Samsung tablet, produced 4 years ago, in 2019](https://m.gsmarena.com/compare.php3?idPhone1=9581&idPhone2=11905):

 * it's thicker (8.1mm vs 5.5mm)
 * it's heavier (493g vs 400g)
 * it's not AMOLED (IPS LCD)
 * it doesn't have an SD card reader
 * its camera is worse (8MP vs 13MP, 1080p video instead of 4k)
 * it's more expensive (670EUR vs 410EUR)

What the Pixel tablet has going for it:

 * a slightly more powerful CPU
 * a stylus
 * more storage (128GB or 256GB vs 64GB or 128GB)
 * more RAM (8GB vs 4GB or 6GB)
 * Wifi 6

I guess I should probably wait for the actual device to come out to
see reviews and how it stacks up, but so far it's kind of impressive
how underwhelming this is. 

Also note that we're comparing against a very old Samsung tablet here,
a fairer comparison might be against the [Samsung Galaxy Tab
S8](https://m.gsmarena.com/compare.php3?&idPhone2=11905&idPhone1=11343). There the sizes are comparable, and the Samsung is more
expensive than the Pixel, but then the Pixel has absolutely zero
advantages and all the other disadvantages.

# The Dock

The "Dock" is also worth a little aside.

See, the tablet comes with a dock that doubles as a speaker. 

You can't buy the tablet without the dock. You have to have a
dock.

I shit you not, actual quote: "Can I purchase a Pixel Tablet without
the Charging Speaker Dock? No, you can only purchase the Pixel Tablet
with the Charging Speaker Dock."

In case you really, really like the dock, "You may purchase additional
Charging Speaker Docks separately (coming soon)." And no, they can't
all play together, only the dock the tablet is docked into will play
audio.

The dock is not a Bluetooth speaker, it can only play audio from that
one tablet that Google made, this one time.

It's also not a battery pack. It's just a charger with speakers in
it.

Promising e-waste.

Again, I hope I'm wrong and that this is going to be a fine
tablet. But so far, it looks like it doesn't even come close to
whatever Samsung threw over the fence before the apocalypse (remember
2019? were we even born yet?). 

"The tablet that only Google could make." Amazing. Hopefully no one
else gets any bright ideas like this.

[[!tag debian-planet android review hardware]]


<!-- posted to the federation on 2023-05-18T12:04:31.580338 -->
[[!mastodon "https://kolektiva.social/@Anarcat/110390533906128226"]]