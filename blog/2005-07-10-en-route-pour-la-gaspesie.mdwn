<!-- Drupal node 8 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="En route pour la gaspésie"]]
[[!meta date="2005-07-10T14:03:38-0500"]]
[[!meta updated="2012-12-14T22:36:32-0500"]]
[[!meta guid="8 at http://anarcat.koumbit.org"]]

Et hop, les heures coulent tranquillement vers mon départ pour la gaspésie, avec ma blonde. On a loué un [Communauto](http://communauto.com/) pour 3 semaines et on a bien l'intention de faire le tour de la gaspésie. Je vais essayer de mettre des nouvelles ici de temps en temps, mais je ne garantis rien. J'ai la ferme intention de prendre un gros congé non seulement de ma [job](http://koumbit.org/) mais aussi de [l'internet](http://anarcat.koumbit.org/aggregator) en général.

Déconnexion demain matin à 10h.

[[!tag "voyage"]]