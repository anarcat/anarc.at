[[!meta title="Not recommending Purism"]]

This is just a quick note to mention that I have updated my [hardware
documentation on the Librem 13v4 laptop](/hardware/laptop/purism-librem13v4). It has unfortunately
turned into a rather lengthy (and ranty) piece about Purism. Let's
just say that waiting weeks for your replacement laptop (yes, it died
again) does wonders for creativity. To quote the full review:

> TL;DR: I recommend people avoid the Purism brand and products. I
> find they have questionable politics, operate in a "libre-washing"
> fashion, and produce unreliable hardware. Will not buy again.

People who have read the article might want to jump directly to the
new sections:

 * [Libre washing](/hardware/laptop/purism-librem13v4/#libre-washing)
 * [Bullshit anti-interdiction](/hardware/laptop/purism-librem13v4/#bullshit-anti-interdiction)
 * [Bullshit crowdfunding](/hardware/laptop/purism-librem13v4/#bullshit-crowdfunding)
 * [Hardware reliability](/hardware/laptop/purism-librem13v4/#hardware-reliability) (or lack thereof)

I have also added the minor section of the [missing mic jack](/hardware/laptop/purism-librem13v4/#no-mic-jack).

I realize that some folks (particularly at Debian) might still work at
Purism, and that this article might be demoralizing for their work. If
that is the case, I am sorry this article triggered you in any way and
I hope this can act as a disclaimer. But I feel it is my duty to
document the issues I am going through, as a user, and to call
bullshit when I see it (let's face it, the anti-interdiction stuff and
the Purism 5 crowd-funding campaign were total bullshit).

I also understand that the pandemic makes life hard for everyone, and
probably makes a bad situation at Purism worse. But those problems
existed before the pandemic happened. They were issues I had
identified in 2019 and that I simply never got around to document.

I wish that people wishing to support the free software movement would
spend their energy towards organisations that actually do honest work
in that direction, like [System76](https://system76.com/) and [Pine64](https://www.pine64.org/). And if you're
going to go crazy with an experimental free hardware design, why not
go retro with the [MNT Reform](https://www.crowdsupply.com/mnt/reform).

In the meantime, if you're looking for a phone, I recommend you give
the [Fairphone](https://www.fairphone.com/) a fair chance. It really is a "fair" (as in, not
the best, but okay) phone that you can moderately liberate, and it
actually frigging works. See also my [hardware review of the FP2](/hardware/phone/fairphone2).

Update: this kind of blew up, for my standards: 10k visitors in ~24h
while I usually get about 1k visitors after a week on any regular blog
post. There were more discussions on the subject here:

 * [Lobsters](https://lobste.rs/s/ecyjq2/not_recommending_purism)
 * [Reddit /r/linux](http://www.reddit.com/r/linux/comments/hr8hvi/not_recommending_purism), [/r/linuxhardware](https://www.reddit.com/r/linuxhardware/comments/hqs48i/debian_developer_not_recommending_purism/), [r/purism](https://www.reddit.com/r/Purism/comments/hqs0vz/debian_developer_not_recommending_purism/)
 * [Hacker news](https://news.ycombinator.com/item?id=23842347)

Trigger warning: some of those threads include personal insults and
explicitly venture into the [[free speech
discussion|2019-05-13-free-speech]], with predictable (sad)
consequences...

Second update: the laptop is basically dead, case is broken without
replacement parts available, see [[this
followup|blog/2022-08-25-one-dead-purism-laptop]] for details.

Third (and hopefully last) update (2023-05-29): people are now having
trouble getting *refunds* from never-shipped or DOA Librem 5 phones,
see [this thread](https://forums.puri.sm/t/has-purism-refunded-you-is-purism-going-insolvent/18953). Purism's [last take on this (2022)](https://puri.sm/posts/where-is-my-librem-5/) is that
"your Librem phone is coming", "currently shipping backlogs", "20
weeks lead time", and "Buy now".

[[!tag debian-planet python-planet hardware review phone laptop]]
