<!-- Drupal node 195 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Debian Wheezy et Debian Québec ce samedi!"]]
[[!meta date="2013-05-07T22:41:55-0500"]]
[[!meta updated="2013-05-08T10:35:20-0500"]]
[[!meta guid="195 at http://anarcat.koumbit.org"]]

*[En français ci-bas]*

As announced by [Fabian](http://www.fabianrodriguez.com/blog/2013/05/06/debian-quebec-is-here) we are launching the [Debian Québec group](http://wiki.debian.org/LocalGroups/DebianQuebec) this saturday, with a [release party](http://wiki.debian.org/LocalGroups/DebianQuebec/ReleasePartyWheezy) at UQAM. I *may* not be present as I was planning to go camping this weekend, but since the weather forecast is rainy so far, I may cancel.

Tel qu'annoncé par [Fabian](http://www.fabianrodriguez.com/blog/2013/05/06/debian-quebec-is-here), nous lançons le [groupe Debian Québec](http://wiki.debian.org/LocalGroups/DebianQuebec) ce samedi, avec un "[release party](http://wiki.debian.org/LocalGroups/DebianQuebec/ReleasePartyWheezy)". Je vais *peut-être être absent vu que je prévoyais sortir en camping, mais vu qu'ils prévoient de la pluie pour 4 jours, je vais peut-être m'abstenir...
<!--break-->


[[!tag "debian" "debian-planet" "nouvelles" "québec" "short"]]