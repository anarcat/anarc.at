<!-- Drupal node 29 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Vraiment décalisser les logiciels propriétaires"]]
[[!meta date="2005-09-16T11:12:16-0500"]]
[[!meta updated="2012-12-14T22:43:35-0500"]]
[[!meta guid="29 at http://anarcat.koumbit.org"]]

Alors le poste de la mort de Hugo a été converti. J'ai remarqué qu'il y avait un disque de 15Gio qui était vide et foutait absolument rien.. Quelle honte! J'ai donc brûlé un CDRW (vive la réutilisation!) de [Debian](http://www.us.debian.org/CD/) (un CD full parce que le netinst a pas marché). L'install s'est évidemment très bien passé, l'installeur de Debian ayant maturé énormément depuis Woody, qui date lui de quelquepart autour de la dernière ère glacière.

Donc, en bref: superbe install encore une fois. J'ai peut-être fait une petite erreur à un stade car j'ai répondu oui à une question qui sonnait comme:

 Il semble y avoir un système d'explotation propriétaire 
 venu tout droit du septième enfer déjà installé sur ce
 système. Souhaitez vous bousiller son bootloader afin
 que vous passiez le reste de la nuit en panique à essayer
 de le remettre en marche correctement parce que dans le
 fond c'est même pas à vous s't'ostie d'machine.

Évidemment, après avoir répondu Oui-oui-oui-OUI! pendant vingt minutes, c'est venu naturellement. Je me suis donc retrouvé avec un système qui bootait juste linux et dans lequel [GRUB](http://www.gnu.org/software/grub/) était pas de booter XP. Hmmm... 

J'ai fini par trouver [un truc](http://www.gnu.org/software/grub/grub-legacy-faq.en.html#q10) dans la doc de GRUB ([RTFM!](http://catb.org/~esr/jargon/html/R/RTFM.html)) qui fourre windows à croire qu'il est sur le bon disque. La config résultante est savoureuse dans son [obfuscation](http://www.ioccc.org/) (ça se dit en français ça?):

 title           Windows XP
 map             (hd0) (hd1)
 map             (hd1) (hd0)
 rootnoverify    (hd1,0)
 makeactive
 chainloader     +1
 savedefault
 boot

En tout cas, ça marche, curieusement. Oh joy.

[[!tag "vraie vie" "logiciel libre"]]