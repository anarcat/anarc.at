<!-- Drupal node 63 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Qui lit ceci?"]]
[[!meta date="2006-01-13T11:13:05-0500"]]
[[!meta updated="2012-12-14T22:45:00-0500"]]
[[!meta guid="63 at http://anarcat.koumbit.org"]]

Qui lit mon blog? C'est souvent une grosse question des bloggeurs... savoir qui les lit, à quelle fréquence, pourquoi et comment. Alors je pose la question: qui lit ce blog? Et pourquoi personne ne participe vraiment? Il y a des boutons "ajouter un commentaire" au bas de chaque billet, sentez vous libre de vous en servir! J'aimerais d'ailleurs que les gens qui liront ce billet prendront le temps de dire "Oui, moi!"

On a souvent l'impression de parler dans le vide, surtout ici. C'est un peu angoissant, car on n'écrit pas véritablement, entièrement pour soi-même quand on fait un blog: c'est une platforme personnelle qu'on espère pouvoir utiliser pour comuniquer ses idées, ses sentiments, sa vie, quoi.. En fait, j'ai tout de même beaucoup d'information sur l'utilisation du blog. Je peux dire, par exemple, que le blog a reçu une moyenne de 158 ["visites"](http://www.mrunix.net/webalizer/webalizer_help.html) par jour en décembre 2005, pour un total de 4905 visites. Cependant, 50% de ces visites sont en fait des bots consultant le feed [RSS](http://fr.wikipedia.org/wiki/Really_Simple_Syndication) du site. Énormément de traffic, soit environ 35%, est également dû aux différents engins de recherche.

Il reste donc un maigre 20-25% de l'impressionnant 150 visites par jour pour des vrais humains. Les statistiques de navigateur concordent: 8,27% pour Mozilla/Netscape/Firefox et 10,26% pour Internet Exploder.

Donc, trève de statistiques... Il n'y a rien comme recevoir un commentaire, sur un blog. Ça confirme qu'on existe bel et bien. Ça confirme qu'on nous lit. Ça renforce les bons articles et démolit les mauvais. C'est bon tout ça. Et ce n'est pas seulement une question d'ego: c'est aussi de vous qu'il s'agit. Ceci n'est pas un media traditionnel, à sens unique. Vous pouvez et devez participer pour que l'internet soit plus grand que ce que le broadcasting a été dans le passé. Participez!

À vous le clavier!

[[!tag "réflexion" "meta"]]