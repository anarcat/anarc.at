[[!meta title="Salvaged my first Debian package"]]

I finally salvaged my first Debian package, [python-invoke](https://tracker.debian.org/pkg/python-invoke). As
part of [ITS 964718](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=964718), I moved the package from the Openstack Team
to the [Python team](https://wiki.debian.org/Teams/PythonTeam). The Python team might not be super happy with
it, because it's breaking some of its rules, but at least someone
(ie. me) is actively working (and using) the package.

[[!toc]]

# Wait what

People not familiar with Debian will not understand *anything* in that
first paragraph, so let me expand. Know-it-all Debian developers (you
know who you are) can skip to the next section.

Traditionally, the [Debian project](https://debian.org/) (my Linux-based operating
system of choice) has prided itself on the self-managed, anarchistic
organisation of its packaging. Each package maintainer is the lord of
their little kingdom. Some maintainers like to accumulate lots of
kingdoms to rule over.

(Yes, it really doesn't sound like anarchism when you put it like
that. Yes, it's complicated: there's a constitution and voting
involved. And yes, we're old.)

Therefore, it's *really* hard to make package maintainers do something
they don't want. Typically, when things go south, someone makes a
complaint to the [Debian Technical Committee](https://www.debian.org/devel/tech-ctte) (CTTE) which is
established by the [Debian constitution](https://www.debian.org/devel/constitution) to resolve such
conflicts. The committee is appointed by the Debian Project leader,
elected each year (and there's an [election coming up](https://www.debian.org/vote/2022/vote_002) if you
haven't heard).

Typically, the CTTE will then vote and formulate a decision. But
here's the trick: maintainers are still free to do whatever they want
after that, in a sense. It's not like the CTTE can just break down
doors and force maintainers to type code. 

(I won't go into the details of the *why* of that, but it involves
legal issues and, I think, something about the [Turing halting
problem](https://en.wikipedia.org/wiki/Halting_problem). Or something like that.)

Anyways. The point is all that is super heavy and no one wants to go
there...

(Know-it-all Debian developers, I know you are still reading this
anyways and disagree with that statement, but please, please, make it
true.)

... but sometimes, packages just get lost. Maintainers get distracted,
or busy with something else. It's not that they *want* to abandon
their packages. They love their little fiefdoms. It's just there was a
famine or a war or something and everyone died, and they have better
things to do than put up fences or whatever.

So clever people in Debian found a better way of handling such
problems than waging war in the poor old CTTE's backyard. It's called
the [Package Salvaging](https://wiki.debian.org/PackageSalvaging) process. Through that process, a maintainer
can propose to take over an existing package from another maintainer,
if a [certain set of condition are met and a specific process is
followed](https://www.debian.org/doc/manuals/developers-reference/pkgs.en.html#package-salvaging).

Normally, taking over another maintainer's package is basically a war
declaration, rarely seen in the history of Debian (yes, I do think it
happened!), as rowdy as ours is. But through this process, it seems we
have found a fair way of going forward.

The process is basically like this:

 1. file a bug proposing the change
 2. wait three weeks
 3. upload a package making the change, with another week delay
 4. you now have one more package to worry about

Easy right? It actually is! Process! It's magic! It will cure your
babies and resurrect your cat!

# So how did that go?

It went well!

The old maintainer was [actually fine with the change](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=964718#10) because
his team wasn't using the package anymore anyways. He asked to be
kept as an uploader, which I was glad to oblige. 

(He replied a few months after the deadline, but I wasn't in a rush
anyways, so that doesn't matter. It was polite for him to answer, even
if, technically, I was already allowed to take it over.)

What happened next is less shiny for me though. I totally forgot about
the ITS, even after the maintainer reminded me of its existence. See,
the thing is the ITS doesn't show up on my [dashboard](https://qa.debian.org/developer.php?login=anarcat@debian.org) at all. So I
totally forgot about it (yes, twice).

In fact, the only reason I remembered it was that got into the process
of formulating *another* ITS ([1008753](https://bugs.debian.org/1008753), [trocla](https://tracker.debian.org/pkg/trocla)) and I was trying
to figure out how to write the email. Then I remembered: "*hey wait, I
think I did this before!*" followed by "*oops, yes, I totally did this
before and forgot for 9 months*".

So, not great. Also, the package is still not in a perfect shape. I
was able to upload the upstream version that was pending 1.5.0 to
clear out the ITS, basically. And then there's already two new
upstream releases to upload, so I pushed 1.7.0 to experimental as
well, for good measure.

Unfortunately, I still can't enable tests because [everything is on
fire](https://bugs.debian.org/1008768), as usual.

But at least my kingdom is growing.

# Appendix

Just in case someone didn't notice the hyperbole, I'm not a monarchist
promoting feudalism as a practice to manage a community. I do not
intend to really "grow my kingdom" and I think the culture around
"property" of "packages" is kind of absurd in Debian. I kind of wish
it would go away.

(Update: It has also been pointed out that I might have made Debian
seem more confrontational than it actually is. And it's kind of true:
most work and interactions in Debian actually go fine, it's only a
minority of issues that degenerate into conflicts. It's just that they
tend to take up a lot of space in the community, and I find that
particularly draining. And I think think our "package ownership"
culture is part of at least some of those problems.)

[Team maintenance](https://wiki.debian.org/Teams), the [LowNMU](https://wiki.debian.org/LowThresholdNmu) process, and [low threshold
adoption](https://wiki.debian.org/LowThresholdAdoption) processes are all steps in the good direction, but they
are all *opt in*. At least the package salvaging process is someone a
little more ... uh... coercive? Or at least it allows the community
to step in and do the right thing, in a sense.

We'll see what happens with the coming wars around the recent tech
committee decision, which are bound to touch on that topic. (Hint: our
next drama is called "usrmerge".) Hopefully, [LWN](https://lwn.net/) will make a
brilliant article to sum it up for us so that I don't have to go
through the inevitable debian-devel flamewar to figure it out. I
already wrecked havoc on the `#debian-devel` IRC channel asking newbie
questions so I won't stir that mud any further for now.

(Update: LWN, of course, *did* make an [article about usrmerge in
Debian](https://lwn.net/Articles/890219/). I will read it soon and can then tell you know if it's
brilliant, but they are typically spot on.)

[[!tag debian-planet debian packaging]]
