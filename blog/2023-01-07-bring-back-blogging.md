[[!meta title="20 years blogging"]]

Many folks have woken up to the dangers of commercialization and
centralisation of this very fine internet we have around here. For
many of us, of course, it's one big "I told you so"...

(To fair, I *stopped* "telling you so" because evangelism is pretty
annoying. It's certainly dishonest coming from an atheist, so I preach
by example now. I often wonder what works better. But I digress.)

Colleagues have been posting about getting back into blogging. This
[post from gwolf](https://gwolf.org/2023/01/refueling-the-blog.html), in particular, reviews his yearly blog output,
and that made me wonder how that looked like from my end. The answer,
of course, is simple to generate:

    anarcat@angela:~$ cd anarc.at
    /home/anarcat/wikis/anarc.at
    anarcat@angela:anarc.at$ ls blog | grep '^[0-9][0-9][0-9][0-9]' | sed s/-.*// | sort | uniq -c  | sort -n -k2
         62 2005
         49 2006
         26 2007
         25 2008
          8 2009
         16 2010
         24 2011
         19 2012
         17 2013
          7 2014
         19 2015
         32 2016
         43 2017
         40 2018
         27 2019
         33 2020
         22 2021
         45 2022
          1 2023

(I thought of drawing this as a [sparkline](https://ikiwiki.info/ikiwiki/directive/postsparkline/) but it looks like
[Sparklines](https://en.wikipedia.org/wiki/Sparkline) are kind of dead. <https://sparkline.org/> doesn't
resolve and the [canonical PHP package](https://tracker.debian.org/pkg/sparkline-php) is gone from Debian. The
[plugin is broken in ikiwiki anyway](https://ikiwiki.info/bugs/sparkline_fails_to_generate_graphs_in_debian_bullseye/
)...)

So it seems like I've been doing this very blog for 18 years, and it's
not even my first blog. I actually [started in 2003](http://insomniaque.org/blog/5%3Ffrom=40.html), which makes
this year my 20-year blogging anniversary.

(And even if that sounds really old, note that I was not actually an
early adopter. [Jorn Barger](https://en.wikipedia.org/wiki/Jorn_Barger) having coined the term "weblog" in
1997. Yes, in another millenia.)

Reading back some of the headlines in those older posts, I have
definitely changed style. I used to write shorter, more random ideas,
and more often. I somehow managed to write more than one article per
week in 2005!

Now, I take more time writing, a habit I picked up while writing for
[LWN](https://lwn.net/) ([[those articles|tag/lwn]]), which started in 2016. But
interestingly, it seems I started producing *more* articles then: I
hit 43 articles per year in 2017, my fourth best year ever.

The best years in terms of numbers are the first two years (2005 and
2006, I didn't check the numbers on earlier years), but I doubt they
are the best in terms of content. Really, the best writing I have ever
did was for LWN. I dare hope I have kept the quality I was encouraged
(forced?) to produce, but I know I cannot come anywhere close to what
the LWN editors were juicing out of me. You can also see that I
immediately dropped to a more normal 27 article in 2019, once I
stopped writing for LWN...

Back when I started blogging, my writing was definitely more
personal. I had less concerns about privacy back then; now, I would
never write about my personal life like I did back then (e.g. "[[I
have a cold|blog/2005-09-29-une-petite-grippe-avec-ca]]"). 

I was also writing *mostly* in French back then, and it's sad to think
that I am rarely writing in my native language here anymore. I guess
that brings me an international audience, which is simultaneously
challenging, gratifying, and terrifying. But it also means I reach out
to people that do *not* speak English (or French, for that matter) as
their first language. For me that is more valuable than catering to my
little corner of culture, at least for now, and especially when
writing technical topics, which is most of what I do now anyways.

Interestingly, I wrote a *lot* in 2022. People sometimes ask me how I
manage to write so much: I don't actually know. I don't have a lot of
free time on my hand, and even less than before in the past two years,
but somehow I keep feeding this blog.

I guess I must have something to say. Can't quite figure out *what*
yet, so maybe I should just keep trying.

Or, if you're new to this Internet thing, [Bring Back Blogging](https://bringback.blog/)!
Whee!

PS: I wish I had time to do a [[review of my visitors like i did for
2021|blog/2022-01-28-one-year-visitors]] but time is missing.

[[!tag gloating meta debian-planet]]
[[!mastodon https://kolektiva.social/@Anarcat/109651820907589631]]
