[[!meta title="Trying out Keybase"]]

I was privileged enough to get access to the alpha preview of
[Keybase.io][]. Last year, the project
[raised 11M$ in venture capital][] and they seem to be attacking the
hard problems, so it is a serious project, worth considering for any
cryptography hacker like me. This was obviously discussed
[elsewhere][] [previously][] but since Keybase has evolved a bit since
then, I figured it would be worth adding my grain of salt.

[elsewhere]: https://news.ycombinator.com/item?id=7452555
[previously]: https://news.ycombinator.com/item?id=7235634
[raised 11M$ in venture capital]: https://keybase.io/blog/2015-07-15/keybase-raises-series-a
[Keybase.io]: https://keybase.io/

[[!toc levels=2]]

The elite alpha problem
=======================

My first problem with Keybase was of course that I wasn't in the elite
club of invited people. Somehow, I finally got in but the problem
remains for the everyone else.

In my case, it was the [Keybase filesystem announcement][] that
brought my attention back to the project, although I have yet to test
that itself - yet another invite-only problem (need to email
`chris@keybase.io` to get access, I guess). Still, 10GB of storage might be
interesting for some people, but considering the number of hurdles or
"friction", as they say, that needs to be jumped over to get access, I
am not sure how much traction this will get for now.

The project has been in alpha for a while now (more than a
year) with no announced public beta just yet, which is way slower than
similarly challenging projects with less capital. [Let's encrypt][],
as a comparison, was publicly announced in 2014 and by the end of
2015, the public beta was opened.

[Let's encrypt]: https://letsencrypt.org/
[Keybase filesystem announcement]: https://keybase.io/docs/kbfs

Private key leakage
===================

My second major concern is that Keybase does worrisome things to
private key material. If you tell it to import your regular GPG keys,
it will copy them into the keybase keyring, which is already a
problem: now you have two places where you store sensitive
secrets.

But worse, Keybase offers you to upload your private key material on
the server, without clearly and explicitely exposing the potential
risks that means to the user. The key, presumably, is encrypted (at
least that is what I can parse from the
[Keybase privacy policy][]). Yet this is still a concern: a determined
attacker (e.g. the government or another malicious entity) could force
Keybase to give up the encrypted private key material *and* install
sniffers that would reveal the passphrase that would allow decryption
and impersonation of the related identity.

That is a huge deal. This is what caused [Hushmail's demise][] in
2007: they had similar promises about how they had a "secure"
email (in this case) service based on OpenPGP, but since they key was
stored on the server, they only had to serve a malicious applet to
victims and were able to give cleartext data to the US government (12
CDs!) under a [Mutual Legal Assistance Treaty][] with Canada.

In Keybase, When I generate a new key, it asks me if I want to upload the key to
their servers, and "Yes" is chosen as a default, so it is clearly a
policy that is encouraged. This is presumably to enable browser-based
crypto, yet this could be done by storing keys within the browser, not
on the server.

The mere possibility that the client can meddle with secret key
material in such a way is a problem. Even if uploading secret keys to
the server is optional, the fact that it is a possibility widens the
attack surface on the client significantly.

GPG has been working on isolating private key operations to a
different process (gpg-agent) or smart cards, SSH has privilege
seperation. The idea of uploading secret key material online is so
beyond current practices that it should bring the project to a full
stop already.

My private key material is pretty sensitive. I am a Debian
developer: if someone gets access to that key, they can upload
arbitrary code for thousands of packages, affecting thousands if not
millions of users. That is a huge deal, and I am not ready to give
that power to an arbitrary third party that I know little about.

Privacy policy issues
=====================

Furthermore, the [Keybase privacy policy][] explicitely states that
*"Logs of this information may persist for an indefinite period"*, which
include, and I quote:

 * Your computer’s IP address
 * Your preferences and settings (time zone, language, privacy preferences, application preferences, etc.)
 * The URL of the site that referred you to the Service
 * The buttons and controls you clicked on (if any) within the Service
 * How long you used the Service and which parts and features you used
 * Session times and lengths

For a company working to improve user's privacy, this is a pretty bad
policy. It exposes Keybase users to undue surveillance, on a *lot* of
private information that is *not* necessary to operate the service. I
haven't actually read the remaining of the policy, as I was already
scared as hell and figure I would just log off for now and try not to
add any extra personal information in that already growing pile.

Let's mention, while I'm here, that the keybase commandline client has
this odd way of working that it forks a daemon in the background all
the time, even to run just `keybase status`. One has to wonder if this
thing is included in the surveillance data above, which would make the
keybase client an amazing surveillance device in itself. To stop all
this, you need:

    keybase logout
    keybase ctl stop

Not quite obvious...

[Mutual Legal Assistance Treaty]: https://en.wikipedia.org/wiki/Mutual_legal_assistance_treaty
[Hushmail's demise]: https://en.wikipedia.org/wiki/Hushmail
[Keybase privacy policy]: https://keybase.io/docs/privacypolicy

Usability issues
================

Which brings me to a series of usability issues I have found while
working with Keybase.
Having worked on usability myself in the [Monkeysphere project][], I
understand how hard those problems can be. But right now, the fact is
that keybase is only a commandline client, with some web-based sugar
sprinkled on top. I recognize the huge efforts that have been done to
make the user experience (UX) as easy as possible through the
commandline, but this is definitely not "grand public" material yet.

[Monkeysphere project]: http://web.monkeysphere.info/

Even as a OpenPGP hacker, Keybase can get confusing. Since it has its
own separate keyring from GPG, things that used to be obvious before
are suddenly new things to learn. For example, I originally
generated a key for `anarcat@debian.org` in keybase, thinking it would
be a separate thing from my current GPG identity. Well, that ended up creating a
new key in my GPG keyring, duplicating my existing identity, with no
way for me to tell this was a Keybase copy. This could have ended up
on the keyservers and confuse anyone looking for my PGP key.

Revoking keys
-------------

So I revoked that key, which, in itself, is not such an obvious
process either. I first had to "drop" the key:

    keybase pgp drop '010143572d4e438f457a7447c9758804cc7be44c1ee2b7915c3904567d0a3fb5cf590a'

To find that magic number, I had to go on the website and click
through the PGP key to end up on the `revoke` link which told me which
magic string to put. The web UI was telling me I could use `keybase
status` to get that information, but I couldn't find that in the
output here:

    $ keybase status
    
    Username:      anarcat
    Logged in:     no
    
    Device:
        name:      angela
        ID:        070f6a13f2a66afbb463f49dadfd4518
        status:    active
    
    Keybase keys:  unlocked
    Session:       anarcat [salt only]
    
    KBFS:
        status:    not running
        version:
        log:       /home/anarcat/.cache/keybase/keybase.kbfs.log
    
    Service:
        status:    running
        version:   1.0.14-1
        log:       /home/anarcat/.cache/keybase/keybase.service.log
    
    Platform Information:
        OS:        linux
        Runtime:   go1.5.3
        Arch:      amd64
    
    Client:
        version:   1.0.14-1
    
    Desktop app:
        status:    not running
        version:
        log:       /home/anarcat/.cache/keybase/Keybase.app.log
    
    Config path:   /home/anarcat/.config/keybase/config.json
    Default user:  anarcat
    Other users:
    
    command-line client:
        keybase status [pid: 14562, version: 1.0.14-1]

So `keybase drop` "deletes" the key from the keybase client (and presumably the
server). One problem here is that we have three different verbs to
mean the same thing: in some places, the website says "deleted" (in
the [web graph][]), the commandline client uses "drop" and everyone else using
PGP in the world uses "revoke". So what is it?

[web graph]: https://keybase.io/anarcat/graph

Also note that just dropping that key is not enough to eradicate that
key: you still need to generate and import a revocation certificate on
GPG's side as well, for that key. This makes sense, because you may
have imported that key and you may not want to destroy it when you
remove it from keybase, but there could be a little better guessing
and UX here as well.

Password usage
--------------

Then another problem is that, when you `signup` on the commandline, it
asks you to choose a passphrase. It's not clear at all from the UI
what that passphrase is for. Is it to protect the private key
material? To login the website? Both? It turns out it is actually to
login to the website and the Keybase API. It *could* also be the key
to the private key material, but I haven't quite figured that out yet.

Similarly, the "paper devices" are a little confusing to me as
well. The registration process is fairly insistent that I need to
write a series of secret words down on a piece of paper and put it my
wallet. This is based on the idea that you can use that key to recover
your account and setup new devices. However, it is not made clear at
all how those credentials should be protected or used. What if I lose
my wallet? What do I do then? Is it okay if i write it on the back of
my laptop instead? I know it sounds like stupid questions to crypto
geek, but it's definitely stuff people will do, and education, at all
levels, is necessary here.

The existing web of trust
-------------------------

Finally, I found it surprisingly counter-intuitive to sign keys with
keybase. I was assuming the whole point of the thing was to expand the
web of trust, but it seems their mission lies somewhere else: there is
no facility to sign keys directly in keybase. You sign statements on
various social sites (Github, Twitter, Reddit...) but doing
traditional key exchanges seems to be off limits.

My obvious use case was to sign my existing key material with my newly
generated `anarcat@keybase.io` key, so that keybase would become
another way of verifying my identity. Well, this is not possible with
keybase directly - you still have to go through the regular:

    gpg --sign-key DEADBEEF

Which is really unfortunate, because some *craaaazy* people like me
insist on doing in person, actually secure key exchanges sometimes,
and the tools to do this right now (mainly gpg, [caff][] and
[monkeysign][]) really need a lot of love and help. Keybase could have
helped a lot in pushing those initiatives forward and bring new ways
of doing secure key exchanges, instead of *only* lowering the standards on
cryptographic identity.

Usual proprietary startup rant
==============================

I can't help but finish by noticing that the whole basic problem with
Keybase is more fundamental than a few usability issues, which can all
be fixed fairly easily.

Keybase.IO is a web-based service that is entirely proprietary. The
client is freely downloadable and [free software][] - but the server
side is not. This makes us rely on a central point of failure and the
goodwill of those operators to not only keep the service running (for
free?) indefinitely, but to protect us against all attackers,
state-run or otherwise.

This is definitely against basic free software principles and the open
architecture of the internet federation, well embodied in the
[Franklin street statement][]. Online services should be decentralized and open
like the [OpenPGP keyservers][] have always been, period.

Conclusion
==========

I am curious to see where Keybase brings us, as a community. So far, I
am concerned about centralization and devaluation of the web of trust
as cryptographic system. I do not believe that trusting multiple
corporate social networking sites bring much benefit to our security,
although it does improve usability significantly and is certainly
better than [TOFU][], so that part of the project is definitely
interesting, *especially* since it allows leveraging classical
internet protocols like DNS and HTTPS.

Key exchange is a critical problem that still hasn't properly been
resolved in the cryptographic community. Most efforts at building
communication tools (say like [Signal][] or [Telegram][]) mostly
ignore the problem and expect people to read up strings of numbers or
rely on synchronous communications (see the [Axolotl rachet][] for
Signal's take on it). Keybase's attempts at fixing this are great, but
needs much more work to actually resolve the PKI issues in a
significant way.

[Axolotl rachet]: https://en.wikipedia.org/wiki/Axolotl_(protocol)
[Telegram]: https://en.wikipedia.org/wiki/Telegram_(software)
[Signal]: https://en.wikipedia.org/wiki/Signal_(software)

More fundamentally, the practice of storing keys on the servers should just
stop: it is a definite no go for me, and a classic crypto mistake that
has bitten way too many people. In my mind, it discredits the whole project. The
point of OpenPGP is end to end encryption across devices: storing the
keys on a server breaks that apart, and doesn't improve much on the
existing HTTPS security systems already in place on the web.

It is especially a problem given the new waves of attacks on
cryptography from western governments, from the UK to
California... Until those issues are resolved, I can't get myself to
recommend Keybase to anyone just yet.

Post-scriptum
-------------

*Note to Keybase developers: I have considered filing issues regarding
the above, but unfortunately, I was unable to filter through the
gigantic [github issues list][] for duplicates, and didn't have time
to file detailed reports for everything. I apologize for bypassing
those usual conventions.*

If you want to try out Keybase to make your own opinion, I do have 4
invites I can give away, but I will do so only if you need to test
specific issues. Otherwise, I note that there is a
[Request For Package][] for the Debian package to become official,
that Debian developers (or Keybase developers!) may be interested in
completing.

[Request For Package]: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=792916
[github issues list]: https://github.com/keybase/keybase-issues/issues
[TOFU]: https://en.wikipedia.org/wiki/Trust_on_first_use
[OpenPGP keyservers]: https://sks-keyservers.net/
[Franklin street statement]: http://web.archive.org/web/20150906201555/http://autonomo.us/2008/07/14/franklin-street-statement/
[free software]: https://github.com/keybase/client
[caff]: https://wiki.debian.org/caff
[monkeysign]: http://web.monkeysphere.info/monkeysign/

[[!tag debian-planet keybaseio software geek debian "free software" privacy security]]
