[[!meta title="My free software activities, february 2016"]]

[[!toc levels=2]]

Debian Long Term Support (LTS)
==============================

This is my third month working on [Debian LTS][], started by
[Raphael Hertzog at Freexian][]. This month was my first month working
on the frontdesk duty and did a good bunch of triage. I also
performed one upload and reviewed a few security issues.

 [Debian LTS]: https://www.freexian.com/services/debian-lts.html
 [Raphael Hertzog at Freexian]: http://www.freexian.com

Frontdesk duties
----------------

I spent some time trying to get familiar with the frontdesk duty. I
still need to document a bit of what I learned, which did involve
asking around for parts of the process. The following issues were
triaged:

 * roundcube in squeeze was happily not vulnerable to
   [CVE-2015-8794][] and [CVE-2015-8793][], as the code affected was
   not present. roundcube is also not shipped with jessie *but* the
   backport is [vulnerable][]
 * the php-openid vulnerability was actually just a code sample, a
   [bug report comment][] clarified all of [CVE-2016-2049][]
 * ffmpeg issues were closed, as it is not supported in squeeze
 * libxml2 was marked as needing work ([CVE-2016-2073][])
 * asterisk was triaged for all distros before i found out it is also
   unsupported in squeeze (CVEs coming up, [AST-2016-001][],
   [AST-2016-001][], [AST-2016-001][])
 * libebml and libmatroska were marked as unsupported, although an
   upload of debian-security-support will be necessary to complete
   that work ([bug #814557][] filed)

 [bug #814557]: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=814557
 [CVE-2016-2073]: https://security-tracker.debian.org/tracker/CVE-2016-2073
 [CVE-2016-2049]: https://security-tracker.debian.org/tracker/CVE-2016-2049
 [bug report comment]: https://github.com/openid/php-openid/issues/128#issuecomment-180546598
 [vulnerable]: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=813843
 [CVE-2015-8793]: https://security-tracker.debian.org/tracker/CVE-2015-8793
 [CVE-2015-8794]: https://security-tracker.debian.org/tracker/CVE-2015-8794
 [AST-2016-001]: http://downloads.asterisk.org/pub/security/AST-2016-001.html
 [AST-2016-002]: http://downloads.asterisk.org/pub/security/AST-2016-002.html
 [AST-2016-003]: http://downloads.asterisk.org/pub/security/AST-2016-003.html

Uploads and reviews
-------------------

I only ended up doing one upload, of the chrony package
([CVE-2016-1567][]), thanks to the maintainer which provided the
patch.

 [CVE-2016-1567]: https://security-tracker.debian.org/tracker/CVE-2016-1567

I tried my best trying to sort through the issues with tiff
([CVE-2015-8668][] and [CVE-2015-7554][]), which didn't have obvious
fixes available. OpenSUSE seem to have patches, but it is really hard
to find them through their issue trackers, which were timing out on
me. Hopefully someone else can pick that one up.

 [CVE-2015-7554]: https://security-tracker.debian.org/tracker/CVE-2015-7554
 [CVE-2015-8668]: https://security-tracker.debian.org/tracker/CVE-2015-8668
 
I also tried and failed to reproduce the cpio issue
([CVE-2016-2037][]), which, at the time, didn't have a patch for a
fix. This ended up being solved and Santiago took up the upload.

 [CVE-2016-2037]: https://security-tracker.debian.org/tracker/CVE-2016-2037

I finally spent some time trying to untangle the mess that is
[libraw][], or more precisely, all the packages that *embed* [dcraw][]
code instead of linking against `libraw`. Now I really feel even more
strongly for the [Debian policy section 4.13][] which states that
Debian packages should not ship with copies of other packages code. It
made it really hard to figure out which packages were vulnerable,
especially because it was hard to figure out which versions of
libraw/dcraw were actually vulnerable to the bug, but also just plain
figure out which packages were copying code from libraw. I wish I had
found out about [secure-testing/data/embedded-code-copies][]
earlier... Still, it was interesting to get familiar with
[codesearch.debian.net][] to try to find copies of the vulnerable
code, which was not working so well. Kudos to darktable 2.0 for
getting rid of their embedded copy of libraw, by the way - it made it
completely not vulnerable to the issue, the versions in stretch and
sid not having the code at all and older versions having
non-vulnerable copies of the code.

 [codesearch.debian.net]: http://codesearch.debian.net/
 [secure-testing/data/embedded-code-copies]: https://anonscm.debian.org/viewvc/secure-testing/data/embedded-code-copies?view=co
 [Debian policy section 4.13]: https://www.debian.org/doc/debian-policy/ch-source.html#s-embeddedfiles
 [dcraw]: https://tracker.debian.org/dcraw
 [libraw]: https://tracker.debian.org/libraw

Issues with VMs again
---------------------

I still had problems running a squeeze VM - not wanting to use
virtualbox because of the overhead, I got lost for a bit trying to use
libvirt and KVM. A bunch of issues crept up: using `virt-manager`
would just fail on startup with an error saying `interface mtu value
is improper`, which is a very unhelpful error message (what *is* a
proper value??) - and, for the record, the MTU on `eth0` and `wlan0`
is the fairly standard 1500, while `lo` is at 65536 bytes, nothing
unusual there as far as I know.

Then the next problem was actually running a VM - I still somewhat
expected to be able to boot off a chroot, something I should
definitely forget about it seems like (boot loader missing? not
sure). I ended up calling `virt-install` with the live ISO image I was
previously using:

    virt-install --virt-type kvm --name squeeze-amd64 --memory 512 --cdrom ~/iso/Debian/cdimage.debian.org_mirror_cdimage_archive_6.0.10_live_amd64_iso_hybrid_debian_live_6.0.10_amd64_gnome_desktop.iso --disk size=4 --os-variant debiansqueeze

At least now I have an installed squeeze VM, something I didn't get to
do in Virtualbox (mostly because I didn't want to wait through the
install, because it was so slow).

Finally, I still have trouble getting a commandline console on the VM:
somehow, running `virtsh console squeeze-amd64` doesn't give me a
login terminal, and *worse*, it actually freezes the terminal that I
can actually get on `virt-viewer squeeze-amd64`, which definitely
sounds like a bug.

I documented a bit more of that setup in the [Debian wiki KVM page][]
so hopefully this will be useful for others.

 [Debian wiki KVM page]: https://wiki.debian.org/KVM#Creating_a_new_host
 
Other free software work
========================

I continued my work on improving timetracking with ledger in my
[ledger-timetracking][] git repository, which now got a place on the
new [plaintextaccounting.org][] website, which acts as a portal for
ledger-like software projects and documentation.

 [plaintextaccounting.org]: http://plaintextaccounting.org
 [ledger-timetracking]: https://github.com/anarcat/ledger-timetracking/

Darktable 2.0
-------------

I had the pleasure of trying the new [Darktable 2.0 release][], which
only recently entered Debian. I built a backport for jessie, which
works beautifully: much faster thumbnail rendering, no dropping of
history when switching views... The new features are great, but I also
appreciate how they are being very conservative in their approach.

Darktable is great software: I may have trouble approaching the
results other are having with lightroom and snapseed, but those are
proprietary software that I can't use anyways. I also suspect that I
just don't have enough of a clue of what I'm doing to get the results
I need in Darktable. Maybe with hand-holding, one day, I will surpass
the results I get with the JPEGs from my Canon camera. Until then, I
turned off RAW exports in my camera to try and control the explosion
of disk use I saw since I got that camera:

    41M     2004
    363M    2005
    937M    2006
    2,2G    2007
    894M    2008
    800M    2009
    1,8G    2010
    1,4G    2011
    9,8G    2012
    31G     2013
    26G     2014
    9,8G    2015

The drop in 2015 is mostly due to me taking less pictures in the last
year, for some reason... 

 [Darktable 2.0 release]: http://www.darktable.org/2015/12/darktable-2-0-released/

Markdown mode hacks
-------------------

I ended up writing some elisp for the markdown mode. It seems I am
always writing links like `[text](link)` which seems more natural at
first, but then the formatting looks messier, as paragraph wrapping is
all off because of the long URLs. So I always ended up converting
those links, which was a painful series of keystrokes.

So I made a macro, and while I'm a it, why not rewrite it as a lisp
function. Twice.

Then I was told by the `markdown-mode.el` developers that they had
*already* fixed that (in the 2.1 version, not in Debian jessie) and
that the `C-c C-a r` key binding actually recognized existing links and
conveniently converted them.

I documented my adventures in [bug #94][], but it seems I wrote this
code for nothing else than re-learning Emacs lisp, which was actually
quite fun.

 [bug #94]: https://github.com/jrblevin/markdown-mode/issues/94

More emacs hacking
------------------

Another thing I always wasted time doing by and is "rename file and
buffer". Often, you visit a file but it's named wrong. My most common
case is a `.txt` file that i rename to `.mdwn`.

I would then have to do:

    M-x rename-file <ret> newfile
    M-x rename-buffer <ret> newfile
    C-x C-s <ret> newfile

Really annoying.

Turns out that `set-visited-file-name` actually does most of the job,
but doesn't actually rename the file, which is really silly. So I
wrote this small function instead:

    (defun rename-file-and-buffer (newfname)
      "combine rename-file and rename-buffer
    
    set-visited-file-name does most of the job, but unfortunately
    doesn't actually rename the file. rename-file does that, but
    doesn't rename the buffer. rename-buffer only renames the buffer,
    which is pretty pointless.
    
    only operates on current buffer because set-visited-file-name
    also does so and we don't bother doing excursions around.
    "
      (interactive "GRename file and bufer: ")
      (let ((oldfname (buffer-file-name)))
        (set-visited-file-name newfname nil t)
        (rename-file oldfname newfname)
        )
      )

Not bound to any key, really trivial, but doing this without that
function is really non-trivial, especially since
`set-visited-file-name` needs special arguments to not mark the file
as modified.

IRC packages updates
--------------------

I updated the [Sopel IRC bot][] package to the latest release, 6.3.0. They
have finally switched to [Requests][], but apart from that, no change was
necessary. I am glad to finally see [SNI support][] working everywhere in
the bot!

 [SNI support]: https://github.com/sopel-irc/sopel/pull/988
 [Sopel IRC bot]: http://sopel.chat
 [Requests]: http://python-requests.org/

I also update the [Charydbis IRC server][] package to the latest
[3.5.0 stable release][]. This release is great news, as I was able to
remove 5 of the [7 patches][] I was dragging along the Debian package. The
previous Charybdis stable release was over 3 years old, as 3.4.2 was
released in (December) 2012!

I spend a good chunk of time [making the package reproducible][]. I
filed a [bug upstream][] and eventually made a [patch][] to make it
possible to hardcode a build timestamp, which seems to have been the
only detectable change in the
[reproducible build infrastructure][]. Charybdis had been [FTBS][] for
a while in sid now, and the upload should fix that as
well. Unfortunately, Charybdis *still* doesn't build with hardening
flags - but hopefully a future update of the package should fix
that. It is probably because
[CFLAGS are not passed around properly][].

[CFLAGS are not passed around properly]: https://qa.debian.org/bls/packages/c/charybdis.html
[FTBS]: https://bugs.debian.org/812655
[reproducible build infrastructure]: https://tests.reproducible-builds.org/rb-pkg/unstable/amd64/charybdis.html
[patch]: https://github.com/charybdis-ircd/charybdis/pull/149
[bug upstream]: https://github.com/charybdis-ircd/charybdis/issues/148
[making the package reproducible]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=768328

There's really interesting stuff going on in the IRC world. Even
though [IRC][] is one of the oldest protocols still in operation
(1988, even before the [Web][World Wide Web], but after [SMTP][] and
the even more venerable [FTP][]), it is still being actively
developed, with a [working group][IRCv3] drafting multiple extensions
to the IRCv3 protocol defined in [[!rfc 1459]].

For example, IRCv3.3 includes a [Strict Transport Security][]
extension, which tries to ensure users use encrypted channels as much
as possible, through warnings and STARTTLS support. Charybdis goes
even further by [proposing][] a reversal of the `+S` ("*secure
channel*" flag) where *all* channels are secure by default, and you
need to *deliberately* mark a channel as insecure with the `+U` flag
if you actually want to allow users on an clear-text connection to
join the channel. A [transition mechanism][proposing] is also
proposed.

[proposing]: http://www.charybdis.io/2016/01/30/charybdis-and-ircv3-3-sts.html
[IRCv3]: http://ircv3.net/
[FTP]: https://en.wikipedia.org/wiki/FTP
[SMTP]: https://en.wikipedia.org/wiki/SMTP
[IRC]: https://en.wikipedia.org/wiki/Internet_Relay_Chat#History
[World Wide Web]: https://en.wikipedia.org/wiki/World_Wide_Web
[Strict Transport Security]: http://ircv3.net/specs/core/sts-3.3.html


[7 patches]: https://sources.debian.net/patches/charybdis/3.4.2-5/
[3.5.0 stable release]: http://www.charybdis.io/2016/01/05/charybdis-3.5.0-release.html
[Charydbis IRC server]: http://www.charybdis.io/

Miscellaneous bug reports
-------------------------

En vrac...

I fell face-first in this amazing game that is [endless-sky][]. I made
a [small pull request][] on the documentation, a [bug report][] and a
[feature request][].

I [forwarded a bug report][], originally filed against monkeysign, to
the pyqrencode maintainers.

I [filed a usability bug against tails-installer][], which just entered
Debian, mostly usability issues.

 [endless-sky]: http://endless-sky.github.io/
 [small pull request]: https://github.com/endless-sky/endless-sky/pull/768
 [bug report]: https://github.com/endless-sky/endless-sky.github.io/issues/2
 [feature request]: https://github.com/endless-sky/endless-sky/issues/774
 [forwarded a bug report]: https://github.com/Arachnid/pyqrencode/issues/7
 [filed a usability bug against tails-installer]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=814432

I discovered the [fim][] image viewer, which re-entered Debian
recently. It seemed perfect to adjust my [photos-import][] workflow,
so I added it to my script, to be able to review photos prior to
importing them into Darktable and git-annex.

 [fim]: http://www.nongnu.org/fbi-improved/
 [photos-import]: https://gitlab.com/anarcat/scripts/blob/1756e5df166e00462de290a42e4a18ee0994d9a3/photos-import

[[!tag monthly-report debian-planet debian python-planet software geek free debian-lts darktable markdown emacs irc]]
