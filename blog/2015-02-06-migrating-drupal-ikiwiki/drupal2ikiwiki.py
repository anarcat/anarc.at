#!/usr/bin/python3

"""Drupal to ikiwiki converter

Usage:
  drupal2ikiwiki [-vv] [options] -u <name> -d <database> <directory>

Options:
  -v --verbose             Be noisy.
  -n --dry-run             Don't write anything.
  -l --limit <count>       Only convert <count> pages [default: 1000]
  -o --offset <offset>     Start at <offset> pages [default: 0]
  -w --overwrite           Do not fail if files exist, overwrite.
  -s --server <server>     MySQL server [default: localhost] 
  -u --user <name>         MySQL user name
  -p --password <server>   MySQL server password (insecure)
  -d --database <database> MySQL database
  <directory>              Directory to export the nodes to, created
if missing.

Password is prompted.

Requirements:

 * docopt (python3-docopt)
 * mysql.connector (python3-mysql.connector)

"""

from datetime import datetime
from docopt import docopt
from getpass import getpass
import mysql.connector
import hashlib
import logging
import errno
import os
import subprocess

dryrun = None
mode = 'x'

def main():
    global dryrun, mode
    args = docopt(__doc__, version='drupal2ikiwiki 0.1')
    if args['--overwrite']:
        mode = 'w'
    levels = { 0: logging.WARNING,
               1: logging.INFO,
               2: logging.DEBUG }
    logging.basicConfig(level=levels[args['--verbose']],
                        format='%(message)s')
    logging.debug('parsed arguments {}'.format(args))
    dryrun = args['--dry-run']

    # 1. connect to drupal DB
    cnx = connect(args)
    # 2. create the directory if missing
    dir_init(args['<directory>'])
    cursor = cnx.cursor()
    offset = int(args['--offset'])
    nid_alias_map = {}
    # 3. for each node, one file per node:
    for row in list_nodes(cursor, offset, args['--limit']):
        # 2.0 has dictionnary, we're poor:
        # https://dev.mysql.com/doc/connector-python/en/connector-python-api-mysqlconnection-cursor.html
        row = dict(zip(cursor.column_names, row))
        row['offset'] = offset
        path = os.path.join(args['<directory>'], row['path'] + '.mdwn')
        if row['nid'] in nid_alias_map:
            logging.debug('{offset}: duplicate node {nid} {created} {path}'.format(**row))
            logging.info('creating redirection {} pointing to {}'
                         .format(path, nid_alias_map[row['nid']]))
            if not dryrun:
                with open(path, mode) as fd:
                    fd.write('[[!meta redir="{}"]]' "\n"
                             '[[!tag redirection]]'
                             .format(nid_alias_map[row['nid']]))
        else:
            logging.debug('{offset}: found node {nid} {created} {path}'.format(**row))
            nid_alias_map[row['nid']] = row['path']
            content = create_content(row)
            logging.info('writing page to {}'.format(path))
            if not dryrun:
                with open(path, mode) as fd:
                    fd.write(content)
        offset+=1
    # reset
    offset = int(args['--offset'])
    cursor = cnx.cursor()
    # 4. fetch all comments, historically
    for row in list_comments(cursor,
                                 args['--offset'],
                                 args['--limit']):
        # copied from above
        row = dict(zip(cursor.column_names, row))
        row['offset'] = offset
        basedir = os.path.join(args['<directory>'], row['path'])
        path = os.path.join(basedir,
                            'comment_{cid}._comment'.format(**row))
        logging.debug(('{offset}: found comment {cid} {timestamp}'
                       ' on node {nid} {path}').format(**row))
        content = create_comment(row)
        # * create comment in subdir, one file per comment
        logging.info('writing comment to {}'.format(path))
        if not dryrun:
            try:
                os.mkdir(basedir)
            except OSError as ex:
                if ex.errno != errno.EEXIST or not os.path.isdir(basedir):
                    raise
                with open(path, mode) as fd:
                    fd.write(content)
        offset+=1
    # 5. create a page per tag?
    cnx.close()

# 1. connect to drupal DB
def connect(args):
    # we could also parse a .cnf file:
    # https://stackoverflow.com/questions/5344396/how-to-use-mysql-defaults-file-my-cnf-with-sqlalchemy
    if not args['--password']:
        args['--password'] = getpass('Password: ')
    return mysql.connector.connect(user=args['--user'],
                                   database=args['--database'],
                                   password=args['--password'],
                                   host=args['--server'])

# 2. create the directory if missing
def dir_init(path):
    if not os.path.exists(path):
        logging.info('creating {} directory'.format(path))
        if not dryrun:
            os.mkdir(path)

# 3. for each node, one file per node:
def list_nodes(cursor, offset, limit):
    # XXX: we assume tags don't have commas
    # XXX: we assume that all nodes have paths in url_alias!
    query = ('SELECT n.nid,type,n.title,status,'
             '"http://anarcat.koumbit.org" AS url,'
             'NOW() AS today,'
             'created, changed, timestamp,'
             'teaser,body,format,dst AS path, '
             'GROUP_CONCAT(td.name SEPARATOR ",") AS tags '
             'FROM node n '
             'INNER JOIN node_revisions nr ON n.vid = nr.vid '
             'INNER JOIN url_alias u ON u.src = CONCAT("node/",n.nid) '
             'INNER JOIN term_node tn ON n.vid = tn.vid '
             'INNER JOIN term_data td ON tn.tid = td.tid '
             'WHERE n.status=1 '
             'GROUP BY dst '
             'ORDER BY nid '
             'LIMIT %s, %s ')
    cursor.execute(query, (int(offset), int(limit)))
    return cursor

def create_content(row):
        for time in ('created', 'changed', 'timestamp'):
            row[time] = datetime.fromtimestamp(row[time])
        # somehow carriage returns ended up in the text, kill that.
        row['body'] = row['body'].replace("\r",'')
        # quotes in titles will fail in meta, use single quotes
        # XXX: use """ instead?
        row['title'] = row['title'].replace('"', "'")
        # * tags (as `\[[!tag foo bar baz]]` at the bottom)
        # explodes tags, wrap them with double quotes, and space-separated
        row['tags'] = ' '.join(['"%s"' % x
                                for x in row['tags'].split(',')])
        # * nid and current date as comments
        # * title: [[directive/meta]]
        # * published date ([[directive/meta]] `date` directive)
        # * modification date ([[directive/meta]] `updated` directive)
        # XXX: updated only works for RSS feeds, not actual modifs date for the web version
        # * body
        # XXX: not sure how to deal with "<!-- break -->"
        # right now we simply ignore it, because ikiwiki doesn't
        # support blog summaries.
        return (
            '<!-- Drupal node {nid} imported from {url} on {today} -->' "\n"
            '[[!meta title="{title}"]]' "\n"
            '[[!meta date="{created:%Y-%m-%dT%H:%M:%S-0500}"]]' "\n"
            '[[!meta updated="{changed:%Y-%m-%dT%H:%M:%S-0500}"]]' "\n"
            '[[!meta guid="{nid} at {url}"]]' "\n"
            "\n"
            '{body}' "\n"
            "\n"
            '[[!tag {tags}]]'
        ).format(**row)
        # XXX: missing
        # * attached files: it seems like previously attached files
        # are now loose in the Drupal filesystem. the only solution i
        # see is to import them manually outside of this script.
        # example: http://anarcat.koumbit.org/node/157 -
        # snap-2010.03.22-17.14.50.png not present in the node
        # metadata, but linked in the body.
        # CCK fields could be imported, but it's a similar WTF: while
        # there's metadata linking the nodes and the files, the URLs
        # are actually raw HTML, so no need to do anything fancy.
        # so just copy files/ over
        # * commit each file seperately or emit git-fast-import data

def list_comments(cursor, offset, limit):
    query = (
        'SELECT n.nid,cid,subject,c.uid,c.comment,hostname,'
        '"http://anarcat.koumbit.org" AS url,'
        'NOW() AS today,'
        'FROM_UNIXTIME(c.timestamp) AS timestamp,'
        'c.status,c.format,name,mail,homepage,dst AS path '
        'FROM comments c '
        'INNER JOIN node n ON n.nid = c.nid '
        'INNER JOIN url_alias u ON u.src = CONCAT("node/",n.nid) '
        'GROUP BY cid '
        'ORDER BY cid '
        'LIMIT %s, %s '
        )
    cursor.execute(query, (int(offset), int(limit)))
    return cursor

#   * comments are a macro, like `\[[!comment ...content="""body"""]]`
#     * author name = `nickname` field
#     * date = same
#     * title = `subject`
#     * content = same
#     * email = `username`? or do not disclose?
# https://ikiwiki.info/ikiwiki/directive/comment/
def create_comment(row):
    # XXX: need to escape triple double quotes!
    # fields to convert:
    # c.uid => c.uid map to claimedauthor?
    # c.status,c.format => should check if variation
    # mail,homepage,hostname => dropped?
    row['comment'] = row['comment'].replace("\r",'')
    row['avatar'] = 'http://cdn.libravatar.org/avatar/' \
                    + hashlib.md5(row['mail'].encode('utf-8').strip().lower()).hexdigest()
    return (
        '<!-- Drupal comment {cid} on node {nid} imported from {url} on {today} -->'
        "\n"
        '[[!comment subject="""{subject}""" ' "\n"
        'date="{timestamp}"' "\n"
        'format="mdwn"' "\n"
        'username="{name}"' "\n"
        'content="""{comment}"""' "\n"
        'avatar="{avatar}"' "\n"
        ']]' "\n"
        ).format(**row)

if __name__ == '__main__':
    main()
