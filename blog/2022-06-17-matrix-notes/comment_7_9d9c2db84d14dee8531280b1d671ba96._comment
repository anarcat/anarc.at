[[!comment format=mdwn
 username="anarcat"
 subject="""talking in "you""""
 date="2022-07-21T15:58:05Z"
 content="""
Wow, there's a lot in there for a "really short" comment. I'll just address this, which seems to be the gist of that last comment:

> I could debate your "already taken over" statement about [...] but I see no point, apart from saying you're wrong (in my opinion, which is, you know, a thing everyone have)

See, that's the thing with opinions. Everyone has them and can throw them around like a kid throws rocks in a park. But eventually, their parents show them that throwing rocks isn't great for the other kids and they stop.

In this case, just telling me "I'm wrong" is not a productive opinion to express on this blog. I really hesitated in publishing your comment at all, because it really doesn't seem to bring anything new to the conversation (other than, you know, "you're wrong").

I would have preferred recommendations on how to correct or rephrase what you feel is so unfair about my article. You can disagree with it all you like, but I'm looking at better ways to improve the article, not an endless debates about peccadilles.

In the meantime, I won't be approving further "you're wrong" comments unless they bring constructive corrections to the article.

I don't believe I have been "unjust" in my analysis or have made "false assumptions". If I did, I am happy to correct those, but I will point out that a 8000 words text is bound to be interpreted in many different ways, so assuming intent of the writer is a mistake you should probably avoid.
"""]]
