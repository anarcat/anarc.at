<!-- Drupal node 111 imported from http://anarcat.koumbit.org on 2015-02-06 17:55:35 -->
[[!meta title="Élections, pièges à cons"]]
[[!meta date="2007-03-10T19:58:44-0500"]]
[[!meta updated="2011-12-22T01:23:48-0500"]]
[[!meta guid="111 at http://anarcat.koumbit.org"]]

Je me suis tapé le devoir à matin, et j'ai eu le bonheur de lire un
article qui parlait comment des "intellectuels" considéraient la
campagne électorale. Fort intéressant: d'une vingtaine d'interpellés,
aucun n'appuie le PLQ, deux appuient l'ADQ, un bon paquet appuient le PQ
par dépit et quelques uns voient dans QS la "seule alternative viable".
Un gros paquet refusent également d'énoncer leur position électorale.

Tout ça pour dire que j'ai trouvé ça crissement déprimant de voir le
monde dirent qu'ils allaient voter pour le PQ parce que c'est la seule
option viable pour l'indépendance (??!) ou parce qu'ils y croient encore
un peu. Ça pue le cynisme à plein nez. J'ai apprécié lire ceux qui
supportent QS, ça me donne quasiement le goût de voter, même si il y en
a une qui disait qu'elle le faisait par regret, parce qu'elle sait que
le PQ ne sait même plus pourquoi il fait l'indépendance.

Toute cette campagne, anyways, a aucune substance intellectuelle ou
sociale (comme la plupart des campagnes électorales au Canada, selon
moi). Les véritables enjeux ne sont jamais réellement débattus tant
qu'ils ne sont pas énoncés par des ennemis électoraux, et à peine. On
est toujours au niveau des attaques ad hominen, on discute jamais
vraiment du fond, ou quand on le fait, on est tous d'accord qu'il faut
réduire les listes d'attente des hôpitaux, la pauvreté, les impôts, la
dette et les gaz à effet de serre, tout en même temps.

Au moins QS a l'honnêteté de pas tout faire ça en même temps (right?),
même qu'ils se le font reprocher ("ils vont monter les impôts! au
meurtre!!!").

Si j'aurais pas perdu tout forme de confiance en notre système électoral
pourri, catholique et colonialiste, je voterais pour eux. Je suis
cependant content d'avoir ma journée de congé et de "rester dans mon lit
douillet"...

Votez bien, votez rien.


[[!tag "réflexion" "politique"]]