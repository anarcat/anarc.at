[[!meta title="Liste d'équipement de voyage et de plein air"]]

> Cette liste d'équipement vise à documenter le matériel nécessaire pour
> tout type de voyage, de plein air ou pas, peu importe la saison ou le
> lieu. Il est évidemment adapté à mon niveau d'entraînement (modéré),
> de connaissances technologiques (élevé) et d'habilités (canot, ski,
> marche en montagne, escalade, apnée).
> 
> La liste est pour mon usage personnel, mais publiée pour que d'autres
> puissent l'utiliser. Il est possible que des équipements critiques
> pour *votre* voyage manquent, et vous êtes encouragés à y
> contribuer. Cependant, les personnes ayant contribué à cette liste se
> déchargent de toute responsabilité pour les dommages ou problèmes
> découlant de l'utilisation de cette liste.

La liste n'est pas destinée à être imprimée: elle est trop longue et
trop variée. Je copie généralement une partie des items que j'ai
besoin dans l'activité choisie dans un calepin ou une feuille de
papier que je peux traîner avec moi dans la maison, voire même dans
l'activité (pour vérifier que j'ai rien oublié!) et éviter d'avoir à
toujours retourner sur l'ordinateur.

[[!toc levels=3]]

# Camping

 * Sac de couchage
 * Tapis de sol
 * Tente
 * Bâche
 * Cordes (4 x 5m aux coins de la bâche)
 * Oreiller
 * Sac a viande
 * Hamac et chaînes
 * Sac à dos, grand et/ou petit, ou valise
 * Lampe de poche
 * Lampe frontale

# Kit de survie

 * Boussole
 * Balises
 * Briquet
 * Allumettes dans étui étanche
 * Sifflet
 * Allume-feu
 * Leatherman / canif suisse / opinel
 * Trousse de premiers soins (voir plus bas)

# Orientation

 * Papier
 * Crayon de plomb
 * Carte topographique / hydrographique / escalade
 * Carte routière
 * GPS
 * Cherche-étoile
 * Cherche-arbre
 * Livre d'identification d'oiseaux, etc
 * Contact d'urgence
 * Dictionnaire de traduction (e.g. fr-es)

# Papiers et autres attaches

 * Montre
 * Passeport (e.g. USA!)
 * Argent en monnaie locale
 * Carte de crédit
 * Carte de débit
 * Permis de conduire
 * Clef(s)
 * Cadenas
 * Loyer (payé)

# Divertissement

 * Livre / liseuse
 * Caméra et chargeur
 * Trépied
 * Musique pour l'auto (MP3 player)
 * Porto ou autre alcool
 * Marie-Jeanne, etc.
 * Instrument de musique (e.g. harmonica)

# Électronique

 * Téléphone cellulaire et chargeur
 * Radio CB/FM/HF/etc, antenne(s) et courant
 * Adapteur SDR (software-defined Radio)
 * Adaptateurs électriques:
   * AC/AC universel (amerique/europe/etc)
   * DC/DC 12V-USB (allume-cigare)
   * AC/DC 120V-USB (wall wart)
   * DC/AC (onduleur)
 * Chargeur / batterie USB
 * Laptop et chargeur
 * Kit laptop #1:
   * Mini-câble USB micro, USB mini, USB-C, USB-A, [Lightning](https://en.wikipedia.org/wiki/Lightning_(connector))
   * Câble USB-A / USB-micro Nokia (2m)
   * Clé USB 16GB Debian
   * [Condom USB](https://en.wikipedia.org/wiki/USB_condom)
   * USB charger (USB-A / [NEMA](https://en.wikipedia.org/wiki/NEMA_connector), 1A / 5V)
 * Kit laptop #2:
   * Câble ethernet RJ45 (1m)
   * Câble vidéo HDMI (1.5m)
   * Câble audio ⅛" (1.5m)
   * "Splitter" audio ⅛"
   * Mini powerbar
   * Adaptateur mini-DisplayPort / HDMI
 * Écouteurs avec micro
 * Haut-parleur Bluetooth

# Équipement technique

 * Raquettes
 * Skis de fond
 * Bâtons
 * Bottes de ski
 * Cire à skis
 * Spatule de ski de secours
 * Thermomètre (pour le fart)
 * Crampons
 * Wetsuit
 * Tuba et masque
 * Petits mousquetons ("not for climbing")

# Cuisine

 * Bouteilles d'eau
 * Thermos
 * Eau ou filtrage, un de:
   * pompe à eau
   * comprimés
   * gouttes
   * lampe UV
 * Briquet
 * Allumettes imperméables
 * Carburant
 * Réchaud
 * Kit de réparation pour réchaud
 * Ustensiles
 * Ustensiles de cuisson
 * Savon à vaisselle (biodégradable, de préférence)
 * Gamelles
 * Bol à soupe
 * Ouvre-Boîte
 * Tampon à récurer avec éponge
 * Linge à vaisselle
 * Glacière
 * Sacs poubelle / compost
 * Tupperwares pour les restes

# Trousse de dépannage

 * Batteries AAA / AA (rechange)
 * Chandelle-torche
 * Chandelles
 * Papier d’aluminium
 * Fil à coudre & aiguille
 * Duct tape
 * Fil de laiton
 * Épingles à couche
 * Fil à pêche
 * ''Hot pad''

# Hygiène personnelle

 * Papier de toilette
 * Serviettes sanitaires / keeper
 * Mouchoir de poche
 * Verres de contact & kit de nettoyage
 * Crème solaire
 * Anti-moustique en crème ou vaporisateur
 * Dentifrice
 * Brosse à dents
 * Savon
 * Baume à lèvres
 * Coupe-ongles
 * Cure-oreilles
 * Crème à mains
 * Déodorant
 * Débarbouillettes humides
 * Bouchons pour oreilles
 * Peigne
 * Rasoir
 * Capotes
 * Détergent
 * Corde à linge

# Vêtements

 * Chapeau
 * Lunettes de ski
 * Lunettes
 * Lunettes de soleil
 * T-shirt
 * Chandail long
 * Chandail de laine / polaire
 * *Bobettes*
 * Pantalon
 * Sous-vêtements synthétique (haut & bas)
 * Pantalons de pluie
 * Pantalons de neige
 * Bottes de marche
 * Souliers de course
 * Sandales
 * Pantoufles
 * Couvres-pantoufles
 * Bas ...
   * ... de laine
   * ... normaux
 * Maillot de bain
 * Serviette
 * Manteau d'hiver
 * Coupe-vent imperméable (gore-tex) / Anorak
 * Tuque
 * Guêtres
 * Gants
 * Gants de construction
 * Mitaines et sous-mitaines
 * Foulard / Masque facial / Cache-cou
 * Filet anti-moustique

# Trousse de premiers soins

J'utilise désormais la [trousse guide de Sirius][]. La liste est en
anglais car la liste papier avec la trousse l'est également. La
trousse coûte 100$ et est disponible par [téléphone][].

 [trousse guide de Sirius]: http://www.siriusmed.com/fr/produit/trousses-de-premiers-soins-et-fournitures-medicales/
 [téléphone]: http://www.siriusmed.com/fr/nous-joindre/
 [Contenu de la trousse]: http://www.siriusmed.com/wp-content/uploads/Sirius-Guide-Kit_trousse-guide.pdf

[Contenu de la trousse][]:

 * Abdominal/combine pad 12.7 x 22.9 cm (1)
 * Accident/illness report form (2)
 * Benzalkonium chloride antiseptic towelettes (12)
 * Blanket, emergency foil/mylar, 142.2 x 203.2 cm (1)
 * Conforming stretch bandage, 7.6 cm x 3.7 m, sterile (2)
 * Cotton tipped applicators, 7.6 cm, sterile (10)
 * Elastic support/compression bandage, 7.6 cm x 1.6 m (1)
 * Emergency survival pocket guide (1)
 * Fabric bandages, 2.2 x 7.6 cm (12)
 * Fabric bandages, knuckle, 3.8 x 7.6 cm (5)
 * Fabric dressing strip, 7.6 cm x 15.2cm (1)
 * First aid antibiotic ointment, 0.9 g
 * Gauze pads, 5.1 x 5.1 cm (6)
 * Gauze pads, 7.6 x 7.6 cm (6)
 * Gauze pads, 10.2 x 10.2 cm (6)
 * Iodine topical antiseptic, 5%, 25 ml (1)
 * Moleskin padding, 5.1cm x 46 cm (2"x18") (1)
 * Nitrile medical examination gloves, large (4)
 * Pencil, 8.9 cm (1)
 * Penlight, disposable (1)
 * Pill holder vial w/tamper-resistant cap (2)
 * Razor w/ prep blade (1)
 * Safety pins, assorted sizes (6)
 * Scissors, universal paramedic, 15.9 cm (1)
 * Second skin moist burn pads, medium, 5.1 x 7.6 cm (1)
 * Splinter forcep, fine point, 11.4 cm (1)
 * Steri-strip closures, 6mm x 7.6cm (1)
 * Syringe only 10cc luer-lok (1)
 * Tape, cotton cloth, 1.27 cm x 4.6 m (1)
 * Tape, cotton cloth, 2.5 cm x 4.6 m (1)
 * Tegaderm, transparent film dressings, 10cm x 2 cm (2)
 * Telfa non-adherent pads, 5.1 x 7.6 cm (5)
 * Thermometer, oral glass dual-scale (1)
 * Triangular bandage 101.6 x 101.6 x 142.2 cm (2)

Extras:

 * *Duct tape*
 * Tampons d'alcool (*Afterbite*) (2)
 * Steri-strip (1) 6mm x 10 cm
 * Safety pins, large (3)
 * Moleskin (1)
 * SOAP notes (2)
 * Fabric bandages, 5.0 cm x 7.6 cm (10, for blisters)
 * Non-adherent pads 7.5 cm x 10 cm (2)

Médicaments:

 * [Paracetamol][] 500mg (Analgésique, *Tylenol*) (35)
 * [ASA][] 325mg (Analgésique, *Aspirin*) (seulement si cardiaques)
 * [Ibuprofen][] 200mg (Anti-inflammatoire, *Advil*) (35)
 * [Loperamide][] 2mg (Antidiarrhéique, *Immodum*) (6)
 * [Loratadine][] 10mg (Anti-allergique, *Claritin*) (été seulement)
 * [Diménhydrinate][] (Anti-nausée, *Gravol*) (en mer seulement)
 * [Épinéphrine][] (Traitement anti-anaphalactique, *Épipen*, seulement si
   allergies sévères dans le groupe, considérer du Benadryl également
   pour compléter, mais voir cette [discussion sur wikipedia][])

 [Paracetamol]: https://en.wikipedia.org/wiki/Paracetamol
 [ASA]: https://en.wikipedia.org/wiki/Aspirine
 [Diménhydrinate]: https://en.wikipedia.org/wiki/Dimenhydrinate
 [Ibuprofen]: https://en.wikipedia.org/wiki/Ibuprofen
 [Loperamide]: https://en.wikipedia.org/wiki/Loperamide
 [Loratadine]: https://en.wikipedia.org/wiki/Loratadine
 [Épinéphrine]: https://en.wikipedia.org/wiki/Epinephrine
 [discussion sur wikipedia]: https://en.wikipedia.org/wiki/Talk:Anaphylaxis#Preferred_post-epipen_medication?

# Notes

## Consommation d'eau

L'hiver, on doit compter, en moyenne, au moins un litre d'eau par jour
par personne, sans compter l'eau nécessaire pour la vaisselle, le
lavage et l'hygiène personnelle. La neige peut *parfois* être utilisée
mais demande évidemment du carburant pour fondre.

L'été, les demandes d'eau sont plus grandes. Selon la température, on
compte au moins 1.5L voire deux litres d'eau à boire par personne par
jour. Le MSPQ (Ministère de la Sécurité Publique du Québec) dit de
prévoir 2L par personne par jour, pour trois jours, dans les cas
d'urgence ([source][Trousse d'urgence du MSPQ]).

## Pour purifier l'eau:

 * Les pompes à eau fonctionnent bien et sont très fiables, mais
   doivent être nettoyées périodiquement, ce qui peut être
   problématique durant une expédition.

 * Les lampes UV sont beaucoup plus pratiques, mais demandent du
   courant.

 * Les pillules goûtent parfois mauvais, mais certaines sont
   pratiquement sans goût. 
   
 * En cas d'urgence, l'iode de la trousse de premiers soin peut être
   utilisée, 6 gouttes (0.3mL) 2% par litre + 30 minutes, dans l'eau
   tiède. Efficacité limitée contre Giardia ([source](https://portail-plein-air.weebly.com/traitement-de-leau.html)).

## Quelques expériences:

 * séjour à un chalet avec électricité sans eau courante, 5 jours, 11
   personnes: 3 cruche de 5 gallons (1L/pers/jour)

 * similaire, 3 jours, 2 personnes: 1 cruche de 2 gallons et un sac
   d'eau de 2+ gallons, cruche non-utilisée (~300+mL/pers/jour)

## Consommation de carburant

Il est toujours difficile de choisir combien de carburant
amener. Voici quelques expériences que j'ai noté:

* [[!wikipedia Naphta]] dans un Whisperlite International: 4.4L d'eau bouillie par 100mL ([source][]) - [ce site][] dit qu'une petite bouteille de 11oz peut durer une semaine, mais ça me semble optimiste.
* trip de ski dans les chics-chocs de 4 jours: utilisé environ 350mL (une bouteille de 325mL pleine et un peu plus) de naphte en plus d'une bouteille de propane Primus pour 6 personnes, incluant plusieurs cafés, thé, chauffé l'eau pour la vaisselle des fois, etc -- TheAnarcat 2015-03-17T11:47:19-0400
* canot-camping parc de la mauricie, 3 jours, 5 personnes: 325 mL épuisés pour plusieurs pâtes, thés -- TheAnarcat 2015-07-24T19:07:09-0400
* canot-camping parc de la verendrye, 5 jours, 6 personnes: ~2 cans de
  propane sur un four coleman -- anarcat 2017-07-14
* lire aussi: <http://bushwalkingnsw.org.au/clubsites/FAQ/FAQ_Efficiency.htm>

 [source]: http://www.cascadedesigns.com/msr/stoves/simple-cooking/whisperlite-universal/product#specs
 [ce site]: http://www.summitpost.org/fuel-consumption-how-much-fuel-to-bring/754460

# Références

Les différentes sources qui a permis de créer cette page.

 * [Liste personnelle de Antoine][]
 * Liste personnelle de [SuperOli][]
 * Fiche technique, [La traversée de Charlevoix][]
 * Randonnée pédestre au Québec, Guide Ulysse, 1997
 * [AMC: The Right Stuff… for winter][]
 * [Liste d’expédition du Parc national de la Mauricie][]
 * ["MégaListe" du groupe de plein air Koumbit][]
 * [Liste pour un voyage de canot-camping avec 2 jours d'approche en vélo][]
 * [Équipement requis de Alexhike.com][]
 * [Trousse d'urgence du MSPQ][]
 * [Our Around the World Packing List][]

 [Liste personnelle de Antoine]: https://anarc.at/pleinair/liste/
 [SuperOli]: https://wiki.koumbit.net/SuperOli
 [La traversée de Charlevoix]: http://www.charlevoix.net/traverse/
 [AMC: The Right Stuff… for winter]: http://www.outdoors.org/publications/outdoors/2002/2002-winter-gear.cfm
 [Liste d’expédition du Parc national de la Mauricie]: http://www.pc.gc.ca/pn-np/qc/mauricie/index_f.asp
 ["MégaListe" du groupe de plein air Koumbit]: https://wiki.koumbit.net/PleinAir/MégaListe
 [Équipement requis de Alexhike.com]: http://www.alexhike.com/informer/equipements-requis/
 [Liste pour un voyage de canot-camping avec 2 jours d'approche en vélo]: https://wiki.koumbit.net/PleinAir/ListeCanotCamping
 [Trousse d'urgence du MSPQ]: https://www.securitepublique.gouv.qc.ca/securite-civile/se-preparer-aux-sinistres/plan-familial-1/trousse-urgence.html
 [Our Around the World Packing List]: https://www.earthtrekkers.com/around-the-world-packing-list/
