# LASAGNE CRÉMEUSE AUX LÉGUMES RÔTIS

## Ingrédients

 * 1 oignon, haché finement
 * 2 gousses d’ail, hachées finement
 * 30 ml (2 c. à soupe) d’huile d’olive
 * 2 boîtes de 796 ml (28 oz) de tomates italiennes entières, écrasées à la main
 * 30 ml (2 c. à soupe) de basilic frais ciselé
 * Sel et poivre

### Légumes rôtis

 * <del>1 grosse aubergine, coupée en rondelles de 1,5 cm (1/2 po) d’épaisseur</del>
 * 2 poivrons de couleur, épépinés et coupés en lanières
 * 2 courgettes, coupées en tranches de 5 mm (1/4 po) d’épaisseur dans le sens de la longueur
 * 60 ml (1/4 tasse) d’huile d’olive
 
### Béchamel au parmesan

 * 30 ml (2 c. à soupe) de beurre
 * 30 ml (2 c. à soupe) de farine
 * 310 ml (1 1/4 tasse) de lait
 * 20 g (1/4 tasse) de fromage parmigiano reggiano râpé
 * 1 pincée de muscade moulue

### Lasagne

 * 15 ml (1 c. à soupe) de beurre
 * 300 g (10 tasses) de bébés épinards frais
 * 9 pâtes à lasagne sans précuisson (voir note)
 * 300 g (3 tasses) de fromage mozzarella râpé
 * 2 courgettes moyennes, tranchées en fins rubans à la mandoline
 * 20 g (1/4 tasse) de fromage parmigiano reggiano râpé

## Directions

### Sauce tomate

Dans une casserole, à feu moyen, dorer l’oignon et l’ail dans
l’huile. Ajouter les tomates. Laisser mijoter 30 minutes. Ajouter le
basilic. Saler et poivrer. Réserver.

### Légumes rôtis

Placer deux grilles au centre du four. Préchauffer le four à 220 °C
(425 °F). Tapisser deux plaques de cuisson de papier parchemin.

Sur une plaque, mélanger l’aubergine avec 45 ml (3 c. à soupe)
d’huile. Saler et poivrer. Sur l’autre plaque, mélanger les poivrons
et les courgettes avec le reste de l’huile (15 ml/1 c. à soupe). Saler
et poivrer. Répartir les légumes bien à plat en une seule
couche. Cuire au four les deux plaques en même temps de 15 à 20
minutes ou jusqu’à ce que les légumes soient bien rôtis. Réserver.
Béchamel au parmesan

Dans une casserole, à feu moyen, fondre le beurre. Ajouter la farine
et cuire 1 minute en remuant à l’aide d’un fouet. Ajouter le lait en
fouettant. Porter à ébullition. Laisser mijoter doucement 5 minutes en
remuant fréquemment pour éviter que la sauce colle au fond. Ajouter le
parmesan et la muscade. Saler et poivrer. Réserver. 

### Lasagne

Placer la grille au centre du four. Régler le four à 190 °C (375 °F).

Dans une grande poêle antiadhésive, à feu moyen-élevé, fondre le
beurre. Ajouter les épinards et cuire 1 minute ou jusqu’à ce qu’ils
soient tout juste tombés.

Dans un plat de cuisson de 33 x 23 cm (13 x 9 po), étaler 250 ml (1
tasse) de sauce tomate et couvrir d’un rang de pâtes. Répartir les
aubergines et les épinards. Ajouter 250 ml (1 tasse) de sauce tomate
et la moitié de la béchamel. Parsemer de 75 g (3/4 tasse) de fromage
mozzarella et couvrir d’un rang de pâtes. Couvrir avec 250 ml (1
tasse) de sauce tomate, les courgettes et les poivrons rôtis et le
reste de la béchamel. Terminer avec un dernier rang de pâtes, le reste
de la sauce tomate et du fromage. Couvrir complètement la surface avec
les tranches crues de courgettes en les tressant si désiré. Parsemer
avec le parmesan.

Cuire au four 45 minutes, puis dorer quelques minutes sous le gril
(broil) du four. Laisser reposer 15 minutes avant de servir.

NOTE: Il est important d’acheter des pâtes à lasagne sèches portant la
mention « précuites et prêtes à utiliser » 
