[[!meta title="Tofu au romarin et sauce au porto"]]

<span /><div class="nocount">

### Marinade

- 1 tasse (250mL) de jus de pommes
- 1/4 de tasse (62.5mL) de moutarde de Dijon
- 2 c. à table (30mL)d’huile d’olive
- 1/4 de tasse (62.5mL) de cassonade
- 2 c. à table (30mL) de miel liquide
- 2 c. à table (30mL) de romarin frais haché
- 4 gousses d’ail hachées finement
- 1 c. à thé (5mL) de poivre noir du moulin
- 1 c. à thé (5mL) de graines de moutarde
- 1 livre (254g) de tofu taillés en cubes de 1 pouce

### Sauce

- 1/4 de tasse (62.5mL) d’huile d’olive
- 2 échalotes hachées
- 3 c. à table (45mL) de porto
- 2 c. à table (30mL) de vinaigre balsamique
- romarin frais
- sel

Dans un bol, à l’aide d’un fouet, mélanger la marinade, puis ajouter
le tofu. S'assurer que le tofu est couvert par la marinade. Couvrir le
plat d’une pellicule de plastique.

Mariner au réfrigérateur au minimum 20 minutes ou jusqu’à 2 heures,
voire 12-24h.

Entre-temps, faire revenir les échalotes dans l'huile. Griller le tofu
jusqu'à ce qu'il soit bien grillé, voire carbonisé, saler et poivrer.

Déglacer avec le porto et vinaigre. Tamiser la marinade, puis
incorporer et réduire.
