---
geometry: margin=3cm
---

# Filets de porc au romarin et sauce au porto 

## Ingredients

- 1 tasse de jus de pommes 
- 1/4 de tasse de moutarde de Dijon
- 2 c. à table d’huile d’olive
- 1/4 de tasse de cassonade
- 2 c. à table de miel liquide
- 2 c. à table de romarin frais haché
- 4 gousses d’ail hachées finement
- 1 c. à thé de poivre noir du moulin
- 1 c. à thé de graines de moutarde

## Sauce

- 1 c. à table d’échalotes hachées
- 3 c. à table de porto
- 2 c. à table de vinaigre balsamique
- 1/4 de tasse d’huile d’olive
- romarin frais
- sel

## Directions

Dans un bol, à l’aide d’un fouet, mélanger le jus de pomme, la moutarde
de Dijon, l’huile d’olive, la cassonade, le miel, l’ail, le romarin, le
poivre et les graines de moutarde.

Placer les filets de porc dans un plat en verre peu profond.

Verser la marinade sur les filets de porc et les retourner pour bien
les enrober.

Couvrir le plat d’une pellicule de plastique. 

Mariner au réfrigérateur au minimum 20 minutes ou jusqu’à 2 heures.

Entre-temps, faire revenir les échalotes dans l'huile.

Ajoutez le porto et vinaigre.

Saler, poivrer et réserver après avoir chauffé.

Ajouter la marinade. 

Faire réduire. 

Passer au tamis.

Cuire à 350°F sur la grille du four ou de 12 à 16 minutes sur le BBQ.

Après avoir mariné, couper les filets de porc en tranches d’un pouce.

Ajouter le jus dans la sauce Porto. 

Arroser de sauce.

\pagebreak

## Tofu addendum

```
Antoine Beaupré <anarcat@orangeseeds.org> (2024-01-07) (inbox sent)
Date: Sun, 07 Jan 2024 16:07:25 -0500
From: Antoine Beaupré <anarcat@orangeseeds.org>
To: Nick Ackerley <ea610@ncf.ca>
Subject: Re: Tofu

On 2024-01-06 10:49:00, Nick Ackerley wrote:
> Hi Antoine!
>
> Can you share your amazing not-pork-tofu recipe with me?
>
> Sad you weren't able to visit with Rachel et al., this time. Maybe next?

It would have been great to hang out indeed! But I took a rain check and
visited other friends up north, but really mostly alone, taking a break
from the family has been kind of great, to be frank. :) Hopefully we can
make this ride soon enough again, now that it seems the boys handle it
much better!

Here's the pork-based recipe, from my mom. Replace tofu with pork,
obviously, but after the marinade, i kind of just wing it: if you can
fry the tofu on a BBQ that's good, oven can be good as well, best would
probably be an air frier. For heapmas, i just fried the green onions
with oil, fried the tofu until it's almost charred, then deglazed with
the port and vineager, then added all the marinade and reduced until it
was yum yummy.

-- 
The odds are greatly against you being immensely smarter than everyone\
else in the field. If your analysis says your terminal velocity is
twice the speed of light, you may have invented warp drive, but the
chances are a lot better that you've screwed up.
- Akin's Laws of Spacecraft Design
```
