Location
========

Dériveurs
---------

[Club de voile memphré](http://www.voilememphremagog.com/location_prix.htm#location):

 * 22$/hre pour 10h
 * 30$/hre
 * 48$/2h
 * 63$/3h
 * 84$/4h
 * 105$/8h

[CLUB NAUTIQUE DU PETIT LAC MAGOG](http://www.cnplm-magog.net/)

 * 25$/hre
 * 40$/2h
 * 50$/3h
 * 70$/jr

30% de rabais pour les membres, 425$ pour membership.

[Burlington community sailing center](http://communitysailingcenter.org/rentals-storage/rental-procedures/):

 * 30$/hr (Hobie One, 420)
 * 55$/hr (Sonar, Rhodes 19)

Quillards
---------

[École de voile Yvanhoe](http://www.voile-yvanhoe.com/index.php/location):

 * 250$ / 9h (8h-17h)
 * 450$ / 33h (8h -17h le lendemain)

Louent un [Tanzer 22](http://sailboatdata.com/viewrecord.asp?CLASS_ID=301) qui loge 4 personnes pour la journée vraiment serrés, peut-être 3 pour la nuit...

[Voil'allier](http://www.voilallier.com/voilier-a-louer-bateau-a-louer.html):

Jusqu'au 15 juin, [ranger 33](http://www.voile.org/cgi-bin/classifieds/classifieds.cgi?db=a_louer&website=&language=&session_key=&search_and_display_db_button=on&results_format=long&db_id=194&query=retrieval):

 * 400$/jour
 * 550$/2 jrs
 * 850$/5 jrs

[Voile.org](http://www.voile.org/cgi-bin/classifieds/classifieds.cgi?db=a_louer&website=&language=&session_key=&search_and_display_db_button=on&results_format=headlines&category=Am%E9rique+(Nord)+/+America+(North)&query=category) a beaucoup de résultats également... Exemples:

 * [Irwin 34](http://www.voile.org/cgi-bin/classifieds/classifieds.cgi?db=a_louer&website=&language=&session_key=&search_and_display_db_button=on&results_format=long&db_id=415&query=retrieval): 450$/jr (Monty's Bay, USA)
