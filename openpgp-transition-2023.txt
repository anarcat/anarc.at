-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Today is 2023-05-30.

I have transitioned from my 2009 RSA key to a elliptic curve
(Curve25519) key.

The old key will continue to be valid for some time, but I prefer all
future correspondence to use the new one. I would also like this new key
to be reintegrated into the web of trust. This message is signed with
both keys to certify this transition.

The old key was:

pub   rsa4096/792152527B75921E 2009-05-29 [SC] [expires: 2023-06-19]
      8DC901CE64146C048AD50FBB792152527B75921E

And the new key is:

pub   ed25519/02293A6FA4E53473 2023-05-30 [SC] [expires: 2024-05-29]
      BBB6CD4C98D74E1358A752A602293A6FA4E53473

Here is a copy of the new public key:

- -----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEZHZPzhYJKwYBBAHaRw8BAQdAWdVzOFRW6FYVpeVaDo3sC4aJ2kUW4ukdEZ36
UJLAHd60I0FudG9pbmUgQmVhdXByw6kgPGFuYXJjYXRAYW5hcmMuYXQ+iJYEExYI
AD4WIQS7ts1MmNdOE1inUqYCKTpvpOU0cwUCZHZPzgIbAwUJAeEzgAULCQgHAgYV
CgkICwIEFgIDAQIeAQIXgAAKCRACKTpvpOU0c/T0AP9eKqSDooC9IwGp0ms69Mau
E9YhsejSOVLEgXCf732OIQEAysJX3SRvjUkVT3qHDYL6uqmDtRhK8QWdshNp2H4G
dQyJAjMEEAEIAB0WIQSNyQHOZBRsBIrVD7t5IVJSe3WSHgUCZHZewAAKCRB5IVJS
e3WSHpl6EADD8F3sFn9hL30/Nlp44LS0FHCqo4tU/9uf0nMuBmgSJHXKi6Jpsy4J
cxkmf5xAyO8A3HiNAporrINSLRwBPSUMReLiD8rzJ/xJO4W+F0jxrNSPWBi+pbZc
NPJSogmcnv8bdh6Cbye1V2p+VWv9kIuY21o7zvRazqs4IRA8bRfBzusrsDBuQqYa
SXnHupXGcBylcEIHXpSSAgZfSpoLjpY23iZexbksCpF96lMJ5pH9cOEdljsI8HL0
WATq0zNdFEWQgq5c/Fh0ku4DDfHSnsf62Qe5j4N9CuZDtknl8ON5pFrzlWU1Yukt
Qv8jgkWLrz4gxXPQizfJHPBtlYApIbt8nGLNp6mtgrO9CWMAnInksfKPLIFwNW1d
45etQgGXQXbBbaq5K9g4fJc4HUQVLg/mEIRtk7xO+Ohsj1h3EjfoLh0X+w/SfUcc
12lcSR1rFPMBs2HKd0u1SUlWhCDxJqFej4ZkE7Ec6IxExj0Pm/Umqxl9nilp/uT4
RBB10iLqkoDAZpTGoUwqG0/gUnWiqAAZKjJku/ZNjO+rzUmzRZBqfX+xD4p97dgb
KgugstucU4t9BsiPQ+FlTCh+25qaP3K3xhjEo1+j1rO9YOgs7dwHVs7zIXeaZ9YY
DfhGS/slkZRz74nGUUThdpFK0p6+1ZrJVeQttiwvm/gb/jjWuWbk9LQlQW50b2lu
ZSBCZWF1cHLDqSA8YW5hcmNhdEBkZWJpYW4ub3JnPoiWBBMWCAA+FiEEu7bNTJjX
ThNYp1KmAik6b6TlNHMFAmR2YJUCGwMFCQHhM4AFCwkIBwMFFQoJCAsFFgIDAQAC
HgECF4AACgkQAik6b6TlNHP7DwEA1tED4AwfQs3u2u6BWFpZ7jAOmD3hRUmWgc8v
5p0kwr8BAKHf2HJBeTeK9LZwrvEplSArR0Gxu0nYjZcFe8v11LcJtCpBbnRvaW5l
IEJlYXVwcsOpIDxhbmFyY2F0QG9yYW5nZXNlZWRzLm9yZz6IlgQTFggAPhYhBLu2
zUyY104TWKdSpgIpOm+k5TRzBQJkdmCtAhsDBQkB4TOABQsJCAcDBRUKCQgLBRYC
AwEAAh4BAheAAAoJEAIpOm+k5TRzVdsBAL2kAWg6DdxdA2c1FkAGXdHAMglOgPnM
FH1vhW0n2j9YAP9MUqfJhqGPL8+6NHkf7NLMNJhta+qN7raoFfZiE5j3DbQpQW50
b2luZSBCZWF1cHLDqSA8YW5hcmNhdEB0b3Jwcm9qZWN0Lm9yZz6IlQQTFggAPhYh
BLu2zUyY104TWKdSpgIpOm+k5TRzBQJkdmC/AhsDBQkB4TOABQsJCAcDBRUKCQgL
BRYCAwEAAh4BAheAAAoJEAIpOm+k5TRzjtIA90Sp94e0UPH1Q+ESVl/udIzr0rK8
9Zcyhy70py7+zDUBAPlCQ/MrmjGJ6uKazr8pSQgoMd23xRw2nbfhX5R0P9kHuDgE
ZHZQzhIKKwYBBAGXVQEFAQEHQJhmFrYgxjnhYVcwbhL39KZ5/ng+Cif30sn+Enzz
mnRkAwEIB4h3BBgWCAAgFiEEu7bNTJjXThNYp1KmAik6b6TlNHMFAmR2UM4CGwwA
CgkQAik6b6TlNHMGDQD4htyxmEvHoVEioEkJ19gyjuqi8Sj69fQdmxQ9GWrgeQEA
hy0LOYfdbJ84P9pRN44Xe1j60gZTHGNAsaw1Bl4atAi4MwRkdlFSFgkrBgEEAdpH
DwEBB0CqMy8Q2nyA/C+3lfNOLzQ4aN6QJqb3ON52IVwF3h+8B4h4BBgWCAAgFiEE
u7bNTJjXThNYp1KmAik6b6TlNHMFAmR2UVICGyAACgkQAik6b6TlNHOU+wEA0s2A
frhExOr9elwl2j2ZzAVBe36qCsIyQgy1uSBR7ScBAMt+pv/ltcybxNQSCt/PWvMj
UfKUt6k5TBMzm1bGnokN
=T57O
- -----END PGP PUBLIC KEY BLOCK-----
-----BEGIN PGP SIGNATURE-----

iHUEARYIAB0WIQS7ts1MmNdOE1inUqYCKTpvpOU0cwUCZHZi/wAKCRACKTpvpOU0
czFYAQDfZw/4IWYLZ254Y70oU1GIZP6Ro3YiYt+v9RigQL3x/gD/ThZEW9ELcVv4
1OqdFB00XiAWCPoXZBpNCOxXAgysUAyJATMEAQEIAB0WIQR7FkIE0JZyOwGWNas+
od3dsmHZewUCZHZjAwAKCRA+od3dsmHZe/h9B/9I2kFBpXot9a3YS1KxjPslDlOx
29FOvZLSVjy+sfEMApkv//R9BolkMvBC8ceFlT6/dN5dJCpd55gni0bPgIGTH3Q5
1P7F+sqOurUdA/Xx8+p64YhcYPwPNs1VR/CLc5ckJVFvwGdly6VBbt+/TsulHgBd
8buKJejH7/p7KHXjzlV58LDCilAFcekWS2Krvv13l9fbyPp2D9oYBa2fgnLfSzPk
7Kh65kJC3EaJk5xZZl4G4c8/6gLJVCCkRzlFAZTiocX3Rh0jx3ka5IwThG6qnRFS
hCSIwyZbhYCK4LtGdNUOSQyXOaZa/NoJGtp9GPDqZ5ox6BCryte0aGBg3N6a
=qC8y
-----END PGP SIGNATURE-----
