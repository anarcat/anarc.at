-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256,SHA1

Fri May 29 15:05:51 EDT 2009

For various reasons[0], I have recently setup a new OpenPGP key and I
will be transitionning away from my old one.

The old key will continue to be valid for some time, but I prefer all
future correspondence to use the new one. I would also like this new key
to be reintegrated into the web of trust. This message is signed with
both keys to certify this transition.

The old key was:

pub   1024D/4023702F 2005-03-08 [expires: 2010-03-12]
      Key fingerprint = C9E1 F123 0DBE 47D5 7BAB  3C60 5860 73B3 4023 702F

And the new key is:

pub   4096R/7B75921E 2009-05-29 [expires: 2010-05-29]
      Key fingerprint = 8DC9 01CE 6414 6C04 8AD5  0FBB 7921 5252 7B75 921E

To fetch the full key from a public key server, you can simply do:

  gpg --keyserver pool.sks-keyservers.net --recv-key 7B75921E

If you already know my old key, you can now verify that the new key is
signed by the old one:

  gpg --check-sigs 7B75921E

If you don't already know my old key, or you just want to be double
extra paranoid, you can check the fingerprint against the one above:

  gpg --fingerprint 7B75921E

If you are satisfied that you've got the right key, and the UIDs match
what you expect, I'd appreciate it if you would sign my key. You can
do that by issuing the following command:

** 
NOTE: if you have previously signed my key but did a local-only
signature (lsign), you will not want to issue the following, instead
you will want to use --lsign-key, and not send the signatures to the
keyserver
**

  gpg --sign-key 7B75921E

I'd like to receive your signatures on my key. You can either send me
an e-mail with the new signatures (if you have a functional MTA on
your system):

  gpg --export 7B75921E | gpg --encrypt -r 7B75921E --armor | mail -s 'OpenPGP Signatures' anarcat@koumbit.org

Additionally, I highly recommend that you implement a mechanism to keep
your key material up-to-date so that you obtain the latest revocations,
and other updates in a timely manner. On a system with cron, you should
add something similar to the following to your personal crontab:

0 12 * * * /usr/bin/gpg --refresh-keys > /dev/null 2>&1

Please let me know if you have any questions, or problems, and sorry
for the inconvenience.

The Anarcat

[0] https://www.debian-administration.org/users/dkg/weblog/48
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)

iQIcBAEBCAAGBQJKIDSHAAoJEHkhUlJ7dZIeoPwP/A5wj/6sRmdBLUbv06udxvrt
dN1Gv78ZDFAvWImL7METOnLFssGh5OJCFzPS7UsgTAeCpjUEUcq8uoMRcarhL4VF
KiW3jNeYRnmDI8ZvihCiEXVdOxAf48sPija6N5Q2PPBbAs8WT90/62HSq8i/C36A
VWfU1i83uzwcMYS6z4rP5XWTf45ev/UnTCkfxYB+T8WxS3eYm2CWvzmaDWh9w+oT
rojZR6q8Un7zVCdxua5+xfqgF7s+tiRPTNlT8G6/giTiymmzCz5S8y+WlmT7BOxp
iiUCN5SnMW/4M5CcZd82/7jkvHY5an/Fptr+EKBWVK/5HIW8UfaZDUHlv+0lac93
PKBWOHmGA907YCz2toHpjcg6KntnfPT6scTsGuhlcX+OtVL6bAqdXDUNRxB+kSV0
ZpjvzD7dAr4hjieevc9Amk7Hc8gtze6RRQrH1Vu4v+qwh8lHPUuiR8FOiQj7sKyx
4QLz6nIha4tqySZstEM+pr6rV2yvL0kC/5n+8zBGD0Q8eQ4s8ybXr3sQOUDltEfd
9s3UhxeMUMo2l4t9IZAy7rY8qBigF9Eo748drVH+fbIH2C69D+HSdx6WYUScXyz9
Lzn70kfvaBmgoThPKAPxGs2fm781A3q4sNWCpV79aQ7K7vCpoUjVqAa66fQzQGQ/
afy3CgWk6rYzCyQu4F2xiEYEARECAAYFAkogNIcACgkQWGBzs0AjcC/L/QCgj/Pi
2pnAPX5xJqclToJ4bSQ3QoMAnjQnHJN/d2DIHzRhyo+syO3RcJnt
=hXSH
-----END PGP SIGNATURE-----
