--
-- anarcat's xmonad config (.xmonad/xmonad.hs)
--

-- requirements:
-- dmenu (from suckless-tools)
-- taffybar
-- xmonad
-- xmonad-contrib
-- trayer
-- libnotify-bin (optional)
--
-- the following config files should be installed along with this one
-- .config/taffybar/taffybar.hs
-- .xmonad/xmonad-session-rc

-- originally copied from clint's config at
-- https://anonscm.debian.org/cgit/users/clint/dotfiles.git/tree/.xmonad/xmonad.hs

-- remaining issues:
-- * resizing/moving windows with the mouse makes them floating
--  -> this is because mouseResizeWindow (in Operations.hs called from
--     the default xmonad.hs) does a "float" on the window... tried
--     MouseResizableTile, but it leaves a gap and still floats
-- * "return" in emacs shows the haskell-mode-hook help, wtf
-- * laptop suspends when closed and ac-powered
-- * does not remember layouts - probably http://xmonad.org/xmonad-docs/xmonad-contrib/XMonad-Layout-PerWorkspace.html

-- nice to haves
-- * mod-t used to move to top (currently dupe of mod-f)
--  -> not sure we need this in xmonad, things float up properly now
-- * mod-a (?) to focus on last urgent window: https://braincrater.wordpress.com/2009/03/14/pimp-your-xmonad-4-urgency-hooks/
-- * notes: https://braincrater.wordpress.com/2008/11/29/pimp-your-xmonad-3-prompt/
-- * hilight workspaces with only copies of windows: https://wiki.haskell.org/Xmonad/Config_archive/Josh_Rickmar's_xmonad.hs
-- * empty workspace magic: http://xmonad.org/xmonad-docs/xmonad-contrib/XMonad-Actions-FindEmptyWorkspace.html
-- * icons for often started apps (browser, transmission, music, etc)

-- reminders:
-- * https://www.haskell.org/wikiupload/b/b8/Xmbindings.png
-- * use mod-h / mod-l to resize panes, not the mouse, which floats windows
-- * use mod-t to unfloat
-- * screen 1 / 2 is mod-w / mod-e
-- * clementine starts hidden in the tray by default!

-- some haskell resources
-- * cheat sheet: http://cheatsheet.codeslower.com/CheatSheet.pdf
-- * operators cheat sheet: http://www.imada.sdu.dk/~rolf/Edu/DM22/F06/haskell-operatorer.pdf
-- * wikibooks: https://en.wikibooks.org/wiki/Haskell
-- * nice learning curve book: http://learnyouahaskell.com/chapters
-- * o'reily's: http://book.realworldhaskell.org/read/

import XMonad.Prompt.ConfirmPrompt (confirmPrompt)
import XMonad
-- if we want to run within XFCE, we should look at
--  https://wiki.archlinux.org/index.php/Xmonad#Xfce_4_and_xmonad
-- import XMonad.Config.Xfce
-- pretty printing to the status bar, and more
import XMonad.Hooks.DynamicLog
-- avoid tiling docks
import XMonad.Hooks.ManageDocks
-- ignore urgency warnings, taffybar will take care of it
-- source: https://braincrater.wordpress.com/2009/03/14/pimp-your-xmonad-4-urgency-hooks/
import XMonad.Hooks.UrgencyHook
-- window settings presets helper
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, composeOne, (-?>))
import XMonad.Hooks.FadeWindows (isFloating)

-- for the status bar (taffybar)
import XMonad.Hooks.EwmhDesktops        (ewmh)
import System.Taffybar.Hooks.PagerHints (pagerHints)

-- for the confirm hook
import Control.Monad(when)
-- to communicate with dmenu
import XMonad.Util.Dmenu(dmenu)

import XMonad.Util.WindowProperties
import XMonad.Util.XUtils
-- to spawn programs
import XMonad.Util.Run(spawnPipe)
-- to easily configure key shortcuts
import XMonad.Util.EZConfig(additionalKeys)
-- to spawn random applications anywhere
import XMonad.Util.NamedScratchpad (namedScratchpadAction, NamedScratchpad(NS), customFloating, namedScratchpadManageHook)

-- various layouts
import XMonad.Layout.Grid (Grid(..))
import XMonad.Layout.ResizableTile (ResizableTall(..))
import XMonad.Layout.SimplestFloat
-- layout modifiers
-- mod-m to expand the current window
import XMonad.Layout.Maximize (maximize, maximizeRestore)
-- remove borders when there's only one window
import XMonad.Layout.NoBorders (smartBorders)
-- unused?
import XMonad.Layout.Reflect (reflectHoriz)

-- various prompts
import XMonad.Prompt
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Ssh (sshPrompt)
import XMonad.Prompt.Theme (themePrompt)
import XMonad.Prompt.Window (windowPromptBringCopy)
import XMonad.Prompt.XMonad (xmonadPrompt)

-- to make windows "sticky" on all desktops (mod-v/V)
import XMonad.Actions.CopyWindow (wsContainingCopies,copyToAll,killAllOtherCopies,kill1)
-- to toggle between workspaces
import XMonad.Actions.CycleWS

-- handle fullscreen events
import XMonad.Hooks.EwmhDesktops (fullscreenEventHook)
-- manage the stack of windows (e.g. toggle the master)
import qualified XMonad.StackSet as W
import XMonad.Config.Xfce
import XMonad.Hooks.ManageHelpers

-- mostly to exit
import System.IO
import System.Exit

-- for "All"
import Data.Monoid

-- float some windows by default
myManageHook = composeAll
    [ manageDocks
    , manageHook xfceConfig
    , isFullscreen --> doFullFloat
    -- float chromium pop-ups (and maybe others?)
    , propertyToQuery (Role "pop-up") =? True --> doFloat
    , className =? "Evince"    --> doFloat
    , className =? "Xpdf"      --> doFloat
    , className =? "Epdfview"  --> doFloat
    , className =? "Gimp"      --> doFloat
    , className =? "Vncviewer" --> doFloat
    , className =? "gm display" --> doFloat
    , className =? "mpv"        --> doFloat
    , className =? "mplayer"    --> doFloat
    , className =? "SafeEyes"    --> doFloat
    , className =? "safeeyes"    --> doFloat
    , title =? "pop-up"         --> doFloat
    -- do not focus notify output
    , className =? "Xfce4-notifyd" --> doIgnore
    ]

-- solarized color theme
colorBlack           = "#002b36" -- base03
colorDarkGray        = "#073642" -- base02
colorLightGray       = "#586e75" -- base01
colorWhite           = "#839496" -- base0
colorRed             = "#dc322f"
colorBlue            = "#268bd2"
colorGreen           = "#859900"
colorYellow          = "#b58900"

-- customize prompt colors
myXPConfig :: XPConfig
myXPConfig = defaultXPConfig { bgColor     = colorBlack
                             , fgColor     = colorWhite
                             , bgHLight    = colorLightGray
                             , fgHLight    = colorDarkGray
                             , borderColor = colorDarkGray
                             , position    = Top
                             }

myLayoutHook  = avoidStruts layouts
    where
        -- layouts list
        -- * tiled: default
        -- * mirror tiled: wide window on top
        -- * grid: what it says on the box
        -- * full: fullscreen
        -- * simplestFloat: floating
        -- all (but full) are "maximize" so mod-m can expand windows
        layouts = tiled ||| Mirror tiled ||| maximize Grid ||| Full ||| maximize simplestFloat
        -- smartBorders removes borders when there's only one window
        -- https://braincrater.wordpress.com/2008/11/15/pimp-your-xmonad-2-smartborders/
        tiled = maximize $ smartBorders (ResizableTall nmaster delta ratio [])
        -- The default number of windows in the master pane
        nmaster = 1
        -- Percent of screen to increment by when resizing panes
        delta   = 1/100
        -- Default proportion of screen occupied by master pane
        ratio   = 2/3

-- toggle floating status of a window
-- can probably be simplified, but i have yet to figure out how
toggleFloat = withFocused (\windowId -> do
                                isF <- runQuery isFloating windowId
                                if isF
                                then windows (W.sink windowId)
                                else float windowId
                          )

-- scratchpads can be brought in from anywhere
scratchpads :: [NamedScratchpad]
scratchpads =
    [ NS "music" "gmpc"
             (title =? "Music")
             (customFloating $ W.RationalRect (1/6) (1/6) (2/3) (2/3))
    , NS "emacs" "emacs"
             (className =? "Emacs")
             (customFloating $ W.RationalRect (3/4) 0 (1/4) (2/3))
    ]

-- define "windows key" as "mod"
modm = mod4Mask

-- the opposite of kill1: if a window is in multiple workspaces, delete it here, if not, do nothing
-- there has to be a simpler way...
killsoft :: X ()
killsoft = do ss <- gets windowset
              whenJust (W.peek ss) $ \w -> when (W.member w $ delete'' w ss) $ windows $ delete'' w
       where delete'' w = W.modify Nothing (W.filter (/= w))

-- | handle X client messages that tell Xmonad to make a window appear
-- on all workspaces
--
-- this should really be using _NET_WM_STATE and
-- _NET_WM_STATE_STICKY. but that's more complicated: then we'd need
-- to inspect a window and figure out the current state and act
-- accordingly. I am not good enough with Xmonad to figure out that
-- part yet.
--
-- Instead, just check for the relevant message and check if the
-- focused window is already on all workspaces and toggle based on
-- that.
--
-- this is designed to interoperate with Emacs's writeroom-mode module
-- and called be called from elisp with:
--
-- (x-send-client-message nil 0 nil "XMONAD_COPY_ALL_SELF" 8 '(0))
toggleStickyEventHook :: Event -> X All
toggleStickyEventHook (ClientMessageEvent {ev_message_type = mt, ev_data = dt}) = do
  dpy <- asks display
  -- the client message we're expecting
  copyAllMsg <- io $ internAtom dpy "XMONAD_COPY_ALL_SELF" False
  -- if the event matches the message we expect, toggle sticky state
  when (mt == copyAllMsg && dt /= []) $ do
    copyToAllToggle
  -- we processed the event completely
  return $ All True
-- ignore other messages
toggleStickyEventHook _ = return $ All True

-- | Toggle between "copyToAll" or "killAllOtherCopies". Copies to all
-- workspaces, or remove from all other workspaces, depending on
-- previous state (checked with "wsContainingCopies").
copyToAllToggle :: X ()
copyToAllToggle = do
    -- check which workspaces have copies
    copies <- wsContainingCopies
    if null copies
      then windows copyToAll -- no workspaces, make sticky
      else killAllOtherCopies -- already other workspaces, unstick


-- main config declaration
myConfig = defaultConfig {
         modMask = modm
       , normalBorderColor = "#111111"
       , focusedBorderColor = "#333333"
       , manageHook = myManageHook
       , terminal = "x-terminal-emulator"
       , handleEventHook = handleEventHook defaultConfig <+> fullscreenEventHook <+> toggleStickyEventHook
       , layoutHook = myLayoutHook
    } `additionalKeys` [
    ((noModMask         , xK_Pause), spawn "xscreensaver-command -lock")
  , ((noModMask         , xK_Print), spawn "snap")
  -- xK_XF86AudioMute
  , ((noModMask         , 0x1008ff12 ), spawn "pactl set-sink-mute 0 toggle")
  -- xK_XF86AudioLowerVolume
  , ((noModMask         , 0x1008ff11), spawn "pactl -- set-sink-volume 0 -2%")
  -- xK_XF86AudioRaiseVolume
  , ((noModMask         , 0x1008ff13), spawn "pactl -- set-sink-volume 0 +2%")
  -- other controls are directly in audio clients (e.g. gmpc)
  , ((modm              , xK_Return), spawn $ XMonad.terminal defaultConfig )
  , ((modm              , xK_F12   ), xmonadPrompt      myXPConfig     )
  , ((modm              , xK_F2    ), spawn "rofi -show ssh" )
  , ((modm              , xK_F3    ), spawn "rofi -show run" )
  , ((modm              , xK_r     ), spawn "rofi -show run" )
  , ((modm              , xK_F5    ), themePrompt       myXPConfig     )
  , ((modm              , xK_F6    ), windowPromptBringCopy myXPConfig )
  -- same, on mod-g for "grep"
  , ((modm              , xK_g     ), windowPromptBringCopy myXPConfig )
  , ((modm              , xK_Left  ), prevWS                                )
  , ((modm              , xK_Right ), nextWS                                )
  , ((modm              , xK_Up    ), prevScreen                            )
  , ((modm              , xK_Down  ), nextScreen                            )
  , ((modm              , xK_Escape), toggleWS                              )
  , ((modm              , xK_f     ), toggleFloat                           )
  , ((modm              , xK_m     ), withFocused $ sendMessage . maximizeRestore )
  -- Make focused window always visible
  , ((modm              , xK_v     ), copyToAllToggle                       )
  -- used to banish a window from the current workspace, if it's also elsewhere
  , ((modm              , xK_c     ), killsoft                                 )
  -- kill even if it's on multiple workspaces
  , ((modm .|. shiftMask, xK_c     ), kill                                  )
  , ((modm              , xK_Return), spawn $ XMonad.terminal myConfig      )
  , ((modm .|. controlMask, xK_h     ), spawn "xscreensaver-command -lock; sudo systemctl suspend" )
  , ((modm .|. shiftMask, xK_h     ),
          confirmPrompt myXPConfig "hibernate?" $ spawn "xscreensaver-command -lock ; sudo systemctl hibernate")
  , ((modm .|. shiftMask .|. controlMask, xK_h     ),
          confirmPrompt myXPConfig "halt?" $ spawn "notify-send 'shutting down...'; sudo systemctl poweroff" )
  , ((modm .|. shiftMask, xK_Return), windows W.swapMaster                  )
  , ((modm              , xK_q     ), spawn "notify-send 'reloading xmonad.hs'; xmonad --recompile && xmonad --restart; notify-send 'reloaded xmonad.hs'")
  , ((modm .|. shiftMask, xK_q     ), confirmPrompt myXPConfig "exit" $ io exitSuccess)
  , ((modm .|. controlMask, xK_m   ), namedScratchpadAction scratchpads "music")
  , ((modm .|. controlMask, xK_e   ), namedScratchpadAction scratchpads "emacs")
      ]

main = xmonad $ withUrgencyHook NoUrgencyHook . ewmh . pagerHints $ myConfig
