When I don't want to feel like an exotic little snowflake that spends
hours needlessly configuring his desktop environment, I go back to the
default, which is usually GNOME. I work with the following
configuration:

 * 4 basic desktops, with Super-[1234] ("windows key") mapped to the 4
   desktops (I used to have a 2x2 layout, but couldn't figure out how
   to do this in GNOME 3)
 * install [impatience](https://extensions.gnome.org/extension/277/impatience/) to disable animations
 * other gnome tweaks:
   * enable the Compose key (super-right)
   * alternatetab (to *not* group alt-tab by app, make sure to
     configure the plugin to show all workspaces)
   * applications menu
   * places status indicator
   * workrave display
   * focus follows mouse (slow, but works)
 * consider [material shell](https://material-shell.com/)

I still have to figure out how to enable the nice shortcuts I have in
my normal setup. In particular, I should bind the same keybindings to
`rofi`. I found the [keyboard navigation manual](https://help.gnome.org/users/gnome-help/stable/keyboard-nav.html.en) very useful to get
familiar with the platform.

Finally, a big blocker is how to configure GPG agent so that it
properly talks with my Yubikey, otherwise I can't talk to any SSH
server. So far I figured out how to disable GNOME Keyring by doing
this:

    ( cat /etc/xdg/autostart/gnome-keyring-ssh.desktop ; echo Hidden=true ) > ~/.config/autostart/gnome-keyring-ssh.desktop

It disables the gnome-keyring startup routine. Then GPG-agent starts
supervised, but doesn't show up in the environment, so we
fail. Apparently, the following shell snippet is supposed to fix that
problem:

    # Needed for GPG2 to bridge with ssh-agent
    #export SSH_AUTH_SOCK=/run/user/1000/gnupg/S.gpg-agent.ssh
    #unset SSH_AGENT_PID
    if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
        export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
    fi

    export GPG_TTY="$(tty)"

    # So annoying to need to do this every time
    gpg-connect-agent updatestartuptty /bye > /dev/null

This is all so weird and broken and confusing that I found about ten
different guides trying to tell people how to do this kind of stuff:

 * [John Hopkins university's McQueen Lab: Yubikey for SSH Authentication](https://occamy.chemistry.jhu.edu/references/pubsoft/YubikeySSH/index.php)
 * [karlgrz: 2 Factor Authentication GPG & SSH keys with pass and Yubikey NEO](https://karlgrz.com/2fa-gpg-ssh-keys-with-pass-and-yubikey-neo/)
 * [Chris's Digital Realm: My Perfect GnuPG / SSH Agent Setup](https://www.bootc.net/archives/2013/06/09/my-perfect-gnupg-ssh-agent-setup/)
 * [Simon Josefsson's blog: OpenPGP Smartcards and GNOME](https://blog.josefsson.org/2015/01/02/openpgp-smartcards-and-gnome/)
 * [Arch wiki: GNOME Keyring](https://wiki.archlinux.org/index.php/GNOME/Keyring#SSH_keys)
 * [lg's blog: YubiKey gpg/ssh: Great security but tricky install](https://lorgor.blogspot.com/2017/01/yubikey-gpgssh-great-security-but.html)

Also: maybe try [PaperWM](https://github.com/paperwm/PaperWM) to get
tiling into GNOME.
