[[!meta title="Wayland: i3 to Sway migration"]]
[[!meta date="2022-11-16T17:04:10-0500"]]

I started migrating my graphical workstations to Wayland, specifically
migrating from i3 to Sway. This is mostly to address serious graphics
bugs in the latest [[Framwork
laptop|hardware/laptop/framework-12th-gen/]], but also something I
felt was inevitable.

The current status is that I've been able to convert my `i3`
configuration to Sway, and adapt my `systemd` startup sequence to the
new environment. Screen sharing only works with Pipewire, so I also
did that migration, which basically requires an upgrade to Debian
bookworm to get a nice enough Pipewire release.

I'm testing Wayland on my laptop and I'm using it as a daily driver.

Most irritants have been solved one way or the other. My main problem
with Wayland right now is that I spent a frigging week doing the
conversion: it's exciting and new, but it basically sucked the life
out of all my other projects and it's distracting, and I want it to
stop.

The rest of this page documents why I made the switch, how it
happened, and what's left to do. Hopefully it will keep you from
spending as much time as I did in fixing this.

TL;DR: Wayland is [mostly ready][arewewaylandyet.com]. Main blockers you might find are
that you need to do manual configurations, [DisplayLink][] (multiple
monitors on a single cable) doesn't work in Sway, HDR and color
management are still in development.

I had to install the following packages:

    apt install \
        brightnessctl \
        foot \
        gammastep \
        gdm3 \
        grim slurp \
        nwg-displays \
        pipewire-pulse \
        sway \
        swayidle \
        swaylock \
        wev \
        wireplumber \
        wlr-randr \
        xdg-desktop-portal-wlr

And did some of tweaks in my `$HOME`, mostly dealing with my esoteric
systemd startup sequence, which you won't have to deal with if you are
not a fan.

Note that this page is bound to be out of date as I make minute
changes to my environment. Typically, changes will be visible in my
[Puppet repository](https://gitlab.com/anarcat/puppet), somewhere like the [desktop.pp file](https://gitlab.com/anarcat/puppet/-/blob/main/site-modules/profile/manifests/desktop.pp), but I
do not make any promise that the content below is up to date.

[[!toc levels=5]]

[DisplayLink]: https://en.wikipedia.org/wiki/DisplayLink

# Why switch?

I originally held back from migrating to Wayland: it seemed like a
complicated endeavor hardly worth the cost. It also didn't seem
actually ready.

But after reading [this blurb on LWN][], I decided to at least
document the situation here. The actual quote that convinced me it
might be worth it was:

> It’s amazing. I have never experienced gaming on Linux that looked
> this smooth in my life.

... I'm not a gamer, but I *do* [[care about
latency|blog/2018-05-04-terminal-emulators-2]]. The [longer version is
worth a read as well][artemis].

The point here is not to bash one side or the other, or even do a
thorough comparison. I start with the premise that Xorg is likely
going away in the future and that I will need to adapt some day. In
fact, the [last major Xorg release][] (21.1, October 2021) is [rumored
to be the last][] ("just like the previous release...", that said,
minor releases are still coming out, e.g. [21.1.4][]). Indeed, it
seems even core Xorg people have moved on to developing Wayland, or at
least Xwayland, which was spun off it its own source tree.

X, or at least Xorg, is in maintenance mode and has been for
years. Granted, the [X Window System][] is getting close to forty
years old at this point: it got us amazingly far for something that
was designed around the time the [first][] [graphical
interface][]. Since Mac and (especially?) Windows released theirs,
they have rebuilt their graphical backends numerous times, but UNIX
derivatives have stuck on Xorg this entire time, which is a testament
to the design and reliability of X. (Or our incapacity at developing
meaningful architectural change across the entire ecosystem, take your
pick I guess.)

What pushed me over the edge is that I had some pretty bad driver
crashes with Xorg while screen sharing under Firefox, in Debian
bookworm (around November 2022). The symptom would be that the UI
would completely crash, reverting to a text-only console, while
Firefox would *keep running*, audio and everything still
working. People could still see my screen, but I couldn't, of course,
let alone interact with it. All processes still running, including
Xorg.

(And no, sorry, I haven't reported that bug, maybe I should have, and
it's actually possible it comes up again in Wayland, of course. But at
first, screen sharing didn't work of course, so it's coming a much
further way. After making screen sharing work, though, the bug didn't
occur again, so I consider this a Xorg-specific problem until further
notice.)

There were also frustrating glitches in the UI, in general. I actually
had to setup a compositor alongside i3 to make things bearable at
all. Video playback in a window was lagging, sluggish, and out of sync.

Wayland fixed all of this.

[artemis]: https://artemis.sh/2022/09/18/wayland-from-an-x-apologist.html
[this blurb on LWN]: https://lwn.net/Articles/908561/
[last major Xorg release]: https://lists.x.org/archives/xorg/2021-October/060799.html
[rumored to be the last]: https://who-t.blogspot.com/2021/09/an-xorg-release-without-xwayland.html
[X Window System]: https://en.wikipedia.org/wiki/X_Window_System
[first]: https://en.wikipedia.org/wiki/Xerox_Alto
[graphical interface]: https://en.wikipedia.org/wiki/Macintosh_128
[21.1.4]: https://lists.x.org/archives/xorg-announce/2022-July/003193.html

# Wayland equivalents

This section documents each tool I have picked as an alternative to
the current Xorg tool I am using for the task at hand. It also touches
on other alternatives and how the tool was configured.

Note that this list is based on the series of tools I use in
[[software/desktop]]. My old setup is kept in [[software/desktop/x11]]
for historical purposes (and people hanging on to X11).

## Window manager: i3 → sway

This seems like kind of a no-brainer. [Sway][] is around, it's
feature-complete, and it's in Debian.

I'm a bit worried about the "Drew DeVault community", to be
honest. There's a certain aggressiveness in the community I don't like
so much; at least an open hostility towards more modern UNIX tools
like containers and systemd that make it hard to do my work while
interacting with that community.

I'm also concern about the lack of unit tests and user manual for
Sway. The [i3 window manager][] has been designed by a fellow
(ex-)Debian developer I have a lot of respect for ([Michael
Stapelberg][]), partly because of i3 itself, but also working with
him on [other projects][]. Beyond the characters, i3 has a [user
guide][], a [code of conduct][], and [lots more
documentation][]. It has a [test suite][].

Sway has... manual pages, with the homepage just telling users to use
`man -k sway` to find what they need. I don't think we need that kind
of elitism in our communities, to put this bluntly.

But let's put that aside: Sway is still a no-brainer. It's the easiest
thing to migrate to, because it's *mostly* compatible with i3. I had
to immediately fix those resources to get a minimal session going:

| i3                   | Sway                     | note                                  |
|----------------------|--------------------------|---------------------------------------|
| `set_from_resources` | `set`                    | no support for X resources, naturally |
| `new_window pixel 1` | `default_border pixel 1` | actually supported in i3 as well      |

That's it. *All* of the other changes I had to do (and there were
actually a lot) were *all* **Wayland-specific** changes, not
*Sway-specific* changes. For example, use `brightnessctl` instead of
`xbacklight` to change the backlight levels.

See a copy of my full [[config/sway/config]] for details.

Other options include:

 * [dwl][]: tiling, minimalist, dwm for Wayland, not in Debian
 * [hikari][]: tiling/stacking, not in Debian
 * [Hyprland][]: tiling, fancy animations, not in Debian ([1040971][])
 * [Qtile][]: tiling, extensible, in Python, not in Debian ([1015267][])
 * [river][]: Zig, stackable, tagging, not in Debian  ([1006593][])
 * [smithay][], and many derivatives: Rust, not in Debian
 * [velox][]: inspired by xmonad and dwm, not in Debian
 * [vivarium][]: inspired by xmonad, not in Debian
 * [wlmaker][]: inspired by Window Maker, not in Debian

[Sway]: http://swaywm.org/
[i3 window manager]: https://i3wm.org/
[Michael Stapelberg]: https://michael.stapelberg.ch/
[other projects]: https://manpages.debian.org
[user guide]: https://i3wm.org/docs/userguide.html
[code of conduct]: https://i3wm.org/conduct.html
[lots more documentation]: https://i3wm.org/docs/
[test suite]: https://github.com/i3/i3/tree/next/testcases
[Hyprland]: https://hyprland.org/
[dwl]: https://github.com/djpohly/dwl
[Qtile]: https://www.qtile.org/
[1015267]: https://bugs.debian.org/1015267
[river]: https://github.com/riverwm/river
[1006593]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1006593
[velox]: https://github.com/michaelforney/velox
[vivarium]: https://github.com/inclement/vivarium
[hikari]: https://hikari.acmelabs.space/
[1040971]: https://bugs.debian.org/1040971
[wlmaker]: https://github.com/phkaeser/wlmaker
[smithay]: https://github.com/Smithay/smithay

## Status bar: py3status → waybar

I have invested quite a bit of effort in setting up my status bar with
[py3status][]. It supports Sway directly, and did not actually require
any change when migrating to Wayland. 

Unfortunately, I had trouble making `nm-applet` work. Based on [this
nm-applet.service][], I found that you need to pass `--indicator` for
it to show up at all.

In theory, [tray icon support was merged in 1.5][], but in practice
there are still [several limitations][], like [icons not
clickable][]. Also, on startup, `nm-applet --indicator` triggers this
error in the Sway logs:
   
    nov 11 22:34:12 angela sway[298938]: 00:49:42.325 [INFO] [swaybar/tray/host.c:24] Registering Status Notifier Item ':1.47/org/ayatana/NotificationItem/nm_applet'
    nov 11 22:34:12 angela sway[298938]: 00:49:42.327 [ERROR] [swaybar/tray/item.c:127] :1.47/org/ayatana/NotificationItem/nm_applet IconPixmap: No such property “IconPixmap”
    nov 11 22:34:12 angela sway[298938]: 00:49:42.327 [ERROR] [swaybar/tray/item.c:127] :1.47/org/ayatana/NotificationItem/nm_applet AttentionIconPixmap: No such property “AttentionIconPixmap”
    nov 11 22:34:12 angela sway[298938]: 00:49:42.327 [ERROR] [swaybar/tray/item.c:127] :1.47/org/ayatana/NotificationItem/nm_applet ItemIsMenu: No such property “ItemIsMenu”
    nov 11 22:36:10 angela sway[313419]: info: fcft.c:838: /usr/share/fonts/truetype/dejavu/DejaVuSans.ttf: size=24.00pt/32px, dpi=96.00

... but that seems innocuous. The tray icon displays but is not
clickable.

Note that there is currently (November 2022) a [pull request][] to
hook up a "Tray D-Bus Menu" which, [according to Reddit][] might fix
this, or at least be somewhat relevant.

If you don't see the icon, check the `bar.tray_output` property in the
Sway config, try: `tray_output *`.

The non-working tray was the biggest irritant in my migration. I have
used `nmtui` to connect to new Wifi hotspots or change connection
settings, but that doesn't support actions like "turn off WiFi".
   
I eventually fixed this by switching from [py3status][] to
[waybar][], which was another yak horde shaving session, but
ultimately, it worked.

sfwbar is an alternative. It behaves well and has a more GUI-like
feel, at the cost of taking up more real estate. After two minutes the
CPU usage was 1.49s vs 1.39s for waybar, in a preliminary test, but
that's without any resource tracking, so it *seems* waybar is
generally more efficient. plus sfwbar doesn't seem to [support urgency
hints](https://github.com/LBCrion/sfwbar/issues/305).

[py3status]: https://py3status.readthedocs.io/
[waybar]: https://github.com/Alexays/Waybar/

Other alternatives include:

 * [hybridbar](https://github.com/hcsubser/hybridbar)
 * [HybridBar](https://github.com/vars1ty/HybridBar) (yes, another)
 * [rootbar](https://hg.sr.ht/~scoopta/rootbar)
 * [sandbar](https://github.com/kolunmi/sandbar)
 * [sfwbar](https://github.com/LBCrion/sfwbar) (now in Debian)
 * [yambar](https://codeberg.org/dnkl/yambar)

## Web browser: Firefox

Firefox has had support for Wayland for a while now, with the team
[enabling it by default in nightlies around January 2022][]. It's
actually not easy to figure out the state of the port, the [meta bug
report][] is still open and it's *huge*: it currently (Sept 2022)
depends on 76 open bugs, it was opened *twelve* (2010) years ago, and
it's still getting daily updates (mostly linking to other tickets).

[Firefox 106][] presumably shipped with "Better screen sharing for
Windows and Linux Wayland users", but I couldn't quite figure out what
those were.

TL;DR: `echo MOZ_ENABLE_WAYLAND=1 >> ~/.config/environment.d/firefox.conf && apt install xdg-desktop-portal-wlr`

[enabling it by default in nightlies around January 2022]: https://www.phoronix.com/news/Firefox-Nightly-Wayland-Rolling
[meta bug report]: https://bugzilla.mozilla.org/show_bug.cgi?id=635134
[Firefox 106]: https://www.mozilla.org/en-US/firefox/106.0/releasenotes/

### How to enable it

Firefox depends on this silly variable to start correctly under
Wayland (otherwise it starts inside Xwayland and looks fuzzy and fails
to screen share):

    MOZ_ENABLE_WAYLAND=1 firefox

To make the change permanent, many recipes recommend adding this to an
environment startup script:

    if [ "$XDG_SESSION_TYPE" == "wayland" ]; then
        export MOZ_ENABLE_WAYLAND=1
    fi

At least that's the theory. In practice, Sway doesn't actually run any
startup shell script, so that can't possibly work. Furthermore,
`XDG_SESSION_TYPE` is not actually set when starting Sway from `gdm3`
which I find really confusing, and I'm not the [only][] [one][]. So
the above trick doesn't actually work, even if the environment
(`XDG_SESSION_TYPE`) *is* set correctly, because we don't have
conditionals in [environment.d(5)][].

(Note that [systemd.environment-generator(7)][] *does* support running
arbitrary commands to generate environment, but for some reason does
not support user-specific configuration files: it only looks at system
directories... Even then it *may* be a solution to have a conditional
`MOZ_ENABLE_WAYLAND` environment, but I'm not sure it would work
because ordering between those two isn't clear: maybe the
`XDG_SESSION_TYPE` wouldn't be set just yet...)

At first, I made [this ridiculous script][] to workaround those
issues. Really, it seems to me Firefox should just parse the
`XDG_SESSION_TYPE` variable here... but then I realized that Firefox
works fine in Xorg when the `MOZ_ENABLE_WAYLAND` is set.

So now I just set that variable in `environment.d` and It Just Works™:

    MOZ_ENABLE_WAYLAND=1

[only]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1021545
[one]: https://github.com/swaywm/sway/pull/4876
[this ridiculous script]: https://gitlab.com/anarcat/scripts/-/blob/3184996fa3e8050aed07451c16b72d69bcb2cd1b/firefox

### Screen sharing

Out of the box, screen sharing doesn't work until you install
[xdg-desktop-portal-wlr][] or similar
(e.g. [xdg-desktop-portal-gnome][] on GNOME). I had to reboot for the
change to take effect.

Without those tools, it shows the usual permission prompt with "Use
operating system settings" as the only choice, but when we accept...
nothing happens. After installing the portals, it actually works, and
works well!

This was tested in Debian bookworm/testing with Firefox ESR 102 and
Firefox 106.

Major caveat: we can only share a full screen, we [can't currently
share just a window][]. The major upside to that is that, by default,
it streams *only* [one output][] which is actually what I want most
of the time! See the [screencast compatibility][] for more
information on what is supposed to work.

This is actually a *huge* improvement over the situation in Xorg,
where Firefox [can only share a window or all monitors][], which led
me to use Chromium a lot for video-conferencing. With this change, in
other words, I will not need Chromium for anything anymore, whoohoo!

If [[!debpkg slurp]], [[!debpkg wofi]], or [[!debpkg bemenu]] are
installed, one of them will be used to pick the monitor to share,
which effectively acts as some minimal security measure. See
[xdg-desktop-portal-wlr(1)][] for how to configure that.

Note that [xdg-desktop-portal-luminous](https://github.com/waycrate/xdg-desktop-portal-luminous) is a rust-based alternative
implementation th at tries to avoid using slurp or other binaries to
select the screen.

[environment.d(5)]: https://manpages.debian.org/environment.d
[systemd.environment-generator(7)]: https://manpages.debian.org/systemd.environment-generator
[xdg-desktop-portal-wlr]: https://github.com/emersion/xdg-desktop-portal-wlr/
[xdg-desktop-portal-gnome]: https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome
[can't currently share just a window]: https://github.com/emersion/xdg-desktop-portal-wlr/wiki/FAQ#will-this-let-me-share-individual-windows
[one output]: https://github.com/emersion/xdg-desktop-portal-wlr/wiki/FAQ#will-this-let-me-share-all-of-my-outputsdisplays-at-once
[screencast compatibility]: https://github.com/emersion/xdg-desktop-portal-wlr/wiki/Screencast-Compatibility
[can only share a window or all monitors]: https://bugzilla.mozilla.org/show_bug.cgi?id=1412333
[xdg-desktop-portal-wlr(1)]: https://manpages.debian.org/xdg-desktop-portal-wlr

### Side note: Chrome fails to share a full screen

I was still using Google Chrome (or, more accurately, Debian's
Chromium package) for some videoconferencing. It's mainly because
Chromium was the only browser which will allow me to share only one of
my two monitors, which is extremely useful.

To start chrome with the Wayland backend, you need to use:

    chromium  -enable-features=UseOzonePlatform -ozone-platform=wayland

If it shows an ugly gray border, check the `Use system title bar and
borders` setting.

It can do *some* screen sharing. Sharing a window and a tab seems to
work, but sharing a full screen doesn't: it's all black. Maybe not
ready for prime time. 

And since Firefox can do what I need under Wayland now, I will not
need to fight with Chromium to work under Wayland:

    apt purge chromium

Note that a similar fix was necessary for Signal Desktop, see [this
commit](https://gitlab.com/anarcat/puppet/-/commit/c13b9c399882fd76817e408baf8312be3f94dce7). Basically you need to figure out a way to pass those same
flags to signal:

    --enable-features=WaylandWindowDecorations --ozone-platform-hint=auto

## Email: notmuch

I'm using [notmuch](https://notmuchmail.org/) to manage my email firehose, and currently use
the Emacs frontend ([notmuch-emacs](https://notmuchmail.org/notmuch-emacs/)).

I've been a little uncomfortable exposing Emacs to the arbitrary input
from the network that Email essentially is. This has shown to be
particularly problematic with the handling of an org-mode
vulnerability providing remote code execution ([CVE-2024-39331](https://security-tracker.debian.org/tracker/CVE-2024-39331)),
with a [botched disclosure](https://lwn.net/ml/all/87wmmguk44.fsf@localhost/).

So (starting in June 2024) I've been testing [neomutt](neomutt.org/) since it has
[notmuch patches](https://neomutt.org/feature/notmuch) that allow somewhat clunky access to my notmuch
database. So far, it's a learning curve, but it works.

See also Emacs, below.

## File manager: thunar

Unchanged.

## News: feed2exec, gnus

See Email, above, or Emacs in Editor, below.

## Editor: Emacs

Emacs was ported to Wayland in version 29. According to [this LWN
article][], the first (partial, to Cairo) port was done in 2014 and a
working port (to GTK3) was completed in 2021, but wasn't merged until
[late 2021][]. That is: after [Emacs 28 was released][] (April 2022)
and [Debian bookworm freeze][]. The Emacs 29 bookworm backport works.

To get the native builds, you need to install the [emacs-pgtk
package](https://packages.debian.org/unstable/emacs-pgtk).

In any case, like many X11 applications, Emacs mostly works fine under
Xwayland. The clipboard works as expected, for example. Scaling is a
bit of an issue: fonts look fuzzy.

I have heard anecdotal evidence of hard lockups with Emacs running
under Xwayland as well, but haven't experienced any problem so far. I
did experience a Wayland crash with the snapshot version however.

[this LWN article]: https://lwn.net/Articles/843896/
[late 2021]: https://batsov.com/articles/2021/12/19/building-emacs-from-source-with-pgtk/
[Emacs 28 was released]: https://www.gnu.org/savannah-checkouts/gnu/emacs/emacs.html#Releases
[Debian bookworm freeze]: https://lists.debian.org/debian-devel/2022/03/msg00251.html

## Backups: borg

Mostly irrelevant, as I do not use a GUI.

## Color theme: srcery, redshift → gammastep

I am keeping [Srcery][] as a color theme, in general.

Redshift is another story: it has [no support for Wayland][] out of
the box, but it's apparently possible to apply a hack on the TTY
before starting Wayland, with:

    redshift -m drm -PO 3000

This tip is from the [arch wiki][] which also has other suggestions
for Wayland-based alternatives. Both KDE and GNOME have their own "red
shifters", and for wlroots-based compositors, they (currently,
Sept. 2022) list the following alternatives:

 * [gammastep][]: in Debian, panel indicator, seems good
 * [clight][]: uses the webcam to probe for light
 * [wl-gammaray][]: fork of gammastep, runs as a DBus service
 * [wlsunset][]: [WNPP][]

I configured `gammastep` with a simple [[config/systemd/user/gammastep.service]] file
associated with the [[config/systemd/user/sway-session.target]].

[gammastep]: https://gitlab.com/chinstrap/gammastep
[Srcery]: https://srcery-colors.github.io/
[no support for Wayland]: https://github.com/jonls/redshift/issues/55
[arch wiki]: https://wiki.archlinux.org/title/Backlight#Wayland
[clight]: https://github.com/FedeDP/Clight
[wl-gammaray]: https://github.com/jeremija/wl-gammarelay
[wlsunset]: https://git.sr.ht/~kennylevinsen/wlsunset
[WNPP]: http://bugs.debian.org/992729

## Display manager: lightdm → gdm3

Switched because lightdm failed to start sway:

    nov 16 16:41:43 angela sway[843121]: 00:00:00.002 [ERROR] [wlr] [libseat] [common/terminal.c:162] Could not open target tty: Permission denied

Possible alternatives:

 * [lightdm elephant greeter][] (I tried [[!debpkg slick-greeter]] and
   [[!debpkg ukui-greeter]], neither could start the Sway session)
 * [greetd][] is a login manager that delegates to greeters, like
   [gtkgreet][], [tuigreet][] (in Debian) and [QtGreet][] (not in
   Debian), tested [agreety][] (part of greetd) but it didn't work at
   all
 * [sddm][]: KDE's default, in Debian, probably heavier or as heavy as
   gdm3

 [gtkgreet]: https://git.sr.ht/~kennylevinsen/gtkgreet

[lightdm elephant greeter]: https://github.com/max-moser/lightdm-elephant-greeter
[greetd]: https://sr.ht/~kennylevinsen/greetd/
[QtGreet]: https://gitlab.com/marcusbritanicus/QtGreet
[tuigreet]: https://github.com/apognu/tuigreet
[agreety]: https://manpages.debian.org/agreety
[sddm]: https://github.com/sddm/sddm

## Terminal: xterm → foot

One of the biggest question mark in this transition was what to do
about Xterm. After writing [two][] [articles][] about terminal
emulators as a professional journalist, decades of working on the
terminal, and probably using dozens of different terminal emulators,
I'm still not happy with *any* of them.

This is such a big topic that I actually have an [[entire blog post
specifically about this|blog/2022-09-19-wayland-terminal-emulators]].

For starters, using xterm under Xwayland works well enough, although
the font scaling makes things look a bit too fuzzy.

I have also tried foot: it ... just works!

Fonts are much crisper than Xterm and Emacs. URLs are not clickable
but the URL selector (<kbd>control-shift-u</kbd>) is just plain
awesome (think "[vimperator][]" for the terminal). 

There's [cool hack to jump between prompts](https://codeberg.org/dnkl/foot/wiki#user-content-jumping-between-prompts).

Copy-paste works. True colors work. The word-wrapping is excellent: it
doesn't lose one byte. Emojis are nicely sized and colored. Font
resize works. There's even scroll back search
(<kbd>control-shift-r</kbd>).

Foot went from a question mark to being a reason to switch to Wayland,
just for this little goodie, which says a lot about the quality of
that software.

The selection clicks are a not quite what I would expect though. In
rxvt and others, you have the following patterns:

 * single click: reset selection, or drag to select
 * double: select word
 * triple: select quotes or line
 * quadruple: select line

I particularly find the "select quotes" bit useful. It seems like foot
just supports double and triple clicks, with word and line
selected. You can select a rectangle with <kbd>control</kbd>,. It
correctly extends the selection word-wise with right click if
double-click was first used.

One major problem with Foot is that it's a new terminal, with its own
[termcap][] entry. Support for foot was added to ncurses in the
[20210731][] release, which was shipped *after* the current Debian
stable release (Debian bullseye, which ships 6.2+20201114-2). A
workaround for this problem is to install the `foot-terminfo` package
on the remote host, which *is* available in Debian stable. 

This should eventually resolve itself, as Debian bookworm has a newer
version. Note that some corrections were also shipped in the
[20211113][] release, but that is also shipped in Debian bookworm.

That said, I am almost certain I will have to revert back to xterm
under Xwayland at some point in the future. Back when I was using
GNOME Terminal, it would mostly work for everything until I had to use
the serial console on a (HP ProCurve) network switch, which have a
fancy [TUI][] that was basically unusable there. I fully expect such
problems with foot, or any other terminal than xterm, for that matter.

The [foot wiki][] has good troubleshooting instructions as well.

Update: I did find one tiny thing to improve with foot, and it's the
default logging level which I found pretty verbose. After discussing
it with the maintainer on IRC, I submitted [this patch](https://codeberg.org/dnkl/foot/pulls/1215) to tweak
it, which I described like this [on Mastodon](https://kolektiva.social/@Anarcat/109365724789181240):

> today's reason why i will go to hell when i die (TRWIWGTHWID?): a
> 600-word, 63 lines commit log for a one line change:
> https://codeberg.org/dnkl/foot/pulls/1215

It's Friday.

[two]: https://anarc.at/blog/2018-04-12-terminal-emulators-1/
[articles]: https://anarc.at/blog/2018-05-04-terminal-emulators-2/
[vimperator]: https://en.wikipedia.org/wiki/Vimperator
[termcap]: https://en.wikipedia.org/wiki/Termcap
[20210731]: https://invisible-island.net/ncurses/NEWS.html#index-t20210731
[20211113]: https://invisible-island.net/ncurses/NEWS.html#index-t20211113
[TUI]: https://en.wikipedia.org/wiki/Text-based_user_interface
[foot wiki]: https://codeberg.org/dnkl/foot/wiki

## Launcher: rofi → fuzzel

rofi does [not support Wayland][]. There was a [rather disgraceful
battle in the pull request][] that led to the creation of a fork
([lbonn/rofi][]), so it's unclear how that will turn out. 

Given how relatively trivial problem space is, there is of course a
profusion of options:

| Tool                    | In Debian       | Notes                                                                    |
|-------------------------|-----------------|--------------------------------------------------------------------------|
| [alfred][]              | yes             | general launcher/assistant tool                                          |
| [anyrun][]              | [ITP 1057118][] | Rust, launchercalculator, plugins, dmenu                                 |
| [bemenu][]              | yes, bookworm+  | inspired by dmenu                                                        |
| [cerebro][]             | no              | Javascript ... uh... thing                                               |
| [dmenu-wl][]            | no              | fork of [dmenu][], straight port to Wayland                              |
| [Fuzzel][]              | yes, bookworm+  | dmenu/drun replacement, app icon overlay                                 |
| [gmenu][]               | no              | drun replacement, with app icons                                         |
| [kickoff][]             | no              | dmenu/run replacement, fuzzy search, "snappy", history, copy-paste, Rust |
| [krunner][]             | yes             | KDE's runner                                                             |
| [mauncher][]            | no              | dmenu/drun replacement, math                                             |
| [nwg-launchers][]       | no              | dmenu/drun replacement, JSON config, app icons, [nwg-shell project][]    |
| [Onagre][]              | no              | rofi/alfred inspired, multiple plugins, Rust                             |
| [πmenu][]               | no              | dmenu/drun rewrite                                                       |
| [Rofi (lbonn's fork)][] | no              | see above                                                                |
| [sirula][]              | no              | `.desktop` based app launcher                                            |
| [Ulauncher][]           | [ITP 949358][]  | generic launcher like Onagre/rofi/alfred, might be overkill              |
| [tofi][]                | yes, bookworm+  | dmenu/drun replacement, C                                                |
| [wlr-which-key][]       | no              | key-driven, limited but simple launcher, inspired by which-key.nvim      |
| [wmenu][]               | no              | fork of dmenu-wl, but mostly a rewrite                                   |
| [Wofi][]                | yes             | dmenu/drun replacement, not actively maintained                          |
| [yofi][]                | no              | dmenu/drun replacement, Rust                                             |

The above list comes partly from <https://arewewaylandyet.com/> and
[awesome-wayland][]. It is likely incomplete.

I have [read some good things][artemis] about bemenu, fuzzel, and wofi.

A particularly tricky option is that my rofi password management
depends on xdotool for some operations. At first, I thought this was
just going to be (thankfully?) impossible, because we actually *like*
the idea that one app cannot send keystrokes to another. But it seems
there *are* actually alternatives to this, like [wtype][] or
[ydotool][], the latter which requires root access. [wl-ime-type][]
does that through the `input-method-unstable-v2` protocol ([sample
emoji picker][], but is not packaged in Debian.

As it turns out, [wtype][] just works as expected, and fixing this was
basically a [two-line patch][]. Another alternative, not in Debian, is
[wofi-pass][].

The other problem is that I actually heavily modified rofi. I use
"modis" which are not actually implemented in wofi *or* tofi, so I'm
left with reinventing those wheels from scratch or using the rofi +
wayland fork... It's really too bad that fork isn't being
reintegrated...

Note that [wlogout][] could be a partial replacement (just for the
"power menu").

[alfred]: https://github.com/albertlauncher/albert
[anyrun]: https://github.com/Kirottu/anyrun
[bemenu]: https://github.com/Cloudef/bemenu
[cerebro]: https://cerebroapp.com/
[dmenu-wl]: https://github.com/nyyManni/dmenu-wayland
[dmenu]: https://wiki.archlinux.org/title/dmenu
[Fuzzel]: https://codeberg.org/dnkl/fuzzel
[gmenu]: https://code.rocketnine.space/tslocum/gmenu
[ITP 1057118]: https://bugs.debian.org/1057118
[ITP 949358]: http://bugs.debian.org/949358
[ITP 982140]: http://bugs.debian.org/982140
[kickoff]: https://github.com/j0ru/kickoff
[krunner]: https://invent.kde.org/frameworks/krunner
[lbonn/rofi]: https://github.com/lbonn/rofi
[mauncher]: https://github.com/mortie/mauncher
[not support Wayland]: https://github.com/davatorium/rofi/issues/446
[nwg-launchers]: https://github.com/nwg-piotr/nwg-launchers
[nwg-shell project]: https://nwg-piotr.github.io/nwg-shell/
[Onagre]: https://github.com/oknozor/onagre
[rather disgraceful battle in the pull request]: https://github.com/davatorium/rofi/pull/1139
[Rofi (lbonn's fork)]: https://github.com/lbonn/rofi
[sample emoji picker]: https://git.sr.ht/~emersion/dotfiles/tree/master/item/bin/emoji-menu
[sirula]: https://github.com/DorianRudolph/sirula
[tofi]: https://github.com/philj56/tofi
[two-line patch]: https://gitlab.com/anarcat/scripts/-/commit/3e8925e7f4257b44eb527bf7cb8f6d8687e9ed3b
[Ulauncher]: https://ulauncher.io
[wl-ime-type]: https://git.sr.ht/~emersion/wl-ime-type
[wlogout]: https://github.com/ArtsyMacaw/wlogout
[wlr-which-key]: https://github.com/MaxVerevkin/wlr-which-key
[wmenu]: https://sr.ht/~adnano/wmenu/
[wofi-pass]: https://github.com/TinfoilSubmarine/wofi-pass
[Wofi]: https://hg.sr.ht/~scoopta/wofi
[wtype]: https://github.com/atx/wtype
[ydotool]: https://github.com/ReimuNotMoe/ydotool
[yofi]: https://github.com/l4l/yofi
[πmenu]: https://github.com/phillbush/pmenu

### Fuzzel

I ended up completely switching to [fuzzel][] after realizing it was
the same friendly author as [foot][]. I did have to severely hack
around its limitations, by rewriting my rofi "modis" with plain shell
scripts. I wrote the following:

 * [dmenu-ssh.py](https://gitlab.com/anarcat/scripts/-/blob/main/dmenu-ssh.py): reads your SSH config and extracts hostnames,
   keeps history sorted by frequency in `~/.cache/dmenu-ssh`
 * [dmenu-bash-history](https://gitlab.com/anarcat/scripts/-/blob/main/dmenu-bash-history): reads your `.history` and `.bash_history`
   files and prompts for a command to run, appending `dmenu_path`,
   which is basically all available commands in your `$PATH`, also
   saves the command in your `.history` file (also required me to bump
   the size of that file to really be useful)
 * [pass-dmenu](https://gitlab.com/anarcat/scripts/-/blob/main/pass-dmenu): was already in use, just a little patch to support
   Wayland, basically list the pass entries sorted by domains
   ([pass-domains](https://gitlab.com/anarcat/scripts/-/blob/main/pass-domains)) and piped the picked password to the clipboard
   or `wl-type`
 * [dmenu-unicode](https://gitlab.com/anarcat/scripts/-/blob/main/dmenu-unicode): (NEW!) grep around the unicode database for
   emojis and other stuff

With those, I can basically use fuzzel or any other `dmenu`-compatible
program and not care, it will "just work".

[foot]: https://codeberg.org/dnkl/foot

## Image viewers: geeqie → geeqie

I wasn't happy with geeqie because the UI is a little weird and it
didn't support copy-pasting images (just their path). Thankfully, the
latter was fixed!

At first, Geeqie seem to work so well under Wayland. The fonts were
fuzzy and the thumbnail preview just didn't work anymore (filed as
[Debian bug 1024092][]). It seems it also has [problems with
scaling][]. All of those problems were solved and I'm now happily
using Geeqie, although I still think the UI is weird.

My main frustration with *all* image viewers I could find out there is
that they fail in one or multiple of those ways:

 * too slow (e.g. Darktable used to be fast, but now fails at "just
   scroll quickly through the collection, feh takes over 30 seconds to
   render a mere 187 thumbnails)

 * too roomy grids (e.g. geeqie has huge gaps around images, darktable
   is better, but also has big gaps, no image viewer I know correctly
   handles portraits and landscape views (that is, by scaling
   portraits images to be as high as landscape images so they all fit
   on the same line, or at least arranging a clever mosaic)

 * clunky (e.g. Darktable used to have a <kbd>z</kbd> key to flip
   between full screen preview of an image and the light table, that's
   gone and I don't know how to find it again; geeqie's side panel is
   stuck to the image, and i keep struggling to find ways to get rid
   of it; feh is just too hard to use, it's like learning vim except
   it's not the same keybinding, and they don't make much more sense)

 * requires rescan (e.g. Digikam required a couple of hours to even
   *start* showing me thumbnails)

 * crashes (both Digikam and Darktable segfault on me regularly)

 * heavy (Darktable is a memory and CPU hog, and keeps a 30GB
   thumbnail folder without which it just can't show images
   efficiently)

My standard for comparison is apps like Google Photos online which
have nice mosaic views, or Aves Libre on Android that you can just zip
through hundreds of images with the flick of a finger, without any
prior scanning. Aves fits all images in squares, which is an
acceptable compromise for me.

nomacs (which [this thread reminded me of](https://forums.linuxmint.com/viewtopic.php?t=321730)) is *pretty* close: it
loads 180 images almost instantly, and displays them without gaps. one
problem is it [doesn't recurse into folders](https://github.com/nomacs/nomacs/issues/297), it's really weird;
when you select a folder in the file explorer on the right, it only
goes down one level, which means i can only browse one day at a
time. The are also security concerns and [sustainability](https://github.com/nomacs/nomacs/issues/987).

Alternatives:

 * [GNOME Photos][]: stock GNOME image viewer, requires
   `tracker-miner-fs-3.service` which I masked here to save I/O and
   battery, shows nothing even when enabled
 * [gwenview][]: KDE viewer, in Debian
 * [koko][]: KDE viewer, in Debian
 * [imv][]: x11/wayland viewer, scriptable, [possible security issues
   and limited format support][], in Debian
 * [loupe][]: Glib, Rust, nice basic viewer, no gallery, default
   viewer in GNOME 45
 * [mvi][]: mpv-based image viewer
 * nomacs: basically abandoned upstream (no release since 2020), has
   an [unpatched][] [CVE-2020-23884][] since July 2020, does [bad
   vendoring][], and is in bad shape in Debian (4 minor releases
   behind).
 * [oculante][]: Rust, not in Debian, flatpak, weird gaps around full
   screen, built-in (custom) file browser has nice image previews
 * [pix][]: KDE/mobile viewer, large gap between images, confusing
   interface, seems designed for mobile, translates poorly on desktop,
   not in Debian, not to be confused with the [X-apps pix][]
   ([ITP](https://bugs.debian.org/968859))
 * [pqiv][]: has grid viewer, but not working that great
 * [pwall][]: mainly grid viewer, interesting but Flatpak is x11-only,
   2 minutes to index 2400 photos, hangs when I click to expand
   photos, iterates into `.git/annex` directories, generating dupes,
   not in Debian
 * [qimgv][]: grid view, "fast", themes, shortcuts, copy/move images,
   basic editing (crop/rotate/resize), scripting, video support, in
   Debian
 * [qview][]: C++, nice simple viewer, "fast" (not so much), no in
   Debian, flatpak
 * [swayimg][]: overlay, in Debian
 * [tiny image finder][]: grid viewer, looks promising but Flatpak
   failed to render any image
 * [vimiv][]: vim-like keybindings, not in Debian

Note that further tests have possibly shown a significant rendering
pipeline issue that makes images look blurry in *all* image viewers
except rare cases, see [this comment](https://todo.sr.ht/~whynothugo/shotman/11#event-404628) for a discussion. That was
filed as a [bug against Sway in Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1093671) for now. There was also a
[bug filed against grim for this](https://todo.sr.ht/~emersion/grim/98) and a [feature request in
shotman](https://todo.sr.ht/~whynothugo/shotman/11), but of which seem to be misplaced: the screenshots are
actually sharp, the problem is the image viewers!

See also [this list][], [this X11 list][] and [that list][] for other
list of image viewers, not necessarily ported to Wayland.

[Debian bug 1024092]: https://bugs.debian.org/1024092
[problems with scaling]: https://github.com/BestImageViewer/geeqie/issues/833
[gwenview]: https://invent.kde.org/graphics/gwenview
[koko]: https://invent.kde.org/graphics/koko
[imv]: https://sr.ht/~exec64/imv/
[possible security issues and limited format support]: https://lwn.net/Articles/908579/
[loupe]: https://gitlab.gnome.org/GNOME/loupe
[mvi]: https://github.com/occivink/mpv-image-viewer
[pix]: https://invent.kde.org/maui/pix
[swayimg]: https://github.com/artemsen/swayimg
[vimiv]: https://karlch.github.io/vimiv/
[this list]: https://anarc.at/blog/2020-09-30-presentation-tools/#other-options
[that list]: https://gitlab.com/anarcat/presentation-ethics/-/blob/master/README.md
[unpatched]: https://github.com/nomacs/nomacs/issues/516
[CVE-2020-23884]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1014124
[bad vendoring]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=974616
[this X11 list]: https://anarc.at/software/desktop/x11#image-viewer-geeqie-eog
[pqiv]: https://github.com/phillipberndt/pqiv
[pwall]: https://github.com/ltiber/pwall
[tiny image finder]: https://levz.dev/image-finder/
[GNOME Photos]: https://wiki.gnome.org/Apps/Photos
[X-apps pix]: https://github.com/linuxmint/pix/
[qimgv]: https://github.com/easymodo/qimgv
[oculante]: https://github.com/woelper/oculante
[qview]: https://interversehq.com/qview/

## Media player: mpv, gmpc / sublime

This is basically unchanged. `mpv` seems to work fine under Wayland,
better than Xorg on my new laptop (as mentioned in the introduction),
and that before the version which [improves Wayland support
significantly][], by bringing native Pipewire support and DMA-BUF
support.

[gmpc][] is more of a problem, mainly because it is abandoned. See
[[blog/2022-08-22-gmpc-alternatives]] for the full discussion, one of
the alternatives there will likely support Wayland.

Finally, I might just switch to [sublime-music][] instead... In any
case, not many changes here, thankfully.

[improves Wayland support significantly]: https://www.phoronix.com/news/MPV-0.35-Released
[gmpc]: https://tracker.debian.org/pkg/gmpc
[sublime-music]: https://gitlab.com/sublime-music/sublime-music

## Screensaver: xsecurelock → swaylock

I was previously using xss-lock and xsecurelock as a screensaver, with
xscreensaver "hacks" as a backend for xsecurelock.

The basic screensaver in Sway seems to be built with [swayidle][] and
[swaylock][]. It's interesting because it's the same "split" design
as xss-lock and xsecurelock.

That, unfortunately, does *not* include the fancy "hacks" provided by
xscreensaver, and that is [unlikely to be implemented upstream][].

Other alternatives include [gtklock][] ([in Debian](https://tracker.debian.org/pkg/gtklock)) and [waylock][] (zig), which
do not solve that problem either. gtklock is interesting though
because it has all sorts of plugins to show information on the lock
screen, which I find it quite lacking in swaylock:

 * [playerctl](https://github.com/jovanlanik/gtklock-playerctl-module) support: control media players ([in Debian](https://packages.debian.org/sid/gtklock-playerctl-module))
 * [userinfo](https://github.com/jovanlanik/gtklock-userinfo-module): show user icon and name ([in Debian](https://packages.debian.org/sid/gtklock-userinfo-module))
 * [more](https://github.com/jovanlanik/gtklock/wiki#references-2)

It looks like [swaylock-plugin][], a swaylock fork, which at least
attempts to solve this problem, although not directly using the real
xscreensaver hacks. [swaylock-effects][] is another attempt at this,
but it only adds more effects, it doesn't delegate the image
display. There's also an attempt at porting xscreensaver out right,
with [wscreensaver](https://sr.ht/~mstoeckl/wscreensaver/).

Other than that, maybe it's time to just let go of those funky
animations and just let swaylock do it's thing, which is display a
static image or just a black screen, which is fine by me.

In the end, I am just using `swayidle` with a configuration based on
[the systemd integration wiki page][] but with additional tweaks from
[this service][], see the resulting [[config/systemd/user/swayidle.service]] file.

Interestingly, damjan also has a [service for swaylock][] itself,
although it's not clear to me what its purpose is...

[swayidle]: https://github.com/swaywm/swayidle
[swaylock]: https://github.com/swaywm/swaylock
[unlikely to be implemented upstream]: https://github.com/swaywm/sway/issues/2254
[gtklock]: https://github.com/jovanlanik/gtklock
[waylock]: https://github.com/ifreund/waylock
[swaylock-plugin]: https://github.com/mstoeckl/swaylock-plugin
[swaylock-effects]: https://github.com/mortie/swaylock-effects/
[the systemd integration wiki page]: https://github.com/swaywm/sway/wiki/Systemd-integration#swayidle
[this service]: https://github.com/xdbob/sway-services/blob/master/systemd/swayidle.service.in
[service for swaylock]: https://github.com/gdamjan/sway-setup/blob/main/systemd/user/swaylock.service

## Screenshot: maim → grim, pubpaste

I'm a heavy user of [maim][] (and a package uploader in Debian). It
looks like the direct replacement to maim (and [slop][]) is [grim][]
(and [slurp][]). There's also [swappy][] which goes on *top* of grim
and allows preview/edit of the resulting image, nice touch (in Debian
since Trixie).

See also [awesome-wayland screenshots][] for other alternatives:
there are many, including X11 tools like [Flameshot][] that also
support Wayland.

One key problem here was that I have my own screenshot / pastebin
software which will needed an update for Wayland as well. That,
thankfully, meant actually cleaning up a lot of horrible code that
involved calling xterm and xmessage for user interaction. Now,
[pubpaste][] uses GTK for prompts and looks much better. (And before
anyone freaks out, I already had to use GTK for proper clipboard
support, so this isn't much of a stretch...)

One thing I'm, missing is some review/annotation
tool. [Satty](https://github.com/gabm/Satty) provides a nice minimal wrapper like that. For now,
I'm using whatever default image viewer I have configured (currently
geeqie), one key feature is that it must support the "copy image to
clipboard" (not the path! the actual full image!) functionality,
typically to paste to GitHub/GitLab issues, or Signal.

I've also started testing [shotman](https://shotman.whynothugo.nl/) (part of Debian Trixie) which
outlined that I might have an issue with fractional display and image
viewers, see the geeqie discussion above.

[maim]: https://github.com/naelstrof/maim
[slop]: https://tracker.debian.org/pkg/slop
[grim]: https://sr.ht/~emersion/grim/
[slurp]: https://github.com/emersion/slurp
[swappy]: https://github.com/jtheoof/swappy
[awesome-wayland screenshots]: https://github.com/natpen/awesome-wayland#screenshots
[Flameshot]: https://github.com/flameshot-org/flameshot
[pubpaste]: https://gitlab.com/anarcat/pubpaste

## Screen recorder: simplescreenrecorder → wf-recorder

In Xorg, I have used both [peek][] or [simplescreenrecorder][] for
screen recordings. The former *will* work in Wayland, but has [no
sound support][]. The latter has a [fork with Wayland support][] but
it is limited and buggy ("doesn't support recording area selection and
has issues with multiple screens").

It looks like [wf-recorder][] will just do everything correctly out
of the box, including audio support (with `--audio`, duh). It's also
packaged in Debian.

One has to wonder how this works while keeping the "between app
security" that Wayland promises, however... Would installing such a
program make my system less secure?

Many other options are available, see the [awesome Wayland
screencasting list][]. In particular, see [wl-screenrec][] which has
hardware encoding and much better performance, not in Debian (see
[1040786][]).

I also use [wshowkeys][] to ... well... show keys pressed during a
recording. Not in Debian, but trivial to package ([947858][]), main
annoyance is it requires a `setuid` binary to work.

[peek]: https://github.com/phw/peek
[simplescreenrecorder]: https://www.maartenbaert.be/simplescreenrecorder/
[no sound support]: https://github.com/phw/peek/issues/105
[fork with Wayland support]: https://github.com/foxcpp/ssr-wlroots
[wf-recorder]: https://github.com/ammen99/wf-recorder
[awesome Wayland screencasting list]: https://github.com/natpen/awesome-wayland#screencasting
[wl-screenrec]: https://github.com/russelltg/wl-screenrec
[1040786]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1040786
[wshowkeys]: https://git.sr.ht/~sircmpwn/wshowkeys
[947858]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=947858

## RSI: workrave → nothing?

[Workrave has no support for Wayland][]. [activity watch][] is a
time tracker alternative, but is not a RSI watcher. KDE has
[rsiwatcher][], but that's a bit too much on the heavy side for my
taste.

[SafeEyes][] looks like an alternative at first, but it has many
issues under Wayland ([escape doesn't work][], [idle doesn't
work][], it just doesn't work really). [timekpr-next][] *could* be
an alternative as well, and has support for Wayland.

I am also considering just abandoning workrave, even if I stick with
Xorg, because it apparently introduces significant latency in the
input pipeline. 

And besides, I've developed a pretty unhealthy alert fatigue with
Workrave. I have used the program for so long that my fingers know
exactly where to click to dismiss those warnings very effectively. It
makes my work just more irritating, and doesn't fix the fundamental
problem I have with computers.

[Workrave has no support for Wayland]: https://github.com/rcaelers/workrave/issues/94
[activity watch]: https://github.com/ActivityWatch/activitywatch
[rsiwatcher]: https://github.com/KDE/rsibreak
[SafeEyes]: http://slgobinath.github.io/SafeEyes/
[escape doesn't work]: https://github.com/slgobinath/SafeEyes/issues/480
[idle doesn't work]: https://github.com/slgobinath/SafeEyes/issues/391
[timekpr-next]: https://mjasnik.gitlab.io/timekpr-next/

## Other apps

This is a constantly changing list, of course. There's a bit of a
"death by a thousand cuts" in migrating to Wayland because you realize
how many things you were using are tightly bound to X.

 * `.Xresources` - just say goodbye to that old resource system, it
   was used, in my case, only for rofi, xterm, and ... [Xboard][]!?

 * keyboard layout switcher: built-in to Sway since 2017 ([PR
   1505][], 1.5rc2+), requires a small configuration change, see
   [this answer][] as well, looks something like this command:
   
        swaymsg input 0:0:X11_keyboard xkb_layout de
    
   or using this config:
   
        input * {
            xkb_layout "ca,us"
            xkb_options "grp:sclk_toggle"
        }

   That works refreshingly well, even better than in Xorg, I must say.
   
   [swaykbdd][] is an alternative that supports per-window layouts
   (in Debian).

 * wallpaper: currently using feh, will need a replacement, TODO:
   figure out something that does, like feh, a random shuffle.
   [swaybg][] just loads a *single* image, duh. [oguri][] might be a
   solution, but unmaintained, [used here][], not in
   Debian. [waypaper](https://github.com/anufrievroman/waypaper), [wpaperd](https://github.com/danyspin97/wpaperd), [wallutils][] and [swww](https://github.com/LGFae/swww) ([ITP](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1084753)) are other option, also not in
   Debian. [azote][] is now in Debian and seems like a good
   alternative. [hyprpaper](https://wiki.hyprland.org/Hypr-Ecosystem/hyprpaper/) is in Debian and works in Sway, but has
   a clunky configuration file, with only one image at a time. In
   theory, it can load other images through IPC, but that only works
   through hypr's IPC mechanism. 
   
   For now I just don't have a wallpaper, the background is a solid
   gray, which is better than Xorg's default (which is whatever crap
   was left around a buffer by the previous collection of programs,
   basically)

 * notifications: previously [dunst][] in some places, which works
   well in both Xorg and Wayland, not a blocker, [fnott][], [salut][]
   (not in Debian) possible alternatives: damjan [uses
   mako][]. Eventually migrated to [sway-nc][], but found it too
   complicated for my needs. Ended up with a simple mako-based setup
   with inhibition.

 * notification area: I had trouble making `nm-applet` work. based on
   [this nm-applet.service][], I found that you need to pass `--indicator`.  In
   theory, [tray icon support was merged in 1.5][], but in practice
   there are still [several limitations][], like [icons not
   clickable][]. On startup, `nm-applet --indicator` triggers this
   error in the Sway logs:
   
        nov 11 22:34:12 angela sway[298938]: 00:49:42.325 [INFO] [swaybar/tray/host.c:24] Registering Status Notifier Item ':1.47/org/ayatana/NotificationItem/nm_applet'
        nov 11 22:34:12 angela sway[298938]: 00:49:42.327 [ERROR] [swaybar/tray/item.c:127] :1.47/org/ayatana/NotificationItem/nm_applet IconPixmap: No such property “IconPixmap”
        nov 11 22:34:12 angela sway[298938]: 00:49:42.327 [ERROR] [swaybar/tray/item.c:127] :1.47/org/ayatana/NotificationItem/nm_applet AttentionIconPixmap: No such property “AttentionIconPixmap”
        nov 11 22:34:12 angela sway[298938]: 00:49:42.327 [ERROR] [swaybar/tray/item.c:127] :1.47/org/ayatana/NotificationItem/nm_applet ItemIsMenu: No such property “ItemIsMenu”
        nov 11 22:36:10 angela sway[313419]: info: fcft.c:838: /usr/share/fonts/truetype/dejavu/DejaVuSans.ttf: size=24.00pt/32px, dpi=96.00

   ... but it seems innocuous. The tray icon displays but, as stated
   above, is not clickable. If you don't see the icon, check the
   `bar.tray_output` property in the Sway config, try: `tray_output *`.
   Note that there is currently (November 2022) a [pull request][] to
   hook up a "Tray D-Bus Menu" which, [according to Reddit][] might
   fix this, or at least be somewhat relevant.
   
   This was the biggest irritant in my migration. I have used `nmtui`
   to connect to new Wifi hotspots or change connection settings, but
   that doesn't support actions like "turn off WiFi".
   
   I eventually fixed this by switching from [py3status][] to
   [waybar](https://github.com/Alexays/Waybar/).

 * window switcher: in `i3` I was using this bespoke [i3-focus][]
   script, which doesn't work under Sway, [swayr][] an option, not in
   Debian. So I put together [this other bespoke hack][] from
   multiple sources, which works.

 * PDF viewer: currently using atril and sioyek (both of which
   supports Wayland), could also just switch to zatura/mupdf
   permanently, see also [[calibre]] for a discussion on document
   viewers

See also [this list of useful addons][] and [this other list][] for other app alternatives.

[Xboard]: https://en.wikipedia.org/wiki/XBoard
[PR 1505]: https://github.com/swaywm/sway/pull/1505
[this answer]: https://unix.stackexchange.com/a/425433/30227
[swaykbdd]: https://github.com/artemsen/swaykbdd
[swaybg]: https://github.com/swaywm/swaybg
[oguri]: https://github.com/vilhalmer/oguri
[used here]: https://github.com/xdbob/sway-services/blob/master/systemd/oguri.service
[wallutils]: https://github.com/xyproto/wallutils
[dunst]: https://dunst-project.org/
[salut]: https://gitlab.com/snakedye/salut/
[uses mako]: https://github.com/gdamjan/sway-setup/blob/main/systemd/user/mako.service
[this nm-applet.service]: https://github.com/gdamjan/sway-setup/blob/main/systemd/user/nm-applet.service
[tray icon support was merged in 1.5]: https://github.com/swaywm/sway/pull/3249
[several limitations]: https://github.com/swaywm/sway/issues/3799
[icons not clickable]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=985205
[pull request]: https://github.com/swaywm/sway/pull/6249
[according to Reddit]: https://www.reddit.com/r/swaywm/comments/px0w11/swaybar_using_tray_bindcode_andor_tray_bindsym/
[i3-focus]: https://gitlab.com/anarcat/scripts/-/blob/main/i3-focus
[swayr]: https://sr.ht/~tsdh/swayr/
[this other bespoke hack]: https://gitlab.com/anarcat/scripts/-/blob/main/sway-focus
[this list of useful addons]: https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway
[this other list]: https://github.com/swaywm/sway/wiki/i3-Migration-Guide
[fnott]: https://codeberg.org/dnkl/fnott
[azote]: https://github.com/nwg-piotr/azote

## More X11 / Wayland equivalents

For all the tools above, it's not exactly clear what options exist in
Wayland, or when they do, which one should be used. But for some basic
tools, it seems the options are actually quite clear. If that's the
case, they should be listed here:

| X11          | Wayland                               | In Debian |
|--------------|---------------------------------------|-----------|
| `arandr`     | [nwg-displays][]                      | yes       |
| `autorandr`  | [kanshi][]                            | yes       |
| `xclock`     | [wlclock][]                           | no        |
| `xdotool`    | [wtype][]                             | yes       |
| `xev`        | [wev][], `xkbcli interactive-wayland` | yes       |
| `xlsclients` | `swaymsg -t get_tree`                 | yes       |
| `xprop`      | [wlprop][] or `swaymsg -t get_tree`   | no        |
| `xrandr`     | [wlr-randr][]                         | yes       |

[lswt][] is a more direct replacement for `xlsclients` but is not
packaged in Debian.

`xkbcli interactive-wayland` is part of the `libxkbcommon-tools`
package.

[lswt]: https://git.sr.ht/~leon_plickat/lswt
[wev]: https://git.sr.ht/~sircmpwn/wev
[wlr-randr]: https://sr.ht/~emersion/wlr-randr/
[kanshi]: https://sr.ht/~emersion/kanshi/
[wlprop]: https://gist.github.com/crispyricepc/f313386043395ff06570e02af2d9a8e0

See also:

 * <https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway>
 * <https://github.com/swaywm/sway/wiki/i3-Migration-Guide>

[this service file]: https://github.com/xdbob/sway-services/blob/master/systemd/kanshi.service

Note that arandr and autorandr are not directly part of
X. [arewewaylandyet.com][] refers to a few alternatives. We suggest
[wdisplays][] and [kanshi][] above (see also [this service file][])
but [wallutils][] can also do the autorandr stuff, apparently, and
[nwg-displays][] can do the arandr part. [shikane][] is a promising
kanshi rewrite in Rust. None of those (but kanshi and nwg-displays)
are packaged in Debian yet.

So I have tried [wdisplays][] and it Just Works, and well. The UI even
looks better and more usable than arandr, so another clean win from
Wayland here. I've since then switched to nwg-displays because it
directly saves a Sway-compatible configuration file in
`~/.config/sway/outputs`, it's just too bad it doesn't also save a
kanshi config, see also the [save profile feature request in
kanshi](https://todo.sr.ht/~emersion/kanshi/81) and the [kanshi support feature request in
nwg-displays](https://github.com/nwg-piotr/nwg-displays/issues/2).

Note that [shikane][] claims to support saving the current
configuration to a file, but it's [not packaged in Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1073813).

I'm currently [kanshi][] as a autorandr replacement and it mostly
works. It can be hard to figure out the right configuration to put,
and auto-detection doesn't always work. A key feature missing for me
is the [save profile functionality](https://todo.sr.ht/~emersion/kanshi/81) that autorandr has and which
makes it much easier to use.

[wdisplays]: https://github.com/artizirk/wdisplays
[nwg-displays]: https://github.com/nwg-piotr/nwg-displays
[wlclock]: https://git.sr.ht/~leon_plickat/wlclock
[shikane]: https://docs.rs/crate/shikane/latest

# Other issues

## systemd integration

I've had trouble getting session startup to work. This is partly
because I had a kind of funky system to start my session in the first
place. I used to have my whole session started from `.xsession` like
this:

    #!/bin/sh

    . ~/.shenv

    systemctl --user import-environment

    exec systemctl --user start --wait xsession.target

But obviously, the `xsession.target` is not started by the Sway
session. It seems to just start a `default.target`, which is really
not what we want because we want to associate the services directly
with the `graphical-session.target`, so that they don't start when
logging in over (say) SSH.

`damjan` on `#debian-systemd` showed me his [sway-setup][] which
features systemd integration. It involves starting a different session
in a completely new `.desktop` file. That work was [submitted
upstream][] but refused on the grounds that "I'd rather not give a
preference to any particular init system." Another PR was
[abandoned][] because "restarting [sway] does not makes sense: that
kills everything".

The work was therefore [moved to the wiki][].

So. Not a great situation. The [upstream wiki][] [systemd
integration][] suggests starting the systemd target *from within
Sway*, which has all sorts of problems:

 * you don't get Sway logs anywhere
 * control groups are all messed up

I have done a lot of work trying to figure this out, but I remember
that starting systemd from Sway didn't actually work for me: my
previously configured systemd units didn't correctly start, and
especially not with the right `$PATH` and environment.

So I went down that rabbit hole and managed to correctly configure
Sway to be started from the `systemd --user` session. 
I have partly followed the wiki but also picked ideas from damjan's
[sway-setup][] and xdbob's [sway-services][]. Another option is
[uwsm][] (not in Debian).

This is the config I have in `.config/systemd/user/`:

 * [[config/systemd/user/sway.service]]
 * [[config/systemd/user/sway-session.target]]

I have also configured those services, but that's somewhat optional:

 * [[config/systemd/user/gammastep.service]]
 * [[config/systemd/user/swayidle.service]]
 * [[config/systemd/user/wcolortaillog.service]]
 * [[config/systemd/user/wterminal.service]]

You will also need at least part of my [[sway config|config/sway/config]], which
sends the systemd notification (because, no, Sway doesn't support any
sort of readiness notification, that would be too easy). (And they [do
not want it](https://github.com/swaywm/sway/pull/7659), [nor does Debian want to carry a patch](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1039857).) And you might
like to see my [[swayidle config|config/swayidle/config]] while you're there.

Update: see [my latest attempt at sway readiness notification](https://github.com/swaywm/sway/pull/7904) for
the last hope.

Finally, you need to hook this up somehow to the login manager. This
is typically done with a desktop file, so drop
[sway-session.desktop][] in `/usr/share/wayland-sessions` and
[sway-user-service][] somewhere in your `$PATH` (typically
`/usr/bin/sway-user-service`).

The session then looks something like this:

    $ systemd-cgls | head -101
    Control group /:
    -.slice
    ├─user.slice (#472)
    │ → user.invocation_id: bc405c6341de4e93a545bde6d7abbeec
    │ → trusted.invocation_id: bc405c6341de4e93a545bde6d7abbeec
    │ └─user-1000.slice (#10072)
    │   → user.invocation_id: 08f40f5c4bcd4fd6adfd27bec24e4827
    │   → trusted.invocation_id: 08f40f5c4bcd4fd6adfd27bec24e4827
    │   ├─user@1000.service … (#10156)
    │   │ → user.delegate: 1
    │   │ → trusted.delegate: 1
    │   │ → user.invocation_id: 76bed72a1ffb41dca9bfda7bb174ef6b
    │   │ → trusted.invocation_id: 76bed72a1ffb41dca9bfda7bb174ef6b
    │   │ ├─session.slice (#10282)
    │   │ │ ├─xdg-document-portal.service (#12248)
    │   │ │ │ ├─9533 /usr/libexec/xdg-document-portal
    │   │ │ │ └─9542 fusermount3 -o rw,nosuid,nodev,fsname=portal,auto_unmount,subt…
    │   │ │ ├─xdg-desktop-portal.service (#12211)
    │   │ │ │ └─9529 /usr/libexec/xdg-desktop-portal
    │   │ │ ├─pipewire-pulse.service (#10778)
    │   │ │ │ └─6002 /usr/bin/pipewire-pulse
    │   │ │ ├─wireplumber.service (#10519)
    │   │ │ │ └─5944 /usr/bin/wireplumber
    │   │ │ ├─gvfs-daemon.service (#10667)
    │   │ │ │ └─5960 /usr/libexec/gvfsd
    │   │ │ ├─gvfs-udisks2-volume-monitor.service (#10852)
    │   │ │ │ └─6021 /usr/libexec/gvfs-udisks2-volume-monitor
    │   │ │ ├─at-spi-dbus-bus.service (#11481)
    │   │ │ │ ├─6210 /usr/libexec/at-spi-bus-launcher
    │   │ │ │ ├─6216 /usr/bin/dbus-daemon --config-file=/usr/share/defaults/at-spi2…
    │   │ │ │ └─6450 /usr/libexec/at-spi2-registryd --use-gnome-session
    │   │ │ ├─pipewire.service (#10403)
    │   │ │ │ └─5940 /usr/bin/pipewire
    │   │ │ └─dbus.service (#10593)
    │   │ │   └─5946 /usr/bin/dbus-daemon --session --address=systemd: --nofork --n…
    │   │ ├─background.slice (#10324)
    │   │ │ └─tracker-miner-fs-3.service (#10741)
    │   │ │   └─6001 /usr/libexec/tracker-miner-fs-3
    │   │ ├─app.slice (#10240)
    │   │ │ ├─xdg-permission-store.service (#12285)
    │   │ │ │ └─9536 /usr/libexec/xdg-permission-store
    │   │ │ ├─gammastep.service (#11370)
    │   │ │ │ └─6197 gammastep
    │   │ │ ├─dunst.service (#11958)
    │   │ │ │ └─7460 /usr/bin/dunst
    │   │ │ ├─wterminal.service (#13980)
    │   │ │ │ ├─69100 foot --title pop-up
    │   │ │ │ ├─69101 /bin/bash
    │   │ │ │ ├─77660 sudo systemd-cgls
    │   │ │ │ ├─77661 head -101
    │   │ │ │ ├─77662 wl-copy
    │   │ │ │ ├─77663 sudo systemd-cgls
    │   │ │ │ └─77664 systemd-cgls
    │   │ │ ├─syncthing.service (#11995)
    │   │ │ │ ├─7529 /usr/bin/syncthing -no-browser -no-restart -logflags=0 --verbo…
    │   │ │ │ └─7537 /usr/bin/syncthing -no-browser -no-restart -logflags=0 --verbo…
    │   │ │ ├─dconf.service (#10704)
    │   │ │ │ └─5967 /usr/libexec/dconf-service
    │   │ │ ├─gnome-keyring-daemon.service (#10630)
    │   │ │ │ └─5951 /usr/bin/gnome-keyring-daemon --foreground --components=pkcs11…
    │   │ │ ├─gcr-ssh-agent.service (#10963)
    │   │ │ │ └─6035 /usr/libexec/gcr-ssh-agent /run/user/1000/gcr
    │   │ │ ├─swayidle.service (#11444)
    │   │ │ │ └─6199 /usr/bin/swayidle -w
    │   │ │ ├─nm-applet.service (#11407)
    │   │ │ │ └─6198 /usr/bin/nm-applet --indicator
    │   │ │ ├─wcolortaillog.service (#11518)
    │   │ │ │ ├─6226 foot colortaillog
    │   │ │ │ ├─6228 /bin/sh /home/anarcat/bin/colortaillog
    │   │ │ │ ├─6230 sudo journalctl -f
    │   │ │ │ ├─6233 ccze -m ansi
    │   │ │ │ ├─6235 sudo journalctl -f
    │   │ │ │ └─6236 journalctl -f
    │   │ │ ├─afuse.service (#10889)
    │   │ │ │ └─6051 /usr/bin/afuse -o mount_template=sshfs -o transform_symlinks -…
    │   │ │ ├─gpg-agent.service (#13547)
    │   │ │ │ ├─51662 /usr/bin/gpg-agent --supervised
    │   │ │ │ └─51719 scdaemon --multi-server
    │   │ │ ├─emacs.service (#10926)
    │   │ │ │ ├─ 6034 /usr/bin/emacs --fg-daemon
    │   │ │ │ └─33203 /usr/bin/aspell -a -m -d en --encoding=utf-8
    │   │ │ ├─xdg-desktop-portal-gtk.service (#12322)
    │   │ │ │ └─9546 /usr/libexec/xdg-desktop-portal-gtk
    │   │ │ ├─xdg-desktop-portal-wlr.service (#12359)
    │   │ │ │ └─9555 /usr/libexec/xdg-desktop-portal-wlr
    │   │ │ └─sway.service (#11037)
    │   │ │   ├─6037 /usr/bin/sway
    │   │ │   ├─6181 swaybar -b bar-0
    │   │ │   ├─6209 py3status
    │   │ │   ├─6309 /usr/bin/i3status -c /tmp/py3status_oy4ntfnq
    │   │ │   └─6969 Xwayland :0 -rootless -terminate -core -listen 29 -listen 30 -…
    │   │ └─init.scope (#10198)
    │   │   ├─5909 /lib/systemd/systemd --user
    │   │   └─5911 (sd-pam)
    │   └─session-7.scope (#10440)
    │     ├─5895 gdm-session-worker [pam/gdm-password]
    │     ├─6028 /usr/libexec/gdm-wayland-session --register-session sway-user-serv…
    [...]

I think that's pretty neat.

 * TODO: consider this improved script: <https://github.com/xdbob/sway-services/blob/master/bin/sway-user-service>
 * TODO: move config into Puppet so it's in sync

[sway-setup]: https://github.com/gdamjan/sway-setup
[submitted upstream]: https://github.com/swaywm/sway/pull/3486
[abandoned]: https://github.com/swaywm/sway/issues/5160#issuecomment-641173221
[moved to the wiki]: https://github.com/swaywm/sway/wiki/Systemd-integration
[upstream wiki]: https://github.com/swaywm/sway/wiki/
[systemd integration]: https://github.com/swaywm/sway/wiki/Systemd-integration
[sway-services]: https://github.com/xdbob/sway-services
[uwsm]: https://github.com/Vladimir-csp/uwsm
[sway-session.desktop]: https://gitlab.com/anarcat/puppet/-/blob/0499fc3036570985a8e92a4e11d7bebb3d69a36c/site-modules/profile/files/sway/sway-session.desktop
[sway-user-service]: https://gitlab.com/anarcat/puppet/-/blob/main/site-modules/profile/files/sway/sway-user-service

## Environment propagation

At first, my terminals and rofi didn't have the right `$PATH`, which
broke a *lot* of my workflow. It's hard to tell exactly how Wayland
gets started or where to inject environment. [This discussion][]
suggests a few alternatives and [this Debian bug report][] discusses
this issue as well.

I eventually picked [environment.d(5)][] since I already manage my user
session with systemd, and it fixes a bunch of other problems. I used
to have a `.shenv` that I had to manually source everywhere. The only
problem with that approach is that it doesn't support conditionals,
but that's something that's rarely needed.

[This discussion]: https://github.com/swaywm/sway/wiki/Setting-Environmental-Variables
[this Debian bug report]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=968910

## Pipewire

This is a whole topic onto itself, but migrating to Wayland also
involves using [Pipewire][] if you want screen sharing to work. You
*can* actually keep using Pulseaudio for audio, that said, but that
migration is actually something I've wanted to do anyways: Pipewire's
design seems much better than Pulseaudio, as it folds in [JACK][]
features which allows for pretty neat tricks. (Which I should probably
show in a separate post, because this one is getting rather long.)

I first tried this migration in Debian bullseye, and it didn't work
very well. Ardour would [fail to export tracks][] and I would get
into weird situations where streams would just drop mid-way. 

A particularly funny incident is when I was in a meeting and I
couldn't hear my colleagues speak anymore (but they could) and I went
on blabbering on my own for a solid 5 minutes until I realized what
was going on. By then, people had tried numerous ways of letting me
know that something was off, including (apparently) coughing, saying
"hello?", chat messages, IRC, and so on, until they just gave up and
left.

I suspect that was also a Pipewire bug, but it could also have been
that I muted the tab by error, as I recently learned that clicking on
the little tiny speaker icon on a tab mutes that tab. Since the tab
itself can get pretty small when you have lots of them, it's actually
quite frequently that I mistakenly mute tabs.

Anyways. Point is: I already knew how to make the migration, and I had
already [documented how to make the change in Puppet][]. It's
basically:

    apt install pipewire pipewire-audio-client-libraries pipewire-pulse wireplumber 

Then, as a regular user:

    systemctl --user daemon-reload
    systemctl --user --now disable pulseaudio.service pulseaudio.socket
    systemctl --user --now enable pipewire pipewire-pulse
    systemctl --user mask pulseaudio

An optional (but key, IMHO) configuration you should also make is to
"switch on connect", which will make your Bluetooth or USB headset
automatically be the default route for audio, when connected. In
`~/.config/pipewire/pipewire-pulse.conf.d/autoconnect.conf`:

    context.exec = [
        { path = "pactl"        args = "load-module module-always-sink" }
        { path = "pactl"        args = "load-module module-switch-on-connect" }
        #{ path = "/usr/bin/sh"  args = "~/.config/pipewire/default.pw" }
    ]

See the excellent — as usual — [Arch wiki page about Pipewire][] for
that trick and more information about Pipewire. Note that you must
*not* put the file in `~/.config/pipewire/pipewire.conf` (or
`pipewire-pulse.conf`, maybe) directly, as that will break your
setup. If you want to add to that file, first copy the template from
`/usr/share/pipewire/pipewire-pulse.conf` first.

So far I'm happy with Pipewire in bookworm, but I've heard mixed
reports from it. I have high hopes it will become the standard media
server for Linux in the coming months or years, which is great because
I've been (rather boldly, I admit) on the record saying [[I don't like
PulseAudio|blog/2013-02-04-why-i-dont-pulseaudio]].

Rereading this now, I feel it might have been a little unfair, as
"over-engineered and tries to do too many things at once" applies
probably even more to Pipewire than PulseAudio (since it also handles
video dispatching).

That said, I think Pipewire took the right approach by implementing
*existing* interfaces like Pulseaudio and JACK. That way we're not
adding a third (or fourth?) way of doing audio in Linux; we're just
making the server better.

[Pipewire]: https://pipewire.org/
[JACK]: https://jackaudio.org/
[fail to export tracks]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=994208
[documented how to make the change in Puppet]: https://gitlab.com/anarcat/puppet/-/blob/main/site-modules/profile/manifests/pipewire.pp
[Arch wiki page about Pipewire]: https://wiki.archlinux.org/title/PipeWire#Sound_does_not_automatically_switch_when_connecting_a_new_device

## Keypress drops

Sometimes I lose keyboard presses. This correlates with the following
warning from Sway:

    déc 06 10:36:31 curie sway[343384]: 23:32:14.034 [ERROR] [wlr] [libinput] event5  - SONiX USB Keyboard: client bug: event processing lagging behind by 37ms, your system is too slow 

... and corresponds to an [open bug report in Sway](https://github.com/swaywm/sway/issues/5423). It seems the
"system is too slow" should really be "your compositor is too slow"
which seems to be the case here on this older system
([[hardware/curie]]). It doesn't happen often, but it does happen,
particularly when a bunch of busy processes start in parallel (in my
case: a linter running inside a container and `notmuch new`).

The [proposed fix](https://github.com/swaywm/sway/pull/6994) for this in Sway is to gain real time privileges
and add the `CAP_SYS_NICE` capability to the binary. We'll see how
that goes in Debian once 1.8 gets released and shipped.

## Output mirroring

Sway does [not support output mirroring](https://github.com/swaywm/sway/issues/1666), a strange limitation
considering the flexibility that software like [wdisplays][] *seem* to
offer.

(In practice, if you layout two monitors on top of each other in that
configuration, they do *not* actually mirror. Instead, sway assigns a
workspace to each monitor, as if they were next to each other but,
confusingly, the cursor appears in *both* monitors. It's extremely
disorienting.)

The bug report has been open since 2018 and has seen a long
discussion, but basically no progress. Part of the problem is the
ticket tries to tackle "more complex configurations" as well, not just
output mirroring, so it's a long and winding road.

Note that other Wayland compositors (e.g. [Hyprland][], GNOME's
Mutter) *do* support mirroring, so it's not a fundamental limitation
of Wayland.

One workaround is to use a tool like
[wl-mirror](https://github.com/Ferdi265/wl-mirror) to make a window
that mirrors a specific output and place *that* in a different
workspace. That way you place the output you want to mirror *to* next
to the output you want to mirror *from*, and use wl-mirror to copy
between the two outputs. The problem is that wl-mirror is [not
packaged in Debian yet](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1012684) (update: fixed since 2023, Debian 13
trixie).

Another workaround mentioned in the thread is to use a [[presentation
tool|blog/2020-09-30-presentation-tools]] which supports mirroring on
its own, or presenter notes. So far I have generally found workarounds
for the problem, but it might be a big limitation for others.

# Improvements over i3

## Tiling improvements

There's a lot of improvements Sway could bring over using plain
i3. There are pretty neat auto-tilers that could replicate the
configurations I used to have in Xmonad or Awesome, see:

 * [autotiling][] (now in Debian)
 * [swaymonad][]

[autotiling]: https://github.com/nwg-piotr/autotiling
[swaymonad]: https://github.com/nicolasavru/swaymonad

Inversely, some people find i3 is actually a *regression* on some
things, namely the way multiple monitors are managed. I actually got
used to this, but people missing the way [AwesomeWM](https://awesomewm.org/) did that
should look at [swaysome](https://gitlab.com/hyask/swaysome).

## Display latency tweaks

TODO: You can tweak the display latency in wlroots compositors with the
[max_render_time][] parameter, possibly getting lower latency than
X11 in the end.

[max_render_time]: https://manpages.debian.org/bullseye/sway/sway.5.en.html

## Sound/brightness changes notifications

The goal here is to display a pop-up to give feedback on volume or
brightness changes, or other state changes.

For now, I am testing [poweralertd](https://sr.ht/~kennylevinsen/poweralertd/) which monitors power sends
standard notifications on state changes and [sway-nc][] (shipped with
bookworm) that replaces dunst and also provides sliders for
backlight. Default config is almost useless, good stuff in the
[discussion forum](https://github.com/ErikReider/SwayNotificationCenter/discussions/183). Still very GUI-y and mouse driven, not enough
text... e.g. we don't see the actual volume or brightness in
percentage, so i still have a brightness module in waybar.

Other alternatives:

 * [Avizo][], not in Debian, requires keybinding wrapper
 * [SwayOSD][], [entered Debian NEW, hopefully in trixie](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1057301), requires
   keybinding wrapper or libinput access, [sample sway config](https://codeberg.org/werdahias/graffe/src/commit/76c109b2ead688fad2ab5ddbe81419d5acbf270e/.config/sway/config#L58-L67)
 * [wayout][], also provides a way to write stuff on the display, but
   is too rudimentary for our needs
 * [wob][], packaged in Debian, just a generic progress bar overlay,
   requires [more elaborate wrappers](https://github.com/francma/wob/blob/729e5c68215547eb8939c4c1778f58cca36b2bc0/contrib/README.md)

[Avizo]: https://github.com/misterdanb/avizo
[SwayOSD]: https://github.com/ErikReider/SwayOSD
[sway-nc]: https://github.com/ErikReider/SwayNotificationCenter
[wob]: https://github.com/francma/wob
[wayout]: https://git.sr.ht/~proycon/wayout

# Debugging tricks

The `xeyes` (in the `x11-apps` package) will run in Wayland, and can
actually be used to easily see if a given window is *also* in
Wayland. If the "eyes" follow the cursor, the app is actually running
in xwayland, so not natively in Wayland.

Another way to see what is using Wayland in Sway is with the command:

    swaymsg -t get_tree

# Other documentation

 * [awesome-wayland][]
 * [arewewaylandyet.com][]
 * [hacktivist.me notes][]
 * [Arch Linux wiki][]
 * [Gentoo apps list][]
 * [Sway: useful addons][]
 * [i3 migration guide][]

[awesome-wayland]: https://github.com/natpen/awesome-wayland
[arewewaylandyet.com]: https://arewewaylandyet.com/
[hacktivist.me notes]: https://hacktivis.me/notes/pure-wayland.shtml
[Arch Linux wiki]: https://wiki.archlinux.org/title/Wayland
[Gentoo apps list]: https://wiki.gentoo.org/wiki/Wayland_Desktop_Landscape
[Sway: useful addons]: https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway
[i3 migration guide]: https://github.com/swaywm/sway/wiki/i3-Migration-Guide

# Conclusion

In general, this took me a long time, but it mostly works. The tray
icon situation is pretty frustrating, but there's a workaround and I
have high hopes it will eventually fix itself. I'm also actually
worried about the DisplayLink support because I eventually *want* to
be using this, but hopefully that's another thing that will hopefully
fix itself before I need it.

## A word on the security model

I'm kind of worried about all the hacks that have been added to
Wayland just to make things work. Pretty much everywhere we need to,
we punched a hole in the security model:

 * windows can spy on each other (although [xdg-desktop-portal-wlr][]
   does confirm if you have some `chooser` installed, see
   [xdg-desktop-portal-wrl(5)][])

 * windows can type over each other (through e.g. [wtype][] through
   the [virtual-keyboard protocol](https://wayland.app/protocols/virtual-keyboard-unstable-v1))

 * windows can overlay on top of each other (so one app could, for
   example, spoof a password dialog, through the [layer-shell
   protocol](https://wayland.app/protocols/wlr-layer-shell-unstable-v1))

[Wikipedia describes the security properties of Wayland][] as it
"isolates the input and output of every window, achieving
confidentiality, integrity and availability for both." I'm not sure
those are actually realized in the actual implementation, because of
all those holes punched in the design, at least in Sway. For example,
apparently the GNOME compositor doesn't have the virtual-keyboard
protocol, but they do have (another?!) [text input protocol](https://www.phoronix.com/news/GNOME-Wayland-GTK-Input-Proto).

Wayland does offer a better basis to implement such a system,
however. It feels like the Linux applications security model lacks
critical decision points in the UI, like the user approving "yes, this
application can share my screen now". Applications themselves *might*
have some of those prompts, but it's not mandatory, and that is
worrisome.

[Wikipedia describes the security properties of Wayland]: https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)#Comparison_with_other_window_systems

[[!tag unix wayland blog desktop firefox emacs debian debian-planet]]

[xdg-desktop-portal-wrl(5)]: https://manpages.debian.org/xdg-desktop-portal-wlr.5
