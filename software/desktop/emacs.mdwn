I am a heavy Emacs users. I'm surprised there isn't any documentation
about this in this wiki, but better late than ever, as they say.

Configuration
=============

My Emacs configuration has *finally* been sanitized and published
online, see my [emacs-d][] git repository for details.

[emacs-d]: https://gitlab.com/anarcat/emacs-d

Packages
========

The original reason why I setup this page is to document the packages
I use. I have little trust in the Emacs packaging system: there are no
cryptographic signatures in package archives so no trust path between
users and developers. When I discussed this in the Emacs community, it
became apparent the only solution to my problem, short term, was to
use the Debian packaging infrastructure instead. This has the
advantage of automating but possibly also delegating maintenance of
those packages.

Here's the list of packages I currently use.

| Package | Emacs | Debian | Description |
| ------- | ----- | ------ | ----------- |
| anzu | [0.67](https://stable.melpa.org/#/anzu) | [0.67.post1](https://tracker.debian.org/elpa-anzu)| Show number of matches in mode-line while searching |
| auto-dictionary | [1.1](https://stable.melpa.org/#/auto-dictionary) | [1.1.post1](https://tracker.debian.org/elpa-auto-dictionary)| Automatic dictionary switcher for flyspell |
| cape | [1.7](https://stable.melpa.org/#/cape) | [#728729](https://bugs.debian.org/728729)| Completion At Point Extensions |
| consult | [1.8](https://stable.melpa.org/#/consult) | [1.8.post1](https://tracker.debian.org/elpa-consult)| Consulting completing-read |
| corfu | [1.5](https://stable.melpa.org/#/corfu) | [1.5.post1](https://tracker.debian.org/elpa-corfu)| COmpletion in Region FUnction |
| crux | [0.5.0](https://stable.melpa.org/#/crux) | [#909337](https://bugs.debian.org/909337)| A Collection of Ridiculously Useful eXtensions |
| dabbrev | N/A | N/A| None |
| devdocs | [20241113.1341](https://melpa.org/#/devdocs) | N/A| Emacs viewer for DevDocs |
| dimmer | [0.4.2](https://stable.melpa.org/#/dimmer) | [0.4.2+repack20220817.a5b6975.3](https://tracker.debian.org/elpa-dimmer)| Visually highlight the selected buffer |
| dockerfile-mode | [1.9](https://stable.melpa.org/#/dockerfile-mode) | [1.9.post3](https://tracker.debian.org/elpa-dockerfile-mode)| Major mode for editing Docker's Dockerfiles |
| eglot | N/A | [1.15.post3](https://tracker.debian.org/elpa-eglot)| None |
| embark | [1.1](https://stable.melpa.org/#/embark) | [1.1.post2](https://tracker.debian.org/elpa-embark)| Conveniently act on minibuffer completions |
| embark-consult | [1.1](https://stable.melpa.org/#/embark-consult) | N/A| Consult integration for Embark |
| flycheck | [34.1](https://stable.melpa.org/#/flycheck) | [34.1.post2](https://tracker.debian.org/elpa-flycheck)| On-the-fly syntax checking |
| flymake | N/A | N/A| None |
| format-all | [0.6.0](https://stable.melpa.org/#/format-all) | [0.6.0.post2](https://tracker.debian.org/elpa-format-all)| Auto-format C, C++, JS, Python, Ruby and 50 other languages |
| frames-only-mode | [1.0.0](https://stable.melpa.org/#/frames-only-mode) | N/A| Use frames instead of Emacs windows |
| gnus-alias | [20230818.1830](https://melpa.org/#/gnus-alias) | N/A| An alternative to gnus-posting-styles |
| go-mode | [1.6.0](https://stable.melpa.org/#/go-mode) | N/A| Major mode for the Go programming language |
| gptel | [0.9.7](https://stable.melpa.org/#/gptel) | N/A| Interact with ChatGPT or other LLMs |
| hl-todo | [3.8.1](https://stable.melpa.org/#/hl-todo) | [3.8.1.post1](https://tracker.debian.org/elpa-hl-todo)| Highlight TODO and similar keywords |
| ledger | N/A | [4.0.0.post2](https://tracker.debian.org/elpa-ledger)| None |
| magit | [4.1.3](https://stable.melpa.org/#/magit) | [4.1.3.post1](https://tracker.debian.org/elpa-magit)| A Git porcelain inside Emacs |
| marginalia | [1.7](https://stable.melpa.org/#/marginalia) | [1.7.post1](https://tracker.debian.org/elpa-marginalia)| Enrich existing commands with completion annotations |
| markdown-mode | [2.6](https://stable.melpa.org/#/markdown-mode) | [2.6.post2](https://tracker.debian.org/elpa-markdown-mode)| Major mode for Markdown-formatted text |
| multiple-cursors | [1.4.0](https://stable.melpa.org/#/multiple-cursors) | [#861127](https://bugs.debian.org/861127)| Multiple cursors for Emacs |
| notmuch | [0.38.3](https://stable.melpa.org/#/notmuch) | [0.38.3.post3](https://tracker.debian.org/elpa-notmuch)| Run notmuch within emacs |
| org | N/A | [9.7.16+dfsg.1](https://tracker.debian.org/elpa-org)| None |
| pdf-tools | [1.1.0](https://stable.melpa.org/#/pdf-tools) | [1.1.0.post3](https://tracker.debian.org/elpa-pdf-tools)| Support library for PDF documents |
| puppet-mode | [0.4](https://stable.melpa.org/#/puppet-mode) | [0.4.post5](https://tracker.debian.org/elpa-puppet-mode)| Major mode for Puppet manifests |
| rainbow-mode | N/A | [1.0.6.post2](https://tracker.debian.org/elpa-rainbow-mode)| None |
| recentf | N/A | N/A| None |
| rg | [2.3.0](https://stable.melpa.org/#/rg) | [2.3.0.post4](https://tracker.debian.org/elpa-rg)| A search tool based on ripgrep |
| shift-number | [0.1](https://stable.melpa.org/#/shift-number) | N/A| Increase/decrease the number at point |
| smart-mode-line | [2.14](https://stable.melpa.org/#/smart-mode-line) | [2.14.post2](https://tracker.debian.org/elpa-smart-mode-line)| A color coded smart mode-line |
| srcery-theme | [20240220.805](https://melpa.org/#/srcery-theme) | N/A| Dark color theme |
| vertico | [1.9](https://stable.melpa.org/#/vertico) | [1.9.post1](https://tracker.debian.org/elpa-vertico)| VERTical Interactive COmpletion |
| visual-fill-column-mode | N/A | N/A| None |
| wc-mode | [1.4.1](https://stable.melpa.org/#/wc-mode) | [1.4.post2](https://tracker.debian.org/elpa-wc-mode)| Running word count with goals (minor mode) |
| which-key | [3.6.0](https://stable.melpa.org/#/which-key) | [3.6.0.post2](https://tracker.debian.org/elpa-which-key)| Display available keybindings in popup |
| writegood-mode | [2.2.0](https://stable.melpa.org/#/writegood-mode) | [2.2.0.post3](https://tracker.debian.org/elpa-writegood-mode)| Polish up poor writing on the fly |
| writeroom-mode | [3.11](https://stable.melpa.org/#/writeroom-mode) | [3.11.post3](https://tracker.debian.org/elpa-writeroom-mode)| Minor mode for distraction-free writing |
| ws-butler | [0.7](https://stable.melpa.org/#/ws-butler) | [0.6.post5](https://tracker.debian.org/elpa-ws-butler)| Unobtrusively remove trailing whitespace |
| yasnippet | [0.14.0](https://stable.melpa.org/#/yasnippet) | [0.14.0+git20230912.76e1eee6.2](https://tracker.debian.org/elpa-yasnippet)| Yet another snippet extension for Emacs |

The above was automatically generated using [check-emacs-packages](https://gitlab.com/anarcat/scripts/blob/main/check-emacs-packages)
with some manual modifications for packages on the main ELPA archive
(as opposed to MELPA, which is surprisingly rare). To feed the list of
packages into the script, I used this mighty pipeline:

    check-emacs-packages $( ( grep '^(use-package' ~/.emacs.d/init.el | sed 's/.* //' ; \
    grep -A2 packages ~/.emacs.d/custom.el  | tail -1 | sed 's/[()]//g;s/ /\n/g' ) \
    | sort -u ) 2>/dev/null
