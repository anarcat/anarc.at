I have a well-customized [[xmonad config file|xmonad.hs]]. Tab 1 is
email, 2 is chat, 3 is web, 4 to 6 is probably a bunch of terminals, 7
and 8 are special and 9 is the music player.

I don't remember why I started using Xmonad. I remember being annoyed
at [Awesome][] for forcing me to rewrite my configs at major upgrades
and noticing my friends were using a cool Haskell-based WM.

My main gripe with Xmonad is the lack of good floating windows
support.

I have used a `xmonad-session-rc` file to load things on startup,
a special feature of the xmonad Debian package that i found quite
useful.

I <del>may</del>have switched to [i3][] when I get bored, because it seems all the cool
kids are switching to that. Or maybe Gnome, or KDE, because [that's
what Mr. Robot says is cool][]. Right? Right.

[that's what Mr. Robot says is cool]: https://www.youtube.com/watch?v=FQM5fU7V-MM
[i3]: http://i3wm.org/
[Awesome]: https://awesomewm.org/
