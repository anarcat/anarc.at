#!/bin/sh

set -e

for dir in sway foot fuzzel mako waybar swayidle swaylock; do
    rsync -a ~/.config/$dir/ config/$dir/
done

for unit in gammastep.service mako.service swayidle.service sway.service sway-session.target wcolortaillog.service  wterminal.service ; do
    rsync ~/.config/systemd/user/$unit config/systemd/user/$unit
done
