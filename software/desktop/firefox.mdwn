[[!meta title="Firefox web browser configuration"]]

[[!toc levels=2]]

TL;DR: I use [Firefox](https://getfirefox.com/), for technical and
political reasons. This page documents my config and why.

I run the "Quantum" version (57+) as it's a huge performance
improvement, even on older machines. Some plugins fell by the wayside
but I was able to find replacement for most of what I need. I
installed it from the upstream tarballs on some machine, but I also
tried using [Snap](https://snapcraft.io) which also works generally well. I documented
that procedure in [the Debian wiki](https://wiki.debian.org/Firefox#Using_snap). For machines running under
buster or later, the quantum version is available as a Debian package
(now ESR too!) so those hacks are not necessary.

# Extensions

This section documents the [Firefox add-ons](https://addons.mozilla.org/) I am using, testing,
or have used in the past.

## Installed

I have those extensions installed and use them very frequently:

 * [browserpass-ce](https://addons.mozilla.org/en-US/firefox/addon/browserpass-ce/) ([[!debpkg webext-browserpass desc="debian
   package"]], [source](https://github.com/browserpass/browserpass)) - super fast access to my passwords. use
   some magic mumble-jumble message passing thing which feels a bit
   creepy. possible alternative: [passff](https://github.com/passff/passff#readme), no Debian package,
   [872773](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=872773).
 * [Cookie AutoDelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/) (no Debian package, [908285](http://bugs.debian.org/908285),
   [source](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete)) - clear long-term identities for all sites except a
   few, too bad it does not sync with uBlock/uMatrix ([issue
   43](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete/issues/43))
 * [Livemarks](https://addons.mozilla.org/en-US/firefox/addon/livemarks/) (no deb, [source](https://github.com/nt1m/livemarks)) or [Awesome RSS](https://addons.mozilla.org/en-US/firefox/addon/awesome-rss/) (no deb,
   [source](https://github.com/shgysk8zer0/awesome-rss)) - replace the [Live bookmarks removal](https://support.mozilla.org/en-US/kb/live-bookmarks-migration)
 * [uBlock Origin][] ([[!debpkg webext-ublock-origin desc="debian
   package"]], [source](https://github.com/gorhill/uBlock)) - making the web sane again
 * [URL to QR Code](https://addons.mozilla.org/en-US/firefox/addon/url-to-qrcode/?src=search) - (no debian package, [source](https://github.com/smoqadam/url-to-qrcode-firefox-addon)) after
   removing another alternative that was proprietary spyware (!! see
   below), I found about 6 different alternatives (this one and
   [1](https://addons.mozilla.org/en-US/firefox/addon/qr-code-util/), [2](https://addons.mozilla.org/en-US/firefox/addon/fxqrl/), [3](https://addons.mozilla.org/en-US/firefox/addon/ffqrcoder/), [4](https://addons.mozilla.org/en-US/firefox/addon/qrify/), [5](https://addons.mozilla.org/en-US/firefox/addon/qr-coder/) - what is wrong with you
   people??) This is the most popular, reviews are mostly positive,
   seems to be working offline, has a free license, and source is
   available. Super simple too.
 * [Wallabager][] (no debian package, [source](https://github.com/wallabag/wallabagger)) - to YOLO a bunch
   of links in a pile outside my web browser that I can read offline
   thanks to [Wallabako](https://gitlab.com/anarcat/wallabako/)

[Wallabager]: https://addons.mozilla.org/en-US/firefox/addon/wallabagger/
[uMatrix]: https://addons.mozilla.org/firefox/addon/umatrix/
[uBlock Origin]: https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/
[GhostText]: https://addons.mozilla.org/en-US/firefox/addon/ghosttext/
[dynamic number of pages]: https://developer.mozilla.org/en-US/docs/Mozilla/Tech/Places/Places_Expiration
[keeps it for 90 days]: https://support.google.com/chrome/answer/95589
[site engagement]: https://www.chromium.org/developers/design-documents/site-engagement
[infamous privacy intrusions]: https://lwn.net/Articles/648392/

Ideally, all of those should be packaged for Debian.

## In testing

I am testing those and they might make it to the top list once I'm happy:

 * [Auto Tab Discard](https://addons.mozilla.org/en-US/firefox/addon/auto-tab-discard/) (no deb, [source](https://github.com/rNeomy/auto-tab-discard)), like the old,
   [dead](https://github.com/greatsuspender/thegreatsuspender/issues/1263) Great Suspender. I was specifically looking for a feature
   to suspend ("discard") a specific time taking up too much CPU (but
   not close it). [all tabs helper](https://addons.mozilla.org/en-US/firefox/addon/all-tabs-helper/) can also do this and manage
   tabs as well, but seems to overlap with the tab menu, Auto Tab
   Discard is also [recommended](https://support.mozilla.org/en-US/kb/add-on-badges). I previously used [Snooze
   tabs](https://addons.mozilla.org/en-US/firefox/addon/snoozetabs/) (no deb, [source](https://github.com/bwinton/SnoozeTabs#readme)) for this as well.
 * [Clean URLs](https://docs.clearurls.xyz/) (no deb, [source](https://github.com/ClearURLs/Addon)) - remove garbage in URLs
 * [Display anchors](https://addons.mozilla.org/en-US/firefox/addon/display-_anchors/) (no deb, [source](https://github.com/Rob--W/display-anchors))
 * Firefox [Multi-account containers][] (no deb, [source](https://github.com/mozilla/multi-account-containers/)) - kind of
   useful to separate work/private stuff and generally keep cross-site
   surveillance under control. I was also using "Container Tab Groups"
   (AKA "TabArray") but this had [data loss issues](https://github.com/menhera-org/TabArray/issues/457). That extension
   was very useful to "hide a container", but the builtin extension
   also provides that feature, just with a bunch more clicks. 
   - builtin: extension button -> click ">" on the right container -> "Hide this
   container"
   - tabarray: right-click on tab and "hide this container" See
   [Hide/Show container more accessible (#755)](https://github.com/mozilla/multi-account-containers/issues/755), [Show only this
   container option (#1662)](https://github.com/mozilla/multi-account-containers/issues/1662), and [Some options are hard to access
   from the container list (#2089)](https://github.com/mozilla/multi-account-containers/issues/2089).
   Containers are rendered mostly irrelevant by the "first party
   isolation" features shipped with Firefox 87 (also known as [total
   cookie protection](https://blog.mozilla.org/security/2021/02/23/total-cookie-protection/)), my primary use case for those containers is
   to have tab groups (e.g. "work", "play") that i can hide easily. 
   Containers are not actually required for *that*, although I do like
   the "temporary container" feature to test sites..
   Mozilla is working on native tab groups (alpha available with
   `browser.tabs.groups.enabled` in nightly as of 2024-11-01, but
   lacking lots of UX), which might make all of this moot as well.
   Also tested [sideberry](https://addons.mozilla.org/en-US/firefox/addon/sidebery/) (overkill, no support for hiding tabs)
   and [simple tab groups](https://addons.mozilla.org/en-US/firefox/addon/simple-tab-groups/) (all but simple, not as usable as tabarray)
 * [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) (no deb, [source](https://github.com/greasemonkey/greasemonkey/)) - mostly for [this one hack
   for Nextcloud Calendar to show UTC times alongside local](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/nextcloud#showing-utc-times-in-weekly-calendar-view)
 * [Link hints][] (no deb, [source](https://github.com/lydell/LinkHints/)) - nice and simple alternative
   to full-scale keyboard driven interface like [tridactyl][], see the
   [keybindings](#keybindings) section below
 * [Popup window](https://addons.mozilla.org/en-US/firefox/addon/popup-window/) (no deb, [source](https://github.com/ettoolong/PopupWindow)) - open the link in a
   pop-up, useful to have an "app-like" window for a website (I use
   this for videoconferencing in a second tab)

[tridactyl]: https://github.com/tridactyl/tridactyl
[builtin Firefox shortcuts]: https://support.mozilla.org/en-US/kb/keyboard-shortcuts-perform-firefox-tasks-quickly
[Multi-account containers]: https://github.com/mozilla/multi-account-containers/

Those should probably not be packaged in Debian until they make it to
the top list.

## Might use again

Those were in testing for a while, then installed, but then I got
tired of them...

 * [Dark Background and Light Text](https://addons.mozilla.org/en-GB/firefox/addon/dark-background-light-text/) (no deb, [source](https://github.com/m-khvoinitsky/dark-background-light-text-extension)) - Mozilla
   also recommends [Midnight Lizard](https://addons.mozilla.org/addon/midnight-lizard-quantum/) for Android which I couldn't
   figure out how to disable by default (and only enable on some
   sites). I also tested [dark reader](https://addons.mozilla.org/addon/darkreader/), which seem a bit
   overkill. Nowadays I just use `browser.in-content.dark-mode`, see
   below.
 * [GhostText][] (no debian package, [#910289](https://bugs.debian.org/910289), [source](https://github.com/GhostText/GhostText))- "It's
   all text" replacement, allowed me to edit text areas with my
   favorite text editor (Emacs), worried about security implications
 * [LibRedirect]() ([no deb](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1018865), [source](https://libredirect.github.io/source_code.html)) - redirect big platforms
   to proxied alternatives, i particularly like the Medium.com
   alternative, https://scribe.rip - stopped using because
   alternatives are too flaky
 * [Minimal](https://addons.mozilla.org/en-US/firefox/addon/minimal-internet-experience/) ([homepage](https://minimal.community/)) - removes autoplay, search suggestions
   and all sorts of junks from many websites (alternatives:
   [shutup](https://addons.mozilla.org/en-US/firefox/addon/shut-up-comment-blocker/) for comments, uBlock origin dynamic rules, e.g. [those
   rules](https://news.ycombinator.com/item?id=26120168)) - replaced by uBlock cosmetic rules
 * [Open in Browser](https://addons.mozilla.org/en-US/firefox/addon/open-in-browser/) (no deb, [source](https://github.com/Rob--W/open-in-browser)) - reopen the file in the
   browser instead of downloading - not really used that much
 * [redirector](https://addons.mozilla.org/en-US/firefox/addon/redirector/) (no deb, [homepage](https://einaregilsson.com/redirector/), [source](https://github.com/einaregilsson/Redirector)) - mainly to
   redirect to `old.reddit.com`, really ([alternative just for reddit](https://addons.mozilla.org/en-US/firefox/addon/old-reddit-redirect/)
 * [Switch container](https://addons.mozilla.org/en-US/firefox/addon/switch-container/) (no deb, [source](https://gitlab.com/mjanetmars/switch-container)) - fixes *one* of the
   issues with multi-account containers (ie. moving tab to another
   container)
 * [Temporary containers](https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/) - not sure that works so
   well... installed it and then never used it so i uninstalled it.
 * [View Page Archive & Cache](https://addons.mozilla.org/en-US/firefox/addon/view-page-archive/) (no deb, [source](https://github.com/dessant/view-page-archive/)) - load page in
   one or many page archives. No "save" button unfortunately, but is
   good enough for my purposes. [The Archiver](https://addons.mozilla.org/en-US/firefox/addon/the-archiver/) (no deb,
   [source](https://www.cathalmcnally.com/tools/the-archiver/)) is another option that does the reverse: save only, no
   view. -- not really used that much

## Previously used

I once used those but eventually removed them for various
reasons. Some are unsupported, non-free software, inconvenient, too
hard to use or simply irrelevant.

 * [adblock plus](https://addons.mozilla.org/fr/firefox/addon/1865) - now selling ads! replaced with ublock
 * [Addons compatibility reporter](https://addons.mozilla.org/en-US/firefox/addon/add-on-compatibility-reporter/) - useless since Firefox 57 /
   Quantum, as incompatible extensions are just *disabled*
 * [Debian buttons](https://icedeb.ktnx.net/) didn't work for me as it requires buttons, so I
   made a simple [bookmarks folder](https://salsa.debian.org/debian/debian-bookmarks-shortcuts) instead
 * [firebug](https://addons.mozilla.org/firefox/1843/) - somewhat built-in
 * [it's all text!][] ([[!debpkg xul-ext-itsalltext desc="debian
   package"]], [source](https://github.com/docwhat/itsalltext)) - now [obsolete](https://github.com/docwhat/itsalltext/issues/94) and replaced by
   GhostText (see above)
 * [password hasher](https://addons.mozilla.org/fr/firefox/addon/3282) -
   has security issues - completely removed any password management
   from my browser, see [[blog/2017-03-02-password-hashers]] for a
   further review of password hashers.
 * [Privacy Badger](https://www.eff.org/privacybadger), tested as a replacement for the more
   aggressive uMatrix. Issues: doesn't allow blanket configuration
   (e.g. block cookies by default) and difficult to make mass
   configuration. Very hard to edit the list of domains, sometimes
   clicking on a domain would scroll back up. Doesn't block any Google
   cookies when I visit their sites, which is a no-no for me.
 * [QR Code Image Generator](https://addons.mozilla.org/en-US/firefox/addon/qr-code-image-generator/) (no debian package, no source??) - to
   send links to my phone. removed because no source code is
   available, is not free software, and seems to not actually be
   working offline, so URL contents are shared with an online
   service.
 * [Sea containers](https://addons.mozilla.org/en-US/firefox/addon/sea-containers/) - another GUI for containers. installed it and
   then never used it so i uninstalled it.
 * [Smart HTTPS](https://addons.mozilla.org/en-US/firefox/addon/smart-https-revived/) (no deb, [source](https://github.com/ilGur1132/Smart-HTTPS)) - some use [HTTPS
   everywhere](https://www.eff.org/https-everywhere) but I found that one works too and doesn't require
   sites to be added to a list. nowadays, https URLs match http URLs
   quite well: long gone are the days where wikipedia had a special
   "secure" URL... HE does have a "Block all unencrypted requests"
   setting, but it does exactly that: it breaks plaintext sites
   completely. See [issue #7936](https://github.com/EFForg/https-everywhere/issues/7936) and [issue #16488](https://github.com/EFForg/https-everywhere/issues/16488) for
   details. Nowadays, I just don't need any extension: I enable
   [HTTPS-only mode](https://blog.mozilla.org/security/2020/11/17/firefox-83-introduces-https-only-mode/) (AKA `dom.security.https_only_mode`). The EFF
   even [deprecated HTTPS everywhere](https://www.eff.org/https-everywhere/set-https-default-your-browser) because of this.
 * [translations](https://addons.mozilla.org/en-GB/firefox/addon/firefox-translations/) (no deb, [source](https://github.com/mozilla/firefox-translations)) - native LLM translation,
   now available natively through `browser.translations.enable`, now
   default. [models also available](https://github.com/mozilla/firefox-translations-models/)
 * [U2F Support](https://addons.mozilla.org/en-US/firefox/addon/u2f-support-add-on/), is now unnecessary as it is builtin, starting
   with FF 57 (see [issue #59](https://github.com/prefiks/u2f4moz/issues/59#issuecomment-325768286)). the upstream issue
   was [#1065729](https://bugzilla.mozilla.org/show_bug.cgi?id=1065729)
 * [uMatrix][] ([[!debpkg webext-umatrix desc="debian package"]],
   [source](https://github.com/gorhill/uMatrix)) - abandoned upstream, replaced by uBlock origin's
   advanced mode and dynamic filtering, see [this migration
   script](https://github.com/ashwinvis/umatrix2ublock), and Cookie Autodelete. Cookie Autodelete is actually
   somewhat nicer than uMatrix because it accepts cookies everywhere,
   but only for a while, so you need a much shorter whitelist
   (basically only the sites which you really want a long-term
   identity on, whereas you had to unblock cookies from a lot more
   sites just to get things to work at all in uMatrix)
 * [Wayback machine](https://addons.mozilla.org/en-US/firefox/addon/wayback-machine_new/) (no deb, [source](https://github.com/internetarchive/wayback-machine-chrome)?) - i also have
   bookmarklets, but this could work better! Unfortunately, it doesn't
   work with other archival sites like archive.is or Google's
   cache. It also tries to be too smart about broken sites: it will
   try to show the archive.org version when it "thinks" the website is
   down, but it often fails to notice when a site is down or think
   it's down when it isn't. Replaced with [View Page Archive &
   Cache][].
 * [webdevelopper toolbar](https://addons.mozilla.org/firefox/60/) - not exactly builtin, but builtins are
   good enough now
 * [yslow](https://addons.mozilla.org/fr/firefox/addon/5369) - now more or less built-in
 * [zotero](https://www.zotero.org/) is in a bad shape in Debian. The
   "XUL" extension is gone from Zotero 5.0, and the 4.0 extension will
   stop working because upstream will drop support in 2018. Debian is
   scrambling to package the newer version that is only standalone
   ([#871502](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=871502)). Right now I'm using the <del>standalone binary from
   upstream</del> flatpak but I'm looking at alternatives, see
   [[services/bookmarks]] for that more general problem.

[it's all text!]: https://addons.mozilla.org/en-US/firefox/addon/its-all-text/

# Surviving the XULocalypse

I wasn't very affected by the "XULocalypse", or the removal of older
"XUL" extensions from Firefox 60. My biggest blocker was [it's all
text!](https://addons.mozilla.org/en-US/firefox/addon/its-all-text/) and I quickly found a replacement. Others have had more
trouble, however, here are some references:

 * [pabs][]
 * [clint](https://xana.scru.org/posts/bamamba/quantumofsadness.html)
 * [Alternatives database](https://mozilla.github.io/extension-finder/), incomplete
 * [Another database](https://gist.github.com/IzzySoft/db7c7d4243283ca84b3abd8b94114e2e), more complete, but probably not exhaustive either
 * [Discussion on Mozilla forum](https://discourse.mozilla.org/t/favorite-webextensions/17087/1)
 * [Mozilla "find a replacement"](https://blog.mozilla.org/addons/2017/10/26/helping-find-compatible-extensions/) documentation, pretty useless

[pabs]: https://bonedaddy.net/pabs3/log/2018/09/08/webextocalypse/
And here are the replacements I have found:

 * [DOM Inspector](https://addons.mozilla.org/en-US/firefox/addon/dom-inspector-6622/): now builtin
 * [it's all text!][]: [GhostText][], Firefox recommends
   [withExEditor](https://addons.mozilla.org/en-US/firefox/addon/withexeditor/) but that requires running a separate process to
   launch the editor, while GhostText implements this through editor
   plugins instead.
 * [u2f support addon](https://addons.mozilla.org/en-US/firefox/addon/u2f-support-add-on/?src=api): now builtin
 * [uBlock origin][]: ported
 * [uMatrix][]: ported
 * [Wallabager][]: ported
 * [web developer](https://addons.mozilla.org/en-US/firefox/addon/web-developer/): ported
 * [vimperator][]: see the [keybindings section](#keybindings) below
 
Those are the extensions I was using for which no replacement exists:

 * *alternative trust models*, like [Monkeysphere][]. as [pabs][]
   documented, those are simply [not supported](https://bugzilla.mozilla.org/show_bug.cgi?id=1489080) <del>yet</del> and
   might never be, which means [Certificate Patrol](http://patrol.psyced.org/),
   [Perspectives](https://perspectives-project.org/firefox/), [Monkeysphere][] and [Communism](https://about.psyc.eu/communism) are all
   incompatible with Firefox 57 and later
 * *tab management*. Plugins that would regroup or hide tabs have had
   difficulty switching over. For example, [vertical tabs](https://addons.mozilla.org/en-US/firefox/addon/vertical-tabs-reloaded/) has been
   ported, but [can't turn off the normal tab strip](https://bugzilla.mozilla.org/show_bug.cg?id=1332447). Mozilla
   maintains a list of [tab organizers](https://addons.mozilla.org/en-US/firefox/collections/mozilla/tab-organizers/) that might answer your
   needs and I'm testing the [Multi-account containers][] stuff for
   now. When there are too many tabs, Firefox automatically adds a
   drop-down on the right of the tab list as well, which happens when
   [browser.tabs.tabMinWidth](http://kb.mozillazine.org/Browser.tabs.tabMinWidth) gets triggered. So turning that *up*
   counter-intuitively enables the drop-down even if there not that
   many tabs, at the cost of showing less tabs at once. I set it to
   `100`, the default being `76`.
 * *profile switching* extensions, like [profilist](https://addons.mozilla.org/en-US/firefox/addon/profilist/), is apparently
   [not supported](https://github.com/Noitidart/Profilist/issues/51) by the new API.
 * *standalone applications* like [Zotero](https://www.zotero.org/): they released a new
   version (5) that switches to a "connector" model where there is
   only a "standalone" program and plugins in the web browser to talk
   to it. Unfortunately, Zotero is still a [XUL](https://en.wikipedia.org/wiki/XULRunner) application and
   uses an old, unsupported Firefox version as a runtime:

        $ ./zotero-bin --version
        Mozilla Firefox 52.7.4

[Monkeysphere]: http://monkeysphere.info/

In my experience, if your upstream is still active, chances are the
extension was ported, provided there are APIs for the feature. The new
"webext" interface has the advantage of being almost directly
compatible with Chrome extensions, which makes it easier to maintain a
plugin across browsers but there are, as we can see, some limitations
in the newer APIs.

For users that can't afford to switch over the the newer extensions,
there are already two forks of Firefox created in direct response to
the removal of XUL/XPCOM from Firefox 57:

 * [Waterfox](https://www.waterfoxproject.org/en-US/)
 * [Pale moon](https://www.palemoon.org/)

It is unclear, however, whether those browsers will be sustainable in
the long term.

[link hints]: https://addons.mozilla.org/en-US/firefox/addon/linkhints/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search

# Configuration

I have set the following configuration options, in a `user.js` file
that I version-control into git:

 * `browser.tabs.loadDivertedInBackground` ([ref](http://kb.mozillazine.org/About:config_entries)):
   true (fixes an issue where focus would change to the firefox window
   (and workspace!) when clicking links in other apps
 * `browser.search.defaultenginename`: [searx.me](https://searx.me/)
   (default search engine)
 * `browser.startup.page` ([ref](http://kb.mozillazine.org/Browser.startup.page)):
   3 (startup with previous session)
 * `browser.tabs.tabMinWidth` ([ref](http://kb.mozillazine.org/Browser.tabs.tabMinWidth)): ensure tab titles are
   readable and trigger the vertical dropdown earlier
 * `network.cookie.cookieBehavior` ([ref](http://kb.mozillazine.org/Network.cookie.cookieBehavior#3_2)):
   1 (no third-party cookies)
 * `browser.in-content.dark-mode`: true (prefer dark CSS, see [this
   discussion](https://css-tricks.com/dark-modes-with-css/), [new in FF 67](https://blog.logrocket.com/whats-new-in-firefox-67-prefers-color-scheme-and-more-195be81df03f/)), also set
   `ui.systemUsesDarkTheme` to `1`. see [this doc](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme), also set
   "theme" to be "dark", flipping to "automatic" when i need "light"
 * `privacy.resistFingerprinting`: true (helps with
   [fingerprinting](https://www.bitestring.com/posts/2023-03-19-web-fingerprinting-is-worse-than-I-thought.html5), but [breaks dark mode](https://bugzilla.mozilla.org/show_bug.cgi?id=1535189), see also [this TB
   bug](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40337))
 * fixing the scroll bars:
   * `widget.gtk.overlay-scrollbars.enabled`: false (don't overlay the
     scrollbars, make it take their own space)
   * `widget.non-native-theme.scrollbar.style`: 4 ("windows 10"
     scrollbar style, which is a big, chunky, square-ish scrollbar)
   * the actual width can be tweaked with
     `widget.non-native-theme.scrollbar.size.override`, I set it to 10
   * a better approach might be to [tweak the GTK theme
     instead](https://unix.stackexchange.com/a/592978/30227)... 
     
     I ended up doing `apt install breeze-gtk-theme` which
     looks a bit better in *other* GTK apps, but somehow Firefox
     doesn't pick that up argh. I still set the following to have
     other GTK apps behave:
     
         gsettings set org.gnome.desktop.interface gtk-theme Breeze-Dark
         gsettings set org.gnome.desktop.interface color-scheme prefer-dark
         gsettings set org.gnome.desktop.interface icon-theme Breeze-Dark
         gsettings set org.gnome.desktop.interface icon-theme Breeze-Dark
         gsettings set org.gnome.desktop.interface gtk-key-theme "Emacs"
         gsettings set org.gnome.desktop.interface font-name "Noto Sans 11"
         gsettings set org.gnome.desktop.interface document-font-name "Noto Sans 11"
         gsettings set org.gnome.desktop.interface monospace-font-name "Fira Mono 11" 

     Useful hack:

         gsettings list-recursively org.gnome.desktop.interface | grep -i font

     Source: https://wiki.archlinux.org/title/GTK

 * `middlemouse.contentLoadURL` ([ref](http://kb.mozillazine.org/Middlemouse.contentLoadURL)):
   false (got used to chromium not doing that, and it seems too risky:
   passwords can leak in DNS too easily if you miss the field)
 * [U2F configuration](https://wiki.mozilla.org/Security/CryptoEngineering#Using_U2F_.2F_WebAuthn):
   * `security.webauth.u2f` - enable U2F token support, to use 2FA
     with the Yubikey and other 2FA tokens
   * `security.webauth.webauthn` - enable [WebAuthN](https://www.w3.org/TR/webauthn/) support, not
     sure what that's for but it sounds promising
 * `browser.urlbar.trimURLs`: false. show protocol regardless of URL
 * `dom.security.https_only_mode`: `true` - only access HTTPS
   websites, click-through for bypass.

I also set privacy parameters following this [user.js](https://gitlab.com/anarcat/scripts/blob/main/firefox-tmp#L7) config
which, incidentally, is injected in temporary profiles started with
this [firefox-tmp](https://gitlab.com/anarcat/scripts/blob/main/firefox-tmp) script I use to replace `chromium
--temp-profile`. This is part of the effort to [sanitize default
Firefox behavior in Debian](https://wiki.debian.org/Firefox#Automatic_connections).

I also override certain site's stylesheets in my
`~/.mozilla/firefox/*/chrome/userContent.css` CSS file. For example,
this restricts the width of pages in the Debian wiki:

    /* limit paragraph width to ease reading, and center */
    @-moz-document domain(wiki.debian.org) {
        div#content { max-width: 60em !important; margin: auto !important; }
    }
    @-moz-document domain(lwn.net) {
        div.ArticleText { max-width: 60em !important; margin: auto !important; }
    }

The syntax of this file is basically undocumented. Its location and
basic usage is documented [in MozillaZine](http://kb.mozillazine.org/UserContent.css) but not much further.

I add some search engines that are misconfigured from [Mycroft](http://mycroftproject.com/search-engines.html?name=linguee) and
import my set of [Debian bookmarks](https://salsa.debian.org/debian/debian-bookmarks-shortcuts) for quick access to Debian
resources.

More similar projects:

 * [arkenfox/user.js](https://github.com/arkenfox/user.js): "Firefox privacy, security and
   anti-tracking: a comprehensive user.js template for configuration
   and hardening"

 * [SebastianSimon/firefox-omni-tweaks](https://github.com/SebastianSimon/firefox-omni-tweaks): "A script that disables
   the clickSelectsAll behavior of Firefox, and more."

 * [Firefox Privacy Guide for Dummies!](https://12bytes.org/articles/tech/firefox/the-firefox-privacy-guide-for-dummies/): actually pretty complete
   and not at all "for dummies"

 * [Mullvad Browser](https://mullvad.net/en/browser/): Tor Browser without Tor, has a [good list of
   tweaks](https://mullvad.net/en/browser/hard-facts)

## Keybindings

I use the default keybindings in Firefox, but try to bind some
extensions by hand. I have also meddled with using the web browser
without the mouse.

I was originally using [vimperator][] for a while, until the
[XULocalypse](#surviving-the-xulocalypse) anyway...

Since then, quite a few [vimperator alternatives][] have popped
up. [tridactyl][] is possibly the most prominent one. tridactyl has
some annoyances, like <kbd>C-f</kbd> being bound to "page down"
although that can be disabled with `:unbind <C-f>`.

[vimium-ff][] ([vimium][]) and [vim-vixen][] are also working
alternatives right now, although the vimperator folks say they lack
some features, I couldn't figure out which. Vimium has the major
problem of not entering the "edit mode" (where keybindings are not
effective) in text areas, or at least in Etherpad.

[SurfingKeys](https://github.com/brookhong/Surfingkeys) is another vim-like extension.

[pentadactyl][] is the father to all of those, but seems to have
disappeared off the internet. [vimfx][] also did not survive the
XULocalypse.

There's also lighter versions like [link hints][] and [key jump][].

[vimium]: https://github.com/philc/vimium
[vimperator]: https://github.com/vimperator/vimperator-labs
[vim-vixen]: https://github.com/ueokande/vim-vixen
[vimfx]: https://github.com/akhodakivskiy/VimFx
[pentadactyl]: http://web.archive.org/web/20171019152504/http://5digits.org/pentadactyl/
[vimium-ff]: https://addons.mozilla.org/en-US/firefox/addon/vimium-ff/
[salsa key]: https://addons.mozilla.org/en-US/firefox/addon/saka-key/
[pulled from AMO]: https://github.com/tridactyl/tridactyl/issues/1800
[vimperator alternatives]: https://github.com/vimperator/vimperator-labs#end-of-life-and-alternatives

[Krabby](https://krabby.netlify.com/), another of those implementations, has an [interesting
list of alternatives](https://github.com/alexherbo2/krabby/blob/c525cf13962f72f4810fdc8f8032e6d9001308ea/docs/alternatives.md). Krabby itself is marked as inactive in
[Qutebrowser][]'s list of alternatives, it hasn't had a commit since
2021 at the time of writing (April 2023).

[key jump]: https://addons.mozilla.org/en-US/firefox/addon/key-jump-keyboard-navigation/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search

Other dead alternatives include:

 * [salsa key][], without vi-like keybindings, GitHub repository
   archived in 2022-11-07

See also the [builtin Firefox shortcuts][].

# Remaining work

My Firefox configuration is not fully automated yet. The `user.js`
hacks above only go so far. For example, the search engine override
[doesn't seem to work anymore](https://superuser.com/questions/1372679/how-to-set-duckduckgo-as-default-search-engine-using-user-js). Similarly, it is not possible to
populate the following:

 * search engines
 * bookmarks
 * extensions

Bookmarks and search engines seems to be hackable through a
[distribution file](https://wiki.mozilla.org/Distribution_INI_File) (and a [different one on mobile](https://wiki.mozilla.org/Mobile/Distribution_Files)??), but I
haven't figured out how that works just yet. It also seems extensions
are going to become harder since [Mozilla decided to stop
sideloading](https://blog.mozilla.org/addons/2020/03/10/support-for-extension-sideloading-has-ended/) for some obscure reason...

I miss the times where bookmarks where just that HTML file sitting in
the profile directory...

# History

I have been a long time user of the "Mozilla" family of web
browsers. My first web browser (apart from [[!wikipedia lynx]]) was
probably the venerable [[!wikipedia Netscape Navigator]], which was
eventually opened source into what was then called Pheonix and then
[[!wikipedia Firefox]]. I eventually abandoned Firefox because of
stability and features: an HTML5 video site would crash firefox, and
when I tried it in Chromium, it worked, so I gave on up on Firefox
then.

But now (Jan 2017) I have switched back to Firefox, mostly because of
privacy reasons. There are multiple privacy issues in Chromium (which
is supposed to be the unbranded version of the Google Chrome browser).
Some [infamous privacy intrusions][] were fixed, but others werent:
bug [[!debbug 792580]] (phones home to [[!wikipedia DoubleClick]] and
[[!wikipedia Google Analytics]]) was filed in 2015 and I confirmed it
in 2016, and it's still not fixed. I have also found troubling the
[site engagement][] profile that Chromium builds on you (which carries
over into the Incognito mode). I also had concerns that Chromium would
keep history indefinitely, but it looks like it actually
[keeps it for 90 days][]. Firefox is now actually worst than Chromium
in that regard as it keeps a [dynamic number of pages][] instead of a
configurable delay. I also had problems with Chromium not opening tabs
when it's lacking focus ([[!debbug 848930]]), a new regression that
was really annoying as I visit a lot of websites... There's the
[ungoogled-chromium][] project which attempts to correct all of those
issues, but that is yet another browser, and it's not packaged in
Debian, so not really an option for me right now.

[ungoogled-chromium]: https://github.com/Eloston/ungoogled-chromium

So long story short, I use firefox now. It's nice to root for the
[[!wikipedia Browser_wars desc="underdog"]] anyways.

# Remaining issues

My remaining concerns with Firefox, right now, are:

 * <del>it's slower than Chromium: Firefox starts in about 2 seconds here
   whereas Chromium starts in less than a second (on `curie`, i3-6100U
   4x2.3Ghz, 16GB of ram, on my laptop, it's even worst: about 7-8
   seconds for Firefox and < 2 seconds for Chromium). since i usually
   have the browser already started, that's kind of okay.</del> This
   is fixed in Firefox 57: it's super fast, startup time seems even
   faster than Chromium.
 * history retention settings are [unclear](https://www.reddit.com/r/firefox/comments/3gbm7m/how_long_does_firefox_keep_history/) - FF computes a number
   that "will not affect performance" but that may mean a number that
   is just huge. For example, it keeps 162 751 pages here, keeping
   pages well over 6 months. There are extensions to fix this like
   [Expire history by days](https://addons.mozilla.org/en-US/firefox/addon/expire-history-by-days/) and [History cleaner](https://addons.mozilla.org/en-US/firefox/addon/history-cleaner/) 

# Alternative browsers

Even though writing a browser is *hard* these days, quite a few people
have tried to tackle this. Here are some interesting alternatives
here:

 * [Tor Browser](https://www.torproject.org/download/): basically a fork of Firefox with (mandatory) Tor
   support baked in, alongside lots of strong privacy patches,
   includes No Script by default
 * [Qutebrowser][]: keyboard-driven, Python and Qt5,
   QtWebEngine-based, some support for adblock
 * [Luakit](https://luakit.github.io/): "micro-browser framework", extensible with Lua,
   C, WebKit2, GTK+
 * [vimb](https://fanglingsu.github.io/vimb/): vim-like, C, WebKit2, GTK

[Qutebrowser]: https://www.qutebrowser.org/

Dead browsers:

 * [uzbl](https://github.com/uzbl/uzbl) (2018)
