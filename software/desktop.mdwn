This page documents, in a spirit of sharing known recipes, my current
and older desktop setups. You can also read a narrative of the history
behind all this an more in [[history]] (2012).

[[!toc levels=2]]

# Current setup

I'm now (2022) using [[wayland]] with Sway, see the [[wayland]] page
for the full documentation of the transition away from [[x11]].

See also the documentation specific to [[firefox]].

# Older setups

 * [[x11]] (using [[wayland]] now), which include:
   * twm, afterstep, blackbox/fluxbox/openbox, englightenment, fvwm
     and GNOME 1 (time immemorial, probably turn of the millenium, and
     apparently before 2005, afterstep itself is 1997-2008, according
     to the [news section on the website](http://www.afterstep.org/news.php?show=old), GNOME 1 is 1997-2002,
     twm is from 1987 and, amazingly, has seen a release in 2022)
   * [[sawfish and rox]] (???-2005)
   * [[gnome2]] (~2006? GNOME 2 itself is 2002-2011)
   * [[wmii]] (2006-2010)
   *  awesome (2010-2015)
   * [[xmonad]] (2015-2019~)
   * i3 (~2019-2022) see [[x11]] for the documentation on that

# Other configurations

 * [[chromium]] (using [[firefox]] now)
 * the [[gnome3]] exception, when I don't want to get complicated
 * I have also tested and used KDE from time to time
 * [[emacs]] has its own page
