[[!toc levels=2]]

Bandwidth usage
===============

Uplink
------

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/if_ng0-day.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/if_ng0.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/if_ng0-month.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/if_ng0.html)

IPv6
----

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/if_gif0-day.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/if_gif0.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/if_gif0-month.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/if_gif0.html)

VPN
---

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/if_tap0-day.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/if_tap0.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/if_tap0-month.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/if_tap0.html)

Latency
=======

Uplink
------

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/multiping-day.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/multiping.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/multiping-month.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/multiping.html)

IPv6
----

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/ping6_ipv6_google_com-day.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/ping6_ipv6_google_com.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/ping6_ipv6_google_com-month.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/ping6_ipv6_google_com.html)

Packet rate
===========

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/pf_states-day.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/pf_states.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/pf_states-month.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/pf_states.html)


Temperature
===========

Marcos, internal
----------------

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/marcos.anarc.at/sensors_temp-week.png" />](http://munin.anarc.at/anarc.at/marcos.anarc.at/sensors_temp.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/marcos.anarc.at/sensors_temp-year.png" />](http://munin.anarc.at/anarc.at/marcos.anarc.at/sensors_temp.html)

Users
=====

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/marcos.anarc.at/users-week.png" />](http://munin.anarc.at/anarc.at/marcos.anarc.at/users.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/marcos.anarc.at/users-year.png" />](http://munin.anarc.at/anarc.at/marcos.anarc.at/users.html)

Time
====

Marcos
------

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/marcos.anarc.at/ntp_offset-week.png" />](http://munin.anarc.at/anarc.at/marcos.anarc.at/ntp_offset.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/marcos.anarc.at/ntp_offset-year.png" />](http://munin.anarc.at/anarc.at/marcos.anarc.at/ntp_offset.html)

Roadkiller
------

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/ntp_offset-week.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/ntp_offset.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/ntp_offset-year.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/ntp_offset.html)

Uptime
======

Marcos
------

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/marcos.anarc.at/uptime-week.png" />](http://munin.anarc.at/anarc.at/marcos.anarc.at/uptime.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/marcos.anarc.at/uptime-year.png" />](http://munin.anarc.at/anarc.at/marcos.anarc.at/uptime.html)

Roadkiller
----------

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/uptime-week.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/uptime.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/anarc.at/roadkiller.anarc.at/uptime-year.png" />](http://munin.anarc.at/anarc.at/roadkiller.anarc.at/uptime.html)

Boulette
--------

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/reseaulibre.ca/boulette.reseaulibre.ca/uptime-week.png" />](http://munin.anarc.at/reseaulibre.ca/boulette.reseaulibre.ca/uptime.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/reseaulibre.ca/boulette.reseaulibre.ca/uptime-year.png" />](http://munin.anarc.at/reseaulibre.ca/boulette.reseaulibre.ca/uptime.html)

Plastik
--------

[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/reseaulibre.ca/platisk.reseaulibre.ca/uptime-week.png" />](http://munin.anarc.at/reseaulibre.ca/platisk.reseaulibre.ca/uptime.html)
[<img src="http://munin.anarc.at/munin-cgi/munin-cgi-graph/reseaulibre.ca/platisk.reseaulibre.ca/uptime-year.png" />](http://munin.anarc.at/reseaulibre.ca/platisk.reseaulibre.ca/uptime.html)
