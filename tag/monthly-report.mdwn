[[!meta title="Free software activities reports"]]

Since late 2015, I report, generally every month, on my free software
activities, both paid (e.g. through the [[debian-lts]] project) or
unpaid work.

I work on a large variety of software, see [[software]] for an overview and [[software/contributions]] for an exhaustive inventory. See also the tag cloud on this page for an idea of what the reports cover.

[[!pagestats pages="tag/*" among="tagged(monthly-report)"]]

[[!inline pages="tagged(monthly-report)" actions="no" archive="yes"
feedshow=10]]
