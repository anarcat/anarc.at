[[!meta title="Goatcounter analytics in ikiwiki"]]

I have started using [Goatcounter](https://www.goatcounter.com/) for analytics after reading
[this LWN article](https://lwn.net/Articles/822568/) called "Lightweight alternatives to Google
Analytics". Goatcounter has an interesting approach to privacy in that
it:

> tracks sessions using a hash of the browser's user agent and IP
> address to identify the client without storing any personal
> information. The salt used to generate these hashes is rotated every
> 4 hours with a sliding window.

There was no Debian package for the project, so I filed a [request for
package](https://bugs.debian.org/964905) and instead made a [fork of the project to add a Docker
image](https://github.com/anarcat/goatcounter).

This page documents how Goatcounter was setup from there...

[[!toc]]

# Initial server configuration

Note that this procedure was slightly modified to use `docker-compose`
to ease upgrades, see the upgrade procedure at the end for details.

 1. build the image from [this fork](https://github.com/anarcat/goatcounter)

        docker build -t zgoat/goatcounter .

 2. create volume for db:

        docker volume create goatcounter

 3. start the server:

        exec docker run --restart=unless-stopped --volume="goatcounter:/home/user/db/" --publish 127.0.0.1:8081:8080 --detach zgoat/goatcounter serve -listen :8080 -tls none

 4. apache configuration:

        <VirtualHost *:80>
                    ServerName analytics.anarc.at
                    Redirect / https://analytics.anarc.at/
                    DocumentRoot /var/www/html/
            </VirtualHost>

        <VirtualHost *:443>
                ServerName analytics.anarc.at
                Use common-letsencrypt-ssl analytics.anarc.at
                DocumentRoot /var/www/html/
                ProxyPass /.well-known/ !
                ProxyPass / http://localhost:8081/
                ProxyPassReverse / http://localhost:8081/
                ProxyPreserveHost on
        </VirtualHost>

 5. add `analytics.anarc.at` to DNS

 6. create a TLS cert with LE:

        certbot certonly --webroot  -d analytics.anarc.at --webroot-path /var/www/html/

    note that goatcounter has code to do this on its own, but we avoid
    it to follow our existing policies and simplify things

 7. create site:

        docker run -it --rm --volume="goatcounter:/home/user/db/" zgoat/goatcounter create -domain analytics.anarc.at -email anarcat+rapports@anarc.at

 8. [add to ikiwiki template](https://gitlab.com/anarcat/ikiwiki-bootstrap-anarcat/-/commit/bde10038f12218a0cd0cea0a4900d9fd3f23e185)

 9. rebuild wiki:

        ikiwiki --setup ikiwiki.setup --rebuild --verbose

# Remaining issues

 * Docker image should be `FROM scratch`, this is statically built
   golang stuff after all... update: someone else made an [image based
   on alpine](https://github.com/arp242/goatcounter/issues/34#issuecomment-793226053), but still not `FROM scratch`... mine could possibly
   work, but I'm not sure how to create the user... or do i need a
   `USER` to run as non-root? TBD.
 * this is all super janky and should be put in config management
   somehow
 * do log parsing instead of Javascript or 1x1 images? now possible,
   but not deployed (yet?)

# Fixed issues

 * <del>cache headers are wrong (120ms!)</del> deployed workaround in
   apache, [reported as a bug upstream](https://github.com/zgoat/goatcounter/issues/342)
 * <del>remove self-referer</del> done, just a matter of configuring
   the URL in the settings. could this be automated too?
 * <del>add pixel tracking for `noscript` users</del> done, but
   required a [patch to ikiwi](https://ikiwiki.info/ikiwiki.cgi?do=goto&page=todo%2Finclude_page_variable_in_base_templates) (and I noticed [another bug while
   doing it](https://ikiwiki.info/ikiwiki.cgi?do=goto&page=bugs%2Fjavascript_resources_placed_after_html_tag))
 * <del>`goatcounter monitor` [doesn't with sqlite](https://github.com/zgoat/goatcounter/issues/343)</del> (fixed upstream!)
 * <del>the :8080 port leaks in some places, namely in the "Site config"
   documentation</del> that is because i was using `-port 8080` which
   was not necessary.
 * <del>move to Docker Compose or podman instead of just starting the thing
   by hand</del> done, although i still need to watch for releases
 * <del>compare with goaccess logs, probably at the end of july, to have
   two full weeks to compare.</del> done, see [[this post|blog/2022-01-28-one-year-visitors]].
 * <del>remove "anarc.at" test site (the site is the analytics site,
   not the tracked site), seems like [this is not possible
   yet](https://github.com/zgoat/goatcounter/issues/344)</del> fixed upstream, site marked as deleted
 * <del>titles are messed up since the 2.x upgrade, see [upstream bug
   report](https://github.com/arp242/goatcounter/issues/559)</del> due to a database migration that normalized the
   tables, will fix itself eventually apparently

# Upgrade procedure

This procedure was done to upgrade from ~1.4.0 to 1.4.2, 2.0.4, and 2.1.1:

 1. rebase on top of target release
 
        cd ~anarcat/dist/goatcounter/
        git rebase v2.1.1

 2. build image:
 
        docker build -t anarcat/goatcounter:2.1.1 .

 3. stop old container:
 
        cd ~anarcat/src/composes/goatcounter/
        docker-compose rm --stop

 4. backup container data:
 
        tar -c -z -C /var/lib/docker/volumes/ -f /var/lib/docker/volumes/goatcounter.tgz goatcounter_goatcounter/

 5. update the image version:

        sed -i s,anarcat/goatcounter:.*,anarcat/goatcounter:2.1.1, docker-compose.yml

 4. run the migrations:
 
        docker-compose run goatcounter db migrate all

 5. start container with new image, make sure to update the `image:` tag:

        docker-compose up -d

[[!tag blog debian-planet python-planet privacy meta ikiwiki stats]]
