Pour déployer [[!wikipedia DNSSEC]], il faut passer par quelques étapes.

 1. *utiliser* DNSSEC - <del>on devrait vérifier la validation des zones, pour commencer</del> ça marche!
 2. *signer* les zonefiles - ensuite, une fois qu'on compren mieux comment ça marche, on déploie nos propres certifications

Après ça, on peut déployer des trucs comme [[!wikipedia DANE]] (voir [[ssl]]) pour des vérifications des certificats [[ssl]].

Utilisation
===========

Ceci a impliqué de basculer à [Unbound](https://unbound.net), un serveur de cache DNS qui utilise DNSSC par défaut et qui fonctionne surprennament bien.

La seule configuration nécessaire fut au niveau du [[split_view]] et de faire qu'il écoute sur toutes les interfaces. Extrait de la configuration:

    server:
            interface: 0.0.0.0
            interface: ::0
            access-control: 0.0.0.0/0 allow
            access-control: ::0/0 allow
            auto-trust-anchor-file: "/usr/local/etc/unbound/root.key"

    stub-zone:
            name: "anarc.at"
            stub-addr: 192.168.0.3

Plusieurs blocs `stub-zone` ont été bien sûr configurés.

Pour que DNSSEC fonctionne, il faut un *trust anchor*, la clef de signature du root zone qui signe toutes les zones en dessous. On peut obtenir cette clef sur le [site de l'IANA](https://data.iana.org/root-anchors/draft-icann-dnssec-trust-anchor.html), puis vérifier avec les certificats X509 ou OpenPGP:

    curl https://data.iana.org/root-anchors/icann.pgp | gpg --import
    wget https://data.iana.org/root-anchors/root-anchors.asc
    wget https://data.iana.org/root-anchors/root-anchors.xml
    gpg --verify root-anchors.asc root-anchors.xml

(!) Je n'ai pas un bon *trust path* vers la clef de l'ICANN, voir [cette page](http://pgp.cs.uu.nl/mk_path.cgi?FROM=0F6C91D2&TO=7B75921E&PATHS=trust+paths), mais on va faire avec.

Ensuite, rouler `unbound-anchor` pour générer le fichier *anchor*, et vérifier que le *digest* correspond à celui donné par `unbound-anchor -l`.

Pour tester DNSSEC, on suit la procédure ([section 2.3.4](http://www.nlnetlabs.nl/publications/dnssec_howto/#x1-130002.3.4) du [howto DNSSEC][]):

    ; <<>> DiG 9.8.4-rpz2+rl005.12-P1 <<>> example.net SOA +dnssec
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 15918
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 3, ADDITIONAL: 1
    
    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags: do; udp: 4096
    ;; QUESTION SECTION:
    ;example.net.                   IN      SOA
    
    ;; ANSWER SECTION:
    example.net.            3001    IN      SOA     sns.dns.icann.org. noc.dns.icann.org. 2013102942 7200 3600 1209600 3600
    example.net.            3001    IN      RRSIG   SOA 8 2 3600 20140211093433 20140204023020 48175 example.net. PMnS365WKz9Cq9dI7ABAwMtnfPk8qtgwYLltqEyienj5oyNHrkC23exq OdnGJvtspBa2qchkBsfPPm3Z4DONfPweahuJj+KTx/yCUGISCA6x3jHm PIeVwSPMWioEBrnGeyCYhS13VvVx+jqNUq9Rbqm/5Qa/6rU6jA0zOZkt 3Fg=
    
    ;; AUTHORITY SECTION:
    example.net.            172201  IN      NS      b.iana-servers.net.
    example.net.            172201  IN      NS      a.iana-servers.net.
    example.net.            172201  IN      RRSIG   NS 8 2 172800 20140211152532 20140204023020 48175 example.net. Xvzw4XXi5YR+t+vriQ0a9pdboZW32W+AgWeGnJ3j+4QcYFkVrGWp8PF1 HfHPQ2gBsGeK75zJS1p2HP52Pi8OZWutBmpb4Z9yKWMg1wH5aLEXwSvU 9P79GEmo+LN2lITx6mSb/xDtXA4Cpytwg+D+FuPMufyhFpOoceYV2LtY qPw=
    
    ;; Query time: 1 msec
    ;; SERVER: 192.168.0.1#53(192.168.0.1)
    ;; WHEN: Mon Feb  3 23:05:04 2014
    ;; MSG SIZE  rcvd: 484

La requête ci-haut n'est en fait *pas* vérifiée parce que le *flag* `ad` n'est pas inscrit dans la section `flags` (juste avant `QUERY`, on voit `flags: qr rd ra;` mais pas `ad`).

Une requête qui fonctionne:

    ; <<>> DiG 9.8.4-rpz2+rl005.12-P1 <<>> example.net SOA +dnssec
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 58677
    ;; flags: qr rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 3, ADDITIONAL: 1
    
    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags: do; udp: 4096
    ;; QUESTION SECTION:
    ;example.net.                   IN      SOA
    
    ;; ANSWER SECTION:
    example.net.            3509    IN      SOA     sns.dns.icann.org. noc.dns.icann.org. 2013102942 7200 3600 1209600 3600
    example.net.            3509    IN      RRSIG   SOA 8 2 3600 20140211093433 20140204023020 48175 example.net. PMnS365WKz9Cq9dI7ABAwMtnfPk8qtgwYLltqEyienj5oyNHrkC23exq OdnGJvtspBa2qchkBsfPPm3Z4DONfPweahuJj+KTx/yCUGISCA6x3jHm PIeVwSPMWioEBrnGeyCYhS13VvVx+jqNUq9Rbqm/5Qa/6rU6jA0zOZkt 3Fg=
    
    ;; AUTHORITY SECTION:
    example.net.            172709  IN      NS      a.iana-servers.net.
    example.net.            172709  IN      NS      b.iana-servers.net.
    example.net.            172709  IN      RRSIG   NS 8 2 172800 20140211152532 20140204023020 48175 example.net. Xvzw4XXi5YR+t+vriQ0a9pdboZW32W+AgWeGnJ3j+4QcYFkVrGWp8PF1 HfHPQ2gBsGeK75zJS1p2HP52Pi8OZWutBmpb4Z9yKWMg1wH5aLEXwSvU 9P79GEmo+LN2lITx6mSb/xDtXA4Cpytwg+D+FuPMufyhFpOoceYV2LtY qPw=
    
    ;; Query time: 0 msec
    ;; SERVER: 192.168.0.1#53(192.168.0.1)
    ;; WHEN: Mon Feb  3 23:37:41 2014
    ;; MSG SIZE  rcvd: 484

Déploiement
===========

À faire, voir [la section 3](http://www.nlnetlabs.nl/publications/dnssec_howto/#x1-220003) du [howto DNSSEC][] qui semble une bonne référence.

 [howto DNSSEC]: http://www.nlnetlabs.nl/publications/dnssec_howto

Il faut répondre à ces questions:

 1. What will be the sizes of your keys? (*probably around 1024 bits, see [this article](https://dnssec.surfnet.nl/?p=57)*)
 1. Will you separate the key- and zone-signing keys functionality? (*not sure*)
 1. How often will you roll the keys? (*every few years, maybe*)
 1. How will system administrators that intend to use your zone as a trust anchor get hold of the  appropriate public key and what mechanism will you provide to enable them to validate the authenticity of your public key? (*root DNSSEC for .org, PGP sigs for .at*)
 1. How will you signal a key rollover or how can you make sure that all interested parties are aware of a rollover? (*see [section 5](http://www.nlnetlabs.nl/publications/dnssec_howto/#x1-400005) of the [howto DNSSEC][]*)

Pour activer DNSSEC dans bind, il faut ajouter, au bloc `options {}`:

    dnssec-enable yes;

Ensuite, suivre la section 3. Mais il est peut-être préférable d'utiliser un outil comme [[!debpkg opendnssec]] qui fait la signature des zones automatiquement...

Pour tester ses zones, on peut utiliser [DNS Viz](http://dnsviz.net/).

Autre documentation
===================

 * [Howto Nlnet labs](http://www.nlnetlabs.nl/publications/dnssec_howto)
 * [Wiki Arch linux](https://wiki.archlinux.org/index.php/DNSSEC)
