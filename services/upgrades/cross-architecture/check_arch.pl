#!/usr/bin/perl -w

if ($#ARGV != 1) {
    print <<EOF
Usage: $0 <from> <to>

Inspects the packages in architecture <from> to see if they are all in
architecture <to>, and at the same version.
EOF
;
    exit 1;
}
my ($from, $to) = @ARGV;

my %pkg_from = ();
my %pkg_to = ();

my $remaining = 0;

open (DPKG, 'dpkg -l |') or die("dpkg doesn't like us: $!");
while (<DPKG>) {
    # ii  libgcc1:amd64                                 1:4.7.2-5  amd64        GCC support library
    # ii  libgcc1:i386                                  1:4.7.2-5  i386         GCC support library
    if (/^(\w\w)\s+(\S+):(\S+)\s+(\d\S+)\s+(\S*).*$/) {
        my ($state, $name, $arch, $version, $arch2) = ($1, $2, $3, $4, $5);
        if ($arch eq $from) {
          #print "package found: $1, $2, $3, $4, $5\n";
          $pkg_from{$name} = $version;
          $remaining += 1;
        } elsif ($arch eq $to) {
          $pkg_to{$name} = $version;
        }
        elsif ($4 eq "all") {
          # noop
        } else {
          print "malformed line: $_\n";
        }
    }
}
close(DPKG);

print "finished parsing dpkg -l, $remaining remaining packages found in arch $from\n";

while( my( $name, $version ) = each( %pkg_from ) ) {
    if (defined($pkg_to{$name})) {
        if ($pkg_to{$name} ne $pkg_from{$name}) {
            warn("package version differs between arch for $name: " .  $pkg_to{$name} . " != " . $pkg_from{$name} . "\n");
        }
    }
    else {
        warn("package $name missing in arch $to\n")
    }
}
