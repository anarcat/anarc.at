I keep on loosing track of how to connect to the damn phone when i need
it. So here's a reminder.

Commandline
===========

Here's a foolproof commandline mode:

 1. disable freaking network-manager, otherwise it will eat the routes
    created by the ppp daemon

 1. enable bluetooth. this could be done with the `blueman-applet`
    gizmo, or the following command:

        rfkill unblock bluetooth

 2. find the bluetooth address and channel of the phone:

        sdptool search DUN

    this could take a while, be patient.

 2. if not already done, pair with the device:

        bluetoothctl pair B0:89:91:94:23:65

 3. connect the serial modem port, with `rfcomm`:

        rfcomm connect rfcomm0 B0:89:91:94:23:65 7

 4. at this stage, there should be a `/dev/rfcomm0` devices that can
    now act as a modem. assuming wvdial is properly configured (see
    below), it should be as simple as:

        wvdial bluetooth

 5. when you're done, turn off bluetooth again:

        rfkill block bluetooth

wvdial
------

I use the `wvdial` package instead of `ppp` because it's simpler to
use. I just dropped this in `/etc/wvdial.conf`:
 
<pre>
[Dialer Defaults]
Phone = *99#
Username = videotron
Password = videotron
Modem Type = bluetooth Modem
ISDN = 0
Init = ATZ
#Init2 = ATQ0 V1 E1 S0=0 &C1 &D2 +FCLASS=0
#Init2 = ATQ0 V1 E1 S0=0 &C1 &D2  S11=55
Init3 = AT+CGDCONT=1,"IP","media.videotron"
Modem = /dev/rfcomm0
Baud = 115200
Stupid Mode = yes
Carrier check = no

[Dialer rfcomm1]
Modem = /dev/rfcomm1

[Dialer rfcomm2]
Modem = /dev/rfcomm2
</pre>

The above is for videotron. The two `Dialer` sections are for testing
with multiple `rfcomm` devices when they start clogging up.

GUI
===

If the GUI works (which [doesn't work in Jessie][]), follow those steps
after starting `blueman-applet`:

1. right-click on the phone in the devices list
2. click on `Serial server`

It should show `Serial port connected to /dev/rfcomm0` in the status
bar (that's the part that [doesn't work in Jessie][], it says `No such
file or directory` instead). Then use `network-manager` or the above
`wvdial` to do the `PPP` connexion.

 [doesn't work in Jessie]: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=779613

Videotron settings
==================

This is dumb and dumber, but:

* access point: `media.videotron`
* username: `videotron`
* password: `videotron`

Yes indeed, heavy security. You shouldn't need to put that anywhere
anymore, but if you need to, those magical secrets are there.

Phone settings
==============

You usually want `PC Suite`. If you want `PC internet`, it's because
you want the reverse: connecting your phone to the internet through
your laptop. To do that, look at the guides in the reference below
since you have internet access anyways now.

Computer settings
=================

To make the sim card work in my x120e, i need a separate modem. See:

http://www.thinkwiki.org/wiki/Mini-PCI_Modem_card
http://www.amazon.com/Lenovo-0A36320-ThinkPad-Gobi-4000/dp/B00HYG93BA

Aka the "Gobi pci-e wwan modem". ie. the sim card socket is there, but
not the modem! It's between 60 and 120$.

Also, see:

https://wiki.archlinux.org/index.php/Lenovo_ThinkPad_T400s#UMTShttps://wiki.archlinux.org/index.php/Lenovo_ThinkPad_T400s#UMTS

Troubleshooting
===============

I sometimes get this in `wvdial`:

    --> Sending: ATZ
    ERROR

... accompanied by this in `dmesg`:

    [mar 2 22:48] Bluetooth: TIOCGSERIAL is not supported

This happens when `rfcomm` configures a connexion on a channel that
works, but that is *not* related to `DUN` (Dial-Up Networking, e.g.
headphones). In that case, `AT` commands will fail and the phone will
ask for confirmation before allowing the connexion (whereas it usually
accepts without prompting when the proper `DUN` channel is used).

`Modem not responding` errors can also happen when the wrong channel is
used, in this case it's when a completely unused channel is used.

If wvdial connected but traffic doesn't flow, try to recreate the
routes:

    ip route add default via 10.211.19.105

The IP address is provided as the `remote IP address` by wvdial.

The following command will show all services visible on bluetooth:

    sdptool browse

Other fun errors I found in my amazing travels:

    --> Sending: ATZ
    ATZ
    AT%IPSYS?
    ERROR
    --> Bad init string.

    --> Sending: ATZ
    OK
    --> Sending: AT+CGDCONT=1,"IP","media.videotron"
    AT+CGDCONT=1,"IP","media.videotron"
    AT%IPSYS?
    ATE0
    OK
    --> Modem initialized.
    --> Sending: ATDT*99#
    --> Waiting for carrier.
    ^CCaught signal 2:  Attempting to exit gracefully...

    --> Sending: ATZ
    NO CARRIER
    GB255
    OK
    --> Sending: AT+CGDCONT=1,"IP","media.videotron"
    OK
    --> Modem initialized.
    --> Sending: ATDT*99#
    --> Waiting for carrier.

    ^CCaught signal 2:  Attempting to exit gracefully...

But the best remains, deep in my heart, that dear:

    +CME ERROR: 148

That is a "Unspecified GPRS error" according to the [gnokii
wiki](http://wiki.gnokii.org/index.php/AT_Errors) ([local
copy](gsm-at-codes)).

I gotta admit this one makes me crack as well:

    --> Waiting for carrier.
    ~[7f]}#@!}!}!} }4}"}&} } } } }%}&[16]"J$}'}"}(}"*"~~[7f]}#@!}!}!}
    }4}"}&} } } } }%}&[16]"J$}'}"}(}"*"~

Wow. And here's more:

    --> Sending: ATZ
    011915003066632
    LG Electronics Inc
    OK
    --> Sending: AT+CGDCONT=1,"IP","media.videotron"
    OK
    --> Modem initialized.
    --> Sending: ATDT*99#
    --> Waiting for carrier.
    +CGDCONT: (1-255),"IP",,,(0),(0,1,2)
    OK
    +CFUN: 1,0
    +CPIN: READY

And the unforgettable:

    OK
    GB255gAT-00-V10f-302-220-NOV-17-2010
    OK
    011915003066632
    LG Electronics Inc
    OK
    +CGDCONT: (1-255),"IP",,,(0),(0,1,2)
    OK
    OK
    +CRSM: 144,0,"98035200011132411410"
    OK
    OK
    302500200225336
    +CRSM: 144,0,"00FFFF03"
    OK
    OK
    +CNUM: "test\5Ftag","*mycensoredphonenumber*",145
    OK
    011915003066632
    OK
    +CLCK:
    ("SC","PS","PN","PU","PP","PC","FD","AO","OI","OX","AI","IR","AB","AG","AC")
    OK
    OK
    OK

Or better yet, the tasty:

    --> Sending: ATZ
    AT+CRSM=176,28589,0,0,
    +CRSM: 144,0,"00FFFF03"
    ATRSM=176,284817
    OK
    --> Sending: AT+CGDCONT=1,"IP","media.videotron"
    OK
    --> Modem initialized.
    --> Sending: ATDT*99#
    --> Waiting for carrier.
    NUM
    +CNUM: "test\5Ftag","*mycensoredphonenumber*",145
    GSN
    OK
    LCK?
    OK
    AT+CU=?
    +CUSD: (0-2)
    MI=
    +CNMI: (0-2),(0-3),(0-3),(0-2),(0-1)
    PMS=?
    +CPMS: ("MT","ME","SM","SR"),("MT","ME","SM"),("MT","ME","SM","SR")

Awesome stuff. Reseting everything a bunch of times and waiting
eventually fixed this after 30 minutes of fudging around.

Sources
=======

 * [Koumbit's guide](https://wiki.koumbit.net/CellulairesVid%C3%A9otronGuide)
 * [bgm's guide](http://www.bidon.ca/en/random/2010-04-06-internet-par-cellulairebluetooth-sur-un-laptop-debian-gnulinux-fido)
 * [Arch's guide](https://wiki.archlinux.org/index.php/Bluetooth_GPRS_Howto)
 * [Debian's guide](https://wiki.debian.org/BluetoothUser)
 
