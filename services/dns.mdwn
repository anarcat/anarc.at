Le service de DNS est géré par bind.

Le service de DNS n'est pas censuré d'aucune façon, mais donne des réponses différentes de l'intérieur ou l'extérieur du réseau.

Il supporte [[IPv6]].

[[!toc levels=2]]

Problèmes connus
================

Certains noms de domaine ne resolvent pas correctement, particulièrement sur le domaine `debian.org`. On soupçonne que c'est lié à DNSSEC, voir [ce post][] pour plus d'information.

Un workaround a été installé dans Unbound, le nameserver récursif:

    edns-buffer-size: 1280

[ce post]: http://serverfault.com/questions/405650/why-are-these-udp-packets-being-dropped

Domaines disponibles
==================

Les zones desservies par le réseau sont:

 * `orangeseeds.org`
 * `anarc.at`
 * `anarcat.ath.cx` - géré chez dyndns.org
 * `reseaulibre.ca` - géré chez gandi.net

Documentation locale
====================

 * [[split view]] - essentiel sur un réseau privé!
 * [[change]] - comment changer le nom du réseau ici
 * [[dnssec]] - la validation des noms de domaines
 * [[migration]] - comment migrer vers une nouvelle IP

Autres fournisseurs
===================

Je considère des alternatives à Gandi. Le temps où Gandi était une
bande d'anarchistes prêt à foutre le feu au système de noms de domaine
est largement révolu: la seule chose qu'ils ont gardé après la vente
est un slogan pourri, en anglais. Leur interface "v5" est parfaitement
infecte, difficile à utiliser, et n'a pas la flexibilité de l'ancienne
interface (qui permettait par example à un tier de payer un nom de
domaine).

J'ai d'ailleurs eu une expérience étrange cette semaine: un nom de
domaine d'un ancien client (enfin, un client de Koumbit) s'est
retrouvé dans ma liste de domaine, avec moi comme contact
partout. J'ai informé Koumbit et, bizarrement, ils ont été capables de
changer tous les contacts du domaine **même s'ils n'étaient pas dans
la liste des contacts**. C'est assez inquiétant et m'indique qu'il
n'est pas clair, dans l'interface, qui a accès à mes domaines.

Voici une comparaison des prix de renouvellement d'un domaine. Les
chiffres viennent de <https://tldes.com/> sauf pour quelques uns qui
n'y sont pas listés.

| Registry          | .com     | .org     | .net     | .ca      | .at      | .info    | Notes                                                           |
|-------------------|----------|----------|----------|----------|----------|----------|-----------------------------------------------------------------|
| bookmyname.com    | 12.37USD | 14.10USD | 14.09USD | N/A      | 18.90USD | 20.61USD | used by friends, no .ca                                         |
| cloudflare.com    | 9.77USD  | 10.11USD | 10.10USD | N/A      | N/A      | 16.18USD | forces you to use their DNS servers                             |
| dnssimple.com     | 14.50USD | 14.00USD | 16.00USD | 16.00CAD | 18.00USD | 21.60USD | also hosting, support for [RFC 8078][]                          |
| dynadot.com       | 11.99USD | 10.99USD | 11.99USD | 9.99USD  | 13.25USD | 17.99USD | hosted wikileaks, blocked in india for cybersquatting           |
| easydns.com       | 19.00USD | 19.00USD | 15.00USD | 12.53USD | 24.00USD | 26.00USD | 12.52USD = 15.00CAD, also hosting                               |
| gandi.net         | 23.99USD | 24.99USD | 24.99USD | 24.99CAD | 29.99USD | 39.99USD | pas de transfer lock .at, [vendus][], prix explosés             |
| glauca.digital    | 22.34USD | 27.58USD | 30.09USD | 28.80CAD | 22.05EUR | 28.74USD | excellent API, support for [RFC 7344][] and [RFC 8078][]        |
| infomaniak.com    | 12.23EUR | 14.70USD | 13.36USD | 14.86USD | 14.70USD | 20.06USD | 2,40 € / year extra for domain privacy, requires identity check |
| joker.com         | 16.99USD | 16.88USD | 18.67USD | N/A      | 15.99USD | 28.80USD |                                                                 |
| leaseweb.com      | 14.40USD | 16.20USD | 15.36EUR | 53.88EUR | 14.40EUR | 16.20USD | rebates for Debian developers                                   |
| mythic-beasts.com | £14.50   | £15.50   | £16.50   | £18.00   | £18.00   | £24.00   | previously used by debian.org, trusted, OpenSRS reseller        |
| mythic-beasts.com | 18.09USD | 19.34USD | 20.59USD | 22.60USD | 22.60USD | 29.95USD | same, USD                                                       |
| namecheap.com     | 16.06USD | 15.16USD | 15.16USD | 13.98USD | N/A      | 21.16USD | 3USD rebate with COUPONFCNC code                                |
| namesilo.com      | 13.95USD | 10.79USD | 11.79USD | 9.99USD  | 11.99USD | 18.49USD |                                                                 |
| nic.at            | N/A      | N/A      | N/A      | N/A      | 40.80EUR | N/A      |                                                                 |
| njal.la           | 16.09USD | 16.09USD | 16.09USD | N/A      | N/A      | 32.19USD | 16.09USD=15EUR pas un registry, anonyme                         |
| openprovider.com  | 16.98USD | 19.99USD | 19.99USD | 20.10USD | 21.03USD | 32.20USD | has a "member" price, suggested by a coworker                   |
| opensrs.net       | 13.75USD | 15.00USD | 15.50USD | 16.00USD | 17.00USD | 22.00USD | reseller, bulk pricing, 95$ minimum                             |
| porkbun.com       | 10.37USD | 10.72USD | 11.48USD | 9.20USD  | N/A      | 16.97USD |                                                                 |

Voir aussi [cette discussion](https://lobste.rs/s/flcpop/what_domain_registrar_is_worth_using).

[vendus]: https://news.gandi.net/en/2019/02/futureofgandi-the-adventure-continues/
[bis]: https://amp-bourse.lefigaro.fr/fonds/montefiore-investment-cede-sa-participation-dans-gandi-a-total-webhosting-solutions-20230221
[RFC 7344]: https://tools.ietf.org/html/rfc7344
[RFC 8078]: https://tools.ietf.org/html/rfc8078

Update: Gandi ont encore monté leurs prix, et je quitte. Je teste
présentement:

 * infomaniak: échec. m'ont facturé le transfert qu'il fonctionne, a
   bloqué sur une demande de vérification d'identité qui demande un
   selfie avec un passeport, par leur application custom, rien de
   moins... bypassé grâce à un contact à l'interne, mais ils m'ont
   quand même facturé alors que le transfert a échoué (il a été fait
   vers mythic-beasts), je considère demander un remboursement ou
   simplement quitter / ignorer

 * mythic-beasts: découvert que c'est un revendeur OpenSRS, donc moins
   intéressant, coûteux, mais trusted... pour l'instant un seul
   domaine là, à migrer vers OpenSRS?

 * OpenSRS: reste ~70$USD de crédit, intéressant parce que très
   puissant, mais pas sûr que je veux être revendeur, j'aurais
   probablement jamais le débit (1000 *nouveaux* par an!) pour avoir
   des rabais, mais somme toute assez bon: DNSSEC fonctionnel, API,
   comptes revendeurs, etc, autre problème: pas de facturation
   automatique sur VISA, il faut débiter manuellement

À considérer, sinon:

 * porkbun - pas cher, mais pas de .at... aussi, c'est un owner de
   TLD, alors watch out les conflits d'intérêts

 * namecheap / easydns / dnssimple - tous relativement équivalents,
   plus chers, mais font aussi serveur DNS, namecheap a pas de .at et
   easyDNS est très cher pour le .at

Situation actuelle:

 * opensrs: test account created, hosted: `debian-policy.info`
   (2025-10-15), not sure I want to keep, could be just for `anarc.at`
 * mythic beasts: idem, to be closed, hosted: `alterne.ca`
   (2025-09-11), maybe keep for `anarc.at` and close OpenSRS because
   it's too complicated?
 * porkbun: `orangeseeds.net` `orangeseeds.org` (transfer started
   2023-12-19), `vichama.ca` (2024-05-17)
 * gandi: `reseaulibre.ca` (2024-04-28), `anarc.at` (2024-09-06),
   `insomniaque.org` (2029-04-28)

Convention de noms
==================

Les noms des machines sur le réseau sont des personalités ou autrices
inspirantes (politiquement ou autre), préférablement des
femmes. Exemples utilisés:

 * [[hardware/angela]] ([Davis][])
 * [[hardware/bell]] ([Hooks][])
 * [[hardware/louise]] ([Michel][])
 * ([Margaret][]) [[hardware/atwood]]
 * [[hardware/margaret]] ([Hamilton][Margaret Hamilton]) - developed
   the on-board flight software for NASA's Apollo program
 * ([Marie][]) [[hardware/curie]]
 * ([Richard][]) dawkins
 * [[hardware/emma]] ([Goldman][])
 * ([Subcommandante][]) [[hardware/server/marcos]]
 * [[hardware/octavia]] ([E. Butler][])
 * [[hardware/rosa]] ([Luxembourg][] or [Parks][])
 * [[hardware/ursula]] ([K. Le Guin][])
 * [[hardware/server/mafalda]] ([yes, the character][])
 * [[hardware/server/plastik]] (a "piece of plastic")
 * ([Harriet][]) [[hardware/tubman]]
 * [[hardware/evelyn]] ([Evelyn Berezin][] - inventor of the computer
   word processor, developed the first computerized banking system
 * [[hardware/svetlana]] Savitskaya - first women to perform a
   spacewalk, not [Valentina Tereshkova](https://en.wikipedia.org/wiki/Valentina_Tereshkova) who voted for the invasion
   of Ukraine

[Parks]: https://en.wikipedia.org/wiki/Rosa_Parks
[Davis]: https://en.wikipedia.org/wiki/Angela_Davis
[Hooks]: https://en.wikipedia.org/wiki/Bell_hooks
[Michel]: https://fr.wikipedia.org/wiki/Louise_Michel
[Margaret]: https://en.wikipedia.org/wiki/Margaret_Atwood
[Marie]: https://en.wikipedia.org/wiki/Marie_Curie
[Richard]: https://en.wikipedia.org/wiki/Richard_Dawkins
[Goldman]: https://en.wikipedia.org/wiki/Emma_Goldman
[Subcommandante]: https://en.wikipedia.org/wiki/Subcomandante_Marcos
[E. Butler]: https://en.wikipedia.org/wiki/Octavia_E._Butler
[Luxembourg]: https://en.wikipedia.org/wiki/Rosa_Luxemburg
[K. Le Guin]: https://en.wikipedia.org/wiki/Ursula_K._Le_Guin
[yes, the character]: https://en.wikipedia.org/wiki/Mafalda
[Harriet]: https://en.wikipedia.org/wiki/Harriet_Tubman
[Fumiko Kaneko]: https://en.wikipedia.org/wiki/Fumiko_Kaneko

Anciens
-------

Ces noms ont été utilisés par le passé et ont été retiré de la
circulation, généralement parce que les machines auxquelles ils ont
été attitrés ont été retirés, mais aussi parce que les noms ne sont
plus compatibles avec la nouvelle convention.

 * [[hardware/server/lenny]] ([[!wikipedia Leonard Peltier]])
 * marvin (the [paranoid android](https://en.wikipedia.org/wiki/Marvin_the_Paranoid_Android))
 * mumia ([Abu Jamal][])
 * orange
 * tangerine
 * roadkill
 * [[hardware/server/roadkiller]]
 * [[hardware/fumiko]] ([Fumiko Kaneko][]) - replaced by [[hardware/evelyn]]

[Abu Jamal]: https://en.wikipedia.org/wiki/Mumia_Abu-Jamal

Potentiels
----------

Les noms suivants pourraient être utilisés pour de futures machines:

 * [Anahareo][] - "writer, animal rights activist and conservationist
   of Algonquin and Mohawk ancestry"
 * [Hannah Arendt][] - "one of the most important political
   philosophers of the twentieth century"
 * [Wendy Carlos][] - trans women, "helped in the development of the
   Moog synthesizer", wrote music for Kubrick's Clockwork Orange, The
   Shining, basically invented electronic music
 * [Claudette Colvin][] - before Rosa [Parks][], there was this rebel!
 * [Viola Desmond][] - challenged racial segregation in Canada
 * [Ada Lovelace][] - first programmer
 * [Grace Hopper][] - inventor of the compiler and linker
 * [Lynn Conway][] - VLSI inventor, fired by IBM when coming out as
   trans in 1968, rebuilt her carreer from scratch, died in 2024
 * [Séverine][] - journaliste, féministe, première femme à diriger un
   grand quotidien en France
 * [Sister Rosetta Tharpe][] - "first great recording star of gospel
   music", "the original soul sister" and "the Godmother of rock and
   roll", among the first to use distortion on the electric guitar,
   [first rock and roll record][], first interracial duet
 * [Sojourner Truth][] - abolitionist, first black women to win a
   court case against a black man
 * [Phillis Wheatley][] - first African-American author of a published
   book of poetry

Space women:

 * Sally Ride - première américaine dans l'espace
 * Svetlana Savitskaya - première femme à marcher dans l'espace
 * Mae Jemison - première femme noire dans l'espace
 * Eileen Collins - première femme commandante de bord et pilote de la
   navette spatiale

[Margaret Hamilton]: https://en.wikipedia.org/wiki/Margaret_Hamilton_(software_engineer)

Voir aussi [cette liste][] de femmes moins connues mais peut-être
tout aussi importantes...

[Hannah Arendt]: https://en.wikipedia.org/wiki/Hannah_Arendt
[Claudette Colvin]: https://en.wikipedia.org/wiki/Claudette_Colvin
[Viola Desmond]: https://en.wikipedia.org/wiki/Viola_Desmond
[Ada Lovelace]: https://en.wikipedia.org/wiki/Ada_Lovelace
[Grace Hopper]: https://en.wikipedia.org/wiki/Grace_Hopper
[Séverine]: https://fr.wikipedia.org/wiki/S%C3%A9verine
[Sojourner Truth]: https://en.wikipedia.org/wiki/Sojourner_Truth
[cette liste]: https://www.hillelwayne.com/important-women-in-cs/
[Wendy Carlos]: https://en.wikipedia.org/wiki/Wendy_Carlos
[Sister Rosetta Tharpe]: https://en.wikipedia.org/wiki/Sister_Rosetta_Tharpe
[first rock and roll record]: https://en.wikipedia.org/wiki/First_rock_and_roll_record
[Phillis Wheatley]: https://en.wikipedia.org/wiki/Phillis_Wheatley
[Anahareo]: https://en.wikipedia.org/wiki/Anahareo?wprov=sfla1
[Evelyn Berezin]: https://en.wikipedia.org/wiki/Evelyn_Berezin
[Lynn Conway]: https://en.wikipedia.org/wiki/Lynn_Conway

Relié
=====

 * [[réseau]]
 * [[mesh]]
 * [[wifi]]

Voir aussi [Zytrax.com][], une excellente documentation de BIND.

[Zytrax.com]: http://www.zytrax.com/books/dns/
